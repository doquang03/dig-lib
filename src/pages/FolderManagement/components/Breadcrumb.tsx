import { HomeIcon, RightChev } from 'assets';
import { Link } from 'react-router-dom';
import { Route } from './Toolbar';

type Props = {
  items: Route[];
  onRefresh: VoidFunction;
};

function Breadcrumb(props: Props) {
  const { items, onRefresh } = props;

  return (
    <div className='flex w-full items-center justify-between rounded-md border-[0.5px] border-[#00000033] px-2 py-1'>
      <div className='flex items-center gap-x-2'>
        <HomeIcon />

        {items?.map((item) => (
          <Link to={item?.key} key={item?.key} className='flex items-center gap-x-1'>
            <RightChev />

            <span className='truncate whitespace-nowrap text-[14px]'>{item?.label}</span>
          </Link>
        ))}
      </div>

      <div className='flex items-center gap-x-1'>
        <div className='mx-2 h-[2rem] w-[1px] bg-[#00000033]' />

        <button onClick={onRefresh}>
          <svg width={20} height={20} viewBox='0 0 20 20' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path
              d='M12.5 5.70312C12.5 5.70312 13.4516 5.23438 10 5.23438C8.76387 5.23438 7.5555 5.60093 6.52769 6.28769C5.49988 6.97445 4.6988 7.95057 4.22576 9.0926C3.75271 10.2346 3.62894 11.4913 3.87009 12.7037C4.11125 13.9161 4.70651 15.0297 5.58059 15.9038C6.45466 16.7779 7.56831 17.3731 8.78069 17.6143C9.99307 17.8554 11.2497 17.7317 12.3918 17.2586C13.5338 16.7856 14.5099 15.9845 15.1967 14.9567C15.8834 13.9289 16.25 12.7205 16.25 11.4844'
              stroke='#A7A7A7'
              strokeMiterlimit={10}
              strokeLinecap='round'
            />
            <path
              d='M10 2.26562L13.125 5.39063L10 8.51563'
              stroke='#A7A7A7'
              strokeLinecap='round'
              strokeLinejoin='round'
            />
          </svg>
        </button>
      </div>
    </div>
  );
}

export default Breadcrumb;
