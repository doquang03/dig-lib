import { useMutation } from '@tanstack/react-query';
import { Modal, ModalProps } from 'antd';
import { bookStoreApis } from 'apis';
import classNames from 'classnames';
import { Button } from 'components';
import 'css/ChangeStore.css';
import { useMemo, useState } from 'react';
import { toast } from 'react-toastify';
import { analysisColor } from 'utils/utils';

type Props = {
  ids: string[];
  idKhoSach: string;
  isLoadingSachCaBiet: boolean;
  ListKhoSach: BookStoreType[];
  ListMaMau: Color[];
  onHide: VoidFunction;
  onSuccess: VoidFunction;
} & ModalProps;

const ChangeStore = (props: Props) => {
  const { ids, idKhoSach, ListKhoSach, ListMaMau, isLoadingSachCaBiet, open, onHide, onSuccess } = props;
  const [selectKhoSach, setSelectKhoSach] = useState('');

  const { mutate: ChangeKhoSach, isLoading } = useMutation({
    mutationFn: (idKhoSachMoi: string) => bookStoreApis.ChuyenKhoSach(ids, idKhoSachMoi, idKhoSach),
    onSuccess: (res, data) => {
      toast.success('Chuyển kho sách thành công');
      onSuccess();
    }
  });

  const handleComplete = () => {
    if (selectKhoSach === '') {
      toast.error('Chưa chọn kho sách');
      return;
    }
    ChangeKhoSach(selectKhoSach);
  };

  const renderElement = (bookStore: BookStoreType) => {
    if (bookStore.Id === idKhoSach) return false;
    let arrColor: {
      Name: string;
    }[] = [];
    ListMaMau.forEach((element) => {
      if (bookStore.Id === element.IdLienKet && element.KieuLienKet === 2) {
        arrColor = element.ListColor;
      }
    });
    const finalColor: string[] = analysisColor(arrColor);
    return (
      <div
        className={classNames(
          'm-1 inline-flex h-[56px]  w-[200px]',
          'animation-card-change',
          selectKhoSach === bookStore?.Id ? 'select-tab-khosach' : '',
          'card-change-book'
        )}
        onClick={() => {
          setSelectKhoSach(bookStore.Id);
        }}
        key={bookStore.Id}
      >
        <div className='colorTab'>
          <div
            style={{
              borderLeft: `15px solid ${finalColor[0]}`,
              borderTop: `15px solid ${finalColor[1]}`,
              borderRight: `15px solid ${finalColor[2]}`,
              borderBottom: `15px solid ${finalColor[3]}`
            }}
          ></div>
        </div>
        <span className='text-box-kho'>{bookStore.Ten}</span>
      </div>
    );
  };

  return (
    <Modal
      open={open}
      title={<h3 className='border-b-2 pb-2 font-bold uppercase text-primary-10'>Chuyển kho sách</h3>}
      closable={false}
      footer={null}
      width={692}
    >
      <div className='mb-4 flex flex-col'>
        <label className='col-span-3 mb-4 text-base'>Chọn kho sách muốn chuyển đến</label>
        <div className='inline-block' style={{ height: '400px', overflow: 'auto' }}>
          {ListKhoSach?.map((x: BookStoreType) => {
            return renderElement(x);
          })}
        </div>
      </div>

      <div className='mt-3 flex items-center justify-end gap-2'>
        <Button type='button' variant='secondary' onClick={onHide} loading={isLoading} disabled={isLoading}>
          Quay về
        </Button>

        <Button loading={isLoading} disabled={isLoading || isLoadingSachCaBiet} onClick={handleComplete}>
          Cập nhật
        </Button>
      </div>
    </Modal>
  );
};

export default ChangeStore;
