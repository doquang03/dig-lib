import { Outlet } from 'react-router-dom';

const Audit = () => {
  return <Outlet />;
};

export default Audit;
