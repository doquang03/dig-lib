import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/TheLoaiSach`;

const typeBookApis = {
  Sua: (idTheLoai: string) => {
    return httpThuVien.post(`/${endPoint}/Sua` + idTheLoai);
  },
  ImportSave: (payload: any) => {
    return httpThuVien.post(`/${endPoint}/ImportSave`, payload);
  },
  EditByID: (id: string) => {
    return httpThuVien.post(`/${endPoint}/EditByID?id=${id}`);
  },
  IndexByTree: (queryString: string) => {
    return httpThuVien.post(`/${endPoint}/IndexByTree` + queryString);
  },
  Xoa: (Id: string) => {
    return httpThuVien.post(`/${endPoint}/Xoa?Id=` + Id);
  },
  PreviewImport: (payload: object) => {
    return httpThuVien({
      method: 'post',
      url: `/${endPoint}/PreviewImport`,
      data: payload,
      headers: { 'Content-Type': 'multipart/form-data' }
    });
  }
};

export default typeBookApis;
