type Unit = {
  IdDonViQuanLy: string;
  MaDonVi: string;
  Ten: string;
  Logo: string;
  LoaiHinh: number;
  LinhVuc: number;
  LoaiHinhDaoTao: number;
  LoaiTruong: string[];
  IdTruongDV: string;
  ListIdPhoDV: string[];
  DiaChi: string;
  DiaChi_IdTinh: string;
  DiaChi_IdHuyen: string;
  DiaChi_IdPhuong: string;
  AccountPrefix: string;
  SubDomain: string;
  CodeName: string;
  DatabaseName: string;
  TrangThai: number;
  Web_Header: string;
  GhiChu: string;
  PhoneNumber: string;
  Id: string;
  CreateDateTime: string;
  AllowDeleteRecord: boolean;
  IsDeleted: boolean;
  InRecycleBin: boolean;
};

type UnitLibrary = {
  DataBaseName: string;
  TongSoSach: number;
  listKhoSach: BookStoreType[];
  TongLuotMuonHS: number;
  TongLuotMuonGV: number;
  DangMuonTreHanHS: number;
  DangMuonTreHanGV: number;
  TraSachDungHanHS: number;
  TraSachDungHanGV: number;
  TraSachTreHanHS: number;
  TraSachTreHanGV: number;
  MuonSachQuaHanHS: number;
  MuonSachQuaHanGV: number;
  LamMatSachHS: number;
  LamMatSachGV: number;
};

type SachDenHanTra = { IdThanhVien: string; TenSach: string; TenThanhVien: string };

type TheMostBorrowingReaders = {
  TenThanhVien: string;
  LopTo: string;
  Value: number;
};

type SchoolDashboard = {
  TongHocSinh: number;
  TongGiaoVien: number;
  TongCanBoThuVien: number;
  TongTaiLieuSo: numbe;
  TongTaiLieuGiay: number;
  TongTaiLieu: number;
  TongHinhAnh: number;
  TongAmThanh: number;
  TongVideo: number;
  DanhSachDenHan: SachDenHanTra[];
  TongSachDangMuon: number;
  TongThanhVien: number;
  TongBanDocVangLai: number;
  TongNguoiDatMuon: number;
  SachCaBietTheoKho: { [key: string]: number };
  KhoSach: { [key: string]: string };
  TopMuonThanhVien: TheMostBorrowingReaders[];
};
