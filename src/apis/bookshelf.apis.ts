import { QueryConfig } from 'hooks/useQueryConfig';
import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/KeSach`;

type GetBookshelfData = {
  lst: Bookshelf[];
} & ResponseList;

const bookshelfApis = {
  getBookshelfs: (payload: QueryConfig) => {
    const { page, pageSize, TextForSearch } = payload;

    return httpThuVien.post<ResponseApi<GetBookshelfData>>(`${endPoint}/GetDataPagingBySearch`, {
      PageSize: pageSize,
      PageNumber: page,
      Keyword: TextForSearch
    });
  },

  getBookshelf: (id: string) => httpThuVien.get<ResponseApi<Bookshelf>>(`${endPoint}/GetById/${id}`),
  deleteBookshelf: (id: string) => httpThuVien.delete<ResponseApi<Bookshelf>>(`${endPoint}/DeleteById/${id}`),
  createBookshelf: (payload: Omit<Bookshelf, 'Id'>) =>
    httpThuVien.post<ResponseApi<{ id: string }>>(`${endPoint}/Create`, payload),
  updateBookshelf: (payload: Bookshelf) => httpThuVien.post<ResponseApi<{}>>(`${endPoint}/Edit`, payload)
};

export default bookshelfApis;
