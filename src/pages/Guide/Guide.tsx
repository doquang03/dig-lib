import { Divider } from 'antd';
import { Title } from 'components';
import { useUser } from 'contexts/user.context';

const guideLines = [
  '1. Kho sách',
  '2. Khởi chạy phần mềm và đăng nhập',
  '3. Quản lý thành viên',
  '4. Quản lý thông tin sách',
  '5. Thông tin sách',
  '6. Thêm từng đầu sách trên giao diện',
  '7. Cài đặt',
  '8. Mượn trả sách',
  '9. Xuất các sổ báo cáo thư viện',
  '10. Thống kê',
  '11. Thư mục sách',
  '12. Sổ theo dõi kinh phí',
  '13. Thống kê bạn đọc',
  '14. Đặt mượn Online'
];

const metaData = [
  ['1. File dữ liệu mẫu nhập từ excel thường', '/Tempalates/MauKhoSach.xls'],
  ['2. File dữ liệu mẫu nhập sổ đăng ký SGK', '/Tempalates/MauSoKhoSach_DKCB_SGK.xls'],
  ['3. File dữ liệu mẫu nhập sổ đăng ký cá biệt', '/Tempalates/MauSoKhoSach_DKCB.xls']
];

const Guide = () => {
  const { userType, isAllowedAdjustment } = useUser();

  if (userType === 2001 && !isAllowedAdjustment) {
    return <h2 className='p-2 '>Bạn không có quyền truy cập chức năng này</h2>;
  }
  return (
    <div>
      <div className='md: flex flex-col justify-between gap-2 md:flex-row'>
        <div className='w-full'>
          <Title title='Hướng dẫn sử dụng' />
          {guideLines.map((item, index) => {
            return (
              <ol key={index} className='mt-2'>
                <a href={`/Tempalates/${item}.pdf`} download={`${item}.pdf`} target='_blank' rel='noreferrer'>
                  {item}
                </a>
              </ol>
            );
          })}
        </div>

        <div className='w-full'>
          <div className='w-full'>
            <Title title='DỮ LIỆU MẪU' />
            {metaData.map((items, index) => (
              <ol key={index} className='mt-2'>
                <a href={items[1]} download={items[1].split('/')[2]}>
                  {items[0]}
                </a>
              </ol>
            ))}
          </div>

          <div className='w-full'>
            <Title title='CÔNG CỤ HỖ TRỢ' />

            <ol className='mt-2'>
              <a target='_blank' href='/Tempalates/ChuyenMaTVExcel.exe'>
                1. Chuyển mã font tiếng việt cho excel
              </a>
            </ol>
          </div>
        </div>
      </div>

      <Divider />

      <div className='w-full md:w-1/2'>
        <Title title='Liên hệ' />

        <div className='mt-2 flex flex-col gap-3'>
          <p>
            <a href='tel:02822532586'>Điện thoại: (028) 22 532 586</a>{' '}
          </p>
          <p>
            <a href='http://zalo.me/0902585262' className='zalo'>
              Zalo: 090.258.5262
            </a>
          </p>
          <p>
            <a href='http://zalo.me/0902585262' className='zalo'>
              Zalo: 090.258.5262
            </a>
          </p>

          <p>
            <a href='mailto:bitech.info123@gmail.com'>Email: bitech.info123@gmail.com</a>
          </p>

          <p>
            <a href='mailto:cskh.bitech@gmail.com'>CSKH: cskh.bitech@gmail.com</a>
          </p>
        </div>
      </div>
    </div>
  );
};

export default Guide;
