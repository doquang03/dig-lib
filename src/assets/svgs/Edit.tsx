import Icon from '@ant-design/icons';
import type { CustomIconComponentProps } from '@ant-design/icons/lib/components/Icon';

export const EditSvg = () => {
  return (
    <svg width='16' height='16' viewBox='0 0 16 16' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path
        d='M12.5997 1.41421C12.8634 1.149 13.221 1 13.5939 1C13.9668 1 14.3245 1.149 14.5882 1.41421C14.8519 1.67943 15 2.03914 15 2.41421C15 2.78929 14.8519 3.149 14.5882 3.41422L8.29124 9.74756L5.63989 10.4142L6.30273 7.74755L12.5997 1.41421Z'
        stroke='#3472A2'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M6.96552 3H2.32567C1.97408 3 1.63689 3.14048 1.38828 3.39052C1.13967 3.64057 1 3.97971 1 4.33333V13.6667C1 14.0203 1.13967 14.3594 1.38828 14.6095C1.63689 14.8595 1.97408 15 2.32567 15H11.6054C11.957 15 12.2942 14.8595 12.5428 14.6095C12.7914 14.3594 12.931 14.0203 12.931 13.6667V9.00001'
        stroke='#3472A2'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  );
};

const EditIcon = (props: Partial<CustomIconComponentProps>) => {
  return <Icon component={() => <EditSvg />} {...props} />;
};

export default EditIcon;
