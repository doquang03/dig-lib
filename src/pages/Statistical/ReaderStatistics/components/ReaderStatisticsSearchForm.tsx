import { DatePicker } from 'antd';
import locale from 'antd/es/date-picker/locale/vi_VN';
import { Input, Select } from 'components';
import { ACCOUNT_TYPE_OPTIONS, RETURN_TYPE_OPTIONS } from 'constants/options';
import dayjs, { type Dayjs } from 'dayjs';
import { debounce } from 'lodash';
import type { PickerMode } from 'rc-picker/lib/interface';
import { ChangeEvent } from 'react';

type Props = {
  picker: PickerMode;
  onChangeSearchParams: (e: ChangeEvent<HTMLSelectElement | HTMLInputElement>) => void;
  onChangeDatePicker: (value: Dayjs | null) => void;
};

const ReaderStatisticsearchForm = (props: Props) => {
  const { picker, onChangeSearchParams, onChangeDatePicker } = props;

  const handleOnChange = debounce((e: ChangeEvent<HTMLSelectElement | HTMLInputElement>) => {
    if (!e) return;

    onChangeSearchParams(e);
  }, 300);

  const handleOnChangeDatePicker = (value: Dayjs | null) => {
    onChangeDatePicker(value || null);
  };
  return (
    <form className='grid grid-cols-12 gap-2 bg-primary-60/20 px-2 py-3'>
      <div className='col-span-6'>
        <Input placeholder='Nhập tên bạn đọc, mã, lớp/tổ' name='textForSearch' onChange={handleOnChange} />
      </div>

      <div className='col-span-2'>
        <Select items={ACCOUNT_TYPE_OPTIONS} name='styleUser' className='w-full' onChange={handleOnChange} />
      </div>

      <div className='col-span-2'>
        <DatePicker
          placeholder={picker === 'month' ? 'Chọn tháng' : 'Chọn ngày'}
          name='rentDate'
          picker={picker}
          onChange={handleOnChangeDatePicker}
          className={'w-full border border-gray-300 py-[0.8rem] px-2 outline-none'}
          format={picker === 'month' ? 'MM/YYYY' : 'DD/MM/YYYY'}
          disabledDate={(current) => current >= dayjs().endOf('day')}
          locale={locale}
          defaultValue={dayjs()}
        />
      </div>
      <div className='col-span-2'>
        <Select items={RETURN_TYPE_OPTIONS} name='returnType' className='w-full' onChange={handleOnChange} />
      </div>
    </form>
  );
};

export default ReaderStatisticsearchForm;
