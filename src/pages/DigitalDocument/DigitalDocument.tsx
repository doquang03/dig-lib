import { Title } from 'components';
import { Helmet } from 'react-helmet-async';
import { useParams } from 'react-router-dom';
import { DocumentsListing } from './components';

const digitalDocuemt = {
  SachDienTu: {
    title: 'Sách điện tử'
  },
  AlbumAnh: {
    title: 'Album ảnh'
  },
  Audio: {
    title: 'Sách nói'
  },
  Video: {
    title: 'Video'
  }
};

const DigitalDocument = () => {
  const { documentType } = useParams<{ documentType: 'SachDienTu' | 'AlbumAnh' | 'Audio' | 'Video' }>() || {};

  if (!documentType) {
    return <div>Tôi là mến</div>;
  }

  return (
    <div>
      <Helmet>
        <meta charSet='utf-8' />
        <title>{digitalDocuemt[documentType].title} - Thư viện số</title>
        <link rel={digitalDocuemt[documentType].title} href={window.location.href} />
      </Helmet>

      <Title title={`Quản lý ${digitalDocuemt[documentType].title}`} />

      <DocumentsListing />
    </div>
  );
};

export default DigitalDocument;
