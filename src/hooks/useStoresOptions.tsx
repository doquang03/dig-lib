import { useQuery } from '@tanstack/react-query';
import { bookstoreTypeApis } from 'apis';
import React, { useMemo } from 'react';

const useStores = () => {
  const {
    data: storesData,
    isLoading,
    ...rest
  } = useQuery({
    queryKey: ['stores'],
    queryFn: bookstoreTypeApis.getAllStores,
    staleTime: Infinity
  });

  const storesResult = useMemo(() => {
    if (!storesData?.data.Item) return [];

    const storeResults = storesData?.data.Item.map(({ Ten, Id }) => {
      return {
        label: Ten,
        value: Id
      };
    });

    return storeResults;
  }, [storesData?.data.Item]);

  return { data: storesResult, isLoading, ...rest };
};

export default useStores;
