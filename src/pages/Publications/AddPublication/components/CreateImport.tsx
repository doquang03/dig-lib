import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Modal, type ModalProps } from 'antd';
import { bookApis } from 'apis';
import { CreatePhieuNhapSachVars } from 'apis/book.apis';
import { Button, DatePicker, Input, InputNumber, Select } from 'components';
import { AutoComplete } from 'components/AutoComplete';
import { UserConText } from 'contexts/user.context';
import dayjs from 'dayjs';
import { useContext, useMemo } from 'react';
import { useForm } from 'react-hook-form';
import { useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { taoPhieuNhapSach } from 'utils/rules';
import { convertDate } from 'utils/utils';

type Props = { onHide: VoidFunction; onSuccess: VoidFunction } & ModalProps;

type FormValues = {
  KhoSach: string;
  SoLuong: string;
  TrangThai: string;
  NguonCungCap: string;
  SoCT: string;
  VSTQ: string;
  NgayNhap: string;
};

const CreateImport = (props: Props) => {
  const { open, onHide, onSuccess } = props;

  const { bookId } = useParams();

  const user = useContext(UserConText);

  const {
    register,
    handleSubmit,
    reset,
    control,
    formState: { errors }
  } = useForm<FormValues>({
    defaultValues: {
      KhoSach: '',
      SoLuong: '1',
      TrangThai: '',
      NguonCungCap: '',
      SoCT: '',
      VSTQ: '',
      NgayNhap: ''
    },
    resolver: yupResolver(taoPhieuNhapSach)
  });

  const { data: optionsData, isLoading: isLoadingForm } = useQuery({
    queryKey: ['GetInitCreateForm'],
    queryFn: () => bookApis.GetInitCreateForm()
  });

  const { mutate: cratePhieuNhapKho, isLoading: isLoadingCreatePhieuNhapKho } = useMutation({
    mutationFn: (payLoad: CreatePhieuNhapSachVars) => bookApis.createPhieuNhapSach(payLoad),
    onSuccess: () => {
      toast.success('Tạo phiếu nhập sách thành công');
      reset();
      onHide();
      onSuccess();
    }
  });

  const {
    Options_KhoSach = [],
    Options_TrangThaiSach = [],
    Options_NguonCungCap = []
  } = useMemo(() => {
    if (!optionsData?.data.Item) {
      return;
    }

    return optionsData?.data.Item;
  }, [optionsData?.data.Item]);

  const handleHide = () => {
    reset();
    onHide();
  };

  const handleCreateImport = handleSubmit((data) => {
    if (!bookId) return;

    cratePhieuNhapKho({
      id: bookId + '',
      IdUserAdmin: user.profile?.Id + '',
      UserName: user.profile?.UserName + '',
      GhiChuPhieuNhap: '',
      DanhSachPhieuNhap: [
        {
          IdKho: data.KhoSach,
          IdNCC: data.NguonCungCap,
          IdTrangThai: data.TrangThai,
          NVS: convertDate(data.NgayNhap),
          SCT: data.SoCT,
          SoLuong: +data.SoLuong,
          SVS: data.VSTQ
        }
      ] as CreatePhieuNhapSachVars['DanhSachPhieuNhap']
    });
  });

  return (
    <Modal
      open={open}
      title={<h3 className='border-b-2 pb-2 font-bold uppercase text-primary-10'>Tạo phiếu nhập kho</h3>}
      closable={false}
      footer={null}
    >
      <form>
        <div className='grid grid-cols-12 content-center items-center gap-3'>
          <div className='col-span-4'>
            <label className='font-bold'>
              Kho <span className='text-danger-10'>*</span>
            </label>
          </div>

          <div className='col-span-8'>
            <AutoComplete
              className='w-full grow'
              name={'KhoSach'}
              placeholder='Nhập kho'
              control={control}
              items={[{ label: 'Chọn kho sách', value: '' }, ...Options_KhoSach]}
              errorMessage={errors.KhoSach?.message}
              showSearch
            />
          </div>

          <div className='col-span-4'>
            <label className='font-bold'>
              Số lượng <span className='text-danger-10'>*</span>
            </label>
          </div>
          <div className='col-span-8'>
            <InputNumber
              control={control}
              name={`SoLuong`}
              placeholder='Nhập số lượng'
              errorMessage={errors?.SoLuong?.message}
              min={1}
              max={999}
            />
          </div>

          <div className='col-span-4'>
            <label className='font-bold'>
              Tình trạng <span className='text-danger-10'>*</span>
            </label>
          </div>
          <div className='col-span-8'>
            <Select
              className='w-full'
              items={[{ value: '', label: 'Chọn tình trạng' }, ...Options_TrangThaiSach]}
              name={`TrangThai`}
              register={register}
              errorMessage={errors?.TrangThai?.message}
            />
          </div>

          <div className='col-span-4'>
            <label className='font-bold'>
              Nguồn cung cấp <span className='text-danger-10'>*</span>
            </label>
          </div>
          <div className='col-span-8'>
            <Select
              items={[{ value: '', label: 'Chọn nguồn cung cấp' }, ...Options_NguonCungCap]}
              name={`NguonCungCap` as const}
              className='w-full'
              register={register}
              errorMessage={errors?.NguonCungCap?.message}
            />
          </div>

          <div className='col-span-4'>
            <label className='font-bold'>Số chứng từ</label>
          </div>
          <div className='col-span-8'>
            <Input
              placeholder='Nhập số chứng từ'
              name={`SoCT` as const}
              register={register}
              errorMessage={errors?.SoCT?.message}
              maxLength={100}
            />
          </div>

          <div className='col-span-4'>
            <label className='font-bold'>Số vào sổ tổng quát </label>
          </div>
          <div className='col-span-8'>
            <Input
              placeholder='Nhập số vào sổ tổng quát'
              name={`VSTQ` as const}
              register={register}
              errorMessage={errors?.VSTQ?.message}
              maxLength={100}
            />
          </div>

          <div className='col-span-4'>
            <label className='font-bold'>
              Ngày vào sổ <span className='text-danger-10'>*</span>
            </label>
          </div>
          <div className='col-span-8'>
            <DatePicker
              control={control}
              name={`NgayNhap` as const}
              disabled={false}
              errorMessage={errors?.NgayNhap?.message}
              disabledDate={(current) => {
                return current && current > dayjs().endOf('day');
              }}
            />
          </div>
        </div>

        <div className='mt-3 flex items-center justify-end gap-2'>
          <Button
            variant='secondary'
            onClick={handleHide}
            loading={isLoadingForm || isLoadingCreatePhieuNhapKho}
            disabled={isLoadingForm || isLoadingCreatePhieuNhapKho}
            type={'button'}
          >
            Quay về
          </Button>

          <Button
            type='button'
            onClick={handleCreateImport}
            loading={isLoadingForm || isLoadingCreatePhieuNhapKho}
            disabled={isLoadingForm || isLoadingCreatePhieuNhapKho}
          >
            Thêm mới
          </Button>
        </div>
      </form>
    </Modal>
  );
};

export default CreateImport;
