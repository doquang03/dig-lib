import { useMutation, useQuery } from '@tanstack/react-query';
import { Checkbox, Modal, ModalProps, Radio, RadioChangeEvent } from 'antd';
import { bookApis, settingApis } from 'apis';
import classNames from 'classnames';
import { Button, Input } from 'components';
import { ChangeEvent, useState } from 'react';
import { toast } from 'react-toastify';

type Props = {
  ids: string[];
  onHide: VoidFunction;
  onSuccess: VoidFunction;
} & ModalProps;

type Receiptform = {
  tuychon: string[];
  tuyChon_NangCao_info: string;
  tuyChon_NangCao_coChu: string;
  Tuychon_InSGK: string;
  LoaiQR: 'ChuHangDoc' | 'MauCu' | 'MauShort';
};

const CreateReceipt = (props: Props) => {
  const { ids, open, onHide, onSuccess } = props;

  const [formValues, setFormValues] = useState<Receiptform>({
    tuychon: [],
    Tuychon_InSGK: 'tc_In4So',
    tuyChon_NangCao_info: 'tc_KhongCham',
    tuyChon_NangCao_coChu: 'tc_CoNho',
    LoaiQR: 'ChuHangDoc'
  } as Receiptform);

  const { data, isLoading: isLoadingTuyChonQR } = useQuery({
    queryKey: ['GeneralSetting'],
    queryFn: () => settingApis.Index(),
    enabled: !!open
  });

  const { mutate, isLoading } = useMutation({
    mutationFn: bookApis.exportQRCode,
    onSuccess: (data) => {
      const blob = new Blob([data.data]);
      const link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob);
      link.download = `Ma_QR_Sach.doc`;
      link.click();

      toast.success('Xuất mã QR thành công');
      onHide();
      onSuccess();
    }
  });

  const handleOnChange = (name: string) => (e: RadioChangeEvent | ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;

    setFormValues((prevState) => {
      if (name === 'tuychon') {
        const index = prevState[name].indexOf(value);
        let array = [];

        if (index !== -1) {
          array = prevState[name].filter((x) => x !== value);
        } else {
          array = [...prevState[name], value];
        }

        return { ...prevState, [name]: array };
      }

      return { ...prevState, [name]: value };
    });
  };

  const handleChoosLoaiQR = (loaiQR: 'ChuHangDoc' | 'MauCu' | 'MauShort') => () => {
    setFormValues((prevState) => {
      return { ...prevState, LoaiQR: loaiQR };
    });
  };

  const handleExportQR = () => {
    const { LoaiQR, tuychon, Tuychon_InSGK } = formValues;
    if (!ids) return;

    mutate({
      ListSachCaBiet: ids,
      loaiQR: LoaiQR,
      tuychon,
      tuychon_InSGK: Tuychon_InSGK,
      tuychon_QR: data?.data.Item.CaiDatMonLoai === 'DDC' ? ['tc_QR_DDC'] : ['tc_QR_Day19'],
      tuyChon_NangCao: [formValues.tuyChon_NangCao_info, formValues.tuyChon_NangCao_coChu]
    });
  };

  return (
    <Modal
      open={open}
      title={<h3 className='border-b-2 pb-2 font-bold uppercase text-primary-10'>Xuất mã QR</h3>}
      closable={false}
      footer={null}
    >
      <div className='grid grid-cols-12'>
        <label className='col-span-3 font-bold'>Tùy chọn</label>
        <div className='col-span-9 flex items-center justify-between gap-1'>
          <Checkbox
            onChange={handleOnChange('tuychon')}
            value={'tc_MaTacGia'}
            disabled={isLoading || isLoadingTuyChonQR}
          >
            Mã tác giả chính
          </Checkbox>

          <Checkbox
            onChange={handleOnChange('tuychon')}
            value={'tc_MaTacGiaPhu'}
            disabled={isLoading || isLoadingTuyChonQR}
          >
            Mã tác giả phụ
          </Checkbox>

          <Checkbox
            onChange={handleOnChange('tuychon')}
            value={'tc_MaTacPham'}
            disabled={isLoading || isLoadingTuyChonQR}
          >
            Mã tác phẩm
          </Checkbox>
        </div>
      </div>

      <div className='mt-5 grid grid-cols-12'>
        <label className='col-span-3 font-bold'>In mã cá biệt theo đầu sách</label>
        <div className='col-span-9 flex items-center gap-6'>
          <div className='flex items-center gap-1'>
            <Radio
              onChange={handleOnChange('Tuychon_InSGK')}
              checked={formValues.Tuychon_InSGK === 'tc_In4So'}
              value={'tc_In4So'}
              disabled={isLoading || isLoadingTuyChonQR}
            >
              In kèm theo mã kiểm soát
            </Radio>
          </div>
          <div className='flex items-center gap-1'>
            <Radio
              onChange={handleOnChange('Tuychon_InSGK')}
              checked={formValues.Tuychon_InSGK === 'tc_InKemMaKS'}
              value={'tc_InKemMaKS'}
              disabled={isLoading || isLoadingTuyChonQR}
            >
              In 4 số: 0001
            </Radio>
          </div>
        </div>
      </div>

      <div className='mt-5 grid grid-cols-12'>
        <label className='col-span-3 font-bold'>Thông tin tác giả - tác phẩm</label>
        <div className='col-span-9 flex items-center gap-6'>
          <div className='flex items-center gap-1'>
            <Radio
              onChange={handleOnChange('tuyChon_NangCao_info')}
              checked={formValues.tuyChon_NangCao_info === 'tc_KhongCham'}
              value={'tc_KhongCham'}
              disabled={isLoading || isLoadingTuyChonQR}
            >
              Không chấm
            </Radio>
          </div>
          <div className='flex items-center gap-1'>
            <Radio
              onChange={handleOnChange('tuyChon_NangCao_info')}
              checked={formValues.tuyChon_NangCao_info === 'tc_CoCham'}
              value={'tc_CoCham'}
              disabled={isLoading || isLoadingTuyChonQR}
            >
              Có chấm
            </Radio>
          </div>
        </div>
      </div>

      <div className='mt-5 grid grid-cols-12'>
        <label className='col-span-3 font-bold'>Cỡ chữ</label>
        <div className='col-span-9 flex items-center gap-12'>
          <div className='flex items-center gap-1'>
            <Radio
              onChange={handleOnChange('tuyChon_NangCao_coChu')}
              checked={formValues.tuyChon_NangCao_coChu === 'tc_CoNho'}
              value={'tc_CoNho'}
              disabled={isLoading || isLoadingTuyChonQR}
            >
              Chữ nhỏ
            </Radio>
          </div>
          <div className='flex items-center gap-1'>
            <Radio
              onChange={handleOnChange('tuyChon_NangCao_coChu')}
              checked={formValues.tuyChon_NangCao_coChu === 'tc_CoTo'}
              value={'tc_CoTo'}
              disabled={isLoading || isLoadingTuyChonQR}
            >
              Chữ to
            </Radio>
          </div>
        </div>
      </div>

      <div className='mt-5 grid grid-cols-12'>
        <div className='col-span-3'>
          <label className='font-bold'>Mẫu 1</label>
        </div>

        <div
          className={classNames('col-span-9 flex items-center gap-12', {
            'rounded-md border border-primary-10 bg-primary-10/50 p-2': formValues.LoaiQR === 'ChuHangDoc'
          })}
        >
          <button
            onClick={handleChoosLoaiQR('ChuHangDoc')}
            className={formValues.LoaiQR === 'ChuHangDoc' ? '-p-2' : 'p-2'}
            disabled={isLoading || isLoadingTuyChonQR}
          >
            <img src='/content/PhieuKho_1.png' alt='Mẫu QR 1' />
          </button>
        </div>
      </div>

      <div className='mt-5 grid grid-cols-12'>
        <label className='col-span-3 font-bold'>Mẫu 2</label>

        <div
          className={classNames('col-span-9 flex items-center gap-12', {
            'rounded-md border border-primary-10 bg-primary-10/50 p-2': formValues.LoaiQR === 'MauCu'
          })}
        >
          <button
            className={formValues.LoaiQR === 'MauCu' ? '-p-2' : 'p-2'}
            onClick={handleChoosLoaiQR('MauCu')}
            disabled={isLoading || isLoadingTuyChonQR}
          >
            <img src='/content/PhieuKho_2.png' alt='Mẫu QR 2' />
          </button>
        </div>
      </div>

      <div className='mt-5 grid grid-cols-12'>
        <label className='col-span-3 font-bold'>Mẫu 3</label>

        <div
          className={classNames('col-span-9 flex items-center gap-12', {
            'rounded-md border border-primary-10 bg-primary-10/50 p-2': formValues.LoaiQR === 'MauShort'
          })}
        >
          <button
            className={formValues.LoaiQR === 'MauShort' ? '-p-2' : 'p-2'}
            onClick={handleChoosLoaiQR('MauShort')}
            disabled={isLoading || isLoadingTuyChonQR}
          >
            <img src='/content/PhieuKho_3.png' alt='Mẫu QR 3' />
          </button>
        </div>
      </div>

      <div className='mt-3 flex items-center justify-end gap-2'>
        <Button variant='secondary' onClick={onHide} loading={isLoading} disabled={isLoading || isLoadingTuyChonQR}>
          Quay về
        </Button>

        <Button onClick={handleExportQR} loading={isLoading} disabled={isLoading || isLoadingTuyChonQR}>
          Xuất mã
        </Button>
      </div>
    </Modal>
  );
};

export default CreateReceipt;
