type GiaoVien = {
  ChucVu: string;
  DiaChi: string;
  GioiTinh: string;
  MaSoThanhVien: string;
  NgaySinh: string;
  SDT: string;
  Ten: string;
  STT: number;
  actions: string;
};
