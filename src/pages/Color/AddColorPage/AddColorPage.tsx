/* eslint-disable array-callback-return */
import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { bookStoreApis, collectionApis } from 'apis';
import colorApis from 'apis/color.apis';
import { AxiosError } from 'axios';
import classNames from 'classnames';
import { Button, Input, ModalDelete, Select, Title, TitleDelete } from 'components';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import { watch } from 'fs';
import { useOutsideClick } from 'hooks';
import { uniqueId } from 'lodash';
import { data } from 'pages/Day19/SearchDay19ByName/contants';
import { RefObject, useCallback, useEffect, useMemo, useState } from 'react';
import { ChromePicker, ColorResult, type Color } from 'react-color';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { colorSchema } from 'utils/rules';
import * as yup from 'yup';

type ColorValue = {
  id: string;
  color: Color;
};

type FormValues = yup.InferType<typeof colorSchema>;

type Props = {
  type?: 'modalview' | 'webview';
  onBack?: VoidFunction;
};

const AddColorPage = (props: Props) => {
  const { onBack, type = 'webview' } = props;

  const [colors, setColors] = useState<ColorValue[]>([]);
  const [colorId, setColorId] = useState<string | undefined>();
  const [visible, setVisible] = useState<boolean>(false);
  const [errorColors, setErrorColors] = useState<boolean>(false);
  const [linkingType, setLinkingType] = useState<number>(2);
  const [linkingIdType, setLinkingIdType] = useState<Selections>([]);
  const [selectedColor, setSelectedColor] = useState<string>();

  const { id } = useParams();

  const { isAllowedAdjustment } = useUser();

  const handleClickOutside = useCallback(() => {
    setVisible(false);
  }, []);

  const ref = useOutsideClick<HTMLDivElement>(handleClickOutside);

  const navigate = useNavigate();

  const queryClient = useQueryClient();

  const {
    register,
    handleSubmit,
    setValue,
    reset,
    setError,
    watch,
    getValues,
    formState: { errors }
  } = useForm<FormValues>({
    defaultValues: {
      TenMau: '',
      IdLienKet: ''
    },
    resolver: yupResolver(colorSchema)
  });

  const IdLienKet = watch('IdLienKet');

  const { data: bookstoreData, isLoading: loadingBookstore } = useQuery({
    queryKey: ['bookstoreOptions', IdLienKet],
    queryFn: () => bookStoreApis.getBookstoreOptions(IdLienKet),
    onSuccess: (data) => {
      setTimeout(() => {
        setValue('IdLienKet', IdLienKet);
      }, 100);
    },
    enabled: !Boolean(id) || (Boolean(id) && IdLienKet !== '')
  });

  const { data: collectionOptionsData, isLoading: loadingCollections } = useQuery({
    queryKey: ['collectionOption'],
    queryFn: collectionApis.GetOptions
  });

  const { refetch, isLoading: loadingColor } = useQuery({
    queryKey: ['color', id],
    queryFn: () => colorApis.getColor(id + ''),
    enabled: Boolean(id),
    onError: () => {
      navigate(path.mamau);
    },
    onSuccess: (data) => {
      setLinkingType(data?.data?.Item?.KieuLienKet);

      setTimeout(() => {
        setValue('IdLienKet', data?.data?.Item?.IdLienKet);
      }, 100);

      setValue('TenMau', data.data.Item.TenMau);

      setColors(
        data?.data?.Item?.ListColor.map((item) => {
          return {
            id: uniqueId(),
            color: item.Name
          };
        })
      );
    }
  });

  const { mutate: createColor, isLoading: loadingCreateColor } = useMutation({
    mutationFn: colorApis.createColor,
    onSuccess: () => {
      setSelectedColor(undefined);
      queryClient.invalidateQueries({ queryKey: ['colors'] });
      toast.success('Tạo mã màu thành công');

      if (onBack) {
        setColors([]);
        reset();
        onBack();
        setErrorColors(false);
        return;
      }

      navigate(path.mamau);
    },
    onError: (error: AxiosError<ResponseApi<{}>>) => {
      colors.length &&
        setError('TenMau', { type: 'validate', message: error.response?.data?.Message }, { shouldFocus: true });
    }
  });

  const { mutate: deleteColor, isLoading: loadingDeleteColor } = useMutation({
    mutationFn: colorApis.deleteColor,
    onSuccess: () => {
      toast.success('Xóa mã màu thành công');
      navigate(path.mamau);
    },
    onError: () => setSelectedColor(undefined)
  });

  const { mutate: updateColor, isLoading: loadingUpdateColor } = useMutation({
    mutationFn: colorApis.updateColor,
    onSuccess: () => {
      refetch();
      toast.success('Cập nhật mã màu sách thành công');
    }
  });

  useEffect(() => {
    if (!visible) {
      return;
    }

    const timer = setTimeout(() => {
      setVisible(false);
    }, 15000);

    return () => {
      clearTimeout(timer);
    };
  }, [visible]);

  useEffect(() => {
    if (!bookstoreData?.data.Item || !collectionOptionsData?.data.Item) {
      return;
    }

    if (linkingType === 1) {
      setLinkingIdType([{ value: '', label: 'Chọn bộ sưu tập' }, ...collectionOptionsData?.data.Item]);
    }

    if (linkingType === 2) {
      setLinkingIdType([{ value: '', label: 'Chọn kho sách' }, ...bookstoreData?.data.Item]);
    }
  }, [bookstoreData?.data.Item, collectionOptionsData?.data.Item, linkingType]);

  useEffect(() => {
    const keyDownHandler = (event: KeyboardEvent) => {
      if (event.key === 'Enter') {
        event.preventDefault();
        setVisible(false);
      }
    };

    document.addEventListener('keydown', keyDownHandler);

    return () => {
      document.removeEventListener('keydown', keyDownHandler);
    };
  }, []);

  const errorMessageKindOfBook = useMemo(() => {
    if (!errors?.IdLienKet?.message) {
      return;
    }

    let message = '';

    switch (linkingType) {
      case 1:
        message = 'Bộ sưu tập không được để trống';
        break;

      case 2:
        message = 'Kho sách không được để trống';
        break;

      default:
        break;
    }

    return message;
  }, [errors?.IdLienKet?.message, linkingType]);

  const isDisable =
    loadingCreateColor || (Boolean(id) && loadingColor) || loadingBookstore || loadingCollections || loadingDeleteColor;

  const handleChangeColor = (value: ColorResult, _id: string) => {
    if (_id === colorId) {
      setColors(
        colors.map(({ id, color }) => {
          if (id === _id) {
            return { color: value.hex, id };
          }

          return { color, id };
        })
      );
    }
  };

  const handleAddColor = () => {
    if (isDisable) {
      return;
    }

    setVisible(false);
    setColors((prevState) => [...prevState, { id: uniqueId(), color: '#ffff' }]);
  };

  const handleColorPicker = (id: string) => {
    setColorId(id);
    setVisible(true);
  };

  const handleRemoveColor = (id: string) => {
    setColors(colors.filter((_color) => _color.id !== id));
  };

  const handleTypeColor = (value: number) => {
    setValue('IdLienKet', '');
    setLinkingType(value);
  };

  const handldeDelete = () => setSelectedColor(getValues('TenMau'));

  const handleBack = () => {
    if (onBack) {
      setColors([]);
      reset();
      return onBack();
    }

    navigate(path.mamau);
  };

  const onSubmit = handleSubmit((data) => {
    if (!colors.length) {
      return setErrorColors(true);
    }

    const listColorValue = colors.map(({ color }) => {
      return {
        Name: color.toString()
      };
    });

    const colorName = data.TenMau.replace(/\s\s+/g, ' ');

    if (!Boolean(id)) {
      !loadingCreateColor &&
        createColor({
          ...data,
          TenMau: colorName,
          ListColor: listColorValue,
          KieuLienKet: +linkingType
        });
    } else {
      !loadingUpdateColor &&
        updateColor({
          ...data,
          TenMau: colorName,
          ListColor: listColorValue,
          KieuLienKet: +linkingType,
          Id: id + ''
        });
    }
  });

  return (
    <div className='p-5'>
      <Title title={Boolean(id) ? 'Cập nhật mã màu' : 'Gán mã màu'} />

      <form className='grid grid-cols-1 md:grid-cols-12' onSubmit={onSubmit}>
        <div className='col-span-6 p-5'>
          <label className='mb-3 font-bold'>
            Tên màu <span className='text-danger-10'>*</span>
          </label>

          <Input
            placeholder='Nhập tên màu'
            name='TenMau'
            register={register}
            errorMessage={errors?.TenMau?.message}
            disabled={isDisable || !isAllowedAdjustment}
          />

          <label className='mt-6 font-bold'>
            Màu <span className='text-danger-10'>*</span>
          </label>

          {errorColors && !colors.length && <p className='text-danger-10'>Màu không được để trống</p>}

          <div className='flex flex-wrap items-center gap-1' ref={ref as RefObject<HTMLDivElement>}>
            {!!colors?.length &&
              colors.map((_color) => {
                const isPicked = _color.id === colorId;

                return (
                  <div className='relative flex items-center' key={_color.id}>
                    <button type='button' onClick={() => handleColorPicker(_color.id)}>
                      <div className='h-8 w-12 shadow-md' style={{ backgroundColor: _color.color.toString() }} />
                    </button>

                    <button
                      type='button'
                      className='rounded-sm border border-danger-10 p-1 text-danger-10 hover:bg-danger-10/40'
                      onClick={() => handleRemoveColor(_color.id)}
                      disabled={!isAllowedAdjustment}
                    >
                      X
                    </button>

                    {isPicked && visible && (
                      <div className='absolute top-[2.5rem] z-50'>
                        <ChromePicker
                          color={_color.color}
                          onChange={(value) => handleChangeColor(value, _color.id)}
                          disableAlpha={true}
                        />
                      </div>
                    )}
                  </div>
                );
              })}

            {colors?.length < 4 && (
              <Button
                variant='default'
                onClick={handleAddColor}
                type='button'
                disabled={isDisable}
                className={isAllowedAdjustment ? '' : 'hidden'}
              >
                Thêm màu
              </Button>
            )}
          </div>
        </div>

        <div className='col-span-6 p-5'>
          <label className='mb-3 font-bold'>
            Màu cho <span className='text-danger-10'>*</span>
          </label>

          <button className='flex' onClick={() => handleTypeColor(2)} type='button' disabled={!isAllowedAdjustment}>
            <Input type='radio' checked={linkingType === 2} disabled={isDisable} />
            <label className='ml-2'>Kho sách</label>
          </button>

          <button className='flex' onClick={() => handleTypeColor(1)} type='button' disabled={!isAllowedAdjustment}>
            <Input type='radio' checked={linkingType === 1} disabled={isDisable} />
            <label className='ml-2'>Bộ sưu tập</label>
          </button>

          <Select
            items={linkingIdType}
            className={classNames('mt-8', {
              'w-full': type === 'modalview',
              'w-1/2': type === 'webview'
            })}
            name='IdLienKet'
            register={register}
            errorMessage={errorMessageKindOfBook}
            disabled={isDisable || !isAllowedAdjustment}
          />

          <div className='mt-10 flex items-center justify-center gap-2'>
            <Button variant='secondary' onClick={handleBack} disabled={isDisable} type='button'>
              Quay về
            </Button>

            {Boolean(id) ? (
              <>
                <Button
                  type='button'
                  variant='danger'
                  onClick={handldeDelete}
                  disabled={isDisable}
                  className={isAllowedAdjustment ? '' : 'hidden'}
                >
                  Xóa
                </Button>
                <Button
                  type='button'
                  variant='default'
                  onClick={onSubmit}
                  disabled={isDisable}
                  className={isAllowedAdjustment ? '' : 'hidden'}
                >
                  Cập nhật
                </Button>
              </>
            ) : (
              <Button
                type='submit'
                variant='default'
                onClick={onSubmit}
                disabled={isDisable}
                className={isAllowedAdjustment ? '' : 'hidden'}
              >
                Thêm mới
              </Button>
            )}
          </div>
        </div>
      </form>

      <ModalDelete
        open={Boolean(selectedColor)}
        closable={false}
        title={<TitleDelete firstText='Bạn chắc chắn muốn xóa' secondText={getValues('TenMau')} />}
        handleCancel={() => setSelectedColor(undefined)}
        handleOk={() => {
          deleteColor(id + '');
        }}
        loading={loadingDeleteColor}
      />
    </div>
  );
};

export default AddColorPage;
