import { useQuery } from '@tanstack/react-query';
import { Table } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { receiptApis } from 'apis';
import { BookIcon } from 'assets';
import classNames from 'classnames';
import { Button, Empty, Loading, SizeChanger, Title } from 'components';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import { usePaginationNavigate, useQueryConfig } from 'hooks';
import CreateReceipt from 'pages/Publications/AddPublication/components/CreateReceipt';
import { useCallback, useMemo, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { getSerialNumber } from 'utils/utils';

const ReceiptDetail = () => {
  const { id } = useParams();
  const [visibleReceiptModal, setVisibleReceiptModal] = useState<boolean>(false);
  const queryConfig = useQueryConfig();
  const [TotalCount, setTotalCount] = useState(0);

  const navigate = useNavigate();
  const handleNavigation = usePaginationNavigate();

  const { isAllowedAdjustment } = useUser();

  const { data, isLoading } = useQuery({
    queryKey: ['receipt', id, queryConfig.page, queryConfig.pageSize],
    queryFn: () => receiptApis.getReceipt(id + '', queryConfig.page as string, queryConfig.pageSize as string),
    enabled: Boolean(id)
  });

  const { List_NhapKho = [], GhiChu } = useMemo(() => {
    if (!data?.data.Item)
      return {} as {
        List_NhapKho: ReceiptById[];
        GhiChu: number;
        TotalCount: number;
        List_SoNhapKho: [];
        List_ChiTietNhapKho: [];
        ListKhoSach: [];
      };
    setTotalCount(data?.data.Item.TotalCount);
    return data?.data.Item;
  }, [data]);

  const columns: ColumnsType<ReceiptById> = [
    {
      title: 'STT',
      dataIndex: 'STT',
      key: 'stt',
      render: (value, record, index) => {
        return (
          queryConfig.page && queryConfig.pageSize && getSerialNumber(+queryConfig.page, +queryConfig.pageSize, index)
        );
      }
    },
    {
      title: 'Mã kiểm soát',
      dataIndex: 'MaKiemSoat',
      key: 'MaKiemSoat'
    },
    {
      title: 'Số đăng ký cá biệt',
      dataIndex: 'MaDKCB',
      key: 'MaDKCB'
    },
    {
      title: 'Tên sách',
      dataIndex: 'TenSach',
      key: 'TenSach'
    },
    {
      title: 'Nguồn cung cấp',
      dataIndex: 'NguonCungCap',
      key: 'NguonCungCap'
    },
    {
      title: 'Số chứng từ',
      dataIndex: 'SoChungTu',
      key: 'SoChungTu'
    },
    {
      title: 'Số VSTQ',
      dataIndex: 'SoVaoSoTongQuat',
      key: 'SoVaoSoTongQuat'
    },
    {
      title: 'Ngày vào sổ',
      dataIndex: 'NgayVaoSo',
      key: 'NgayVaoSo'
    },
    {
      title: 'Giá tiền',
      dataIndex: 'GiaTien',
      key: 'GiaTien',
      render: (value, record) => (
        <>
          {record.GiaTien
            ? new Intl.NumberFormat('vi-VN', {
                style: 'currency',
                currency: 'VND',
                maximumFractionDigits: 9
              }).format(Number(record.GiaTien))
            : ''}
        </>
      )
    }
  ];

  const handlVisileReceiptModal = () => setVisibleReceiptModal(true);

  const handleExportSuccess = () => {
    setVisibleReceiptModal(false);
  };

  const onPaginate = useCallback(
    (page: number, pageSize: number) => {
      handleNavigation({ ...queryConfig, page: page + '', pageSize: pageSize + '' });
    },
    [handleNavigation, queryConfig]
  );

  const pagination = useMemo(() => {
    return {
      current: (!!queryConfig.page && +queryConfig.page) || 1,
      pageSize: (!!queryConfig.pageSize && +queryConfig.pageSize) || 30,
      total: TotalCount,
      onChange: onPaginate,
      hideOnSinglePage: true,
      showSizeChanger: false,
      showQuickJumper: true,
      locale: { jump_to: '', page: '' }
    };
  }, [TotalCount, onPaginate, queryConfig.page, queryConfig.pageSize]);

  const handleBack = () => navigate(path.quanlynhapkho);

  return (
    <div className='p-5'>
      <Title title='Chi tiết phiếu nhập' />

      <div className='mt-3 flex w-1/2 items-center gap-2'>
        <BookIcon /> <span className='font-bold'>Ghi chú: {GhiChu || 'Không có ghi chú'}</span>
      </div>

      <div className='my-4'>
        <Table
          columns={columns}
          dataSource={List_NhapKho || []}
          className='custom-table'
          pagination={pagination}
          rowKey={(record) => record.STT}
          loading={isLoading}
          locale={{
            emptyText: () => <Empty />
          }}
        />
      </div>
      <div
        className={classNames('relative', {
          // @ts-ignore
          'mt-[64px]': (queryConfig.page && +queryConfig.pageSize) >= TotalCount
        })}
      >
        <div className='absolute bottom-3'>
          <SizeChanger visible={!!List_NhapKho?.length} value={queryConfig.pageSize} total={TotalCount.toString()} />
        </div>
      </div>
      <div className='flex items-center justify-end gap-2'>
        <Button onClick={handlVisileReceiptModal} className={!isAllowedAdjustment ? 'hidden' : ''}>
          Xuất mã QR
        </Button>
        <Button variant='secondary' onClick={handleBack}>
          Quay về
        </Button>
      </div>

      <CreateReceipt
        ids={List_NhapKho.map((item) => item.IdSDKCB)}
        open={visibleReceiptModal}
        onHide={() => setVisibleReceiptModal(false)}
        onSuccess={handleExportSuccess}
      />

      <Loading open={isLoading} />
    </div>
  );
};

export default ReceiptDetail;
