import { CloseCircleFilled, DeleteFilled } from '@ant-design/icons';
import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Table, Tooltip } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { bookStoreApis, bookstoreTypeApis, memberApis, rentBack, settingApis } from 'apis';
import { SettingIcon } from 'assets';
import classNames from 'classnames';
import { Button, DatePicker, Empty, Input, Loading, Select, Title } from 'components';
import { AutoComplete } from 'components/AutoComplete';
import { UserConText, useUser } from 'contexts/user.context';
import dayjs, { Dayjs } from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';
import { useBookStatuses, useDebounce } from 'hooks';
import { uniqueId } from 'lodash';
import { useContext, useEffect, useMemo, useState } from 'react';
import { useCookies } from 'react-cookie';
import { useFieldArray, useForm } from 'react-hook-form';
import { useLocation } from 'react-router-dom';
import { toast } from 'react-toastify';
import { rentBackSchema, returnDateNumberSchema, returnExtendSchema, returnSchema } from 'utils/rules';
import * as yup from 'yup';
import ActionModal from '../components/ActionModal';
import MemberCard from '../components/MemberCard';
import Tabs from '../components/Tabs';

dayjs.extend(customParseFormat);

type MuonSachForm = yup.InferType<typeof rentBackSchema> & { khoSach: string; maCB: string; memberName: string };

dayjs.extend(customParseFormat);

const RentBackBookPage = () => {
  const [activeTab, setActiveTab] = useState<number>(0);
  const [tabs, setTabs] = useState<
    {
      tabName: string;
      user: Pick<Student, 'Id' | 'LoaiTK'> | undefined;
    }[]
  >([
    {
      tabName: 'Bạn đọc mới',
      user: undefined
    }
  ]);

  const [khoSachItem, setKhoSachItem] = useState<Selections>([]);
  const [selectedBooks, setSelectedBooks] = useState<HoldingBook[]>([]);
  const [page, setPage] = useState(1);
  const [paginationSize, setPaginationSize] = useState(10);
  const [action, setAction] = useState<{
    book: RentingBook;
    action: 'return' | 'extend';
  }>();
  const [userAdmin, setUserAdmin] = useState<string>('');
  const [selectedBooksByUser, setSelectedBooksByUser] = useState<{
    [key: string]: HoldingBook[];
  }>({});
  const [configReturnDateVisible, setConfigReturnDateVisible] = useState<boolean>(false);

  const { state } = useLocation();
  const idUserReserving = state ? state.idUserReserving : undefined;

  const _user = tabs[activeTab]?.user;

  const _userAdmin = useContext(UserConText);

  useEffect(() => {
    if (!_userAdmin.profile?.Id) return;

    setUserAdmin(_userAdmin.profile?.Id);
  }, [_userAdmin.profile?.Id]);

  const [tabsCookies, setTabsCookies, removeCookie] = useCookies([userAdmin]);
  const [keyCookies, setActiveKey] = useCookies(['activeKey']);

  const { userType, isAllowedAdjustment } = useUser();

  const {
    register,
    control,
    formState: { errors },
    watch,
    handleSubmit,
    reset,
    setFocus,
    getValues,
    setValue
  } = useForm<MuonSachForm>({
    resolver: yupResolver(rentBackSchema)
  });

  const { append, remove } = useFieldArray({
    control,
    name: 'rentBack'
  });

  const { data: memberData, isLoading: loadingMember } = useQuery({
    queryKey: ['member', _user?.Id, _user?.LoaiTK],
    queryFn: () => memberApis.getOneData(_user?.Id + ''),

    enabled: Boolean(_user?.Id),
    onSuccess: (data) => {
      if (data.data.Item.IsDeleted) {
        toast.error(`Dữ liệu ${data.data.Item.Ten}`);

        handleRemoveTab(activeTab);
      }
    },
    onError: () => {
      handleRemoveTab(activeTab);
    }
  });

  const { mutate: getMemberByScanned } = useMutation({
    mutationFn: (id: string) => {
      return memberApis.getOneData(id);
    },
    onSuccess: (data) => {
      useDebounceMember.setIsExcuted(false);
      const member = data.data.Item;
      setValue('memberName', '');

      if (member.TrangThai === 1) return;

      let _tabs = [...tabs];

      for (let index = 0; index < _tabs.length; index++) {
        if (index === activeTab) {
          const temp = member.LoaiTK === 'gv' ? member.ChucVu : member.LopHoc;
          _tabs[index] = {
            tabName: member.Ten + ' - ' + temp,
            user: {
              Id: member.Id,
              LoaiTK: member.LoaiTK
            }
          };
        }
      }

      removeCookie(userAdmin);

      setTabs(_tabs);
      setTabsCookies(userAdmin, _tabs);
      setValue('khoSach', '');
      setValue('maCB', '');
      setSelectedBooksByUser((prev) => {
        return { ...prev, [member.Id]: [] };
      });
    },
    onError: () => {
      setValue('memberName', '');
    }
  });

  const { mutate: getMemberById, isLoading: loadingGetMemberById } = useMutation({
    mutationFn: memberApis.getOneData,
    onSuccess: (data) => {
      const _member = data.data.Item;

      if (_member.TrangThai === 1) return;

      if (!_userAdmin?.profile?.Id) return;

      const _tabs = tabsCookies[_userAdmin.profile?.Id] || tabs;

      const isExisted = _tabs.length
        ? _tabs.find((tab: { user?: { Id?: string } }) => tab?.user?.Id === _member.Id)
        : false;

      const index = _tabs.findIndex((item: { user?: { Id?: string } }) => item.user?.Id === _member.Id);
      setActiveKey('activeKey', index);
      setActiveTab(index);

      if (_tabs.length === 5 || isExisted) return;

      !_tabs[0].hasOwnProperty('user') && _tabs.splice(0, 1);

      const temp = _member.LoaiTK === 'gv' ? _member.ChucVu : _member.LopHoc;
      _tabs.push({
        tabName: _member.Ten + ' - ' + temp,
        user: {
          Id: _member.Id,
          LoaiTK: _member.LoaiTK
        }
      });

      setTabs(_tabs);
      setActiveTab(_tabs.length - 1);
      setActiveKey('activeKey', _tabs.length - 1);
      setTabsCookies(_userAdmin?.profile?.Id, _tabs);
      setValue('khoSach', '');
      setValue('maCB', '');
      setSelectedBooksByUser((prev) => {
        return { ...prev, [_member.Id]: [] };
      });
    },
    onError: () => {
      setValue('memberName', '');
    }
  });

  const member = useMemo(() => {
    if (!memberData?.data.Item) return;

    return { ...memberData.data.Item, LoaiTK: _user?.LoaiTK };
  }, [_user?.LoaiTK, memberData?.data.Item]);

  const {
    control: actionsControl,
    formState: { errors: actionsErrors },
    handleSubmit: actionHandleSubmit,
    reset: actionReset,
    setValue: actionSetValue
  } = useForm<{ returnExtendDate: Dayjs | null }>({
    resolver: yupResolver(returnExtendSchema)
  });

  const {
    register: returnRegister,
    formState: { errors: returnErrors },
    handleSubmit: returnHandleSubmit,

    reset: returnReset,
    setValue: setValueReturn
  } = useForm<{ status?: string }>({
    resolver: yupResolver(returnSchema)
  });

  const {
    register: returnDateNumberRegister,
    formState: { errors: returnDateNumberErrors },
    handleSubmit: returnDateNumberHandleSubmit,
    setValue: setValueDateNumber
  } = useForm<{ returnDateNumber?: string }>({
    resolver: yupResolver(returnDateNumberSchema)
  });

  const memberSearchValue = watch('memberName');
  const useDebounceMember = useDebounce(memberSearchValue);
  const { data: membersData, isFetching: loadingMemberData } = useQuery({
    queryKey: ['memberData', memberSearchValue, !Boolean(_user)],
    queryFn: () => {
      useDebounceMember.setIsExcuted(false);
      return memberApis.getDataBySearch(memberSearchValue);
    },
    enabled: Boolean(memberSearchValue) && !Boolean(_user) && useDebounceMember.isExcuted
  });

  // user's book renting
  const {
    data: rentingBookData,
    isLoading: loadingRentingBook,
    refetch: refetchRentingBook,
    isFetching
  } = useQuery({
    queryKey: ['rentingBooks', _user?.Id, activeTab],
    queryFn: () => rentBack.getRentingBook(_user?.Id + ''),
    enabled: Boolean(_user),
    onSuccess: (data) => {
      setAction(undefined);

      if (paginationSize * (page - 1) >= data?.data?.Item?.length) {
        setPage(page - 1);
      }
    }
  });

  // holding books in queue
  const {
    isLoading: loadingHoldingBooksByUser,
    refetch: refetchHoldingBooksByUser,
    remove: removeHoldingBooksQuery
  } = useQuery({
    queryKey: ['getHoldingBooksByMember', _user?.Id],
    queryFn: () => rentBack.getBooksByMember(_user?.Id as string),
    enabled: Boolean(_user?.Id),
    staleTime: Infinity,
    onSuccess: (data) => {
      remove();
      const finalData: HoldingBook[] = data?.data.Item?.ListSachDangDuocChon?.map((book) => {
        return {
          IdSachCaBiet: book.IdSachCaBiet,
          LinkBia: book.LinkBia,
          TenSach: book.TenSach,
          NgayMuon: book.NgayMuon,
          NgayTra: book.NgayTra,
          IdTrangThai: book.IdTrangThai,
          MaKSCB: book.MaKSCB,
          IsDangMuon: book.IsDangMuon
        };
      });

      append(finalData);

      setSelectedBooksByUser((prev) => {
        return {
          ...prev,
          [_user?.Id as string]: finalData
        };
      });

      setSelectedBooks(finalData || []);
    }
  });

  useEffect(() => {
    if (!selectedBooksByUser[_user?.Id as string]) return;

    setSelectedBooks(selectedBooksByUser[_user?.Id as string]);
  }, [_user?.Id, selectedBooksByUser, activeTab]);

  const { mutateAsync: updateBooksInQueue } = useMutation({
    mutationFn: (payload: { books: HoldingBook[]; userId: string }) => {
      return rentBack.addBookToQueue(payload.books, payload.userId);
    }
  });

  const IdKhoSach = watch('khoSach');
  const maCBValue = watch('maCB');

  const { isExcuted, setIsExcuted } = useDebounce(maCBValue);
  const {
    data: dataListSachCaBiet,
    isFetching: isFetchingSachCaBiet,
    remove: removeListSachCaBietQuery
  } = useQuery({
    queryKey: ['SachCaBiet', maCBValue],
    queryFn: () => {
      setIsExcuted(false);
      return bookStoreApis.GetListSCB(
        {
          idKho: IdKhoSach || '',
          TextForSearch: maCBValue
        },
        true
      );
    },
    enabled: !!maCBValue && isExcuted,
    cacheTime: 3000
  });

  const bookCBResult = useMemo(() => {
    if (!dataListSachCaBiet?.data?.Item.List_SachCB?.length && !dataListSachCaBiet?.data?.Item.List_Sach?.length)
      return [] as Array<SachCaBiet>;

    if (!maCBValue) return [];

    return dataListSachCaBiet?.data.Item.List_SachCB;
  }, [dataListSachCaBiet?.data.Item.List_Sach, dataListSachCaBiet?.data.Item.List_SachCB, maCBValue]);

  useQuery({
    queryKey: ['stores'],
    queryFn: bookstoreTypeApis.getAllStores,
    onSuccess: (res) => {
      const storeResults = res.data.Item.map(({ Ten, Id }) => {
        return {
          label: Ten,
          value: Id
        };
      });
      setKhoSachItem([{ label: 'Chọn kho sách', value: '' }, ...storeResults]);
    }
  });

  const {
    data: numberOfRentingDays,
    isLoading: loadingNumberOfRentingDays,
    refetch: fetchLaiNgay
  } = useQuery({
    queryKey: ['numberOfRentingDays', _user?.LoaiTK],
    queryFn: settingApis.getNumberOfRentingDays,
    onSuccess: (data) => {
      setValueDateNumber(
        'returnDateNumber',
        _user?.LoaiTK === 'gv' ? data.data.Item.NgayMuonSachGiaoVien + '' : data.data.Item.NgayMuonSach + ''
      );
    },
    enabled: Boolean(_user)
  });

  const { mutateAsync: setNumberOfRentingDays, isLoading: loadingSetNumberOfRentingDays } = useMutation({
    mutationFn: settingApis.setNumberOfRentingDays,
    onSuccess: () => {
      toast.success('Cập nhật số ngày mượn thành công');
      fetchLaiNgay();
    }
  });

  const { data: bookStatusData } = useBookStatuses();

  const { mutate: rentBook, isLoading: loadingRentBook } = useMutation({
    mutationFn: rentBack.updateListBook,
    onSuccess: async (data) => {
      toast.success('Cho mượn sách thành công');
      removeListSachCaBietQuery();
      setSelectedBooks([]);
      reset();
      refetchRentingBook();
      setValue('maCB', '');

      setSelectedBooksByUser((prev) => {
        return { ...prev, [_user?.Id as string]: [] };
      });

      if (data.data.Item.ListFail.length) {
        let array = [];

        const listOfFail = data.data.Item.ListFail;
        for (let index = 0; index < selectedBooks.length; index++) {
          for (let j = 0; j < listOfFail.length; j++) {
            if (listOfFail[j] === selectedBooks[index].IdSachCaBiet) {
              selectedBooks[index].IsDangMuon = true;
              array.push(selectedBooks[index]);
            }
          }
        }

        await updateBooksInQueue({
          books: array,
          userId: _user?.Id as string
        });

        refetchHoldingBooksByUser();
        setPage(1);

        toast.warning('Không thể cho mượn những sách đang được thành viên khác mượn!');
      } else {
        await updateBooksInQueue({
          books: [],
          userId: _user?.Id as string
        });

        refetchHoldingBooksByUser();
        setPage(1);
      }
    }
  });

  const { mutate: returnBook, isLoading: loadingReturnBook } = useMutation({
    mutationFn: rentBack.returnBook,
    onSuccess: () => {
      toast.success('Trả sách thành công');

      setAction(undefined);
      setValueReturn('status', '');
      refetchRentingBook();
      returnReset();
    }
  });

  const { mutate: extendRenting, isLoading: loadingExtendRenting } = useMutation({
    mutationFn: rentBack.extendRengting,
    onSuccess: () => {
      toast.success('Gia hạn sách thành công');
      setAction(undefined);
      actionReset();
      refetchRentingBook();
      actionSetValue('returnExtendDate', null);
    }
  });

  const { mutate: EditBySCB, isLoading: editBySCB } = useMutation({
    mutationKey: ['dataBarCode'],
    mutationFn: bookStoreApis.EditBySCB,
    onSuccess: (data) => {
      const Item = data.data?.Item;
      const index = selectedBooks?.findIndex((_book) => _book.IdSachCaBiet === Item.Id);

      setValue('maCB', '');

      if (!Item.hasOwnProperty('Id')) return;

      if (index === -1) {
        let books = [...selectedBooks];

        let newBook: HoldingBook = {
          IdSachCaBiet: Item?.Id || '',
          TenSach: Item?.TenSach || '',
          MaKSCB: Item?.MaKSCB || '',
          LinkBia: Item?.LinkBiaSach || '',
          // @ts-ignore
          NgayMuon: dayjs(),
          NgayTra: '',
          IdTrangThai: Item?.IdTrangThai || '',
          IsDangMuon: false
        };

        books.unshift(newBook);
        setSelectedBooks(books);
      } else {
        toast.warning(`Sách ${Item.TenSach} đã được thêm vào hàng đợi`);
      }
    },
    onError: () => {
      setValue('maCB', '');
    }
  });

  const results = useMemo(() => {
    if (!membersData?.data.Item) return [];

    if (!memberSearchValue) {
      return [];
    }

    return membersData.data.Item;
  }, [membersData?.data.Item, memberSearchValue]);

  const columns: ColumnsType<RentingBook> = useMemo(() => {
    return [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        render: (value: any, record: any, j: number) => (page - 1) * paginationSize + j + 1,
        width: 100
      },
      {
        title: 'Mã cá biệt',
        dataIndex: 'MaKSCB',
        key: 'MaKSCB'
      },
      {
        title: 'Tên sách',
        dataIndex: 'TenSach',
        key: 'TenSach',
        render: (value, { TenSach }) => (
          <Tooltip placement='topLeft' title={TenSach} arrow={true} className='truncate'>
            <p>{TenSach}</p>
          </Tooltip>
        ),
        onCell: (record) => ({
          className: 'text-left'
        }),
        ellipsis: true
      },
      {
        title: 'Bìa sách',
        dataIndex: 'LinkBiaSach',
        key: 'LinkBiaSach',
        render: (value, record) => (
          <center>
            <img src={record.LinkBiaSach || '/content/book.png'} alt={record.TenSach} width={70} height={40} />
          </center>
        )
      },
      {
        title: 'Ngày mượn',
        dataIndex: 'NgayMuon',
        key: 'NgayMuon'
      },
      {
        title: 'Ngày trả',
        dataIndex: 'NgayTra',
        key: 'NgayTra'
      },
      {
        title: 'Tình trạng sách',
        dataIndex: 'IdTinhTrangSach',
        key: 'IdTinhTrangSach',
        render: (value, record) => {
          return bookStatusData.find(({ value }) => value === record.IdTinhTrangSach)?.label || '--';
        }
      },
      {
        title: 'Hành động',
        dataIndex: 'actions',
        key: 'actions',
        render: (value, record) => (
          <div className='flex items-center justify-center gap-2'>
            <Button
              className='bg-tertiary-50 hover:bg-tertiary-50/75'
              onClick={() => {
                setValueReturn('status', record.IdTinhTrangSach);
                setAction({ book: record, action: 'return' });
              }}
            >
              <span className='text-tertiary-50/100'>Trả sách</span>
            </Button>

            <Button
              className='bg-tertiary-40 hover:bg-tertiary-40/75'
              onClick={() => {
                actionSetValue('returnExtendDate', dayjs(record.NgayTra, 'DD/MM/YYYY').add(1, 'day'));
                setAction({ book: record, action: 'extend' });
              }}
            >
              <span className='text-tertiary-40/100'>Gia hạn</span>
            </Button>
          </div>
        )
      }
    ];
  }, [actionSetValue, bookStatusData, page, paginationSize, activeTab]);

  useEffect(() => {
    if (!tabsCookies || !userAdmin) return;

    for (const key in tabsCookies) {
      if (key === userAdmin) {
        setTabs(tabsCookies[key]);
        break;
      }
    }
  }, [tabsCookies, userAdmin]);

  useEffect(() => {
    if (!keyCookies.activeKey) return;

    setActiveTab(Number(keyCookies.activeKey));
  }, []);
  const location = useLocation();

  useEffect(() => {
    const handleTabClose = (event: any) => {
      event.preventDefault();

      if (Boolean(_user?.Id) && !selectedBooksByUser[_user?.Id as string]) return;

      for (const key in selectedBooksByUser) {
        updateBooksInQueue({ books: selectedBooksByUser[key], userId: key });
      }
    };

    window.addEventListener('beforeunload', handleTabClose);

    return () => {
      window.removeEventListener('beforeunload', handleTabClose);
    };
  }, [selectedBooksByUser, updateBooksInQueue, location.pathname, _user?.Id]);

  useEffect(() => {
    if (!_user?.Id) return;

    setFocus('maCB');
  }, [_user?.Id, setFocus, activeTab]);

  useEffect(() => {
    if (!maCBValue) return;

    const handleScanQR = (e: any) => {
      if (e.key === 'Enter') {
        if (maCBValue?.split('-').length === 4) EditBySCB(maCBValue);
      }

      if (e.key === 'Shift') return;
    };

    document.addEventListener('keydown', handleScanQR);

    return () => {
      document.removeEventListener('keydown', handleScanQR);
    };
  }, [maCBValue]);

  const tabMenu = memberSearchValue && memberSearchValue.split('-').length >= 4 ? memberSearchValue.split('-')[1] : '';
  useEffect(() => {
    if (!tabMenu || !useDebounceMember.isExcuted) return;
    getMemberByScanned(tabMenu);
  }, [tabMenu, useDebounceMember.isExcuted]);

  useEffect(() => {
    if (!idUserReserving || tabs.length === 5) return;

    !loadingGetMemberById && getMemberById(idUserReserving);

    return () => {
      removeHoldingBooksQuery();
      window.history.replaceState({}, document.title, window.location.pathname);
    };
  }, [idUserReserving]);

  const handleSelectBook = (book: SachCaBiet & { NgayTra?: string }) => () => {
    if (
      (numberOfRentingDays?.data?.Item?.SLSachDuocMuon || 0) -
        (rentingBookData?.data?.Item?.length || 0) -
        selectedBooks.length <=
      0
    ) {
      return toast.warning(`Không thể mượn thêm sách`);
    }

    if (Boolean(book.IdPhieuMuon)) {
      return toast.warning(`Sách ${book.MaKSCB} - ${book.TenSach} hiện tại đang được mượn`);
    }

    const index = selectedBooks?.findIndex((_book) => _book.IdSachCaBiet === book.Id);

    if (index === -1) {
      let books = [...selectedBooks];

      let newBook: HoldingBook = {
        IdSachCaBiet: book.Id || '',
        TenSach: book.TenSach || '',
        MaKSCB: book.MaKSCB || '',
        LinkBia: book.LinkBiaSach || '',
        // @ts-ignore
        NgayMuon: dayjs(),
        NgayTra: '',
        IdTrangThai: book.IdTrangThai || '',
        IsDangMuon: false
      };

      books.push(newBook);

      setSelectedBooks(books);

      setSelectedBooksByUser((prev) => {
        return { ...prev, [_user?.Id as string]: books };
      });
    } else {
      toast.warning(`Sách ${book.MaKSCB} - ${book.TenSach} đã được thêm vào hàng đợi.`);
    }
  };

  useEffect(() => {
    if (Boolean(member)) {
      setFocus('maCB');
    }
  }, [activeTab, member, setFocus]);

  useEffect(() => {
    const handleAddBookIntoQueue = (e: KeyboardEvent) => {
      if (e.key === 'Enter') {
        if (bookCBResult.length === 1) {
          if (Boolean(bookCBResult[0].IdPhieuMuon)) {
            return toast.warning(`Sách ${bookCBResult[0].MaKSCB} - ${bookCBResult[0].TenSach} hiện tại đang được mượn`);
          }

          const index = selectedBooks?.findIndex((_book) => _book.IdSachCaBiet === bookCBResult[0].Id);

          if (index === -1) {
            let books = [...selectedBooks];

            let newBook: HoldingBook = {
              IdSachCaBiet: bookCBResult[0].Id || '',
              TenSach: bookCBResult[0].TenSach || '',
              MaKSCB: bookCBResult[0].MaKSCB || '',
              LinkBia: bookCBResult[0].LinkBiaSach || '',
              // @ts-ignore
              NgayMuon: dayjs(),
              NgayTra: '',
              IdTrangThai: bookCBResult[0].IdTrangThai || '',
              IsDangMuon: false
            };

            books.unshift(newBook);

            setSelectedBooks(books);

            setSelectedBooksByUser((prev) => {
              return { ...prev, [_user?.Id as string]: books };
            });

            setValue('maCB', '');
          } else {
            toast.warning(`Sách ${bookCBResult[0].MaKSCB} - ${bookCBResult[0].TenSach} đã được thêm vào hàng đợi.`);
          }
        }
      }
    };

    window.addEventListener('keydown', handleAddBookIntoQueue);

    return () => {
      window.removeEventListener('keydown', handleAddBookIntoQueue);
    };
  }, [bookCBResult, bookCBResult.length]);

  if (userType === 2001 && !isAllowedAdjustment) {
    return <h2 className='p-2 '>Bạn không có quyền truy cập chức năng này</h2>;
  }

  const handleAddNewTab = () => {
    let _tabs = [...tabs];
    _tabs.push({ tabName: 'Bạn đọc mới', user: undefined });

    setTabs(_tabs);
    setActiveTab(_tabs.length - 1);
    setActiveKey('activeKey', _tabs.length - 1);
    setTabsCookies(userAdmin, _tabs);
    setFocus('memberName');
  };

  const handleActiveTab = (index: number) => {
    setActiveTab(index);
    setActiveKey('activeKey', index);

    if (Boolean(tabs[index].user)) {
      setValue('maCB', '');
      setFocus('maCB');
      setValue('khoSach', '');
      refetchRentingBook();
    }

    setPage(1);
    setPaginationSize(10);

    setFocus('memberName');
  };

  const handleRemoveTab = (index: number) => {
    const _tabs = [...tabs];
    let activeKey = activeTab;

    if (index === activeKey) {
      if (activeKey > _tabs.length - 2) {
        activeKey = _tabs.length - 2;
      }
    } else {
      if (activeKey > index) {
        activeKey--;
      }
    }

    if (index !== 0 || _tabs.length > 1) {
      _tabs.splice(index, 1);
      !_user?.Id && setFocus('memberName');

      setTabs(_tabs);
      setTabsCookies(userAdmin, _tabs);
      _user?.Id && updateBooksInQueue({ books: [], userId: _user?.Id });
      removeHoldingBooksQuery();
      setActiveTab(activeKey);
      setActiveKey('activeKey', activeKey);
    } else if (_user?.Id) {
      _tabs.splice(index, 1);
      !_user?.Id && setFocus('memberName');

      setTabs([
        {
          tabName: 'Bạn đọc mới',
          user: undefined
        }
      ]);

      setTabsCookies(userAdmin, [
        {
          tabName: 'Bạn đọc mới',
          user: undefined
        }
      ]);

      removeHoldingBooksQuery();
      setActiveTab(0);
      setActiveKey('activeKey', 0);
    }
  };

  const handleChooseResult =
    (user: { Id: string; MaThanhVien: string; Ten: string; LoaiTK: string; ChucVu: string; LopHoc: string }) => () => {
      let _tabs = [...tabs];

      for (let index = 0; index < _tabs.length; index++) {
        if (index === activeTab) {
          const temp = user.LoaiTK === 'gv' ? user.ChucVu : user.LopHoc;
          _tabs[index] = {
            tabName: user.Ten + ' - ' + temp,
            user: {
              Id: user.Id,
              LoaiTK: user.LoaiTK
            }
          };
        }
      }

      removeCookie(userAdmin);

      setTabs(_tabs);
      setTabsCookies(userAdmin, _tabs);
      setValue('khoSach', '');
      setValue('maCB', '');
      setValue('memberName', '');
      setSelectedBooksByUser((prev) => {
        return { ...prev, [user.Id]: [] };
      });
    };

  const handleRemoveSelectedBook = (index: number) => () => {
    if (!_user) return;

    const _selectedBooks = [...selectedBooks];
    _selectedBooks.splice(index, 1);
    setSelectedBooks(_selectedBooks);

    setSelectedBooksByUser((prev) => {
      return { ...prev, [_user?.Id as string]: _selectedBooks };
    });

    !_selectedBooks.length && updateBooksInQueue({ books: _selectedBooks, userId: _user?.Id });

    remove(index);
  };

  const handleRentBook = handleSubmit((data) => {
    !!selectedBooks.length &&
      rentBook({
        idUser: _user?.Id + '',
        listSachCB: selectedBooks.map(({ IdSachCaBiet }, index) => {
          return {
            IdSCB: IdSachCaBiet + '',
            NgayTra: dayjs(data?.rentBack?.[index].NgayTra).format('DD/MM/YYYY'),
            NgayMuon: dayjs(data?.rentBack?.[index].NgayMuon).format('DD/MM/YYYY')
          };
        })
      });
  });

  const handleExtend = actionHandleSubmit((data) => {
    data.returnExtendDate &&
      extendRenting([{ Id: action?.book?.Id + '', NgayTraNew: dayjs(data.returnExtendDate).format('DD/MM/YYYY') }]);
  });

  const handleReturn = returnHandleSubmit((data) => {
    data.status && returnBook([{ Id: action?.book?.Id + '', IdTrangThai: data.status, IdUser: _user?.Id + '' }]);
  });

  const handleBack = () => {
    setAction(undefined);
    actionReset();
    setValueReturn('status', '');
  };

  const handleConfigReturnDate = returnDateNumberHandleSubmit((data) => {
    if (!data) return;

    if (!numberOfRentingDays?.data.Item) return;

    let studentRentingDays = numberOfRentingDays?.data.Item.NgayMuonSach;
    let teacherRentingDays = numberOfRentingDays?.data.Item.NgayMuonSachGiaoVien;

    if (_user?.LoaiTK === 'gv') {
      teacherRentingDays = Number(data.returnDateNumber);
    } else {
      studentRentingDays = Number(data.returnDateNumber);
    }

    setNumberOfRentingDays({
      NgayMuonSach: studentRentingDays,
      NgayMuonSachGiaoVien: teacherRentingDays,
      NgayMuonSachGiaoVienMax: numberOfRentingDays?.data.Item.NgayMuonSachGiaoVienMax,
      NgayMuonSachMax: numberOfRentingDays?.data?.Item?.NgayMuonSachMax
    }).then(() => {
      if (!selectedBooks.length) return;

      selectedBooks.forEach((book) => {
        // @ts-ignore
        if (book.NgayMuon >= dayjs().add(Number(data.returnDateNumber), 'day')) {
          // @ts-ignore
          book.NgayMuon = dayjs();
        }
        // @ts-ignore
        book.NgayTra = dayjs().add(Number(data.returnDateNumber), 'day');
      });

      setSelectedBooks(selectedBooks);
      setSelectedBooksByUser((prev) => {
        return { ...prev, [_user?.Id as string]: selectedBooks };
      });

      toogleConfigReturnDateNumber();
    });
  });

  const toogleConfigReturnDateNumber = () => {
    setConfigReturnDateVisible((prev) => !prev);
  };

  return (
    <div className='p-5'>
      <Title title='Mượn trả sách' />

      <div className='flex items-center justify-between'>
        <Tabs
          tabs={tabs}
          activeTab={activeTab}
          onChange={handleActiveTab}
          onAddNewTab={handleAddNewTab}
          onRemove={handleRemoveTab}
        />

        {Boolean(_user) && (
          <Tooltip title={`Cấu hình số ngày mượn trả sách`} placement='left'>
            <button onClick={toogleConfigReturnDateNumber}>
              <SettingIcon fill='gray' width={25} height={25} />
            </button>
          </Tooltip>
        )}
      </div>

      {!Boolean(_user) && (
        <div className='relative mt-3'>
          <Input
            containerClassName='relative'
            placeholder='Nhập tên, mã học sinh hoặc giáo viên'
            autoFocus
            name='memberName'
            register={register}
            right={
              <svg
                xmlns='http://www.w3.org/2000/svg'
                fill='none'
                viewBox='0 0 24 24'
                strokeWidth={1.5}
                stroke='currentColor'
                className='absolute right-2 top-1/4 h-6 w-6'
              >
                <path
                  strokeLinecap='round'
                  strokeLinejoin='round'
                  d='M3.75 4.875c0-.621.504-1.125 1.125-1.125h4.5c.621 0 1.125.504 1.125 1.125v4.5c0 .621-.504 1.125-1.125 1.125h-4.5A1.125 1.125 0 013.75 9.375v-4.5zM3.75 14.625c0-.621.504-1.125 1.125-1.125h4.5c.621 0 1.125.504 1.125 1.125v4.5c0 .621-.504 1.125-1.125 1.125h-4.5a1.125 1.125 0 01-1.125-1.125v-4.5zM13.5 4.875c0-.621.504-1.125 1.125-1.125h4.5c.621 0 1.125.504 1.125 1.125v4.5c0 .621-.504 1.125-1.125 1.125h-4.5A1.125 1.125 0 0113.5 9.375v-4.5z'
                />
                <path
                  strokeLinecap='round'
                  strokeLinejoin='round'
                  d='M6.75 6.75h.75v.75h-.75v-.75zM6.75 16.5h.75v.75h-.75v-.75zM16.5 6.75h.75v.75h-.75v-.75zM13.5 13.5h.75v.75h-.75v-.75zM13.5 19.5h.75v.75h-.75v-.75zM19.5 13.5h.75v.75h-.75v-.75zM19.5 19.5h.75v.75h-.75v-.75zM16.5 16.5h.75v.75h-.75v-.75z'
                />
              </svg>
            }
          />

          {Boolean(memberSearchValue) && (
            <div className='absolute top-10 mt-3 max-h-48 w-full overflow-y-auto bg-white shadow-md'>
              {Boolean(results.length) && !!results.length ? (
                results.map((member) => (
                  <button
                    key={uniqueId() || member?.MaThanhVien}
                    className='block w-full border-b-2 p-2 text-left'
                    onClick={handleChooseResult(member)}
                  >
                    {member?.Ten || ''} - {member?.MaThanhVien || ''}
                  </button>
                ))
              ) : loadingMemberData ? (
                <p className='my-2 text-center'>Đang tìm kiếm thành viên</p>
              ) : (
                <Empty label={'Không tìm thấy thành viên với từ khóa ' + memberSearchValue + '.'} />
              )}
            </div>
          )}
        </div>
      )}

      {Boolean(_user) && Boolean(member) && (
        <div className='mt-3'>
          <div className='grid grid-cols-12 gap-3'>
            <div className='col-span-12 flex justify-center sm:col-span-2'>
              {/* @ts-ignore */}
              <MemberCard member={member} onCloseTab={() => handleRemoveTab(activeTab)} />
            </div>

            <div className='col-span-12 sm:col-span-10'>
              <div className='flex items-center gap-3'>
                <AutoComplete
                  className='w-full flex-grow'
                  name={'khoSach' as const}
                  placeholder='Nhập kho'
                  control={control}
                  items={khoSachItem}
                  showSearch
                  errorMessage={errors.khoSach?.message}
                />

                <Input
                  placeholder='Nhập tên sách, mã sách cá biệt'
                  containerClassName='w-full relative'
                  register={register}
                  name='maCB'
                  errorMessage={errors.maCB?.message}
                  disabled={!!memberData?.data?.Item?.IdThanhVien}
                  right={
                    <button
                      className={classNames(
                        'absolute right-2 top-1/3 transition-all delay-100 duration-200 ease-linear',
                        {
                          'opacity-0': !maCBValue,
                          'opacity-90': Boolean(maCBValue)
                        }
                      )}
                      onClick={() => setValue('maCB', '')}
                    >
                      <CloseCircleFilled style={{ color: '#C8C8C8' }} />
                    </button>
                  }
                />
              </div>

              {!!maCBValue && (
                <>
                  {bookCBResult?.length ? (
                    <div className='mt-1 max-h-52 overflow-y-auto rounded-lg bg-white p-2 shadow-md'>
                      {bookCBResult.map((book) => (
                        <div
                          className='my-2 border-b-2 hover:cursor-pointer'
                          onClick={handleSelectBook(book)}
                          key={book.Id}
                        >
                          {book.MaKSCB} - {book.TenSach}
                        </div>
                      ))}
                    </div>
                  ) : (
                    <div className='mt-1 max-h-52 overflow-y-auto rounded-lg bg-white p-2 text-center shadow-md'>
                      {isFetchingSachCaBiet ? 'Đang tìm sách' : 'Không tìm thấy sách'}
                    </div>
                  )}
                </>
              )}

              {!!selectedBooks.length && (
                <div className='mt-3 max-h-[300px] overflow-x-auto bg-white shadow-md'>
                  <table className='table'>
                    <thead className='sticky top-0 z-10 bg-primary-10 text-white'>
                      <tr>
                        <th className='text-center'>STT</th>
                        <th className='text-center'>Mã cá biệt</th>
                        <th className='text-center'>Tên sách</th>
                        <th className='text-center'>Bìa sách</th>
                        <th className='text-center'>Ngày mượn</th>
                        <th className='text-center'>Ngày trả</th>
                        <th className='text-center'>Tình trạng sách</th>
                        <th className='text-center'>Hành động</th>
                      </tr>
                    </thead>
                    <tbody className='bg-white'>
                      {selectedBooks.map((item, index) => {
                        const {
                          IdSachCaBiet,
                          TenSach = '--',
                          MaKSCB = '--',
                          IdTrangThai,
                          LinkBia,
                          NgayMuon = dayjs(),
                          NgayTra = ''
                        } = item;
                        // @ts-ignore
                        setValue(`rentBack.${index}.NgayMuon`, NgayMuon);
                        if (_user && numberOfRentingDays?.data?.Item) {
                          // @ts-ignore
                          setValue(
                            `rentBack.${index}.NgayTra`,
                            // @ts-ignore
                            NgayTra
                              ? dayjs(NgayTra)
                              : dayjs().add(
                                  _user?.LoaiTK === 'gv'
                                    ? numberOfRentingDays?.data.Item.NgayMuonSachGiaoVien
                                    : numberOfRentingDays?.data.Item.NgayMuonSach,
                                  'day'
                                )
                          );
                        }

                        return (
                          <tr className='text-center' key={IdSachCaBiet}>
                            <td>{index + 1}</td>

                            <td>{MaKSCB}</td>

                            <td>{TenSach || '--'}</td>
                            <td>
                              <center>
                                <img src={LinkBia || '/content/book.png'} alt={TenSach} width={70} height={70} />
                              </center>
                            </td>
                            <td>
                              <DatePicker
                                control={control}
                                name={`rentBack.${index}.NgayMuon`}
                                onChangeOut={(value: any) => {
                                  item.NgayMuon = value ? (dayjs(value) as unknown as string) : '';

                                  setSelectedBooksByUser((prev) => {
                                    return { ...prev, [_user?.Id as string]: selectedBooks };
                                  });
                                }}
                                errorMessage={errors?.rentBack?.[index]?.NgayMuon?.message}
                                disabledDate={(current) => {
                                  return (
                                    // @ts-ignore
                                    current && current > getValues(`rentBack.${index}.NgayTra`)
                                  );
                                }}
                                placeholder='Chọn ngày mượn'
                                clearIcon={false}
                              />
                            </td>
                            <td>
                              <DatePicker
                                control={control}
                                name={`rentBack.${index}.NgayTra`}
                                onChangeOut={(value: any) => {
                                  item.NgayTra = value ? (dayjs(value) as unknown as string) : '';

                                  setSelectedBooksByUser((prev) => {
                                    return { ...prev, [_user?.Id as string]: selectedBooks };
                                  });
                                }}
                                errorMessage={errors?.rentBack?.[index]?.NgayTra?.message}
                                disabledDate={(current) => {
                                  return (
                                    (current && current < dayjs().startOf('day')) ||
                                    // @ts-ignore
                                    current < getValues(`rentBack.${index}.NgayMuon`)
                                  );
                                }}
                                placeholder='Chọn ngày trả'
                                clearIcon={false}
                              />
                            </td>
                            <td>{bookStatusData.find(({ value }) => value === IdTrangThai)?.label || '--'}</td>
                            <td>
                              <button onClick={handleRemoveSelectedBook(index)}>
                                <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
                              </button>
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </table>
                </div>
              )}

              {!!selectedBooks?.length && (
                <div className='mt-3 flex justify-end'>
                  <Button onClick={handleRentBook} loading={loadingRentBook} disabled={loadingRentBook}>
                    Cho mượn sách
                  </Button>
                </div>
              )}
            </div>
          </div>
        </div>
      )}

      {Boolean(_user) && (
        <div>
          <h3 className='mt-6 mb-3 text-xl font-semibold text-primary-10'>Danh sách đang mượn</h3>

          <Table
            columns={columns}
            className='custom-table'
            dataSource={rentingBookData?.data.Item || []}
            pagination={{
              onChange(current, pageSize) {
                setPage(current);
                setPaginationSize(pageSize);
              },
              current: page || 1,
              hideOnSinglePage: true,
              showSizeChanger: false,
              pageSize: 10,
              total: rentingBookData?.data.Item.length
            }}
            loading={loadingRentingBook || isFetching}
            locale={{
              emptyText: () => <Empty />
            }}
            rowKey={(record) => record.Id}
            bordered
            scroll={{ x: 980 }}
          />
        </div>
      )}

      <ActionModal
        title='Chọn Tình trạng sách'
        open={action?.action === 'return'}
        onSubmit={handleReturn}
        onBack={handleBack}
        labelSubmit='Trả sách'
        loading={loadingReturnBook || isFetchingSachCaBiet}
      >
        <Select
          items={[{ label: 'Chọn tình trạng sách', value: '' }, ...bookStatusData]}
          className='w-full'
          register={returnRegister}
          name='status'
          errorMessage={returnErrors.status?.message}
          disabled={loadingReturnBook || isFetchingSachCaBiet}
        />
      </ActionModal>

      <ActionModal
        title='Chọn ngày gia hạn'
        open={action?.action === 'extend'}
        onSubmit={handleExtend}
        onBack={handleBack}
        loading={loadingExtendRenting}
      >
        <DatePicker
          control={actionsControl}
          name='returnExtendDate'
          errorMessage={actionsErrors?.returnExtendDate?.message}
          disabledDate={(current) => {
            return current && current <= dayjs().endOf('day');
          }}
          placeholder='Chọn ngày trả'
          disabled={loadingExtendRenting}
          defaultValue={dayjs(action?.book.NgayTra).add(1, 'day')}
        />
      </ActionModal>

      <ActionModal
        title='Cấu hình số ngày mượn trả sách'
        open={configReturnDateVisible}
        onSubmit={handleConfigReturnDate}
        onBack={toogleConfigReturnDateNumber}
        loading={loadingSetNumberOfRentingDays || loadingNumberOfRentingDays}
      >
        <Input
          register={returnDateNumberRegister}
          name='returnDateNumber'
          placeholder='Nhập số ngày trả'
          type='number'
          containerClassName='pl-2 bg-white w-full border rounded-lg flex mb-1'
          className='mr-2 w-full text-right font-semibold text-black outline-none'
          right={
            <div className='rounded-br-lg rounded-tr-lg bg-secondary-20/30 py-3 px-3'>
              <span className='text-xs'>Ngày</span>
            </div>
          }
          disabled={loadingNumberOfRentingDays || loadingSetNumberOfRentingDays}
        />

        {!!returnDateNumberErrors.returnDateNumber?.message && (
          <span className='font-semibold text-danger-10'>{returnDateNumberErrors.returnDateNumber?.message}</span>
        )}
      </ActionModal>

      <Loading
        open={
          loadingRentBook ||
          (loadingMember && Boolean(_user?.Id)) ||
          (loadingHoldingBooksByUser && Boolean(_user?.Id)) ||
          editBySCB
        }
      />
    </div>
  );
};

export default RentBackBookPage;
