FROM node:18.18.2-alpine
RUN apk update && apk add git

ENV REACT_APP_API_AUTH=https://identity.bitechco.link/
ENV REACT_APP_API_BASE_URL=https://api-gateway.bitechco.link:5100
ENV REACT_APP_CLIENT_ID=web_ThuVienSo
ENV REACT_APP_SERVICE_THUVIENSO=thuvienso
ENV REACT_APP_SERVICE_MLLH=mucluclienhop
ENV REACT_APP_ACCOUNT_INFO=https://quanly.bitechco.link/ThongTinCaNhan
ENV REACT_APP_SECRECT_KEY=L8tf9oYGCriTcrEMF1Bh

RUN mkdir -p /dig-lib
WORKDIR /dig-lib
COPY . .
COPY package*.json ./
COPY yarn.lock ./

RUN yarn install
RUN yarn build:staging

EXPOSE 3000
CMD ["yarn", "start:staging"]
