import { TabsProps } from 'antd';
import { Button, Tabs, Title } from 'components';
import { useState } from 'react';
import { ListOfReservingBooks } from './components';
import { useNavigate } from 'react-router-dom';
import { path } from 'constants/path';

const items: TabsProps['items'] = [
  {
    key: 'month',
    label: `Thống kê tháng`,
    children: <ListOfReservingBooks picker='month' />,
    forceRender: true
  },
  {
    key: 'day',
    label: `Thống kê ngày`,
    children: <ListOfReservingBooks picker='date' />,
    forceRender: false
  }
];

const StaticalReservingBooks = () => {
  const [activeTab, setActiveTab] = useState<string>('month');

  const handleChangeTab = (activeTab: string) => setActiveTab(activeTab);

  const navigate = useNavigate();

  return (
    <div className='p-5'>
      <Title title='LỊCH SỬ ĐẶT MƯỢN ONLINE' />

      <Tabs
        items={items}
        activeKey={activeTab}
        onChange={handleChangeTab}
        destroyInactiveTabPane={true}
        className='mt-3'
        right={
          <Button onClick={() => navigate(path.thongkedanhsachdatmuonbieudo)} className='ml-auto'>
            Xem biểu đồ
          </Button>
        }
      />
    </div>
  );
};

export default StaticalReservingBooks;
