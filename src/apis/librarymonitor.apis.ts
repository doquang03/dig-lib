import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/CanBoThuVien`;

const libraryMonitorApis = {
  Index: (queryString: string) => {
    return httpThuVien.post(`/${endPoint}/Index` + queryString);
  }
};

export default libraryMonitorApis;
