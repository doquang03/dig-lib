import { useMutation, useQueryClient } from '@tanstack/react-query';
import { Modal } from 'antd';
import { connectionApis } from 'apis';
import { Button, Input } from 'components';
import { useUser } from 'contexts/user.context';
import { ChangeEvent, FormEvent, useCallback, useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { LocalStorageEventTartget } from 'utils/auth';

const FAILURE_TIMES = 5;
const BLOCK_SECONDS = 300;
const CREATED_FAILED_TIME = 'CREATED_FAILED_TIME';

type Props = {
  loading: boolean;
  onConnect: VoidFunction;
};

const ConnectionPage = (props: Props) => {
  const [key, setKey] = useState<string>('');
  const [error, setError] = useState<boolean>(false);
  const [loginFailureTimes, setLoginFailureTimes] = useState(0);
  const [diffSeconds, setDiffSeconds] = useState<number>(0);

  const queryClient = useQueryClient();

  const { profile } = useUser();

  const { mutate: connectToCenter, isLoading: loadingConnectToCenter } = useMutation({
    mutationFn: connectionApis.connectToHost,
    onSuccess: () => {
      queryClient.invalidateQueries(['getUnitConnectionStatus']);
      toast.success('Kết nối mục lục liên hợp thành công');
      props.onConnect();
    },
    onError: () => {
      toast.error('Key không hợp lệ. Vui lòng thử lại!');
      setLoginFailureTimes((pre) => pre + 1);
    }
  });

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (!e) return;

    const { value } = e.target;
    setKey(value);
  };

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    if (diffSeconds) {
      return;
    }

    if (!key) {
      return;
    } else {
      setError(false);
    }

    if (loginFailureTimes >= FAILURE_TIMES) {
      localStorage.setItem(CREATED_FAILED_TIME, new Date() + '');
      setLoginFailureTimes(0);
      const heheEvent = new Event('hehe');
      LocalStorageEventTartget.dispatchEvent(heheEvent);
      return;
    }

    !loadingConnectToCenter && connectToCenter(key.trim());
  };

  const createdFailedTime = localStorage.getItem(CREATED_FAILED_TIME) || '';
  const getSeconds = useCallback(() => {
    if (loginFailureTimes === FAILURE_TIMES || createdFailedTime) {
      const currentTime = new Date();
      const failedTime = new Date(
        new Date(createdFailedTime).setSeconds(new Date(createdFailedTime).getSeconds() + BLOCK_SECONDS)
      );

      // @ts-ignore
      const diffSeconds = Math.round(Math.abs(failedTime - currentTime) / 1000);

      setDiffSeconds(diffSeconds);
    }
  }, [loginFailureTimes, createdFailedTime]);

  useEffect(() => {
    if (!createdFailedTime) return;

    getSeconds();
  }, [getSeconds, createdFailedTime]);

  useEffect(() => {
    LocalStorageEventTartget.addEventListener('hehe', getSeconds);

    const timer = setTimeout(() => {
      if (diffSeconds) {
        if (diffSeconds > 300) {
          setDiffSeconds(0);
          return localStorage.removeItem(CREATED_FAILED_TIME);
        }
        setDiffSeconds((prevState) => {
          if (prevState - 1 <= 0) {
            localStorage.removeItem(CREATED_FAILED_TIME);
          }
          return prevState - 1;
        });
      }
    }, 1000);

    return () => {
      clearTimeout(timer);
      LocalStorageEventTartget.removeEventListener('hehe', getSeconds);
    };
  }, [loginFailureTimes, diffSeconds, getSeconds, createdFailedTime]);

  if (props.loading) {
    return (
      <Modal open centered footer={null} closable={false}>
        <p>Hệ thống đang kiểm trang tình trạng kết nối của đơn vị {profile?.NameSchool}.</p>
        <p>Vui lòng đợi. </p>
      </Modal>
    );
  }

  const minutes = Math.floor(diffSeconds / 60) || 0;
  const seconds = diffSeconds - minutes * 60 || 0;

  return (
    <div>
      <div className='mt-3 flex items-center gap-1'>
        <svg width='22' height='22' viewBox='0 0 22 22' fill='none' xmlns='http://www.w3.org/2000/svg'>
          <g clipPath='url(#clip0_7186_4580)'>
            <path
              d='M11 21.3125C16.6954 21.3125 21.3125 16.6954 21.3125 11C21.3125 5.30456 16.6954 0.6875 11 0.6875C5.30456 0.6875 0.6875 5.30456 0.6875 11C0.6875 16.6954 5.30456 21.3125 11 21.3125Z'
              fill='#FF7D00'
            />
            <path
              d='M10.1595 13.095C10.1469 13.017 10.1406 12.942 10.1406 12.8698C10.1406 12.7976 10.1406 12.722 10.1406 12.6429C10.1313 12.2627 10.2009 11.8846 10.3452 11.5326C10.4711 11.2356 10.6453 10.9615 10.8608 10.7214C11.0672 10.4972 11.2977 10.2966 11.5483 10.1232C11.7912 9.95251 12.0204 9.78064 12.2358 9.60762C12.428 9.45346 12.6011 9.2769 12.7514 9.08168C12.8902 8.89404 12.9622 8.66532 12.9559 8.43199C12.9596 8.27113 12.9228 8.11192 12.8489 7.96897C12.7751 7.82602 12.6665 7.70392 12.5331 7.61387C12.2501 7.40991 11.8118 7.30793 11.2183 7.30793C10.9356 7.3047 10.6545 7.34885 10.3864 7.43855C10.1435 7.52166 9.90876 7.62695 9.68516 7.75309C9.48605 7.86491 9.29585 7.9919 9.11625 8.13293C8.9501 8.2647 8.80401 8.37413 8.67797 8.46121L7.73438 7.23918C7.97111 6.98342 8.23837 6.75772 8.53016 6.56715C8.8259 6.37333 9.14168 6.21198 9.47203 6.0859C9.80332 5.95926 10.1457 5.86374 10.4947 5.80059C10.835 5.73862 11.1801 5.70698 11.5259 5.70605C12.587 5.70605 13.412 5.92777 14.0009 6.37121C14.2835 6.57429 14.5118 6.84366 14.6658 7.15564C14.8198 7.46762 14.8949 7.81265 14.8844 8.16043C14.8984 8.58331 14.821 9.00423 14.6575 9.39449C14.5215 9.70362 14.3305 9.98549 14.0938 10.2264C13.8614 10.4512 13.6075 10.6527 13.3358 10.8279C13.0751 10.9979 12.8264 11.1857 12.5916 11.39C12.3636 11.5868 12.1686 11.8189 12.0141 12.0775C11.8471 12.3861 11.7663 12.734 11.7803 13.0846L10.1595 13.095ZM9.86734 15.1695C9.85986 15.0192 9.88467 14.869 9.94011 14.729C9.99555 14.5891 10.0803 14.4627 10.1887 14.3582C10.4214 14.1519 10.7255 14.0446 11.0361 14.0592C11.356 14.0415 11.6703 14.1487 11.9127 14.3582C12.021 14.4627 12.1057 14.5892 12.1611 14.7291C12.2166 14.869 12.2414 15.0192 12.2341 15.1695C12.2405 15.3202 12.2152 15.4706 12.1599 15.611C12.1045 15.7513 12.0203 15.8784 11.9127 15.9842C11.6724 16.198 11.3572 16.308 11.0361 16.2901C10.8817 16.2973 10.7274 16.274 10.582 16.2215C10.4366 16.169 10.3029 16.0884 10.1887 15.9842C10.0814 15.8783 9.99755 15.7511 9.94248 15.6107C9.88742 15.4704 9.8624 15.3201 9.86906 15.1695H9.86734Z'
              fill='white'
            />
          </g>

          <defs>
            <clipPath id='clip0_7186_4580'>
              <rect width='22' height='22' fill='white' />
            </clipPath>
          </defs>
        </svg>

        <p className='text-[16px] font-bold text-primary-10'>Hướng dẫn:</p>
      </div>

      <ul className='ml-6 list-inside list-disc'>
        <li className='text-primary-10'>
          Nhập key được cung cấp từ <span className='font-bold'>Mục lục liên hợp</span> để để kết nối.
        </li>

        <li className='text-primary-10'>Nếu nhập sai key trên 5 lần sẽ bị khóa tạm thời trong 5 phút.</li>
      </ul>

      <form className='w-1/2' onSubmit={handleSubmit}>
        <div className='my-4 grid grid-cols-12 gap-3'>
          <div className='col-span-1 font-bold'>Key:</div>
          <div className='col-span-11'>
            <Input
              placeholder='Nhập Key'
              name='key'
              maxLength={256}
              onChange={handleChange}
              value={key}
              errorMessage={
                (error && !Boolean(key) && !diffSeconds && 'Key không được để trống') ||
                (!!diffSeconds && `Đã vượt quá số lần cho phép. Vui lòng thử lại sau ${minutes} phút ${seconds} giây.`)
              }
            />
          </div>
        </div>

        <Button
          className='my-4 ml-auto'
          variant={!key || (error && !Boolean(key)) || !!diffSeconds || props.loading ? 'disabled' : 'default'}
          type='submit'
          disabled={!key || (error && !Boolean(key)) || !!diffSeconds || props.loading}
          loading={loadingConnectToCenter || props.loading}
        >
          Kết nối
        </Button>
      </form>
    </div>
  );
};

export default ConnectionPage;
