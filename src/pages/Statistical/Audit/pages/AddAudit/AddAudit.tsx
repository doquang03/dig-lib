import { CloseOutlined, DeleteFilled, FilterFilled } from '@ant-design/icons';
import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Checkbox, CheckboxOptionType, Input as InputAntd, InputRef, Table, Tooltip } from 'antd';
import type { ColumnType } from 'antd/es/table';
import { ColumnsType } from 'antd/es/table';
import { bookstoreTypeApis } from 'apis';
import auditApis from 'apis/audit.api';
import { Button, DatePicker, Empty, Input, Loading, Select, Title } from 'components';
import { FORMAT_DATE_PICKER } from 'constants/config';
import { path } from 'constants/path';
import { AuditContext } from 'contexts/audit';
import { UserConText, useUser } from 'contexts/user.context';
import dayjs, { Dayjs } from 'dayjs';
import { useBookStatuses, useColumnFilterTableProps } from 'hooks';
import { debounce, uniqBy } from 'lodash';
import { ChangeEvent, Key, useContext, useMemo, useRef, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { autditSchema } from 'utils/rules';
import { convertDate, download, getSerialNumber, removeVietnameseTones } from 'utils/utils';

type DataIndex = keyof BookAudit;

const CheckboxGroup = Checkbox.Group;

const AddAudit = () => {
  const [selectedBooksToLiquidate, setSelectedBooksToLiquidate] = useState<BookAudit[]>([]);
  const [{ page, pageSize }, setPaging] = useState<{ page: number; pageSize: number }>({ page: 1, pageSize: 30 });
  const [stores, setStores] = useState<BookStoreType[]>([]);
  const [col, setCol] = useState<keyof BookAudit>();
  const [ids, setIds] = useState<string[]>([]);
  const ref = useRef<HTMLDivElement | null>(null);

  const navigate = useNavigate();

  const { auditId } = useParams();

  const { data: bookStatuses } = useBookStatuses();

  const { isAllowedAdjustment } = useUser();

  const { getColumnSearchProps } = useColumnFilterTableProps<BookAudit>((key) => setCol(key));

  const user = useContext(UserConText);
  const {
    auditName,
    storeId,
    createDateFinalize,
    createDateReport,
    ghiChu,
    setAudit,
    selectedBooks,
    setSelectedBooks
  } = useContext(AuditContext);

  const {
    register,
    control,
    handleSubmit,
    getValues,
    setValue,
    formState: { errors }
  } = useForm<{
    ten: string;
    idKho: string;
    createDateReport: Dayjs;
    createDateFinalize: Dayjs;
    ghiChu?: string;
  }>({
    defaultValues: {
      ten: auditName,
      idKho: storeId,
      createDateReport: dayjs(createDateReport),
      createDateFinalize: dayjs(createDateFinalize),
      ghiChu: ghiChu
    },
    resolver: yupResolver(autditSchema)
  });

  const {
    data: auditData,
    isLoading: loadingAuditData,
    refetch: refetchAuditData,
    isRefetching: isRefetchingAuditData
  } = useQuery({
    queryKey: ['audit', auditId],
    queryFn: () => auditApis.getAudit(auditId + ''),
    enabled: Boolean(auditId),
    onSuccess: (data) => {
      const { soKiemKe, ListKiemKe } = data.data.Item;
      setValue('ten', soKiemKe.TenPhieu);
      setValue('ghiChu', soKiemKe.GhiChu);
      setValue('idKho', soKiemKe.IdKho);
      setValue('createDateReport', dayjs(soKiemKe.NgayVaoSo));
      setValue('createDateFinalize', dayjs(soKiemKe.NgayChotSo));

      setSelectedBooks((prevState) => {
        return uniqBy([...prevState, ...ListKiemKe], 'IdSachCaBiet');
      });
    }
  });

  // search zone
  const searchInput = useRef<InputRef>(null);

  const { mutateAsync: createAudit, isLoading: loadingCreateAudit } = useMutation({
    mutationFn: auditApis.createAudit,
    onSuccess: () => {
      setSelectedBooks([]);
      if (!Boolean(auditId)) {
        toast.success('Tạo phiếu kiểm kê thành công');

        setAudit({
          auditName: 'Phiếu kiểm kê tài sản - Ngày ' + convertDate(new Date()),
          storeId: '',
          createDateReport: dayjs(),
          createDateFinalize: dayjs()
        });

        navigate(path.thongkekiemketaisan);
        return;
      }

      toast.success('Cập nhật phiếu kiểm kê thành công');
    }
  });

  const { mutateAsync: deleteSBBPhieuKiemKe, isLoading: loadingDeleteBook } = useMutation({
    mutationFn: (ids: string[]) => auditApis.removeListSCBPhieuKiemKe(ids, auditId + ''),
    mutationKey: ['deleteSBBPhieuKiemKe'],
    onSuccess: () => {
      refetchAuditData();
    }
  });

  const { mutate: downloadExcelFile, isLoading: loadingDownLoad } = useMutation({
    mutationFn: () => auditApis.downloadExcel(auditId + ''),
    onSuccess: (data) => {
      toast.success('Tải file Excel thành công');

      download(`ExcelPhieuKiemke_${auditData?.data.Item.soKiemKe.MaKiemKe}.xls`, data.data);
    }
  });

  const { data: storesData, isLoading: loadingStore } = useQuery({
    queryKey: ['stores'],
    queryFn: bookstoreTypeApis.getAllStores,
    onSuccess: (data) => {
      setStores(data.data.Item);
    }
  });

  const storesResult = useMemo(() => {
    if (!storesData?.data.Item) return [];

    const storeResults = storesData?.data.Item.map(({ Ten, Id }) => {
      return {
        label: Ten,
        value: Id
      };
    });

    return storeResults;
  }, [storesData?.data.Item]);

  const handleChangeTrangThai = (event: ChangeEvent<HTMLSelectElement>, maDKCB: string) => {
    setSelectedBooks((prevState) => {
      return prevState.map((book) => {
        if (maDKCB === book.MaDKCB) {
          book.IdTinhTrang = event.target.value;
        }

        return book;
      });
    });
  };

  const handleChangeGhiChu = debounce((event: ChangeEvent<HTMLInputElement>, maDKCB: string) => {
    setSelectedBooks((prevState) => {
      return prevState.map((book) => {
        if (maDKCB === book.MaDKCB) {
          book.GhiChu = event.target.value;
        }

        return book;
      });
    });
  }, 400);

  const handleRemove = (maDKCB: string, id?: string) => {
    if (loadingDeleteBook) return;

    if (!Boolean(auditId) && id) return;

    const result = selectedBooks.filter(({ MaDKCB }) => MaDKCB !== maDKCB);

    setSelectedBooks(result);
    setIds((prevState) => {
      return [...prevState, id + ''];
    });
  };

  const handleFilterStore = (value: string) => {
    if (!storesData?.data.Item.length) return;

    const data = storesData?.data.Item.filter((store) =>
      removeVietnameseTones(store.Ten.toLocaleLowerCase().trim()).includes(
        removeVietnameseTones(value.toLocaleLowerCase()).trim()
      )
    );

    setStores(data);
  };

  // select filter
  const getColumnFilterProps = (
    dataIndex: DataIndex,
    options: CheckboxOptionType[],
    isFilter?: boolean
  ): ColumnType<BookAudit> => ({
    filterDropdown: ({ setSelectedKeys, confirm, close }) => (
      <div className='relative flex flex-col gap-1 p-2' onKeyDown={(e) => e.stopPropagation()}>
        {isFilter && <InputAntd placeholder='Nhập tên kho' onChange={(e) => handleFilterStore(e.target.value)} />}

        <CheckboxGroup
          options={options}
          onChange={(value) => setSelectedKeys((value as Key[]) ? (value as Key[]) : [])}
          className='flex max-h-[500px] flex-1 flex-col overflow-y-auto'
        />

        <button
          onClick={() => {
            close();
          }}
          className='absolute -right-2 -top-3 rounded-full bg-slate-500/75 px-1'
        >
          <CloseOutlined style={{ fontSize: 14, color: 'white' }} />
        </button>

        <button onClick={() => confirm()}>Chọn</button>
      </div>
    ),
    filterIcon: (filtered: boolean) => (
      <FilterFilled style={{ color: filtered ? '#1890ff' : 'white', fontSize: 'bold' }} />
    ),
    onFilter: (value, record) => {
      // @ts-ignore
      return record?.[dataIndex]?.indexOf(value as string) === 0;
    },
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    }
  });

  const columns: ColumnsType<BookAudit> = [
    {
      key: 'stt',
      dataIndex: 'stt',
      title: 'STT',
      render: (value, record, index) => getSerialNumber(page, pageSize, index)
    },
    {
      key: 'MaDKCB',
      dataIndex: 'MaDKCB',
      title: 'Mã đăng ký cá biệt',
      onCell: (record) => ({
        className:
          record.MaDKCB?.toLocaleLowerCase().includes('đã xóa') ||
          record.MaDKCB?.toLocaleLowerCase().includes('đã thanh lý')
            ? 'text-danger-10'
            : ''
      }),
      ...getColumnSearchProps('Mã ĐKCB', 'MaDKCB', col === 'MaDKCB')
    },
    {
      key: 'TenSach',
      dataIndex: 'TenSach',
      title: 'Tên sách',
      ...getColumnSearchProps('Tên sách', 'TenSach', col === 'TenSach')
    },
    {
      key: 'IdKho',
      dataIndex: 'IdKho',
      title: 'Kho',
      render: (value, record) => storesData?.data.Item.find(({ Id }) => Id === record.IdKho)?.Ten || '--',
      ...getColumnFilterProps(
        'IdKho',
        stores.map(({ Id, Ten }) => {
          return {
            label: Ten,
            value: Id
          };
        }) as CheckboxOptionType[],
        true
      )
    },
    {
      key: 'IdTinhTrang',
      dataIndex: 'IdTinhTrang',
      title: 'Tình trạng',
      render: (value, record) => {
        return (
          <Select
            items={bookStatuses}
            defaultValue={record.IdTinhTrang}
            onChange={(e) => handleChangeTrangThai(e, record.MaDKCB)}
          />
        );
      },
      ...getColumnFilterProps('IdTinhTrang', bookStatuses as CheckboxOptionType[])
    },
    {
      key: 'Ghi chú',
      dataIndex: 'GhiChu',
      title: 'Ghi chú',
      render: (value, record, index) => {
        return (
          <Input
            placeholder='Nhập ghi chú'
            onChange={(e) => handleChangeGhiChu(e, record.MaDKCB)}
            defaultValue={record.GhiChu || ''}
          />
        );
      }
    },
    {
      key: 'action',
      dataIndex: 'action',
      title: '',
      render(value, record) {
        return (
          <Tooltip title='Xóa'>
            <button
              className='mx-2'
              onClick={(e) => handleRemove(record.MaDKCB, record.Id)}
              disabled={loadingDeleteBook}
            >
              <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
            </button>
          </Tooltip>
        );
      }
    }
  ];

  const onSubmit = handleSubmit(async (data) => {
    try {
      await createAudit({
        Id: auditId || '',
        TenPhieu: data.ten,
        IdKho: data.idKho,
        NgayVaoSo: new Date(data.createDateReport + '').toISOString(),
        NgayChotSo: new Date(data.createDateFinalize + '').toISOString(),
        IdUserAdmin: user.profile?.Id || '',
        UserName: user?.profile?.FullName || '',
        GhiChu: data.ghiChu || '',
        books: selectedBooks.map((book) => {
          return {
            Id: book.Id || '',
            IdSachCaBiet: book.IdSachCaBiet,
            MaDKCB: book.MaDKCB,
            GiaBia: book.GiaBia,
            IdTinhTrang: book.IdTinhTrang,
            IdKho: book.IdKho,
            TenSach: book.TenSach,
            GhiChu: book.GhiChu || '',
            Final: true
          };
        })
      });

      !!ids.length && (await deleteSBBPhieuKiemKe(ids));
    } catch (error) {}
  });

  const handleBack = () => {
    setAudit({
      auditName: 'Phiếu kiểm kê tài sản - Ngày ' + convertDate(new Date()),
      storeId: '',
      createDateReport: dayjs(),
      createDateFinalize: dayjs()
    });

    setSelectedBooks([]);

    navigate(path.thongkekiemketaisan);
  };

  const handleAddBooks = () => {
    handleSubmit((data) => {
      setAudit({
        auditName: data.ten,
        storeId: data.idKho,
        createDateReport: data.createDateReport,
        createDateFinalize: data.createDateFinalize,
        ghiChu: data.ghiChu
      });
    })();

    navigate(path.addBooksToAudit, { state: auditId });
  };

  const onSelectChange = (_: Key[], selectedRowValue: BookAudit[]) => {
    setSelectedBooksToLiquidate(selectedRowValue);
  };

  const rowSelections = useMemo(() => {
    return Boolean(auditId)
      ? {
          selectedRowKeys: selectedBooksToLiquidate.map((item) => item.MaDKCB),
          preserveSelectedRowKeys: true,
          onChange: onSelectChange,
          getCheckboxProps: (record: BookAudit) => ({
            disabled:
              !Boolean(record.Id) ||
              record.MaDKCB?.toLocaleLowerCase().includes('đã xóa') ||
              record.MaDKCB?.toLocaleLowerCase().includes('đã thanh lý') ||
              (record.IdPhieuMuon !== null && record.IdPhieuMuon !== ''),
            name: record.TenSach
          })
        }
      : undefined;
  }, [selectedBooksToLiquidate, auditId]);

  const handleLiquidateBooks = () =>
    navigate(path.taoPhieuXuatKho, {
      state: {
        selectedSCB: selectedBooksToLiquidate.map((book) => {
          return {
            Id: book.IdSachCaBiet,
            MaCaBiet: book.MaDKCB,
            MaKiemSoat: book.MaKiemSoat,
            TenSach: book.TenSach,
            GiaBia: book.GiaBia,
            TrangThai: bookStatuses.find((status) => status.value === book.IdTinhTrang)?.label
          };
        })
      }
    });

  return (
    <div ref={ref}>
      <Title title={Boolean(auditId) ? 'Cập nhật phiếu kiểm kê' : 'Thêm phiếu kiểm kê'} />
      <form onSubmit={onSubmit}>
        <div className='mt-3 flex flex-col items-center md:flex-row md:gap-4'>
          <div className='w-full md:w-1/2 '>
            <label className='font-semibold'>
              Tên phiếu kiểm kê <span className='text-danger'>*</span>
            </label>

            <Input
              placeholder='Nhập tên phiếu kiểm kê'
              name='ten'
              register={register}
              errorMessage={errors.ten?.message}
            />
          </div>

          <div className='flex w-full flex-col md:w-1/2'>
            <label className='font-semibold'>
              Phạm vi kiểm kê <span className='text-danger'>*</span>
            </label>

            <Select
              items={[{ label: 'Tất cả kho', value: '' }, ...storesResult]}
              disabled={loadingStore || !storesData?.data?.Item.length}
              className='w-full'
              name='idKho'
              register={register}
            />
          </div>
        </div>

        <div className='flex flex-col items-center md:mt-3 md:flex-row md:gap-4'>
          <div className='flex w-full flex-col items-center gap-2 md:w-1/2 md:flex-row'>
            <div className='w-full md:w-1/2'>
              <label className='font-semibold'>
                Ngày lập phiếu <span className='text-danger'>*</span>
              </label>

              <DatePicker
                placeholder='Chọn ngày lập phiếu'
                control={control}
                name='createDateReport'
                defaultValue={dayjs(dayjs().format(FORMAT_DATE_PICKER[0]))}
                errorMessage={errors.createDateReport?.message}
                disabledDate={(current) => {
                  return (current && current) > dayjs().endOf('day');
                }}
              />
            </div>

            <div className='w-full md:w-1/2'>
              <label className='font-semibold'>
                Ngày chốt số liệu <span className='text-danger'>*</span>
              </label>

              <DatePicker
                placeholder='Chọn ngày chốt số liệu'
                control={control}
                name='createDateFinalize'
                defaultValue={dayjs(dayjs().format(FORMAT_DATE_PICKER[0]))}
                errorMessage={errors.createDateFinalize?.message}
                disabledDate={(current) => {
                  return (current && current) < dayjs(getValues('createDateReport')).startOf('day');
                }}
              />
            </div>
          </div>

          <div className='flex w-full flex-col md:w-1/2'>
            <label className='font-semibold'>Ghi chú</label>

            <Input placeholder='Nhập ghi chú' name='ghiChu' register={register} />
          </div>
        </div>
      </form>

      <p className='mt-3 font-semibold text-primary-10'>Thông tin kiểm kê chi tiết</p>

      <div className='flex items-center justify-between'>
        <div className='mt-2 flex items-center gap-1'>
          {isAllowedAdjustment && (
            <>
              <Button onClick={handleAddBooks}>{!Boolean(auditId) ? 'Thêm nhanh' : 'Thêm sách kiểm kê'} </Button>
              {Boolean(auditId) && (
                <>
                  <Button onClick={() => downloadExcelFile()} loading={loadingDownLoad} disabled={loadingDownLoad}>
                    Tải file Excel
                  </Button>

                  <Button
                    variant={!!selectedBooksToLiquidate.length ? 'default' : 'disabled'}
                    onClick={handleLiquidateBooks}
                  >
                    Tạo phiếu thanh lý
                  </Button>
                </>
              )}
            </>
          )}
        </div>

        <div className='rounded-md bg-tertiary-30/20 p-1'>
          <p className='font-semibold text-tertiary-30'>
            Tổng giá trị phiếu kiểm kê:{' '}
            <span className='font-bold'>
              {new Intl.NumberFormat('vi-VN', {
                style: 'currency',
                currency: 'VND',
                maximumFractionDigits: 9
              }).format(selectedBooks.reduce((total, { GiaBia }) => (total += Number(GiaBia)) || 0, 0))}{' '}
            </span>{' '}
          </p>
        </div>
      </div>

      <Table
        columns={columns}
        className='custom-table mt-3'
        bordered
        rowSelection={rowSelections}
        locale={{
          emptyText: () => <Empty />
        }}
        dataSource={selectedBooks}
        rowKey={(row) => row.MaDKCB}
        pagination={{
          onChange(current, pageSize) {
            setPaging({ page: current, pageSize: pageSize });
            ref.current?.scrollIntoView({ behavior: 'smooth' });
          },
          current: page,
          pageSize: pageSize,
          total: selectedBooks && selectedBooks.length,
          hideOnSinglePage: false,
          showQuickJumper: true,
          locale: { jump_to: '', page: '', items_per_page: '/Trang' },
          pageSizeOptions: [30, 50, 100]
        }}
      />

      <div className='mt-3 flex items-center justify-end gap-2'>
        <Button onClick={handleBack} variant='secondary'>
          Quay về
        </Button>

        {isAllowedAdjustment && <Button onClick={onSubmit}>{!Boolean(auditId) ? 'Thêm mới' : 'Cập nhật'}</Button>}
      </div>

      <Loading
        open={
          loadingCreateAudit || (Boolean(auditId) && loadingAuditData) || isRefetchingAuditData || loadingDeleteBook
        }
      />
    </div>
  );
};

export default AddAudit;
