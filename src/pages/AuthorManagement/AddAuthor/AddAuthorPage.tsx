import { RedoOutlined } from '@ant-design/icons';
import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery } from '@tanstack/react-query';
import { authorApis } from 'apis';
import { AxiosError } from 'axios';
import { Button, Input, Loading, Title } from 'components';
import { path } from 'constants/path';
import 'css/AddAuthor.css';
import { useEffect, useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router';
import { toast } from 'react-toastify';
import { authorSchema } from 'utils/rules';
import { GenerateVietnameseAuthorCode } from 'utils/utils';
import * as yup from 'yup';

type AuthorForm = yup.InferType<typeof authorSchema>;

const initialAuthorValue = {
  TenTacGia: '',
  MaTacGia: '',
  QuocTich: '',
  MoTa: ''
};

const AddAuthorPage = () => {
  const { authorId } = useParams();

  const [currentSchema, setCurrentSchema] = useState(authorSchema);
  const [duplicateAuthor, setDuplicateAuthor] = useState<Array<string>>([]);
  const [isTrigger, setIsTrigger] = useState('idle');

  const {
    register,
    handleSubmit,
    setValue,
    getValues,
    trigger,
    formState: { errors }
  } = useForm<AuthorForm>({
    defaultValues: initialAuthorValue,
    resolver: yupResolver(currentSchema)
  });

  const navigate = useNavigate();

  const { isLoading, refetch } = useQuery({
    queryKey: ['EditByID', authorId],
    queryFn: () => authorApis.EditByID(authorId as string),
    onSuccess: (res) => {
      let Item = res.data.Item;

      setValue('TenTacGia', Item.TenTacGia);
      setValue('MaTacGia', Item.MaTacGia);
      setValue('QuocTich', Item.QuocTich);
      setValue('MoTa', Item.MoTa);
    },
    enabled: !!authorId
  });

  const { mutate: handleCreate, isLoading: isLoadingCreate } = useMutation({
    mutationFn: (payload: object) => authorApis.Create(payload),
    onSuccess: () => {
      toast.success('Thêm tác giả thành công');
      handleBack();
    },
    onError: (
      ex: AxiosError<
        ResponseApi<{
          MaTacGia: string;
        }>
      >
    ) => {
      if (ex?.response?.data?.ExMessage === 'MaTacGia') {
        let tempAuthor = duplicateAuthor.concat([ex?.response?.data?.Item?.MaTacGia]);
        setDuplicateAuthor(tempAuthor);
        let authorSchemaClone = authorSchema.clone();
        let authorSchemaObject = yup.object({
          MaTacGia: yup
            .string()
            .trim()
            .notOneOf(tempAuthor, ex?.response?.data?.Message)
            .required('Mã tác giả không được để trống')
        });
        authorSchemaClone = authorSchemaClone.concat(authorSchemaObject);
        setCurrentSchema(authorSchemaClone);
        setIsTrigger('MaTacGia');
      }
    }
  });

  const { mutate: handleEdit, isLoading: isLoadingEdit } = useMutation({
    mutationFn: (payload: object) => authorApis.Edit(payload),
    onSuccess: () => {
      toast.success('Cập nhật tác giả thành công');
      refetch();
    },
    onError: (
      ex: AxiosError<
        ResponseApi<{
          MaTacGia: string;
        }>
      >
    ) => {
      if (ex?.response?.data?.ExMessage === 'MaTacGia') {
        let tempAuthor = duplicateAuthor.concat([ex?.response?.data?.Item?.MaTacGia]);
        setDuplicateAuthor(tempAuthor);
        let authorSchemaClone = authorSchema.clone();
        let authorSchemaObject = yup.object({
          MaTacGia: yup
            .string()
            .trim()
            .notOneOf(tempAuthor, ex?.response?.data?.Message)
            .required('Mã tác giả không được để trống')
        });
        authorSchemaClone = authorSchemaClone.concat(authorSchemaObject);
        setCurrentSchema(authorSchemaClone);
        setIsTrigger('MaTacGia');
      }
    }
  });

  useEffect(() => {
    if (isTrigger === 'MaTacGia') {
      trigger(isTrigger);
      setIsTrigger('idle');
    }
  }, [isTrigger, trigger]);

  const onSubmit = handleSubmit((data, event) => {
    if (event === undefined) return;
    data.MaTacGia = data.MaTacGia.replace(/ +(?= )/g, '');
    data.MaTacGia = data.MaTacGia.toLocaleUpperCase();
    setValue('MaTacGia', data.MaTacGia);
    if (!authorId) {
      handleCreate({
        ...data,
        TenTacGia: data.TenTacGia.replace(/\s+/g, ' '),
        MaTacGia: data.MaTacGia.replace(/\s+/g, ' ')
      });
    } else {
      handleEdit({
        Id: authorId as string,
        TenTacGia: data.TenTacGia.replace(/\s+/g, ' '),
        MaTacGia: data.MaTacGia.replace(/\s+/g, ' '),
        QuocTich: data?.QuocTich?.replace(/\s+/g, ' '),
        MoTa: data.MoTa
      });
    }
  });

  const handleGetCode = () => {
    setValue('MaTacGia', GenerateVietnameseAuthorCode(getValues('TenTacGia')));
  };

  const isDisable = useMemo(() => !!authorId && isLoading, [authorId, isLoading]);

  const handleBack = () => {
    navigate(path.tacgia);
  };

  return (
    <div className='p-5'>
      <Title title={!!authorId ? 'Cập nhật tác giả' : 'Thêm mới tác giả'} />

      <form className='relative my-6 w-5/6' onSubmit={onSubmit}>
        <div className='mt-10 grid grid-cols-1 md:grid-cols-12'>
          <div className='custom-row col-span-6 px-2'>
            <div className='my-2'>
              <label className='mb-3 font-bold'>
                Tên tác giả <span className='text-danger-10'>*</span>
              </label>

              <Input
                placeholder='Nhập tên tác giả'
                name='TenTacGia'
                register={register}
                onKeyUp={() => {
                  if (!authorId) handleGetCode();
                }}
                errorMessage={errors?.TenTacGia?.message}
                disabled={isDisable}
                maxLength={256}
              />
            </div>

            <div className='my-2'>
              <label className='mb-3 font-bold'>
                Mã tác giả <span className='text-danger-10'>*</span>
              </label>
              <div className='container-refesh'>
                <Input
                  containerClassName='container-refesh-input'
                  placeholder='Nhập mã tác giả'
                  name='MaTacGia'
                  register={register}
                  errorMessage={errors?.MaTacGia?.message}
                  disabled={isDisable}
                  maxLength={51}
                />
                <Button type='button' className='container-refesh-button' onClick={handleGetCode}>
                  <RedoOutlined></RedoOutlined>
                </Button>
              </div>
            </div>
          </div>
          <div className='custom-row col-span-6 px-2'>
            <div className='my-2'>
              <label className='mb-3 font-bold'>Quốc tịch</label>
              <Input
                placeholder='Nhập quốc tịch'
                name='QuocTich'
                register={register}
                errorMessage={errors?.QuocTich?.message}
                disabled={isDisable}
                maxLength={101}
              />
            </div>

            <div className='my-2'>
              <label className='mb-3 font-bold'>Mô tả</label>

              <Input
                placeholder='Nhập nội dung mô tả'
                name='MoTa'
                register={register}
                errorMessage={errors?.MoTa?.message}
                disabled={isDisable}
                maxLength={256}
              />
            </div>
          </div>
        </div>
        <div className='mt-5 flex justify-end px-2'>
          <Button type='button' variant='secondary' className='font-semibold' onClick={handleBack}>
            Quay về
          </Button>

          {!!authorId ? (
            <Button variant='default' type='submit' className='ml-2 font-semibold'>
              Cập nhật
            </Button>
          ) : (
            <Button variant='default' type='submit' className='ml-2 font-semibold'>
              Thêm mới
            </Button>
          )}
        </div>
      </form>
      <Loading open={isLoadingCreate || isLoadingEdit}></Loading>
    </div>
  );
};

export default AddAuthorPage;
