import { CloseCircleOutlined, PlusCircleOutlined } from '@ant-design/icons';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { Checkbox, DatePicker, InputNumber, Radio, RadioChangeEvent } from 'antd';
import { CheckboxChangeEvent } from 'antd/es/checkbox';
import { newspaperMagazineApis } from 'apis';
import { BaseModal, Button } from 'components';
import dayjs from 'dayjs';
import { uniqueId } from 'lodash';
import { useState } from 'react';
import { toast } from 'react-toastify';

const { RangePicker } = DatePicker;

type RangeValue = Parameters<NonNullable<React.ComponentProps<typeof DatePicker.RangePicker>['onChange']>>[0];

type Props = {
  open: boolean;
  onInvisile: VoidFunction;
  id: string;
  onSuccess: VoidFunction;
};

const RegisterModal = (props: Props) => {
  const { id, open, onInvisile, onSuccess } = props;

  const [termRecieveMode, setTermRecieveMode] = useState<'day' | 'month'>('day');
  const [formValues, setFormValue] = useState<{
    StartDay: string;
    EndDay: string;
    SoLuong: number;
    IdBaoTapChi: string;
    isTuan?: boolean;
    ListDay: string[];
    listInputs: { id: string; value: string }[];
  }>({
    StartDay: dayjs().toISOString(),
    EndDay: dayjs().endOf('year').toISOString(),
    SoLuong: 1,
    IdBaoTapChi: '',
    ListDay: [],
    listInputs: [
      { id: uniqueId(), value: '1' },
      { id: uniqueId(), value: '15' }
    ]
  });
  const [error, setError] = useState<boolean>(false);
  const [activeInput, setActiveInput] = useState<string>('');

  const queryClient = useQueryClient();

  const { mutate: register, isLoading: loadingRegister } = useMutation({
    mutationFn: newspaperMagazineApis.registerNewspaperMagazine,
    onSuccess: () => {
      toast.success('Đăng ký thành công');

      onSuccess();

      queryClient.refetchQueries({
        queryKey: [
          'getNewspaperMagazineRegisterationsData',
          {
            Min: dayjs().startOf('week').toISOString(),
            Max: dayjs().endOf('week').toISOString()
          }
        ]
      });

      queryClient.refetchQueries({
        queryKey: ['getNewspaperMagazine']
      });

      setFormValue({
        StartDay: dayjs().toISOString(),
        EndDay: dayjs().endOf('year').toISOString(),
        SoLuong: 1,
        IdBaoTapChi: '',
        ListDay: [],
        listInputs: [
          { id: uniqueId(), value: '1' },
          { id: uniqueId(), value: '15' }
        ]
      });

      setTermRecieveMode('day');

      setError(false);

      onInvisile();
    }
  });

  const handleChangeTermReciveMode = (e: RadioChangeEvent) => {
    if (!e) return;

    setTermRecieveMode(e.target.value);

    setFormValue((prevState) => {
      return {
        ...prevState,
        ListDay: []
      };
    });
  };

  const handleAddInput = () => {
    setFormValue((prevState) => {
      return {
        ...prevState,
        listInputs: [...formValues.listInputs, { id: uniqueId(), value: '30' }]
      };
    });
  };

  const handleRemoveInput = (index: number) => () => {
    const _datesReceive = [...formValues.listInputs];

    _datesReceive.splice(index, 1);

    setFormValue((prevState) => {
      return {
        ...prevState,
        listInputs: _datesReceive
      };
    });
  };

  const handleUpdate = () => {
    if (!id && formValues) return;

    if (termRecieveMode === 'day' && !formValues.ListDay.length) {
      setError(true);

      return;
    } else if (termRecieveMode === 'month' && !formValues.listInputs.length) {
      setError(true);

      return;
    }

    const { StartDay, EndDay, ListDay, SoLuong } = formValues;

    register({
      IdBaoTapChi: id,
      StartDay,
      isTuan: termRecieveMode === 'day',
      EndDay,
      ListDay: termRecieveMode === 'day' ? ListDay : formValues.listInputs.map(({ value }) => value),
      SoLuong
    });
  };

  const handleChangeRangePicker = (values: RangeValue) => {
    if (!values?.length) return;

    setFormValue((prevState) => {
      return {
        ...prevState,
        StartDay: values?.[0]?.startOf('day').toISOString() as string,
        EndDay: values?.[1]?.endOf('day').toISOString() as string
      };
    });
  };

  const handleChangeWeekday = (e: CheckboxChangeEvent) => {
    if (!e) return;

    const { value } = e.target;

    let array: string[] = [...formValues.ListDay, value];

    if (formValues.ListDay.includes(value)) {
      array = array.filter((item) => item !== value);
    }

    setFormValue((prevState) => {
      return {
        ...prevState,
        ListDay: array
      };
    });
  };

  const handleChangeDay = (inputId: string, _value: number) => {
    if (activeInput === inputId) {
      setFormValue((prevState) => {
        return {
          ...prevState,
          listInputs: [...formValues.listInputs].map(({ id, value }, _index) => {
            if (id === inputId) {
              value = _value + '';
            }

            return { id, value };
          })
        };
      });
    }
  };

  const handleChangeNumber = (value: number) => {
    setFormValue((prevState) => {
      return {
        ...prevState,
        SoLuong: value
      };
    });
  };

  const handleBack = () => {
    setFormValue({
      StartDay: new Date(dayjs().format('MM/DD/YYYY')).toISOString(),
      EndDay: new Date(dayjs().endOf('year').format('MM/DD/YYYY')).toISOString(),
      SoLuong: 1,
      IdBaoTapChi: '',
      ListDay: [],
      listInputs: [
        { id: uniqueId(), value: '1' },
        { id: uniqueId(), value: '15' }
      ]
    });

    setError(false);

    setTermRecieveMode('day');

    onInvisile();
  };

  return (
    <BaseModal
      title={'Đăng ký báo tạp chí'}
      open={open}
      onInvisile={handleBack}
      onOk={handleUpdate}
      loading={loadingRegister}
    >
      <div className='mt-3'>
        <div className='flex items-center'>
          <p className='font-bold'>Ngày bắt đầu </p> -{' '}
          <p className='font-bold'>
            Ngày kết thúc <span className='text-danger-10'>*</span>
          </p>
        </div>

        <RangePicker
          className='mt-2 border border-gray-300 py-[0.8rem] px-2 outline-none'
          format={['DD/MM/YYYY', 'DD-MM-YYYY']}
          value={[dayjs(formValues.StartDay), dayjs(formValues.EndDay)]}
          placeholder={['Ngày bắt đầu', 'Ngày kết thúc']}
          onChange={handleChangeRangePicker}
          clearIcon={false}
        />
        {(!formValues.EndDay || !formValues.StartDay) && error && (
          <p className='text-danger-10'>Ngày đăng ký không được để trống</p>
        )}
      </div>

      <div className='mt-5 flex flex-col gap-1'>
        <label className='font-bold'>Tần suất nhận báo, tạp chí</label>

        <Radio onChange={handleChangeTermReciveMode} value={'day'} checked={termRecieveMode === 'day'}>
          Hàng tuần vào ngày
        </Radio>

        {termRecieveMode === 'day' && (
          <div className='ml-6'>
            <Checkbox
              onChange={handleChangeWeekday}
              value={'1'}
              checked={formValues.ListDay.includes('1')}
              className='pl-3'
            >
              Thứ 2
            </Checkbox>
            <Checkbox
              onChange={handleChangeWeekday}
              value={'2'}
              checked={formValues.ListDay.includes('2')}
              className='pl-3'
            >
              Thứ 3
            </Checkbox>
            <Checkbox
              onChange={handleChangeWeekday}
              value={'3'}
              checked={formValues.ListDay.includes('3')}
              className='pl-3'
            >
              Thứ 4
            </Checkbox>
            <Checkbox
              onChange={handleChangeWeekday}
              value={'4'}
              checked={formValues.ListDay.includes('4')}
              className='pl-3'
            >
              Thứ 5
            </Checkbox>
            <Checkbox
              onChange={handleChangeWeekday}
              value={'5'}
              checked={formValues.ListDay.includes('5')}
              className='pl-3'
            >
              Thứ 6
            </Checkbox>
            <Checkbox
              onChange={handleChangeWeekday}
              value={'6'}
              checked={formValues.ListDay.includes('6')}
              className='pl-3'
            >
              Thứ 7
            </Checkbox>
            <Checkbox
              onChange={handleChangeWeekday}
              value={'0'}
              checked={formValues.ListDay.includes('0')}
              className='pl-3'
            >
              Chủ Nhật
            </Checkbox>

            {!formValues.ListDay.length && error && (
              <p className='mt-1 text-danger-10'>Tần suất nhận báo, tạp chí không được để trống</p>
            )}
          </div>
        )}

        <Radio
          onChange={handleChangeTermReciveMode}
          value={'month'}
          checked={termRecieveMode === 'month'}
          className='mt-1'
        >
          Hàng tháng vào ngày
        </Radio>

        {termRecieveMode === 'month' && (
          <div>
            <div className='mt-1 ml-6 flex items-center gap-2'>
              {formValues.listInputs.map(({ id, value }, index) => {
                return (
                  <div className='relative' key={id} onClick={() => setActiveInput(id)}>
                    <InputNumber
                      className='rounded-md border border-gray-300 px-1 py-2 text-center text-black outline-none'
                      min={1}
                      max={31}
                      value={+value}
                      onChange={(value) => value && handleChangeDay(id, value)}
                      maxLength={2}
                    />

                    <button onClick={handleRemoveInput(index)} className={index === 0 ? 'hidden' : ''}>
                      <CloseCircleOutlined className='absolute -top-1 -right-1' />
                    </button>
                  </div>
                );
              })}

              <Button onClick={handleAddInput} className={formValues.ListDay.length === 3 ? 'hidden' : ''}>
                <PlusCircleOutlined />
              </Button>
            </div>

            {!formValues.listInputs.length && error && (
              <p className='mt-1 text-danger-10'>Tần suất nhận báo, tạp chí không được để trống</p>
            )}
          </div>
        )}
      </div>

      <div className='mt-5 flex flex-col'>
        <label className='font-bold'>Mỗi ngày nhập</label>

        <div className='ml-6 mt-3 flex items-center gap-1'>
          <InputNumber
            placeholder='Nhập số báo, tạp chí'
            className='rounded-md border border-gray-300 px-1 py-2 text-center text-black outline-none'
            value={formValues.SoLuong}
            min={1}
            max={9999}
            maxLength={4}
            onChange={(value) => value && handleChangeNumber(value)}
          />

          <span>quyển báo, tạp chí</span>
        </div>
      </div>
    </BaseModal>
  );
};

export default RegisterModal;
