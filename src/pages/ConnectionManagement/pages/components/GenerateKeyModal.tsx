/* eslint-disable react-hooks/exhaustive-deps */
import { useMutation, useQuery, type UseMutateAsyncFunction } from '@tanstack/react-query';
import { Modal, Radio, type ModalProps, type RadioChangeEvent } from 'antd';
import { connectionApis } from 'apis';
import { type AxiosResponse } from 'axios';
import { Button, Input, Title } from 'components';
import dayjs from 'dayjs';
import { useOutsideClick } from 'hooks';
import {
  memo,
  useCallback,
  useEffect,
  useMemo,
  useState,
  type ChangeEvent,
  type FormEvent,
  type MutableRefObject
} from 'react';
import { toast } from 'react-toastify';

type Props = {
  loading?: boolean;
  onClose: VoidFunction;
  onSuccess: UseMutateAsyncFunction<
    AxiosResponse<
      ResponseApi<{
        Key: Key[];
        Count: number;
      }>,
      any
    >,
    unknown,
    void,
    unknown
  >;
} & ModalProps;

const initialState: {
  memberUnitName: string;
  key: string;
  keyType: 'isLimitedTime' | 'unending';
  lifeDays: number;
  workingId: string;
} = {
  key: '',
  memberUnitName: '',
  keyType: 'isLimitedTime',
  lifeDays: 30,
  workingId: ''
};

const GenerateKey = (props: Props) => {
  const { open, loading, onClose, onSuccess, ...rest } = props;

  const [formValues, setFormValues] = useState(initialState);
  const [error, setError] = useState<boolean>(false);
  const [visible, setVisible] = useState<boolean>(false);

  const ref = useOutsideClick<HTMLDivElement>(useCallback(() => setVisible(false), []));

  const { data: listOfUnitsData, isLoading: loadingListOfUnits } = useQuery({
    queryKey: ['getListOfUnits'],
    queryFn: connectionApis.getListOfUnitDeparmentEdu,
    enabled: open,
    staleTime: Infinity
  });

  // generate new key
  const { mutate: generateKey, isLoading: loadingGenerateKey } = useMutation({
    mutationFn: connectionApis.generateKey,
    onSuccess: (data) => {
      setFormValues((prevState) => {
        return { ...prevState, key: data.data.Item.Key };
      });
    }
  });

  const { mutate: assignKey, isLoading: loadingAssignKey } = useMutation({
    mutationFn: connectionApis.assignKeyForSpecificUnit,
    onSuccess: (data) => {
      toast.success(`Tạo key mới thành công cho đơn vị ${formValues.memberUnitName}`);
      setFormValues(initialState);
      setError(false);
      onClose();
      onSuccess();
    }
  });

  const listOfUnits = listOfUnitsData?.data?.Item?.DonVi || [];

  const invalidExpectationDate = useMemo(() => {
    if (!formValues.lifeDays) return;

    return dayjs().add(formValues.lifeDays, 'day').endOf('day').format('HH:mm:ss DD/MM/YYYY');
  }, [formValues.lifeDays]);

  useEffect(() => {
    open && !formValues.key && generateKey();
  }, [open, formValues.key]);

  const handleChooseMemberUnit = (e: ChangeEvent<HTMLInputElement>) => {
    if (!e) return;

    const { value } = e.target;

    setFormValues((prevState) => {
      return {
        ...prevState,
        memberUnitName: value.trimStart()
      };
    });

    if (visible) {
      setVisible(false);
    }
  };

  const handleChangeKeyLifeTime = (e: RadioChangeEvent) => {
    if (!e) return;

    const { value } = e.target;

    setFormValues((prevState) => {
      return { ...prevState, keyType: value };
    });
  };

  const handleChangeLifeDays = (e: ChangeEvent<HTMLInputElement>) => {
    if (formValues.keyType !== 'isLimitedTime') return;

    setFormValues((prevState) => {
      return { ...prevState, lifeDays: Math.ceil(+e.target.value) };
    });
  };

  const handleChooseUnit = (unit: { name: string; workingId: string }) => {
    if (loadingListOfUnits) return;

    const { name, workingId } = unit;

    setFormValues((prevState) => {
      return { ...prevState, memberUnitName: name, workingId };
    });

    setVisible((prevState) => !prevState);
  };

  const handleToogleVisibleMemberUnits = () => {
    !loadingListOfUnits && setVisible((prevState) => !prevState);
  };

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const { key, memberUnitName, workingId } = formValues;

    const name = memberUnitName.replace(/ +(?= )/g, '').trimStart();

    if (!name.trim() || !key) {
      return setError(true);
    }

    assignKey({
      NameMember: name.trimEnd(),
      WorkingId: workingId,
      Key: key,
      ListTimePackages: [
        {
          ExtensionTime:
            formValues.keyType === 'unending'
              ? null
              : // convert to seconds
                formValues.lifeDays * 24 * 60 * 60
        }
      ],
      IsActive: true
    });
  };

  const handleClose = () => {
    setError(false);
    setFormValues(initialState);
    onClose();
  };

  return (
    <Modal open={open} {...rest} footer={null} closable={false} centered>
      <Title title='Thêm mới key' />

      <form onSubmit={handleSubmit}>
        <label className='mt-4 mb-2 block font-bold'>
          Tên đơn vị <span className='text-danger-10'>*</span>
        </label>

        <div className='relative' ref={ref as MutableRefObject<HTMLDivElement>}>
          <Input
            placeholder={` ${visible ? 'Chọn' : 'Nhập'} đơn vị`}
            containerClassName={`bg-white w-full border rounded-lg flex mb-1 px-2 py-3 items-center ${
              error && !formValues.memberUnitName.trim() ? 'border-danger-10' : ''
            }`}
            className='w-full outline-none'
            onChange={handleChooseMemberUnit}
            right={
              <button onClick={handleToogleVisibleMemberUnits} type='button'>
                <svg
                  width='12'
                  height='8'
                  viewBox='0 0 12 8'
                  fill='none'
                  xmlns='http://www.w3.org/2000/svg'
                  className={
                    visible
                      ? `rotate-180 transition-all duration-150 ease-in-out`
                      : 'rotate-0 transition-all duration-150 ease-in-out'
                  }
                >
                  <path d='M1.41 0L6 4.58L10.59 0L12 1.41L6 7.41L0 1.41L1.41 0Z' fill='black' fillOpacity='0.35' />
                </svg>
              </button>
            }
            value={formValues.memberUnitName}
          />
          <p className={`${error && !formValues.memberUnitName.trim() ? 'visible text-danger-10' : 'hidden'} `}>
            Đơn vị không được để trống
          </p>

          <ul
            className={`absolute z-10 flex list-none flex-col overflow-y-auto rounded-md bg-white shadow-md ${
              !visible
                ? 'invisible h-0 w-full transition-all duration-150 ease-linear'
                : 'max-h-[200px] w-full py-2 transition-all duration-150 ease-linear'
            }`}
          >
            {listOfUnits.map((unit, index) => (
              <li
                key={index}
                className='py-1 px-3 hover:cursor-pointer hover:rounded-md hover:bg-secondary-30/75'
                onClick={() => handleChooseUnit({ name: unit.label, workingId: unit.value + '' || '' })}
              >
                {unit.label}
              </li>
            ))}
          </ul>
        </div>

        <label className='mt-4 mb-2 block font-bold'>
          Key <span className='text-danger-10'>*</span>
        </label>

        <Input
          containerClassName='bg-white w-full border rounded-lg flex pl-1'
          className='w-full px-1 outline-none '
          right={
            <Button type='button' onClick={() => generateKey()} disabled={loadingListOfUnits || loadingGenerateKey}>
              <svg width='24' height='24' viewBox='0 0 43 43' fill='none' xmlns='http://www.w3.org/2000/svg'>
                <path
                  d='M12.835 22.1323H11.3823C11.3825 19.9003 12.0773 17.7238 13.3703 15.9046C14.6634 14.0853 16.4905 12.7137 18.5982 11.9799C20.7059 11.2461 22.9897 11.1866 25.1328 11.8096C27.2759 12.4326 29.172 13.7072 30.558 15.4566V13.8512H32.0107V18.9361H26.9262V17.4833H30.1837C29.3669 16.0705 28.1931 14.8973 26.78 14.0814C25.3669 13.2654 23.7641 12.8353 22.1323 12.8342C17.0057 12.8342 12.835 17.0053 12.835 22.1323ZM31.4296 22.1323C31.4296 27.2594 27.2589 31.4304 22.1323 31.4304C20.5006 31.4293 18.8978 30.9993 17.4847 30.1833C16.0715 29.3673 14.8977 28.1941 14.081 26.7814H17.3384V25.3286H12.2539V30.4135H13.7066V28.8081C15.0927 30.5575 16.9887 31.8321 19.1318 32.455C21.2749 33.078 23.5587 33.0185 25.6665 32.2847C27.7742 31.551 29.6013 30.1793 30.8943 28.3601C32.1873 26.5409 32.8821 24.3643 32.8823 22.1323H31.4296Z'
                  fill='white'
                />
              </svg>
            </Button>
          }
          defaultValue={formValues.key}
        />

        <label className='mt-4 mb-2 block font-bold'>
          Hiệu lực
          <span className='font ml-1 font-normal text-danger-10'>
            {formValues.keyType === 'unending' ? 'Vĩnh viễn' : `(Dự kiến hết hạn: ${invalidExpectationDate || '--'})`}
          </span>
        </label>

        <div className='flex items-center'>
          <Radio
            onChange={handleChangeKeyLifeTime}
            value={'isLimitedTime'}
            checked={formValues.keyType === 'isLimitedTime'}
          >
            Hết hạn sau
          </Radio>

          <div className='flex w-[90px] items-center rounded-lg border px-3'>
            <input
              className='w-[31px] outline-none'
              max={31}
              value={formValues.lifeDays}
              disabled={formValues.keyType !== 'isLimitedTime'}
              onKeyPress={(event) => {
                if (!/[0-9]/.test(event.key)) {
                  event.preventDefault();
                }
              }}
              maxLength={3}
              min={1}
              onChange={handleChangeLifeDays}
              onBlur={() => {
                if (!formValues.lifeDays) {
                  setFormValues((prevState) => {
                    return { ...prevState, lifeDays: 1 };
                  });
                }
              }}
            />

            <div className='rounded-br-md rounded-tr-md border-l border-l-secondary-10 bg-secondary-20/30 py-3 px-2'>
              <span className='text-xs'>Ngày</span>
            </div>
          </div>

          <span className='ml-1'>Kể từ ngày kết nối thành công</span>
        </div>

        <Radio
          onChange={handleChangeKeyLifeTime}
          className='mt-2'
          value={'unending'}
          checked={formValues.keyType === 'unending'}
        >
          Vĩnh viễn
        </Radio>

        <div className='mt-3 flex items-center justify-end gap-2'>
          <Button variant='secondary' loading={loading || loadingAssignKey} onClick={handleClose} type='button'>
            Quay về
          </Button>

          <Button loading={loading || loadingAssignKey} type='submit'>
            Tạo mới
          </Button>
        </div>
      </form>
    </Modal>
  );
};

export default memo(GenerateKey);
