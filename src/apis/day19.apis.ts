import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/Day19`;

type GetDay19Vars = {
  Keyword?: string;
  Sort: number;
};

const day19Apis = {
  getDay19List: (payload: GetDay19Vars) => {
    const { Keyword = '', Sort = 0 } = payload;

    return httpThuVien.post<ResponseApi<Day19[]>>(`${endPoint}/GetDataBySearch`, {
      Keyword,
      Sort
    });
  },

  getDay19: (id: string) => httpThuVien.get<ResponseApi<Day19>>(`${endPoint}/${id}`),
  deleteDay19: (id: string) => httpThuVien.delete<ResponseApi<{}>>(`${endPoint}/${id}`),
  createDay19: (payload: Omit<Day19, 'Id'>) => httpThuVien.post<ResponseApi<{}>>(`${endPoint}`, payload),
  updateDay19: (payload: Day19) => httpThuVien.put<ResponseApi<{}>>(`${endPoint}`, payload)
};

export default day19Apis;
