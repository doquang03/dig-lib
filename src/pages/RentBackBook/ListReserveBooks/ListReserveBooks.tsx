import { useMutation } from '@tanstack/react-query';
import { Table } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { bookReservingApis } from 'apis';
import classNames from 'classnames';
import { Button, Input, ModalDelete, Select, SizeChanger, Title, TitleDelete } from 'components';
import { LOCATION_READER_OPTIONS, TYPE_READER_OPTIONS } from 'constants/options';
import { path } from 'constants/path';
import dayjs from 'dayjs';
import { ChangeEvent, FormEvent, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getSerialNumber } from 'utils/utils';

const ListReserveBooks = () => {
  const [params, setParams] = useState<{
    sortName?: boolean;
    sortMemberCode?: boolean;
    sortClassGroup?: boolean;
    sortValidTime?: boolean;
    textSearch: string;
    userType: string;
    IsGlobal: string;
    page: number;
    pageSize: number;
  }>({
    textSearch: '',
    userType: '',
    page: 1,
    pageSize: 30,
    IsGlobal: ''
  });
  const [selectedRecord, setSelectedRecord] = useState<BookReserving>();

  const navigate = useNavigate();

  const {
    data: booksReservingData,
    isLoading: loadingBooksReserving,
    mutate
  } = useMutation({
    mutationFn: (vars: { textSearch?: string; userType?: string; IsGlobal?: boolean }) => {
      const req: Partial<{
        TextForSearch: string;
        LoaiTK: string;
        SortTenBanDoc: boolean;
        SortMaThanhVien: boolean;
        SortLopTo: boolean;
        SortThoiGianHetHieuLucGanNhat: boolean;
        IsGlobal?: boolean;
      }> = {};

      req.IsGlobal = vars.IsGlobal;

      if (vars.textSearch) {
        req.TextForSearch = vars.textSearch;
      }

      if (vars.userType) {
        req.LoaiTK = vars.userType;
      }

      if (params.sortName) {
        req.SortTenBanDoc = params.sortName;
      }

      if (params.sortClassGroup) {
        req.SortLopTo = params.sortClassGroup;
      }

      if (params.sortMemberCode) {
        req.SortMaThanhVien = params.sortMemberCode;
      }

      if (params.sortValidTime) {
        req.SortThoiGianHetHieuLucGanNhat = params.sortValidTime;
      }

      return bookReservingApis.getListOfBooksReserving({ page: params.page, pageSize: params.pageSize, ...req });
    }
  });

  const { mutate: cancelReservation, isLoading: loadingCancelResvervation } = useMutation({
    mutationFn: bookReservingApis.cancelReservation,
    onSuccess: () => {
      toast.success('Hủy phiếu thành công');

      setSelectedRecord(undefined);

      mutate({
        textSearch: params.textSearch,
        userType: params.userType,
        IsGlobal: params.IsGlobal === '1' ? true : params.IsGlobal === '0' ? false : undefined
      });
    }
  });

  useEffect(
    () =>
      mutate({
        textSearch: '',
        userType: ''
      }),
    []
  );

  const { List_OrderBooks, count } = booksReservingData?.data?.Item || { List_OrderBooks: [], count: 0 };

  const columns: ColumnsType<BookReserving> = [
    {
      key: 'STT',
      title: 'STT',
      render: (value, record, index) => getSerialNumber(params.page, params.pageSize, index)
    },
    {
      key: 'MaThanhVien',
      title: 'Mã bạn đọc',
      dataIndex: 'MaThanhVien'
    },
    {
      key: 'TenBanDoc',
      title: 'Tên bạn đọc',
      dataIndex: 'TenBanDoc',
      render: (value, record) => (
        <div
          className='italic text-primary-50 hover:cursor-pointer hover:underline'
          onClick={() => navigate(path.muonTraSach, { state: { idUserReserving: record.idMember } })}
        >
          {record.TenBanDoc}
        </div>
      ),
      onCell: (record) => ({
        className: 'text-left'
      })
    },
    {
      key: 'DonVi',
      title: 'Đơn vị',
      dataIndex: 'DonVi'
    },
    {
      key: 'SoLuong',
      title: 'Số lượng sách đặt mượn',
      dataIndex: 'SoLuong'
    },
    {
      key: 'ThoiGianHetHieuLucGanNhat',
      title: 'Thời gian hết hiệu lực gần nhất',
      dataIndex: 'ThoiGianHetHieuLucGanNhat',
      render: (value, record) => dayjs(record.ThoiGianHetHieuLucGanNhat).format('HH:mm DD/MM/YYYY')
    },
    {
      key: 'action',
      title: '',
      dataIndex: 'action',
      width: 200,
      render: (value, record) => (
        <center>
          <Button onClick={() => setSelectedRecord(record)} variant='danger'>
            <svg width='12' height='11' viewBox='0 0 12 11' fill='none' xmlns='http://www.w3.org/2000/svg'>
              <path
                d='M6.2257 0C3.12176 0 0.612793 2.4585 0.612793 5.5C0.612793 8.5415 3.12176 11 6.2257 11C9.32963 11 11.8386 8.5415 11.8386 5.5C11.8386 2.4585 9.32963 0 6.2257 0ZM9.03215 7.4745L8.24073 8.25L6.2257 6.2755L4.21066 8.25L3.41924 7.4745L5.43428 5.5L3.41924 3.5255L4.21066 2.75L6.2257 4.7245L8.24073 2.75L9.03215 3.5255L7.01712 5.5L9.03215 7.4745Z'
                fill='white'
              />
            </svg>
            Hủy đặt
          </Button>
        </center>
      )
    }
  ];

  const handleSubmitForm = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    mutate({
      textSearch: params.textSearch,
      userType: params.userType,
      IsGlobal: params.IsGlobal === '1' ? true : params.IsGlobal === '0' ? false : undefined
    });
  };

  const handleChangeTextSearch = (e: ChangeEvent<HTMLInputElement>) => {
    if (!e) return;

    const { value } = e.target;

    setParams((prevState) => {
      return {
        ...prevState,
        textSearch: value
      };
    });
  };

  const handleChangeUserType = (e: ChangeEvent<HTMLSelectElement>) => {
    if (!e) return;

    const { value } = e.target;

    setParams((prevState) => {
      return {
        ...prevState,
        userType: value
      };
    });
  };

  const handleChangeLocationType = (e: ChangeEvent<HTMLSelectElement>) => {
    if (!e) return;

    const { value } = e.target;

    setParams((prevState) => {
      return {
        ...prevState,
        IsGlobal: value
      };
    });
  };

  const handleRefresh = () => {
    setParams({
      textSearch: '',
      userType: '',
      page: 1,
      pageSize: 30,
      IsGlobal: ''
    });

    mutate({
      textSearch: '',
      userType: ''
    });
  };

  return (
    <div className='p-5'>
      <Title title='DANH SÁCH ĐẶT MƯỢN SÁCH ONLINE' />

      <form
        className='mt-3 flex flex-1 flex-col items-center gap-1 bg-primary-50/20 p-2.5 md:flex-row'
        onSubmit={handleSubmitForm}
      >
        <Input
          placeholder={'Nhập tên, mã bạn đọc'}
          containerClassName='w-1/4'
          onChange={handleChangeTextSearch}
          value={params.textSearch}
        />

        <Select
          items={TYPE_READER_OPTIONS}
          className='w-full md:w-1/4'
          onChange={handleChangeUserType}
          value={params.userType}
        />

        <Select
          items={LOCATION_READER_OPTIONS}
          className='w-full md:w-1/4'
          onChange={handleChangeLocationType}
          value={params.IsGlobal}
        />

        <Button variant='secondary' type='button' className='w-full md:w-auto' onClick={handleRefresh}>
          Làm mới
        </Button>

        <Button variant='default' type='submit' className='w-full md:w-auto'>
          Tìm kiếm
        </Button>
      </form>

      <Table
        columns={columns}
        dataSource={List_OrderBooks}
        className='custom-table mt-3'
        bordered
        loading={loadingBooksReserving}
        rowKey={(row) => row.MaThanhVien}
        pagination={{
          onChange(current, pageSize) {
            setParams((prevState) => {
              return {
                ...prevState,
                page: current,
                pageSize
              };
            });
          },
          pageSize: params.pageSize,
          hideOnSinglePage: true,
          showSizeChanger: false
        }}
      />

      <div
        className={classNames('relative', {
          'mt-[64px]': params.pageSize >= (count || 0)
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger visible={Boolean(count)} value={params.pageSize + ''} total={count + ''} />
        </div>
      </div>

      <ModalDelete
        open={Boolean(selectedRecord)}
        title={
          <TitleDelete firstText='Bạn có chắc chắn muốn hủy đặt mượn sách từ' secondText={selectedRecord?.TenBanDoc} />
        }
        handleOk={() => selectedRecord && cancelReservation(selectedRecord?.idMember)}
        handleCancel={() => setSelectedRecord(undefined)}
        loading={loadingCancelResvervation}
        labelOK='Hủy đặt mượn'
      />
    </div>
  );
};

export default ListReserveBooks;
