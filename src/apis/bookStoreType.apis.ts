import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/LoaiKho`;

const bookStoreTypeApis = {
  getBookStoreTypes: (payload: {
    TextForSearch?: string;
    page: string;
    sizeNumber: string;
    CachTangMaCBKS?: string;
  }) => {
    let stringQuery = `page=${payload.page}&sizeNumber=${payload.sizeNumber}`;

    payload?.TextForSearch && (stringQuery += `&TextForSearch=${payload?.TextForSearch?.replace(/\s\s+/g, ' ')}`);
    payload?.CachTangMaCBKS && (stringQuery += `&CachTangMaCBKS=${payload.CachTangMaCBKS}`);

    return httpThuVien.post<ResponseApi<{ ListKhoSach: BookStoreType[]; count: number; openRow: [] }>>(
      `${endPoint}/IndexByTree?${stringQuery}`
    );
  },
  getAllStores: () => httpThuVien.get<ResponseApi<BookStoreType[]>>(`${endPoint}/GetAll`),
  IndexByTreeCountBook: () =>
    httpThuVien.post<ResponseApi<{ ListKhoSach: BookStoreType[]; ListMaMau: Color[] }>>(
      `${endPoint}/IndexByTreeCountBook`
    ),
  createBookStoreSType: (payload: CreateBookStoreTypeVars) =>
    httpThuVien.post<ResponseApi<BookStoreType>>(`${endPoint}/TaoMoiKhoSach`, payload),
  deleteBookStore: (id: string) => httpThuVien.post<ResponseApi<BookStoreType>>(`${endPoint}/XoaKhoSach?Id=${id}`),
  EditByID: (id: string) => httpThuVien.post<ResponseApi<BookStoreType>>(`${endPoint}/EditByID?Id=${id}`)
};

export default bookStoreTypeApis;
