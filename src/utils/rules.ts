import { regex } from 'constants/regex';
import * as yup from 'yup';
import { mixed } from 'yup';

export const studentSchema = yup
  .object({
    Ten: yup
      .string()
      .trim()
      .required('Tên học sinh không được để trống')
      .max(255, 'Tên học sinh chứa tối đa 255 ký tự')
      .matches(regex.excludeNumber, 'Tên học sinh chỉ chứa chữ cái, ký tự đặc biệt')
      .nonNullable(),
    MaSoThanhVien: yup
      .string()
      .trim()
      .max(50, 'Mã học sinh chứa tối đa 50 ký tự')
      .matches(regex.excludeSpecialCharacters, {
        excludeEmptyString: true,
        message: 'Mã  học sinh chỉ chứa chữ số và chữ cái, không chứa ký tự đặc biệt'
      }),
    LopHoc: yup.string().trim().required('Lớp học không được để trống').max(30, 'Lớp học chứa tối đa 30 ký tự'),
    DiaChi: yup.string().trim().max(2000, 'Địa chỉ chứa tối đa 2000 ký tự').notRequired(),
    ThoiHanThe: yup.string().trim().required('Thời hạn thẻ không được để trống'),
    NgaySinh: yup.string().trim().required('Ngày sinh không được để trống'),
    GioiTinh: yup.string().trim().required('Giới tính không được để trống'),
    NienKhoa: yup.string().trim().required('Năm học không được để trống'),
    SDT: yup.string().trim().matches(regex.vietnamesePhoneNumber, 'Số điện thoại không hợp lệ').notRequired(),
    TrangThai: yup.string().trim().notOneOf([''], 'Tình trạng không được để trống')
  })
  .required();

export const teacherSchema = yup
  .object({
    name: yup
      .string()
      .trim()
      .required('Tên giáo viên không được để trống')
      .max(255, 'Tên giáo viên chứa tối đa 255 ký tự')
      .matches(regex.excludeNumber, 'Tên giáo viên chỉ chứa chữ cái, ký tự đặc biệt')
      .nonNullable(),
    id: yup.string().trim().max(50, 'Mã giáo viên chứa tối đa 50 ký tự').matches(regex.excludeSpecialCharacters, {
      excludeEmptyString: true,
      message: 'Mã giáo viên chỉ chứa chữ cái và chữ số, không chứa ký tự đặc biệt'
    }),
    organization: yup
      .string()
      .trim()
      .required('Tổ không được để trống')
      .max(30, 'Tổ chứa tối đa 30 ký tự')
      .nonNullable(),
    address: yup.string().trim().max(2000, 'Địa chỉ chứa tối đa 2000 ký tự'),
    dateOfBirth: yup.string().trim().required('Ngày sinh không được để trống'),
    gender: yup.string().trim().required('Giới tính không được để trống'),
    phone: yup.string().trim().nullable().matches(regex.vietnamesePhoneNumber, 'Số điện thoại không hợp lệ'),
    status: yup.string().trim().notOneOf([''], 'Tình trạng không được để trống'),
    advance: yup.bool(),
    avatar: mixed(),
    classify: yup.string().trim().required('Phân loại không được để trống'),
    workYear: yup.string().trim().required('Năm công tác không được để trống'),
    qualification: yup
      .string()
      .trim()
      .required('Trình độ chuyên môn không được để trống')
      .max(20, 'Trình độ chuyên môn chứa tối đa 20 ký tự'),
    email: yup
      .string()
      .trim()
      .nullable()
      .max(255, 'Email chứa tối đa 255 ký tự')
      .matches(regex.emailAccess, 'Địa chỉ email chưa đúng định dạng')
  })
  .required();

export const teacherSchemaCollapse = yup
  .object({
    name: yup
      .string()
      .trim()
      .required('Tên giáo viên không được để trống')
      .max(255, 'Tên giáo viên chứa tối đa 255 ký tự')
      .matches(regex.excludeNumber, 'Tên giáo viên chỉ chứa chữ cái, ký tự đặc biệt')
      .nonNullable(),
    id: yup.string().trim().max(50, 'Mã giáo viên chứa tối đa 50 ký tự').matches(regex.excludeSpecialCharacters, {
      excludeEmptyString: true,
      message: 'Mã giáo viên chỉ chứa chữ cái và chữ số, không chứa ký tự đặc biệt'
    }),
    organization: yup
      .string()
      .trim()
      .required('Tổ không được để trống')
      .max(30, 'Tổ chứa tối đa 30 ký tự')
      .nonNullable(),
    address: yup.string().trim().max(2000, 'Địa chỉ chứa tối đa 2000 ký tự'),
    dateOfBirth: yup.string().trim().required('Ngày sinh không được để trống'),
    gender: yup.string().trim().required('Giới tính không được để trống'),
    phone: yup.string().trim().nullable().matches(regex.vietnamesePhoneNumber, 'Số điện thoại không hợp lệ'),
    status: yup.string().trim().notOneOf([''], 'Tình trạng không được để trống'),
    advance: yup.bool(),
    avatar: mixed()
  })
  .required();

export const teacherSchemaAdvance = yup
  .object({
    classify: yup.string().trim().required('Phân loại không được để trống'),
    workYear: yup.string().trim().required('Năm công tác không được để trống'),
    qualification: yup
      .string()
      .trim()
      .required('Trình độ chuyên môn không được để trống')
      .max(20, 'Trình độ chuyên môn chứa tối đa 20 ký tự'),
    email: yup
      .string()
      .trim()
      .nullable()
      .max(255, 'Email chứa tối đa 255 ký tự')
      .matches(regex.emailAccess, 'Địa chỉ email chưa đúng định dạng')
  })
  .required();

export const collectionSchema = yup
  .object({
    name: yup
      .string()
      .trim()
      .required('Tên bộ sưu tập không được để trống')
      .max(255, 'Tên bộ sưu tập chứa tối đa 255 ký tự')
      .nonNullable()
  })
  .required();

export const topicSchema = yup
  .object({
    Ten: yup
      .string()
      .trim()
      .required('Tên chủ điểm không được để trống')
      .max(255, 'Tên chủ điểm chứa tối đa 255 ký tự')
      .nonNullable()
  })
  .required();

export const colorSchema = yup.object({
  TenMau: yup.string().trim().required('Tên màu không được để trống').max(255, 'Tên màu phải dưới 255 ký tự'),
  IdLienKet: yup.string().trim().required('')
});

export const publishingCompanySchema = yup
  .object({
    name: yup
      .string()
      .trim()
      .required('Tên nhà xuất bản không được để trống')
      .matches(regex.excludeNumber, 'Tên nhà xuất bản không được nhập chữ số')
      .max(255, 'Tên nhà xuất bản chứa tối đa 255 ký tự')
      .nonNullable(),
    note: yup.string().trim()
  })
  .required();

export const placePublicationSchema = yup
  .object({
    name: yup
      .string()
      .trim()
      .required('Tên nơi xuất bản không được để trống')
      .matches(regex.excludeNumber, 'Tên nơi xuất bản không được nhập chữ số')
      .max(255, 'Tên nơi xuất bản chứa tối đa 255 ký tự')
      .nonNullable(),
    shortname: yup.string().trim().max(100, 'Tên viết tắt chứa tối đa 100 ký tự').trim()
  })
  .required();

export const authorSchema = yup
  .object({
    TenTacGia: yup
      .string()
      .trim()
      .required('Tên tác giả không được để trống')
      .matches(regex.excludeNumber, 'Tên tác giả không được nhập chữ số')
      .max(255, 'Tên tác giả chứa tối đa 255 ký tự')
      .nonNullable(),
    MaTacGia: yup
      .string()
      .trim()
      .required('Mã tác giả không được để trống')
      .max(100, 'Mã tác giả chứa tối đa 100 ký tự'),
    QuocTich: yup.string().trim().max(100, 'Quốc tịch chứa tối đa 100 ký tự.'),
    MoTa: yup.string().trim().max(255, 'Mô tả chứa tối đa 255 ký tự')
  })
  .required();

export const languageSchema = yup
  .object({
    name: yup
      .string()
      .trim()
      .required('Tên ngôn ngữ không được để trống')
      .matches(regex.excludeNumber, 'Tên ngôn ngữ không được nhập chữ số')
      .max(255, 'Tên ngôn ngữ chứa tối đa 255 ký tự')
      .nonNullable(),
    shortname: yup.string().trim()
  })
  .required();

export const subjectSchema = yup
  .object({
    Ten: yup
      .string()
      .trim()
      .required('Tên môn học không được để trống')
      .max(255, 'Tên môn học chứa tối đa 255 ký tự')
      .nonNullable(),
    MaMonHoc: yup
      .string()
      .trim()
      .required('Mã môn học không được để trống')
      .max(100, 'Mã môn học chứa tối đa 100 ký tự'),
    KhoiLop: yup.array().min(1, 'Khối lớp không được để trống')
  })
  .required();

export const typeBookSchema = yup
  .object({
    TenTheLoai: yup
      .string()
      .trim()
      .required('Tên thể loại không được để trống')
      .max(255, 'Tên thể loại chứa tối đa 255 ký tự'),
    MaTheLoai: yup
      .string()
      .trim()
      .required('Mã thể loại không được để trống không được để trống')
      .max(50, 'Mã thể loại chứa tối đa 50 ký tự'),
    MoTa: yup.string().trim()
  })
  .required();

export const bookshelfSchema = yup.object({
  TenKe: yup
    .string()
    .trim()
    .required('Tên kệ sách không được để trống')
    .max(255, 'Tên kệ sách chứa tối đa 255 ký tự')
    .nonNullable(),
  ViTri: yup
    .string()
    .trim()
    .required('Vị trí kệ sách không được để trống')
    .max(255, 'Vị trí kệ sách chứa tối đa 255 ký tự')
    .nonNullable(),
  GhiChu: yup.string().notRequired().max(255, 'Mô tả chứa tối đa 255 ký tự').nonNullable()
});

export const statusBookSchema = yup.object({
  TenTT: yup
    .string()
    .trim()
    .required('Tên tình trạng sách không được để trống')
    .max(255, 'Tên tình trạng sách chứa tối đa 255 ký tự')
    .nonNullable()
});

export const supplierSchema = yup.object({
  TenNguonCungCap: yup
    .string()
    .trim()
    .required('Tên nguồn cung cấp không được để trống')
    .max(255, 'Tên nguồn cung cấp phải dưới 255 ký tự')
    .matches(regex.excludeNumber, 'Tên nguồn cung cấp không được nhập chữ số')
    .nonNullable(),
  Code: yup
    .string()
    .trim()
    .required('Mã nguồn cung cấp không được để trống')
    .max(255, 'Vị trí nguồn cung cấp chứa tối đa 255 ký tự')
    .nonNullable()
});

export const ddcSchema = yup.object({
  Ten: yup
    .string()
    .trim()
    .required('Tên DDC không được để trống')
    .max(255, 'Tên DDC chứa tối đa 255 ký tự')
    .nonNullable(),
  MaDDC: yup
    .string()
    .trim()
    .required('Mã DDC không được để trống')
    .matches(regex.excludeString, 'Mã DDC chỉ chứa chữ số, ký tự đặc biệt')
    .max(255, 'Mã DDC chứa tối đa dưới 255 ký tự')
    .nonNullable()
});

export const day19Schema = yup.object({
  Ten: yup
    .string()
    .trim()
    .required('Tên 19 dãy không được để trống')
    .max(255, 'Tên 19 dãy chứa tối đa 255 ký tự')
    .nonNullable(),
  MaDay19: yup
    .string()
    .trim()
    .required('Mã 19 dãy không được để trống')
    .max(255, 'Mã 19 dãy chứa tối đa dưới 255 ký tự')
    .nonNullable()
});

export const settingSchema = yup.object({
  TenThuVien: yup
    .string()
    .trim()
    .required('Tên thư viện không được để trống')
    .max(255, 'Tên thư viện chứa tối đa 255 ký tự')
    .nonNullable(),
  TenMien: yup
    .string()
    .trim()
    .required('Tên miền không được để trống')
    .max(255, 'Tên miền chứa tối đa 255 ký tự')
    .nonNullable(),
  DiaChi: yup.string().trim().required('Địa chỉ không được bỏ trống').max(255, 'Địa chỉ chứa tối đa 255 ký tự'),
  TheHeader1: yup
    .string()
    .trim()
    .max(255, 'Thông tin Sở phòng chứa tối đa 255 ký tự')
    .required('Thông tin Sở phòng không được để trống')
    .nonNullable(),
  TheHeader2: yup
    .string()
    .trim()
    .max(255, 'Thông tin trường chứa tối đa 255 ký tự.')
    .required('Thông tin trường không được để trống')
    .nonNullable(),
  SoNgayMuon: yup.number().required('Không được để trống!').min(1, 'Tối thiểu là 1 ngày'),
  SoNgayMuonGiaoVien: yup.number().required('Không được để trống!').min(1, 'Tối thiểu là 1 ngày'),
  SoNgayMuonMax: yup.number().required('Không được để trống!').min(1, 'Tối thiểu là 1 ngày'),
  SoNgayMuonGiaoVienMax: yup.number().required('Không được để trống!').min(1, 'Tối thiểu là 1 ngày'),
  NgaySachMoi: yup.string().trim(),
  ThangSachMoi: yup.string().trim(),
  SLSachDuocDatMuon: yup.number().required('Không được để trống!').min(1, 'Tối thiểu là 1 sách'),
  ThoiGianHieuLucDatSach: yup.number().required('Không được để trống!').min(1, 'Tối thiểu là 1 giờ'),
  SDT: yup.string().trim().notRequired(),
  Email: yup
    .string()
    .trim()
    .nullable()
    .max(255, 'Email chứa tối đa 255 ký tự')
    .matches(regex.emailAccess, 'Địa chỉ email chưa đúng định dạng')
});

export const bookSchema = yup.object({
  TenSach: yup
    .string()
    .trim()
    .required('Tên đầu sách không được để trống')
    .max(255, 'Tên đầu sách chứa tối đa 255 ký tự'),
  TenSongNgu: yup
    .string()
    .trim()
    .notRequired()
    .notOneOf([yup.ref('TenSach')], 'Tên song ngữ không thể trùng với tên đầu sách'),

  MaKiemSoat: yup.string().trim(),

  IdBoSuuTap: yup.string().trim().required('Bộ sưu tập không được để trống'),
  IdKeSach: yup.string().trim().notRequired(),
  IdNhaXuatBan: yup.string().trim().notRequired(),
  IdNgonNgu: yup.string().trim().notRequired(),
  IdNoiXuatBan: yup.string().trim().notRequired(),
  IdThuMucSach: yup.string().trim().notRequired(),
  IdMaMau: yup.string().trim().notRequired(),

  ISBN: yup.string().trim().notRequired(),
  ISSN: yup.string().trim().notRequired(),
  DDC: yup.string().trim().notRequired(),
  LLC: yup.string().trim().notRequired(),
  Day19: yup.string().trim().notRequired(),
  SKU: yup.string().trim().notRequired(),

  NamXuatBan: yup.string().trim().required('Năm xuất bản không được để trống'),
  TaiBan: yup.string().trim().notRequired(),
  XuatXu: yup.string().trim().notRequired(),
  SoTrang: yup.string().trim().notRequired(),
  MoTaVatLy: yup.string().trim().notRequired(),
  NguoiBienDich: yup.string().trim().notRequired(),
  TomTat: yup.string().trim().notRequired(),
  TaiLieuDinhKem: yup.string().trim().notRequired(),
  CongKhai: yup.string().trim().notRequired(),

  ListTacGiaJson: yup.array(),
  ListTacGiaPhuJson: yup.array(),

  GiaBia: yup.string().trim().notRequired(),
  PhiMuonSach: yup.string().trim().notRequired(),
  SoLuongTong: yup.string().trim().notRequired(),
  SoLuongConLai: yup.string().trim().notRequired(),
  SoLanDuocMuon: yup.string().trim().notRequired(),
  LinkBiaSach: yup.string().trim().notRequired(),
  MARC21: yup.string().trim().notRequired(),
  QRlink: yup.string().trim().notRequired(),
  QRData: yup.string().trim().notRequired(),

  IdMonHoc: yup.string().trim().notRequired(),
  IdChuDiem: yup.string().trim().notRequired(),
  IdKhoiLop: yup.array().notRequired(),
  TungThu: yup.string().trim().notRequired(),
  PhuChu: yup.string().trim().notRequired(),

  CoSach: yup.string().notRequired(),

  MaSach_BoGiaoDucCap: yup.string().trim().notRequired(),

  MinhHoa: yup.string().trim().notRequired(),

  NhapKho: yup.array().of(
    yup.object().shape({
      KhoSach: yup.string().trim().notOneOf([''], 'Kho sách không được để trống'),
      SoLuong: yup.string().notOneOf(['', '0'], 'Số lượng không được để trống'),
      TrangThai: yup.string().trim().notOneOf([''], 'Tình trạng không được để trống'),
      NguonCungCap: yup.string().trim().notOneOf([''], 'Nguồn cung cấp không được để trống'),
      SoCT: yup.string().trim().notRequired(),
      VSTQ: yup.string().trim().notRequired(),
      NgayNhap: yup.string().trim().notOneOf([''], 'Ngày nhập không được để trống')
    })
  ),
  GhiChu: yup.string().trim().notRequired()
});

export const documentFolderSchema = yup.object({
  TenThuMuc: yup
    .string()
    .trim()
    .required('Tên thư mục không được để trống')
    .max(255, 'Tên thư mục chứa tối đa 255 ký tự')
    .nonNullable(),
  MaThuMuc: yup
    .string()
    .trim()
    .required('Mã thư mục không được để trống')
    .max(100, 'Mã thư mục chứa tối đa 100 ký tự')
    .nonNullable(),
  Type: yup.string().trim().required('Loại tài liệu không được để trống').nonNullable(),
  IsActive: yup.boolean()
});

export const updateSachCaBietSchema = yup.object({
  IdTrangThai: yup.string().trim().required('Tình trạng không được để trống'),
  IdNguonCungCap: yup.string().trim().required('Nguồn cung cấp không được để trống'),
  NgayVaoSo_String: yup.string().trim().required('Ngày vào sổ không được để trống')
});

export const taoPhieuNhapSach = yup.object({
  KhoSach: yup.string().trim().notOneOf([''], 'Kho sách không được để trống'),
  SoLuong: yup.string().required('Số lượng không được để trống'),
  TrangThai: yup.string().trim().notOneOf([''], 'Tình trạng không được để trống'),
  NguonCungCap: yup.string().trim().notOneOf([''], 'Nguồn cung cấp không được để trống'),
  NgayNhap: yup.string().trim().notOneOf([''], 'Ngày nhập không được để trống')
});

export const bookstoreTypeSchema = yup.object({
  Ten: yup.string().trim().required('Tên kho sách không được để trống'),
  MaKho: yup.string().required('Mã kho sách không được để trống'),
  CachTangMaCB: yup.string().required('Cách tăng mã cá biệt không được để trống')
});

export const addReceiptSchema = yup.object({
  GhiChu: yup.string().trim().notRequired(),
  sachCaBiet: yup.array().of(
    yup.object().shape({
      KhoSach: yup.string().trim().notOneOf([''], 'Kho sách không được để trống'),
      SoLuong: yup.string().required('Số lượng không được để trống'),
      TrangThai: yup.string().trim().notOneOf([''], 'Tình trạng không được để trống'),
      NguonCungCap: yup.string().trim().notOneOf([''], 'Nguồn cung cấp không được để trống'),
      SoCT: yup.string().trim().notRequired(),
      VSTQ: yup.string().trim().notRequired(),
      NgayNhap: yup.string().trim().notOneOf([''], 'Ngày nhập không được để trống')
    })
  )
});

export const taoSoNhapKhoSchema = yup.object({
  GhiChu: yup.string().trim().notRequired(),
  KhoSach: yup.string().trim().notOneOf([''], 'Kho sách không được để trống'),
  NguonCungCap: yup.string().trim().notOneOf([''], 'Nguồn cung cấp không được để trống'),
  NgayNhap: yup.string().trim().required('Ngày nhập không được để trống'),
  VSTQ: yup.string().trim().notRequired(),
  SoCT: yup.string().trim().notRequired(),
  trangThai: yup.array().of(
    yup.object().shape({
      TrangThai: yup.string().trim().notOneOf([''], 'Tình trạng không được để trống')
    })
  )
});

export const createExportSchema = yup.object({
  GhiChu: yup.string().trim().required('Lý do không được để trống'),
  NgayXuatKho: yup.string().trim().required('Ngày xuất kho không được để trống')
});

export const MaSachCaBiet = yup.object({
  MaCBMoi: yup
    .string()
    .trim()
    .required('Mã sách cá biệt không được để trống')
    .max(5, 'Mã số cá biệt chứa tối đa 5 ký tự')
});

export const HuongDanSuDung = yup.object({
  Ten: yup
    .string()
    .trim()
    .required('Tên hướng dẫn sử dụng không được để trống')
    .max(255, 'Tên hướng dẫn sử dụng chứa tối đa 255 ký tự')
    .nonNullable()
});

export const rentBackSchema = yup.object({
  rentBack: yup.array().of(
    yup.object().shape({
      NgayTra: yup.string().trim().notOneOf([''], 'Ngày trả không được để trống'),
      NgayMuon: yup.string().trim().notOneOf([''], 'Ngày mượn không được để trống')
    })
  )
});

export const returnExtendSchema = yup.object({
  status: yup.string().trim().notOneOf([''], 'Tình trạng không được để trống'),
  returnExtendDate: yup.string().trim().required('Ngày trả không được để trống')
});

export const returnSchema = yup.object({
  status: yup.string().trim().notOneOf([''], 'Tình trạng không được để trống')
});

export const returnDateNumberSchema = yup.object({
  returnDateNumber: yup.string().trim().notOneOf([''], 'Số ngày trả không được để trống')
});

export const autditSchema = yup.object({
  ten: yup.string().trim().notOneOf([''], 'Tên phiếu kiểm kê không được để trống'),
  createDateReport: yup.string().trim().notOneOf([''], 'Ngày lập phiếu không được để trống'),
  createDateFinalize: yup.string().trim().notOneOf([''], 'Ngày chốt số liệu không được để trống')
});

export const updateCollectionAndFolderSchema = yup.object({
  updateCollection: yup.string().trim().notOneOf([''], 'Ngày lập phiếu không được để trống'),
  updateFolderDocument: yup.string().trim().notOneOf([''], 'Ngày chốt số liệu không được để trống')
});

export const newspaperMagazineSchema = (type: 'newspaper' | 'magazine') => {
  let _type = '';

  switch (type) {
    case 'newspaper':
      _type = 'báo';
      break;

    case 'magazine':
      _type = 'tạp chí';
      break;
    default:
      break;
  }

  return yup.object({
    code: yup.string().trim().notOneOf([''], `Mã ${_type} không được để trống`),
    name: yup.string().trim().notOneOf([''], `Tên ${_type} không được để trống`)
  });
};
