import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { bookshelfApis } from 'apis';
import { AxiosError } from 'axios';
import { Button, Input, Title } from 'components';
import { path } from 'constants/path';
import { useEffect, useMemo } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { bookshelfSchema } from 'utils/rules';

const initalFormValues: Omit<Bookshelf, 'Id'> = {
  CreateDateTime: '',
  GhiChu: '',
  TenKe: '',
  ViTri: ''
};

const AddBookshelfPage = () => {
  const { id } = useParams();

  const navigate = useNavigate();

  const queryClient = useQueryClient();

  const {
    register,
    handleSubmit,
    setValue,
    setError,
    formState: { errors }
  } = useForm<typeof initalFormValues>({
    defaultValues: initalFormValues,
    resolver: yupResolver(bookshelfSchema)
  });

  const { data: bookshelfData, refetch } = useQuery({
    queryKey: ['bookshelf', id],
    queryFn: () => bookshelfApis.getBookshelf(id + ''),
    enabled: !!id,
    onError: () => {
      navigate(path.kesach);
    }
  });

  const bookshelf = useMemo(() => {
    if (!bookshelfData?.data.Item) {
      return;
    }

    const { CreateDateTime, Id, GhiChu, TenKe, ViTri } = bookshelfData.data.Item;
    return { CreateDateTime, Id, GhiChu, TenKe, ViTri };
  }, [bookshelfData?.data.Item]);

  const { mutate: createBookshelf, isLoading: loadingCreateBookshelf } = useMutation({
    mutationFn: bookshelfApis.createBookshelf,
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ['bookshelfs'] });
      toast.success('Tạo kệ sách thành công');
      navigate(path.kesach);
    },
    onError: (error: AxiosError<ResponseApi<{}>>) => {
      setError('TenKe', { type: 'validate', message: error?.response?.data?.Message });
    }
  });

  const { mutate: updateBookshelf, isLoading: loadingUpdateBookshelf } = useMutation({
    mutationFn: bookshelfApis.updateBookshelf,
    onSuccess: () => {
      refetch();
      toast.success('Cập nhật kệ sách thành công');
    }
  });

  useEffect(() => {
    if (Boolean(id)) {
      setValue('TenKe', bookshelf?.TenKe + '');
      bookshelf?.ViTri && setValue('ViTri', bookshelf.ViTri);
      bookshelf?.GhiChu && setValue('GhiChu', bookshelf.GhiChu);
    }
  }, [bookshelf?.GhiChu, bookshelf?.TenKe, bookshelf?.ViTri, id, setValue]);

  const onSubmit = handleSubmit((data) => {
    let { TenKe, ViTri } = data;

    TenKe = TenKe.replace(/ +(?= )/g, '');
    ViTri = ViTri.replace(/ +(?= )/g, '');

    setValue('TenKe', TenKe);
    setValue('ViTri', ViTri);

    if (!Boolean(id)) {
      !loadingCreateBookshelf &&
        createBookshelf({
          TenKe,
          ViTri,
          GhiChu: data.GhiChu,
          CreateDateTime: new Date().toISOString()
        });
    } else {
      !loadingUpdateBookshelf &&
        updateBookshelf({
          TenKe,
          ViTri,
          GhiChu: data.GhiChu,
          Id: bookshelf?.Id + '',
          CreateDateTime: bookshelf?.CreateDateTime + ''
        });
    }
  });

  const handleBack = () => navigate(path.kesach);

  return (
    <div className='p-5'>
      <Title title={Boolean(id) ? 'Cập nhật kệ sách' : 'Thêm mới kệ sách'} />

      <form className='w-1/2 px-3' onSubmit={onSubmit}>
        <label className='mt-3 font-bold'>
          Tên kệ sách <span className='text-danger-10'>*</span>
        </label>
        <Input
          placeholder='Nhập tên kệ sách'
          name='TenKe'
          register={register}
          errorMessage={errors?.TenKe?.message}
          maxLength={256}
        />

        <label className='mt-4 font-bold'>
          Vị trí kệ sách <span className='text-danger-10'>*</span>
        </label>
        <Input
          placeholder='Nhập vị trí kệ sách'
          name='ViTri'
          register={register}
          errorMessage={errors?.ViTri?.message}
          maxLength={256}
        />

        <label className='mt-3 font-bold'>Mô tả</label>
        <Input
          component={'textarea'}
          placeholder='Nhập nội dung mô tả'
          name='GhiChu'
          register={register}
          errorMessage={errors?.GhiChu?.message}
          maxLength={256}
        />

        <div className='flex items-center justify-end gap-2'>
          <Button variant='secondary' onClick={handleBack} type='button'>
            Quay về
          </Button>

          <Button variant='default' type='submit' disabled={loadingCreateBookshelf || loadingUpdateBookshelf}>
            {Boolean(id) ? 'Cập nhật' : 'Thêm mới'}
          </Button>
        </div>
      </form>
    </div>
  );
};

export default AddBookshelfPage;
