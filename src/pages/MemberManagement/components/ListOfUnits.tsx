import { useQuery } from '@tanstack/react-query';
import { connectionApis } from 'apis';
import classNames from 'classnames';
import { Input } from 'components';
import { useQueryParams } from 'hooks';
import { ChangeEvent, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

type Props = {
  title: string;
  isPassive?: boolean;
  isProative?: boolean;
};

const ListOfUnits = (props: Props) => {
  const { title, isPassive, isProative } = props;

  const navigate = useNavigate();

  const { unit, workingId } = useQueryParams();

  const { data: listOfUnitsPermission, isLoading: loadingListOfUnitsPermission } = useQuery({
    queryKey: ['getDetailsUnitPermission'],
    queryFn: () => connectionApis.getDetailsUnitPermission({ Id: workingId }),
    onSuccess: (data) => {
      setUnits(data.data.Item.ListModel);
    }
  });

  useEffect(() => {
    if (!unit && !workingId) {
      navigate(-1);
    }
  }, [navigate, unit, workingId]);

  const [units, setUnits] = useState<UnitPermission[]>([]);

  const handleSearch = (e: ChangeEvent<HTMLInputElement>) => {
    if (!e) return;

    const { value } = e.target;

    const list = listOfUnitsPermission?.data.Item.ListModel || [];

    const listFiltered = list.filter((unit) =>
      unit.MemberName.toLocaleLowerCase().trim().includes(value.toLocaleLowerCase().trim())
    );

    setUnits(listFiltered);
  };

  return (
    <div>
      <div className='boder-[1px] h-[500px] rounded-md border shadow-md'>
        <div className='rounded-tl-md rounded-tr-md bg-primary-10 p-1 font-bold text-white'>{title}</div>

        <div className='h-[450px] overflow-y-auto p-2'>
          <div className='flex items-center gap-2'>
            <p className='flex-shrink-0 font-bold'>Tìm kiếm</p>

            <Input
              containerClassName='flex items-center w-full relative'
              placeholder='Nhập tên, mã thư viện cần tìm'
              right={
                <button
                  className={classNames('absolute right-1 bg-white transition-all delay-100 duration-200 ease-linear')}
                >
                  <svg width='18' height='18' viewBox='0 0 18 18' fill='none' xmlns='http://www.w3.org/2000/svg'>
                    <path
                      d='M12.5 11H11.71L11.43 10.73C12.41 9.59 13 8.11 13 6.5C13 2.91 10.09 0 6.5 0C2.91 0 0 2.91 0 6.5C0 10.09 2.91 13 6.5 13C8.11 13 9.59 12.41 10.73 11.43L11 11.71V12.5L16 17.49L17.49 16L12.5 11ZM6.5 11C4.01 11 2 8.99 2 6.5C2 4.01 4.01 2 6.5 2C8.99 2 11 4.01 11 6.5C11 8.99 8.99 11 6.5 11Z'
                      fill='#A7A7A7'
                    />
                  </svg>
                </button>
              }
              onChange={handleSearch}
            />
          </div>

          {loadingListOfUnitsPermission ? (
            <p>Đang tải dữ liệu</p>
          ) : (
            <ul className='my-3 list-none'>
              {units.map((unit, index) => (
                <li className='my-1 flex items-center gap-1 truncate' key={index}>
                  {isPassive && (
                    <>
                      {unit.Passive ? (
                        <svg width='18' height='18' viewBox='0 0 18 18' fill='none' xmlns='http://www.w3.org/2000/svg'>
                          <path
                            d='M16 0H2C0.89 0 0 0.9 0 2V16C0 17.1 0.89 18 2 18H16C17.11 18 18 17.1 18 16V2C18 0.9 17.11 0 16 0ZM7 14L2 9L3.41 7.59L7 11.17L14.59 3.58L16 5L7 14Z'
                            fill='#119757'
                          />
                        </svg>
                      ) : (
                        <svg width='18' height='18' viewBox='0 0 18 18' fill='none' xmlns='http://www.w3.org/2000/svg'>
                          <rect width='18' height='18' rx='2' fill='#FF1616' />
                          <path
                            d='M14 5.00714L12.9929 4L9 7.99286L5.00714 4L4 5.00714L7.99286 9L4 12.9929L5.00714 14L9 10.0071L12.9929 14L14 12.9929L10.0071 9L14 5.00714Z'
                            fill='white'
                          />
                        </svg>
                      )}
                    </>
                  )}

                  {isProative && (
                    <>
                      {unit.Proactive ? (
                        <svg width='18' height='18' viewBox='0 0 18 18' fill='none' xmlns='http://www.w3.org/2000/svg'>
                          <path
                            d='M16 0H2C0.89 0 0 0.9 0 2V16C0 17.1 0.89 18 2 18H16C17.11 18 18 17.1 18 16V2C18 0.9 17.11 0 16 0ZM7 14L2 9L3.41 7.59L7 11.17L14.59 3.58L16 5L7 14Z'
                            fill='#119757'
                          />
                        </svg>
                      ) : (
                        <svg width='18' height='18' viewBox='0 0 18 18' fill='none' xmlns='http://www.w3.org/2000/svg'>
                          <rect width='18' height='18' rx='2' fill='#FF1616' />
                          <path
                            d='M14 5.00714L12.9929 4L9 7.99286L5.00714 4L4 5.00714L7.99286 9L4 12.9929L5.00714 14L9 10.0071L12.9929 14L14 12.9929L10.0071 9L14 5.00714Z'
                            fill='white'
                          />
                        </svg>
                      )}
                    </>
                  )}

                  {unit.MemberName}
                </li>
              ))}
            </ul>
          )}
        </div>
      </div>
    </div>
  );
};

export default ListOfUnits;
