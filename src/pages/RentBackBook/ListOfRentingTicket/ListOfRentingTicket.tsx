import { PrinterFilled } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { DatePicker, Table, Tooltip } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { rentBack, settingApis } from 'apis';
import classNames from 'classnames';
import { Button, Empty, Input, Select, SizeChanger, Title } from 'components';
import dayjs from 'dayjs';
import { useStateAsync } from 'hooks';
import { isNull, isUndefined, omitBy } from 'lodash';
import { ChangeEvent, FormEvent, useEffect, useMemo, useState } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { download, getSerialNumber } from 'utils/utils';

const { RangePicker } = DatePicker;

type RangeValue = Parameters<NonNullable<React.ComponentProps<typeof DatePicker.RangePicker>['onChange']>>[0];

const initialValue = {
  page: 1,
  pageSize: 30,
  ThoiGianTao: {
    Min: '',
    Max: dayjs().endOf('day').toISOString()
  },
  TextForSearch: '',
  cachMuon: ''
};

const ListOfRentingTicket = () => {
  const [searchParams, setSearchParams] = useStateAsync<{
    page: number;
    pageSize: number;
    TextForSearch?: string;
    ThoiGianTao?: SearchRangeField;
    TinhTrang?: string;
    IsTaiCho?: boolean | null;
    cachMuon: string;
  }>(initialValue);

  // const {
  //   data: rentingTicketsData,
  //   isLoading: loadingRentingTickets,
  //   refetch: refetchRentingTicketsQuery,
  //   isRefetching
  // } = useQuery({
  //   queryKey: ['getRentingTickets'],
  //   queryFn: () => rentBack.getRentingTickets(searchParams)
  // });

  const { isLoading: loadingStartDateData, refetch } = useQuery({
    queryKey: ['startDate'],
    queryFn: settingApis.getStartDate,
    onSuccess: (data) => {
      const date = data.data.Item.NgayBatDauNamHoc.split('/');

      const dateResult = `${date[1]}/${date[0]}/${new Date().getFullYear()}`;

      const isAfterToday = dayjs(dateResult).isAfter(dayjs());

      if (isAfterToday) {
        setSearchParams((prevState) => {
          return {
            ...prevState,
            ThoiGianTao: {
              Min: dayjs(dateResult).subtract(1, 'year').toISOString(),
              Max: dayjs().endOf('day').toISOString()
            }
          };
        });
      } else {
        setSearchParams((prevState) => {
          return {
            ...prevState,
            ThoiGianTao: {
              Min: dayjs(dateResult).toISOString(),
              Max: dayjs().endOf('day').toISOString()
            }
          };
        });
      }
    }
  });

  const {
    mutateAsync: getRentingTickets,
    isLoading: loadingRentingTickets,
    data: rentingTicketsData
  } = useMutation({
    mutationFn: rentBack.getRentingTickets,
    mutationKey: ['getRentingTickets']
  });

  const { mutateAsync: print, isLoading: loadingPrint } = useMutation({
    mutationFn: rentBack.printTicket,
    onSuccess: (data) => {}
  });

  const rentingTickets = useMemo(() => {
    if (!rentingTicketsData?.data.Item.ListPhieuMuonSachUnit.length) return;

    const { ListPhieuMuonSachUnit, TotalCount } = rentingTicketsData?.data.Item;

    return { ListPhieuMuonSachUnit, TotalCount };
  }, [rentingTicketsData?.data.Item]);

  useEffect(() => {
    if (!loadingStartDateData) getRentingTickets(searchParams);
  }, [loadingStartDateData]);

  const columns: ColumnsType<{
    Id: string;
    MaPhieu: string;
    UserName: string;
    SoLuong: string;
    ThoiGianMuon: string;
    NgayHenTraGanNhat: string;
    TinhTrang: string;
  }> = [
    {
      key: 'STT',
      title: 'STT',
      render: (value, record, index) => getSerialNumber(searchParams.page, searchParams.pageSize, index),
      width: 100
    },
    {
      key: 'MaPhieu',
      title: 'Mã phiếu mượn',
      dataIndex: 'MaPhieu',
      onCell: () => ({
        className: 'underline text-primary-50 italic'
      }),
      render: (value, record, index) => <Link to={`ChiTietPhieuMuon/${record.Id}`}>{record.MaPhieu}</Link>
    },
    {
      key: 'UserName',
      title: 'Người mượn',
      dataIndex: 'UserName',
      render: (value, { UserName }) => (
        <Tooltip placement='topLeft' title={UserName} arrow={true} className='truncate'>
          <p>{UserName}</p>
        </Tooltip>
      ),
      onCell: (record) => ({
        className: 'text-left'
      }),
      ellipsis: true
    },
    {
      key: 'SoLuong',
      title: 'Số lượng sách',
      dataIndex: 'SoLuong'
    },
    {
      key: 'ThoiGianMuon',
      title: 'Ngày mượn',
      dataIndex: 'ThoiGianMuon',
      render: (value, { ThoiGianMuon }) => dayjs(ThoiGianMuon).format('DD/MM/YYYY')
    },
    {
      key: 'NgayHenTraGanNhat',
      title: 'Ngày hẹn trả gần nhất',
      dataIndex: 'NgayHenTraGanNhat',
      render: (value, { NgayHenTraGanNhat }) => (NgayHenTraGanNhat === '31/12/9999' ? '--' : NgayHenTraGanNhat)
    },
    {
      key: 'TinhTrang',
      title: 'Tình trạng',
      dataIndex: 'TinhTrang',
      render: (value, { TinhTrang }) => (
        <span
          className={classNames('rounded-full border p-1 px-2 shadow-md ', {
            'border-tertiary-70 bg-tertiary-70/20 text-tertiary-70 shadow-tertiary-70/40': TinhTrang.includes('quyển'),
            'shadow-danger10/40 border-danger-10 bg-danger-10/20 text-danger-10': TinhTrang.includes('Chưa trả'),
            'border-tertiary-30 bg-tertiary-30/20 text-tertiary-30 shadow-tertiary-30/40': TinhTrang === 'Đã trả'
          })}
        >
          {TinhTrang}
        </span>
      )
    },
    {
      key: 'actions',
      title: '',
      render: (value, record) => (
        <button
          onClick={async () => {
            try {
              const data = await print(record.Id);
              download(`PhieuMuon_${record.MaPhieu}.doc`, data.data);
              toast.success(`Tải phiếu mượn ${record.MaPhieu} thành công`);
            } catch (error) {}
          }}
          disabled={loadingPrint}
        >
          <PrinterFilled style={{ color: '#3472A2', fontSize: 24 }} />
        </button>
      ),
      width: 100
    }
  ];

  const handleRefresh = async () => {
    await refetch();

    await setSearchParams(initialValue);

    await getRentingTickets(initialValue);
  };

  const handleChange = (e: ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
    if (!e) return;

    const { name, value } = e.target;

    setSearchParams((prevState) => {
      return { ...prevState, [name]: value };
    });
  };

  const handleChangeRangePicker = (values: RangeValue) => {
    if (!values?.length) return;

    // @ts-ignore
    setSearchParams((prevState) => {
      return {
        ...prevState,
        ThoiGianTao: {
          Min: dayjs(values?.[0]).startOf('day').toISOString(),
          Max: dayjs(values?.[1]).endOf('day').toISOString()
        }
      };
    });
  };

  const handleChangeKindOfRenting = (e: ChangeEvent<HTMLSelectElement>) => {
    if (!e) return;

    let kindOfRenting = null;
    const { value } = e.target;

    setSearchParams((prevState) => {
      switch (value) {
        case '1':
          kindOfRenting = true;
          break;

        case '2':
          kindOfRenting = false;

          break;
        default:
          kindOfRenting = null;

          break;
      }
      return { ...prevState, cachMuon: value, IsTaiCho: kindOfRenting };
    });
  };

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    // @ts-ignore
    getRentingTickets({
      ...omitBy(searchParams, isUndefined || isNull),
      page: 1,
      pageSize: 30
    });
  };

  return (
    <div className='p-5'>
      <Title title='Danh sách phiếu mượn sách' />

      <form className='my-3 flex flex-1 items-center gap-1 bg-primary-50/20 p-2.5' onSubmit={handleSubmit}>
        <Input
          placeholder={'Nhập mã phiếu, tên người mượn'}
          containerClassName='flex-grow'
          name='TextForSearch'
          onChange={handleChange}
          value={searchParams.TextForSearch}
        />

        <RangePicker
          className='flex-grow border border-gray-300 py-[0.77rem] px-2 outline-none'
          format={['DD/MM/YYYY', 'DD-MM-YYYY']}
          value={[dayjs(searchParams.ThoiGianTao?.Min), dayjs(searchParams.ThoiGianTao?.Max)]}
          placeholder={['Ngày bắt đầu', 'Ngày kết thúc']}
          onChange={handleChangeRangePicker}
          clearIcon={false}
        />

        <Select
          items={[
            { value: '', label: 'Chọn tình trạng' },
            { value: 'Chưa trả', label: 'Chưa trả' },
            { value: 'Đã trả', label: 'Đã trả' },
            { value: 'Còn lại', label: 'Còn lại' }
          ]}
          className='flex-grow'
          onChange={handleChange}
          name='TinhTrang'
          value={searchParams.TinhTrang}
        />

        <Select
          items={[
            { value: '', label: 'Chọn cách mượn' },
            { value: '1', label: 'Đọc tại chỗ' },
            { value: '2', label: 'Mang về nhà' }
          ]}
          className='flex-grow'
          onChange={handleChangeKindOfRenting}
        />

        <Button variant='secondary' type='button' onClick={handleRefresh}>
          Làm mới
        </Button>

        <Button variant='default' type='submit'>
          Tìm kiếm
        </Button>
      </form>

      <Table
        columns={columns}
        className='custom-table'
        bordered
        loading={loadingRentingTickets}
        dataSource={rentingTickets?.ListPhieuMuonSachUnit}
        rowKey={(key) => key.MaPhieu}
        pagination={{
          current: searchParams.page,
          pageSize: searchParams.pageSize,
          total: rentingTickets?.TotalCount,
          onChange: async (page: number, pageSize: number) => {
            await setSearchParams((prevState) => {
              return {
                ...prevState,
                page,
                pageSize
              };
            });

            await getRentingTickets({ ...searchParams, page, pageSize });
          },
          hideOnSinglePage: true,
          showSizeChanger: false,
          showQuickJumper: true,
          locale: { jump_to: '', page: '' }
        }}
        locale={{ emptyText: () => <Empty /> }}
      />

      <div
        className={classNames('relative', {
          // @ts-ignore
          'mt-[64px]': searchParams.pageSize >= rentingTickets?.TotalCount
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger
            visible={!!rentingTickets?.ListPhieuMuonSachUnit}
            value={rentingTickets?.ListPhieuMuonSachUnit.length + ''}
            total={rentingTickets?.TotalCount + ''}
            onChange={async (pageSize) => {
              await setSearchParams((prevState) => {
                return { ...prevState, page: 1, pageSize: +pageSize };
              });

              await getRentingTickets(searchParams);
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default ListOfRentingTicket;
