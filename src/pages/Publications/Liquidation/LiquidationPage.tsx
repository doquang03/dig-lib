import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Tag } from 'antd';
import { bookApis, bookStoreApis, liquidationApis } from 'apis';
import { Button, DatePicker, Input, Loading, Select, Title } from 'components';
import { path } from 'constants/path';
import { UserConText } from 'contexts/user.context';
import dayjs from 'dayjs';
import { useContext, useEffect, useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useLocation, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { createExportSchema } from 'utils/rules';
import { removeVietnameseTones } from 'utils/utils';

type FormValues = { MaCaBiet: string; TenSach: string; MaKiemSoat: string; TrangThai: string };
type FormSeacrhValues = {
  TextForSearch: string;
};
type FormSubmit = {
  GhiChu: string;
  NgayXuatKho: string;
};
const LiquidationPage = () => {
  const user = useContext(UserConText);

  type exampleCheckStatus = {
    [key: string]: StatusBook;
  };
  type exampleCheckBook = {
    [key: string]: Book;
  };
  const [searchNotChosenBook, setSearchNotChosenBook] = useState<Array<SachCaBiet & { LinkBiaSach?: string }>>([]);
  const [searchChosenBook, setSearchChosenBook] = useState<Array<SachCaBiet & { LinkBiaSach?: string }>>([]);
  const [chosenBooks, setChosenBooks] = useState<Array<SachCaBiet & { LinkBiaSach?: string }>>([]);
  const [notChosenBooks, setNotChosenBooks] = useState<Array<SachCaBiet & { LinkBiaSach?: string }>>([]);
  const [statusCheck, setStatusCheck] = useState<exampleCheckStatus>({});
  const [liquidBooksCheck, setLiquidBooksCheck] = useState<exampleCheckBook>({});
  const [statusList, setStatusList] = useState<Array<Option>>([]);

  const navigate = useNavigate();

  const { state } = useLocation();

  const liquidBooks = state ? state.liquidBooks : undefined;
  const liquidBooksId = useMemo(() => {
    return liquidBooks.map((element: Book) => {
      return element?.Id;
    });
  }, [liquidBooks]);

  const {
    register: registerSearch,
    reset: resetSearch,
    handleSubmit
  } = useForm<FormSeacrhValues>({
    defaultValues: {
      TextForSearch: ''
    }
  });

  const { register, getValues } = useForm<FormValues>({
    defaultValues: {
      MaCaBiet: '',
      MaKiemSoat: '',
      TenSach: ''
    }
  });
  const { register: registerChoice, getValues: getValuesChoice } = useForm<FormValues>({
    defaultValues: {
      MaCaBiet: '',
      MaKiemSoat: '',
      TenSach: ''
    }
  });
  const {
    register: registerSubmit,
    getValues: getValuesSubmit,
    control,
    handleSubmit: submitThanhLy,
    formState: { errors }
  } = useForm<FormSubmit>({
    defaultValues: {
      GhiChu: '',
      NgayXuatKho: ''
    },
    resolver: yupResolver(createExportSchema)
  });

  const { data: bookData, isLoading: isLoadingGetBookById } = useQuery({
    queryKey: ['book', liquidBooks],
    queryFn: () => bookApis.getArrayBookId(liquidBooksId),
    enabled: !!liquidBooks,
    onSuccess(res) {
      functionCheckInit(res?.data?.Item);
    }
  });

  const functionCheckInit = (Item: EditArraySach) => {
    let checkStatus = { ...statusCheck };
    Item.List_TrangThai.forEach((element: StatusBook) => {
      checkStatus[element?.Id || '-1'] = element;
    });
    setStatusCheck(checkStatus);

    setStatusList([
      { label: 'Chọn tình trạng', value: '' },
      ...(bookData?.data.Item.List_TrangThai.map((element) => {
        return { label: element.TenTT, value: element.Id };
      }) || [])
    ]);

    let listSach = [...notChosenBooks];
    Item.List_SachCB.forEach((_book) => {
      const index = notChosenBooks?.findIndex((book) => book.Id === _book.Id);
      if (index === -1) {
        const indexOf = chosenBooks?.findIndex((book) => book.Id === _book.Id);
        if (indexOf === -1) {
          listSach.push(_book);
        }
      }
    });
    setNotChosenBooks(listSach);

    // setNotChosenBooks(Item.List_SoLuongTrangThaiSachVM || []);
  };

  useEffect(() => {
    if (!liquidBooks || !liquidBooks?.length) {
      return navigate(path.sach);
    }
  }, [liquidBooks, navigate]);

  const handleChoose = (_book: SachCaBiet & { LinkBiaSach?: string }) => () => {
    const index = notChosenBooks?.findIndex((book) => book.Id === _book.Id);
    const indexOf = chosenBooks?.findIndex((book) => book.Id === _book.Id);
    if (index !== undefined && index !== -1) {
      notChosenBooks?.splice(index, 1);
      setNotChosenBooks(notChosenBooks);
    }
    if (indexOf === -1) {
      setChosenBooks((prevState) => [...prevState, _book]);
    }
  };

  const handleUnchosen = (_book: SachCaBiet & { LinkBiaSach?: string }) => () => {
    const index = notChosenBooks?.findIndex((book) => book.Id === _book.Id);
    const indexOf = chosenBooks?.findIndex((book) => book.Id === _book.Id);

    if (index === -1) {
      setNotChosenBooks((prevState) => [...prevState, _book]);
    }
    if (indexOf !== undefined && indexOf !== -1) {
      chosenBooks?.splice(indexOf, 1);
      setChosenBooks(chosenBooks);
    }
  };

  const handleSearchNotChoosenBooks = () => {
    setSearchNotChosenBook(handleSearchNot(notChosenBooks) || []);
  };

  const handleSearchNot = (array: Array<SachCaBiet & { LinkBiaSach?: string }>) => {
    return array?.filter(
      (book) =>
        removeVietnameseTones((book?.MaKSCB || '').toLocaleLowerCase())?.includes(
          removeVietnameseTones(getValues('MaCaBiet').toLocaleLowerCase())
        ) &&
        removeVietnameseTones((book?.TenSach || '').toLocaleLowerCase())?.includes(
          removeVietnameseTones(getValues('TenSach').toLocaleLowerCase())
        ) &&
        removeVietnameseTones((book?.MaKiemSoat || '').toLocaleLowerCase())?.includes(
          removeVietnameseTones(getValues('MaKiemSoat').toLocaleLowerCase())
        ) &&
        (getValues('TrangThai') === '' || book.IdTrangThai === getValues('TrangThai'))
    );
  };

  const handleSearchChoosenBooks = () => {
    setSearchChosenBook(handleSearch(chosenBooks) || []);
  };

  const handleSearch = (array: Array<SachCaBiet & { LinkBiaSach?: string }>) => {
    return array?.filter(
      (book) =>
        removeVietnameseTones((book?.MaKSCB || '').toLocaleLowerCase())?.includes(
          removeVietnameseTones(getValuesChoice('MaCaBiet').toLocaleLowerCase())
        ) &&
        removeVietnameseTones((book?.TenSach || '').toLocaleLowerCase())?.includes(
          removeVietnameseTones(getValuesChoice('TenSach').toLocaleLowerCase())
        ) &&
        removeVietnameseTones((book?.MaKiemSoat || '').toLocaleLowerCase())?.includes(
          removeVietnameseTones(getValuesChoice('MaKiemSoat').toLocaleLowerCase())
        ) &&
        (getValuesChoice('TrangThai') === '' || book.IdTrangThai === getValuesChoice('TrangThai'))
    );
  };

  const handleBack = () => navigate(path.sach);

  useEffect(() => {
    setSearchNotChosenBook(notChosenBooks);
    handleSearchNotChoosenBooks();

    setSearchChosenBook(chosenBooks);
    handleSearchChoosenBooks();
  }, [chosenBooks, notChosenBooks]);

  const handleResetField = () => {
    resetSearch();
  };

  const { mutate: searchSach, isLoading: isLoadingSearch } = useMutation({
    mutationFn: (TextForSearch: string) =>
      bookStoreApis.GetListSCB(
        {
          idKho: '',
          TextForSearch: TextForSearch,
          DaTra: true
        },
        false
      ),
    onSuccess(res) {
      functionCheckInit(res?.data?.Item);
    }
  });

  const onSubmit = handleSubmit((data) => {
    data.TextForSearch = data.TextForSearch.trim();
    if (data.TextForSearch.length >= 1) searchSach(data.TextForSearch);
  });

  const { mutate: apiThanhLy, isLoading: isLoadingThanhLy } = useMutation({
    mutationFn: (payload: {
      listCaBiet: Array<SachCaBiet & { LinkBiaSach?: string }>;
      ghiChu: string;
      ngayVaoXo: string;
    }) =>
      liquidationApis.TaoPhieuXuatSach(
        payload.listCaBiet,
        user.profile?.Id as string,
        user.profile?.UserName as string,
        payload.ghiChu,
        payload.ngayVaoXo
      ),
    onSuccess() {
      toast.success('Thanh lý sách thành công');
      navigate(path.sach);
    }
  });

  const handleThanhLy = submitThanhLy((data) => {
    apiThanhLy({ listCaBiet: chosenBooks, ghiChu: data.GhiChu, ngayVaoXo: data.NgayXuatKho });
  });
  return (
    <div className='p-5'>
      <Title title='Tạo phiếu thanh lý' />
      <form className='my-5 flex flex-col flex-wrap gap-2 bg-secondary-10 py-3 px-3.5 md:flex-row' onSubmit={onSubmit}>
        <Input
          placeholder='Nhập tên, mã kiểm soát'
          containerClassName='w-[100%] md:w-[unset] md:grow'
          name='TextForSearch'
          register={registerSearch}
        />
        <Button type='button' variant='secondary' className='font-semibold' onClick={handleResetField}>
          Làm mới
        </Button>

        <Button type='submit' variant='default' className='font-semibold'>
          Tìm kiếm
        </Button>
      </form>

      <h3 className=' font-semibold uppercase text-primary-10'>Chọn sách cần thanh lý theo mã cá biệt</h3>

      <div className='max-h-[300px] overflow-x-auto'>
        <table className='table  shadow-md'>
          <thead className='sticky top-0 z-10 bg-primary-10'>
            <tr className='text-center text-white'>
              <td className='rounded-tl-md'>STT</td>
              <td>Mã cá biệt</td>
              <td>Tên sách</td>
              <td>Mã kiểm soát</td>
              <td>Bìa sách</td>
              <td>Tình trạng</td>
              <td>Đang cho mượn</td>
              <td className='rounded-tr-md'></td>
            </tr>
          </thead>
          <tbody className='bg-white'>
            <tr className='sticky top-[36.5px] z-10 bg-white'>
              <td>Tìm kiếm</td>
              <td>
                <Input
                  placeholder='Mã cá biệt'
                  name='MaCaBiet'
                  register={register}
                  onKeyUp={handleSearchNotChoosenBooks}
                />
              </td>
              <td>
                <Input
                  placeholder='Tên sách'
                  name='TenSach'
                  register={register}
                  onKeyUp={handleSearchNotChoosenBooks}
                />
              </td>
              <td>
                <Input
                  placeholder='Mã kiểm soát'
                  name='MaKiemSoat'
                  register={register}
                  onKeyUp={handleSearchNotChoosenBooks}
                />
              </td>
              <td className='flex flex-grow'></td>
              <td>
                <Select
                  items={statusList || []}
                  className='w-full'
                  register={register}
                  name='TrangThai'
                  onChange={handleSearchNotChoosenBooks}
                />
              </td>
              <td></td>
              <td></td>
            </tr>

            {searchNotChosenBook?.map((x: SachCaBiet & { LinkBiaSach?: string }, index: number) => {
              const isBorrowing = Boolean(x.IdPhieuMuon) ? 'Đang cho mượn' : 'Có sẵn';
              return (
                <tr className='text-center' key={x.Id + '' + index}>
                  <td className='content-center'>{index + 1}</td>
                  <td>{x.MaKSCB}</td>
                  <td>{x.TenSach}</td>
                  <td>{x.MaKiemSoat}</td>
                  <td>
                    <img
                      src={x.LinkBiaSach || '/content/Book.png'}
                      alt='Bìa sách'
                      className='ml-auto mr-auto block w-[140px] '
                    />
                  </td>
                  <td>{statusCheck[x.IdTrangThai as string]?.TenTT}</td>
                  <td>
                    <Tag color={Boolean(x.IdPhieuMuon) ? 'blue' : 'cyan'}>{isBorrowing}</Tag>
                  </td>
                  <td>
                    <Button
                      variant={Boolean(x.IdPhieuMuon) ? 'disabled' : 'default'}
                      onClick={handleChoose(x)}
                      disabled={Boolean(x.IdPhieuMuon)}
                    >
                      Chọn
                    </Button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>

      <h3 className=' font-semibold uppercase text-primary-10'>Danh sách thanh lý</h3>

      <div className='max-h-[300px] overflow-x-auto'>
        <table className='table shadow-md'>
          <thead className='sticky top-0 z-10 bg-primary-10'>
            <tr className='text-center text-white'>
              <td className='rounded-tl-md'>STT</td>
              <td>Mã cá biệt</td>
              <td>Tên sách</td>
              <td>Mã kiểm soát</td>
              <td>Bìa sách</td>
              <td>Tình trạng</td>
              <td></td>
              <td className='rounded-tr-md'></td>
            </tr>
          </thead>
          <tbody className='bg-white'>
            <tr className='sticky top-[36.5px] z-10 bg-white'>
              <td>Tìm kiếm</td>
              <td>
                <Input
                  placeholder='Mã cá biệt'
                  register={registerChoice}
                  name='MaCaBiet'
                  onKeyUp={handleSearchChoosenBooks}
                />
              </td>
              <td>
                <Input
                  placeholder='Tên sách'
                  register={registerChoice}
                  name='TenSach'
                  onKeyUp={handleSearchChoosenBooks}
                />
              </td>
              <td>
                <Input
                  placeholder='Mã kiểm soát'
                  register={registerChoice}
                  name='MaKiemSoat'
                  onKeyUp={handleSearchChoosenBooks}
                />
              </td>
              <td className='flex flex-grow'></td>
              <td>
                <Select
                  items={statusList || [{ label: 'Chọn trang thái', value: '' }]}
                  className='w-full'
                  register={registerChoice}
                  name='TrangThai'
                  onChange={handleSearchChoosenBooks}
                />
              </td>
              <td></td>
              <td></td>
            </tr>

            {searchChosenBook.map((x, index) => {
              return (
                <tr className='text-center' key={x.Id + '' + index}>
                  <td className='content-center'>{index + 1}</td>
                  <td>{x.MaKSCB}</td>
                  <td>{x.TenSach}</td>
                  <td>{x.MaKiemSoat}</td>
                  <td>
                    <img
                      src={x.LinkBiaSach || '/content/Book.png'}
                      alt='Bìa sách'
                      className='ml-auto mr-auto block w-[140px]'
                    />
                  </td>
                  <td>{statusCheck[x.IdTrangThai as string]?.TenTT}</td>
                  <td></td>
                  <td>
                    <Button onClick={handleUnchosen(x)}>Bỏ chọn</Button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>

      <div className='mt-3'>
        <div className='grid grid-cols-1 gap-2 md:grid-cols-12'>
          <div className='col-span-6'>
            <span className='font-bold'>
              Ghi chú <span className='text-danger-10'>*</span>
            </span>

            <Input
              placeholder='Nội dung ghi chú'
              containerClassName='flex-grow mt-1'
              register={registerSubmit}
              name='GhiChu'
              maxLength={255}
              errorMessage={errors?.GhiChu?.message}
            />
          </div>

          <div className=' col-span-6'>
            <span className='font-bold'>
              Ngày xuất kho <span className='text-danger-10'>*</span>
            </span>

            <div className='mt-1'>
              <DatePicker
                control={control}
                name={`NgayXuatKho` as const}
                errorMessage={errors?.NgayXuatKho?.message}
                disabledDate={(current) => {
                  return current && current > dayjs().endOf('day');
                }}
              />
            </div>
          </div>
        </div>

        <div className='mt-3 flex items-center justify-end gap-2'>
          <Button variant='secondary' onClick={handleBack}>
            Quay về
          </Button>

          <Button onClick={handleThanhLy}>Thanh lý</Button>
        </div>
      </div>
      <Loading open={isLoadingGetBookById || isLoadingSearch || isLoadingThanhLy}></Loading>
    </div>
  );
};

export default LiquidationPage;
