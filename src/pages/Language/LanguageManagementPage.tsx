import { path } from 'constants/path';
import { Outlet, useMatch } from 'react-router-dom';
import LanguageManagermentContent from './LanguageManagermentContent/LanguageManagermentContent';

const LanguageManagementPage = () => {
  const match = useMatch(path.ngonngu);

  return <>{Boolean(match) ? <LanguageManagermentContent /> : <Outlet />}</>;
};

export default LanguageManagementPage;
