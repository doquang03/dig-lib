import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { Modal, type ModalProps } from 'antd';
import { publishingCompanyApis } from 'apis';
import type { AxiosError } from 'axios';
import { Button, Input } from 'components';
import { memo, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import { publishingCompanySchema } from 'utils/rules';
import * as yup from 'yup';

type PublishingCompanyForm = yup.InferType<typeof publishingCompanySchema>;

type Props = {
  onBack: VoidFunction;
  onSuccess?: (value: string) => Promise<void> | void;
} & ModalProps;

const PublishingCompanyModal = (props: Props) => {
  const { title, onBack, onSuccess, ...rest } = props;

  const queryClient = useQueryClient();

  const [currentSchema, setCurrentSchema] = useState(publishingCompanySchema);
  const [duplicatePublishingCompany, setDuplicatePublishingCompany] = useState<Array<string>>([]);
  const [isTrigger, setIsTrigger] = useState('idle');

  const {
    register,
    handleSubmit,
    trigger,
    unregister,
    reset,
    setValue,
    formState: { errors }
  } = useForm<PublishingCompanyForm>({
    defaultValues: {
      name: ''
    },
    resolver: yupResolver(currentSchema)
  });

  const { mutate: handleCreate, isLoading: isLoadingCreate } = useMutation({
    mutationFn: (payload: object) => publishingCompanyApis.Create(payload),
    onSuccess: (data) => {
      toast.success('Thêm nhà xuất bản thành công');
      queryClient.invalidateQueries(['GetInitCreateForm']);
      reset();
      onBack();
      onSuccess?.(data.data.Item.Id);
    },
    onError: (
      ex: AxiosError<
        ResponseApi<{
          Ten: string;
        }>
      >
    ) => {
      if (ex?.response?.data?.ExMessage === 'Ten') {
        let tempPublishingCompany = duplicatePublishingCompany.concat([ex?.response?.data?.Item?.Ten]);
        setDuplicatePublishingCompany(tempPublishingCompany);
        let publishingCompanySchemaClone = publishingCompanySchema.clone();
        let publishingCompanySchemaObject = yup.object({
          name: yup
            .string()
            .trim()
            .notOneOf(tempPublishingCompany, ex?.response?.data?.Message)
            .required('Tên nhà xuất bản không được để trống')
        });
        publishingCompanySchemaClone = publishingCompanySchemaClone.concat(publishingCompanySchemaObject);
        setCurrentSchema(publishingCompanySchemaClone);
        setIsTrigger('name');
      }
    }
  });

  useEffect(() => {
    if (isTrigger === 'name') {
      trigger(isTrigger);
      setIsTrigger('idle');
    }
  }, [isTrigger, trigger]);

  const handleClose = () => {
    unregister('name');
    onBack();
  };

  const onSubmit = handleSubmit((data) => {
    data.name = data.name.replace(/ +(?= )/g, '');
    setValue('name', data.name);

    handleCreate({ Ten: data.name ?? '', GhiChu: data.note ?? '' });
  });

  return (
    <Modal
      title={<h3 className='font-bold uppercase text-primary-10'>{title}</h3>}
      {...rest}
      footer={null}
      closable={false}
    >
      <form onSubmit={onSubmit}>
        <label className='font-bold'>
          Tên nhà xuất bản <span className='text-danger-10'>*</span>
        </label>
        <Input
          placeholder='Nhập tên nhà xuất bản'
          register={register}
          name='name'
          errorMessage={errors?.name?.message}
          maxLength={256}
          containerClassName='mt-1 mb-3'
        />

        <label className='font-bold'>Ghi chú</label>

        <Input
          component={'textarea'}
          placeholder='Nhập nội dung ghi chú'
          register={register}
          name='note'
          errorMessage={errors?.note?.message}
          containerClassName='mt-1 mb-3'
        />

        <div className='mt-3 flex items-center justify-end gap-2'>
          <Button variant='secondary' type='button' onClick={handleClose} disabled={isLoadingCreate}>
            Quay về
          </Button>

          <Button variant='default' disabled={isLoadingCreate}>
            Thêm mới
          </Button>
        </div>
      </form>
    </Modal>
  );
};

export default memo(PublishingCompanyModal);
