export { default as StatisticalManagementPage } from './StatisticalManagementPage';
export * from './ListOfRentingBooks';
export * from './ReaderStatistics';
export * from './StatisticExportBooks';
export * from './NumberOfBooks';
export * from './StatisticReading';
export * from './ExportReport';
export * from './Audit';
