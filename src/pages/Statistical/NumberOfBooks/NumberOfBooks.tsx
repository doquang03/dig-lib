import { useQuery } from '@tanstack/react-query';
import { DatePicker, Table } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { settingApis, statistic } from 'apis';
import classNames from 'classnames';
import { Empty, Select, SizeChanger, Title } from 'components';
import { LOCATION_READER_OPTIONS } from 'constants/options';
import { path } from 'constants/path';
import 'css/NumberOfBooks.css';
import dayjs from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';
import { uniqueId } from 'lodash';
import { ChangeEvent, useMemo, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { getSerialNumber } from 'utils/utils';
dayjs.extend(customParseFormat);

const { RangePicker } = DatePicker;

type RangeValue = Parameters<NonNullable<React.ComponentProps<typeof DatePicker.RangePicker>['onChange']>>[0];

const NumberOfBooks = () => {
  const navigate = useNavigate();
  const [total, setTotal] = useState(0);
  const [pageSize, setPageSize] = useState<number>(30);
  const [page, setPage] = useState<number>(1);
  const [searchParams, setSearchParams] = useState<{
    TuNgay: string;
    DenNgay: string;
    sort: string;
    page?: number;
    pageSize?: number;
    IsGlobal: string;
  }>({
    TuNgay: '',
    DenNgay: dayjs().endOf('day').toISOString(),
    page: 1,
    pageSize: 30,
    sort: '',
    IsGlobal: ''
  });

  const { isLoading: loadingStartDateData } = useQuery({
    queryKey: ['startDate'],
    queryFn: settingApis.getStartDate,
    onSuccess: (data) => {
      const date = data.data.Item.NgayBatDauNamHoc.split('/');

      const dateResult = `${date[1]}/${date[0]}/${new Date().getFullYear()}`;

      const isAfterToday = dayjs(dateResult).isAfter(dayjs());

      if (isAfterToday) {
        setSearchParams((prevState) => {
          return {
            ...prevState,
            TuNgay: dayjs(dateResult).subtract(1, 'year').toISOString()
          };
        });
      } else {
        setSearchParams((prevState) => {
          return {
            ...prevState,
            TuNgay: dayjs(dateResult).toISOString()
          };
        });
      }
    }
  });

  const { data: numberOfBooksData, isLoading: loadingNumberOfBooks } = useQuery({
    queryKey: ['NumberOfBooks', searchParams],
    queryFn: () => statistic.GetData_SLAP(searchParams),
    enabled: Boolean(searchParams.TuNgay)
  });

  const numberOfBooks = useMemo(() => {
    if (!numberOfBooksData?.data.Item) return;
    const DataItem = numberOfBooksData?.data.Item;
    const totalItem: NumberOfBook = {
      idKho: '',
      Name: 'Tổng số',
      SoLuongSach: DataItem.SumTL + '',
      SoLuongSCB: DataItem.SumSLTLAnPham + '',
      SoLuongMuonSach: DataItem.SumSLLuotMuon + ''
    };
    setTotal(DataItem.TongSoLuong);
    const item = [...DataItem.ListKho];
    item.unshift(totalItem);
    return item;
  }, [numberOfBooksData?.data.Item]);

  const columns: ColumnsType<NumberOfBook> = useMemo(() => {
    return [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        className: `min-content`,
        onCell: (_) => ({
          className: _.idKho === '' ? 'border-right-none' : ''
        }),
        width: 100,
        render: (value, record, index) => {
          if (record.idKho === '') return '';
          else return getSerialNumber(page, pageSize, index) - 1;
        }
      },
      {
        title: 'Kho sách',
        dataIndex: 'Name',
        render: (value, record) => {
          return record.idKho === '' ? (
            <span className='text-left font-bold text-danger-10'>{record.Name + ' :'}</span>
          ) : (
            <p
              onClick={() => {
                navigate(path.khosach, {
                  state: { idKho: record.idKho }
                });
              }}
              className='cursor-pointer truncate text-left text-primary-50 underline'
            >
              {record.Name}
            </p>
          );
        },
        width: '40%',
        ellipsis: true,
        onCell: (record) => ({
          className: 'text-left'
        })
      },
      {
        title: 'Số lượng đầu sách',
        dataIndex: 'SoLuongSach',
        sorter: true,
        onCell: (_) => ({
          className: _.idKho === '' ? 'font-bold text-danger-10' : ''
        }),
        key: 'SoLuongSach'
      },
      {
        title: 'Số lượng sách cá biệt',
        dataIndex: 'SoLuongSCB',
        sorter: true,
        onCell: (_) => ({
          className: _.idKho === '' ? 'font-bold text-danger-10' : ''
        }),
        key: 'SoLuongSCB'
      },
      {
        title: 'Tổng số lần được mượn',
        dataIndex: 'SoLuongMuonSach',
        sorter: true,
        onCell: (_) => ({
          className: _.idKho === '' ? 'font-bold text-danger-10' : ''
        }),
        key: 'SoLuongMuonSach'
      }
    ];
  }, [navigate, page, pageSize]);

  const pagination = useMemo(() => {
    let size = 30;
    let currentPage = 1;

    if (pageSize) {
      size = pageSize;
    }
    if (currentPage) {
      currentPage = page;
    }
    setSearchParams((prevState) => {
      return {
        ...prevState,
        page: currentPage,
        pageSize: size
      };
    });
    return {
      current: currentPage,
      pageSize: size + 1,
      total: total,
      onChange: (page: number, pageSize: number) => {
        setPage(page);
        setPageSize(pageSize - 1);
      },
      hideOnSinglePage: false,
      showSizeChanger: false,
      showQuickJumper: true,
      locale: { jump_to: '', page: '' }
    };
  }, [pageSize, total, page]);

  const handleTableChange = (pagination: any, filters: any, sorter: any) => {
    let value = '';
    if (sorter.columnKey === 'SoLuongSach') {
      if (sorter.order === 'ascend') value = '11';
      if (sorter.order === 'descend') value = '1';
    }
    if (sorter.columnKey === 'SoLuongSCB') {
      if (sorter.order === 'ascend') value = '22';
      if (sorter.order === 'descend') value = '2';
    }
    if (sorter.columnKey === 'SoLuongMuonSach') {
      if (sorter.order === 'ascend') value = '33';
      if (sorter.order === 'descend') value = '3';
    }
    setSearchParams((prevState) => {
      return {
        ...prevState,
        sort: value
      };
    });
  };

  const handleChangeRangePicker = (values: RangeValue) => {
    if (!values?.length) return;

    // @ts-ignore
    setSearchParams((prevState) => {
      return {
        ...prevState,
        TuNgay: values?.[0]?.startOf('day').toISOString(),
        DenNgay: values?.[1]?.endOf('day').toISOString()
      };
    });
  };

  const handleChangeLocationType = (e: ChangeEvent<HTMLSelectElement>) => {
    if (!e) return;

    const { value } = e.target;

    setSearchParams((prevState) => {
      return {
        ...prevState,
        IsGlobal: value
      };
    });
  };

  return (
    <div className='p-5'>
      <Title title='Số lượng ấn phẩm' />

      <form className='mt-3 flex flex-1 items-center gap-1 bg-primary-50/30 p-2.5'>
        <Select
          items={LOCATION_READER_OPTIONS}
          className='w-full md:w-1/4'
          onChange={handleChangeLocationType}
          value={searchParams.IsGlobal}
        />
        <RangePicker
          className='w-full border border-gray-300 py-[0.8rem] px-2 outline-none md:w-1/4'
          format={['DD/MM/YYYY', 'DD-MM-YYYY']}
          value={[dayjs(searchParams.TuNgay), dayjs(searchParams.DenNgay)]}
          placeholder={['Ngày bắt đầu', 'Ngày kết thúc']}
          disabledDate={(current) => {
            return current && current > dayjs().endOf('day');
          }}
          onChange={handleChangeRangePicker}
          disabled={loadingStartDateData}
        />
      </form>

      <Table
        columns={columns}
        dataSource={numberOfBooks || []}
        pagination={pagination}
        scroll={{ x: 980 }}
        rowKey={(record) => uniqueId()}
        className='custom-table mt-3'
        indentSize={40}
        bordered
        locale={{
          emptyText: () => <Empty />
        }}
        loading={loadingNumberOfBooks}
        onChange={handleTableChange}
        showSorterTooltip={{
          title:
            searchParams.sort === ''
              ? 'Chọn để sắp xếp giảm dần'
              : ['11', '22', '33'].includes(searchParams.sort)
              ? 'Chọn để sắp xếp tăng dần'
              : 'Chọn để hủy sắp xếp'
        }}
      />

      <div
        className={classNames('relative', {
          'mt-[0px]': pageSize >= (total || 0)
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger
            visible={!!total}
            value={pageSize + ''}
            currentPage={page.toString()}
            total={total + ''}
            onChange={(pageSize) => {
              setPage(1);
              setPageSize(+pageSize);
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default NumberOfBooks;
