import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/QuanLyXuatKho`;

const exportApis = {
  getSachDKCB: (query: string) => httpThuVien.post<ResponseApi<Book[]>>(`${endPoint}/FindSachDKCB?query=${query}`),
  PrintSo: (id: string, Mau: string) =>
    httpThuVien.post(
      `${endPoint}/PrintSo?id=${id}&Mau=${Mau}`,
      {},
      {
        responseType: 'blob'
      }
    )
};

export default exportApis;
