export { default as TypeBookManagementPage } from './TypeBookManagementPage';
export * from './AddTypeBook';
export * from './AddTypeBookByExcel';
export * from './TypeBookManagermentContent';
