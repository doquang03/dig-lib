import { useQuery } from '@tanstack/react-query';
import { statistic } from 'apis';
import { Loading, Select, Title } from 'components';
import { SCHOOL_YEAR_OPTIONS } from 'constants/options';
import { useState } from 'react';

const InteractionRate = () => {
  const [year, setYear] = useState('');
  const { data: interactData, isLoading: loadingInteractData } = useQuery({
    queryKey: ['interactData', year],
    queryFn: () => statistic.XuatBaoCao_Json({ year: year }),
    staleTime: Infinity
  });
  return (
    <div>
      <Title title='Tỉ lệ tương tác' />

      <Select
        items={[{ label: 'Tất cả', value: '' }, ...SCHOOL_YEAR_OPTIONS]}
        className='mt-3'
        onChange={(e) => {
          setYear(e.target.value.split('-')[0]);
        }}
      />

      <div className='mt-3'>
        <table className='table  shadow-md'>
          <thead className='sticky top-0 z-10 bg-primary-10'>
            <tr className='text-center text-white'>
              <th className='rounded-tl-md text-center'>Tỉ lệ tương tác</th>
              <th className='text-center'>Tổng số học sinh</th>
              <th className='text-center'>Tỉ lệ tương tác</th>
              <th className='rounded-tr-md text-center'>Tổng số giáo viên</th>
            </tr>
          </thead>
          <tbody>
            <tr className='text-center'>
              <td>{`${interactData?.data?.Item[0] | 0} học sinh (${interactData?.data?.Item[1] | 0}%)`}</td>
              <td>{`${interactData?.data?.Item[2] | 0}`}</td>
              <td>{`${interactData?.data?.Item[3] | 0} giáo viên (${interactData?.data?.Item[4] | 0}%)`}</td>
              <td>{`${interactData?.data?.Item[5] | 0}`}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <Loading open={loadingInteractData}></Loading>
    </div>
  );
};

export default InteractionRate;
