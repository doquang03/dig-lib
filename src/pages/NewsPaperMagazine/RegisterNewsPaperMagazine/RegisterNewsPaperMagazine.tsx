import { TabsProps } from 'antd';
import { Tabs, Title } from 'components';
import { useState } from 'react';
import { StatisticByMonth, StatisticByWeek } from './components';

const items: TabsProps['items'] = [
  {
    key: 'week',
    label: `Thống kê tuần`,
    children: <StatisticByWeek />,
    forceRender: true
  },
  {
    key: 'day',
    label: `Thống kê tháng`,
    children: <StatisticByMonth />,
    forceRender: false
  }
];

const RegisterNewsPaperMagazine = () => {
  const [activeTab, setActiveTab] = useState<string>('week');

  const handleChangeTab = (activeTab: string) => setActiveTab(activeTab);

  return (
    <div className='p-5'>
      <Title title='Đăng ký báo, tạp chí' />

      <Tabs
        items={items}
        activeKey={activeTab}
        onChange={handleChangeTab}
        destroyInactiveTabPane={true}
        className='mt-3'
      />
    </div>
  );
};

export default RegisterNewsPaperMagazine;
