import { Outlet } from 'react-router-dom';

const ExportManagement = () => {
  return <Outlet />;
};

export default ExportManagement;
