import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Modal, Tooltip } from 'antd';
import Table, { type ColumnsType } from 'antd/es/table';
import { teacherApis } from 'apis';
import { AxiosError } from 'axios';
import classNames from 'classnames';
import { Button, Empty, Input, Loading, Select, SizeChanger, Title } from 'components';
import { CLASSIFY_OPTIONS, WORK_YEAR_OPTIONS } from 'constants/options';
import { path } from 'constants/path';
import 'css/AddLibraryMonitor.css';
import 'css/ComponentAntD.css';
import { usePaginationNavigate, useQueryConfig } from 'hooks';
import { isEmpty, omitBy } from 'lodash';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import { createSearchParams, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { teacherSchemaAdvance } from 'utils/rules';
import { convertDate, getSerialNumber } from 'utils/utils';
import * as yup from 'yup';
type TeacherForm = yup.InferType<typeof teacherSchemaAdvance>;
// TODO: Delete as soon as api implement
type Mock = {
  Mail: string;
  GioiTinh: string;
  Id: string;
  Ten: string;
  TrangThai: number;
  NgaySinh: string;
};

type EditData = {
  Id: string;
  PhanLoai: string;
  Mail: string;
  TrinhDoChuyenMon: string;
  NamCongTac: string;
  IsCanBoThuVien: boolean;
};

type FormInput = {
  TextForSearch: string;
};

const AddLibraryMonitorPage = () => {
  const [selectedLibraryMonitor, setSelectedLibraryMonitor] = useState<{
    LibraryMonitor: Mock | undefined;
  }>();
  type templateUpdateLibraryMonitor = {
    [key: string]: object;
  };
  const [updateLibraryMonitor] = useState<templateUpdateLibraryMonitor>({});
  const [visibleModal, setVisibleModal] = useState<boolean>(false);
  const [totalUnCanBoThuVien, setTotalUnCanBoThuVien] = useState(0);

  const [duplicateMail, setDuplicateMail] = useState<Array<string>>([]);
  const [currentSchema, setCurrentSchema] = useState(teacherSchemaAdvance);
  const [isTrigger, setIsTrigger] = useState('idle');

  const initialTeacherValue = {
    email: '',
    workYear: '',
    classify: '',
    qualification: ''
  };
  const {
    register,
    handleSubmit,
    trigger,
    reset,
    formState: { errors }
  } = useForm<TeacherForm>({
    defaultValues: initialTeacherValue,
    resolver: yupResolver(currentSchema)
  });

  const inputForm = useForm<FormInput>({
    defaultValues: {
      TextForSearch: ''
    }
  });

  const navigate = useNavigate();

  const handleNavigation = usePaginationNavigate();

  const queryConfig = useQueryConfig();

  // eslint-disable-next-line no-empty-pattern
  const { data: dataUnCanBoThuVien, isFetching } = useQuery({
    queryKey: ['IndexByCanBoThuVien', queryConfig.page, queryConfig.pageSize, queryConfig.TextForSearch],
    queryFn: () => {
      let page = 1;
      if (typeof queryConfig.page === 'string') page = parseInt(queryConfig.page);
      let queryString = `?page=${page}&pageSize=${queryConfig.pageSize}`;
      if (queryConfig.TextForSearch !== undefined) queryString += `&TextForSearch=${queryConfig.TextForSearch}`;
      return teacherApis.IndexByCanBoThuVien(queryString);
    }
  });

  const listUnCanBoThuVien = useMemo(() => {
    if (!dataUnCanBoThuVien?.data?.Item) return;
    const { ListThanhVien, count } = dataUnCanBoThuVien?.data?.Item;
    setTotalUnCanBoThuVien(count);
    return ListThanhVien;
  }, [dataUnCanBoThuVien?.data?.Item]);

  const { mutate: Save, isLoading: isLoadingSave } = useMutation({
    mutationFn: (payload: object) => teacherApis.EditAdvance(payload),
    onSuccess: (res) => {
      toast.success('Thao tác thành công');
      navigate(path.canbothuvien);
    }
  });

  const handleSave = () => {
    let arrResult: Array<string> = [];
    for (let param in updateLibraryMonitor) {
      arrResult.push(JSON.stringify(updateLibraryMonitor[param]));
    }
    Save(arrResult);
  };

  const { mutate: checkEmail, isLoading: isLoadingCheckEmail } = useMutation({
    mutationFn: (data: TeacherForm) => teacherApis.CheckMail(data?.email as string),
    onSuccess: (res, data) => {
      checkLibraryMonitor(data);
      if (data?.email && data?.email !== '') {
        let tempMail = duplicateMail.concat([data?.email]);
        setDuplicateMail(tempMail);
        let teacherSchemaClone = teacherSchemaAdvance.clone();
        let mailSchemaObject = yup.object({
          email: yup.string().notOneOf(tempMail, 'Địa chỉ email đã tồn tại')
        });
        teacherSchemaClone = teacherSchemaClone.concat(mailSchemaObject);
        setCurrentSchema(teacherSchemaClone);
      }
      setVisibleModal(false);
    },
    onError: (
      ex: AxiosError<
        ResponseApi<{
          Mail: string;
          MSTV: string;
        }>
      >
    ) => {
      if (ex?.response?.data?.ExMessage === 'Mail') {
        let tempMail = duplicateMail.concat([ex?.response?.data?.Item?.Mail]);
        setDuplicateMail(tempMail);
        let teacherSchemaClone = teacherSchemaAdvance.clone();
        let mailSchemaObject = yup.object({
          email: yup.string().notOneOf(tempMail, ex?.response?.data?.Message)
        });
        teacherSchemaClone = teacherSchemaClone.concat(mailSchemaObject);
        setCurrentSchema(teacherSchemaClone);
        setIsTrigger('email');
      }
    }
  });

  useEffect(() => {
    if (isTrigger === 'email') {
      trigger(isTrigger);
      setIsTrigger('idle');
    }
  }, [isTrigger, trigger]);

  const handleConfirm = handleSubmit((data) => {
    checkEmail(data);
  });

  const columns: ColumnsType<Mock> = useMemo(() => {
    const unCheckLibraryMonitor = (record: Mock) => {
      const id = record?.Id;
      if (id) delete updateLibraryMonitor[id];
      let tempMail = duplicateMail.filter((item) => item !== record.Mail);
      setDuplicateMail(tempMail);
      let teacherSchemaClone = teacherSchemaAdvance.clone();
      let mailSchemaObject = yup.object({
        email: yup.string().notOneOf(tempMail, 'Địa chỉ email đã tồn tại')
      });
      teacherSchemaClone = teacherSchemaClone.concat(mailSchemaObject);
      setCurrentSchema(teacherSchemaClone);
    };
    return [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        render: (value, record, index) => {
          return (
            queryConfig.page && queryConfig.pageSize && getSerialNumber(+queryConfig.page, +queryConfig.pageSize, index)
          );
        }
      },
      {
        title: 'Tên giáo viên',
        dataIndex: 'Ten',
        key: 'Ten',
        onCell: (record) => ({
          className: 'text-left'
        }),
        ellipsis: true,
        render: (value, { Ten }) => (
          <Tooltip title={Ten} arrow={true} className='truncate' placement='topLeft'>
            <p>{Ten}</p>
          </Tooltip>
        )
      },
      {
        title: 'Mã giáo viên',
        dataIndex: 'MaSoThanhVien',
        key: 'MaSoThanhVien'
      },
      {
        title: 'Tổ',
        dataIndex: 'ChucVu',
        key: 'ChucVu'
      },
      {
        title: 'Ngày sinh',
        dataIndex: 'NgaySinh',
        key: 'NgaySinh',
        render: (value, { NgaySinh }) => convertDate(NgaySinh)
      },
      {
        title: 'Giới tính',
        dataIndex: 'GioiTinh',
        key: 'GioiTinh'
      },
      {
        title: 'Năm công tác',
        dataIndex: 'NamCongTac',
        key: 'NamCongTac'
      },
      {
        title: 'Hành động',
        dataIndex: 'actions',
        align: 'center',
        key: 'actions',
        render: (value, record, index) => {
          return !(updateLibraryMonitor[record.Id] !== undefined) ? (
            <Button
              variant={'default'}
              className='w-[100%] md:w-auto'
              style={{ display: 'inline-block', minWidth: '74px' }}
              key={'Chọn'}
              onClick={(e) => {
                e.stopPropagation();
                setSelectedLibraryMonitor((preveState) => {
                  setVisibleModal(true);
                  reset();
                  return { ...preveState, LibraryMonitor: record };
                });
              }}
            >
              Chọn
            </Button>
          ) : (
            <Button
              variant={'default'}
              className='gray-button w-[100%] md:w-auto'
              style={{ display: 'inline-block', minWidth: '74px' }}
              key={'Bỏ chọn'}
              onClick={(e) => {
                e.stopPropagation();
                unCheckLibraryMonitor(record);
                setSelectedLibraryMonitor((preveState) => {
                  return { ...preveState, LibraryMonitor: record };
                });
              }}
            >
              Bỏ chọn
            </Button>
          );
        }
      }
    ];
  }, [duplicateMail, queryConfig.page, queryConfig.pageSize, reset, updateLibraryMonitor]);

  const onPaginate = useCallback(
    (page: number, pageSize: number) => {
      handleNavigation({ ...queryConfig, page: page + '', pageSize: pageSize + '' });
    },
    [handleNavigation, queryConfig]
  );

  const pagination = useMemo(() => {
    return {
      current: (!!queryConfig.page && +queryConfig.page) || 1,
      pageSize: (!!queryConfig.pageSize && +queryConfig.pageSize) || 20,
      total: totalUnCanBoThuVien,
      onChange: onPaginate,
      hideOnSinglePage: true,
      showSizeChanger: false,
      showQuickJumper: true,
      locale: { jump_to: '', page: '' }
    };
  }, [onPaginate, queryConfig.page, queryConfig.pageSize, totalUnCanBoThuVien]);

  useEffect(() => {
    Object.keys(queryConfig).forEach((key) => {
      if (!key) {
        return;
      }
      // @ts-ignore
      inputForm.setValue(key, queryConfig[key]);
    });
  }, [queryConfig, inputForm.setValue, inputForm]);

  const onSubmit = inputForm.handleSubmit((data) => {
    const { pathname } = window.location;

    navigate({
      pathname: pathname,
      search: createSearchParams({
        page: queryConfig.page + '',
        ...omitBy(data, isEmpty)
      }).toString()
    });
  });

  const handleResetField = () => {
    const { pathname } = window.location;

    navigate({
      pathname: pathname,
      search: createSearchParams({}).toString()
    });

    inputForm.reset();
  };

  const handleCancel = () => {
    setVisibleModal(false);
  };

  const handleBack = () => {
    navigate(path.canbothuvien);
  };

  const checkLibraryMonitor = (data: any) => {
    const id = selectedLibraryMonitor?.LibraryMonitor?.Id;
    if (id) {
      let tempData: EditData;
      tempData = {
        Id: id,
        TrinhDoChuyenMon: data.qualification,
        Mail: data.email,
        NamCongTac: data.workYear,
        PhanLoai: data.classify,
        IsCanBoThuVien: true
      };
      updateLibraryMonitor[id] = tempData;
    }
    setSelectedLibraryMonitor(undefined);
  };

  // TODO: Do something with selected LibraryMonitors
  let locale = {
    emptyText: 'Không tìm được kết quả phù hợp'
  };

  return (
    <div className='p-5'>
      <Title title='CHỌN CÁN BỘ THƯ VIỆN' />

      <form className='my-5 flex flex-col flex-wrap gap-2 bg-secondary-10 py-3 px-3.5 md:flex-row' onSubmit={onSubmit}>
        <Input
          placeholder='Nhập mã, tên giáo viên cần tìm kiếm'
          containerClassName='w-[100%] md:w-[920px]'
          name='TextForSearch'
          register={inputForm.register}
        />

        <Button type='button' variant='secondary' className='font-semibold' onClick={handleResetField}>
          Làm mới
        </Button>

        <Button type='submit' variant='default' className='font-semibold'>
          Tìm kiếm
        </Button>
      </form>

      <div className='relative my-6' style={{ marginBottom: 0 }}>
        <Table
          loading={isFetching && !listUnCanBoThuVien?.length}
          columns={columns}
          dataSource={listUnCanBoThuVien || []}
          pagination={pagination}
          scroll={{ x: 980 }}
          rowKey={(record) => record.Id}
          className='custom-table'
          locale={{
            emptyText: () => <Empty label={locale.emptyText} />
          }}
          bordered
        />
        <div
          className={classNames('relative', {
            // @ts-ignore
            'mt-[64px]': (queryConfig.page && +queryConfig.pageSize) >= totalUnCanBoThuVien
          })}
        >
          <div className='absolute bottom-1'>
            <SizeChanger
              visible={!!listUnCanBoThuVien?.length}
              value={queryConfig.pageSize}
              total={totalUnCanBoThuVien.toString()}
            />
          </div>
        </div>
      </div>
      <div className='mt-5 flex justify-end'>
        <Button type='button' variant='secondary' className='font-semibold' onClick={handleBack}>
          Quay về
        </Button>
        <Button variant='default' type='submit' className='ml-2 font-semibold' onClick={handleSave}>
          Lưu
        </Button>
      </div>
      <Modal
        className='model-choses-library-monitor'
        open={visibleModal}
        closable={false}
        width={'60vw'}
        footer={[
          <Button className='gray-button' onClick={handleCancel}>
            Quay về
          </Button>,
          <Button className='btn btn-primary btn-sm blue-button' style={{ marginLeft: '10px' }} onClick={handleConfirm}>
            Cập nhật
          </Button>
        ]}
      >
        <Title title={'BỔ SUNG THÔNG TIN GIÁO VIÊN'} style={{ border: 'none' }} />
        <div className='mt-10 grid grid-cols-1 md:grid-cols-12'>
          <div className='custom-row col-span-6 px-2'>
            <div className='my-2'>
              <label className='mb-2 font-bold'>
                Trình độ chuyên môn <span className='text-danger-10'>*</span>
              </label>

              <Input
                name='qualification'
                register={register}
                className='w-full rounded-md border border-gray-300 py-3.5 px-4 outline-none focus:border-primary-30 focus:shadow-sm'
                errorMessage={errors?.qualification?.message}
                placeholder='Nhập trình độ chuyên môn'
                maxLength={21}
              />
            </div>
            <div className='my-2'>
              <label className='mb-2 font-bold'>
                Năm công tác <span className='text-danger-10'>*</span>
              </label>

              <Select
                items={WORK_YEAR_OPTIONS}
                className='w-full'
                name='workYear'
                register={register}
                errorMessage={errors?.workYear?.message}
              />
            </div>
          </div>
          <div className='custom-row col-span-6 px-2'>
            <div className='my-2'>
              <label className='mb-2 font-bold'>
                Phân loại <span className='text-danger-10'>*</span>
              </label>

              <Select
                items={CLASSIFY_OPTIONS}
                name='classify'
                register={register}
                className='w-full'
                errorMessage={errors?.classify?.message}
              />
            </div>
            <div className='my-2'>
              <label className='mb-2 font-bold'>Email</label>

              <Input
                className='w-full rounded-md border border-gray-300 py-3.5 px-4 outline-none focus:border-primary-30 focus:shadow-sm'
                register={register}
                name='email'
                errorMessage={errors?.email?.message}
                maxLength={256}
                placeholder='Nhập Email'
              />
            </div>
          </div>
        </div>
      </Modal>
      <Loading open={isLoadingSave || isLoadingCheckEmail}></Loading>
    </div>
  );
};

export default AddLibraryMonitorPage;
