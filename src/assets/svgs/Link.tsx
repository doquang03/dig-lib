import Icon from '@ant-design/icons';
import type { CustomIconComponentProps } from '@ant-design/icons/lib/components/Icon';

const LinkSvg = () => {
  return (
    <svg width='19' height='19' viewBox='0 0 19 19' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <g clip-path='url(#clip0_10028_701)'>
        <path
          d='M9.5 0.222656C14.6248 0.222656 18.7773 4.3752 18.7773 9.5C18.7773 14.6248 14.6248 18.7773 9.5 18.7773C4.3752 18.7773 0.222656 14.6248 0.222656 9.5C0.222656 4.3752 4.3752 0.222656 9.5 0.222656Z'
          fill='url(#paint0_linear_10028_701)'
        />
        <path
          d='M12.6877 10.6615H12.1088C11.9492 10.6615 11.8193 10.7914 11.8193 10.951V12.9809H6.01914V7.18066H8.62793C8.7875 7.18066 8.91738 7.05078 8.91738 6.89121V6.3123C8.91738 6.15273 8.7875 6.02285 8.62793 6.02285H5.72969C5.25098 6.02285 4.86133 6.4125 4.86133 6.89121V13.2703C4.86133 13.749 5.25098 14.1387 5.72969 14.1387H12.1088C12.5875 14.1387 12.9771 13.749 12.9771 13.2703V10.9473C12.9771 10.7914 12.8473 10.6615 12.6877 10.6615Z'
          fill='white'
        />
        <path
          d='M13.7007 4.86133H11.3813C10.9954 4.86133 10.7987 5.33262 11.0733 5.60352L11.7228 6.25293L7.30674 10.6689C7.13604 10.8396 7.13604 11.1143 7.30674 11.285L7.71494 11.6932C7.88564 11.8639 8.16025 11.8639 8.33096 11.6932L12.747 7.27715L13.3964 7.92285C13.6673 8.19375 14.1386 8.00449 14.1386 7.61484V5.29551C14.1349 5.0543 13.9419 4.86133 13.7007 4.86133Z'
          fill='white'
        />
      </g>
      <defs>
        <linearGradient
          id='paint0_linear_10028_701'
          x1='-1.80353'
          y1='14.3319'
          x2='18.915'
          y2='14.1031'
          gradientUnits='userSpaceOnUse'
        >
          <stop stop-color='#11277C' />
          <stop offset='1' stop-color='#0000C5' />
        </linearGradient>
        <clipPath id='clip0_10028_701'>
          <rect width='19' height='19' fill='white' />
        </clipPath>
      </defs>
    </svg>
  );
};

const LinkIcon = (props: Partial<CustomIconComponentProps & { fill: string }>) => {
  return <Icon component={() => <LinkSvg />} {...props} />;
};

export default LinkIcon;
