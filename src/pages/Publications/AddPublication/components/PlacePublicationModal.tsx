import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { Modal, type ModalProps } from 'antd';
import { placePublicationApis } from 'apis';
import type { AxiosError } from 'axios';
import { Button, Input } from 'components';
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import { placePublicationSchema } from 'utils/rules';
import * as yup from 'yup';

type PlacePublicationForm = yup.InferType<typeof placePublicationSchema>;

type Props = {
  onBack: VoidFunction;
  onSuccess: (placePulicationValue: string) => void;
} & ModalProps;

const PlacePublicationModal = (props: Props) => {
  const { title, onBack, onSuccess, ...rest } = props;

  const queryClient = useQueryClient();

  const [currentSchema, setCurrentSchema] = useState(placePublicationSchema);
  const [duplicatePlacePublication, setDuplicatePlacePublication] = useState<Array<string>>([]);
  const [isTrigger, setIsTrigger] = useState('idle');

  const {
    register,
    handleSubmit,
    trigger,
    reset,
    unregister,
    setValue,
    formState: { errors }
  } = useForm<PlacePublicationForm>({
    defaultValues: {
      name: ''
    },
    resolver: yupResolver(currentSchema)
  });

  const { mutate: handleCreate, isLoading: isLoadingCreate } = useMutation({
    mutationFn: (payload: object) => placePublicationApis.Create(payload),
    onSuccess: (data) => {
      toast.success('Thêm nơi xuất bản thành công');
      queryClient.invalidateQueries(['GetInitCreateForm']).then(() => onSuccess(data.data.Item.id));
      reset();
      onBack();
    },
    onError: (
      ex: AxiosError<
        ResponseApi<{
          Ten: string;
        }>
      >
    ) => {
      if (ex?.response?.data?.ExMessage === 'Ten') {
        let tempPlacePublication = duplicatePlacePublication.concat([ex?.response?.data?.Item?.Ten]);
        setDuplicatePlacePublication(tempPlacePublication);
        let placePublicationSchemaClone = placePublicationSchema.clone();
        let placePublicationSchemaObject = yup.object({
          name: yup
            .string()
            .trim()
            .notOneOf(tempPlacePublication, ex?.response?.data?.Message)
            .required('Tên nơi xuất bản không được để trống')
        });
        placePublicationSchemaClone = placePublicationSchemaClone.concat(placePublicationSchemaObject);
        setCurrentSchema(placePublicationSchemaClone);
        setIsTrigger('name');
      }
    }
  });

  useEffect(() => {
    if (isTrigger === 'name') {
      trigger(isTrigger);
      setIsTrigger('idle');
    }
  }, [isTrigger, trigger]);

  const handleClose = () => {
    unregister('name');
    onBack();
  };

  const onSubmit = handleSubmit((data) => {
    let input = data.name.replace(/\s\s+/g, ' ');
    let shortname = data.shortname && data.shortname.replace(/\s\s+/g, ' ');

    setValue('name', input);
    setValue('shortname', shortname);

    handleCreate({ Ten: input, TenNgan: shortname });
  });

  return (
    <Modal
      title={<h3 className='font-bold uppercase text-primary-10'>{title}</h3>}
      {...rest}
      footer={null}
      closable={false}
    >
      <form onSubmit={onSubmit}>
        <label className='font-bold'>
          Tên nơi xuất bản <span className='text-danger-10'>*</span>
        </label>
        <Input
          placeholder='Tên nơi xuất bản'
          register={register}
          name='name'
          errorMessage={errors?.name?.message}
          maxLength={256}
          containerClassName='mt-1 mb-3'
        />

        <label className='font-bold'>Tên viết tắt</label>
        <Input
          placeholder='Nhập tên ngắn'
          register={register}
          name='shortname'
          errorMessage={errors?.shortname?.message}
          containerClassName='mt-1 mb-3'
        />

        <div className='mt-3 flex items-center justify-end gap-2'>
          <Button variant='secondary' type='button' onClick={handleClose} disabled={isLoadingCreate}>
            Quay về
          </Button>

          <Button variant='default' disabled={isLoadingCreate}>
            Thêm mới
          </Button>
        </div>
      </form>
    </Modal>
  );
};

export default PlacePublicationModal;
