import { Checkbox } from 'antd';
import { path } from 'constants/path';
import { HTMLAttributes, forwardRef } from 'react';
import { Link, useParams } from 'react-router-dom';
import { useFolderManagement } from '../contexts/useFolderManagement';

type Props = {
  id: string;
  name: string;
  image: string;
  isSelected?: boolean;
  onSelect?: VoidFunction;
} & HTMLAttributes<HTMLDivElement>;

const BookCard = forwardRef<HTMLDivElement, Props>((props, ref) => {
  const { id } = useParams();

  const { id: bookId, name, image, isSelected, onSelect, ...rest } = props;

  const { selectedBookIds } = useFolderManagement();

  return (
    <div className='relative' {...rest} ref={ref}>
      <Checkbox
        checked={selectedBookIds.includes(bookId || '')}
        className='absolute right-0 -top-[1px] z-10'
        onChange={(e) => {
          e.stopPropagation();
          onSelect?.();
        }}
      />
      <Link to={`${path.folderManagement}/${id}/${bookId}`} className='w-full'>
        <div
          className={`overflow-hidden rounded-md  shadow transition-transform duration-100 ease-out hover:translate-y-[-0.04rem] hover:bg-[#B1DDFF]/30 hover:shadow-md  ${
            isSelected ? 'bg-[#B1DDFF] hover:bg-[#B1DDFF]' : 'bg-white'
          }`}
        >
          <div className='relative w-full pt-[100%]'>
            <img
              src={image || '/content/Book.png'}
              alt={image}
              className='absolute top-0 left-0 h-full w-full bg-white object-cover'
            />
          </div>

          <div className='relative'>
            <div className='overflow-hidden p-2'>
              <div className='relative min-h-[3rem] text-left text-base line-clamp-2'>
                <span className='text-[14px]'>{name}</span>
              </div>
            </div>
          </div>
        </div>
      </Link>
    </div>
  );
});

export default BookCard;
