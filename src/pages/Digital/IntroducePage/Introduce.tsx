import { CKEditor } from '@ckeditor/ckeditor5-react';
import { useMutation, useQuery } from '@tanstack/react-query';
import { settingApis } from 'apis';
import CustomEditorBuild from 'ckeditor5-custom-build';
import { Button, Loading } from 'components';
import { useEffect, useMemo, useState } from 'react';
import { toast } from 'react-toastify';
type Props = {
  activeTab: string;
};
const Introduce = (props: Props) => {
  const { activeTab } = props;
  const [gioiThieu, setGioiThieu] = useState('');
  const [editor, setEditor] = useState<typeof CustomEditorBuild>();

  const { isFetching, remove } = useQuery({
    queryKey: ['IndexGioiThieu'],
    queryFn: () => settingApis.IndexGioiThieu(),
    enabled: activeTab === 'introduce',
    onSuccess: (res) => {
      let Item = res?.data?.Item;
      setGioiThieu(Item.GioiThieu);
    }
  });

  const { mutate: ExcuteGioiThieu, isLoading: isLoadingSet } = useMutation({
    mutationKey: ['SetGioiThieu'],
    mutationFn: () => settingApis.SetGioiThieu(editor?.getData()),
    onSuccess: (res) => {
      toast.success('Cập nhật thành công nội dung giới thiệu');
    }
  });

  const disiable = useMemo(() => {
    if (gioiThieu === '') return true;
    return false;
  }, [gioiThieu]);

  useEffect(() => {
    return () => remove();
  }, []);

  return (
    <>
      <div
        className='form-group form-group-sm row ckeditor-container long-editor'
        style={{ margin: 'auto', paddingTop: '1.25rem' }}
      >
        <CKEditor
          editor={CustomEditorBuild}
          data={gioiThieu}
          onReady={(editor) => {
            setEditor(editor);
          }}
          onChange={() => {
            setGioiThieu(editor?.getData());
          }}
        ></CKEditor>
      </div>
      <div className='mt-5 flex justify-end'>
        <Button
          type='button'
          className='ml-2 font-semibold'
          variant={disiable ? 'disabled' : 'default'}
          disabled={disiable}
          onClick={() => ExcuteGioiThieu()}
        >
          Cập nhật
        </Button>
      </div>
      <Loading open={isFetching || isLoadingSet}></Loading>
    </>
  );
};

export default Introduce;
