import { path } from './path';

export const IDENTITY_CONFIG = {
  authority: process.env.REACT_APP_API_AUTH + '',
  client_id: 'web_ThuVienSo',
  redirect_uri: `http://${window.location.host}/signin-oidc`,
  popup_redirect_uri: `http://${window.location.host}/signin-oidc-popup`,
  silent_redirect_uri: `http://${window.location.host}/silent_renew.html`,
  response_type: 'code',
  scope:
    'openid profile api-account-26baf8eb-76f6-4139-9586-38d96fbd37e9 api-donvi-ba61aea6-f6c3-426b-91c0-7161808a6991 api-hocsinh-1e0af3ec-091f-4d57-ab57-216f89df25f5 api-thuvien-38758600-cc38-44dc-9b09-2acab0751788',
  post_logout_redirect_uri: `http://${window.location.host}/logout/callback`,
  automaticSilentRenew: true
};

export const REQUEST_TIME_OUT = 15000;

export const SizeCollapse = 1280;

export const FORMAT_DATE_PICKER = ['DD/MM/YYYY', 'MM/YYYY', 'DD-MM-YYYY', 'MM-YYYY', 'YYYY', 'DD/MM'];

export const AUDIO_FILE_TYPE = ['mp3', 'wma', 'wav', 'flac', 'alac', 'ogg', 'pcm', 'aac', 'aiff', 'm4a'];

export const VIDEO_FILE_TYPE = ['avi', 'mp4', 'wmv', 'mkv', 'vob', 'flv', 'mpeg', 'webm'];

export const EDUCATION_DEPARTMENT_PATHNAME = [
  path.home,
  path.lichsutruycap,
  path.connectionManagement,
  path.listOfKeys,
  path.hostLibraryManagement,
  path.memberManagement,
  path.titleApproval,
  path.reviewDocument,
  path.listOfUnits,
  path.errorSyncDocument,
  path.hostDocument,
  path.folderManagement
  // path.listOfErrorDocument
];
