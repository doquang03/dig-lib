import { BaseModal } from 'components';
import { useWindowDimensions } from 'hooks';
import { useState } from 'react';
import ErrorDocumentItem from './ErrorDocumentItem';
import { useQuery } from '@tanstack/react-query';
import { errorSyncDocuments } from 'apis';
import { useNavigate } from 'react-router-dom';
import { path } from 'constants/path';

type Props = {
  documents: ErrorSyncDocumentById[];
};

function DocumentTab(props: Props) {
  const { documents } = props;

  const [selectedDocument, setSeletedDocument] = useState<ErrorSyncDocumentById>();

  const { height, width } = useWindowDimensions();

  const navigate = useNavigate();

  const { data: listOfDetailDocumentErrorData, isLoading: loadingListOfData } = useQuery({
    queryKey: ['getListOfDetailDocumentErrorById', selectedDocument?.Id],
    queryFn: () => errorSyncDocuments.getListOfDetailDocumentErrorById(selectedDocument?.Id + ''),
    enabled: !!selectedDocument?.Id
  });

  const listOfDetailDocumentError = listOfDetailDocumentErrorData?.data.Item?.ListModel || [];

  function handleClickDocumentItem(document: ErrorSyncDocumentById) {
    setSeletedDocument(document);
  }

  function handleClickDetailDocumentItem(document: ErrorSyncDocumentById) {
    console.log('document', document);
    switch (document.LoaiSach) {
      case 0:
        navigate(`/Sach/CapNhat/${document.IdSach}`, {
          state: { isFromErrorSyncedDocument: true, errorSyncedDocumentId: document.Id }
        });
        break;

      case 1:
      case 2:
        switch (document.Type) {
          case 'document':
            navigate(`/SachDienTu/${document.IdSach}`, {
              state: { isFromErrorSyncedDocument: true, errorSyncedDocumentId: document.Id }
            });
            break;
          case 'image':
            navigate(`/AlbumAnh/${document.IdSach}`, {
              state: { isFromErrorSyncedDocument: true, errorSyncedDocumentId: document.Id }
            });
            break;
          case 'audio':
            navigate(`/Audio/${document.IdSach}`, {
              state: { isFromErrorSyncedDocument: true, errorSyncedDocumentId: document.Id }
            });
            break;
          case 'video':
            navigate(`/Video/${document.IdSach}`, {
              state: { isFromErrorSyncedDocument: true, errorSyncedDocumentId: document.Id }
            });
            break;
        }
        break;

      default:
        break;
    }
  }

  return (
    <div className='my-3 flex flex-col justify-center gap-3'>
      {documents?.map((document) => (
        <ErrorDocumentItem
          key={document.Id}
          document={document}
          onClick={() => handleClickDocumentItem(document)}
          abbreviation={document.TomTat || ''}
        />
      ))}

      <BaseModal
        width={width / 2}
        style={{
          height: height / 1.5,
          width: width / 2
        }}
        className='overflow-y-auto'
        title={`TÀI LIỆU LỖI ĐỒNG BỘ - ${selectedDocument?.TenSach}`}
        open={!!selectedDocument}
        onInvisile={() => setSeletedDocument(undefined)}
        hasUpdate={false}
        shouldFixedFooter
        loading={loadingListOfData}
      >
        <div className='my-3 flex flex-col gap-3'>
          {listOfDetailDocumentError?.map((document) => (
            <ErrorDocumentItem
              document={document}
              errorMessage={document.ErrorMessage}
              key={document.Id}
              onClick={() => handleClickDetailDocumentItem(document)}
            />
          ))}
        </div>
      </BaseModal>
    </div>
  );
}

export default DocumentTab;
