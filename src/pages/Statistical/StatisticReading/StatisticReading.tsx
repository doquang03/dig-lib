import { useMutation, useQuery } from '@tanstack/react-query';
import { DatePicker, Table, Tooltip } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { bookStoreApis, statistic } from 'apis';
import classNames from 'classnames';
import { Button, Empty, Select, SizeChanger, Title } from 'components';
import { useUser } from 'contexts/user.context';
import dayjs from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';
import { uniqueId } from 'lodash';
import { ChangeEvent, useMemo, useState } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getSerialNumber } from 'utils/utils';

dayjs.extend(customParseFormat);

const { RangePicker } = DatePicker;

type RangeValue = Parameters<NonNullable<React.ComponentProps<typeof DatePicker.RangePicker>['onChange']>>[0];

const StatisticReading = () => {
  const [pageSize, setPageSize] = useState<number>(30);
  const [page, setPage] = useState<number>(1);
  const [searchParams, setSearchParams] = useState<{
    idKho?: string;
    TuNgay: string;
    DenNgay: string;
    sort: string;
    page?: number;
    pageSize?: number;
  }>({
    TuNgay: dayjs().startOf('year').startOf('day').toISOString(),
    DenNgay: dayjs().endOf('day').toISOString(),
    page: 1,
    pageSize: 30,
    sort: ''
  });

  const { isAllowedAdjustment } = useUser();

  const { data: bookstoreData, isLoading: loadingBookstore } = useQuery({
    queryKey: ['bookstoreOptions'],
    queryFn: () => bookStoreApis.getBookstoreOptions()
  });

  const { data: thongKeLuotDocData, isLoading: loadingthongKeLuotDoc } = useQuery({
    queryKey: ['thongKeLuotDoc', searchParams],
    queryFn: () => statistic.GetData_SLLDM(searchParams)
  });

  const { mutate: downloadExcel, isLoading: loadingDownloadExcel } = useMutation({
    mutationFn: () =>
      statistic.downloadExcel_SLLDM({
        TuNgay: searchParams.TuNgay,
        Denngay: searchParams.DenNgay,
        idKho: searchParams.idKho || '',
        sort: searchParams.sort || ''
      }),
    onSuccess: (data) => {
      toast.success('Tải thống kê lượt đọc, mượn thành công');
      const blob = new Blob([data.data, { type: 'application/xls' }]);
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = 'ThongKeLuotDocMuon.xls';
      link.click();
    }
  });

  const thongKeLuotDoc = useMemo(() => {
    if (!thongKeLuotDocData?.data.Item) return;

    return thongKeLuotDocData?.data.Item;
  }, [thongKeLuotDocData?.data.Item]);

  const columns: ColumnsType<NumberOfReading> = useMemo(() => {
    return [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        className: 'min-content',
        render: (value, record, index) => getSerialNumber(page, pageSize, index),
        width: 100
      },
      {
        title: 'Mã kiểm soát',
        dataIndex: 'MaKiemSoat'
      },
      {
        title: 'Tên sách',
        dataIndex: 'TenSach',
        sorter: true,
        key: 'TenSach',
        render: (value, { TenSach }) => (
          <Tooltip title={TenSach} arrow={true} className='truncate' placement='topLeft'>
            <p>{TenSach}</p>
          </Tooltip>
        ),
        ellipsis: true,
        onCell: (record) => ({
          className: 'text-left'
        })
      },
      {
        title: 'Kho sách',
        dataIndex: 'TenKho',
        key: 'TenKho',
        render: (value, { TenKho }) => (
          <Tooltip title={TenKho} arrow={true} className='truncate'>
            <p>{TenKho}</p>
          </Tooltip>
        ),
        ellipsis: true,
        onCell: (record) => ({
          className: 'text-left'
        })
      },
      {
        title: 'Số bản sách',
        dataIndex: 'SoLuongSCB',
        sorter: true,
        key: 'SoLuongSCB'
      },
      {
        title: 'Số lượt',
        children: [
          {
            title: 'Mượn về',
            dataIndex: 'MuonVe',
            sorter: true,
            key: 'MuonVe'
          },
          {
            title: 'Đọc tại chỗ',
            dataIndex: 'DocTaiCho',
            sorter: true,
            key: 'DocTaiCho'
          },
          {
            title: 'Tổng số lượt',
            dataIndex: 'TongLuot',
            sorter: true,
            key: 'TongLuot'
          }
        ]
      }
    ];
  }, [page, pageSize]);

  const pagination = useMemo(() => {
    let size = 30;
    let currentPage = 1;

    if (pageSize) {
      size = pageSize;
    }
    if (currentPage) {
      currentPage = page;
    }

    return {
      current: currentPage,
      pageSize: size,
      total: thongKeLuotDoc?.TongSoLuong,
      onChange: (page: number, pageSize: number) => {
        setPage(page);
        setPageSize(pageSize);
      },
      showSizeChanger: false,
      showQuickJumper: true,
      locale: { jump_to: '', page: '' }
    };
  }, [page, pageSize, thongKeLuotDoc?.TongSoLuong]);

  const handleChangeSelect = (e: ChangeEvent<HTMLSelectElement>) => {
    if (!e) return;

    const { value, name } = e.target;
    setSearchParams((prevState) => {
      return {
        ...prevState,
        [name]: value
      };
    });
  };

  const handleChangeRangePicker = (values: RangeValue) => {
    if (!values) return;

    // @ts-ignore
    setSearchParams((prevState) => {
      return {
        ...prevState,
        TuNgay: values?.[0]?.startOf('day').toISOString(),
        DenNgay: values?.[1]?.endOf('day').toISOString()
      };
    });
  };

  const handleDownloadExcel = () => {
    downloadExcel();
  };

  const handleTableChange = (pagination: any, filters: any, sorter: any) => {
    let value = '';
    if (sorter.columnKey === 'TenSach') {
      if (sorter.order === 'ascend') value = '11';
      if (sorter.order === 'descend') value = '1';
    }
    if (sorter.columnKey === 'SoLuongSCB') {
      if (sorter.order === 'ascend') value = '22';
      if (sorter.order === 'descend') value = '2';
    }
    if (sorter.columnKey === 'MuonVe') {
      if (sorter.order === 'ascend') value = '33';
      if (sorter.order === 'descend') value = '3';
    }
    if (sorter.columnKey === 'DocTaiCho') {
      if (sorter.order === 'ascend') value = '44';
      if (sorter.order === 'descend') value = '4';
    }
    if (sorter.columnKey === 'TongLuot') {
      if (sorter.order === 'ascend') value = '55';
      if (sorter.order === 'descend') value = '5';
    }
    setSearchParams((prevState) => {
      return {
        ...prevState,
        sort: value
      };
    });
  };

  return (
    <div className='p-5'>
      <Title title='Thống kê thanh lý' />

      <form className='mt-3 flex flex-1 items-center gap-1 bg-primary-50/30 p-2.5'>
        <Select
          items={[{ label: 'Chọn kho', value: '' }, ...(bookstoreData?.data.Item || [])]}
          className='w-1/4'
          name='idKho'
          disabled={!bookstoreData?.data.Item.length || loadingBookstore}
          onChange={handleChangeSelect}
        />

        <RangePicker
          className='w-1/4 border border-gray-300 py-[0.8rem] px-2 outline-none'
          format={['DD/MM/YYYY', 'DD-MM-YYYY']}
          defaultValue={[dayjs().startOf('year'), dayjs()]}
          placeholder={['Ngày bắt đầu', 'Ngày kết thúc']}
          disabledDate={(current) => {
            return current && current > dayjs().endOf('day');
          }}
          onChange={handleChangeRangePicker}
        />
      </form>

      {isAllowedAdjustment && (
        <Button onClick={handleDownloadExcel} loading={loadingDownloadExcel} disabled={loadingDownloadExcel}>
          Tải file Excel
        </Button>
      )}

      <Table
        columns={columns}
        dataSource={thongKeLuotDoc?.ListDauSach || []}
        pagination={pagination}
        scroll={{ x: 980 }}
        rowKey={(record) => uniqueId()}
        className='custom-table'
        indentSize={40}
        bordered
        locale={{
          emptyText: () => <Empty />
        }}
        loading={loadingthongKeLuotDoc}
        onChange={handleTableChange}
        showSorterTooltip={{
          title:
            searchParams.sort === ''
              ? 'Chọn để sắp xếp giảm dần'
              : ['11', '22', '33', '44', '55'].includes(searchParams.sort)
              ? 'Chọn để sắp xếp tăng dần'
              : 'Chọn để hủy sắp xếp'
        }}
      />

      <div
        className={classNames('relative', {
          'mt-[0px]': pageSize >= (thongKeLuotDoc?.TongSoLuong || 0)
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger
            visible={!!thongKeLuotDoc?.TongSoLuong}
            value={pageSize + ''}
            currentPage={page.toString()}
            total={thongKeLuotDoc?.TongSoLuong + ''}
            onChange={(pageSize) => {
              setPage(1);
              setPageSize(+pageSize);
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default StatisticReading;
