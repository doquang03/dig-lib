import { DeleteFilled, DownloadOutlined } from '@ant-design/icons';
import { useQuery } from '@tanstack/react-query';
import { Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { typeBookApis } from 'apis';
import classNames from 'classnames';
import { Button, Loading, ProgressBar, SizeChanger, Title } from 'components';
import { path } from 'constants/path';
import 'css/AddTypeBookByExcelPage.css';
import { usePaginationNavigate, useQueryConfig } from 'hooks';
import { useCallback, useMemo, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { getSerialNumber, saveByteArray } from 'utils/utils';

type TheLoaiSach = {
  STT: number;
  MaTheLoaiCha: string;
  TenTheLoai: string;
  MaTheLoai: string;
  MoTa: string;
  actions: string;
};

const AddTypeBookByExcelPage = () => {
  const [step, setStep] = useState<number>(1);
  // Mock state.
  const [listTheLoaiSachExcel, setListTheLoaiSachExcel] = useState<Array<TheLoaiSach>>([]);
  const [listSuccess, setListSuccess] = useState([]);
  const [listFail, setlistFail] = useState([]);
  const [tempFile, setTempFile] = useState<File>();
  const [cellError, setCellError] = useState<Array<string>>([]);
  const [isLoadingEnable, setIsLoadingEnable] = useState(false);

  const [isDone, setIsDone] = useState(false);

  const fileInutRef = useRef<HTMLInputElement | null>(null);
  const ref = useRef<HTMLDivElement | null>(null);
  const [file, setFile] = useState<File>();

  const queryConfig = useQueryConfig();

  const handleNavigation = usePaginationNavigate();

  const navigate = useNavigate();

  // This is test api. Not real api. Just use for testing!
  const { isFetching } = useQuery({
    queryKey: ['PreviewImport', file],
    queryFn: () => {
      let bodyFormData = new FormData();
      bodyFormData.append('file', file as Blob);
      let request = typeBookApis.PreviewImport(bodyFormData);
      request.then((res) => {
        let tempData = res?.data?.Item?.RawDataList;
        let result: Array<TheLoaiSach> = [];
        for (let i = 0; i < tempData.length; i++) {
          result.push({
            STT: i,
            MaTheLoaiCha: tempData[i][1],
            TenTheLoai: tempData[i][2] || '',
            MaTheLoai: tempData[i][3] || '',
            MoTa: tempData[i][4] || '',
            actions: ''
          });
        }
        setListTheLoaiSachExcel(result);
      });
      return request;
    },
    enabled: step !== 1
  });

  useQuery({
    queryKey: ['ImportSave', step],
    queryFn: () => {
      let tempData;
      let result = [];
      for (let i = 0; i < listTheLoaiSachExcel.length; i++) {
        tempData = [];
        tempData[0] = i;
        for (let param in listTheLoaiSachExcel[i]) {
          switch (param) {
            case 'MaTheLoaiCha':
              tempData[1] = listTheLoaiSachExcel[i].MaTheLoaiCha;
              break;
            case 'TenTheLoai':
              tempData[2] = listTheLoaiSachExcel[i].TenTheLoai;
              break;
            case 'MaTheLoai':
              tempData[3] = listTheLoaiSachExcel[i].MaTheLoai;
              break;
            case 'MoTa':
              tempData[4] = listTheLoaiSachExcel[i].MoTa;
              break;
          }
        }
        result.push(JSON.stringify(tempData));
      }
      setIsLoadingEnable(true);
      let request = typeBookApis.ImportSave(result);
      request.then((res) => {
        // let tempData = res?.data?.Item;
        // setListSuccess(tempData?.ListSuccess);
        // setlistFail(tempData?.ListFail);
        // setTempFile(tempData?.MemoryStream);
        // setCellError(tempData?.CellError);
        // console.log(tempData?.CellError);
        // let listShow = tempData.ListShow;
        // let resultFinal: Array<TheLoaiSach> = [];
        // let tempFor: TheLoaiSach;
        // for (let i = 0; i < listShow.length; i++) {
        // tempFor = {
        //   STT: listShow[i][0] - 1,
        //   Ten: listShow[i][1],
        //   MaSoThanhVien: listShow[i][2],
        //   GioiTinh: listShow[i][3],
        //   NgaySinh: listShow[i][4],
        //   ChucVu: listShow[i][5],
        //   DiaChi: listShow[i][6],
        //   SDT: listShow[i][7],
        //   actions: listShow[i][8]
        // };
        // resultFinal.push(tempFor);
        // }
        // setListTheLoaiSachExcel(resultFinal);
        // setIsDone(true);
        // setIsLoadingEnable(false);
      });
      return request;
    },
    enabled: step === 3
  });

  const columns: ColumnsType<TheLoaiSach> = useMemo(() => {
    const handleDeleteRow = (STT: number) => {
      let result: Array<TheLoaiSach> = [];
      for (let i = 0; i < listTheLoaiSachExcel.length; i++) {
        if (STT === listTheLoaiSachExcel[i].STT) {
          continue;
        }
        result.push(listTheLoaiSachExcel[i]);
      }
      setListTheLoaiSachExcel(result);
    };
    return [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        render: (value, record, index) => {
          return (
            queryConfig.page && queryConfig.pageSize && getSerialNumber(+queryConfig.page, +queryConfig.pageSize, index)
          );
        }
      },
      {
        title: 'Mã thể loại cha',
        dataIndex: 'MaTheLoaiCha',
        key: 'MaTheLoaiCha',
        render: (value, record, index) => {
          return (
            <>
              <div
                className={
                  step === 3 && cellError[record.STT] && cellError[record.STT].indexOf('1') !== -1
                    ? 'min-h-38 bg-tertiary-40 py-2'
                    : ''
                }
              >
                {record.MaTheLoaiCha}
              </div>
            </>
          );
        }
      },
      {
        title: 'Tên thể loại sách',
        dataIndex: 'TenTheLoai',
        key: 'TenTheLoai',
        render: (value, record, index) => {
          return (
            <>
              <div
                className={
                  step === 3 && cellError[record.STT] && cellError[record.STT].indexOf('2') !== -1
                    ? 'min-h-38 bg-tertiary-40 py-2'
                    : ''
                }
              >
                {record.TenTheLoai}
              </div>
            </>
          );
        }
      },
      {
        title: 'Mã thể loại',
        dataIndex: 'MaTheLoai',
        key: 'MaTheLoai',
        render: (value, record, index) => {
          return (
            <>
              <div
                className={
                  step === 3 && cellError[record.STT] && cellError[record.STT].indexOf('5') !== -1
                    ? 'min-h-38 bg-tertiary-40 py-2'
                    : ''
                }
              >
                {record.MaTheLoai}
              </div>
            </>
          );
        }
      },
      {
        title: 'Mô tả',
        dataIndex: 'MoTa',
        key: 'MoTa',
        render: (value, record, index) => {
          return (
            <>
              <div
                className={
                  step === 3 && cellError[record.STT] && cellError[record.STT].indexOf('4') !== -1
                    ? 'min-h-38 bg-tertiary-40 py-2'
                    : ''
                }
              >
                {record.MoTa}
              </div>
            </>
          );
        }
      },
      {
        title: (step === 2 && 'Hành động') || (step === 3 && 'Nội dung lỗi'),
        dataIndex: 'actions',
        key: 'actions',
        render: (value, record) => {
          return (
            <>
              {step === 2 && (
                <button
                  className='mx-2'
                  onClick={(e) => {
                    e.stopPropagation();
                    handleDeleteRow(record.STT);
                  }}
                >
                  <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
                </button>
              )}
              {step === 3 && <div style={{ color: 'red' }}>{record.actions}</div>}
            </>
          );
        }
      }
    ];
  }, [cellError, listTheLoaiSachExcel, queryConfig.page, queryConfig.pageSize, step]);

  const onPaginate = useCallback(
    (page: number, pageSize: number) => {
      handleNavigation({ ...queryConfig, page: page + '', pageSize: pageSize + '' });
    },
    [handleNavigation, queryConfig]
  );

  const pagination = useMemo(() => {
    return {
      current: (!!queryConfig.page && +queryConfig.page) || 1,
      pageSize: (!!queryConfig.pageSize && +queryConfig.pageSize) || 20,
      total: listTheLoaiSachExcel?.length,
      onChange: onPaginate,
      hideOnSinglePage: true,
      showSizeChanger: false
    };
  }, [onPaginate, queryConfig.page, queryConfig.pageSize, listTheLoaiSachExcel?.length]);

  const handleOnFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files?.[0];

    setFile(fileFromLocal);
    setStep(2);
  };

  const handleAccessFileInputRef = () => {
    fileInutRef.current?.click();
  };

  const handleBack = () => {
    navigate(path.giaovien);
  };

  const handleBackCurrent = () => {
    setFile(undefined);
    setListTheLoaiSachExcel([]);
    setStep(1);
  };

  const handleSubmit = () => {
    // TODO: Integrate api to submit data
    ref.current?.scrollIntoView({ behavior: 'smooth' });
    setStep(3);
  };
  return (
    <div className='p-5' ref={ref}>
      <Title title='THÊM THỂ LOẠI SÁCH TỪ EXCEL' />

      <div className='my-10'>
        <ProgressBar activeStep={step} type='excel' />
      </div>

      {/* TODO: Condition are step > 1 and data is not undefined*/}
      {step === 1 && (
        <div className='flex items-center justify-center rounded-md bg-tertiary-10 py-4'>
          <a href={`/Tempalates/MauTheLoaiSach.xls`} download='MauTheLoaiSach'>
            <Button className='bg-tertiary-20 hover:bg-tertiary-20/30'>
              <DownloadOutlined />
              File excel mẫu
            </Button>
          </a>

          <input
            type='file'
            accept='.csv,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel'
            className='hidden'
            ref={fileInutRef}
            onChange={handleOnFileChange}
          />

          <Button variant='default' className='ml-2' onClick={handleAccessFileInputRef}>
            Chọn tệp
          </Button>
        </div>
      )}

      {step > 1 && (
        <div>
          {step === 2 && (
            <>
              <p>
                Tổng số giáo viên: <span className='font-bold'>{listTheLoaiSachExcel?.length}</span>
              </p>

              <h3 className='text-primary-10'>DANH SÁCH GIÁO VIÊN</h3>
            </>
          )}

          {step === 3 && (
            <>
              <p className='text-tertiary-30'>
                Lưu thành công: <span className='font-bold'>{listSuccess.length} học sinh</span>
              </p>

              <p className='flex items-center text-danger-10'>
                Lưu thất bại: <span className='font-bold'>{listFail.length} học sinh</span>{' '}
                <Button
                  variant='danger'
                  className='ml-2'
                  onClick={() => {
                    saveByteArray(tempFile, 'DsTheLoaiSachBiLoi.xls', 'application/xls');
                  }}
                >
                  <DownloadOutlined />
                  Tải file lỗi
                </Button>
              </p>

              <h3 className='uppercase text-primary-10'>Danh sách học sinh bị lỗi</h3>
            </>
          )}

          {((step === 2 && !!listTheLoaiSachExcel?.length) || (step === 3 && listFail.length > 0)) && (
            <>
              <div className='mt-6'>
                <Table
                  loading={isFetching && isLoadingEnable}
                  columns={columns}
                  pagination={pagination}
                  dataSource={listTheLoaiSachExcel}
                  scroll={{ x: 980 }}
                  rowKey={(record) => record.STT}
                  className='custom-table'
                  bordered
                />
              </div>

              <div
                className={classNames('relative', {
                  // @ts-ignore
                  'mt-[64px]': (queryConfig.page && +queryConfig.pageSize) >= listTheLoaiSachExcel.length
                })}
              >
                <div className='absolute bottom-1'>
                  <SizeChanger
                    visible={
                      !isFetching &&
                      queryConfig.pageSize !== undefined &&
                      listTheLoaiSachExcel.length > +queryConfig.pageSize
                    }
                    value={queryConfig.pageSize}
                    total={listTheLoaiSachExcel.length.toString()}
                  />
                </div>
              </div>
            </>
          )}
        </div>
      )}

      <div className='mr-10 mt-6 flex items-center  justify-end'>
        {step === 1 && (
          <>
            <Button variant='secondary' onClick={handleBack}>
              Quay về
            </Button>
          </>
        )}
        {!!listTheLoaiSachExcel.length && (
          <>
            {step === 2 && (
              <>
                <Button variant='secondary' onClick={handleBackCurrent}>
                  Quay về
                </Button>
                <Button variant='default' className='ml-2' onClick={handleSubmit}>
                  Tiếp tục
                </Button>
              </>
            )}

            {step === 3 && isDone && (
              <Button variant='default' className='ml-2' onClick={handleBack}>
                Hoàn tất
              </Button>
            )}
          </>
        )}
      </div>
      <Loading open={(step === 3 && isLoadingEnable) || (step === 2 && isFetching)} />
    </div>
  );
};

export default AddTypeBookByExcelPage;
