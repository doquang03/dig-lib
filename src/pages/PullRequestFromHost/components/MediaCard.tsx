import { AUDIO_FILE_TYPE, VIDEO_FILE_TYPE } from 'constants/config';
import { path } from 'constants/path';
import { type ChangeEvent } from 'react';
import { Link } from 'react-router-dom';
import { getEmbed } from 'utils/utils';

type Props = {
  id: string;
  name: string;
  image: string;
  isSelected: boolean;
  linkFile: string;
  LienKet: string;
  onSelect: (bookId: string) => void;
};

const MediaCard = (props: Props) => {
  const { id, name, image, isSelected, onSelect, linkFile, LienKet } = props;

  const handleSelect = (e: ChangeEvent<HTMLInputElement>) => {
    if (!e) return;

    onSelect(id);
  };

  const checkFileExtension = (extension: 'video' | 'audio') => {
    switch (extension) {
      case 'video':
        return VIDEO_FILE_TYPE.some((x) => linkFile.includes(x));

      case 'audio':
        return AUDIO_FILE_TYPE.some((x) => linkFile.includes(x));
      default:
        break;
    }
  };

  return (
    <div className='relative'>
      <input
        type='checkbox'
        className='absolute top-0 right-0 z-20 w-[30px]'
        checked={isSelected}
        onChange={handleSelect}
      />
      <Link to={`${path.pullRequest}/${id}`}>
        <div className='overflow-hidden rounded-sm bg-white shadow hover:shadow-md'>
          {checkFileExtension('audio') ? (
            <>
              <div className='relative h-1/2 w-full pt-[50%]'>
                <img
                  src={image || '/content/Book.png'}
                  alt={image}
                  className='absolute top-0 left-0 h-full w-full bg-white object-cover'
                />
              </div>

              <audio controls key={linkFile} className='absolute bottom-[2.7rem] w-full opacity-70'>
                <source src={linkFile} />
              </audio>
            </>
          ) : checkFileExtension('video') ? (
            <div className='relative h-1/2 w-full pt-[50%]'>
              <video controls key={linkFile} className='absolute top-0 left-0 h-full w-full bg-white object-cover'>
                <source src={linkFile} type='video/mp4' />
              </video>
            </div>
          ) : (
            LienKet &&
            getEmbed(LienKet) && (
              <div className='relative h-1/2 w-full'>
                <iframe
                  width={'100%'}
                  height={250}
                  src={`//www.youtube.com/embed/${getEmbed(LienKet)}`}
                  frameBorder='0'
                  allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
                  allowFullScreen
                  title='Embedded youtube'
                />
              </div>
            )
          )}
        </div>

        <div className='mt-1 overflow-hidden p-1'>
          <div className='min-h-[2rem] text-xs line-clamp-2'>{name}</div>
        </div>
      </Link>
    </div>
  );
};

export default MediaCard;
