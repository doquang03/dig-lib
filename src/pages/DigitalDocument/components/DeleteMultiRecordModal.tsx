import { useMutation } from '@tanstack/react-query';
import { digitalDocumentApis } from 'apis';
import { ModalDelete } from 'components';
import dayjs from 'dayjs';
import { memo } from 'react';
import { toast } from 'react-toastify';

type Props = {
  records?: DigitalDocument[];
  visible: boolean;
  onCancel: () => void;

  onSuccess: VoidFunction;
};

const DeleteMultiRecordModal = (props: Props) => {
  const { records, visible, onCancel, onSuccess } = props;

  const { mutate, isLoading } = useMutation({
    mutationFn: digitalDocumentApis.deleteMulti,
    onSuccess: () => {
      toast.success('Xóa thành công tài liệu đã chọn');
      onSuccess();
    }
  });

  const handleSubmit = () => {
    records?.length && mutate(records?.map(({ Id }) => Id));
  };

  return (
    <ModalDelete
      open={Boolean(records?.length) && visible}
      closable={false}
      width={400}
      title={
        <>
          <h3 className='text-bold'>Bạn có muốn chắc chắn xóa những tài nguyên số này không?</h3>
          <p className='text-[16px] font-extralight leading-normal'>Bạn sẽ không thể khôi phục dữ liệu sau khi xóa.</p>
        </>
      }
      handleCancel={onCancel}
      handleOk={handleSubmit}
      loading={isLoading}
    >
      <div className='max-h-[300px] overflow-x-auto'>
        {records?.map(({ Id, NgayXuatBan, TenSach }, index) => {
          return (
            <div key={Id} className='flex items-center justify-between gap-5 border-b-2 px-2 py-2 font-bold'>
              <div className='flex items-center gap-1'>
                <p className={'text-primary-10'}>{index + 1} </p>

                <p className={'truncate text-left text-primary-10 underline'}>{TenSach}</p>
              </div>

              <p className={'mr-4 text-left'}>{dayjs(NgayXuatBan).format('DD/MM/YYYY')}</p>
            </div>
          );
        })}
      </div>
    </ModalDelete>
  );
};

export default memo(DeleteMultiRecordModal);
