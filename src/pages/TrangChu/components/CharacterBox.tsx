import { LibraryIcon, StudentIcon, TeacherIcon } from '../assets/icons';
import LibraryMonitorIcon from '../assets/icons/LibraryMonitorIcon';

type Props = {
  character: 'library' | 'student' | 'teacher' | 'libraryMonitor';
  value?: number | string;
};

const data = {
  bgBox: {
    library: '#FCEEEE',
    student: '#EFF4F8',
    teacher: '#E5F4ED',
    libraryMonitor: '#FCEEEE'
  },
  icon: {
    library: <LibraryIcon />,
    student: <StudentIcon />,
    teacher: <TeacherIcon />,
    libraryMonitor: <LibraryMonitorIcon />
  },
  label: {
    library: 'Thư viện',
    student: 'Học sinh',
    teacher: 'Giáo viên',
    libraryMonitor: 'Cán bộ TV'
  }
};

const CharacterBox = (props: Props) => {
  const { character, value = '--' } = props;

  return (
    <div
      className={`grid h-40 w-full place-items-center bg-[${data.bgBox[character]}] rounded-lg px-7 shadow-md shadow-[${data.bgBox[character]}]`}
    >
      <div className='flex items-center gap-x-2'>
        {data.icon[character]}

        <p className='text-primary-10'>{data.label[character]}</p>
      </div>

      <p className='text-3xl font-semibold'>{value || '--'}</p>
    </div>
  );
};

export default CharacterBox;
