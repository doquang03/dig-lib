import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/BoSuuTap`;

const collectionApis = {
  Index: () => {
    return httpThuVien.post(`/${endPoint}/Index`);
  },
  Delete: (id: string) => {
    return httpThuVien.post(`/${endPoint}/Delete?id=` + id);
  },
  EditByID: (id: string) => {
    return httpThuVien.post(`/${endPoint}/EditByID?id=` + id);
  },
  Create: (payload: object) => {
    return httpThuVien.post(`/${endPoint}/Create`, payload);
  },
  EditModel: (payload: object) => {
    return httpThuVien.post(`/${endPoint}/EditModel`, payload);
  },
  GetOptions: () => httpThuVien.get<ResponseApi<Selections>>(`/${endPoint}/GetOptions`)
};

export default collectionApis;
