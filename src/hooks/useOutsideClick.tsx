import { useEffect, useRef } from 'react';

export default function useOutsideClick<T>(callback: () => void) {
  const ref = useRef<T>();

  useEffect(() => {
    const handleClick = (event: MouseEvent) => {
      // @ts-ignore
      if (ref.current && !ref.current.contains(event.target)) {
        callback();
      }
    };

    document.addEventListener('click', handleClick);

    return () => {
      document.removeEventListener('click', handleClick);
    };
  }, [callback, ref]);

  return ref;
}
