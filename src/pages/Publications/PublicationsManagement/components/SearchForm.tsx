import { TableOutlined, UnorderedListOutlined } from '@ant-design/icons';
import classNames from 'classnames';
import { Button, Input, Select, TreeSelect } from 'components';
import { usePaginationNavigate, useQueryConfig } from 'hooks';
import { isEmpty, omitBy, trim } from 'lodash';
import { useEffect, useMemo } from 'react';
import { useForm } from 'react-hook-form';
import useOptionsSeach from '../hooks/useOptionsSeach';
import { useUser } from 'contexts/user.context';
import { OPTIONS_BY_TYPESCHOOL } from 'constants/options';
import { useQuery } from '@tanstack/react-query';
import { bookstoreTypeApis } from 'apis';

type Props = {
  onChangeViewMode: VoidFunction;
  onResetForm: VoidFunction;
  onSubmit?: VoidFunction;
  viewMode: string;
  isLargeView?: boolean;
};
const initialValues = {
  TextForSearch: '',
  TacGia: '',
  NXB: '',
  ThuMucSach: '',
  BST: '',
  OrderBy: '',
  MonHoc: '',
  KhoiLop: '',
  ChuDiem: '',
  KeSach: '',
  IsPublish: '',
  TaiLieuGiay: '',
  IdKho: ''
};

const orderByValues = [
  { label: 'Chọn sắp xếp', value: '' },
  { label: 'Thứ tự A - Z', value: '1' },
  { label: 'Thứ tự Z - A', value: '11' },
  { label: 'Mã sách giảm', value: '2' },
  { label: 'Mã sách tăng', value: '22' },
  { label: 'Thời gian mượn sách giảm', value: '3' },
  { label: 'Thời gian mượn sách tăng', value: '33' },
  { label: 'Năm xuất bản giảm', value: '44' },
  { label: 'Năm xuất bản tăng', value: '4' }
];

export function SearchForm(props: Props) {
  const { onChangeViewMode, viewMode, onResetForm, onSubmit: onSubmitForm, isLargeView = true } = props;

  const { register, handleSubmit, reset, setValue, control } = useForm<typeof initialValues>({
    defaultValues: initialValues
  });

  const handleNavigation = usePaginationNavigate();

  const queryConfig = useQueryConfig();

  const options = useOptionsSeach();

  const { TypeSchool } = useUser();

  const { data: storesData, isLoading: loadingStores } = useQuery({
    queryKey: ['stores'],
    queryFn: bookstoreTypeApis.getAllStores,
    staleTime: Infinity
  });

  const storesResultAll = useMemo(() => {
    if (!storesData?.data.Item) return [];

    const storeResults = storesData?.data.Item.map(({ Ten, Id }) => {
      return {
        label: Ten,
        value: Id
      };
    });

    return storeResults;
  }, [storesData?.data.Item]);

  const grade_options = useMemo(() => {
    const array: { label: string; value: string }[] = [
      {
        label: 'Chọn khối lớp',
        value: ''
      }
    ];
    if (TypeSchool) {
      for (let param in OPTIONS_BY_TYPESCHOOL) {
        if (TypeSchool.includes(param)) {
          OPTIONS_BY_TYPESCHOOL[param].forEach((element) => {
            array.push(element);
          });
        }
      }
    }
    return array;
  }, [TypeSchool]);

  useEffect(() => {
    Object.keys(queryConfig).forEach((key) => {
      if (!key) {
        return;
      }
      // @ts-ignore
      setValue(key, queryConfig[key]);
    });
  }, [queryConfig, setValue]);

  const onSubmit = handleSubmit((data) => {
    const searchResult = trim(data.TextForSearch, ' ').replace(/\s\s+/g, ' ');

    handleNavigation({ ...omitBy({ ...data, TextForSearch: searchResult }, isEmpty), page: '1' });

    onSubmitForm?.();
  });

  const handleResetField = () => {
    reset();
    handleNavigation({ page: '1' });
    onResetForm();
  };

  const handleChangeViewMode = () => {
    onChangeViewMode();
  };

  return (
    <form
      className={classNames('my-[10px] flex grid-cols-12 flex-col gap-2 bg-secondary-10 py-3 px-3.5', {
        'hidden md:grid': isLargeView
      })}
      onSubmit={onSubmit}
    >
      <div className='col-span-12 md:col-span-6 lg:col-span-3'>
        <Input
          placeholder='Nhập tên sách, mã kiểm soát, ISBN, SKU, ...'
          containerClassName='w-[100%]'
          name='TextForSearch'
          register={register}
        />
      </div>

      <div className='col-span-12 md:col-span-6 lg:col-span-3'>
        <Select items={options?.optionsTacGia || []} className='w-[100%]' register={register} name='TacGia' />
      </div>

      <div className='col-span-12 md:col-span-6 lg:col-span-3'>
        <TreeSelect items={options?.optionsTMS || []} className='w-[100%]' name='ThuMucSach' control={control} />
      </div>

      <div className='col-span-12 md:col-span-6 lg:col-span-3'>
        <Select items={options?.optionsNXB || []} className='w-[100%]' name='NXB' register={register} />
      </div>

      <div className='col-span-12 md:col-span-6 lg:col-span-3'>
        <Select items={options?.optionsBST || []} className='w-[100%]' name='BST' register={register} />
      </div>

      <div className='col-span-12 md:col-span-6 lg:col-span-3'>
        <Select items={orderByValues} className='w-[100%]' name='OrderBy' register={register} />
      </div>

      <div className='col-span-12 md:col-span-6 lg:col-span-3'>
        <Select items={options?.optionsMonHoc || []} className='w-[100%]' name='MonHoc' register={register} />
      </div>

      <div className='col-span-12 md:col-span-6 lg:col-span-3'>
        <Select items={grade_options} className='w-[100%]' name='KhoiLop' register={register} />
      </div>

      <div className='col-span-12 md:col-span-6 lg:col-span-3'>
        <Select items={options?.optionsChuDiem || []} className='w-[100%]' name='ChuDiem' register={register} />
      </div>

      <div className='col-span-12 md:col-span-6 lg:col-span-3'>
        <Select items={options?.optionsKeSach || []} className='w-[100%]' name='KeSach' register={register} />
      </div>

      <div className='col-span-12 md:col-span-6 lg:col-span-3'>
        <Select
          items={[
            { label: 'Chọn trạng thái tài liệu', value: '' },
            { label: 'Đã gửi vào MLLH', value: 'true' },
            { label: 'Chưa gửi vào MLLH', value: 'false' }
          ]}
          className='w-[100%]'
          name='IsPublish'
          register={register}
        />
      </div>

      <div className='col-span-12 md:col-span-6 lg:col-span-3'>
        <Select
          items={[
            { label: 'Chọn loại tài liệu', value: '' },
            { label: 'Tài liệu giấy', value: '1' },
            { label: 'Tài liệu giấy và số', value: '2' }
          ]}
          className='w-[100%]'
          name='TaiLieuGiay'
          register={register}
        />
      </div>
      <div className='col-span-12 md:col-span-6 lg:col-span-3'>
        <Select
          items={[{ label: 'Chọn kho sách', value: '' }, ...storesResultAll]}
          className='w-full'
          name='IdKho'
          register={register}
          disabled={!storesResultAll.length || loadingStores}
        />
      </div>
      <div className='col-span-12 flex items-center gap-2 md:col-span-6 lg:col-span-3'>
        <Button type='button' variant='secondary' className='font-semibold' onClick={handleResetField}>
          Làm mới
        </Button>

        <Button type='submit' variant='default' className='font-semibold'>
          Tìm kiếm
        </Button>

        <div className='flex items-center'>
          <button
            type='button'
            className={classNames(
              'flex transform items-center justify-center gap-1 rounded-l-md py-2.5 px-2.5 text-white',
              {
                'bg-primary-50': viewMode === 'grid',
                'bg-primary-10': viewMode === 'list'
              }
            )}
            onClick={handleChangeViewMode}
          >
            <TableOutlined />
          </button>

          <button
            onClick={handleChangeViewMode}
            type='button'
            className={classNames(
              '0 flex transform items-center justify-center gap-1 rounded-r-md py-2.5 px-2.5 text-white',
              {
                'bg-primary-50': viewMode === 'list',
                'bg-primary-10': viewMode === 'grid'
              }
            )}
          >
            <UnorderedListOutlined />
          </button>
        </div>
      </div>
    </form>
  );
}

export default SearchForm;
