import { SetStateAction } from 'react';
import useStateWithCallback from './useStateWithCallback';

type InitialState<S> = S | (() => S);

type SetStateActionAsync<S> = (value: SetStateAction<S>) => Promise<S>;

type Return<S> = [S, SetStateActionAsync<S>];

function useStateAsync<S>(initialState: InitialState<S>): Return<S> {
  const [state, setState] = useStateWithCallback(initialState);

  const setStateAsync: SetStateActionAsync<S> = (value) => {
    return new Promise((resovle) => {
      setState(value, resovle);
    });
  };

  return [state, setStateAsync];
}

export default useStateAsync;
