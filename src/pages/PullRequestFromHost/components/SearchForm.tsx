import { useQuery } from '@tanstack/react-query';
import { pullRequestApis } from 'apis';
import { Button, Input, Select } from 'components';
import { FormEvent, useState, type ChangeEvent } from 'react';

type Params = {
  sortBy?: string | undefined;
  libraryId: string;
  searchForms: {
    operator: '&' | '|' | '!';
    searchField: string;
    value: string;
  }[];
};

type Props = { onSearch: (value: Params) => void; loading: boolean; onChangeUnit: (unitId: string) => void };

const SEARCH_OPTIONS = [
  { value: '&', label: 'AND (Và)' },
  { value: '|', label: 'OR (Hoặc)' },
  { value: '!', label: 'NOT (Không phải)' }
];

const SEARCH_FIELDS = [
  { value: 'all', label: 'Tất cả' },
  { value: 'title', label: 'Tên nhan đề' },
  { value: 'author', label: 'Tác giả' },
  { value: 'isbn', label: 'ISBN' },
  { value: 'language', label: 'Ngôn ngữ' },
  { value: 'publish_date', label: 'Năm xuất bản' },
  { value: 'publisher', label: 'Nhà xuất bản' },
  { value: 'placeofpublication', label: 'Nơi xuất bản' },
  { value: 'ddc', label: 'DDC' },
  { value: 'day19', label: '19 Dãy' },
  { value: 'bookstore', label: 'Kho sách' }
];

const SORT_OPTIONS = [
  { value: 'sortTitleIncrease', label: 'Nhan đề (Tăng dần)' },
  { value: 'sortTitleDecrease', label: 'Nhan đề (Giảm dần)' },
  { value: 'sortPublishYearIncrease', label: 'Năm xuất bản (Tăng dần)' },
  { value: 'sortPublishYearDecrease', label: 'Năm xuất bản (Giảm dần)' },
  { value: 'sortCreateDateTimeDecrease', label: 'Ngày tạo mới nhất' },
  { value: '', label: 'Không sắp xếp' }
];

const SearchForm = (props: Props) => {
  const { onSearch, onChangeUnit } = props;

  const [searchAdvance, setSearchAdvance] = useState<boolean>(false);
  const { data, isLoading } = useQuery({
    queryKey: ['getListOfUnits'],
    queryFn: pullRequestApis.getListOfUnits
  });

  const listOfUnits = data?.data.Item.DonVi || [];

  const [params, setParams] = useState<Params>({
    sortBy: '',
    libraryId: '',
    searchForms: [{ operator: '&', searchField: 'all', value: '' }]
  });

  const isEnabled = params.searchForms.some(({ value }) => value);

  const handleChangeLibrary = (e: ChangeEvent<HTMLSelectElement>) => {
    setParams((prevState) => {
      return { ...prevState, libraryId: e.target.value || '' };
    });

    onChangeUnit(e.target.value);
  };

  const handleChangeSearchField = (e: ChangeEvent<HTMLSelectElement>, index: number) => {
    if (!e) return;

    const tempArray = [...params.searchForms];

    const { value } = e.target;

    tempArray.forEach(() => {
      tempArray[index].searchField = value;
    });

    setParams((prevState) => {
      return {
        ...prevState,
        searchForms: tempArray
      };
    });
  };

  const handleChangeSearchOption = (e: ChangeEvent<HTMLSelectElement>, index: number) => {
    if (!e) return;

    const tempArray = [...params.searchForms];

    const { value } = e.target;

    tempArray.forEach((_) => {
      tempArray[index].operator = value as '&' | '|' | '!';
    });

    setParams((prevState) => {
      return {
        ...prevState,
        searchForms: tempArray
      };
    });
  };

  const handleChangeTextSearch = (e: ChangeEvent<HTMLInputElement>, index: number) => {
    if (!e) return;

    const tempArray = [...params.searchForms];

    const { value } = e.target;

    tempArray.forEach((_) => {
      tempArray[index].value = value;
    });

    setParams((prevState) => {
      return {
        ...prevState,
        searchForms: tempArray
      };
    });
  };

  const handleSearchOpac = (e?: FormEvent<HTMLFormElement>) => {
    e?.preventDefault();

    setParams((prevState) => {
      return {
        ...prevState,
        page: 1
      };
    });

    onSearch(params);
  };

  const handleChangeSort = (e: ChangeEvent<HTMLSelectElement>) => {
    if (!e) return;

    const { value } = e.target;

    const req: Partial<{
      SortBookName: boolean;
      SortPublishDate: boolean;
      SortTime: boolean;
    }> = {};

    switch (value) {
      case 'sortTitleIncrease':
        req.SortBookName = true;
        break;

      case 'sortTitleDecrease':
        req.SortBookName = false;
        break;

      case 'sortPublishYearIncrease':
        req.SortPublishDate = true;
        break;

      case 'sortPublishYearDecrease':
        req.SortPublishDate = true;
        break;

      case 'sortCreateDateTimeIncrease':
        req.SortTime = true;
        break;

      case 'sortCreateDateTimeDecrease':
        req.SortTime = true;
        break;

      case '':
        break;

      default:
        break;
    }

    setParams((prevState) => {
      return {
        ...prevState,
        sortBy: value
      };
    });
  };

  const handleRefresh = () => {
    setParams({
      libraryId: '',
      sortBy: '',
      searchForms: [{ operator: '&', searchField: 'all', value: '' }]
    });
  };

  const handleSearchAdvance = () => {
    setSearchAdvance(true);

    if (params.searchForms.length === 1) {
      setParams((prevState) => {
        return { ...prevState, searchForms: [...params.searchForms, { operator: '&', searchField: 'all', value: '' }] };
      });
    }
  };

  return (
    <>
      {searchAdvance ? (
        <>
          {/* advance search */}
          {params.searchForms.map((x, index) => (
            <div className='mt-2 flex flex-col items-center gap-1 md:flex-row' key={index}>
              {index === 0 ? (
                <Select
                  className='w-1/5'
                  value={params.libraryId}
                  items={[{ value: '', label: 'Tất cả thư viện' }, ...listOfUnits]}
                  onChange={handleChangeLibrary}
                  disabled={isLoading}
                />
              ) : (
                <Select
                  className='w-1/5'
                  onChange={(e) => handleChangeSearchOption(e, index)}
                  value={params.searchForms[index]?.operator}
                  items={SEARCH_OPTIONS}
                />
              )}

              <Select
                className='w-1/5'
                onChange={(e) => handleChangeSearchField(e, 1)}
                value={params.searchForms[index]?.searchField}
                items={SEARCH_FIELDS}
              />

              <div className='flex flex-grow items-center'>
                <Input
                  containerClassName='shadow-sm rounded w-full'
                  value={params.searchForms[index]?.value}
                  onChange={(e) => handleChangeTextSearch(e as ChangeEvent<HTMLInputElement>, index)}
                />

                <div className='flex max-w-[100px] items-center gap-1'>
                  {params.searchForms.length - 1 === index && (
                    <>
                      <button
                        className='text-[20px] text-primary-10'
                        onClick={() =>
                          setParams((prevState) => {
                            return {
                              ...prevState,
                              searchForms: [...params.searchForms, { operator: '&', searchField: 'all', value: '' }]
                            };
                          })
                        }
                      >
                        [+]
                      </button>

                      {index !== 1 && (
                        <button
                          className='text-[20px] text-primary-10'
                          onClick={() => {
                            if (index === 1) {
                              return;
                            }

                            const array = [...params.searchForms];
                            array.splice(index, 1);

                            setParams((prevState) => {
                              return {
                                ...prevState,
                                searchForms: array
                              };
                            });
                          }}
                        >
                          [-]
                        </button>
                      )}
                    </>
                  )}
                </div>
              </div>
            </div>
          ))}

          <form className='mt-2 flex items-center' onSubmit={handleSearchOpac}>
            <p className='mr-1 w-1/5 whitespace-normal pr-1 text-right text-[14px] md:whitespace-nowrap'>
              Sắp xếp theo:
            </p>

            <select
              className='col-span-10 w-1/5 rounded-md border px-1 py-2.5 text-[14px] text-primary-10 shadow-sm outline-none focus:border-primary-10'
              onChange={handleChangeSort}
              value={params.sortBy}
            >
              {SORT_OPTIONS.map(({ label, value }) => (
                <option value={value} key={value}>
                  {label}
                </option>
              ))}
            </select>

            <div className='col-span-12 flex items-center gap-1 md:col-span-3'>
              <Button
                className={`ml-1 w-[121px] rounded-md py-2 text-[14px] font-bold ${
                  !isEnabled ? 'bg-primary-80 cursor-not-allowed' : ''
                }`}
                disabled={!isEnabled}
                onClick={handleRefresh}
                type='button'
                variant='secondary'
              >
                LÀM MỚI
              </Button>

              <Button
                className='w-[121px] rounded-md bg-primary-10 py-2 text-[14px] font-bold text-white'
                type='submit'
              >
                TÌM KIẾM
              </Button>

              <button
                className='ml-3 flex shrink-0 items-center gap-2 font-bold'
                type='button'
                onClick={() => setSearchAdvance(false)}
              >
                Thu gọn{' '}
                <svg
                  width={13}
                  height={14}
                  viewBox='0 0 13 14'
                  fill='none'
                  xmlns='http://www.w3.org/2000/svg'
                  className='rotate-180'
                >
                  <path
                    d='M6.19506 7.06513L0.147338 1.01128C0.0469557 0.894064 -0.00549832 0.743285 0.000458343 0.589073C0.00641405 0.434862 0.0703436 0.288577 0.179469 0.179452C0.288594 0.0703266 0.434879 0.00639806 0.58909 0.00044142C0.743301 -0.00551522 0.894081 0.0469393 1.0113 0.147322L6.5627 5.69873L11.9548 0.306635C12.0696 0.192512 12.2249 0.128456 12.3868 0.128456C12.5487 0.128456 12.7039 0.192512 12.8188 0.306635C12.8762 0.363597 12.9218 0.431365 12.9529 0.506033C12.984 0.580701 13 0.660789 13 0.741678C13 0.822567 12.984 0.902655 12.9529 0.977323C12.9218 1.05199 12.8762 1.11976 12.8188 1.17672L6.93034 7.06513C6.83272 7.16242 6.70052 7.21704 6.5627 7.21704C6.42488 7.21704 6.29268 7.16241 6.19506 7.06513Z'
                    fill='#3472A2'
                  />
                  <path
                    d='M6.19508 12.9351L0.147355 6.88128C0.0469719 6.76406 -0.00548211 6.61328 0.000473602 6.45907C0.00643027 6.30486 0.0703589 6.15857 0.179484 6.04945C0.28861 5.94032 0.434894 5.87639 0.589106 5.87044C0.743317 5.86448 0.894097 5.91693 1.01131 6.01732L6.56272 11.5687L11.9548 6.17663C12.0696 6.06251 12.2249 5.99845 12.3868 5.99845C12.5487 5.99845 12.704 6.06251 12.8188 6.17663C12.9329 6.29143 12.9969 6.44673 12.9969 6.60861C12.9969 6.77049 12.9329 6.92579 12.8188 7.04059L6.93036 12.9351C6.83274 13.0324 6.70054 13.087 6.56272 13.087C6.4249 13.087 6.2927 13.0324 6.19508 12.9351Z'
                    fill='#3472A2'
                  />
                </svg>
              </button>
            </div>
          </form>
        </>
      ) : (
        // normal search
        <form className='flex flex-grow flex-col gap-1 md:flex-row' onSubmit={handleSearchOpac}>
          <Select
            className='w-1/5'
            value={params.libraryId}
            items={[{ value: '', label: 'Tất cả thư viện' }, ...listOfUnits]}
            onChange={handleChangeLibrary}
          />

          <Select
            value={params.searchForms[0]?.searchField}
            className='w-1/5'
            onChange={(e) => handleChangeSearchField(e, 0)}
            items={SEARCH_FIELDS}
          />

          <Input
            placeholder='Bạn muốn tìm gì?'
            containerClassName='flex-grow relative'
            value={params.searchForms[0]?.value}
            onChange={(e) => handleChangeTextSearch(e as ChangeEvent<HTMLInputElement>, 0)}
            right={
              <Button type='submit' className={`absolute right-0 top-0 h-[51px]`}>
                <svg width={18} height={18} viewBox='0 0 18 18' fill='none' xmlns='http://www.w3.org/2000/svg'>
                  <path
                    fillRule='evenodd'
                    clipRule='evenodd'
                    d='M12.4728 11H11.6746L11.3752 10.7C12.373 9.6 12.9717 8.1 12.9717 6.5C12.9717 2.9 10.078 0 6.48587 0C2.8937 0 0 2.9 0 6.5C0 10.1 2.8937 13 6.48587 13C8.08239 13 9.57913 12.4 10.6767 11.4L10.9761 11.7V12.5L15.9652 17.5L17.462 16L12.4728 11ZM6.48587 11C3.9913 11 1.99565 9 1.99565 6.5C1.99565 4 3.9913 2 6.48587 2C8.98043 2 10.9761 4 10.9761 6.5C10.9761 9 8.98043 11 6.48587 11Z'
                    fill='white'
                  />
                </svg>
              </Button>
            }
          />

          <Button className='shrink-0' onClick={handleSearchAdvance}>
            Tìm kiếm nâng cao
          </Button>
        </form>
      )}
    </>
  );
};

export default SearchForm;
