import { DeleteFilled, DownloadOutlined } from '@ant-design/icons';
import { useMutation } from '@tanstack/react-query';
import { Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { studentApis } from 'apis';
import classNames from 'classnames';
import { Button, Empty, Loading, ProgressBar, Title } from 'components';
import { path } from 'constants/path';
import { uniqueId } from 'lodash';
import { useMemo, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';

let locale = {
  emptyText: 'Không có dữ liệu!'
};

const AddStudentByExcelFilePage = () => {
  const [step, setStep] = useState<number>(1);
  const [link, setLink] = useState<string>('');
  const [rowPreviewData, setPreviewRowData] = useState<UploadFileData['RawDataList']>([[]]);
  const [rowSavedData, setRowSaveData] = useState<Pick<UploadFileData, 'ListFail' | 'ListSuccess' | 'ListShow'>>({
    ListFail: [],
    ListSuccess: [],
    ListShow: []
  });
  const [page, setPage] = useState(1);
  const [paginationSize, setPaginationSize] = useState(10);

  const fileInutRef = useRef<HTMLInputElement | null>(null);
  const ref = useRef<HTMLDivElement | null>(null);

  const navigate = useNavigate();

  const { mutate: UploadFileData, isLoading: uploadingExcelFile } = useMutation({
    mutationFn: studentApis.UploadFileData,
    onSuccess: (data) => {
      setPreviewRowData(data.data.Item.RawDataList);
      setStep(2);
    }
  });

  const { mutate: saveExcelFile, isLoading: savingExcelFile } = useMutation({
    mutationFn: studentApis.saveExcelFile,
    onSuccess: (data) => {
      ref.current?.scrollIntoView({ behavior: 'smooth' });

      setStep(3);

      const byteArray = new Uint8Array(data.data.Item.MemoryStream);

      const blob = new Blob([byteArray], { type: 'application/xls' });

      const link = URL.createObjectURL(blob);

      setLink(link);

      setPreviewRowData([]);

      setRowSaveData((prevState) => {
        return {
          ...prevState,
          ListFail: data.data.Item.ListFail,
          ListSuccess: data.data.Item.ListSuccess,
          ListShow: data.data.Item.ListShow
        };
      });
    }
  });

  const disable = useMemo(() => savingExcelFile || uploadingExcelFile, [savingExcelFile, uploadingExcelFile]);

  const rowData = useMemo(() => {
    if (step === 2 && !!rowPreviewData.length) {
      return rowPreviewData;
    } else if (step === 3 && !!rowSavedData.ListShow.length) {
      return rowSavedData.ListShow;
    }
  }, [rowPreviewData, rowSavedData.ListShow, step]);

  const columns: ColumnsType<any> | undefined = useMemo(() => {
    if (rowData) {
      for (let index = 0; index < rowData?.length; index++) {
        return [
          {
            title: 'STT',
            dataIndex: 'stt',
            key: 'stt',
            render: (value, record, j: number) => (page - 1) * paginationSize + j + 1
          },
          {
            title: 'Tên học sinh',
            dataIndex: 'Ten',
            key: 'Ten',
            render: (value, record) => {
              return (
                <>
                  {step === 2 && record[1]}

                  {step === 3 && (
                    <p
                      className={classNames('', {
                        'min-h-38 errorBackGround py-2': record[0].Color
                      })}
                    >
                      {record[0].Value}
                    </p>
                  )}
                </>
              );
            }
          },
          {
            title: 'Mã học sinh',
            dataIndex: 'MaThanhVien',
            key: 'MaThanhVien',
            render: (value, record) => {
              return (
                <>
                  {step === 2 && record[2]}

                  {step === 3 && (
                    <p
                      className={classNames('', {
                        'min-h-38 errorBackGround py-2': record[1].Color
                      })}
                    >
                      {record[1].Value}
                    </p>
                  )}
                </>
              );
            }
          },
          {
            title: 'Lớp học',
            dataIndex: 'LopHoc',
            key: 'LopHoc',
            render: (value, record) => {
              return (
                <>
                  {step === 2 && record[6]}

                  {step === 3 && (
                    <p
                      className={classNames('', {
                        'min-h-38 errorBackGround py-2': record[5].Color
                      })}
                    >
                      {record[5].Value}
                    </p>
                  )}
                </>
              );
            }
          },
          {
            title: 'Số điện thoại',
            dataIndex: 'SDT',
            key: 'SDT',
            render: (value, record) => {
              return (
                <>
                  {step === 2 && record[8]}

                  {step === 3 && <p>{record[6].Value}</p>}
                </>
              );
            }
          },
          {
            title: 'Năm học',
            dataIndex: 'NamHoc',
            key: 'NamHoc',
            render: (value, record) => {
              return (
                <>
                  {step === 2 && record[5]}

                  {step === 3 && (
                    <p
                      className={classNames('', {
                        'min-h-38 errorBackGround py-2': record[4].Color
                      })}
                    >
                      {record[4].Value}
                    </p>
                  )}
                </>
              );
            }
          },
          {
            title: 'Ngày Sinh',
            dataIndex: 'NgaySinh',
            key: 'NgaySinh',
            render: (value, record) => {
              return (
                <>
                  {step === 2 && record[4]}

                  {step === 3 && (
                    <p
                      className={classNames('', {
                        'min-h-38 errorBackGround py-2': record[3].Color
                      })}
                    >
                      {record[3].Value}
                    </p>
                  )}
                </>
              );
            }
          },
          {
            title: 'Địa chỉ',
            dataIndex: 'DiaChi',
            key: 'DiaChi',
            render: (value, record) => {
              return (
                <>
                  {step === 2 && record[7]}

                  {step === 3 && (
                    <p
                      className={classNames('', {
                        'min-h-38 errorBackGround py-2': record[7].Color
                      })}
                    >
                      {record[7].Value}
                    </p>
                  )}
                </>
              );
            }
          },
          {
            title: 'Giới tính',
            dataIndex: 'GioiTinh',
            key: 'GioiTinh',
            render: (value, record) => {
              return (
                <>
                  {step === 2 && record[3]}

                  {step === 3 && (
                    <p
                      className={classNames('', {
                        'min-h-38 errorBackGround py-2': record[2].Color
                      })}
                    >
                      {record[2].Value}
                    </p>
                  )}
                </>
              );
            }
          },
          {
            title: (step === 2 && 'Hành động') || (step === 3 && 'Nội dung lỗi'),
            dataIndex: 'actions',
            key: 'actions',
            render: (value, record, index) => {
              return (
                <div className='max-w-[300px]'>
                  {step === 2 && (
                    <button
                      className='mx-2'
                      onClick={(e) => {
                        let array = [...rowPreviewData];
                        array.splice(index, 1);
                        setPreviewRowData(array);

                        if (paginationSize * (page - 1) >= array.length) {
                          setPage(page - 1);
                        }
                      }}
                    >
                      <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
                    </button>
                  )}

                  {step === 3 && <p className='ml-[50px] text-danger-10'>{record[8].Value}</p>}
                </div>
              );
            }
          }
        ];
      }
    }
  }, [page, rowData, rowPreviewData, step]);

  const handleOnFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files?.[0];

    const form = new FormData();

    if (fileFromLocal) {
      form.append('file', fileFromLocal);
      UploadFileData(form);
    }
  };

  const handleAccessFileInputRef = () => {
    fileInutRef.current?.click();
  };

  const handleBack = () => {
    if (step === 2) {
      setStep(1);
      setPreviewRowData([]);
    } else {
      navigate(path.hocsinh);
    }
  };

  const handleSubmit = () => {
    saveExcelFile(rowPreviewData);
  };

  console.log('page', page);

  return (
    <div className='p-5' ref={ref}>
      <Title title='Thêm mới học sinh từ file Excel' />

      <div className='my-10'>
        <ProgressBar activeStep={step} type='excel' />
      </div>

      {step === 1 && (
        <div className='flex items-center justify-center rounded-md bg-tertiary-10 py-4'>
          <a href={`/Tempalates/MauHS.xls`} download='MauHS.xls'>
            <Button className='bg-tertiary-20 hover:bg-tertiary-20/30' disabled={disable}>
              <DownloadOutlined />
              File excel mẫu
            </Button>
          </a>

          <input
            type='file'
            accept='.csv,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel'
            className='hidden'
            ref={fileInutRef}
            onChange={handleOnFileChange}
          />

          <Button variant='default' className='ml-2' onClick={handleAccessFileInputRef} disabled={disable}>
            Chọn tệp
          </Button>
        </div>
      )}

      {step > 1 && (
        <div>
          {step === 2 && (
            <>
              <p>
                {!rowPreviewData?.length ? (
                  <>
                    <p className='font-bold'>Không có học sinh nào được nhận diện. Hãy chọn file khác!</p>

                    <input
                      type='file'
                      accept='.csv,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel'
                      className='hidden'
                      ref={fileInutRef}
                      onChange={handleOnFileChange}
                    />

                    <Button variant='default' className='ml-2' onClick={handleAccessFileInputRef} disabled={disable}>
                      Chọn tệp
                    </Button>
                  </>
                ) : (
                  <>
                    Tổng số học sinh: <span className='font-bold'>{rowPreviewData?.length}</span>
                  </>
                )}
              </p>

              <h3 className='text-primary-10'>DANH SÁCH HỌC SINH</h3>
            </>
          )}

          {step === 3 && (
            <>
              <p className='text-tertiary-30'>
                Lưu thành công: <span className='font-bold'>{' ' + rowSavedData.ListSuccess.length} học sinh</span>
              </p>

              <p className='gap-1 text-danger-10'>
                Lưu thất bại: <span className='font-bold'>{' ' + rowSavedData.ListFail.length} học sinh</span>
                {!!rowSavedData.ListShow.length && (
                  <a href={link} download='DanhSachHocSinhLoi.xls'>
                    <Button variant='danger' style={{ display: 'inline-block' }} className='ml-2'>
                      <DownloadOutlined />
                      Tải file lỗi
                    </Button>
                  </a>
                )}
              </p>

              {!!rowSavedData.ListShow.length && (
                <h3 className='uppercase text-primary-10'>Danh sách học sinh bị lỗi</h3>
              )}
            </>
          )}

          <div className='my-6'>
            {!!rowSavedData.ListShow.length && step === 3 && (
              <Table
                pagination={{
                  onChange(current, pageSize) {
                    setPage(current);
                    setPaginationSize(pageSize);
                  },
                  hideOnSinglePage: true,
                  showSizeChanger: false,
                  current: page,
                  pageSize: 10,
                  total: rowSavedData.ListShow.length
                }}
                loading={disable}
                columns={columns}
                dataSource={rowData || []}
                scroll={{ x: 1200 }}
                rowKey={() => uniqueId()}
                className='custom-table'
                locale={{
                  emptyText: () => <Empty label={locale.emptyText} />
                }}
                bordered
              />
            )}

            {step === 2 && (
              <Table
                pagination={{
                  onChange(current, pageSize) {
                    setPage(current);
                    setPaginationSize(pageSize);
                  },
                  hideOnSinglePage: true,
                  showSizeChanger: false,
                  current: page,
                  pageSize: 10,
                  total: rowData?.length
                }}
                loading={disable}
                columns={columns}
                dataSource={rowData || []}
                scroll={{ x: 1200 }}
                rowKey={() => uniqueId()}
                className='custom-table'
                locale={{
                  emptyText: () => <Empty label={locale.emptyText} />
                }}
                bordered
              />
            )}
          </div>
        </div>
      )}

      <div className='mr-10 mt-2 flex items-center justify-end'>
        {step < 3 && (
          <Button variant='secondary' onClick={handleBack} disabled={disable}>
            Quay về
          </Button>
        )}

        {!!rowPreviewData?.length && (
          <>
            {step === 2 && (
              <Button variant='default' className='ml-2' onClick={handleSubmit} disabled={disable}>
                Tiếp tục
              </Button>
            )}
          </>
        )}

        {step === 3 && (
          <Button variant='default' className='ml-2' onClick={handleBack}>
            Hoàn tất
          </Button>
        )}
      </div>

      <Loading open={disable} />
    </div>
  );
};

export default AddStudentByExcelFilePage;
