export const MacDinhThuTuKhoSach = [
  'STT',
  'TenSach',
  'TenSongNgu',
  'ISBN',
  'SoLuong',
  'IdTrangThai',
  'IdNguonCungCap',
  'SoChungTu',
  'SoVaoSoTongQuat',
  'NgayVaoSo_String',
  'Day19',
  'DDC',
  'TacGia',
  'TacGiaPhu',
  'IdNhaXuatBan',
  'IdNoiXuatBan',
  'NamXuatBan',
  'IdMonHoc',
  'KhoiLopString',
  'IdChuDiem',
  'IdKeSach',
  'IdThuMucSach',
  'SoTrang',
  'IdNgonNgu',
  'XuatXu',
  'NguoiBienDich',
  'TaiBan',
  'SKU',
  'LLC',
  'IdMaMau',
  'PhiMuonSach',
  'GiaBia',
  'MoTaVatLy',
  'CoSach',
  'TaiLieuDinhKem',
  'MinhHoa',
  'TungThu',
  'PhuChu',
  'TomTat'
];

export const MacDinhThuTuKhoSach_SCB = [
  'NgayVaoSo_String',
  'STT',
  'SoTTBanSach',
  'TacGia',
  'TenSach',
  'IdNhaXuatBan',
  'IdNoiXuatBan',
  'NamXuatBan',
  'PhatKhong',
  'Mua',
  'MonLoai',
  'SoVaoSoTongQuat',
  'NgayVaSoBienBanXuat',
  'KiemKe1',
  'KiemKe2',
  'KiemKe3',
  'KiemKe4',
  'KiemKe5',
  'GhiChu',
  'IdNguonCungCap',
  'SoChungTu',
  'TenSongNgu',
  'ISBN',
  'TacGiaPhu',
  'IdMonHoc',
  'KhoiLopString',
  'IdChuDiem',
  'IdKeSach',
  'IdThuMucSach',
  'SoTrang',
  'IdNgonNgu',
  'XuatXu',
  'NguoiBienDich',
  'TaiBan',
  'SKU',
  'LLC',
  'IdMaMau',
  'PhiMuonSach',
  'MoTaVatLy',
  'CoSach',
  'TaiLieuDinhKem',
  'MinhHoa',
  'TungThu',
  'PhuChu',
  'TomTat'
];

export const MacDinhThuTuKhoSach_SCB_SGK = [
  'NgayVaoSo_String',
  'STT',
  'SoChungTu',
  'SoVaoSoTongQuat',
  'MonLoai',
  'NamXuatBan',
  'SoLuong',
  'GiaBia',
  'ToTalPrice',
  'KiemKe1',
  'KiemKe2',
  'KiemKe3',
  'KiemKe4',
  'KiemKe5',
  'KiemKe6',
  'KiemKe7',
  'KiemKe8',
  'KiemKe9',
  'KiemKe10',
  'GhiChu',
  'TacGia',
  'IdNguonCungCap',
  'IdNhaXuatBan',
  'IdNoiXuatBan',
  'TenSongNgu',
  'ISBN',
  'TacGiaPhu',
  'IdMonHoc',
  'KhoiLopString',
  'IdChuDiem',
  'IdKeSach',
  'IdThuMucSach',
  'SoTrang',
  'IdNgonNgu',
  'XuatXu',
  'NguoiBienDich',
  'TaiBan',
  'SKU',
  'LLC',
  'IdMaMau',
  'PhiMuonSach',
  'MoTaVatLy',
  'CoSach',
  'TaiLieuDinhKem',
  'MinhHoa',
  'TungThu',
  'PhuChu',
  'TomTat'
];
