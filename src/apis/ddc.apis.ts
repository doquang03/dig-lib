import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/DDC`;

type GetDDCVars = {
  Keyword?: string;
  Sort: number;
};

type GetDataImportSave = {
  FileErrorDownload: {
    ContentType: string;
    FileName: string;
    MemoryStream: any;
  };
} & Pick<UploadFileData<DDC>, 'ListSuccess' | 'ListFail' | 'ListShow'>;

const ddcApis = {
  getDDCList: (payload: GetDDCVars) => {
    const { Keyword = '', Sort = 0 } = payload;

    return httpThuVien.post<ResponseApi<DDC[]>>(`${endPoint}/GetDataBySearch`, {
      Keyword,
      Sort
    });
  },

  getDDC: (id: string) => httpThuVien.get<ResponseApi<DDC>>(`${endPoint}/${id}`),
  deleteDCC: (id: string) => httpThuVien.delete<ResponseApi<{}>>(`${endPoint}/${id}`),
  createDDC: (payload: Omit<DDC, 'Id' | 'CreateDatetTime'>) =>
    httpThuVien.post<ResponseApi<{}>>(`${endPoint}`, payload),
  updateDDC: (payload: Omit<DDC, 'CreateDatetTime'>) => httpThuVien.put<ResponseApi<{}>>(`${endPoint}`, payload),
  uploadFileData: (file: FormData) =>
    httpThuVien.post<UploadFileData<DDC>>(`${endPoint}/PreviewImport`, file, {
      headers: { 'Content-Type': 'multipart/form-data', Accept: '*/*' }
    }),
  saveExcelFile: (payload: string[][]) =>
    httpThuVien.post<ResponseApi<GetDataImportSave>>(`${endPoint}/ImportSave`, payload)
};

export default ddcApis;
