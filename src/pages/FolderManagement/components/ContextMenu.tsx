import { HTMLAttributes, ReactNode, forwardRef } from 'react';

type Props = {
  x: number;
  y: number;
  show: boolean;
  children: ReactNode;
} & HTMLAttributes<HTMLDivElement>;

const ContextMenu = forwardRef<HTMLDivElement, Props>((props, ref) => {
  const { x, y, show, children, ...rest } = props;

  return (
    <>
      <div
        style={{
          top: y,
          left: x,
          position: 'absolute'
        }}
        {...rest}
        className={`z-10 shrink-0 whitespace-nowrap rounded-md bg-white p-2 shadow-inner ${
          show
            ? 'visible h-auto translate-x-1 translate-y-1 transition-transform duration-100 ease-linear'
            : 'invisible -translate-x-1 translate-y-1 opacity-0 duration-100 ease-out'
        } ${!x && !y ? 'hidden opacity-0' : ''}`}
        ref={ref}
      >
        {children}
      </div>
    </>
  );
});

export default ContextMenu;
