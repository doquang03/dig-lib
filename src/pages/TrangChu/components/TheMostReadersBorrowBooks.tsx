import { Table } from 'antd';
import { ColumnsType } from 'antd/es/table';

function TheMostReadersBorrowBooks({ data }: { data: TheMostBorrowingReaders[] }) {
  const cols: ColumnsType<TheMostBorrowingReaders> = [
    {
      title: 'Tên bạn đọc',
      dataIndex: 'TenThanhVien',
      ellipsis: true
    },
    {
      title: 'Lớp',
      dataIndex: 'LopTo'
    },
    {
      title: 'Số lượt',
      dataIndex: 'Value'
    }
  ];

  return (
    <div className='my-3 rounded-xl bg-white p-2 shadow-md'>
      <div className='flex items-center'>
        <h3 className='text-xl font-bold text-primary-10'>BẠN ĐỌC MƯỢN SÁCH NHIỀU NHẤT</h3>
      </div>

      <Table
        rowKey={(row) => row.TenThanhVien}
        columns={cols}
        className='custom-table my-3 h-[27rem] overflow-y-auto'
        pagination={false}
        dataSource={data || []}
        bordered
      />
    </div>
  );
}

export default TheMostReadersBorrowBooks;
