import { ExportReport, InteractionRate, NoteBookReport, NoteRentingBooks } from '../../components';

const ExportReportManagement = () => {
  return (
    <div className='flex flex-col gap-4 p-5'>
      <ExportReport />

      <NoteBookReport />

      <NoteRentingBooks />

      <InteractionRate />
    </div>
  );
};

export default ExportReportManagement;
