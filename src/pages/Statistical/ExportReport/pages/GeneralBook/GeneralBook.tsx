import { useMutation, useQuery } from '@tanstack/react-query';
import { TabsProps } from 'antd';
import { statistic } from 'apis';
import { Button, Loading, Tabs } from 'components';
import { useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { DocumentStoreBySchoolYear, TotalDocuments } from './components';

type TabItem = 'part1' | 'part2' | 'part3';

const GeneralBook = () => {
  const [activeTab, setActiveTab] = useState<TabItem>('part1');

  const navigate = useNavigate();

  const { state } = useLocation();
  const YearFrom = state ? state.YearFrom : new Date().getFullYear();
  const YearTo = state ? state.YearTo : new Date().getFullYear();

  const { data, isFetching: isLoading } = useQuery({
    queryFn: () => statistic.GetData_SoDKTQKho({ YearFrom: +YearFrom, YearTo: +YearTo, currentPage: 1 }),
    queryKey: ['GetData_SoDKTQKho', YearFrom, YearTo]
  });

  const { mutate: downloadSoTongQuat, isLoading: loadingDownloadSoTongQuat } = useMutation({
    mutationFn: () =>
      statistic.downloadSoDangKyTongQuatKho({
        YearFrom: YearFrom,
        YearTo: YearTo,
        Down: true
      }),
    onSuccess: (data) => {
      toast.success('Tải sổ tổng quát thành công');
      const blob = new Blob([data.data, { type: 'application/xls' }]);
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = 'SoDKTQ_Kho.xls';
      link.click();
    }
  });

  const items: TabsProps['items'] = [
    {
      key: 'part1',
      label: `Phần 1`,
      children: <TotalDocuments documentType='import' data={data} />,
      forceRender: true
    },
    {
      key: 'part2',
      label: `Phần 2`,
      children: <TotalDocuments documentType='export' data={data} />,
      forceRender: false
    },
    {
      key: 'part3',
      label: `Phần 3`,
      children: <DocumentStoreBySchoolYear data={data} />,
      forceRender: false
    }
  ];

  const handleChangeTab = (activeTab: string) => setActiveTab(activeTab as TabItem);

  return (
    <div className='py-5'>
      <Tabs items={items} activeKey={activeTab} onChange={handleChangeTab} destroyInactiveTabPane={true} />

      <div className='mt-3 flex items-center justify-end gap-2'>
        <Button variant='secondary' onClick={() => navigate(-1)} loading={loadingDownloadSoTongQuat} type='button'>
          Quay về
        </Button>

        <Button
          onClick={() => {
            downloadSoTongQuat();
          }}
          loading={loadingDownloadSoTongQuat}
          type='button'
        >
          Tải xuống
        </Button>
      </div>
      <Loading open={isLoading}></Loading>
    </div>
  );
};

export default GeneralBook;
