import { CameraFilled } from '@ant-design/icons';
import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Checkbox } from 'antd';
import { teacherApis } from 'apis';
import { AxiosError } from 'axios';
import classNames from 'classnames';
import { Button, DatePicker, Input, Loading, ModalDelete, Select, Title, TitleDelete } from 'components';
import { CLASSIFY_OPTIONS, EUser, GENDER_OPTIONS, STATUS_OPTIONS, WORK_YEAR_OPTIONS } from 'constants/options';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import 'css/AddTeacher.css';
import dayjs from 'dayjs';
import { useMemo, useRef, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router';
import { useSearchParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { teacherSchema, teacherSchemaCollapse } from 'utils/rules';
import { convertDate, getAvatarUrl } from 'utils/utils';
import * as yup from 'yup';
import locale from 'antd/es/date-picker/locale/vi_VN';

type TeacherForm = yup.InferType<typeof teacherSchema>;

const initialTeacherValue = {
  name: '',
  id: '',
  address: '',
  email: '',
  dateOfBirth: '',
  gender: '',
  workYear: '',
  phone: ''
};

type Mock = {
  Mail: string;
  GioiTinh: string;
  Id: number;
  Ten: string;
  TrangThai: number;
  NgaySinh: string;
  DaTra: boolean;
};

const AddTeacherPage = () => {
  const { teacherId } = useParams();
  const [searchParams] = useSearchParams();
  const isLibrary = searchParams.get('type') === 'Library' ? true : false;
  const [file, setFile] = useState<File>();

  const [stateAdvance, setStateAdvance] = useState(false);
  const [currentImage, setCurrentImage] = useState();
  const [teacher, setTeacher] = useState<Mock>();
  const [isDelete, setIsDelete] = useState(false);

  const { isAllowedAdjustment } = useUser();

  const fileInutRef = useRef<HTMLInputElement | null>(null);
  const [schema, setSchema] = useState(teacherSchemaCollapse);
  const {
    register,
    handleSubmit,
    control,
    setValue,
    setError,
    formState: { errors }
  } = useForm<TeacherForm>({
    defaultValues: initialTeacherValue,
    resolver: yupResolver(schema)
  });

  const navigate = useNavigate();

  const previewImage = useMemo(() => {
    if (file) return URL.createObjectURL(file);
    if (currentImage) return currentImage;
    return '';
  }, [currentImage, file]);

  const resultPreview = useMemo(() => {
    // TODO: integrate api user
    return previewImage ? previewImage : getAvatarUrl('');
  }, [previewImage]);

  const { isFetching, refetch } = useQuery({
    queryKey: ['EditByID', teacherId],
    queryFn: () => teacherApis.EditByID(teacherId as string),
    onSuccess: (res) => {
      let Item = res.data.Item;
      if (Item.LinkAvatar !== null) setCurrentImage(Item.LinkAvatar);
      setValue('name', Item.Ten);
      setValue('id', Item.MaSoThanhVien);
      if (Item.DiaChi) setValue('address', Item.DiaChi);
      // @ts-ignore
      setValue('dateOfBirth', dayjs(Item.NgaySinh, 'DD/MM/YYYY'));
      setValue('gender', Item.GioiTinh);
      if (Item.SDT) setValue('phone', Item.SDT);
      setValue('status', Item.TrangThai);
      setValue('organization', Item.ChucVu);
      if (Item.IsCanBoThuVien) {
        setSchema(teacherSchema);
        if (Item.Mail) setValue('email', Item.Mail);
        setValue('classify', Item.PhanLoai);
        setValue('qualification', Item.TrinhDoChuyenMon);
        setValue('workYear', Item.NamCongTac);
      } else {
        setSchema(teacherSchemaCollapse);
      }
      setStateAdvance(Item.IsCanBoThuVien);
      setTeacher(Item);
    },
    onError: () => {
      navigate(path.hocsinh);
    },
    // teacherId === true => excute query function
    enabled: !!teacherId
  });

  const { mutate: handleCreate, isLoading: isLoadingCreate } = useMutation({
    mutationFn: (bodyFormData: FormData) => teacherApis.CreateUserByModel(bodyFormData),
    onSuccess: () => {
      toast.success('Thêm giáo viên thành công');
      if (isLibrary) navigate(path.canbothuvien);
      else navigate(path.giaovien);
    },
    onError: (
      ex: AxiosError<
        ResponseApi<{
          Mail: string;
          MSTV: string;
        }>
      >
    ) => {
      if (ex?.response?.data?.ExMessage === 'Mail') {
        setError('email', { type: 'duplicate', message: ex.response?.data.Message });
      }
      if (ex?.response?.data?.ExMessage === 'MSTV') {
        setError('id', { type: 'duplicate', message: ex.response?.data.Message });
      }
    }
  });

  const { mutate: handleEdit, isLoading: isLoadingEdit } = useMutation({
    mutationFn: (bodyFormData: FormData) => teacherApis.Edit(bodyFormData),
    onSuccess: () => {
      toast.success('Cập nhật giáo viên thành công');
      refetch();
    },
    onError: (
      ex: AxiosError<
        ResponseApi<{
          Mail: string;
          MSTV: string;
        }>
      >
    ) => {
      if (ex?.response?.data?.ExMessage === 'Mail') {
        setError('email', { type: 'duplicate', message: ex.response?.data.Message });
      }
      if (ex?.response?.data?.ExMessage === 'MSTV') {
        setError('id', { type: 'duplicate', message: ex.response?.data.Message });
      }
    }
  });

  const { mutate: recoveryPassword, isLoading: isLoadingRecoveryPassword } = useMutation({
    mutationFn: () => teacherApis.ResetPassword(teacherId as string),
    onSuccess: () => {
      toast.success('Phục hồi mật khẩu thành công');
    }
  });

  const { mutate: deleteSingle, isLoading: isLoadingDeleteSingle } = useMutation({
    mutationFn: () => teacherApis.DeleteSingle(teacherId as string),
    onSuccess: () => {
      toast.success('Xóa giáo viên thành công');
      handleBack();
    }
  });

  const handleDelete = () => {
    setIsDelete(false);
    deleteSingle();
  };

  const onSubmit = handleSubmit((data, event) => {
    if (event === undefined) return;
    let bodyFormData = new FormData();
    bodyFormData.append('Ten', data.name.replace(/\s+/g, ' '));
    bodyFormData.append('GioiTinh', data.gender);
    bodyFormData.append('NgaySinh', convertDate(data.dateOfBirth));
    bodyFormData.append('TemptNgaySinh', convertDate(data.dateOfBirth));
    data.id = data.id?.toLocaleUpperCase();
    bodyFormData.append('MaSoThanhVien', data.id as string);
    bodyFormData.append('DiaChi', data.address as string);
    bodyFormData.append('SDT', data.phone as string);
    bodyFormData.append('HinhChanDung', file as Blob);
    bodyFormData.append('ChucVu', data.organization);
    bodyFormData.append('IsCanBoThuVien', stateAdvance.toString());
    if (stateAdvance) {
      bodyFormData.append('PhanLoai', data.classify);
      bodyFormData.append('TrinhDoChuyenMon', data.qualification);
      bodyFormData.append('Mail', data.email as string);
      bodyFormData.append('NamCongTac', data.workYear);
    }
    if (teacherId) {
      bodyFormData.append('Id', teacherId);
      bodyFormData.append('TrangThai', data.status as string);
      handleEdit(bodyFormData);
    } else {
      handleCreate(bodyFormData);
    }
  });

  const isDisabled = useMemo(() => (!!teacherId && isFetching) || !isAllowedAdjustment, [teacherId, isFetching]);

  const handleAccessFileInputRef = () => {
    fileInutRef.current?.click();
  };

  const handleOnFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files?.[0];

    if (!fileFromLocal) {
      return;
    }

    if (fileFromLocal && !fileFromLocal.type.startsWith('image')) {
      return;
    }
    setFile(fileFromLocal);
  };

  const handleBack = () => navigate(-1);

  const onCheckLibraryMonitor = (e: any) => {
    if (e.target.checked) setSchema(teacherSchema);
    else setSchema(teacherSchemaCollapse);
    setStateAdvance(e.target.checked);
  };

  return (
    <div className='p-5'>
      <Title title={!!teacherId ? 'Cập nhật giáo viên' : 'Thêm mới giáo viên'} />

      <form className='container' onSubmit={onSubmit}>
        <div className='mt-10 grid grid-cols-1 md:grid-cols-12'>
          <div className='relative col-span-3 mb-10 flex h-full w-full justify-center overflow-hidden'>
            <input
              type='file'
              accept='image/jpeg,image/gif,image/jpg,image/png,image/bmp'
              className='hidden'
              ref={fileInutRef}
              onChange={handleOnFileChange}
              disabled={isDisabled}
            />

            <div className='group relative h-44 w-44'>
              <div className='my-5 h-44 w-44'>
                <img
                  src={resultPreview ? resultPreview : '/content/default-avatar.jpeg'}
                  alt='Hình đại diện'
                  className='h-full w-full rounded-full object-cover shadow-md transition-all duration-200 ease-in-out group-hover:blur-[2px]'
                />
              </div>

              <button
                type='button'
                onClick={handleAccessFileInputRef}
                className='absolute inset-0 my-5 flex  h-44 w-44 scale-110 items-center justify-center rounded-full p-5 opacity-0 transition-all duration-200 ease-out group-hover:scale-100 group-hover:bg-black/40 group-hover:opacity-100'
              >
                <span className='mr-2 text-white'>Chọn hình ảnh</span> <CameraFilled style={{ color: 'white' }} />
              </button>
            </div>
          </div>
          <div className='custom-row col-span-4 px-2'>
            <div className='my-2'>
              <label className='mb-3 font-bold'>
                Tên giáo viên <span className='text-danger-10'>*</span>
              </label>

              <Input
                placeholder='Nhập tên giáo viên'
                name='name'
                register={register}
                errorMessage={errors?.name?.message}
                disabled={isDisabled}
                maxLength={256}
              />
            </div>

            <div className='my-2'>
              <label className='mb-3 font-bold'>Mã giáo viên</label>

              <Input
                placeholder='Nhập mã giáo viên'
                name='id'
                register={register}
                errorMessage={errors?.id?.message}
                disabled={isDisabled}
                maxLength={51}
              />
              <p className='mt-1 italic text-primary-10'>(Nếu không nhập, mã giáo viên sẽ được tạo tự động)</p>
            </div>

            <div className='my-2'>
              <label className='mb-3 font-bold'>
                Tổ <span className='text-danger-10'>*</span>
              </label>

              <Input
                placeholder='Nhập tổ'
                name='organization'
                register={register}
                errorMessage={errors?.organization?.message}
                disabled={isDisabled}
                maxLength={31}
              />
            </div>

            <div className='my-2'>
              <label className='mb-3 font-bold'>Địa chỉ</label>

              <Input
                placeholder='Nhập địa chỉ'
                name='address'
                register={register}
                errorMessage={errors?.address?.message}
                disabled={isDisabled}
                autoFocus={true}
                maxLength={2001}
              />
            </div>
            <div className='my-2'>
              <Checkbox onChange={onCheckLibraryMonitor} checked={stateAdvance}>
                Là cán bộ thư viện
              </Checkbox>
            </div>
            {!!stateAdvance && (
              <div className='my-2'>
                <label className='mb-2 font-bold'>
                  Trình độ chuyên môn <span className='text-danger-10'>*</span>
                </label>

                <Input
                  name='qualification'
                  register={register}
                  className='focus:shadow-smundefined w-full rounded-md border border-gray-300 py-3.5 px-4 outline-none focus:border-primary-30'
                  errorMessage={errors?.qualification?.message}
                  disabled={isDisabled}
                  placeholder='Nhập trình độ chuyên môn'
                  maxLength={21}
                />
              </div>
            )}
            {!!stateAdvance && (
              <div className='my-2'>
                <label className='mb-2 font-bold'>
                  Năm công tác <span className='text-danger-10'>*</span>
                </label>

                <Select
                  items={WORK_YEAR_OPTIONS}
                  className='w-full'
                  name='workYear'
                  register={register}
                  errorMessage={errors?.workYear?.message}
                  disabled={isDisabled}
                />
              </div>
            )}
          </div>
          <div className='custom-row col-span-5 px-2'>
            <div className='my-2'>
              <label className='mb-3 font-bold'>
                Ngày sinh <span className='text-danger-10'>*</span>
              </label>
              <DatePicker
                control={control}
                name='dateOfBirth'
                disabled={isDisabled}
                disabledDate={(current) => {
                  return current && current > dayjs().endOf('day');
                }}
                placeholder='Chọn ngày sinh'
                errorMessage={errors?.dateOfBirth?.message}
                locale={locale}
              />
            </div>

            <div className='my-3' style={{ height: '100px' }}>
              <label className='mb-2 font-bold'>
                Giới tính <span className='text-danger-10'>*</span>
              </label>

              <Select
                items={GENDER_OPTIONS}
                className='w-full'
                name='gender'
                register={register}
                errorMessage={errors?.gender?.message}
                disabled={isDisabled}
              />
            </div>

            <div className='my-3.5'>
              <label className='mb-2 font-bold'>Số điện thoại</label>

              <Input
                placeholder='Nhập số điện thoại'
                name='phone'
                register={register}
                errorMessage={errors?.phone?.message}
                onKeyPress={(event) => {
                  if (!/[0-9]/.test(event.key)) {
                    event.preventDefault();
                  }
                }}
                disabled={isDisabled}
                maxLength={10}
              />
            </div>

            {!!teacherId && (
              <div className='my-3'>
                <div className='my-2'>
                  <label className='mb-2 font-bold'>
                    Tình trạng <span className='text-danger-10'>*</span>
                  </label>

                  <Select
                    items={STATUS_OPTIONS}
                    name='status'
                    register={register}
                    className='w-full'
                    disabled={isDisabled}
                    errorMessage={errors?.status?.message}
                  />
                </div>
              </div>
            )}
            {!teacherId && (
              <div className='my-3'>
                <div className='my-2'>
                  <div style={{ height: '79px' }}></div>
                </div>
              </div>
            )}
            <div className='my-2'>
              <div style={{ height: '18px' }}></div>
            </div>
            {!!stateAdvance && (
              <div className='my-2'>
                <label className='mb-2 font-bold'>
                  Phân loại <span className='text-danger-10'>*</span>
                </label>

                <Select
                  items={CLASSIFY_OPTIONS}
                  name='classify'
                  register={register}
                  className='w-full'
                  errorMessage={errors?.classify?.message}
                  disabled={isDisabled}
                />
              </div>
            )}

            {!!stateAdvance && (
              <div className='my-2'>
                <label className='mb-2 font-bold'>Email</label>

                <Input
                  name='email'
                  register={register}
                  className='focus:shadow-smundefined w-full rounded-md border border-gray-300 py-3.5 px-4 outline-none focus:border-primary-30'
                  errorMessage={errors?.email?.message}
                  disabled={isDisabled}
                  placeholder='Nhập Email'
                  maxLength={256}
                />
              </div>
            )}
          </div>
        </div>
        <div className='mt-5 flex justify-end'>
          <Button type='button' variant='secondary' className='font-semibold' onClick={handleBack}>
            Quay về
          </Button>

          {!!teacherId ? (
            <>
              {!isLibrary ? (
                <>
                  <Button
                    variant='danger'
                    type='button'
                    className={classNames('', {
                      hidden: !isAllowedAdjustment,
                      'ml-2 font-semibold': isAllowedAdjustment
                    })}
                    onClick={() => {
                      if (teacher && (!teacher.DaTra || teacher.TrangThai === EUser.DeActive)) {
                        !teacher.DaTra && toast.error(`Giáo viên ${teacher.Ten} chưa trả sách`);
                        teacher.TrangThai === EUser.DeActive && toast.error(`Giáo viên ${teacher.Ten} đang bị khóa`);
                        return;
                      }
                      setIsDelete(true);
                    }}
                  >
                    Xóa giáo viên
                  </Button>

                  <Button
                    variant='default'
                    type='button'
                    className={classNames('', {
                      hidden: !isAllowedAdjustment,
                      'ml-2 font-semibold': isAllowedAdjustment
                    })}
                    onClick={() => {
                      recoveryPassword();
                    }}
                  >
                    Phục hồi mật khẩu
                  </Button>

                  <Button
                    variant='default'
                    type='button'
                    className={classNames('', {
                      hidden: !isAllowedAdjustment,
                      'ml-2 font-semibold': isAllowedAdjustment
                    })}
                    onClick={(e) => {
                      e.stopPropagation();
                      navigate(path.xuatthethuviengiaovien, { state: { selectedTeachers: [teacherId + ''] } });
                    }}
                  >
                    In thẻ thư viện
                  </Button>
                </>
              ) : (
                <></>
              )}
              <Button
                type='button'
                onClick={onSubmit}
                variant={isDisabled ? 'disabled' : 'default'}
                className={classNames('', {
                  hidden: !isAllowedAdjustment,
                  'ml-2 font-semibold': isAllowedAdjustment
                })}
                disabled={isDisabled}
              >
                Cập nhật
              </Button>
            </>
          ) : (
            <Button
              variant='default'
              type='submit'
              className={classNames('', {
                hidden: !isAllowedAdjustment,
                'ml-2 font-semibold': isAllowedAdjustment
              })}
            >
              Thêm mới
            </Button>
          )}
        </div>
      </form>
      <ModalDelete
        open={isDelete}
        closable={false}
        title={<TitleDelete firstText='Bạn có chắn chắn muốn xóa giáo viên' secondText={teacher?.Ten}></TitleDelete>}
        // TODO: Remove inline funtion, integrate api
        handleCancel={() => {
          setIsDelete(false);
        }}
        handleOk={() => {
          handleDelete();
        }}
      />
      <Loading open={isLoadingCreate || isLoadingEdit || isLoadingRecoveryPassword || isLoadingDeleteSingle}></Loading>
    </div>
  );
};

export default AddTeacherPage;
