import { Outlet } from 'react-router-dom';

const Media = () => <Outlet />;

export default Media;
