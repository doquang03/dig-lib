export { default as StudentManagementPage } from './StudentManagementPage';
export * from './AddStudent';
export * from './StudentManagementContent';
export * from './AddStudentByExcelFile';
export * from './AddStudentAvatar';
