import { Checkbox, Modal, ModalProps, Radio } from 'antd';
import { Button, Title } from 'components';
import { useState } from 'react';

type Props = {
  onBack: VoidFunction;
  onSubmit: VoidFunction;
  setIsTaiCho: React.Dispatch<React.SetStateAction<boolean>>;
  setOptionSplit: React.Dispatch<React.SetStateAction<boolean>>;
  typeAccount: string;
  isLoadingDownLoad: boolean;
} & ModalProps;

const NoteRentingBookModal = (props: Props) => {
  const { open, onSubmit, onBack, isLoadingDownLoad, typeAccount, setIsTaiCho, setOptionSplit } = props;
  const [value, setValue] = useState(false);
  return (
    <Modal open={open} footer={null} closable={false} width={610}>
      <Title title={'Tải sổ mượn sách của ' + (typeAccount === 'gv' ? 'giáo viên' : 'học sinh')} />
      <div className='mt-3 flex flex-col gap-3'>
        <div>
          <label className='font-semibold'>Chọn hình thức mượn sách:</label>
          <div className='flex items-center'>
            <Radio.Group
              onChange={(e) => {
                setValue(e.target.value);
                setIsTaiCho(e.target.value);
              }}
              value={value}
            >
              <Radio value={false}>Mượn về nhà</Radio>
              <Radio value={true}>Đọc tại chỗ</Radio>
            </Radio.Group>
          </div>
        </div>

        <div className='flex flex-col'>
          <label className='font-semibold'>Tùy chọn:</label>
          <Checkbox
            onChange={(e) => {
              setOptionSplit(e.target.checked);
            }}
            defaultChecked={true}
          >
            {`In mỗi ${typeAccount === 'gv' ? 'tổ' : 'lớp'} một danh sách`}
          </Checkbox>
        </div>
      </div>

      <div className='flex justify-end gap-2'>
        <Button variant='secondary' onClick={onBack} loading={isLoadingDownLoad}>
          Quay về
        </Button>
        <Button variant='default' onClick={onSubmit} loading={isLoadingDownLoad}>
          In danh sách
        </Button>
      </div>
    </Modal>
  );
};

export default NoteRentingBookModal;
