import { BookFilled, CarryOutOutlined, EditOutlined, QrcodeOutlined } from '@ant-design/icons';
import { useQuery } from '@tanstack/react-query';
import { Table, Tooltip } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { bookApis } from 'apis';
import type { GetBookByIdData } from 'apis/book.apis';
import { Button, Empty, Loading } from 'components';
import { useUser } from 'contexts/user.context';
import { Key, useMemo, useState } from 'react';
import CreateImport from './CreateImport';
import CreateReceipt from './CreateReceipt';
import UpdateLiquiBook from './UpdateLiquiBook';

const WarehouseInfor = (props: GetBookByIdData & { onSuccess: VoidFunction }) => {
  const {
    SachDTO: { SoLuongTong = 0, SoLuongConLai = 0, Id },
    SLDangDuocDat = 0,
    SLDangDuocMuon = 0,
    TongSoLanDuocMuon = 0,
    onSuccess
  } = props;

  const [selectedSachCaBiets, setSelectedSachCaBiets] = useState<GetWareHouseData['List_SoLuongTrangThaiSachVM']>([]);
  const [selectedSachCaBiet, setSelectedSachCaBiet] = useState<TrangThaiSachVM | undefined>();
  const [visibleLiqidationModal, setVisibleLiquidationModal] = useState<boolean>(false);
  const [visibleReceiptModal, setVisibleReceiptModal] = useState<boolean>(false);
  const [visibleImportModal, setVisibleImportModal] = useState<boolean>(false);

  const { isAllowedAdjustment } = useUser();

  const { data, isLoading, refetch, isFetching } = useQuery({
    queryKey: ['wareHouseInfo', Id],
    queryFn: () => bookApis.getWareHouseInfoById(Id + '')
  });

  const {
    List_NguonCungCap = [],
    List_SoLuongTrangThaiSachVM = [],
    List_TrangThaiSach = []
  } = useMemo(() => {
    if (!data?.data.Item) {
      return {} as GetWareHouseData;
    }
    return data?.data.Item;
  }, [data?.data.Item]);

  const handleEditSachCaBiet = (book: TrangThaiSachVM) => () => {
    setSelectedSachCaBiet(book);

    setVisibleLiquidationModal(true);
  };

  const colums: ColumnsType<TrangThaiSachVM> = [
    {
      title: 'Mã cá biệt',
      dataIndex: 'MaCaBiet_That',
      key: 'MaCaBiet_That'
    },
    {
      title: 'Tình trạng sách',
      dataIndex: 'TrangThai',
      key: 'TrangThai'
    },
    {
      title: 'Số chứng từ',
      dataIndex: 'SoChungTu',
      key: 'SoChungTu'
    },
    {
      title: 'Số vào sổ tổng quát',
      dataIndex: 'SoVaoSoTongQuat',
      key: 'SoVaoSoTongQuat'
    },
    {
      title: 'Ngày vào sổ',
      dataIndex: 'NgayVaoSo_String',
      key: 'NgayVaoSo_String'
    },
    {
      title: 'Nguồn cung cấp',
      dataIndex: 'NguonCungCap_String',
      key: 'NguonCungCap_String'
    },
    {
      title: 'Trạng thái sách',
      dataIndex: 'TrangThai',
      key: 'TrangThai',
      render: (value, record) => {
        return (
          <div className='flex items-center justify-center gap-2 '>
            <Tooltip title={record.IsMuon ? 'Đang mượn' : 'Có sẵn'} arrow={true}>
              <CarryOutOutlined style={{ color: record.IsMuon ? '#08c' : 'gray' }} />
            </Tooltip>

            <Tooltip title={'Có sẵn'} arrow={true}>
              <BookFilled style={{ color: 'gray' }} />
            </Tooltip>

            <Tooltip title={record.IsXuatQR ? 'Đã xuất QR' : 'Chưa xuất QR'} arrow={true}>
              <QrcodeOutlined style={{ color: record.IsXuatQR ? '#08c' : 'gray' }} />
            </Tooltip>
          </div>
        );
      }
    },
    {
      title: '',
      dataIndex: 'action',
      key: 'action',
      render: (value: any, record: TrangThaiSachVM, j: number) => {
        return (
          <button onClick={handleEditSachCaBiet(record)} type='button'>
            <EditOutlined style={{ fontSize: '20px', color: '#08c' }} />
          </button>
        );
      }
    }
  ];

  const onSelectChange = (_: Key[], selectedRowValue: GetWareHouseData['List_SoLuongTrangThaiSachVM']) => {
    setSelectedSachCaBiets(selectedRowValue);
  };

  const handleEditLiqidationBook = () => {
    setVisibleLiquidationModal(true);
  };

  const handleCreateReceipt = () => setVisibleReceiptModal(true);

  const handleCreateImport = () => setVisibleImportModal(true);

  const handleUpdateSuccess = () => {
    refetch();
    setSelectedSachCaBiets([]);
    setVisibleLiquidationModal(false);
    setSelectedSachCaBiet(undefined);
  };

  const handleExportSuccess = () => {
    setSelectedSachCaBiets([]);
    setVisibleReceiptModal(false);
    setSelectedSachCaBiet(undefined);
    refetch();
    onSuccess();
  };

  const isDisabled = !selectedSachCaBiets.length;

  return (
    <div className='mx-5'>
      <p className='font-bold'>
        Tổng số lượng sách: <span> {SoLuongTong}</span>{' '}
      </p>
      <p className='my-2 font-bold'>
        Số lượng sách còn lại: <span> {SoLuongConLai}</span>{' '}
      </p>
      <p className='my-2 font-bold'>
        Số sách đang được mượn: <span> {SLDangDuocMuon}</span>{' '}
      </p>
      <p className='my-2 font-bold'>
        Số sách đang được đặt mượn: <span> {SLDangDuocDat}</span>{' '}
      </p>
      <p className='font-bold'>
        Tổng số lần được mượn: <span> {TongSoLanDuocMuon}</span>{' '}
      </p>

      <div className='mb-4 flex items-center justify-between'>
        <h3 className='font-bold uppercase text-primary-10'>Tình trạng sách</h3>

        <div className={isAllowedAdjustment ? 'flex gap-3' : 'hidden'}>
          <Button type='button' onClick={handleCreateImport}>
            Tạo phiếu nhập kho
          </Button>

          <Button
            onClick={handleEditLiqidationBook}
            type='button'
            variant={isDisabled ? 'secondary' : 'default'}
            disabled={isDisabled}
          >
            Chỉnh sửa sách
          </Button>

          <Button
            onClick={handleCreateReceipt}
            type='button'
            variant={isDisabled ? 'secondary' : 'default'}
            disabled={isDisabled}
          >
            Xuất mã QR
          </Button>
        </div>
      </div>

      <Table
        columns={colums}
        rowSelection={{
          selectedRowKeys: selectedSachCaBiets.map((item) => item.Id),
          onChange: onSelectChange
        }}
        className='custom-table'
        rowKey={(record) => record.Id}
        loading={isLoading}
        locale={{
          emptyText: () => <Empty />
        }}
        dataSource={List_SoLuongTrangThaiSachVM || []}
        pagination={{
          showSizeChanger: false
        }}
      />

      <UpdateLiquiBook
        open={visibleLiqidationModal}
        onHide={() => setVisibleLiquidationModal(false)}
        suppliers={List_NguonCungCap}
        bookStatus={List_TrangThaiSach}
        selectedSachCaBiets={selectedSachCaBiets}
        selectedSachCaBiet={selectedSachCaBiet}
        onSuccess={handleUpdateSuccess}
      />

      <CreateReceipt
        ids={selectedSachCaBiets.map((item) => item.Id)}
        open={visibleReceiptModal}
        onHide={() => setVisibleReceiptModal(false)}
        onSuccess={handleExportSuccess}
      />

      <CreateImport
        open={visibleImportModal}
        onHide={() => setVisibleImportModal(false)}
        onSuccess={handleExportSuccess}
      />

      <Loading open={isLoading || isFetching} />
    </div>
  );
};

export default WarehouseInfor;
