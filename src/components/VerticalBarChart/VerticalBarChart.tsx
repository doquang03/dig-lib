import { Tooltip } from 'antd';
import { useEffect, useMemo, useRef, useState } from 'react';

type DataSet = { value: number; label: string };

type Option = {
  labels: {
    xAxisLabel?: string;
    yAxisLabel?: string;
  };
};

type Props = {
  labels: string[];
  datasets: DataSet[];
  options: Partial<Option>;
};

const HEIGHT_CHART = 160;

function VerticalBarChart(props: Props) {
  const { labels, datasets } = props;

  const [parentWidth, setParentWidth] = useState<number>(0);

  const ref = useRef<HTMLDivElement>(null);

  const { yAxisValues, temp } = useMemo(() => {
    const maxValue = datasets.length ? Math.max(...datasets.map(({ value }) => value)) : 0;

    let temp = '1';

    [...Array(maxValue.toString().length - 1)].forEach((_) => (temp += '0'));

    const yAxisValues = Math.ceil(maxValue / Number(temp)) + 1 || 0;

    return { yAxisValues, temp, maxValue };
  }, [datasets]);

  useEffect(() => {
    if (!ref?.current?.offsetWidth) return;

    setParentWidth(ref?.current?.offsetWidth - (24 * 2 + 45));
  }, [ref?.current?.offsetWidth]);

  return (
    <div className={`mt-3 flex items-center justify-center`} ref={ref}>
      <div className='pl-10'>
        <div className='relative'>
          {/* y axis label */}
          {props.options.labels?.yAxisLabel ? (
            <span
              className='absolute left-0 bottom-1/2 -rotate-90 text-xs font-bold'
              style={{
                left: -125
              }}
            >
              {props.options.labels?.yAxisLabel}
            </span>
          ) : null}

          <div
            className='w-[1px] bg-black'
            style={{
              height: HEIGHT_CHART
            }}
          />

          {/* y axis ranges */}
          {[...Array(yAxisValues)].map((_, index) => (
            <div
              key={index}
              className='absolute -left-1 z-10 h-[1px] bg-black/40'
              style={{
                // -1 cause of height
                bottom: index * (HEIGHT_CHART / yAxisValues) - 1,
                zIndex: 10,
                width: parentWidth
              }}
            >
              <span
                className='absolute -top-3'
                style={{
                  left: -temp.length * 8
                }}
              >
                {index * Number(temp)}
              </span>
            </div>
          ))}

          {/* y axis value */}
          {datasets.map(({ label, value }, index) => {
            return (
              <Tooltip key={label} title={`${label} - ${value}`} className='hover:cursor-pointer'>
                <div
                  className='absolute bottom-0 z-10 rounded bg-gradient-to-t from-[#005FA8] to-blue-500/80 shadow-md'
                  style={{
                    height: (value * (HEIGHT_CHART / yAxisValues)) / Number(temp),
                    width: 30,
                    left: index * (parentWidth / labels.length) + 20
                  }}
                />
              </Tooltip>
            );
          })}
        </div>

        <div
          className='h-[0.5px] bg-black'
          style={{
            width: parentWidth
          }}
        />

        <div className='relative mt-7'>
          {/* x axis label */}
          {labels.map((label, index) => (
            <div key={label} className='relative'>
              <label
                className='absolute whitespace-nowrap text-xs'
                style={{
                  rotate: '-45deg',
                  left: index * (parentWidth / labels.length)
                }}
              >
                {label}
              </label>

              <div
                className='absolute -top-7 h-[5px] w-[1px] bg-black'
                style={{
                  left: index * (parentWidth / labels.length) + 34
                }}
              />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default VerticalBarChart;
