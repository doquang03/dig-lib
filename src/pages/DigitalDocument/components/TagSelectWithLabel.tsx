import { Select, type SelectProps } from 'antd';
import classNames from 'classnames';
import { Button } from 'components';
import 'css/AddPublicationPage.css';
import 'css/AutoComplete.css';
import { Revert } from 'pages/TitleApproval/icons';
import { memo } from 'react';

type Props = {
  label: string;
  required?: boolean;
  error?: string;
  onClickAddableButton?: VoidFunction;
  hasAddableButton?: boolean;
  items: Option[];
  hasMerge?: boolean;
  onMerge?: VoidFunction;
} & SelectProps;

const Option = Select.Option;

const TagSelectWithLabel = (props: Props) => {
  const {
    label,
    required,
    error,
    className,
    onClickAddableButton,
    hasAddableButton = false,
    items,
    hasMerge = false,
    onMerge,
    ...rest
  } = props;

  return (
    <div className='grid grid-cols-12'>
      <label className='col-span-3 flex items-center gap-1 font-bold'>
        {label}

        {required && <span className='text-danger-10'>*</span>}
      </label>

      <div className='col-span-9 flex items-center gap-1'>
        <Select
          mode={'tags'}
          {...rest}
          className={classNames(`select-antd-tags h-full w-full grow`, {
            'rounded-md border border-red-500': !!error
          })}
        >
          {items.map((element) => {
            return (
              <Option value={element.value} key={element.value}>
                {element.label}
              </Option>
            );
          })}
        </Select>

        {hasAddableButton && (
          <Button onClick={onClickAddableButton}>
            <svg width='20' height='21' viewBox='0 0 20 21' fill='none' xmlns='http://www.w3.org/2000/svg'>
              <path
                d='M11.957 13.0723V20.7539H7.35156V13.0723H0.179688V8.58984H7.35156V0.996094H11.957V8.58984H19.1289V13.0723H11.957Z'
                fill='white'
              />
            </svg>
          </Button>
        )}

        {hasMerge ? (
          <button onClick={onMerge}>
            <Revert />
          </button>
        ) : null}
      </div>
    </div>
  );
};

export default memo(TagSelectWithLabel);
