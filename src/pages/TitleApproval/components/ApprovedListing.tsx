import { useQuery } from '@tanstack/react-query';
import { Table, Tooltip } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { approvalApis } from 'apis';
import classNames from 'classnames';
import { SizeChanger } from 'components';
import { path } from 'constants/path';
import dayjs, { Dayjs } from 'dayjs';
import { useStateAsync } from 'hooks';
import { ChangeEvent } from 'react';
import { Link } from 'react-router-dom';
import { getSerialNumber } from 'utils/utils';
import SearchForm from './SearchForm';

const ApprovedListing = () => {
  const [params, setParams] = useStateAsync<{
    page: number;
    pageSize: number;
    nameCode: string;
    publishedDate: string;
  }>({ page: 1, pageSize: 30, nameCode: '', publishedDate: '' });

  const {
    data: listOFApprovalsData,
    isLoading: loadingListOfApprovals,
    refetch,
    isRefetching
  } = useQuery({
    queryKey: ['getListOfApprovedUnits', params.page, params.pageSize],
    queryFn: () => {
      const _params: { page: number; pageSize: number } & Partial<{
        TextForSearch: string;
        TimeSubmit: string;
      }> = {
        page: params.page,
        pageSize: params.pageSize
      };

      if (params.nameCode) {
        _params.TextForSearch = params.nameCode;
      }

      if (params.publishedDate) {
        _params.TimeSubmit = params.publishedDate;
      }

      return approvalApis.getListOfApprovals({ ..._params, IsComplete: true });
    }
  });

  const listOfListOFApprovals = listOFApprovalsData?.data.Item || { Count: 0, Key: [] };

  const colums: ColumnsType<BookApproval> = [
    {
      key: 'stt',
      dataIndex: 'stt',
      title: 'STT',
      render: (value, record, index) => getSerialNumber(params.page, params.pageSize, index)
    },
    {
      key: 'Tên đơn vị',
      dataIndex: 'Tên đơn vị',
      title: 'Tên đơn vị',
      render: (value, { NameMember = '' }) =>
        NameMember.length > 40 ? (
          <Tooltip title={NameMember} arrow={true}>
            <p>{NameMember?.substring(0, 40).concat('...')}</p>
          </Tooltip>
        ) : (
          <p>{NameMember}</p>
        ),
      onCell: (record) => ({
        className: 'text-left w-[230px]'
      })
    },
    {
      key: 'Tên tài liệu',
      dataIndex: 'Tên tài liệu',
      title: 'Tên tài liệu',
      render: (value, { Title = '', Id, NameMember }) =>
        Title.length > 60 ? (
          <Tooltip title={Title} arrow={true}>
            <Link to={`${path.titleApproval}/${Id}`}>
              <p className='text-left'>{Title?.substring(0, 60).concat('...')}</p>
            </Link>
          </Tooltip>
        ) : (
          <Link to={`${path.titleApproval}/${Id}`} state={{ schoolName: NameMember }}>
            <p className='text-left'>{Title}</p>
          </Link>
        ),
      onCell: (record) => ({
        className: 'text-left hover:underline text-primary-30'
      })
    },
    {
      key: 'Loại tài liệu',
      dataIndex: 'BookType',
      title: 'Loại tài liệu',
      render: (value, { BookType }) => (
        <p>
          {BookType === 0 && 'Tài liệu giấy'}
          {BookType === 1 && 'Tài liệu số'}
          {BookType === 2 && 'Tài liệu giấy và số'}
        </p>
      )
    },
    {
      key: 'Author',
      dataIndex: 'Author',
      title: 'Tác giả',
      width: 200
    },
    {
      key: 'UserName',
      dataIndex: 'UserName',
      title: 'Người gửi'
    },
    {
      key: 'Thời gian gửi bản ghi',
      dataIndex: 'Thời gian gửi bản ghi',
      title: 'Thời gian gửi bản ghi',
      render: (value, record) =>
        record?.CreateDateTime === '0001-01-01T00:00:00Z'
          ? '--'
          : dayjs(record.CreateDateTime).format('HH:mm DD/MM/YYYY')
    },
    {
      key: 'Thời gian duyệt bản ghi',
      dataIndex: 'Thời gian duyệt bản ghi',
      title: 'Thời gian duyệt bản ghi',
      render: (value, record) => (!record?.CompleteTime ? '--' : dayjs(record.CompleteTime).format('HH:mm DD/MM/YYYY'))
    },
    {
      key: 'Trạng thái',
      dataIndex: 'Trạng thái',
      title: 'Trạng thái',
      render: (value, { IsApprove }) => (
        <p
          className={classNames('whitespace-nowrap rounded-full border-[1px] p-2', {
            'border-[#119757] bg-[#119757]/10 text-[#119757] shadow-[#119757]/80': IsApprove,
            'border-danger-10 bg-[#FF16161A]/10 text-danger-10': !IsApprove
          })}
        >
          {IsApprove ? 'Đã duyệt' : 'Từ chối'}
        </p>
      )
    }
  ];

  const handleChangeName = (e: ChangeEvent<HTMLInputElement>) => {
    if (!e) return;

    setParams((prevState) => {
      return { ...prevState, nameCode: e.target.value };
    });
  };

  const handleChangeDate = (value: Dayjs | null) => {
    setParams((prevState) => {
      return { ...prevState, publishedDate: value ? value?.toISOString() : '' };
    });
  };

  const handleRefresh = async () => {
    await setParams({ page: 1, pageSize: 30, nameCode: '', publishedDate: '' });
    await refetch();
  };

  const handleSearch = async () => {
    if (isRefetching || loadingListOfApprovals) {
      return;
    }

    await setParams((prevState) => {
      return { ...prevState, page: 1, pageSize: 30 };
    });

    await refetch();
  };

  return (
    <div className='mt-3'>
      <SearchForm
        searchParams={{ name: params.nameCode, date: params.publishedDate }}
        onChangeName={handleChangeName}
        onChangeDate={handleChangeDate}
        onRefresh={handleRefresh}
        onSearch={handleSearch}
      />

      <Table
        columns={colums}
        className='custom-table my-3'
        bordered
        pagination={{
          current: params.page,
          pageSize: params.pageSize,
          total: listOfListOFApprovals.Count,
          onChange: (page, pageSize) => {
            setParams((prevState) => {
              return { ...prevState, page, pageSize };
            });
          },
          hideOnSinglePage: true,
          showSizeChanger: false,
          showQuickJumper: true,
          locale: { jump_to: '', page: '' }
        }}
        loading={false}
        rowKey={(row) => row.Id}
        dataSource={listOfListOFApprovals.Key}
      />

      <div
        className={classNames('relative', {
          'mt-[64px]': params.pageSize >= 0
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger
            visible={!!listOfListOFApprovals.Count}
            value={listOfListOFApprovals.Key.length + ''}
            total={listOfListOFApprovals.Count + ''}
            onChange={(pageSize) =>
              setParams((prevState) => {
                return { ...prevState, pageSize: +pageSize };
              })
            }
          />
        </div>
      </div>
    </div>
  );
};

export default ApprovedListing;
