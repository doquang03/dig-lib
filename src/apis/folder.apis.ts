import httpThuVien from 'utils/httpThuVien';
import { serialize } from 'utils/utils';

const CONTROLLER = `${process.env.REACT_APP_SERVICE_MLLH}/api/Document`;
const BOOK_CONTROLLDER = `${process.env.REACT_APP_SERVICE_MLLH}/api/Books`;

type AddNewFolderParams = Pick<Folder, 'Id' | 'IdParent' | 'Type' | 'TenThuMuc' | 'IsActive'>;

export type BookInFolder = Pick<Book, 'Id' | 'TenSach' | 'TacGia' | 'NamXuatBan' | 'GiaBia'> & PullRequestBook;

const folderApis = {
  // FOLDER_CONTROLLER
  getFolders: () =>
    httpThuVien.post<ResponseApi<{ ListThuMucSach: Folder[]; Count: number }>>(`/${CONTROLLER}/GetThuMucSach`, {}),
  addNewFolder: (params: AddNewFolderParams) =>
    httpThuVien.post<ResponseApi<Folder>>(`${CONTROLLER}/InsertAndEdit`, params),
  deleteFolder: (id: string) => httpThuVien.post<ResponseApi>(`${CONTROLLER}/DeleteOnClick`, { Id: id, TenThuMuc: '' }),
  mergeFoldersWithSameName: (params: { idFrom: string; idTarget: string }) =>
    httpThuVien.post<ResponseApi<{ ListModel: BookInFolder[]; count: number }>>(
      `${CONTROLLER}/Merge?${serialize(params)}`
    ),

  // BOOK_CONTROLLER
  getBooksInFolder: (params: { value: string; page: number; pageSize: number; IdThuMucSach: string; Type: string }) =>
    httpThuVien.post<ResponseApi<{ ListModel: BookInFolder[]; count: number }>>(`${BOOK_CONTROLLDER}/SearchOpac`, {
      ...params,
      ListQuery: [{ Operator: '&', SearchField: 'all', Value: params.value || '' }]
    }),
  deleteBookInFolder: (id: string) => httpThuVien.delete<ResponseApi>(`${BOOK_CONTROLLDER}/DeleteBook/${id}`),
  getBookFiles: (id: string) =>
    httpThuVien.get<
      ResponseApi<{
        Marc21: string;
        ListParam: string[];
        ListMedia: FileContent[];
        LinkBiaSach: string;
      }>
    >(`${BOOK_CONTROLLDER}/DetailBook/${id}`),
  deleteBooksInfolder: (ids: string[]) =>
    httpThuVien.delete<ResponseApi>(`${BOOK_CONTROLLDER}/DeleteMulti`, {
      data: ids
    }),
  moveBooksToFolder: (folderId: string, bookIds: string[]) =>
    httpThuVien.post<ResponseApi>(`${BOOK_CONTROLLDER}/CapNhatThuMucSach?idTMS=${folderId}`, bookIds)
};

export default folderApis;
