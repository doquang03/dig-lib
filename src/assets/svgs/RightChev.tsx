import React from 'react';

function RightChev() {
  return (
    <svg width={6} height={10} viewBox='0 0 6 10' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path
        d='M4.25476e-07 8.825L3.7085 5L9.10842e-08 1.175L1.1417 2.12363e-07L6 5L1.1417 10L4.25476e-07 8.825Z'
        fill='#184785'
      />
    </svg>
  );
}

export default RightChev;
