import { Tooltip } from 'antd';
import { Button } from 'components';
import { useLanguages } from 'hooks';
import { useMemo } from 'react';
import { useNavigate } from 'react-router-dom';

type Props = {
  onSelect: (book: Book) => void;
  book: Book;
  index: number;
  serialNumber?: number | string;
  options?: {
    optionsBST: Selections;
    optionsChuDiem: Selections;
    optionsMonHoc: Selections;
    optionsNXB: Selections;
    optionsTacGia: Selections;
    optionsTMS: Selections;
  };
};

const BookItem = (props: Props) => {
  const {
    book: {
      Id,
      ISBN,
      MaKiemSoat,
      TenSach,
      NamXuatBan,
      SoLuongTong,
      Ten_NhaXuatBan,
      SoLuongConLai,
      LinkBiaSach,
      IdBoSuuTap,
      IdThuMucSach,
      Ten_NgonNgu,
      Id_TacGia,
      IsDigitalResource
    },
    onSelect,
    index,
    serialNumber,
    options
  } = props;

  const navigate = useNavigate();

  const { data: languageData } = useLanguages();

  const collection = options?.optionsBST?.find(({ value }) => value === IdBoSuuTap)?.label;
  const authors = options?.optionsTacGia?.filter(({ value }) => Id_TacGia?.includes(value + ''));
  const nhaXuatban =
    options?.optionsNXB?.find(({ value }) => value === Ten_NhaXuatBan)?.value !== ''
      ? options?.optionsNXB?.find(({ value }) => value === Ten_NhaXuatBan)?.label
      : '--';

  const document =
    options?.optionsTMS?.find(({ value }) => value === IdThuMucSach)?.value !== ''
      ? options?.optionsTMS?.find(({ value }) => value === IdThuMucSach)?.label
      : '--';

  const language = useMemo(() => {
    if (!languageData?.length) return;

    return languageData?.find(({ Id }) => Id === Ten_NgonNgu)?.Ten || '--';
  }, [Ten_NgonNgu, languageData]);

  const handleUpdateBook = () => navigate(`CapNhat/${Id}`);

  const handleOnChange = (book: Book) => () => {
    onSelect(book);
  };

  return (
    <div className='rounded-lg border border-x-2 border-b-2 border-primary-10 shadow-md'>
      <div className='flex items-center justify-between gap-1 rounded-t-md bg-primary-10 px-1 py-2'>
        <div className='flex items-center gap-2'>
          <span className='rounded-full bg-white py-1 px-2 text-xs text-primary-10'>{serialNumber}</span>
          {TenSach && TenSach.length > 20 ? (
            <Tooltip placement='topLeft' title={TenSach} arrow={true}>
              <span className='font-bold text-white'>{TenSach.substring(0, 20).concat('...')}</span>
            </Tooltip>
          ) : (
            <span className='font-bold text-white'>{TenSach}</span>
          )}

          <span className='font-semibold text-white'>
            -{' '}
            {MaKiemSoat && MaKiemSoat.length > 10 ? (
              <Tooltip placement='topLeft' title={MaKiemSoat} arrow={true}>
                <span className=' font-bold text-white'>{MaKiemSoat.substring(0, 10).concat('...')}</span>
              </Tooltip>
            ) : (
              <span className=' font-bold text-white'>{MaKiemSoat}</span>
            )}
          </span>
        </div>

        <input type='checkbox' onChange={handleOnChange(props.book)} checked={index !== -1} />
      </div>

      <div className='flex gap-4 p-2'>
        <img src={LinkBiaSach || '/content/Book.png'} alt='Bìa sách' className='mb-[21px] h-[168px] w-[150px]' />

        <div className='w-full flex-col'>
          <p className='font-semibold'>
            ISBN: <span className=' font-bold'>{ISBN || '--'}</span>
          </p>
          <p className='font-semibold'>
            Bộ sưu tập:{' '}
            {collection && collection.length > 18 ? (
              <Tooltip placement='topLeft' title={collection} arrow={true}>
                <span className=' font-bold'>{collection.substring(0, 18).concat('...')}</span>
              </Tooltip>
            ) : (
              <span className=' font-bold'>{collection}</span>
            )}
          </p>
          <p className='font-semibold'>
            Tác giả:{' '}
            {authors?.length ? (
              authors?.length > 2 ? (
                <Tooltip
                  title={authors?.map(({ label }) => (
                    <p key={label}>{label}</p>
                  ))}
                >
                  {authors?.slice(0, 2).map((author, index) => (
                    <span className='font-bold' key={index}>
                      {author.label.length > 20
                        ? // <Tooltip placement='topLeft' title={author.label} arrow={true}>
                          author.label.substring(0, 20).concat('...')
                        : // </Tooltip>
                          author.label}
                      {authors.length - 1 === index ? '' : ', '}
                    </span>
                  ))}
                  ...
                </Tooltip>
              ) : (
                authors?.map((author, index) => (
                  <span className='font-bold' key={index}>
                    {author.label.length > 20 ? (
                      <Tooltip placement='topLeft' title={author.label} arrow={true}>
                        {author.label.substring(0, 20).concat('...')}
                      </Tooltip>
                    ) : (
                      author.label
                    )}
                    {authors.length - 1 === index ? '' : ', '}
                  </span>
                ))
              )
            ) : (
              <span className='font-bold'>--</span>
            )}
          </p>
          <p className='font-semibold'>
            Thư mục tài liệu:{' '}
            {document && document.length > 25 ? (
              <Tooltip placement='topLeft' title={document} arrow={true}>
                <span className='font-bold'>{document.substring(0, 25).concat('...')}</span>
              </Tooltip>
            ) : (
              <span className='font-bold'>{document}</span>
            )}{' '}
          </p>
          <p className='font-semibold'>
            Ngôn ngữ: <span className=' font-bold'>{language}</span>
          </p>
          <p className='font-semibold'>
            Năm xuất bản: <span className=' font-bold'>{NamXuatBan}</span>
          </p>
          <p className='font-semibold'>
            Nhà xuất bản:{' '}
            <span className=' font-bold'>
              {' '}
              {nhaXuatban && nhaXuatban.length > 15 ? (
                <Tooltip placement='topLeft' title={nhaXuatban} arrow={true}>
                  <span className=' font-bol'>{nhaXuatban.substring(0, 15).concat('...')}</span>
                </Tooltip>
              ) : (
                <span className=' font-bold'>{nhaXuatban}</span>
              )}{' '}
            </span>
          </p>
          <div className='flex items-center gap-2'>
            <p className='font-semibold'>
              Tổng: <span className=' font-bold'>{SoLuongTong}</span>
            </p>
            <p className='font-semibold'>
              Kho: <span className=' font-bold'>{SoLuongConLai}</span>
            </p>
          </div>
        </div>
      </div>

      <div className='mt-1 flex shrink-0 justify-end gap-1 px-2 pb-2 align-bottom'>
        {IsDigitalResource && (
          <div className='rounded-md border border-[#D9C510]/80 bg-[#D9C510]/10 p-2'>
            <p className='font-bold text-[#D9C510]'>Tài liệu giấy & số</p>
          </div>
        )}

        <Button variant='primary' onClick={handleUpdateBook}>
          Cập nhật
        </Button>
      </div>
    </div>
  );
};

export default BookItem;
