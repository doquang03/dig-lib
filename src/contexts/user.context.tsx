import { useQuery } from '@tanstack/react-query';
import { authApis, connectionApis } from 'apis';
import { IDENTITY_CONFIG } from 'constants/config';
import { createContext, useContext, useMemo } from 'react';
import { useAuth } from 'react-oidc-context';
import { getUnitFromSS } from 'utils/utils';

// create initial user value
const initialUserValue: User = {
  AccountType: 0,
  Address: null,
  Avatar: null,
  DateOfBirth: '',
  Email: '',
  FirstName: '',
  FullName: '',
  Gender: 0,
  Id: '',
  IdDVCT_Chinh: '',
  IdDVCT_TamThoi: null,
  IdDVCT_Working: '',
  LastName: '',
  LogoSchool: '',
  NameSchool: '',
  Permissions: [],
  PhoneNumber: null,
  Roles: [],
  UserName: '',
  TypeSchool: []
};

type ContextValue = {
  profile: User | undefined;
  isLoading: boolean;
  userType?: number;
  reset?: () => void;
  isAllowedAdjustment?: boolean;
  TypeSchool?: string[];
  permission?: { Enable: boolean; EnableDigital: boolean } | undefined;
};

// create initial context value
const initialContextValue = {
  profile: initialUserValue,
  isLoading: false
};
export const UserConText = createContext<ContextValue>(initialContextValue);

export const UserProvider = ({ children }: { children: React.ReactNode }) => {
  const { isAuthenticated, removeUser, clearStaleState, signoutSilent } = useAuth();

  const {
    data,
    isLoading,
    remove: removeProfileQuery
  } = useQuery({
    queryKey: ['profile', isAuthenticated],
    queryFn: authApis.getProfile,
    onError: () => {
      clearStaleState();
      removeUser();
      signoutSilent({ post_logout_redirect_uri: IDENTITY_CONFIG.post_logout_redirect_uri });
    },
    enabled: isAuthenticated
  });

  const {
    data: workplaceData,
    isLoading: loadingWorkPlace,
    remove: removeWorkplaceQuery
  } = useQuery({
    queryKey: ['profile', isAuthenticated, data?.data.Item.IdDVCT_Chinh],
    queryFn: () => authApis.DonViCongTac(data?.data.Item.IdDVCT_Chinh + ''),
    enabled: isAuthenticated && Boolean(data?.data.Item?.IdDVCT_Chinh)
  });

  const {
    data: dataPermision,
    isLoading: loadingPermission,
    remove: removePermission
  } = useQuery({
    queryKey: ['GetPermissionHostLibrary'],
    queryFn: connectionApis.GetPermissionHostLibrary,
    enabled: isAuthenticated
  });

  // userType = 2001 => Sở | Phòng
  // userType = 3000 => Trường
  const userType = useMemo(() => {
    return workplaceData?.data.Item.LoaiHinh;
  }, [workplaceData?.data.Item.LoaiHinh]);

  // get only user is 2001 and accessed into some schools
  const unitInfor = getUnitFromSS(userType);

  const reset = () => {
    removeWorkplaceQuery();
    removeProfileQuery();
    removePermission();
  };

  return (
    <UserConText.Provider
      value={{
        profile: data?.data?.Item,
        userType,
        isLoading: isLoading || loadingWorkPlace || loadingPermission,
        reset,
        isAllowedAdjustment: unitInfor?.isAllowedAdjustment || userType === 3000,
        TypeSchool: data?.data?.Item.TypeSchool,
        permission: dataPermision?.data?.Item
      }}
    >
      {children}
    </UserConText.Provider>
  );
};

export const useUser = () => {
  return useContext(UserConText);
};
