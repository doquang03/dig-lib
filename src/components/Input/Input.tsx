import classNames from 'classnames';
import type { ElementType, InputHTMLAttributes, KeyboardEvent, ReactNode } from 'react';
import type { FieldPath, FieldValues, RegisterOptions, UseFormRegister } from 'react-hook-form/dist/types';

type Props<TFieldValues extends FieldValues> = {
  containerClassName?: string;
  register?: UseFormRegister<TFieldValues>;
  rules?: RegisterOptions;
  errorMessage?: string | boolean;
  classNameError?: string;
  name?: FieldPath<TFieldValues>;
  component?: ElementType;
  left?: ReactNode;
  right?: ReactNode;
} & InputHTMLAttributes<HTMLInputElement>;

export default function Input<TFieldValues extends FieldValues>(props: Props<TFieldValues>) {
  const {
    containerClassName = '',
    register,
    name,
    rules,
    errorMessage,
    classNameError = 'text-[14px] text-red-500 mt-1',
    component = 'input',
    left,
    right,
    ...rest
  } = props;
  const registerResult = register && name ? register(name, rules) : null;

  const Component = component;

  const onKeyDown = (e: KeyboardEvent<HTMLInputElement>) => {
    if (rest.type === 'number' && e.code === 'KeyE') {
      e.preventDefault();
    }
  };

  return (
    <div className={classNames(`max-md:w-full ${containerClassName}`)}>
      {left && left}
      <Component
        onKeyDown={onKeyDown}
        className={classNames(
          'w-full rounded-md border border-gray-300 py-3.5 px-2 outline-none focus:border-primary-30 focus:shadow-sm' +
            rest.className,
          {
            'border-red-500 focus:border-red-500': !!errorMessage
          }
        )}
        {...rest}
        {...registerResult}
        name={name}
      />
      {right && right}
      {!!errorMessage && <p className={classNameError}>{errorMessage}</p>}
    </div>
  );
}
