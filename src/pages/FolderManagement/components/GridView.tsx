import { type BookInFolder } from 'apis/folder.apis';
import { Empty } from 'components';
import { useWindowDimensions } from 'hooks';
import { type MouseEvent } from 'react';
import { useParams } from 'react-router';
import { useFolderManagement } from '../contexts/useFolderManagement';
import BookCard from './BookCard';
import { Checkbox } from 'antd';
import type { CheckboxChangeEvent } from 'antd/es/checkbox';

type Props = {
  data: BookInFolder[];
};

function GridView(props: Props) {
  const { data } = props;

  const { height } = useWindowDimensions();

  const { bookId } = useParams();

  const { setContextBook, selectedBookIds, setSelectedBookIds } = useFolderManagement();

  function handleShowContextBookMenu(e: MouseEvent<HTMLDivElement, globalThis.MouseEvent>, book: BookInFolder) {
    e.preventDefault();

    const { clientX: x, clientY: y } = e;

    setContextBook({
      x,
      y,
      book
    });
  }

  function handleCheckAll(e: CheckboxChangeEvent) {
    e.stopPropagation();

    for (let index = 0; index < data.length; index++) {
      const element = data[index];

      if (selectedBookIds.length === data.length) {
        setSelectedBookIds([]);
      } else {
        if (selectedBookIds.includes(element?.Id || '')) continue;
        else setSelectedBookIds((prevState) => [...prevState, element?.Id || '']);
      }
    }
  }

  function handleSelectBook(bookId: string) {
    console.log('bookId', bookId);
    if (selectedBookIds.includes(bookId)) {
      setSelectedBookIds(selectedBookIds.filter((item) => item !== bookId));
    } else {
      setSelectedBookIds((prevState) => [...prevState, bookId]);
    }
  }

  return (
    <div className='flex'>
      <div className='relative flex flex-grow gap-1'>
        <div
          className='w-full flex-1 flex-col overflow-y-auto'
          style={{
            maxHeight: height - 50 - 42 - 150
          }}
        >
          {!!data.length && (
            <Checkbox onChange={handleCheckAll} checked={selectedBookIds.length === data.length}>
              Chọn tất cả
            </Checkbox>
          )}

          <div className='grid grid-cols-8 gap-2'>
            {data?.map((item) => (
              <div className='col-span-2' key={item.Id}>
                <BookCard
                  id={item.Id + ''}
                  image={item.AnhBia || ''}
                  name={item.TenSach || ''}
                  isSelected={bookId === item.Id}
                  onContextMenu={(e) => handleShowContextBookMenu(e, item)}
                  onSelect={() => handleSelectBook(item.Id || '')}
                />
              </div>
            ))}
          </div>

          {!data.length && (
            <div
              className={`grid place-items-center`}
              style={{
                height: height - 50 - 42 - 150
              }}
            >
              <Empty />
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default GridView;
