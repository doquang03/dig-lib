import { useQuery } from '@tanstack/react-query';
import { Spin } from 'antd';
import { folderApis } from 'apis';
import { memo, useState } from 'react';
import { Document, Page, pdfjs } from 'react-pdf';
import { useParams } from 'react-router';
import { useFolderManagement } from '../contexts/useFolderManagement';

import { Empty } from 'components';
import { AUDIO_FILE_TYPE, VIDEO_FILE_TYPE } from 'constants/config';
import { path } from 'constants/path';
import { useNavigate } from 'react-router-dom';

pdfjs.GlobalWorkerOptions.workerSrc = new URL('pdfjs-dist/build/pdf.worker.min.js', import.meta.url).toString();

function BookFilesSection() {
  const { isPreviewMode } = useFolderManagement();

  const [numPages, setNumPages] = useState<number>();

  const { id, bookId } = useParams();

  const navigate = useNavigate();

  const { data, isLoading } = useQuery({
    queryKey: ['getBookFiles', bookId],
    queryFn: () => folderApis.getBookFiles(bookId + ''),
    enabled: !!bookId,
    onError: () => {
      navigate(`${path.folderManagement}/${id}`);
    }
  });

  const files = data?.data.Item.ListMedia || ([] as FileContent[]);

  function onDocumentLoadSuccess({ numPages }: { numPages: number }): void {
    setNumPages(numPages);
  }

  const options = {
    cMapUrl: 'cmaps/',
    cMapPacked: true,
    standardFontDataUrl: 'standard_fonts/'
  };

  const file = files?.[0] || ({} as FileContent);

  const fileType = file?.FileName?.split('.');

  const isPPTFile = ['pptx', 'ppt'].includes(fileType?.[fileType.length - 1]);

  const isPdfFile = ['pdf'].includes(fileType?.[fileType.length - 1]);

  const isAudio = AUDIO_FILE_TYPE.includes(fileType?.[fileType.length - 1]);

  const isVideo = VIDEO_FILE_TYPE.includes(fileType?.[fileType.length - 1]);

  return (
    <>
      {isLoading ? (
        <div className='grid h-full place-items-center'>
          <Spin spinning />
        </div>
      ) : (
        !!files.length &&
        // ppt & pdf & audio & video
        (files.length === 1 ? (
          <>
            {isPPTFile && (
              <>
                <a href={file.LinkFile} target='_blank' rel='noreferrer'>
                  <img src={'/content/ppt.png'} alt={file.FileName} className='h-1/2 w-full shadow-md' />

                  <span className='text-sm italic text-[#00000061] hover:underline'>{file.FileName}</span>
                </a>
              </>
            )}

            {isPdfFile && (
              <Document
                file={file.LinkFile}
                onLoadSuccess={onDocumentLoadSuccess}
                renderMode='canvas'
                options={options}
              >
                {[...Array(numPages)].map((_, pageIndex) => (
                  <Page pageNumber={pageIndex + 1} width={isPreviewMode ? 520 : 0} key={pageIndex} />
                ))}
              </Document>
            )}

            {isAudio && (
              <audio controls className='w-full'>
                <source src={file.LinkFile} />
              </audio>
            )}

            {isVideo && (
              <video width={'100%'} height={700} controls autoPlay={false} key={file.FileName}>
                <source src={file.LinkFile} type='video/mp4' />
              </video>
            )}
          </>
        ) : (
          // image
          files.length > 1 && (
            <div className='flex flex-col items-center gap-1'>
              {files.map((file) => (
                <img src={file.LinkFile} key={file.PageNumber} alt={file.FileName} className='h-full w-full' />
              ))}
            </div>
          )
        ))
      )}

      {!files.length && !isLoading && (
        <div className='grid h-full place-items-center'>
          <Empty />
        </div>
      )}
    </>
  );
}

export default memo(BookFilesSection);
