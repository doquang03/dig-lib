import { CaretDownOutlined } from '@ant-design/icons';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { Dropdown, Modal, Switch, Table, Tooltip } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { SorterResult } from 'antd/es/table/interface';
import { connectionApis } from 'apis';
import { Button, Loading, ModalDelete, TitleDelete } from 'components';
import { useUser } from 'contexts/user.context';
import dayjs from 'dayjs';
import { useState } from 'react';
import { toast } from 'react-toastify';
import { getSerialNumber } from 'utils/utils';

type Props = {
  loading: boolean;
  onDisconnect: VoidFunction;
};

const SettingPermission = (props: Props) => {
  const [{ page, pageSize }] = useState<{ page: number; pageSize: number; sort: string }>({
    page: 1,
    pageSize: 30,
    sort: ''
  });
  const [isVisibleDisconnectionModal, setIsVisibleDisconnectionModal] = useState<boolean>(false);

  const {
    data: listOfUnitsInHostData,
    isLoading: loadingListOfUnitsInHost,
    refetch,
    isRefetching
  } = useQuery({
    queryKey: ['getListOfUnitsInHost'],
    queryFn: connectionApis.getUnitsInHost
  });

  const listOfUnitsInHost = listOfUnitsInHostData?.data.Item?.ListModel || [];

  const queryClient = useQueryClient();

  const { profile } = useUser();

  const { mutate: disconnect, isLoading: loadingDisconnect } = useMutation({
    mutationFn: connectionApis.disconnect,
    onSuccess: () => {
      toast.success('Ngắt kết nối mục lục liên hợp thành công');
      queryClient.invalidateQueries(['getUnitConnectionStatus']);
      setIsVisibleDisconnectionModal(false);
      props.onDisconnect();
    }
  });

  const { mutate: adjustPermission, isLoading: loadingAdjustPermission } = useMutation({
    mutationFn: connectionApis.adjustPermission,
    onSuccess: () => {
      refetch();
    }
  });

  const handleDisconnect = () => {
    disconnect();
  };

  const changeStatus = (params: { unit: UnitInHost; isPassive?: boolean; isProactive?: boolean }) => {
    const { MemberName, WorkingId } = params.unit;

    const reqBody: { Passive?: boolean; Proactive?: boolean } = {};

    if (params.isPassive) {
      reqBody.Passive = true;
    } else {
      reqBody.Passive = false;
    }

    if (params.isProactive) {
      reqBody.Proactive = true;
    } else {
      reqBody.Proactive = false;
    }

    adjustPermission({ MemberName, WorkingId, ...reqBody });
  };

  const columns: ColumnsType<UnitInHost> = [
    {
      key: 'STT',
      title: 'STT',
      render: (value, record, index) => {
        return getSerialNumber(page, pageSize, index);
      },
      width: 80
    },
    {
      key: 'TMemberName',
      title: 'Thư viện thành viên',
      dataIndex: 'MemberName',
      render: (value, { MemberName }) => <p className='text-left'>{MemberName}</p>
    },
    {
      key: 'Thời gian kết nối',
      title: 'Thời gian kết nối',
      dataIndex: 'ConnectionTime',
      width: 180,
      render: (value, { ConnectionTime }) =>
        ConnectionTime ? dayjs(ConnectionTime).format('HH:mm:ss DD/MM/YYYY') : '--',
      sorter: true
    },
    { key: 'Tổng số biểu ghi', title: 'Tổng số biểu ghi', dataIndex: 'TotalRecord', width: 150 },
    {
      key: 'Cho phép truy cập',
      title: (
        <Tooltip title='Cho phép các thành viên của thư viện này kết nối đến'>
          <p className='flex items-center justify-center gap-2 '>
            Cho phép truy cập{' '}
            <svg width='12' height='12' viewBox='0 0 12 12' fill='none' xmlns='http://www.w3.org/2000/svg'>
              <path
                d='M6 0C2.68594 0 0 2.68594 0 6C0 9.31406 2.68594 12 6 12C9.31406 12 12 9.31406 12 6C12 2.68594 9.31406 0 6 0ZM6 10.875C3.31172 10.875 1.125 8.68805 1.125 6C1.125 3.31195 3.31172 1.125 6 1.125C8.68828 1.125 10.875 3.31195 10.875 6C10.875 8.68805 8.68828 10.875 6 10.875ZM6 7.875C5.57812 7.875 5.25 8.20312 5.25 8.625C5.25 9.04688 5.55703 9.375 6 9.375C6.40078 9.375 6.75 9.04688 6.75 8.625C6.75 8.20312 6.40078 7.875 6 7.875ZM6.77578 3H5.57812C4.66406 3 3.9375 3.72656 3.9375 4.64062C3.9375 4.94531 4.19531 5.20312 4.5 5.20312C4.80469 5.20312 5.0625 4.94531 5.0625 4.64062C5.0625 4.35938 5.27578 4.125 5.55703 4.125H6.75469C7.05703 4.125 7.3125 4.35938 7.3125 4.64062C7.3125 4.82812 7.21875 4.97109 7.05469 5.06484L5.71875 5.88281C5.53125 6 5.4375 6.1875 5.4375 6.375V6.75C5.4375 7.05469 5.69531 7.3125 6 7.3125C6.30469 7.3125 6.5625 7.05469 6.5625 6.75V6.70312L7.61953 6.04688C8.11172 5.74219 8.41641 5.20312 8.41641 4.64062C8.4375 3.72656 7.71094 3 6.77578 3Z'
                fill='white'
              />
            </svg>
          </p>
        </Tooltip>
      ),
      render: (value, record) => {
        return (
          <Switch
            size='small'
            checked={record.Proactive}
            onChange={(value) =>
              !loadingAdjustPermission &&
              changeStatus({ unit: record, isProactive: !record.Proactive, isPassive: record.Passive })
            }
          />
        );
      },
      width: 200
    },
    {
      key: 'Được phép truy cập',
      title: (
        <Tooltip title='Cho phép các thành viên trong thư viện kết nối đến thư viện này'>
          <p className='flex items-center justify-center gap-2'>
            Được phép truy cập{' '}
            <svg width='12' height='12' viewBox='0 0 12 12' fill='none' xmlns='http://www.w3.org/2000/svg'>
              <path
                d='M6 0C2.68594 0 0 2.68594 0 6C0 9.31406 2.68594 12 6 12C9.31406 12 12 9.31406 12 6C12 2.68594 9.31406 0 6 0ZM6 10.875C3.31172 10.875 1.125 8.68805 1.125 6C1.125 3.31195 3.31172 1.125 6 1.125C8.68828 1.125 10.875 3.31195 10.875 6C10.875 8.68805 8.68828 10.875 6 10.875ZM6 7.875C5.57812 7.875 5.25 8.20312 5.25 8.625C5.25 9.04688 5.55703 9.375 6 9.375C6.40078 9.375 6.75 9.04688 6.75 8.625C6.75 8.20312 6.40078 7.875 6 7.875ZM6.77578 3H5.57812C4.66406 3 3.9375 3.72656 3.9375 4.64062C3.9375 4.94531 4.19531 5.20312 4.5 5.20312C4.80469 5.20312 5.0625 4.94531 5.0625 4.64062C5.0625 4.35938 5.27578 4.125 5.55703 4.125H6.75469C7.05703 4.125 7.3125 4.35938 7.3125 4.64062C7.3125 4.82812 7.21875 4.97109 7.05469 5.06484L5.71875 5.88281C5.53125 6 5.4375 6.1875 5.4375 6.375V6.75C5.4375 7.05469 5.69531 7.3125 6 7.3125C6.30469 7.3125 6.5625 7.05469 6.5625 6.75V6.70312L7.61953 6.04688C8.11172 5.74219 8.41641 5.20312 8.41641 4.64062C8.4375 3.72656 7.71094 3 6.77578 3Z'
                fill='white'
              />
            </svg>
          </p>
        </Tooltip>
      ),
      render: (value, record) => {
        return (
          <Switch
            size='small'
            checked={record.Passive}
            onChange={(value) =>
              !loadingAdjustPermission &&
              changeStatus({ unit: record, isPassive: !record.Passive, isProactive: record.Proactive })
            }
          />
        );
      },
      width: 200
    }
  ];

  const handleTableChange = (pagination: any, filters: any, sorter: SorterResult<UnitInHost>) => {
    if (sorter.order === 'ascend') {
      listOfUnitsInHost.sort((a, b) => (dayjs(a.ConnectionTime).isAfter(dayjs(b.ConnectionTime)) ? 1 : -1));
    }

    if (sorter.order === 'descend') {
      listOfUnitsInHost.sort((a, b) => (dayjs(b.ConnectionTime).isAfter(dayjs(a.ConnectionTime)) ? 1 : -1));
    }
  };

  if (props.loading) {
    return (
      <Modal open centered footer={null} closable={false}>
        <p>Hệ thống đang kiểm trang tình trạng kết nối của đơn vị {profile?.NameSchool}.</p>
        <p>Vui lòng đợi. </p>
      </Modal>
    );
  }

  return (
    <div>
      <div className='mt-3 flex items-center justify-between'>
        <Dropdown
          menu={{
            items: [
              {
                key: 'disconnect',
                label: 'Ngắt kết nối',
                onClick: () => setIsVisibleDisconnectionModal(true)
              }
            ]
          }}
        >
          <Button>
            Tiện ích
            <CaretDownOutlined />
          </Button>
        </Dropdown>
      </div>

      <p className='text-[20px] font-bold text-primary-10'>Danh sách thư viện thành viên</p>

      <div className='my-3 flex items-center justify-between'>
        <div className='flex items-center gap-1'>
          <svg width='18' height='14' viewBox='0 0 18 14' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path d='M5.59 10.58L1.42 6.41L0 7.82L5.59 13.41L17.59 1.41L16.18 0L5.59 10.58Z' fill='#119757' />
          </svg>

          <p className='text-tertiary-30'>Tổng số thư viện đang kết nối: {listOfUnitsInHost.length}</p>
        </div>

        <div className='flex items-center gap-1'>
          <svg width='18' height='14' viewBox='0 0 18 14' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path d='M5.59 10.58L1.42 6.41L0 7.82L5.59 13.41L17.59 1.41L16.18 0L5.59 10.58Z' fill='#119757' />
          </svg>

          <p className='text-tertiary-30'>
            Tổng số biểu ghi: {listOfUnitsInHost.reduce((total, unit) => (total += unit.TotalRecord), 0)}
          </p>
        </div>
      </div>

      <Table
        className='custom-table mb-3'
        dataSource={listOfUnitsInHost}
        columns={columns}
        bordered
        pagination={false}
        loading={loadingListOfUnitsInHost}
        // @ts-ignore
        onChange={handleTableChange}
        rowKey={(row) => row.MemberName}
        locale={{
          triggerDesc: 'Click để sắp xếp tăng dần',
          triggerAsc: 'Click để sắp xếp giảm dần',
          cancelSort: 'Click để hủy sắp xếp'
        }}
      />

      <ModalDelete
        open={isVisibleDisconnectionModal}
        title={<TitleDelete firstText='Bạn có chắc chắn muốn Ngắt kết nối với mục lục liên hợp' />}
        handleOk={handleDisconnect}
        handleCancel={() => setIsVisibleDisconnectionModal(false)}
        labelOK='Ngắt kết nối'
        loading={loadingDisconnect || props.loading}
      />

      <Loading open={loadingAdjustPermission || isRefetching} />
    </div>
  );
};

export default SettingPermission;
