import { Tabs, TabsProps } from 'antd';
import classNames from 'classnames';
import { ReactNode } from 'react';

type Props = {
  className?: string;
  items: TabsProps['items'];
  activeKey: string;
  onChange: (activeKey: string) => void;
  right?: ReactNode;
} & TabsProps;

const TabsApp = (props: Props) => {
  const { items = [], activeKey, onChange, className, right } = props;

  const renderTabBar = () => {
    return (
      <div className={classNames('flex w-full flex-grow items-center gap-2')}>
        {items.map(({ label, key }) => {
          const tabActive = key === activeKey;

          return (
            <button
              key={key}
              onClick={() => onChange(key)}
              className={classNames('rounded py-2 px-4 shadow-md shadow-black/40', {
                'transform bg-tertiary-10 font-semibold text-primary-10 transition-all delay-100 duration-200 ease-linear':
                  tabActive,
                'bg-white text-black/40': !tabActive
              })}
            >
              {label}
            </button>
          );
        })}

        {right}
      </div>
    );
  };

  return (
    <Tabs
      className={classNames(className)}
      type='card'
      size='large'
      items={items}
      activeKey={activeKey}
      animated={true}
      renderTabBar={renderTabBar}
      destroyInactiveTabPane={true}
    />
  );
};

export default TabsApp;
