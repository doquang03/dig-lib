import { useQueries, useQuery } from '@tanstack/react-query';
import { Spin, Table } from 'antd';
import { expenseApis, settingApis } from 'apis';
import { Button, Title } from 'components';
import { SCHOOL_YEAR_OPTIONS_1 } from 'constants/options';
import dayjs from 'dayjs';
import { ChangeEvent, useMemo, useRef, useState } from 'react';
import { Bar } from 'react-chartjs-2';

import { ColumnsType } from 'antd/es/table';
import {
  BarElement,
  CategoryScale,
  Chart as ChartJS,
  Legend,
  LinearScale,
  Title as ChartTitle,
  Tooltip
} from 'chart.js';
import { useNavigate } from 'react-router-dom';

ChartJS.register(CategoryScale, LinearScale, BarElement, ChartTitle, Tooltip, Legend);

export const options = {
  responsive: true,
  maintainAspectRatio: false,
  plugins: {
    datalabels: {
      display: false
    },
    legend: {
      display: true,
      position: 'bottom' as const
    },
    title: {
      display: true
    }
  },
  scales: {
    x: {
      stacked: true,
      title: {
        display: false
      }
    },
    y: {
      stacked: true,
      title: {
        display: false
      }
    }
  }
};

const ExpenseChart = () => {
  const [yearValue, setYearValue] = useState<number>(new Date().getFullYear());
  const [labels, setLabels] = useState<string[]>([SCHOOL_YEAR_OPTIONS_1[0].value]);

  const selectFromRef = useRef<HTMLSelectElement | null>(null);
  const selectToRef = useRef<HTMLSelectElement | null>(null);

  const navigate = useNavigate();

  const { data: startDateData, isLoading: loadingStartDateData } = useQuery({
    queryKey: ['startDate'],
    queryFn: settingApis.getStartDate,
    onSuccess: (data) => {}
  });

  const expensesQueries = useQueries({
    queries: labels.map((schoolYear) => {
      const date = startDateData?.data.Item.NgayBatDauNamHoc.split('/');

      const years = schoolYear.split('-');

      const minDate = `${date?.[1]}/${date?.[0]}/${years[0].trimEnd()}`;
      const maxDate = `${date?.[1]}/${date?.[0]}/${years[1].trimStart()}`;

      const min = dayjs(minDate).endOf('day').toISOString();
      const max = dayjs(maxDate).subtract(1, 'day').startOf('day').toISOString();

      return {
        queryKey: ['expenses', min, max],
        queryFn: () =>
          expenseApis.getMany(
            {
              Min: min,
              Max: max
            },
            1,
            0
          )
      };
    })
  });

  const schoolYearsTo = useMemo(() => {
    const tempArray = [{ value: '', label: 'Chọn năm học' }];
    for (let index = 0; index < SCHOOL_YEAR_OPTIONS_1.length; index++) {
      const elm = +SCHOOL_YEAR_OPTIONS_1[index].value.split('-')[0].trimStart();

      if (yearValue < elm) {
        tempArray.push(SCHOOL_YEAR_OPTIONS_1[index]);
      }
    }

    return tempArray;
  }, [yearValue]);

  const onClick = () => {
    selectFromRef && selectFromRef?.current?.focus();
  };

  const onClick1 = () => {
    selectToRef && selectToRef?.current?.focus();
  };

  const handleSelectSchoolYearFrom = (e: ChangeEvent<HTMLSelectElement>) => {
    if (!e || loadingStartDateData) return;

    const dateStart = startDateData?.data.Item.NgayBatDauNamHoc;

    if (!dateStart) return;

    const { value } = e.target;

    const years = value.split('-');

    setYearValue(+years[0].trimStart());

    if (labels.length <= 1) {
      setLabels([value]);

      return;
    }

    const lastYear = +years[0]?.trimEnd();

    const maxYearValue = labels[labels.length - 1];

    const diffYear = +maxYearValue.split('-')[1] - lastYear;

    const tempArray = [];
    for (let index = 0; index <= diffYear; index++) {
      tempArray.push(lastYear + index);
    }

    let _labels = [];
    for (let index = 0; index < tempArray.length - 1; index++) {
      _labels.push(`${tempArray[index]} - ${tempArray[index + 1]}`);
    }

    setLabels(_labels);
  };

  const handleSelectSchoolYearTo = (e: ChangeEvent<HTMLSelectElement>) => {
    if (!e || loadingStartDateData) return;

    const dateStart = startDateData?.data.Item.NgayBatDauNamHoc;

    if (!dateStart) return;

    const { value } = e.target;

    if (!value) {
      setLabels([`${yearValue} - ${yearValue + 1}`]);

      return;
    }

    const years = value.split('-');

    const lastYear = +years[1]?.trimEnd();

    const diffYear = lastYear - yearValue;

    const tempArray = [];
    for (let index = 0; index <= diffYear; index++) {
      tempArray.push(yearValue + index);
    }

    let _labels = [];
    for (let index = 0; index < tempArray.length - 1; index++) {
      _labels.push(`${tempArray[index]} - ${tempArray[index + 1]}`);
    }

    setLabels(_labels);
  };

  const data = {
    labels,
    datasets: [
      {
        label: 'Tổng thu',
        data: expensesQueries.map((query) => query.data?.data.Item.Sum_Thu),
        backgroundColor: '#119757',
        stack: 'Stack 0'
      },
      {
        label: 'Quỹ ngân sách',
        data: expensesQueries.map((query) => query.data?.data.Item.Sum_QuyNganSach),
        backgroundColor: '#3472A2',
        stack: 'Stack 0'
      },
      {
        label: 'Tổng chi',
        data: expensesQueries.map((query) => query.data?.data.Item.Sum_Chi),
        backgroundColor: '#dc3545',
        stack: 'Stack 1'
      }
    ]
  };

  const tableData = useMemo(() => {
    if (!expensesQueries.length) return [];

    const tempArray: {
      yearSchool: string;
      receipts: number;
      expenditures: number;
      budget: number;
      left: number;
    }[] = [];

    for (let index = 0; index < labels.length; index++) {
      const elmExpense = expensesQueries[index].data?.data.Item;
      const elemLabel = labels[index];

      tempArray.push({
        yearSchool: elemLabel,
        receipts: elmExpense?.Sum_Thu || 0,
        expenditures: elmExpense?.Sum_Chi || 0,
        budget: elmExpense?.Sum_QuyNganSach || 0,
        left:
          (elmExpense &&
            elmExpense?.Sum_QuyNganSach + elmExpense?.Sum_Thu + elmExpense?.Sum_Ton - elmExpense?.Sum_Chi) ||
          0
      });
    }

    return tempArray;
  }, [expensesQueries, labels]);

  const columns: ColumnsType<{
    yearSchool: string;
    receipts: number;
    expenditures: number;
    budget: number;
    left: number;
  }> = [
    {
      key: 'STT',
      title: 'STT',
      render: (value, record, index) => index + 1
    },
    {
      key: 'yearSchool',
      title: 'Năm học',
      dataIndex: 'yearSchool',
      sorter: (a, b) => +a.yearSchool.split('-')[0] - +b.yearSchool.split('-')[0]
    },
    {
      key: 'receipts',
      title: 'Tổng thu',
      dataIndex: 'receipts',
      render: (value, record, index) =>
        new Intl.NumberFormat('vi-VN', {
          style: 'currency',
          currency: 'VND',
          maximumFractionDigits: 9
        }).format(Number(record.receipts || 0)),
      sorter: (a, b) => a.receipts - b.receipts
    },
    {
      key: 'expenditures',
      title: 'Tổng chi',
      dataIndex: 'expenditures',
      render: (value, record, index) =>
        new Intl.NumberFormat('vi-VN', {
          style: 'currency',
          currency: 'VND',
          maximumFractionDigits: 9
        }).format(Number(record.expenditures || 0)),
      sorter: (a, b) => a.expenditures - b.expenditures
    },
    {
      key: 'budget',
      title: 'Quỹ ngân sách',
      dataIndex: 'budget',
      render: (value, record, index) =>
        new Intl.NumberFormat('vi-VN', {
          style: 'currency',
          currency: 'VND',
          maximumFractionDigits: 9
        }).format(Number(record.budget || 0)),
      sorter: (a, b) => a.budget - b.budget
    },
    {
      key: 'left',
      title: 'Tổng tồn',
      dataIndex: 'left',
      render: (value, record, index) =>
        new Intl.NumberFormat('vi-VN', {
          style: 'currency',
          currency: 'VND',
          maximumFractionDigits: 9
        }).format(Number(record.left || 0)),
      sorter: (a, b) => a.left - b.left
    }
  ];

  return (
    <div className='my-3'>
      <Title title='Theo dõi kinh phí thư viện qua các năm' />

      <div className='mt-3 flex items-center justify-between'>
        <div className='flex items-center gap-3'>
          <div className='flex items-center gap-1'>
            <p>Từ năm</p>

            <div className='relative w-[200px]' onClick={onClick}>
              <select
                className='date-picker top-2 z-20 h-10 w-[200px] rounded-md border border-primary-10  bg-transparent px-1 outline-none'
                ref={selectFromRef}
                onChange={handleSelectSchoolYearFrom}
              >
                {SCHOOL_YEAR_OPTIONS_1.map((schoolYear, index) => (
                  <option
                    key={index}
                    value={schoolYear.value}
                    style={{
                      marginLeft: '10px'
                    }}
                  >
                    {schoolYear.label}
                  </option>
                ))}
              </select>

              <div className='absolute right-2 top-2 z-10'>{loadingStartDateData ? <Spin spinning /> : <></>}</div>
            </div>
          </div>

          <div className='flex items-center gap-1'>
            <p>đến</p>

            <div className='relative w-[200px]' onClick={onClick1}>
              <select
                className='date-picker top-2 z-20 h-10 w-[200px] rounded-md border border-primary-10  bg-transparent px-1 outline-none'
                ref={selectToRef}
                onChange={handleSelectSchoolYearTo}
              >
                {schoolYearsTo.map((schoolYear, index) => (
                  <option key={index} value={schoolYear.value}>
                    {schoolYear.label}
                  </option>
                ))}
              </select>

              <div className='absolute right-2 top-2 z-10'>{loadingStartDateData ? <Spin spinning /> : <></>}</div>
            </div>
          </div>
        </div>
      </div>

      <h3 className='text-primary-10'>Biểu đồ theo dõi kinh phí thư viện</h3>

      <div className='my-3 flex items-center justify-center'>
        <div className='h-[500px] w-1/2'>
          <Bar options={options} data={data} fallbackContent={<Spin />} />
        </div>
      </div>

      <Table
        columns={columns}
        dataSource={tableData}
        rowKey={(row) => row.yearSchool}
        className='custom-table'
        bordered
      />
      <div className='flex items-center justify-end gap-2'>
        <Button onClick={() => navigate(-1)}>Quay về</Button>
      </div>
    </div>
  );
};

export default ExpenseChart;
