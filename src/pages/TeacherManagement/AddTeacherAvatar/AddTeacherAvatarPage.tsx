import { DeleteFilled } from '@ant-design/icons';
import { useMutation } from '@tanstack/react-query';
import { Table, Tooltip } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { teacherApis } from 'apis';
import { Button, Loading, ProgressBar, Title } from 'components';
import { path } from 'constants/path';
import 'css/AddTeacherAvatarPage.css';
import { useMemo, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';

type Mock = {
  STT: number;
  Ten: string;
  MaSoThanhVien: string;
  HinhChanDung: string;
  HinhHienTai: string;
};

type NotMatch = { STT: string; fileName: string };

const AddTeacherAvatarPage = () => {
  const [step, setStep] = useState<number>(1);
  // Mock state.

  const fileInutRef = useRef<HTMLInputElement | null>(null);

  const ref = useRef<HTMLDivElement>(null);

  const [file, setFile] = useState<File>();

  const [listAvatar, setListAvatar] = useState<Array<Mock>>([]);

  const [listError, setListError] = useState<Array<NotMatch>>([]);

  const [totalFail, setTotalFail] = useState();

  const [totalEntry, setTotalEntry] = useState();

  const [page, setPage] = useState(1);
  const [paginationSize, setPaginationSize] = useState(10);

  const navigate = useNavigate();

  const { mutate: UpLoadAvatar_Preview, isLoading: isLoadingEnable } = useMutation({
    mutationFn: (fileCurrent: File) => {
      let bodyFormData = new FormData();
      bodyFormData.append('file', fileCurrent as Blob);
      return teacherApis.UpLoadAvatar_Preview(bodyFormData);
    },
    onSuccess: (res) => {
      let tempData = res?.data?.Item;
      let RawDataList = tempData.RawDataList;
      let tempUnit: Mock;
      let result: Array<Mock> = [];
      for (let i = 0; i < RawDataList?.length; i++) {
        tempUnit = {
          STT: i,
          Ten: RawDataList[i][0],
          MaSoThanhVien: RawDataList[i][1],
          HinhChanDung: RawDataList[i][2],
          HinhHienTai: RawDataList[i][3]
        };
        result.push(tempUnit);
      }
      setListAvatar(result);

      let tempError = tempData?.ErrorName;
      let resultError: Array<NotMatch> = [];
      let ErrorUnit: NotMatch;
      for (let i = 0; i < tempError.length; i++) {
        ErrorUnit = {
          STT: (i + 1).toString(),
          fileName: tempError[i]
        };
        resultError.push(ErrorUnit);
      }
      setListError(resultError);

      setStep(2);
    }
  });

  const { mutate: UploadSave, isLoading: isLoadingUploadSave } = useMutation({
    mutationFn: (payload: Array<string>) => teacherApis.UpLoadAvatar_Save(payload),
    onSuccess: (res) => {
      setTotalEntry(res?.data?.Item?.ListSuccess?.length);
      setTotalFail(res?.data?.Item?.ToTalFail);
    }
  });

  const handleUploadSave = () => {
    let tempData;
    let result = [];
    for (let i = 0; i < listAvatar.length; i++) {
      tempData = [];
      tempData[0] = i;
      for (let param in listAvatar[i]) {
        switch (param) {
          case 'Ten':
            tempData[0] = listAvatar[i].Ten;
            break;
          case 'MaSoThanhVien':
            tempData[1] = listAvatar[i].MaSoThanhVien;
            break;
          case 'HinhChanDung':
            tempData[2] = listAvatar[i].HinhChanDung;
            break;
        }
      }
      result.push(JSON.stringify(tempData));
    }
    UploadSave(result);
  };

  const columnsTeacherData: ColumnsType<Mock> = useMemo(() => {
    const handleDeleteRow = (STT: number) => {
      let result: Array<Mock> = [];
      for (let i = 0; i < listAvatar.length; i++) {
        if (STT === listAvatar[i].STT) {
          continue;
        }
        result.push(listAvatar[i]);
      }
      setListAvatar(result);
    };

    let column: ColumnsType<Mock> = [
      {
        title: 'STT',
        dataIndex: 'STT',
        key: 'STT',
        render: (value: any, record: any, j: number) => (page - 1) * paginationSize + j + 1
      },
      {
        title: 'Tên giáo viên',
        dataIndex: 'Ten',
        key: 'Ten',
        render: (value, { Ten }) =>
          Ten.length > 25 ? (
            <Tooltip placement='topLeft' title={Ten} arrow={true}>
              <p className='text-center'>{Ten.substring(0, 25).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{Ten}</p>
          )
      },
      {
        title: 'Mã giáo viên',
        dataIndex: 'MaSoThanhVien',
        key: 'MaSoThanhVien'
      }
    ];
    if (step === 2)
      column.push({
        title: 'Ảnh hiện tại',
        dataIndex: 'currentAvatar',
        render: (value, record) => {
          return (
            <>
              {step === 2 && (
                <div className='flex items-center justify-center'>
                  <img
                    src={!record?.HinhHienTai ? '/content/default-avatar.jpeg' : record?.HinhHienTai}
                    alt=''
                    className='h-[80px] w-[80px]'
                  />
                </div>
              )}
            </>
          );
        }
      });
    column.push({
      title: (step === 2 && 'Ảnh cập nhật') || (step === 3 && 'Ảnh đại diện'),
      dataIndex: 'actions',
      key: 'actions',
      render: (value, record) => {
        return (
          <>
            <div className='flex items-center justify-center'>
              <img src={record?.HinhChanDung} alt='' className='h-[80px] w-[80px]' />
            </div>
          </>
        );
      }
    });
    if (step === 2)
      column.push({
        title: 'Hành động',
        dataIndex: 'actions',
        key: 'actions',
        render: (value, record) => {
          return (
            <>
              {step === 2 && (
                <button
                  className='mx-2'
                  onClick={(e) => {
                    e.stopPropagation();
                    handleDeleteRow(record.STT);
                  }}
                >
                  <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
                </button>
              )}
            </>
          );
        }
      });
    return column;
  }, [listAvatar, page, paginationSize, step]);

  const columnsNotMatchTable: ColumnsType<NotMatch> = [
    {
      title: 'STT',
      dataIndex: 'STT',
      key: 'STT',
      width: '100px'
    },
    {
      title: 'Tên file',
      dataIndex: 'fileName',
      key: 'fileName'
    }
  ];

  const handleOnFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files?.[0];

    const form = new FormData();
    if (fileFromLocal) {
      // TODO: Change name
      form.append('file', fileFromLocal);

      // TODO: Integrate api upload form
      // setStep to 2 on success
      setFile(fileFromLocal);
      UpLoadAvatar_Preview(fileFromLocal);
    }
  };

  const handleAccessFileInputRef = () => {
    fileInutRef.current?.click();
  };

  const handleBack = () => {
    navigate(path.giaovien);
  };

  const handleBackCurrent = () => {
    ref.current?.scrollIntoView({ behavior: 'smooth' });
    setListAvatar([]);
    setFile(undefined);
    setStep(1);
  };

  const handleSubmit = () => {
    // TODO: Integrate api to submit data
    ref.current?.scrollIntoView({ behavior: 'smooth' });
    handleUploadSave();
    setStep(3);
  };

  return (
    <div className='p-5' ref={ref}>
      <Title title='Cập nhật ảnh đại diện cho giáo viên' />

      <div className='my-10'>
        <ProgressBar activeStep={step} type='avatar' />
      </div>

      {/* TODO: Condition are step > 1 and data is not undefined*/}
      {step === 1 && (
        <div className='flex items-center justify-center rounded-md bg-tertiary-10 py-4'>
          <input
            type='file'
            accept='.zip,.rar,.7zip'
            className='hidden'
            ref={fileInutRef}
            onChange={handleOnFileChange}
          />

          <Button variant='default' className='ml-2' onClick={handleAccessFileInputRef}>
            Chọn tệp
          </Button>
        </div>
      )}

      <div>
        {/* show when data invalid is not undefined */}
        {step === 2 && !!listError?.length && (
          <div className='w-min'>
            <div className='w-full rounded-md bg-tertiary-40 px-3 py-4'>
              <span className='p-0'>
                Tổng số file không trùng khớp với mã giáo viên: <span className='font-bold'>{listError.length}</span>
              </span>
            </div>

            <Table
              rootClassName='w-[350px]'
              loading={isLoadingEnable}
              columns={columnsNotMatchTable}
              pagination={false}
              // mock data
              dataSource={listError}
              rowKey={(record) => record.STT}
              className='custom-table'
              scroll={{ y: 165 }}
              bordered
            />
          </div>
        )}
        {step === 2 && (
          <>
            <p className='mt-4'>
              Tổng số hình ảnh được nhận diện:
              <span className='font-bold'> {listAvatar?.length}</span>
            </p>

            {listAvatar?.length > 0 && (
              <h3 className='text-primary-10'>DANH SÁCH GIÁO VIÊN ĐƯỢC CẬP NHẬT ẢNH ĐẠI DIỆN</h3>
            )}
          </>
        )}

        {step === 3 && listAvatar.length > 0 && (
          <>
            <p className='text-tertiary-30'>
              Cập nhật thành công: <span className='font-bold'>{' ' + totalEntry} giáo viên</span>
            </p>

            <p className='text-danger-10'>
              Cập nhật thất bại: <span className='font-bold'>{' ' + listError.length} giáo viên</span>
            </p>

            <h3 className='text-primary-10'>DANH SÁCH GIÁO VIÊN CẬP NHẬT ẢNH ĐẠI DIỆN THÀNH CÔNG</h3>
          </>
        )}

        {!!listAvatar.length && (
          <>
            <div className='mt-6'>
              <Table
                loading={isLoadingEnable}
                columns={columnsTeacherData}
                pagination={{
                  onChange(current, pageSize) {
                    setPage(current);
                    setPaginationSize(pageSize);
                  },
                  defaultPageSize: 10,
                  hideOnSinglePage: true,
                  showSizeChanger: false
                }}
                dataSource={listAvatar || []}
                scroll={{ x: 980 }}
                rowKey={(record) => record.MaSoThanhVien}
                className='custom-table'
                bordered
              />
            </div>
          </>
        )}
      </div>

      <div className='mt-6 mr-10 flex items-center justify-end'>
        {step === 1 && (
          <>
            <Button variant='secondary' onClick={handleBack}>
              Quay về
            </Button>
          </>
        )}

        <>
          {step === 2 && !isLoadingEnable && (
            <>
              <Button variant='secondary' onClick={handleBackCurrent}>
                Quay về
              </Button>
              {listAvatar?.length > 0 && (
                <Button variant='default' className='ml-2' onClick={handleSubmit}>
                  Tiếp tục
                </Button>
              )}
            </>
          )}

          {step === 3 && !isLoadingUploadSave && (
            <Button variant='default' className='ml-2' onClick={handleBack}>
              Hoàn tất
            </Button>
          )}
        </>
      </div>
      <Loading open={isLoadingEnable || isLoadingUploadSave}></Loading>
    </div>
  );
};

export default AddTeacherAvatarPage;
