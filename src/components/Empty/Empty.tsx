import { EmptyIcon } from 'assets';

const Empty = ({ label }: { label?: string }) => {
  return (
    <div className='flex flex-col items-center justify-center gap-2'>
      <EmptyIcon />

      <p className='font-semibold'>{label || 'Không có dữ liệu'} </p>
    </div>
  );
};

export default Empty;
