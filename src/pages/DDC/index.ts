export { default as DDCPage } from './DCCPage';
export * from './AddDDC';
export * from './AddDDCByExcel';
export * from './DDCManagement';
