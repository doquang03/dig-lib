import { useEffect, useState } from 'react';

const useDebounce = (deps?: string, milisecond: number = 500) => {
  const [isExcuted, setIsExcuted] = useState<boolean>(false);

  useEffect(() => {
    if (!deps) return;
    const _timer = setTimeout(() => {
      setIsExcuted(true);
    }, milisecond);
    return () => {
      clearTimeout(_timer);
    };
  }, [deps]);
  return { isExcuted, setIsExcuted };
};

export default useDebounce;
