import { Outlet } from 'react-router-dom';

const Expense = () => <Outlet />;

export default Expense;
