import { Modal, ModalProps } from 'antd';
import { Button } from 'components';

type Props = {
  books: Book[];
  failures: FailureMessage[];
} & ModalProps;

const FailurePublishingModelModal = (props: Props) => {
  const { books, failures, onCancel, ...rest } = props;
  return (
    <Modal centered footer={null} closable={false} {...rest} width={700}>
      <h1 className='my-3 text-[20px] text-primary-10'>Danh sách tài liệu không thể chuyển đổi</h1>

      <div className='my-2 max-h-[40rem] w-full overflow-x-auto overflow-y-auto'>
        <table width={'100%'} className='table-striped table-bordered table w-full whitespace-normal'>
          <thead>
            <tr>
              <th>STT</th>
              <th>Sách</th>
              <th>Lý do</th>
            </tr>
          </thead>

          <tbody>
            {books.map((book, index) => (
              <tr key={book.Id}>
                <td>{index + 1}</td>
                <td>
                  {book.MaKiemSoat} - {book.TenSach}
                </td>
                <td>{failures.find((_) => _.Id === book.Id)?.Message}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>

      <div className='grid place-items-center'>
        <Button onClick={onCancel} className=''>
          Đóng
        </Button>
      </div>
    </Modal>
  );
};

export default FailurePublishingModelModal;
