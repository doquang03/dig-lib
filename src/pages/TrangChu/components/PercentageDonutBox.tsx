import { memo } from 'react';
import { Doughnut } from 'react-chartjs-2';

type Props = {
  title: string;
  data: {
    labels: string[];
    datasets: {
      data: number[];
      backgroundColor: string[];
    }[];
  };
  position?: 'center' | 'left' | 'top' | 'right' | 'bottom' | 'chartArea';
  totalValue: number;
  dataAhaha: { haha: { [key: string]: number } };
};

function PercentageDonutBox(props: Props) {
  const { title, data, position = 'right', totalValue, dataAhaha } = props;

  return (
    <div className='w-full rounded-xl bg-white p-2 shadow-sm'>
      <h3 className='text-lg font-bold uppercase text-primary-10'>{title}</h3>

      <div className='flex h-[213px] items-center justify-center'>
        <Doughnut
          key={totalValue.toString()}
          data={data}
          plugins={[
            {
              id: 'textCenter',
              beforeDatasetsDraw(chart: any, args: any, pluginOptions: any) {
                const { ctx, data } = chart;

                ctx.save();
                ctx.font = 'bolder 16px sans-serif';
                ctx.fillStyle = 'black';
                ctx.textAlign = 'center';
                ctx.textBaseline = 'middle';
                ctx.fillText(
                  `${totalValue || '--'}`,
                  chart.getDatasetMeta(0).data[0].x,
                  chart.getDatasetMeta(0).data[0].y
                );
              }
            }
          ]}
          options={{
            plugins: {
              legend: {
                display: true,
                position: position,
                labels: {
                  pointStyle: 'circle',
                  borderRadius: 50,
                  useBorderRadius: true,
                  usePointStyle: true,
                  textAlign: 'center'
                }
              },
              tooltip: {
                callbacks: {
                  label: function (context) {
                    let label = context.dataset.label || '';

                    if (label) {
                      label += ': ';
                    }

                    return (label += dataAhaha.haha[context.label]);
                  }
                }
              },
              datalabels: {
                formatter: (value) => {
                  return value > 9 ? value + '%' : null;
                },
                color: 'white'
              }
            },
            maintainAspectRatio: false,
            responsive: true
          }}
        />
      </div>
    </div>
  );
}

export default memo(PercentageDonutBox);
