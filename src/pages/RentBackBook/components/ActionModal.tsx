import { Modal, ModalProps } from 'antd';
import { Button } from 'components';
import { ReactNode } from 'react';

type Props = {
  children: ReactNode;
  labelSubmit?: string;
  title: string;
  onSubmit: (e?: React.BaseSyntheticEvent<object, any, any> | undefined) => Promise<void>;
  onBack: VoidFunction;
  loading: boolean;
} & ModalProps;

const ActionModal = (props: Props) => {
  const { open, children, labelSubmit = 'Cập nhật', title, onBack, onSubmit, loading } = props;
  return (
    <Modal
      open={open}
      title={<h3 className='border-b-2 pb-2 font-bold uppercase text-primary-10'>{title}</h3>}
      closable={false}
      footer={null}
      width={400}
    >
      {children}
      <div className='mt-3 flex items-center justify-end gap-2'>
        <Button variant='secondary' onClick={onBack} disabled={loading} loading={loading}>
          Quay về
        </Button>

        <Button
          className='bg-tertiary-40 hover:bg-tertiary-40/75'
          onClick={onSubmit}
          disabled={loading}
          loading={loading}
        >
          <span className='text-tertiary-40/100'>{labelSubmit}</span>
        </Button>
      </div>
    </Modal>
  );
};

export default ActionModal;
