import { CloseCircleFilled } from '@ant-design/icons';
import { Button, Modal, type ModalProps } from 'antd';
import { ReactNode } from 'react';
import 'css/ComponentAntD.css';

type Props = {
  handleOk: () => void;
  children?: ReactNode;
} & ModalProps;

function ModalAlert(props: Props) {
  const { handleOk, title, children, ...rest } = props;

  return (
    <Modal
      className='model-delete'
      {...rest}
      footer={[
        <Button className='blue-button' onClick={handleOk}>
          OK
        </Button>
      ]}
    >
      <CloseCircleFilled className='alert-icon'></CloseCircleFilled>
      <div className='title-h2'>{title}</div>
      {children}
    </Modal>
  );
}

export default ModalAlert;
