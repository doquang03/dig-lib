import httpAuth from 'utils/httpAuth';

const endPoint = 'api/v1/Profile';
const endPointDVCT = 'api/v1/DonViCongTac';

const authApis = {
  getProfile: () => httpAuth.get<ResponseApi<User>>(`/${endPoint}`),
  DonViCongTac: (Id: string) => httpAuth.get(`/${endPointDVCT}/${Id}`),
  GetListDonViChild: (Id: string) => httpAuth.get(`/${endPointDVCT}/${Id}/GetListDonViChild`),
  ListChildActivated: (Id: string) => httpAuth.get(`/${endPointDVCT}/ListChildActivated/${Id}/PMQLTVS`)
};

export default authApis;
