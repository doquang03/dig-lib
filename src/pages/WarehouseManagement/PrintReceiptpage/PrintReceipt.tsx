import { useMutation } from '@tanstack/react-query';
import { historyApis, receiptApis } from 'apis';
import { Button, Input, Loading, Title } from 'components';
import { path } from 'constants/path';
import { useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { download } from 'utils/utils';

const PrintReceipt = () => {
  const [mau, setMau] = useState<number>(1);
  const { state } = useLocation();
  const receiptId = state ? state : undefined;
  const navigate = useNavigate();

  const { mutate } = useMutation({
    mutationFn: historyApis.logger
  });

  const { mutate: downloadFile, isLoading: loadingFile } = useMutation({
    mutationFn: () => receiptApis.PrintSo(receiptId, 'Mau' + mau),
    onSuccess: (data) => {
      toast.success('Tải biên bản nhập sách vào kho thư viện thành công');
      download('BienBanNhapSach.doc', data.data);
    }
  });

  const hanldeOnChangeMau = (mau: number) => () => setMau(mau);

  const handleBack = () => navigate(path.quanlynhapkho);

  return (
    <div className='p-5'>
      <Title title='IN PHIẾU NHẬP KHO' />

      <p className='font-bold text-primary-10'>Chọn mẫu in phiếu nhập kho</p>

      <h3 className='font-bold text-primary-10'>Danh sách sách cá biệt nhập kho</h3>

      <div className='grid grid-cols-12 items-center'>
        <button className='col-span-12 md:col-span-6' onClick={hanldeOnChangeMau(1)}>
          <div className='flex items-center gap-2'>
            <Input type='radio' checked={mau === 1} onChange={hanldeOnChangeMau(1)} className='' />
            <label>Mẫu 1</label>
          </div>

          <img src='/content/MauInNK1.png' alt='Mẫu nhập kho 1' />
        </button>

        <button className='col-span-12 md:col-span-6' onClick={hanldeOnChangeMau(2)}>
          <div className='flex items-center gap-2'>
            <Input type='radio' checked={mau === 2} onChange={hanldeOnChangeMau(2)} className='' />
            <label>Mẫu 2</label>
          </div>

          <img src='/content/MauInNK2.png' alt='Mẫu nhập kho 2' />
        </button>
      </div>

      <div className='mt-2 flex items-center justify-end gap-2'>
        <Button variant='secondary' onClick={handleBack}>
          Quay về
        </Button>

        {/* <a href={`/Tempalates/MauNK${mau}.pdf`} target='_blank' rel='noreferrer'>
          <Button
            onClick={() =>
              mutate({
                message: 'In phiếu nhập kho thành công',
                chucNang: 'Kho sách',
                suKien: 'In phiếu nhập kho',
                chiTiet: ''
              })
            }
          >
            In phiếu nhập kho
          </Button>
        </a> */}

        <Button onClick={() => downloadFile()}>Tải phiếu nhập kho</Button>
      </div>
      <Loading open={loadingFile}></Loading>
    </div>
  );
};

export default PrintReceipt;
