import { DownloadOutlined } from '@ant-design/icons';
import { useMutation } from '@tanstack/react-query';
import { statistic } from 'apis';
import { Button, Select, Title } from 'components';
import { MONTH_OPTIONS, YEARS_OPTIONS } from 'constants/options';
import { ReactNode, useState } from 'react';
import { toast } from 'react-toastify';
import NoteRentingBookModal from './component/NoteRentingBookModal';
import { useUser } from 'contexts/user.context';

type submitParam = {
  day?: string;
  month?: string;
  year?: string;
  TinhTrang: 0 | 1 | 2 | 3 | 4 | 5;
  styleUser: '' | 'hs' | 'gv';
  SortNguoiMuon?: boolean;
  SortNgayMuon?: boolean;
  SortToLop?: boolean;
  SortTrangThai?: boolean;
};

const NoteRentingBooktItem = ({
  itemType,
  children,
  onDownload,
  setSearchParams,
  loadingDownloadExcel
}: {
  itemType: string;
  children?: ReactNode;
  onDownload: VoidFunction;
  setSearchParams: React.Dispatch<React.SetStateAction<submitParam>>;
  loadingDownloadExcel: boolean;
}) => {
  const { isAllowedAdjustment } = useUser();

  return (
    <div>
      <h3 className='mt-0 text-primary-10'>{'Sổ mượn sách của ' + (itemType === 'gv' ? 'giáo viên' : 'học sinh')}</h3>
      <div className='flex items-center gap-2'>
        <div className='flex w-1/2 gap-1'>
          <Select
            items={YEARS_OPTIONS}
            className='w-1/3'
            onChange={(e) => {
              setSearchParams((prevState) => {
                return {
                  ...prevState,
                  year: e.target.value
                };
              });
            }}
          />

          <Select
            items={MONTH_OPTIONS}
            className='w-1/3'
            defaultValue={('0' + (new Date().getMonth() + 1)).slice(-2)}
            onChange={(e) => {
              setSearchParams((prevState) => {
                return {
                  ...prevState,
                  month: e.target.value
                };
              });
            }}
          />
          <Select
            items={[
              { label: 'Sắp xếp theo', value: '' },
              { label: 'Ngày mượn sách giảm dần', value: '1' },
              { label: 'Ngày mượn sách tăng dần', value: '11' },
              { label: `Tên ${itemType === 'gv' ? 'giáo viên' : 'học sinh'} giảm dần`, value: '2' },
              { label: `Tên ${itemType === 'gv' ? 'giáo viên' : 'học sinh'} tăng dần`, value: '22' },
              { label: `${itemType === 'gv' ? 'Tổ' : 'Lớp'} giảm dần`, value: '3' },
              { label: `${itemType === 'gv' ? 'Tổ' : 'Lớp'} tăng dần`, value: '33' },
              { label: `Tình trạng mượn giảm dần`, value: '4' },
              { label: `Tình trạng mượn tăng dần`, value: '44' }
            ]}
            onChange={(e) => {
              setSearchParams((prevState) => {
                return {
                  ...prevState,
                  SortNguoiMuon: undefined,
                  SortNgayMuon: undefined,
                  SortToLop: undefined,
                  SortTrangThai: undefined
                };
              });
              let value: boolean | undefined = undefined;
              let name: string = '';
              switch (e.target.value) {
                case '1':
                case '2':
                case '3':
                case '4':
                  value = false;
                  break;
                case '11':
                case '22':
                case '33':
                case '44':
                  value = true;
                  break;
              }
              switch (e.target.value) {
                case '1':
                case '11':
                  name = 'SortNgayMuon';
                  break;
                case '2':
                case '22':
                  name = 'SortNguoiMuon';
                  break;
                case '3':
                case '33':
                  name = 'SortToLop';
                  break;
                case '4':
                case '44':
                  name = 'SortTrangThai';
                  break;
              }
              setSearchParams((prevState) => {
                return {
                  ...prevState,
                  [name]: value
                };
              });
            }}
            className='w-1/3'
            name='sort'
          />
        </div>

        <Button
          loading={loadingDownloadExcel}
          onClick={onDownload}
          disabled={loadingDownloadExcel || !isAllowedAdjustment}
          variant={isAllowedAdjustment ? 'default' : 'disabled'}
        >
          <DownloadOutlined />
          Tải xuống
        </Button>
      </div>
    </div>
  );
};

const NoteRentingBook = () => {
  const [searchParamsGV, setSearchParamsGV] = useState<submitParam>({
    month: (new Date().getMonth() + 1).toString(),
    year: new Date().getFullYear().toString(),
    TinhTrang: 0,
    styleUser: 'gv'
  });
  const [searchParamsHS, setSearchParamsHS] = useState<submitParam>({
    month: (new Date().getMonth() + 1).toString(),
    year: new Date().getFullYear().toString(),
    TinhTrang: 0,
    styleUser: 'hs'
  });
  const [typeAccount, setTypeAccount] = useState<'hs' | 'gv'>('gv');
  const [visible, setVisiable] = useState(false);
  const [isTaiCho, setIsTaiCho] = useState(false);
  const [optionSplit, setOptionSplit] = useState(true);

  const { mutate: downloadExcel, isLoading: loadingDownloadExcel } = useMutation({
    mutationFn: (param: submitParam) => statistic.DownExcelSoMuonSach(param, isTaiCho, optionSplit),
    onSuccess: (data) => {
      toast.success('Tải danh mục mượn sách thành công');
      const blob = new Blob([data.data, { type: 'application/xls' }]);
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = 'DanhMucMuonSach.xls';
      link.click();
      setVisiable(false);
    }
  });

  return (
    <div>
      <Title title='Sổ mượn sách' />

      <div className='mt-3 flex flex-col gap-3'>
        <NoteRentingBooktItem
          itemType='gv'
          setSearchParams={setSearchParamsGV}
          loadingDownloadExcel={false}
          onDownload={() => {
            setTypeAccount('gv');
            setVisiable(true);
          }}
        />

        <NoteRentingBooktItem
          itemType='hs'
          setSearchParams={setSearchParamsHS}
          loadingDownloadExcel={false}
          onDownload={() => {
            setTypeAccount('hs');
            setVisiable(true);
          }}
        />
        <NoteRentingBookModal
          open={visible}
          onBack={() => setVisiable(false)}
          onSubmit={() => {
            if (typeAccount === 'gv') downloadExcel(searchParamsGV);
            if (typeAccount === 'hs') downloadExcel(searchParamsHS);
          }}
          setIsTaiCho={setIsTaiCho}
          setOptionSplit={setOptionSplit}
          typeAccount={typeAccount}
          isLoadingDownLoad={loadingDownloadExcel}
        ></NoteRentingBookModal>
      </div>
    </div>
  );
};

export default NoteRentingBook;
