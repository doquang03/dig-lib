import { CameraFilled } from '@ant-design/icons';
import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import locale from 'antd/es/date-picker/locale/vi_VN';
import { studentApis } from 'apis';
import { AxiosError } from 'axios';
import classNames from 'classnames';
import { Button, DatePicker, Input, Loading, ModalDelete, Select, Title, TitleDelete } from 'components';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import dayjs from 'dayjs';
import { useOptionsFilterData } from 'hooks';
import { useEffect, useMemo, useRef, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router';
import { toast } from 'react-toastify';
import { studentSchema } from 'utils/rules';
import { convertDate } from 'utils/utils';
import * as yup from 'yup';

type StudentForm = yup.InferType<typeof studentSchema>;

const initialStudentValue = {
  Ten: '',
  MaSoThanhVien: '',
  LopHoc: '',
  DiaChi: '',
  ThoiHanThe: '',
  NgaySinh: '',
  GioiTinh: '',
  NienKhoa: '',
  SDT: ''
};

const AddStudentPage = () => {
  const { studentId } = useParams();

  const { isAllowedAdjustment } = useUser();

  const [file, setFile] = useState<File | undefined>();
  const [visible, setVisible] = useState<boolean>(false);

  const fileInutRef = useRef<HTMLInputElement | null>(null);
  const ref = useRef<HTMLDivElement | null>(null);

  const {
    register,
    handleSubmit,
    control,
    setValue,
    reset,
    unregister,
    setError,
    formState: { errors }
  } = useForm<StudentForm>({
    defaultValues: initialStudentValue,
    resolver: yupResolver(studentSchema)
  });

  const navigate = useNavigate();

  const queryClient = useQueryClient();

  const previewImage = useMemo(() => (file ? URL.createObjectURL(file) : ''), [file]);

  const { status, schoolYears, genders } = useOptionsFilterData();

  const {
    data: studentData,
    isLoading: loadingStudent,
    refetch
  } = useQuery({
    queryKey: ['student', studentId],
    queryFn: () => studentApis.getStudent(studentId + ''),
    // studentId === true => excute query function
    enabled: !!studentId,
    onError: () => {
      navigate(path.hocsinh);
    }
  });

  const student = useMemo(() => {
    if (!studentData?.data.Item) {
      return {} as Student;
    }

    return studentData?.data.Item;
  }, [studentData?.data.Item]);

  const { mutate: createStudent, isLoading: creatingUser } = useMutation({
    mutationFn: (payload: FormData) => studentApis.createStudent(payload),
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ['students'] });
      toast.success('Thêm học sinh thành công');
      setFile(undefined);
      reset();
      navigate(path.hocsinh, { preventScrollReset: true });
    }
  });

  const { mutate: updateStudent, isLoading: updatingStudent } = useMutation({
    mutationFn: (payload: FormData) => studentApis.updateStudent(payload),
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ['students'] });
      toast.success('Cập nhật học sinh thành công');
      setValue('Ten', student?.Ten + '');
      refetch();
      setFile(undefined);
    },
    onError: (error: AxiosError<ResponseApi>) => {
      setError('MaSoThanhVien', { message: error.response?.data.Message, type: 'validate' }, { shouldFocus: true });
    }
  });

  const { mutate: deleteStudent, isLoading: deletingStudent } = useMutation({
    mutationFn: () => studentApis.deleteStudent([studentId + '']),
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ['students'] });
      toast.success('Xóa học sinh thành công');
      navigate(path.hocsinh, { preventScrollReset: true });
    }
  });

  const { mutate: recoveryPassword, isLoading: recoveringPassword } = useMutation({
    mutationFn: studentApis.recoveryPassword,
    onSuccess: () => {
      toast.success('Phục hồi mật khẩu thành công');
      refetch();
    }
  });

  const isDisabled =
    creatingUser || (loadingStudent && Boolean(studentId)) || deletingStudent || updatingStudent || recoveringPassword;

  useEffect(() => {
    if (!studentId && student) return;

    const { Ten, MaSoThanhVien, LopHoc, DiaChi, ThoiHanThe, NgaySinh, GioiTinh, NienKhoa, SDT, TrangThai } = student;

    setValue('Ten', Ten);
    setValue('MaSoThanhVien', MaSoThanhVien);
    setValue('LopHoc', LopHoc);
    setValue('DiaChi', DiaChi);
    // @ts-ignore
    setValue('ThoiHanThe', dayjs(ThoiHanThe, 'DD/MM/YYYY'));
    // @ts-ignore
    setValue('NgaySinh', dayjs(NgaySinh, 'DD/MM/YYYY'));
    setValue('GioiTinh', GioiTinh);
    setValue('NienKhoa', NienKhoa);
    setValue('SDT', SDT);
    setValue('TrangThai', TrangThai + '');
  }, [setValue, student, studentId, unregister]);

  useEffect(() => {
    ref.current?.scrollIntoView({ behavior: 'smooth' });
  }, []);

  const handleAccessFileInputRef = () => {
    fileInutRef.current?.click();
  };

  const handleOnFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files?.[0];

    if (!fileFromLocal) {
      return;
    }

    if (fileFromLocal && !fileFromLocal.type.startsWith('image')) {
      return;
    }
    setFile(fileFromLocal);
  };

  const onSubmit = handleSubmit((data) => {
    const formData = new FormData();
    data.MaSoThanhVien = data?.MaSoThanhVien?.toLocaleUpperCase();
    file && formData.append('req.HinhChanDung', file);
    data?.MaSoThanhVien && formData.append(`req.MaSoThanhVien`, data.MaSoThanhVien);
    data?.DiaChi && formData.append(`req.DiaChi`, data.DiaChi);
    data?.TrangThai && formData.append(`req.TrangThai`, data.TrangThai);
    Boolean(studentId) && formData.append('req.Id', studentId + '');

    formData.append(`req.Ten`, data.Ten.replace(/\s\s+/g, ' '));
    formData.append(`req.GioiTinh`, data.GioiTinh);
    formData.append(`req.NgaySinh`, convertDate(data.NgaySinh));
    formData.append(`req.NienKhoa`, data.NienKhoa);
    formData.append(`req.LopHoc`, data.LopHoc);
    formData.append(`req.SDT`, data?.SDT || '');
    formData.append(`req.ThoiHanThe`, convertDate(data.ThoiHanThe));

    if (!Boolean(studentId)) {
      !creatingUser && createStudent(formData);
    } else {
      !updatingStudent && updateStudent(formData);
    }
  });

  const handleRecoveryPassword = () => {
    recoveryPassword(studentId + '');
  };

  const handleBack = () => navigate(-1);

  const handleDelete = () => {
    if (!student.DaTra || student.TrangThai === 1) {
      !student.DaTra && toast.error(`Học sinh ${student.Ten} chưa trả sách`);
      student.TrangThai === 1 && toast.error(`Học sinh ${student.Ten} đang bị khóa`);

      return;
    }

    setVisible(true);
  };

  const handleExportCard = () => {
    navigate(path.xuatthethuvien, {
      state: {
        selectedStudents: [studentId]
      }
    });
  };

  return (
    <div className='p-5' ref={ref}>
      <Title title={!!studentId ? 'Cập nhật học sinh' : 'Thêm mới học sinh'} />

      <form className='container' onSubmit={onSubmit}>
        <div className='mt-10 grid grid-cols-1 md:grid-cols-12'>
          <div className='relative col-span-3 mb-10 flex h-full w-full justify-center overflow-hidden'>
            <input
              name='HinhChanDung'
              type='file'
              accept='image/jpeg,image/gif,image/jpg,image/png,image/bmp'
              className='hidden'
              ref={fileInutRef}
              onChange={handleOnFileChange}
              disabled={isDisabled || !isAllowedAdjustment}
            />

            <div className='group relative h-44 w-44'>
              <div className='my-5 h-44 w-44'>
                <img
                  src={previewImage || studentData?.data?.Item?.LinkAvatar || '/content/default-avatar.jpeg'}
                  alt='Hình đại diện'
                  className='h-full w-full rounded-full object-cover shadow-md transition-all duration-200 ease-in-out group-hover:blur-[2px]'
                />
              </div>

              <button
                type='button'
                onClick={handleAccessFileInputRef}
                disabled={!isAllowedAdjustment}
                className='absolute inset-0 my-5 flex  h-44 w-44 scale-110 items-center justify-center rounded-full p-5 opacity-0 transition-all duration-200 ease-out group-hover:scale-100 group-hover:bg-black/40 group-hover:opacity-100'
              >
                <span className='mr-2 text-white'>Chọn hình ảnh</span> <CameraFilled style={{ color: 'white' }} />
              </button>
            </div>
          </div>

          <div className='col-span-4 px-2'>
            <div className='my-2'>
              <label className='mb-3 font-bold'>
                Tên học sinh <span className='text-danger-10'>*</span>
              </label>

              <Input
                placeholder='Nhập tên học sinh'
                name='Ten'
                register={register}
                errorMessage={errors?.Ten?.message}
                disabled={isDisabled || !isAllowedAdjustment}
                maxLength={256}
              />
            </div>

            <div className='my-3'>
              <label className='mb-2.5 font-bold'>Mã học sinh</label>

              <Input
                placeholder='Nhập mã học sinh'
                name='MaSoThanhVien'
                register={register}
                errorMessage={errors?.MaSoThanhVien?.message}
                disabled={isDisabled || !isAllowedAdjustment}
                maxLength={51}
              />
              <p className='mt-1 italic text-primary-10'>(Nếu không nhập, mã học sinh sẽ được tạo tự động)</p>
            </div>

            <div className='my-2'>
              <label className='mb-2.5 font-bold'>
                Lớp học <span className='text-danger-10'>*</span>
              </label>

              <Input
                placeholder='Nhập lớp học'
                name='LopHoc'
                register={register}
                errorMessage={errors?.LopHoc?.message}
                disabled={isDisabled || !isAllowedAdjustment}
                maxLength={31}
              />
            </div>

            <div className='my-2'>
              <label className='mb-3 font-bold'>Địa chỉ</label>

              <Input
                placeholder='Nhập địa chỉ'
                name='DiaChi'
                errorMessage={errors?.DiaChi?.message}
                register={register}
                disabled={isDisabled || !isAllowedAdjustment}
                maxLength={2001}
              />
            </div>

            <div className='my-2'>
              <label className='mb-3 font-bold'>
                Thời hạn thẻ <span className='text-danger-10'>*</span>
              </label>

              <DatePicker
                control={control}
                name='ThoiHanThe'
                disabled={isDisabled || !isAllowedAdjustment}
                disabledDate={(current) => {
                  return current && current < dayjs().startOf('day');
                }}
                placeholder='Chọn thời hạn thẻ'
                errorMessage={errors?.ThoiHanThe?.message}
                locale={locale}
              />
            </div>
          </div>

          <div className='col-span-5 px-2'>
            <div className='my-2'>
              <label className='mb-3 font-bold'>
                Ngày sinh <span className='text-danger-10'>*</span>
              </label>

              <DatePicker
                control={control}
                name='NgaySinh'
                disabled={isDisabled || !isAllowedAdjustment}
                disabledDate={(current) => {
                  return current && current > dayjs().endOf('day');
                }}
                placeholder='Chọn ngày sinh'
                errorMessage={errors?.NgaySinh?.message}
                locale={locale}
              />
            </div>

            <div className='my-3'>
              <label className='mb-2 font-bold'>
                Giới tính <span className='text-danger-10'>*</span>
              </label>

              <Select
                items={genders || []}
                className='w-full'
                name='GioiTinh'
                register={register}
                errorMessage={errors?.GioiTinh?.message}
                disabled={isDisabled || !isAllowedAdjustment}
              />
            </div>

            <div
              className={classNames('', {
                'mt-[15px]': errors?.GioiTinh?.message,
                'mt-[37px]': !errors?.GioiTinh?.message
              })}
            >
              <label className='mb-2 font-bold'>
                Năm học <span className='text-danger-10'>*</span>
              </label>

              <Select
                items={schoolYears || []}
                className='w-full'
                name='NienKhoa'
                register={register}
                errorMessage={errors?.NienKhoa?.message}
                disabled={isDisabled || !isAllowedAdjustment}
              />
            </div>

            <div className='mt-3'>
              <label className='mb-2 font-bold'>Số điện thoại</label>

              <Input
                placeholder='Nhập số điện thoại'
                name='SDT'
                register={register}
                onKeyPress={(event) => {
                  if (!/[0-9]/.test(event.key)) {
                    event.preventDefault();
                  }
                }}
                errorMessage={errors?.SDT?.message}
                disabled={isDisabled || !isAllowedAdjustment}
                maxLength={10}
              />
            </div>

            {!!studentId && (
              <div className='my-3'>
                <div className='my-2'>
                  <label className='mb-2 font-bold'>
                    Tình trạng <span className='text-danger-10'>*</span>
                  </label>

                  <Select
                    items={status || []}
                    name='TrangThai'
                    register={register}
                    className='w-full'
                    disabled={isDisabled || !isAllowedAdjustment}
                    errorMessage={errors?.TrangThai?.message}
                  />
                </div>
              </div>
            )}
          </div>
        </div>
        <div className='mt-10 flex flex-col justify-end gap-2 md:flex-row'>
          <Button
            type='button'
            variant='secondary'
            className='font-semibold'
            onClick={handleBack}
            disabled={isDisabled}
          >
            Quay về
          </Button>

          {!!studentId ? (
            <>
              <Button
                variant='danger'
                type='button'
                className={classNames('', {
                  'font-semibold': isAllowedAdjustment,
                  hidden: !isAllowedAdjustment
                })}
                onClick={handleDelete}
                disabled={isDisabled || !isAllowedAdjustment}
              >
                Xóa học sinh
              </Button>

              <Button
                variant='default'
                type='button'
                className={classNames('', {
                  'font-semibold': isAllowedAdjustment,
                  hidden: !isAllowedAdjustment
                })}
                onClick={handleRecoveryPassword}
                disabled={isDisabled || !isAllowedAdjustment}
              >
                Phục hồi mật khẩu
              </Button>

              <Button
                type='button'
                variant='default'
                className={classNames('', {
                  'font-semibold': isAllowedAdjustment,
                  hidden: !isAllowedAdjustment
                })}
                disabled={isDisabled || !isAllowedAdjustment}
                onClick={handleExportCard}
              >
                In thẻ thư viện
              </Button>

              <Button
                type='button'
                onClick={onSubmit}
                variant={isDisabled ? 'disabled' : 'default'}
                className={classNames('', {
                  'font-semibold': isAllowedAdjustment,
                  hidden: !isAllowedAdjustment
                })}
                disabled={isDisabled || !isAllowedAdjustment}
              >
                Cập nhật
              </Button>
            </>
          ) : (
            <Button
              type='button'
              onClick={onSubmit}
              variant={isDisabled ? 'disabled' : 'default'}
              className={classNames('', {
                'font-semibold': isAllowedAdjustment,
                hidden: !isAllowedAdjustment
              })}
              disabled={isDisabled || !isAllowedAdjustment}
            >
              Thêm mới
            </Button>
          )}
        </div>
      </form>

      <ModalDelete
        open={visible}
        closable={false}
        title={<TitleDelete firstText={'Bạn có chắn chắn muốn xóa học sinh'} secondText={student?.Ten} />}
        handleCancel={() => setVisible(false)}
        handleOk={deleteStudent}
        loading={deletingStudent}
      />

      <Loading open={creatingUser} />
    </div>
  );
};

export default AddStudentPage;
