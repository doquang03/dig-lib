import React from 'react';

const RightArrow = () => (
  <svg width={45} height={32} viewBox='0 0 45 32' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <g filter='url(#filter0_d_7818_11658)'>
      <path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M22.5 23L22.5 17.1342L5 17.1342L5 6.86581L22.5 6.86581L22.5 0.999999L40 12L22.5 23Z'
        fill='#1E88B6'
        fillOpacity='0.19'
        shapeRendering='crispEdges'
      />
      <path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M22.5 23L22.5 17.1342L5 17.1342L5 6.86581L22.5 6.86581L22.5 0.999999L40 12L22.5 23Z'
        stroke='#184785'
        strokeWidth={2}
        strokeLinecap='round'
        strokeLinejoin='round'
        shapeRendering='crispEdges'
      />
    </g>
    <defs>
      <filter
        id='filter0_d_7818_11658'
        x={0}
        y={0}
        width={45}
        height={32}
        filterUnits='userSpaceOnUse'
        colorInterpolationFilters='sRGB'
      >
        <feFlood floodOpacity={0} result='BackgroundImageFix' />
        <feColorMatrix
          in='SourceAlpha'
          type='matrix'
          values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0'
          result='hardAlpha'
        />
        <feOffset dy={4} />
        <feGaussianBlur stdDeviation={2} />
        <feComposite in2='hardAlpha' operator='out' />
        <feColorMatrix type='matrix' values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0' />
        <feBlend mode='normal' in2='BackgroundImageFix' result='effect1_dropShadow_7818_11658' />
        <feBlend mode='normal' in='SourceGraphic' in2='effect1_dropShadow_7818_11658' result='shape' />
      </filter>
    </defs>
  </svg>
);

export default RightArrow;
