import { CloseOutlined, PlusOutlined } from '@ant-design/icons';
import { Tooltip } from 'antd';
import classNames from 'classnames';
import { MouseEvent } from 'react';

type Props = {
  tabs: {
    tabName: string;
    user?: Pick<Student, 'Id' | 'LoaiTK'>;
  }[];
  activeTab: number;
  onChange: (tabIndex: number) => void;
  onAddNewTab: VoidFunction;
  onRemove: (tabIndex: number) => void;
};

const Tabs = (props: Props) => {
  const { activeTab, tabs, onAddNewTab, onRemove, onChange } = props;

  const handleRemoveTab = (event: MouseEvent<HTMLDivElement>, index: number) => {
    event.stopPropagation();

    onRemove(index);
  };

  return (
    <div className='mt-3 flex flex-nowrap items-center gap-2'>
      {tabs?.map(
        (
          item: {
            tabName: string;
          },
          index: number
        ) => {
          return (
            <Tooltip title={item.tabName} key={index}>
              <button
                type='button'
                className={classNames('relative flex-shrink truncate text-ellipsis rounded-md py-2 pl-2 pr-7', {
                  'bg-primary-10 text-white shadow-md': activeTab === index,
                  ' bg-secondary-30/25 text-secondary-30': activeTab !== index
                })}
                onClick={() => onChange(index)}
              >
                {item.tabName}

                <div className='absolute right-1 top-2' onClick={(e) => handleRemoveTab(e, index)}>
                  <CloseOutlined />
                </div>
              </button>
            </Tooltip>
          );
        }
      )}

      {tabs.length < 5 && (
        <button onClick={onAddNewTab}>
          <PlusOutlined style={{ fontSize: 20, color: '#3472A2' }} />
        </button>
      )}
    </div>
  );
};

export default Tabs;
