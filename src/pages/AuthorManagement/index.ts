export { default as AuthorManagementPage } from './AuthorManagementPage';
export * from './AddAuthor';
export * from './AuthorManagermentContent';
export * from './AddAuthorByExcel';
export * from './NoteAuthorPage.tsx';
