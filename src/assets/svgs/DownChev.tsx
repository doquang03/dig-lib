import { SVGProps } from 'react';

function DownChev(props: SVGProps<SVGSVGElement>) {
  return (
    <svg width={14} height={7} viewBox='0 0 14 7' fill='none' xmlns='http://www.w3.org/2000/svg' {...props}>
      <path d='M1.645 0L7 4.32659L12.355 0L14 1.33198L7 7L0 1.33198L1.645 0Z' fill='#184785' />
    </svg>
  );
}

export default DownChev;
