import { DeleteFilled, EditOutlined } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { DatePicker, Table, Tooltip } from 'antd';
import locale from 'antd/es/date-picker/locale/vi_VN';
import { ColumnsType } from 'antd/es/table';
import auditApis from 'apis/audit.api';
import classNames from 'classnames';
import { Button, Empty, Input, ModalDelete, SizeChanger, Title } from 'components';
import { FORMAT_DATE_PICKER } from 'constants/config';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import dayjs, { Dayjs } from 'dayjs';
import { debounce } from 'lodash';
import { ChangeEvent, useMemo, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { convertDate, getSerialNumber } from 'utils/utils';

const AuditList = () => {
  const [{ page, pageSize }, setPaging] = useState<{ page: number; pageSize: number }>({ page: 1, pageSize: 30 });
  const [searchParams, setSearchParams] = useState<
    Partial<{
      TenPhieu: string;
      MaPhieu: string;
      NgayVaoSo: string;
      NgayChotSo: string;
    }>
  >();
  const [selectedAudit, setSelectedAudit] = useState<Audit>();

  const { isAllowedAdjustment } = useUser();

  const navigate = useNavigate();

  const {
    data: auditData,
    isLoading: loadingAudit,
    refetch,
    isFetching
  } = useQuery({
    queryKey: ['audit', page, pageSize, searchParams],
    queryFn: () => {
      let obj: Partial<{
        MaPhieuKiemKe: SearchInputField;
        TextForSearch: string;
        NgayVaoSo: SearchRangeField;
        NgayChotSo: SearchRangeField;
        page: number;
        pageSize: number;
      }> = { page, pageSize };

      if (!!searchParams?.MaPhieu) {
        obj.MaPhieuKiemKe = { truongSearch: searchParams?.MaPhieu || '', operatorSearch: 'contains' };
      }

      if (!!searchParams?.TenPhieu) {
        obj.TextForSearch = searchParams?.TenPhieu;
      }

      if (!!searchParams?.NgayVaoSo) {
        obj.NgayVaoSo = {
          Max: dayjs(searchParams?.NgayVaoSo).endOf('day').utc().format(),
          Min: dayjs(searchParams?.NgayVaoSo).startOf('day').utc().format()
        };
      }

      if (!!searchParams?.NgayChotSo) {
        obj.NgayChotSo = {
          Max: dayjs(searchParams?.NgayChotSo).endOf('day').utc().format(),
          Min: dayjs(searchParams?.NgayChotSo).startOf('day').utc().format()
        };
      }

      return auditApis.getAudits(obj);
    },
    onSuccess: () => {
      setSelectedAudit(undefined);
    }
  });

  const { mutate: deleteAudit, isLoading: loadingDeleteAudit } = useMutation({
    mutationFn: auditApis.deleteAudit,
    onSuccess: () => {
      toast.success('Xóa phiếu kiểm kê thành công');
      refetch();
    }
  });

  const { List_SoKiemKe, TotalCount } = useMemo(() => {
    if (!auditData?.data.Item) return { List_SoKiemKe: [], TotalCount: 0 };

    return auditData?.data.Item;
  }, [auditData?.data.Item]);

  const columns: ColumnsType<Audit> = [
    {
      key: 'stt',
      dataIndex: 'stt',
      title: 'STT',
      render: (value, record, index) => getSerialNumber(page, pageSize, index),
      width: 100
    },
    {
      key: 'MaKiemKe',
      dataIndex: 'MaKiemKe',
      title: 'Mã phiếu kiểm kê',
      render: (value, record) => (
        <Link className='text-primary-60/70 underline' to={`CapNhat/${record.Id}`}>
          {record.MaKiemKe}
        </Link>
      ),
      width: 180
    },
    {
      key: 'TenPhieu',
      dataIndex: 'TenPhieu',
      title: 'Tên phiếu kiểm kê',
      render: (value, { TenPhieu }) => (
        <Tooltip title={TenPhieu} arrow={true} className='truncate'>
          <p>{TenPhieu}</p>
        </Tooltip>
      ),
      ellipsis: true,
      onCell: (record) => ({
        className: 'text-left'
      })
    },
    {
      key: 'NgayVaoSo',
      dataIndex: 'NgayVaoSo',
      title: 'Ngày lập phiếu',
      render: (value, { NgayVaoSo }) => convertDate(NgayVaoSo),
      width: 150
    },
    {
      key: 'NgayChotSo',
      dataIndex: 'NgayChotSo',
      title: 'Ngày chốt số liệu',
      render: (value, { NgayChotSo }) => convertDate(NgayChotSo),
      width: 150
    },
    {
      key: 'GhiChu',
      dataIndex: 'GhiChu',
      title: 'Ghi chú',
      render: (value, { GhiChu }) => (
        <Tooltip title={GhiChu} arrow={true} className='truncate'>
          <p>{GhiChu}</p>
        </Tooltip>
      ),
      ellipsis: true,
      onCell: (record) => ({
        className: 'text-left'
      })
    },
    {
      key: 'actions',
      dataIndex: 'actions',
      title: 'Hành động',
      width: 180,
      render: (value, record) => {
        return (
          <>
            <Tooltip title='Cập nhật'>
              <Link to={`CapNhat/${record.Id}`} className='mx-2'>
                <EditOutlined style={{ fontSize: '20px', color: '#08c' }} />
              </Link>
            </Tooltip>

            <Tooltip title='Xóa'>
              <button className='mx-2' onClick={() => setSelectedAudit(record)}>
                <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
              </button>
            </Tooltip>
          </>
        );
      }
    }
  ];

  const handleAddAudit = () => navigate(path.addAudit);

  const handleDeleteAudit = () => deleteAudit(selectedAudit?.Id + '');

  const handleChangeSearch = debounce((e: ChangeEvent<HTMLInputElement>) => {
    if (!e) return;

    const { value, name } = e.target;

    setSearchParams((prevState) => {
      return { ...prevState, [name]: value };
    });
  }, 400);

  const handleChangeDate = (value: Dayjs | null, name: string) => {
    setSearchParams((prevState) => {
      return { ...prevState, [name]: value };
    });
  };

  return (
    <div className='p-5'>
      <Title title='Kiểm kê tài sản' />

      <form className='mt-3 flex flex-1 items-center gap-1 bg-primary-50/20 p-2.5'>
        <Input
          placeholder='Nhập tên phiếu kiểm kê'
          containerClassName='w-1/4'
          name='TenPhieu'
          onChange={handleChangeSearch}
        />

        <Input
          placeholder='Nhập mã phiếu kiểm kê'
          containerClassName='w-1/4'
          name='MaPhieu'
          onChange={handleChangeSearch}
        />

        <DatePicker
          format={FORMAT_DATE_PICKER[0]}
          placeholder='Chọn Ngày lập phiếu'
          className='w-1/4 border border-gray-300 py-[0.8rem] px-2 outline-none'
          locale={locale}
          disabledDate={(current) => {
            return current && current > dayjs().endOf('day');
          }}
          onChange={(value) => handleChangeDate(value, 'NgayVaoSo')}
        />

        <DatePicker
          format={FORMAT_DATE_PICKER[0]}
          placeholder='Chọn ngày chốt số liệu'
          className='w-1/4 border border-gray-300 py-[0.8rem] px-2 outline-none'
          locale={locale}
          onChange={(value) => handleChangeDate(value, 'NgayChotSo')}
        />
      </form>

      <Button onClick={handleAddAudit} className={isAllowedAdjustment ? 'mt-3' : 'hidden'}>
        Tạo phiếu kiểm kê
      </Button>

      <Table
        columns={columns}
        className='custom-table mt-3'
        bordered
        locale={{
          emptyText: () => <Empty />
        }}
        pagination={{
          onChange(current, pageSize) {
            setPaging({ page: current, pageSize: pageSize });
          },
          current: page,
          pageSize: pageSize,
          total: TotalCount,
          hideOnSinglePage: true,
          showSizeChanger: false,
          showQuickJumper: true,
          locale: { jump_to: '', page: '' }
        }}
        dataSource={List_SoKiemKe}
        loading={loadingAudit}
        rowKey={(row) => row.Id}
      />

      <div
        className={classNames('relative', {
          'mt-[64px]': Number(pageSize) >= TotalCount
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger visible={!!TotalCount} value={pageSize + ''} currentPage={page + ''} total={TotalCount + ''} />
        </div>
      </div>

      <ModalDelete
        open={Boolean(selectedAudit)}
        closable={false}
        title={
          <>
            <h3 className='text-bold'>Bạn có muốn chắc chắn xóa phiếu kiểm kê {selectedAudit?.MaKiemKe} này không?</h3>
            <p className='text-[16px] font-extralight leading-normal'>Bạn sẽ không thể khôi phục sau khi xóa!</p>
          </>
        }
        loading={loadingDeleteAudit || isFetching}
        handleCancel={() => {
          setSelectedAudit(undefined);
        }}
        handleOk={handleDeleteAudit}
      />
    </div>
  );
};

export default AuditList;
