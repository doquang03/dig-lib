export { default as TeacherManagementPage } from './TeacherManagementPage';
export * from './AddTeacher';
export * from './TeacherManagermentContent';
export * from './AddTeacherAvatar';
export * from './AddTeacherByExcel';
