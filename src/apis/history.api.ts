import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/LichSuTruyCap`;

const historyApis = {
  Index: (minDate: string, maxDate: string, page: number, sizeNumber: number, Sorter: boolean) =>
    httpThuVien.post(
      `${endPoint}/Index?firstDate=${minDate}&lastDate=${maxDate}&page=${page}&sizeNumber=${sizeNumber}&sorter=${Sorter}`
    ),
  downloadExcelHistory: (minDate: string, maxDate: string) =>
    httpThuVien.post(
      `${endPoint}/DownloadExcel?firstDate=${minDate}&lastDate=${maxDate}`,
      {},
      { responseType: 'arraybuffer' }
    ),
  logger: (payload: { message: string; chucNang: string; suKien: string; chiTiet: string }) => {
    return httpThuVien.post(`${endPoint}/PrintImportLogger`, payload);
  }
};

export default historyApis;
