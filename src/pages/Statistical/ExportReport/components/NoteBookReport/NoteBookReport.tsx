import { DownloadOutlined, EyeFilled, MailFilled } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { bookstoreTypeApis, statistic } from 'apis';
import { Button, Select, Title } from 'components';
import { MONTH_OPTIONS, SCHOOL_YEAR_OPTIONS, YEARS_OPTIONS } from 'constants/options';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import { ChangeEvent, ReactNode, useMemo, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

const NoteBookReportItem = ({
  itemTitle,
  children,
  loading,
  onWatch,
  onDownload,
  onSend
}: {
  itemTitle: string;
  children?: ReactNode;
  loading?: boolean;
  onWatch?: VoidFunction;
  onDownload: VoidFunction;
  onSend?: VoidFunction;
}) => {
  const { isAllowedAdjustment } = useUser();

  return (
    <div>
      <h3 className='mt-0 text-primary-10'>{itemTitle}</h3>
      <div className='flex items-center gap-2'>
        {children}

        {Boolean(onWatch) && (
          <Button onClick={onWatch} loading={loading} disabled={loading}>
            <EyeFilled />
            Xem
          </Button>
        )}

        <Button
          onClick={onDownload}
          loading={loading}
          disabled={loading || !isAllowedAdjustment}
          variant={isAllowedAdjustment ? 'default' : 'disabled'}
        >
          <DownloadOutlined />
          Tải xuống
        </Button>

        {Boolean(onSend) && (
          <Button
            onClick={onSend}
            loading={loading}
            disabled={loading || !isAllowedAdjustment}
            variant={isAllowedAdjustment ? 'default' : 'disabled'}
          >
            <MailFilled />
            Gửi
          </Button>
        )}
      </div>
    </div>
  );
};

const yearsValue = SCHOOL_YEAR_OPTIONS[0].value.split('-');

const NoteBookReport = () => {
  const [shoolYear, setShoolyear] = useState<number[]>([+yearsValue[0], +yearsValue[1]]);
  const [month, setMonth] = useState<number>();
  const [year, setYear] = useState<number>();
  const [idKho, setIdKho] = useState('');
  const [monthSGK, setMonthSGK] = useState<number>();
  const [yearSGK, setYearSGK] = useState<number>();
  const [idKhoSGK, setIdKhoSGK] = useState('');
  const [sorter, setSorter] = useState('0');
  const [idKhoMucLuc, setIdKhoMucLuc] = useState('');
  const navigate = useNavigate();

  const { data: storesData, isLoading: loadingStores } = useQuery({
    queryKey: ['stores'],
    queryFn: bookstoreTypeApis.getAllStores,
    staleTime: Infinity
  });

  const storesResult = useMemo(() => {
    if (!storesData?.data.Item) return [];

    const storeResults = storesData?.data.Item.filter(
      (element) => element?.CachTangMaCB && +element?.CachTangMaCB !== 2
    ).map(({ Ten, Id }) => {
      return {
        label: Ten,
        value: Id
      };
    });

    return storeResults;
  }, [storesData?.data.Item]);

  const storesResultAll = useMemo(() => {
    if (!storesData?.data.Item) return [];

    const storeResults = storesData?.data.Item.map(({ Ten, Id }) => {
      return {
        label: Ten,
        value: Id
      };
    });

    return storeResults;
  }, [storesData?.data.Item]);

  const storesResultSGK = useMemo(() => {
    if (!storesData?.data.Item) return [];

    const storeResults = storesData?.data.Item.filter(
      (element) => element?.CachTangMaCB && +element?.CachTangMaCB === 2
    ).map(({ Ten, Id }) => {
      return {
        label: Ten,
        value: Id
      };
    });

    return storeResults;
  }, [storesData?.data.Item]);

  const { mutate: downloadSoTongQuat, isLoading: loadingDownloadSoTongQuat } = useMutation({
    mutationFn: () =>
      statistic.downloadSoDangKyTongQuatKho({
        YearFrom: shoolYear[0],
        YearTo: shoolYear[1],
        Down: true
      }),
    onSuccess: (data) => {
      toast.success('Tải sổ tổng quát thành công');

      const blob = new Blob([data.data, { type: 'application/xls' }]);
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = 'SoDKTQ_Kho.xls';
      link.click();
    }
  });

  const { mutate: DownSoDangKyCaBiet_Kho, isLoading: loadingDownSoDangKyCaBiet_Kho } = useMutation({
    mutationFn: () => {
      let param: { Month?: string; Year?: string; IdKhoSach: string } = {
        Month: month + '',
        Year: year + '',
        IdKhoSach: idKho
      };
      if (!month) delete param.Month;
      if (!year) delete param.Year;
      return statistic.DownSoDangKyCaBiet_Kho(param);
    },
    onSuccess: (data) => {
      toast.success('Tải sổ đăng ký cá biệt thành công');
      const blob = new Blob([data.data, { type: 'application/xls' }]);
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = 'SoDKCB_Kho.xls';
      link.click();
    }
  });

  const { mutate: DownThuMucSachKho, isLoading: loadingDownThuMucSachKho } = useMutation({
    mutationFn: () =>
      statistic.ThuMucSachKho({
        Sort: sorter,
        idKho: idKhoMucLuc
      }),
    onSuccess: (data) => {
      toast.success('Tải sổ đăng ký sách giáo kho thành công');
      const blob = new Blob([data.data, { type: 'application/xls' }]);
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = 'MucLucThuVien.xls';
      link.click();
    }
  });

  const { mutate: DownSoDangKyCaBietSGK_Kho, isLoading: loadingDownSoDangKyCaBietSGK_Kho } = useMutation({
    mutationFn: () => {
      let param: { month?: string; year?: string; IdKho: string } = {
        month: monthSGK + '',
        year: yearSGK + '',
        IdKho: idKhoSGK
      };
      if (!monthSGK) delete param.month;
      if (!yearSGK) delete param.year;
      return statistic.DownSoDangKyCaBietSGK_Kho(param);
    },
    onSuccess: (data) => {
      toast.success('Tải sổ đăng ký sách giáo kho thành công');
      const blob = new Blob([data.data, { type: 'application/xls' }]);
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = 'SoDKCB_SGK_Kho.xls';
      link.click();
    }
  });

  const handleChangeSchoolYear = (e: ChangeEvent<HTMLSelectElement>) => {
    const { value } = e.target;
    setShoolyear(value.split('-').map((item) => Number(item)));
  };

  const handleWatch = (path: string) => () =>
    navigate(path, {
      state: {
        YearFrom: shoolYear[0],
        YearTo: shoolYear[1]
      }
    });

  const handleWatchSCB = (path: string) => () =>
    navigate(path, {
      state: {
        Month: month,
        Year: year,
        IdKhoSach: idKho
      }
    });

  const handleWatchSGK = (path: string) => () =>
    navigate(path, {
      state: {
        Month: month,
        Year: year,
        IdKhoSach: idKho
      }
    });

  return (
    <div>
      <Title title='Sổ sách báo cáo' />

      <div className='mt-3 flex flex-col gap-3'>
        <NoteBookReportItem
          itemTitle='Sổ tổng quát'
          onWatch={handleWatch(path.soTongQuat)}
          // onSend={() => {}}
          onDownload={downloadSoTongQuat}
          children={<Select items={SCHOOL_YEAR_OPTIONS || []} className='w-1/2' onChange={handleChangeSchoolYear} />}
          loading={loadingDownloadSoTongQuat}
        />

        <NoteBookReportItem
          itemTitle='Sổ đăng ký cá biệt'
          onWatch={handleWatchSCB(path.soDangKyCaBiet)}
          // onSend={() => {}}
          onDownload={DownSoDangKyCaBiet_Kho}
          loading={loadingDownSoDangKyCaBiet_Kho}
          children={
            <div className='flex w-1/2 gap-1'>
              <Select
                items={[{ label: 'Chọn năm', value: '' }, ...YEARS_OPTIONS]}
                className='w-1/3'
                onChange={(e) => {
                  if (e.target.value === '') setYear(undefined);
                  else setYear(+e.target.value);
                }}
              />
              <Select
                items={[{ label: 'Chọn tháng', value: '' }, ...MONTH_OPTIONS]}
                className='w-1/3'
                onChange={(e) => {
                  if (e.target.value === '') setMonth(undefined);
                  else setMonth(+e.target.value);
                }}
              />
              <Select
                items={[{ label: 'Tất cả kho', value: '' }, ...storesResult]}
                className='w-1/3'
                onChange={(e) => {
                  setIdKho(e.target.value);
                }}
                disabled={!storesResult.length || loadingStores}
              />
            </div>
          }
        />

        <NoteBookReportItem
          itemTitle='Sổ đăng ký sách giáo khoa'
          onWatch={handleWatchSGK(path.soDangKySachGiaoKhoa)}
          // onSend={() => {}}
          onDownload={() => {
            DownSoDangKyCaBietSGK_Kho();
          }}
          loading={loadingDownSoDangKyCaBietSGK_Kho}
          children={
            <div className='flex w-1/2 gap-1'>
              <Select
                items={[{ label: 'Chọn năm', value: '' }, ...YEARS_OPTIONS]}
                className='w-1/3'
                onChange={(e) => {
                  if (e.target.value === '') setYearSGK(undefined);
                  else setYearSGK(+e.target.value);
                }}
              />
              <Select
                items={[{ label: 'Chọn tháng', value: '' }, ...MONTH_OPTIONS]}
                className='w-1/3'
                onChange={(e) => {
                  if (e.target.value === '') setMonthSGK(undefined);
                  else setMonthSGK(+e.target.value);
                }}
              />
              <Select
                items={[{ label: 'Tất cả kho', value: '' }, ...storesResultSGK]}
                className='w-1/3'
                onChange={(e) => {
                  setIdKhoSGK(e.target.value);
                }}
                disabled={!storesResult.length || loadingStores}
              />
            </div>
          }
        />

        <NoteBookReportItem
          itemTitle='Mục lục thư viện'
          onDownload={() => {
            DownThuMucSachKho();
          }}
          loading={loadingDownThuMucSachKho}
          children={
            <div className='flex w-1/2 gap-1'>
              <Select
                items={[{ label: 'Tất cả kho', value: '-1' }, ...storesResultAll]}
                className='w-full'
                onChange={(e) => {
                  setIdKhoMucLuc(e.target.value);
                }}
                disabled={!storesResult.length || loadingStores}
              />
              <Select
                items={[
                  { label: 'Sắp xếp theo', value: '0' },
                  { label: `Tên sách giảm dần`, value: '1' },
                  { label: `Tên sách tăng dần`, value: '11' },
                  { label: 'Mã kiểm soát giảm dần', value: '2' },
                  { label: 'Mã kiểm soát tăng dần', value: '22' },
                  { label: `Môn loại giảm dần`, value: '3' },
                  { label: `Môn loại tăng dần`, value: '33' }
                ]}
                className='w-full'
                onChange={(e) => {
                  setSorter(e.target.value);
                }}
              />
            </div>
          }
        />
      </div>
    </div>
  );
};

export default NoteBookReport;
