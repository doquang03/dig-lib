import { Modal, ModalProps } from 'antd';
import { AddColorPage } from 'pages/Color';

type Props = {
  onBack: VoidFunction;
} & ModalProps;

function ColorModal(props: Props) {
  const { title, onBack, ...rest } = props;

  return (
    <Modal
      title={<h3 className='font-bold uppercase text-primary-10'>{title}</h3>}
      {...rest}
      footer={null}
      closable={false}
      width={1000}
    >
      <AddColorPage onBack={onBack} type='modalview' />
    </Modal>
  );
}

export default ColorModal;
