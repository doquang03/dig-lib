import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery } from '@tanstack/react-query';
import { newspaperMagazineApis } from 'apis';
import { AxiosError } from 'axios';
import { Button, Input, Loading, Select, Title } from 'components';
import { path } from 'constants/path';
import { useLanguages } from 'hooks';
import { useMemo } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { newspaperMagazineSchema } from 'utils/rules';

const exceptThisSymbols = ['e', 'E', '+', '-', '.'];

const AddNewspaper = () => {
  const { id } = useParams();

  const {
    register,
    handleSubmit,
    formState: { errors },
    setError,
    setValue,
    unregister
  } = useForm<{
    code: string;
    name: string;
    bilingualName: string;
    releaseTerm: string;
    language: string;
    editorialAgency: string;
    issuingAgency: string;
    paperSize: string;
    pageNumber: string;
    price: string;
  }>({
    resolver: yupResolver(newspaperMagazineSchema('newspaper'))
  });

  const { data: languages } = useLanguages();

  const navigate = useNavigate();

  const { mutate: mutateAddNewspaper, isLoading: loadingAddNewspaper } = useMutation({
    mutationFn: newspaperMagazineApis.createNewspapgerMagazine,
    onSuccess: () => {
      toast.success('Thêm mới báo thành công');
      navigate(path.newspaperMagazineListing);
    },
    onError: (error: AxiosError<ResponseApi<NewspaperMagazine>>) => {
      error?.response?.data.Item.MaBaoTapChi &&
        setError('code', { message: error?.response?.data.Message, type: 'validate' });

      error?.response?.data.Item.Ten && setError('name', { message: error?.response?.data.Message, type: 'validate' });

      error?.response?.data.Item.TenSongNgu &&
        setError('bilingualName', { message: error?.response?.data.Message, type: 'validate' });
    }
  });

  const { mutate: updateNewspaper, isLoading: loadingUpdateNewspaper } = useMutation({
    mutationFn: newspaperMagazineApis.updateNewspapgerMagazine,
    onSuccess: () => {
      toast.success('Cập nhật báo thành công');
    },
    onError: (error: AxiosError<ResponseApi<NewspaperMagazine>>) => {
      error?.response?.data.Item.MaBaoTapChi &&
        setError('code', { message: error?.response?.data.Message, type: 'validate' });

      error?.response?.data.Item.Ten && setError('name', { message: error?.response?.data.Message, type: 'validate' });

      error?.response?.data.Item.TenSongNgu &&
        setError('bilingualName', { message: error?.response?.data.Message, type: 'validate' });
    }
  });

  const { isLoading: loadingNewspaperData } = useQuery({
    queryKey: ['getOneData', id],
    queryFn: () => newspaperMagazineApis.getOneData(id + ''),
    enabled: Boolean(id),
    onSuccess: (data) => {
      const { Ten, MaBaoTapChi, CQBienTap, CQPhatHanh, CoGiay, GiaBia, IdNgonNgu, SoTrang, KyHan, TenSongNgu } =
        data.data.Item;

      setValue('code', MaBaoTapChi);
      setValue('name', Ten);
      setValue('bilingualName', TenSongNgu);
      setValue('releaseTerm', KyHan);
      setValue('language', IdNgonNgu);
      setValue('editorialAgency', CQBienTap);
      setValue('issuingAgency', CQPhatHanh);
      setValue('paperSize', CoGiay);
      setValue('pageNumber', SoTrang.toString());
      setValue('price', GiaBia.toString());
    }
  });

  const languageOptions = useMemo(() => {
    if (!languages.length) return [];

    return languages.map(({ Id, Ten }) => {
      return {
        value: Id,
        label: Ten
      };
    });
  }, [languages]);

  const onSubmit = handleSubmit((data) => {
    let { code, name, bilingualName, editorialAgency, issuingAgency } = data;

    code = code.replace(/ +(?= )/g, '');
    name = name.replace(/ +(?= )/g, '');
    bilingualName = bilingualName.replace(/ +(?= )/g, '');
    editorialAgency = editorialAgency.replace(/ +(?= )/g, '');
    issuingAgency = issuingAgency.replace(/ +(?= )/g, '');

    setValue('code', code);
    setValue('name', name);
    setValue('bilingualName', bilingualName);
    setValue('editorialAgency', editorialAgency);
    setValue('issuingAgency', issuingAgency);

    Boolean(id)
      ? updateNewspaper({
          Id: id + '',
          MaBaoTapChi: code,
          Ten: name,
          TenSongNgu: bilingualName,
          KyHan: data.releaseTerm,
          CQBienTap: editorialAgency,
          CQPhatHanh: issuingAgency,
          CoGiay: data.paperSize,
          SoTrang: +data.pageNumber,
          GiaBia: +data.price || 0,
          ISSN: '',
          IdNgonNgu: data.language,
          isBao: true
        })
      : mutateAddNewspaper({
          MaBaoTapChi: code,
          Ten: data.name,
          TenSongNgu: bilingualName,
          KyHan: data.releaseTerm,
          CQBienTap: editorialAgency,
          CQPhatHanh: issuingAgency,
          CoGiay: data.paperSize,
          SoTrang: +data.pageNumber,
          GiaBia: +data.price || 0,
          ISSN: '',
          IdNgonNgu: data.language,
          isBao: true
        });
  });

  const handleBack = () => navigate(-1);

  return (
    <div>
      <Title title={Boolean(id) ? 'Cập nhật báo' : 'Thêm báo mới'} />

      <form action='' className='mt-3 grid grid-cols-12  gap-12'>
        <div className='col-span-4 flex flex-col gap-4'>
          <div>
            <label className='mb-1 font-semibold'>
              Mã báo <span className='text-danger'>*</span>
            </label>

            <Input placeholder='Nhập mã báo' register={register} name='code' errorMessage={errors.code?.message} />
          </div>

          <div>
            <label className='mb-1 font-semibold'>
              Tên báo <span className='text-danger'>*</span>
            </label>

            <Input placeholder='Nhập tên báo' register={register} name='name' errorMessage={errors.name?.message} />
          </div>

          <div>
            <label className='mb-1 font-semibold'>Tên báo song ngữ</label>

            <Input
              placeholder='Nhập tên báo song ngữ'
              register={register}
              name='bilingualName'
              errorMessage={errors.bilingualName?.message}
            />
          </div>

          <div className='flex flex-grow items-center gap-2'>
            <div className='w-full'>
              <label className='mb-1 font-semibold'>Kỳ hạn phát hành</label>

              <Select
                register={register}
                name='releaseTerm'
                items={[
                  { value: 'Hàng tuần', label: 'Hàng tuần' },
                  { value: 'Hàng tháng', label: 'Hàng tháng' }
                ]}
                className='w-full'
              />
            </div>
            <div className='w-full'>
              <label className='mb-1 font-semibold'>Ngôn ngữ</label>

              <Select
                items={[{ label: 'Chọn ngôn ngữ', value: '' }, ...languageOptions]}
                className='w-full'
                register={register}
                name='language'
              />
            </div>
          </div>
        </div>

        <div className='col-span-4 flex flex-col gap-4'>
          <div>
            <label className='mb-1 font-semibold'>Cơ quan biên tập</label>

            <Input placeholder='Nhập cơ quan biên tập' register={register} name='editorialAgency' />
          </div>

          <div>
            <label className='mb-1 font-semibold'>Cơ quan phát hành</label>

            <Input placeholder='Nhập cơ quan phát hành' register={register} name='issuingAgency' />
          </div>

          <div className='flex flex-grow items-center gap-2'>
            <div className='w-full'>
              <label className='mb-1 font-semibold'>Khổ giấy</label>

              <Input
                placeholder='Nhập khổ giấy'
                type='number'
                containerClassName='pl-2 bg-white w-full border rounded-lg flex mb-1'
                className='mr-2 w-full outline-none'
                right={
                  <div className='rounded-br-md rounded-tr-md bg-secondary-20/30 py-3 px-3'>
                    <span className='text-xs'>cm</span>
                  </div>
                }
                register={register}
                name='paperSize'
              />
            </div>

            <div className='w-full'>
              <label className='mb-1 font-semibold'>Số trang</label>

              <Input
                type='text'
                placeholder='Nhập số trang'
                containerClassName='pl-2 bg-white w-full border rounded-lg flex mb-1'
                className='mr-2 w-full outline-none'
                right={
                  <div className='rounded-br-md rounded-tr-md bg-secondary-20/30 py-3 px-3'>
                    <span className='text-xs'>trang</span>
                  </div>
                }
                register={register}
                name='pageNumber'
                onKeyDown={(e) => {
                  exceptThisSymbols.includes(e.key) && e.preventDefault();
                }}
                onKeyPress={(event) => {
                  if (!/[0-9]/.test(event.key)) {
                    event.preventDefault();
                  }
                }}
                maxLength={3}
              />
            </div>
          </div>

          <div>
            <label className='mb-1 font-semibold'>Giá bìa</label>

            <Input
              type='number'
              placeholder='Nhập giá bìa'
              containerClassName='pl-2 bg-white w-full border rounded-lg flex mb-1'
              className='mr-2 w-full outline-none'
              right={
                <div className='rounded-br-md rounded-tr-md bg-secondary-20/30 py-3 px-3'>
                  <span className='text-xs'>đồng</span>
                </div>
              }
              register={register}
              name='price'
              onKeyDown={(e) => exceptThisSymbols.includes(e.key) && e.preventDefault()}
            />
          </div>
        </div>
      </form>

      <div className='mt-3 mr-3 flex w-2/3 items-center justify-end gap-1'>
        <Button variant='secondary' type='button' onClick={handleBack}>
          Quay về
        </Button>

        <Button type='button' onClick={onSubmit}>
          {Boolean(id) ? 'Cập nhật' : 'Thêm mới'}
        </Button>
      </div>

      <Loading open={loadingAddNewspaper || (loadingNewspaperData && Boolean(id)) || loadingUpdateNewspaper} />
    </div>
  );
};

export default AddNewspaper;
