import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/QuanLyXuatKho`;

export type GetExportVars = Partial<{
  page: string;
  pageSize: string;
  MaPhieuXuat: SearchInputField;
  SoLuong: SearchInputField;
  NguoiTao: SearchInputField;
  GiaTriXuatKho: SearchRangeField;
  ThoiGianTao: SearchRangeField;
}>;

const liquidationApis = {
  TaoPhieuXuatSach: (
    listCaBiet: Array<SachCaBiet & { LinkBiaSach?: string }>,
    IdUserAdmin: string,
    UserName: string,
    GhiChuPhieuXuat: string,
    NgayVaoSo: string
  ) =>
    httpThuVien.post(
      `/${endPoint}/TaoPhieuXuatSach?IdUserAdmin=${IdUserAdmin}&UserName=${UserName}&GhiChuPhieuXuat=${GhiChuPhieuXuat}&NgayVaoSo=${NgayVaoSo}`,
      listCaBiet
    ),
  LayPhieuXuatSach: (payload: GetExportVars) =>
    httpThuVien.post<ResponseApi<{ List_SoXuatKho: ExportItem[]; TotalCount: number }>>(`/${endPoint}/Index`, payload),
  LayPhieuXuatSachById: (id: string, page: string, pageSize: string) =>
    httpThuVien.get<
      ResponseApi<{
        ListChiTietXuatKho: ExportDetail[];
        soXuatKho: ExportItem;
        TotalCount: number;
      }>
    >(`/${endPoint}/ChiTietXuatKho?Id=${id}&page=${page}&pageSize=${pageSize}`),
  DownLoadFile: (id: string) =>
    httpThuVien.get(`/${endPoint}/DownloadExcel/${id}`, {
      responseType: 'blob'
    }),
  deleteExport: (id: string) => httpThuVien.delete(`/${endPoint}/DeletePhieu/${id}`)
};

export default liquidationApis;
