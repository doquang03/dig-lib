import classNames from 'classnames';
import { Button, Input, Title } from 'components';
import { path } from 'constants/path';
import { uniqueId } from 'lodash';
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { removeVietnameseTones } from 'utils/utils';
import { data } from './contants';

let alphabet = [...Array(26).keys()].map((i) => String.fromCharCode(i + 97));

interface DataType {
  key: React.ReactNode;
  name: string;
  code: string;
  children?: DataType[];
}

// @ts-ignore
function filterd(array, fn) {
  // @ts-ignore
  return array.reduce((r, o) => {
    var children = filterd(o.children || [], fn);
    if (fn(o) || children.length) r.push(Object.assign({}, o, children.length && { children }));
    return r;
  }, []);
}

const SearchDay19ByNamePage = () => {
  const [activeCharacter, setActiveCharacter] = useState<string>('a');
  const [filteredData, setFilteredData] = useState<DataType[]>(data);
  const [originalData, setOriginalData] = useState<DataType[]>(data);

  const { register, setValue } = useForm();

  const navigate = useNavigate();

  useEffect(() => {
    const result = data.filter((item) =>
      removeVietnameseTones(item.name.toLocaleLowerCase()).startsWith(activeCharacter)
    );

    setFilteredData(result);
    setOriginalData(result);
  }, [activeCharacter]);

  const handleActiveCharacter = (character: string) => () => {
    setActiveCharacter(character);
    setValue('TextForSearch', '');
  };

  const search = (e: any) => {
    const currValue = e.target.value;

    const result = data.filter((item) =>
      removeVietnameseTones(item.name.toLocaleLowerCase()).startsWith(activeCharacter)
    );

    if (currValue) {
      const result = filterd(originalData, ({ name }: { name: string }) =>
        removeVietnameseTones(name.toLocaleLowerCase())?.includes(removeVietnameseTones(currValue.toLocaleLowerCase()))
      );

      setFilteredData(result);
    } else {
      setFilteredData(result);
    }
  };

  const hanldeBack = () => navigate(path.day19);

  return (
    <div>
      <Title title='Bảng tra cứu theo tên' />
      <div className='mt-2 flex items-center '>
        {alphabet.map((character) => {
          const isActive = activeCharacter === character;

          return (
            <button onClick={handleActiveCharacter(character)} key={character}>
              <p
                className={classNames('p-4 text-lg font-bold uppercase', {
                  'bg-primary-10 text-white': isActive,
                  'text-primary-10': !isActive
                })}
              >
                {character}
              </p>
            </button>
          );
        })}
      </div>
      <Button onClick={hanldeBack} className='mt-3'>
        Quay về
      </Button>

      <form className='my-5 flex flex-col flex-wrap gap-2 bg-secondary-10 py-3 px-3.5 md:flex-row'>
        <Input
          placeholder='Nhập tên 19 dãy cần tìm'
          containerClassName='w-[100%] md:w-[unset] md:grow'
          name='TextForSearch'
          register={register}
          type='text'
          onKeyUp={search}
        />
      </form>
      <div className='mt-4'>
        <table className='table'>
          <thead className='bg-primary-10 text-white'>
            <tr>
              <th className='rounded-tl-md text-center'>STT</th>
              <th className='text-center'>Tên</th>
              <th className='text-center'>Tên Con</th>
              <th className='rounded-tr-md text-center'>Mã</th>
            </tr>
          </thead>

          {filteredData.map((item, index) => {
            return (
              <tbody key={uniqueId()}>
                <tr key={uniqueId()}>
                  <td className='text-center'>{item.name && index + 1}</td>
                  <td className='text-center'>{item.name}</td>
                  <td className='text-center'></td>
                  <td className='text-center'>{item.code}</td>
                </tr>

                {item?.children?.length &&
                  item?.children.map((x) => {
                    return (
                      <tr key={x.name}>
                        <td className='text-center'></td>
                        <td className='text-center'></td>
                        <td className='text-center'>{x.name}</td>
                        <td className='text-center'>{x.code}</td>
                      </tr>
                    );
                  })}
              </tbody>
            );
          })}
        </table>
      </div>
    </div>
  );
};

export default SearchDay19ByNamePage;
