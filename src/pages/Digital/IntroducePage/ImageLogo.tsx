import { CloseSquareFilled } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { settingApis } from 'apis';
import { UploadIcon } from 'assets';
import { Button, Loading } from 'components';
import { useRef, useState } from 'react';
import { toast } from 'react-toastify';

type Props = {
  activeTab: string;
};
const ImageLogo = (props: Props) => {
  const fileInutRef = useRef<HTMLInputElement | null>(null);
  const fileInutRefData = useRef<HTMLInputElement | null>(null);
  const [logoFile, setLogoFile] = useState<File | null | undefined>(undefined);
  const [imagesLogo, setImagesLogo] = useState<Array<string>>([]);
  const [imagesData, setImagesData] = useState<Array<string | File>>([]);

  const { isLoading: isLoadingLogo } = useQuery({
    queryKey: ['GetImageLogo'],
    queryFn: () => settingApis.GetImageLogo(),
    onSuccess(data) {
      if (data.data?.Item?.src !== '') setImagesLogo([data.data?.Item?.src]);
    }
  });

  const { mutate: mutateLogo, isLoading: isLoadingSubmitLogo } = useMutation({
    mutationFn: () => {
      let bodyFormData = new FormData();
      bodyFormData.append('file', logoFile as Blob);
      return settingApis.UploadImageLogo(bodyFormData);
    },
    mutationKey: ['UploadImageLogo'],
    onSuccess(data) {
      toast.success('Upload hình ảnh logo thành công');
      setLogoFile(undefined);
      if (data.data?.Item?.src !== '') setImagesLogo([data.data?.Item?.src]);
    }
  });

  const { refetch, isLoading: isLoadingPageImageData } = useQuery({
    queryKey: ['pageImageData'],
    queryFn: () => settingApis.GetImageData(),
    onSuccess: (data) => {
      setImagesData(data.data.Item.ListImage);
    }
  });

  const { mutate: uploadImage, isLoading: isLoadingUploadImages } = useMutation({
    mutationFn: settingApis.uploadImagesFile,
    onSuccess: (data) => {
      toast.success('Cập nhật thành công banner');
      refetch();
    }
  });
  const handleOnFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files;

    if (!fileFromLocal) {
      return;
    }
    setLogoFile(fileFromLocal[0]);
    setImagesLogo([URL.createObjectURL(fileFromLocal[0])]);
  };

  const handleOnFileChangeData = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files;

    if (!fileFromLocal) {
      return;
    }
    setImagesData((prev) => [...prev, ...fileFromLocal]);
  };

  const handleDeleteFile = (index: number) => () => {
    const array = [...imagesLogo];

    array.splice(index, 1);
    setImagesLogo(array);
    setLogoFile(null);
  };

  const handleDeleteFileData = (index: number) => () => {
    const array = [...imagesData];

    array.splice(index, 1);
    setImagesData(array);
  };

  const handleAccessFileInputRef = () => {
    fileInutRef.current?.click();
  };

  const handleAccessFileInputRefData = () => {
    fileInutRefData.current?.click();
  };

  const submitImage = () => {
    if (logoFile !== undefined) {
      mutateLogo();
    }
    const formImageData = new FormData();
    let count = 0;
    for (let i = 0; i < imagesData.length; i++) {
      if (typeof imagesData[i] === 'string') {
        formImageData.append('listLink', imagesData[i]);
        // why didn't use index?
        formImageData.append('stt', count++ + '');
      } else {
        formImageData.append('listFile', imagesData[i]);
      }
    }
    uploadImage(formImageData);
  };

  return (
    <div className='flex flex-col pt-5'>
      <label className='text-base font-bold'>Logo thư viện:</label>
      <div className='flex gap-2'>
        <Button type='button' variant={'default'} className='mt-4' onClick={handleAccessFileInputRef}>
          <UploadIcon />
          <span className='ml-2 text-sm'>Chọn tệp</span>
        </Button>

        <input
          type='file'
          accept={'image/jpeg,image/gif,image/jpg,image/png,image/bmp'}
          className='hidden'
          ref={fileInutRef}
          onChange={handleOnFileChange}
          multiple={false}
          // @ts-ignore
          onClick={(e: React.MouseEvent<HTMLInputElement, MouseEvent>) => (e.target.value = null)}
        />
      </div>
      <div className='mt-5 flex'>
        {!!imagesLogo.length && (
          <>
            {imagesLogo?.reverse().map((image, index) => {
              return (
                <div className='relative col-span-6 h-[170px] md:col-span-4 lg:col-span-2' key={image}>
                  <button className='absolute right-0 -top-1 shadow-lg' type='button' onClick={handleDeleteFile(index)}>
                    <CloseSquareFilled style={{ color: 'red' }} />
                  </button>

                  <a href={image} target='_blank' rel='noreferrer'>
                    <img src={image} alt={image} className='h-full shadow-md' />
                  </a>
                </div>
              );
            })}
          </>
        )}
      </div>
      <label className='mt-2.5 text-base font-bold'>Hình ảnh giới thiệu (banner):</label>
      <div className='flex gap-2'>
        <Button type='button' variant={'default'} className='mt-4' onClick={handleAccessFileInputRefData}>
          <UploadIcon />
          <span className='ml-2 text-sm'>Chọn tệp</span>
        </Button>

        <input
          type='file'
          accept={'image/jpeg,image/gif,image/jpg,image/png,image/bmp'}
          className='hidden'
          ref={fileInutRefData}
          onChange={handleOnFileChangeData}
          multiple={true}
          // @ts-ignore
          onClick={(e: React.MouseEvent<HTMLInputElement, MouseEvent>) => (e.target.value = null)}
        />
      </div>
      <div className='mt-5 grid grid-cols-12 gap-3'>
        {!!imagesData.length && (
          <>
            {imagesData?.reverse().map((image, index) => {
              return (
                <div
                  className='relative col-span-6  md:col-span-4 lg:col-span-2'
                  key={typeof image === 'string' ? image : URL.createObjectURL(image)}
                >
                  <button
                    className='absolute right-0 -top-1 shadow-lg'
                    type='button'
                    onClick={handleDeleteFileData(index)}
                  >
                    <CloseSquareFilled style={{ color: 'red' }} />
                  </button>
                  {typeof image === 'string' ? (
                    <a href={image} target='_blank' rel='noreferrer'>
                      <img src={image} alt={image} className='w-full shadow-md' />
                    </a>
                  ) : (
                    <a href={URL.createObjectURL(image)} target='_blank' rel='noreferrer'>
                      <img
                        src={URL.createObjectURL(image)}
                        alt={URL.createObjectURL(image)}
                        className='w-full shadow-md'
                      />
                    </a>
                  )}
                </div>
              );
            })}
          </>
        )}
      </div>
      <div className='mt-3 flex items-center justify-end gap-2'>
        <Button onClick={submitImage} type='button' disabled={!imagesData.length && !imagesLogo.length}>
          Cập nhật
        </Button>
      </div>
      <Loading open={isLoadingLogo || isLoadingSubmitLogo || isLoadingUploadImages || isLoadingPageImageData} />
    </div>
  );
};

export default ImageLogo;
