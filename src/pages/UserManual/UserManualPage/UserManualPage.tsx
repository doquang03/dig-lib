import { Button, Loading, ModalDelete, Title, TitleDelete } from 'components';
import { useUser } from 'contexts/user.context';
import ModelUpdateManualPage from './ModelUpdateManualPage';
import { useState } from 'react';
import { useMutation, useQuery } from '@tanstack/react-query';
import userManualApis from 'apis/usermanual.api';
import { DeleteFilled, EditOutlined } from '@ant-design/icons';
import { toast } from 'react-toastify';

const UserManualPage = () => {
  const { userType, isAllowedAdjustment } = useUser();
  const [visiable, setVisiable] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);

  const [selectDelete, setSelectDelete] = useState<UserManual | undefined>(undefined);

  const { data, refetch, isLoading, isRefetching } = useQuery({
    queryKey: ['getUserManuals'],
    queryFn: () => userManualApis.getUserManuals(),
    enabled: !(userType === 2001 && !isAllowedAdjustment),
    onSuccess: () => {
      setSelectDelete(undefined);
      setIsModalOpen(false);
    }
  });
  const { mutate: mutateDelete, isLoading: isLoadingDelte } = useMutation({
    mutationFn: userManualApis.removeUserManual,
    onSuccess: () => {
      refetch();
      toast.success('Xóa hướng dẫn sử dụng thành công');
    }
  });
  if (userType === 2001 && !isAllowedAdjustment) {
    return <h2 className='p-2 '>Bạn không có quyền truy cập chức năng này</h2>;
  }

  return (
    <>
      <div className='setting-page p-5'>
        <Title title='Hướng dẫn sử dụng' />
        <Button
          variant={'default'}
          className={isAllowedAdjustment ? 'mt-3 w-[100%] md:w-auto' : 'hidden'}
          onClick={() => {
            setVisiable(true);
          }}
        >
          Tạo hướng dẫn sử dụng
        </Button>
        {data?.data?.Item?.ListModel.map((element, index) => {
          return (
            <div className='mt-4' key={element.Id}>
              <label className='truncate align-middle text-base font-bold' style={{ maxWidth: 'calc(100% - 60px)' }}>
                {index + 1 + '. ' + element.Ten}
              </label>{' '}
              <button
                className='align-middle'
                onClick={() => {
                  setSelectDelete(element);
                  setVisiable(true);
                }}
              >
                <EditOutlined className='ml-2' style={{ fontSize: '20px', color: '#08c' }} />
              </button>
              <button
                className='align-middle'
                onClick={() => {
                  setSelectDelete(element);
                  setIsModalOpen(true);
                }}
              >
                <DeleteFilled className='ml-2' style={{ fontSize: '20px', color: 'red' }} />
              </button>
              <div className='mt-4 flex gap-2'>
                <a
                  href={element.LinkFile}
                  target='_blank'
                  rel='noreferrer'
                  className='ml-6 flex items-center text-[#3472A2] hover:underline hover:underline-offset-4'
                >
                  {element.LinkFile &&
                    element.LinkFile.slice(element.LinkFile.lastIndexOf('/') + 1, element.LinkFile.lastIndexOf('?'))}
                </a>
              </div>
            </div>
          );
        })}
      </div>
      <ModelUpdateManualPage
        open={visiable}
        model={selectDelete}
        onHide={() => {
          setSelectDelete(undefined);
          setVisiable(false);
        }}
        onSuccess={() => {
          refetch();
        }}
      ></ModelUpdateManualPage>

      <ModalDelete
        className='model-delete'
        open={isModalOpen}
        closable={false}
        handleCancel={() => {
          setIsModalOpen(false);
          setSelectDelete(undefined);
        }}
        loading={isLoadingDelte || isRefetching}
        handleOk={() => mutateDelete(selectDelete?.Id as string)}
        title={
          <TitleDelete
            firstText='Bạn có chắn chắn muốn xóa hướng dẫn sử dụng'
            secondText={selectDelete?.Ten}
          ></TitleDelete>
        }
      ></ModalDelete>

      <Loading open={isLoading}></Loading>
    </>
  );
};

export default UserManualPage;
