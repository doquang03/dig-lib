import { Modal, type ModalProps } from 'antd';
import classNames from 'classnames';
import { Button } from 'components/Button';
import { Title } from 'components/Title';
import { type ReactNode } from 'react';

export type Props = {
  title?: ReactNode;
  labelOk?: string;
  loading?: boolean;
  onInvisile: VoidFunction;
  onOk?: VoidFunction;
  hasUpdate?: boolean;
  shouldFixedFooter?: boolean;
} & ModalProps;

const BaseModal = (props: Props) => {
  const {
    open,
    title,
    children,
    labelOk = 'Cập nhật',
    loading,
    hasUpdate = true,
    shouldFixedFooter = false,
    onInvisile,
    onOk,
    ...rest
  } = props;

  return (
    <Modal open={open} footer={null} closable={false} centered {...rest}>
      {title && <Title title={title as string} />}

      {children}

      <div
        className={classNames('mt-3 flex items-center justify-end gap-2', {
          'sticky bottom-3 z-10': shouldFixedFooter
        })}
      >
        <Button variant='secondary' onClick={onInvisile} loading={loading} disabled={loading}>
          Quay về
        </Button>

        {hasUpdate && (
          <Button onClick={onOk} loading={loading} disabled={loading}>
            {labelOk}
          </Button>
        )}
      </div>
    </Modal>
  );
};

export default BaseModal;
