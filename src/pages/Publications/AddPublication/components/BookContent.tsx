import { CloseSquareFilled, DeleteFilled, DownloadOutlined, EyeFilled } from '@ant-design/icons';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { bookApis } from 'apis';
import { AxiosResponse } from 'axios';
import { Button, Loading, Select } from 'components';
import { AUDIO_FILE_TYPE, VIDEO_FILE_TYPE } from 'constants/config';
import { useUser } from 'contexts/user.context';
import { useEffect, useMemo, useRef, useState } from 'react';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';

type Props = {
  bookId?: string;
  bookContentData?: GetBookContent;
  getImagesFile: (files: File[]) => void;
  getFile: (file: File | undefined) => void;
  getContentType: (contentType: 'PDF' | 'image' | 'video' | 'audio' | '') => void;
  getImageFromData: (images: string[]) => void;
  type: 'PDF' | 'image' | 'video' | 'audio' | '';
};

const BookContent = (props: Props) => {
  const { bookId, bookContentData, type, getImagesFile, getFile, getContentType, getImageFromData } = props;

  const [imagesPreview, setImagesPreview] = useState<File[]>([]);
  const [imagesData, setImagesData] = useState<string[]>([]);
  const [file, setFile] = useState<File>();

  const { isAllowedAdjustment } = useUser();

  const fileInutRef = useRef<HTMLInputElement | null>(null);

  const src = useMemo(() => file && URL.createObjectURL(file), [file]);

  const { register, setValue, watch, reset } = useForm<{ bookContent: 'PDF' | 'image' | 'video' | 'audio' | '' }>({
    defaultValues: { bookContent: '' }
  });

  const { mutate: deleteFile, isLoading: isLoadingDeleteFile } = useMutation({
    mutationFn: bookApis.deleteFile,
    onSuccess: () => {
      toast.success('Xóa file thành công');

      reset();
    }
  });

  useEffect(() => {
    if (!type) return;

    if (type === 'image' && bookContentData?.ListImage.length && bookContentData.isImage) {
      setImagesData(bookContentData.ListImage);
    }

    setValue('bookContent', type);
  }, [bookContentData, setValue, type]);

  const bookContent = bookContentData?.ListFile?.[0];

  const fileTypeBookContent = bookContent?.FileName.split('.') || [];

  const bookContentValue = watch('bookContent');

  const handleAccessFileInputRef = () => {
    fileInutRef.current?.click();
  };

  const handleOnFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files;
    getContentType(bookContentValue);

    if (!fileFromLocal) {
      return;
    }

    const fileSizeInMb = Number(((e.target?.files?.[0].size || 0) / (1024 * 1024)).toFixed(2));

    if (bookContentValue === 'PDF') {
      if (fileSizeInMb > 300) {
        return toast.warning('Dung lượng file sách điện tử phải nhỏ hơn 300Mb');
      }
      if (fileFromLocal[0].type !== 'application/pdf') {
        return toast.warning('Không đúng định dạng file PDF');
      }

      setFile(fileFromLocal[0]);
      getFile(fileFromLocal[0]);
    }

    if (bookContentValue === 'image') {
      const fileArray: File[] = [];
      for (const file of fileFromLocal) {
        if (file.type.includes('image')) {
          fileArray.push(file);
        }
      }

      setImagesPreview((prevState) => {
        getImagesFile([...prevState, ...fileArray]);
        return [...prevState, ...fileArray];
      });
    }

    if (bookContentValue === 'audio') {
      if (!fileFromLocal[0].type.includes('audio')) {
        return toast.warning('Không đúng định dạng Audio');
      }

      if (fileSizeInMb > 300) {
        return toast.warning('Dung lượng file audio phải nhỏ hơn 300Mb');
      }

      setFile(fileFromLocal[0]);
      getFile(fileFromLocal[0]);
    }

    if (bookContentValue === 'video') {
      if (!fileFromLocal[0].type.includes('video')) {
        return toast.warning('Không đúng định dạng Video');
      }

      if (fileSizeInMb > 300) {
        return toast.warning('Dung lượng file video phải nhỏ hơn 300Mb');
      }

      setFile(fileFromLocal[0]);
      getFile(fileFromLocal[0]);
    }
  };

  const inputAcceptions = useMemo(() => {
    switch (bookContentValue) {
      case 'image':
        return 'image/jpeg,image/gif,image/jpg,image/png,image/bmp';

      case 'PDF':
        return 'application/pdf';

      case 'audio':
        return '.mp3,audio/*';

      case 'video':
        return 'video/mp4,video/x-m4v,video/*';

      default:
        break;
    }
  }, [bookContentValue]);

  const handleRemovePreviewImage = (index: number) => () => {
    const array = [...imagesPreview];

    array.splice(index, 1);
    setImagesPreview(array);
    getImagesFile(array);
  };

  const handleRemovePreviewFile = () => () => {
    setFile(undefined);

    if (!!bookContentData?.ListFile?.[0]?.FileName) {
      // @ts-ignore
      queryClient.setQueryData(['bookContent', bookId], (prevData: AxiosResponse<ResponseApi<GetBookContent>>) => {
        deleteFile({ id: bookId + '', fileName: bookContentData?.ListFile?.[0]?.FileName || '' });

        prevData.data.Item.ListFile = [];

        return { ...prevData };
      });
    }
  };

  const queryClient = useQueryClient();

  const handleRemoveServerFile = () => () => {
    // setFile(undefined);
    // @ts-ignore
    queryClient.setQueryData(['bookContent', bookId], (prevData: AxiosResponse<ResponseApi<GetBookContent>>) => {
      deleteFile({ id: bookId + '', fileName: bookContentData?.ListFile?.[0]?.FileName || '' });

      prevData.data.Item.ListFile = [];

      return { ...prevData };
    });
  };

  const handleDeleteFile = (index: number) => () => {
    const array = [...imagesData];

    array.splice(index, 1);
    setImagesData(array);
    getImageFromData(array);
  };

  const handleRemovePDF = () => {
    if (file) {
      setFile(undefined);
      getFile(undefined);
    } else if (bookContentData?.ListFile?.[0].FileName) {
      deleteFile({ id: bookId + '', fileName: bookContentData?.ListFile?.[0]?.FileName });
    }
  };

  const handlePreviewPDF = () => {
    // const previewPDF = file && URL.createObjectURL(file);
    const fileURL = (file && URL.createObjectURL(file)) || bookContentData?.ListFile?.[0]?.LinkFile;

    //Open the URL on new Window
    const pdfWindow = window.open();
    // @ts-ignore
    pdfWindow.location.href = fileURL;
  };

  //  queryKey: ['bookContent', bookId]

  return (
    <div className='flex flex-col'>
      <label className='font-bold'>Nội dung sách</label>
      <div className='flex gap-2'>
        <Select
          items={[
            { value: '', label: 'Chọn loại nội dung sách' },
            { value: 'PDF', label: 'PDF' },
            { value: 'image', label: 'Hình ảnh' },
            { value: 'audio', label: 'Audio' },
            { value: 'video', label: 'Video' }
          ]}
          name='bookContent'
          register={register}
        />
        <Button
          type='button'
          variant={!bookContentValue ? 'disabled' : 'default'}
          onClick={handleAccessFileInputRef}
          disabled={!bookContentValue || !isAllowedAdjustment}
          className={!isAllowedAdjustment ? 'hidden' : ''}
        >
          Chọn tệp
        </Button>
        {bookContentValue && (
          <span className='my-auto ml-5 text-xs italic text-[#A7A7A7]'>
            Lưu ý: Bạn có thể upload file:{' '}
            <span className='font-bold'>
              {bookContentValue === 'PDF'
                ? '*pdf'
                : bookContentValue === 'video'
                ? '*avi, *mp4, *wmv, *mkv, *vob, *flv, *mpeg, *webm'
                : bookContentValue === 'audio'
                ? '*mp3, *wma, *wav, *flac, *alac, *ogg, *pcm, *aac, *m4a, *aiff'
                : '*jpeg, *gif, *jpg, *png, *bmp'}
            </span>{' '}
            với dung lượng tối đa: 300 MB
          </span>
        )}{' '}
        <input
          type='file'
          accept={inputAcceptions}
          className='hidden'
          ref={fileInutRef}
          onChange={handleOnFileChange}
          multiple={bookContentValue === 'image'}
          // @ts-ignore
          onClick={(e: React.MouseEvent<HTMLInputElement, MouseEvent>) => (e.target.value = null)}
        />
      </div>

      {bookContentValue === 'image' && (
        <>
          {/* add images */}
          <p className='mt-4 font-bold text-primary-10'>
            Thông tin hiển thị: {`${imagesPreview.length + imagesData.length} hình ảnh`}
          </p>

          <div className='mt-2 grid grid-cols-12 gap-3'>
            {!!imagesData.length && (
              <>
                {imagesData?.reverse().map((image, index) => {
                  return (
                    <div className='relative col-span-6 h-[170px] w-[full] md:col-span-4 lg:col-span-2' key={image}>
                      <button
                        className='absolute right-0 -top-1 shadow-lg'
                        type='button'
                        onClick={handleDeleteFile(index)}
                      >
                        <CloseSquareFilled style={{ color: 'red' }} />
                      </button>

                      <a href={image} target='_blank' rel='noreferrer'>
                        <img src={image} alt={image} className='h-full w-full shadow-md' />
                      </a>
                    </div>
                  );
                })}
              </>
            )}

            {imagesPreview.reverse().map((image, index) => {
              const src = URL.createObjectURL(image);

              return (
                <div
                  className='relative col-span-6 h-[170px] w-[full] md:col-span-4 lg:col-span-2'
                  key={image.lastModified + index}
                >
                  <button
                    className='absolute right-0 -top-1 shadow-lg'
                    type='button'
                    onClick={handleRemovePreviewImage(index)}
                  >
                    <CloseSquareFilled style={{ color: 'red' }} />
                  </button>

                  {Boolean(src) && (
                    <a href={src} target='_blank' rel='noreferrer'>
                      <img src={src} alt='Hình ảnh nội dung' className='h-full w-full shadow-md' />
                    </a>
                  )}
                </div>
              );
            })}
          </div>
        </>
      )}

      {bookContentValue === 'PDF' && (
        <>
          {file && file.type.toLocaleLowerCase().includes('pdf') && (
            <div className='max-w-xl'>
              <p className='mt-4 font-bold text-primary-10'>Thông tin hiển thị</p>
              <div className='flex items-center justify-between'>
                <p className='mt-4 font-bold text-primary-10'>
                  {file?.name || bookContentData?.ListFile?.[0]?.FileName}
                </p>

                <div className='flex items-center gap-5'>
                  <button onClick={handlePreviewPDF} type='button'>
                    <EyeFilled style={{ fontSize: '20px', color: '#3472A2' }} />
                  </button>

                  {/* <a
                    href={bookContentData?.ListFile?.[0]?.LinkFile}
                    download={bookContentData?.ListFile?.[0]?.FileName}
                  >
                    <button type='button'>
                      <DownloadOutlined style={{ fontSize: '20px', color: '#3472A2' }} />
                    </button>
                  </a> */}

                  <button onClick={handleRemovePDF} type='button'>
                    <DeleteFilled style={{ fontSize: '20px', color: '#dc3545' }} />
                  </button>
                </div>
              </div>
            </div>
          )}

          {!file && bookContent && bookContent.FileName.toLocaleLowerCase().includes('pdf') && (
            <div className='max-w-xl'>
              <p className='mt-4 font-bold text-primary-10'>Thông tin hiển thị</p>
              <div className='flex items-center justify-between'>
                <p className='mt-4 font-bold text-primary-10'>{bookContentData?.ListFile?.[0]?.FileName}</p>

                <div className='flex items-center gap-5'>
                  <button onClick={handlePreviewPDF} type='button'>
                    <EyeFilled style={{ fontSize: '20px', color: '#3472A2' }} />
                  </button>

                  <a
                    href={bookContentData?.ListFile?.[0]?.LinkFile}
                    download={bookContentData?.ListFile?.[0]?.FileName}
                  >
                    <button type='button'>
                      <DownloadOutlined style={{ fontSize: '20px', color: '#3472A2' }} />
                    </button>
                  </a>

                  <button onClick={handleRemovePDF} type='button'>
                    <DeleteFilled style={{ fontSize: '20px', color: '#dc3545' }} />
                  </button>
                </div>
              </div>
            </div>
          )}
        </>
      )}

      {bookContentValue === 'audio' && (
        <div className='w-full'>
          <p className='text-[20px] font-bold text-primary-10'>Thông tin hiển thị</p>
          {/* file from local */}
          {file && file.type.includes('audio') && (
            <div className='relative w-full'>
              <button className='absolute top-0 left-1/3 z-10' type='button' onClick={handleRemovePreviewFile()}>
                <CloseSquareFilled style={{ color: 'red' }} />
              </button>

              <audio controls className='mt-3 mb-1 w-1/3 shadow-md' key={src}>
                <source src={src} />
              </audio>

              <p className='text-primary-10'>{file.name}</p>
            </div>
          )}

          {/* file from db */}
          {!file && bookContent && AUDIO_FILE_TYPE.includes(fileTypeBookContent[fileTypeBookContent.length - 1]) && (
            <div className='relative w-full'>
              <button className='absolute top-0 left-1/3 z-10' type='button' onClick={handleRemoveServerFile()}>
                <CloseSquareFilled style={{ color: 'red' }} />
              </button>

              <audio controls className='mt-3 mb-1 w-1/3' key={src}>
                <source src={bookContent.LinkFile} />
              </audio>

              <p className='text-primary-10'>{bookContent.FileName}</p>
            </div>
          )}
        </div>
      )}

      {bookContentValue === 'video' && (
        <div className='w-full'>
          <p className='text-[20px] font-bold text-primary-10'>Thông tin hiển thị</p>
          {file && file.type.includes('video') && (
            <div className='relative w-full'>
              {/* file from local */}
              <button className='absolute top-0 left-1/2 z-10' type='button' onClick={handleRemovePreviewFile()}>
                <CloseSquareFilled style={{ color: 'red' }} />
              </button>

              <video width={700} height={700} controls autoPlay muted key={src} className='mt-3'>
                <source src={src} type='video/mp4' />
              </video>

              <p className='text-primary-10'>{file.name}</p>
            </div>
          )}

          {/* file from db */}
          {!file && bookContent && VIDEO_FILE_TYPE.includes(fileTypeBookContent[fileTypeBookContent.length - 1]) && (
            <div className='relative w-full'>
              <button className='absolute top-0 left-1/2 z-10' type='button' onClick={handleRemoveServerFile()}>
                <CloseSquareFilled style={{ color: 'red' }} />
              </button>

              <video width={700} height={700} controls autoPlay muted key={src} className='mt-3'>
                <source src={bookContent.LinkFile} type='video/mp4' />
              </video>

              <p className='text-primary-10'>{bookContent.FileName}</p>
            </div>
          )}
        </div>
      )}

      <Loading open={isLoadingDeleteFile} />
    </div>
  );
};

export default BookContent;
