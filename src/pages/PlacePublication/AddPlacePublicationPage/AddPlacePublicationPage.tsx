import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery } from '@tanstack/react-query';
import { placePublicationApis } from 'apis';
import { AxiosError } from 'axios';
import { Button, Input, Loading, Title } from 'components';
import { path } from 'constants/path';
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { placePublicationSchema } from 'utils/rules';
import * as yup from 'yup';
type placePublicationForm = yup.InferType<typeof placePublicationSchema>;

const AddPlacePublicationPage = () => {
  const { placePublicationId } = useParams();

  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    setValue,
    setError,
    formState: { errors }
  } = useForm<placePublicationForm>({
    defaultValues: {
      name: ''
    },
    resolver: yupResolver(placePublicationSchema)
  });

  const { isFetching } = useQuery({
    queryKey: ['EditByID', placePublicationId],
    queryFn: () => placePublicationApis.EditByID(placePublicationId as string),
    onSuccess: (res) => {
      setValue('name', res?.data?.Item?.Ten);
      setValue('shortname', res?.data?.Item?.TenNgan);
    },
    onError: () => {
      navigate(path.noixuatban);
    },
    enabled: !!placePublicationId
  });

  const { mutate: handleCreate, isLoading: isLoadingCreate } = useMutation({
    mutationFn: (payload: object) => placePublicationApis.Create(payload),
    onSuccess: () => {
      toast.success('Thêm nơi xuất bản thành công');
      navigate(path.noixuatban);
    },
    onError: (
      ex: AxiosError<
        ResponseApi<{
          Ten: string;
        }>
      >
    ) => {
      if (ex?.response?.data?.ExMessage === 'Ten') {
        setError('name', { type: 'duplicate', message: ex.response?.data.Message });
      }
    }
  });

  const { mutate: handleEdit, isLoading: isLoadingEdit } = useMutation({
    mutationFn: (payload: object) => placePublicationApis.EditModel(payload),
    onSuccess: () => {
      toast.success('Cập nhật nơi xuất bản thành công');
    },
    onError: (
      ex: AxiosError<
        ResponseApi<{
          Ten: string;
        }>
      >
    ) => {
      if (ex?.response?.data?.ExMessage === 'Ten') {
        setError('name', { type: 'duplicate', message: ex.response?.data.Message });
      }
    }
  });

  const handleCompelete = handleSubmit((data) => {
    data.name = data.name.replace(/\s+/g, ' ');
    data.shortname = data.shortname?.replace(/\s+/g, ' ');
    setValue('name', data.name);
    if (!placePublicationId) {
      handleCreate({ Ten: data.name, TenNgan: data.shortname });
    } else {
      if (isFetching) return;
      handleEdit({ Id: placePublicationId, Ten: data.name, TenNgan: data.shortname });
    }
  });

  function backPlacePublication() {
    navigate(path.noixuatban);
  }

  return (
    <div className='p-5'>
      <Title title={!!placePublicationId ? 'CẬP NHẬT NƠI XUẤT BẢN' : 'THÊM MỚI NƠI XUẤT BẢN'} />

      <div className='relative my-6'>
        <div>
          <div className='form-group row' style={{ marginBottom: '10px' }}>
            <label
              className='col-form-label col-sm-2 font-bold'
              style={{ marginBottom: '0', paddingTop: '5px', width: '100%' }}
            >
              Tên nơi xuất bản <span style={{ color: 'red' }}>*</span>
            </label>
          </div>
          <div className='form-group row'>
            <div className='component-form-group'>
              <Input
                placeholder='Nhập tên nơi xuất bản'
                className='form-control form-control-sm'
                register={register}
                name='name'
                errorMessage={errors?.name?.message}
                maxLength={256}
              ></Input>
            </div>
          </div>
          <div className='form-group row' style={{ marginBottom: '10px' }}>
            <label
              className='col-form-label col-sm-2 font-bold'
              style={{ marginBottom: '0', paddingTop: '5px', width: '100%' }}
            >
              Tên viết tắt
            </label>
          </div>
          <div className='form-group row'>
            <div className='component-form-group'>
              <Input
                placeholder='Nhập tên viết tắt'
                className='form-control form-control-sm'
                register={register}
                name='shortname'
                errorMessage={errors?.shortname?.message}
                maxLength={101}
              ></Input>
            </div>
          </div>
          <div className='form-group row'>
            <div className='component-form-group flex' style={{ textAlign: 'end' }}>
              <Button className='gray-button' onClick={backPlacePublication}>
                Quay về
              </Button>
              {!placePublicationId ? (
                <Button
                  className='btn btn-primary btn-sm blue-button'
                  onClick={handleCompelete}
                  style={{ marginLeft: '10px' }}
                >
                  Thêm mới
                </Button>
              ) : (
                <Button
                  className='btn btn-primary btn-sm blue-button'
                  onClick={handleCompelete}
                  style={{ marginLeft: '10px' }}
                >
                  Cập nhật
                </Button>
              )}
            </div>
          </div>
        </div>
      </div>
      <Loading open={isLoadingCreate || isLoadingEdit}></Loading>
    </div>
  );
};

export default AddPlacePublicationPage;
