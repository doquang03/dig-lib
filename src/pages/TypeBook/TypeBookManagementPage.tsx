import { path } from 'constants/path';
import { Outlet, useMatch } from 'react-router-dom';
import TypeBookManagermentContent from './TypeBookManagermentContent/TypeBookManagermentContent';

const TypeBookManagementPage = () => {
  const match = useMatch(path.theloaisach);

  return <>{Boolean(match) ? <TypeBookManagermentContent /> : <Outlet />}</>;
};

export default TypeBookManagementPage;
