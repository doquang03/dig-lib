import { useMutation, useQuery } from '@tanstack/react-query';
import { Table } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { connectionApis } from 'apis';
import visitingReaderApis from 'apis/visitingreader.apis';
import classNames from 'classnames';
import { Button, Input, Select, SizeChanger, Title } from 'components';
import { TYPE_READER_OPTIONS } from 'constants/options';
import { path } from 'constants/path';
import { ChangeEvent, FormEvent, useEffect, useMemo, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { convertDate, getSerialNumber } from 'utils/utils';

const VisitingReaders = () => {
  const [params, setParams] = useState<{
    textSearch: string;
    userType: string;
    WorkingId: string;
    page: number;
    pageSize: number;
  }>({
    textSearch: '',
    userType: '',
    page: 1,
    pageSize: 30,
    WorkingId: ''
  });

  const navigate = useNavigate();

  const { data: formOptionsData, isLoading: isLoadingForm } = useQuery({
    queryKey: ['getListModelUnitWorkingId'],
    queryFn: connectionApis.getListModelUnitWorkingId
  });

  const {
    data: visitingRederData,
    isLoading: loadingvisitingReder,
    mutate
  } = useMutation({
    mutationFn: (vars: { textSearch?: string; userType?: string; WorkingId: string }) => {
      const req: Partial<{
        TextForSearch: string;
        LoaiTK: string;
        WorkingId: string;
      }> = {};

      if (vars.WorkingId) {
        req.WorkingId = vars.WorkingId;
      }

      if (vars.textSearch) {
        req.TextForSearch = vars.textSearch;
      }

      if (vars.userType) {
        req.LoaiTK = vars.userType;
      }

      return visitingReaderApis.GetAll({ page: params.page, pageSize: params.pageSize, ...req });
    }
  });

  const options = useMemo(() => {
    if (!formOptionsData?.data.Item) return;

    return [{ label: 'Chọn trường', value: '' }, ...formOptionsData?.data.Item.DonVi];
  }, [formOptionsData]);

  useEffect(
    () =>
      mutate({
        textSearch: '',
        userType: '',
        WorkingId: ''
      }),
    []
  );

  const { ListModel, Count } = visitingRederData?.data?.Item || { ListModel: [], Count: 0 };

  const columns: ColumnsType<VisitingReader> = [
    {
      key: 'STT',
      title: 'STT',
      render: (value, record, index) => getSerialNumber(params.page, params.pageSize, index)
    },
    {
      key: 'Ten',
      title: 'Tên bạn đọc',
      dataIndex: 'Ten'
    },
    {
      key: 'MaSoThanhVien',
      title: 'Mã bạn đọc',
      dataIndex: 'MaSoThanhVien'
    },
    {
      key: 'LoaiTK',
      title: 'Loại bạn đọc',
      dataIndex: 'LoaiTK',
      render: (value, record) => (record.LoaiTK === 'hs' ? 'Học sinh' : record.LoaiTK === 'gv' ? 'Giáo viên' : '')
    },
    {
      key: 'TenTruong',
      title: 'Trường',
      dataIndex: 'TenTruong'
    },
    {
      key: 'Lop/To',
      title: 'Lớp/Tổ',
      dataIndex: 'Lop/To',
      render: (value, record) => (record.LoaiTK === 'hs' ? record.LopHoc : record.LoaiTK === 'gv' ? record.ChucVu : '')
    },
    {
      key: 'NgaySinh',
      title: 'Ngày sinh',
      dataIndex: 'NgaySinh',
      render: (value, { NgaySinh }) => <p>{convertDate(NgaySinh)}</p>
    },
    {
      key: 'GioiTinh',
      title: 'Giới tính',
      dataIndex: 'GioiTinh'
    },
    {
      key: 'DaMuon',
      title: 'Đã mượn, trả',
      dataIndex: 'DaMuon',
      render: (value, { DaMuon }) => <p>{DaMuon + ' quyển sách'}</p>
    },
    {
      key: 'DangMuon',
      title: 'Đang mượn',
      dataIndex: 'DangMuon',
      render: (value, record) => (
        <div
          className='italic text-primary-50 hover:cursor-pointer hover:underline'
          onClick={() => navigate(path.muonTraSach, { state: { idUserReserving: record.Id } })}
        >
          {record.DangMuon + ' quyển sách'}
        </div>
      ),
      onCell: (record) => ({
        className: 'text-left'
      })
    }
  ];

  const handleSubmitForm = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    mutate({
      textSearch: params.textSearch,
      userType: params.userType,
      WorkingId: params.WorkingId
    });
  };

  const handleChangeTextSearch = (e: ChangeEvent<HTMLInputElement>) => {
    if (!e) return;

    const { value } = e.target;

    setParams((prevState) => {
      return {
        ...prevState,
        textSearch: value
      };
    });
  };

  const handleChangeUserType = (e: ChangeEvent<HTMLSelectElement>) => {
    if (!e) return;

    const { value } = e.target;

    setParams((prevState) => {
      return {
        ...prevState,
        userType: value
      };
    });
  };

  const handleChangeLocationType = (e: ChangeEvent<HTMLSelectElement>) => {
    if (!e) return;

    const { value } = e.target;

    setParams((prevState) => {
      return {
        ...prevState,
        WorkingId: value
      };
    });
  };

  const handleRefresh = () => {
    setParams({
      textSearch: '',
      userType: '',
      page: 1,
      pageSize: 30,
      WorkingId: ''
    });

    mutate({
      textSearch: '',
      userType: '',
      WorkingId: ''
    });
  };

  return (
    <div className='p-5'>
      <Title title='BẠN ĐỌC LIÊN THƯ VIỆN' />

      <form
        className='mt-3 flex flex-1 flex-col items-center gap-1 bg-primary-50/20 p-2.5 md:flex-row'
        onSubmit={handleSubmitForm}
      >
        <Input
          placeholder={'Nhập tên, mã bạn đọc'}
          containerClassName='w-1/4'
          onChange={handleChangeTextSearch}
          value={params.textSearch}
        />

        <Select
          items={TYPE_READER_OPTIONS}
          className='w-full md:w-1/4'
          onChange={handleChangeUserType}
          value={params.userType}
        />

        <Select
          items={options || []}
          className='w-full md:w-1/4'
          onChange={handleChangeLocationType}
          value={params.WorkingId}
        />

        <Button variant='secondary' type='button' className='w-full md:w-auto' onClick={handleRefresh}>
          Làm mới
        </Button>

        <Button variant='default' type='submit' className='w-full md:w-auto'>
          Tìm kiếm
        </Button>
      </form>

      <Table
        columns={columns}
        dataSource={ListModel}
        className='custom-table mt-3'
        bordered
        loading={loadingvisitingReder || isLoadingForm}
        rowKey={(row) => row.Id}
        pagination={{
          onChange(current, pageSize) {
            setParams((prevState) => {
              return {
                ...prevState,
                page: current,
                pageSize
              };
            });
          },
          pageSize: params.pageSize,
          hideOnSinglePage: true,
          showSizeChanger: false
        }}
      />

      <div
        className={classNames('relative', {
          'mt-[64px]': params.pageSize >= (Count || 0)
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger visible={Boolean(Count)} value={params.pageSize + ''} total={Count + ''} />
        </div>
      </div>
    </div>
  );
};

export default VisitingReaders;
