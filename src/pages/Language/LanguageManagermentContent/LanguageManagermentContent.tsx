import { DeleteFilled, EditOutlined } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Tooltip } from 'antd';
import Table, { type ColumnsType } from 'antd/es/table';
import { languageApis } from 'apis';
import classNames from 'classnames';
import { Button, Empty, Input, Loading, ModalDelete, SizeChanger, Title, TitleDelete } from 'components';
import type { ButtonCustomProps } from 'components/Button/Button';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import { usePaginationNavigate, useQueryConfig } from 'hooks';
import { isEmpty, omitBy, trim } from 'lodash';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import { createSearchParams, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getSerialNumber } from 'utils/utils';

// TODO: Delete as soon as api implement
type Mock = {
  Id: string;
  Ten: string;
  TenNgan: string;
};

type FormInput = {
  TextForSearch: string;
};

const LanguageManagermentContent = () => {
  const { register, handleSubmit, reset, setValue } = useForm<FormInput>({
    defaultValues: {
      TextForSearch: ''
    }
  });

  const navigate = useNavigate();

  const handleNavigation = usePaginationNavigate();

  const queryConfig = useQueryConfig();
  const [totalListLanguage, setTotalListLanguage] = useState(0);
  const [selectedDelete, setSelectedDelete] = useState<Mock>();
  const [isVisiable, setIsVisiable] = useState(false);

  const { isAllowedAdjustment } = useUser();

  const {
    data: dataLanguage,
    refetch,
    isFetching
  } = useQuery({
    queryKey: ['Language', queryConfig.page, queryConfig.pageSize, queryConfig.TextForSearch],
    queryFn: () => {
      let page = 1;
      if (typeof queryConfig.page === 'string') page = parseInt(queryConfig.page);
      let queryString = `?page=${page}&sizeNumber=${queryConfig.pageSize}`;
      if (queryConfig.TextForSearch !== undefined) queryString += `&TextForSearch=${queryConfig.TextForSearch}`;
      let request = languageApis.Index(queryString);
      return request;
    }
  });

  const listLanguage = useMemo(() => {
    if (!dataLanguage?.data?.Item) return;
    const { ListLanguage, count } = dataLanguage?.data?.Item;
    setTotalListLanguage(count);
    return ListLanguage;
  }, [dataLanguage?.data?.Item]);

  const { mutate: handleDelete, isLoading: isLoadingDelete } = useMutation({
    mutationFn: (Id: string) => languageApis.Delete(Id),
    onSuccess: () => {
      toast.success('Xóa ngôn ngữ thành công');

      refetch();
    }
  });

  const onPaginate = useCallback(
    (page: number, pageSize: number) => {
      handleNavigation({ ...queryConfig, page: page + '', pageSize: pageSize + '' });
    },
    [handleNavigation, queryConfig]
  );

  const pagination = useMemo(() => {
    return {
      current: (!!queryConfig.page && +queryConfig.page) || 1,
      pageSize: (!!queryConfig.pageSize && +queryConfig.pageSize) || 30,
      total: totalListLanguage,
      onChange: onPaginate,
      hideOnSinglePage: true,
      showSizeChanger: false,
      showQuickJumper: true,
      locale: { jump_to: '', page: '' }
    };
  }, [onPaginate, queryConfig.page, queryConfig.pageSize, totalListLanguage]);

  const columns = useMemo(() => {
    const _columns: ColumnsType<Mock> = [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        className: 'min-content',
        render: (value, record, index) => {
          return (
            queryConfig.page && queryConfig.pageSize && getSerialNumber(+queryConfig.page, +queryConfig.pageSize, index)
          );
        },
        width: 100
      },
      {
        title: 'Tên ngôn ngữ',
        dataIndex: 'Ten',
        key: 'Ten',
        render: (value, { Ten }) => (
          <Tooltip placement='topLeft' title={Ten} arrow={true}>
            <p>{Ten}</p>
          </Tooltip>
        ),
        onCell: (record) => ({
          className: 'text-left'
        }),
        ellipsis: true
      },
      {
        title: 'Tên viết tắt',
        dataIndex: 'TenNgan',
        key: 'TenNgan',
        onCell: (record) => ({
          className: 'text-left'
        }),
        ellipsis: true
      }
    ];

    if (isAllowedAdjustment) {
      _columns.push({
        title: 'Hành động',
        dataIndex: 'actions',
        key: 'actions',
        className: 'min-content',
        width: 200,
        render: (value, record) => {
          return (
            <>
              <button
                className='mx-2'
                onClick={(e) => {
                  e.stopPropagation();
                  navigate(`CapNhat/${record.Id}`);
                }}
              >
                <EditOutlined style={{ fontSize: '20px', color: '#08c' }} />
              </button>

              <button
                className='mx-2'
                onClick={(e) => {
                  e.stopPropagation();
                  setSelectedDelete(() => {
                    return record;
                  });
                  setIsVisiable(true);
                }}
              >
                <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
              </button>
            </>
          );
        }
      });
    }

    return _columns;
  }, [isAllowedAdjustment, navigate, queryConfig.page, queryConfig.pageSize]);

  useEffect(() => {
    Object.keys(queryConfig).forEach((key) => {
      if (!key) {
        return;
      }
      // @ts-ignore
      setValue(key, queryConfig[key]);
    });
  }, [queryConfig, setValue]);

  const onSubmit = handleSubmit((data) => {
    handleNavigation({
      ...omitBy({ ...data, TextForSearch: trim(data.TextForSearch, ' ').replace(/\s\s+/g, ' ') }, isEmpty),
      page: '1'
    });
  });

  const handleResetField = () => {
    const { pathname } = window.location;

    navigate({
      pathname: pathname,
      search: createSearchParams({}).toString()
    });

    reset();
  };

  // TODO: Do something with selected Languages
  const handleSelectedLanguages = () => {
    navigate(path.themngonngu);
  };

  let locale = {
    emptyText: 'Không tìm được kết quả phù hợp'
  };

  return (
    <div className='p-5'>
      <Title title='DANH SÁCH NGÔN NGỮ' />

      <form className='my-5 flex flex-col gap-2 bg-secondary-10 py-3 px-3.5 md:flex-row' onSubmit={onSubmit}>
        <div className='flex w-full flex-col gap-2 md:w-1/2 md:flex-row'>
          <Input placeholder='Nhập ngôn ngữ' containerClassName='w-[100%]' name='TextForSearch' register={register} />

          <Button type='button' variant='secondary' className='shrink-0 font-semibold' onClick={handleResetField}>
            Làm mới
          </Button>

          <Button type='submit' variant='default' className='shrink-0 font-semibold'>
            Tìm kiếm
          </Button>
        </div>
      </form>

      <Button
        className={classNames('w-[100%] md:w-auto', {
          hidden: !isAllowedAdjustment,
          'w-[100%] md:w-auto': isAllowedAdjustment
        })}
        onClick={handleSelectedLanguages}
      >
        Thêm ngôn ngữ
      </Button>

      <div className='mt-6'>
        <Table
          loading={isFetching && !listLanguage?.length}
          columns={columns}
          dataSource={listLanguage || []}
          pagination={pagination}
          scroll={{ x: 980 }}
          rowKey={(record) => record.Id}
          className='custom-table'
          locale={{
            emptyText: () => <Empty label={locale.emptyText} />
          }}
          bordered
        />
      </div>

      <div
        className={classNames('relative', {
          // @ts-ignore
          'mt-[64px]': (queryConfig.page && +queryConfig.pageSize) >= totalListLanguage
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger
            visible={!!listLanguage?.length}
            value={queryConfig.pageSize}
            total={totalListLanguage.toString()}
          />
        </div>
      </div>

      <ModalDelete
        open={isVisiable}
        closable={false}
        title={
          <TitleDelete firstText='Bạn có chắn chắn muốn xóa ngôn ngữ' secondText={selectedDelete?.Ten}></TitleDelete>
        }
        // TODO: Remove inline funtion, integrate api
        handleCancel={() => {
          setSelectedDelete(undefined);
          setIsVisiable(false);
        }}
        handleOk={() => {
          setIsVisiable(false);
          handleDelete(selectedDelete?.Id as string);
          setSelectedDelete(undefined);
        }}
      />
      <Loading open={isLoadingDelete}></Loading>
    </div>
  );
};

export default LanguageManagermentContent;
