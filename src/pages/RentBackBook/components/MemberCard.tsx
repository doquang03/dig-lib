import { Button } from 'components';
import { Link } from 'react-router-dom';
import { convertDate } from 'utils/utils';

const MemberCard = ({
  member,
  onCloseTab,
  isLeft = true
}: {
  member: Student;
  onCloseTab?: VoidFunction;
  isLeft?: boolean;
}) => {
  return (
    <div className='grid h-80 w-52 flex-shrink-0 flex-col place-items-center items-center justify-center gap-1 rounded bg-white py-2 px-1 shadow-md'>
      <img
        src={member?.HinhChanDung || '/content/default-avatar.jpeg'}
        alt={member?.Ten || ''}
        className='h-40 w-48 hover:cursor-pointer'
      />
      {member?.Id ? (
        <Link
          to={member?.LoaiTK === 'gv' ? `/GiaoVien/CapNhat/${member?.Id}` : `/HocSinh/CapNhat/${member?.Id}`}
          className='my-1 truncate font-bold text-tertiary-20 underline hover:cursor-pointer'
        >
          {member?.Ten?.length > 20 ? member?.Ten.substring(0, 20).concat('...') : member?.Ten}
        </Link>
      ) : (
        <span className='my-1 truncate font-bold text-tertiary-20'>
          {' '}
          {member?.Ten?.length > 20 ? member?.Ten.substring(0, 20).concat('...') : member?.Ten}
        </span>
      )}

      <div className='flex flex-col'>
        {member?.LoaiTK === 'gv' ? (
          <span className='font-semibold'>
            Tổ: {member?.ChucVu?.length > 10 ? member.ChucVu.substring(0, 10).concat('...') : member?.ChucVu || ''}
          </span>
        ) : (
          <span className='font-semibold'>
            Lớp: {member?.LopHoc?.length > 8 ? member?.LopHoc.substring(0, 8).concat('...') : member?.LopHoc || ''}{' '}
          </span>
        )}
        {member?.LoaiTK === 'gv' ? (
          <span className='font-semibold'>Mã giáo viên: {member.MaSoThanhVien}</span>
        ) : (
          <span className='font-semibold'>Mã học sinh: {member?.MaSoThanhVien} </span>
        )}
        <span className='font-semibold'>Ngày sinh: {convertDate(member?.NgaySinh)}</span>

        {isLeft && (
          <Button variant='danger' className='mt-2' onClick={onCloseTab}>
            Thoát
          </Button>
        )}
      </div>
    </div>
  );
};

export default MemberCard;
