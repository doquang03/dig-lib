import { useQuery } from '@tanstack/react-query';
import { Modal } from 'antd';
import { connectionApis } from 'apis';
import { Title } from 'components';
import { useUser } from 'contexts/user.context';
import { ConnectionPage, SettingPermission } from './components';

const Connection = () => {
  const {
    data: unitConnectionStatusData,
    isLoading: loadingUnitConnectionStatus,
    refetch: refecthUnitConcectionStatus,
    isRefetching
  } = useQuery({
    queryKey: ['getUnitConnectionStatus'],
    queryFn: connectionApis.getUnitConnectionStatus
  });

  const { profile } = useUser();

  const isConnected = unitConnectionStatusData?.data.Item.connection;

  if (loadingUnitConnectionStatus || isRefetching) {
    return (
      <Modal open centered footer={null} closable={false}>
        <p>Hệ thống đang kiểm trang tình trạng kết nối của đơn vị {profile?.NameSchool}.</p>
        <p>Vui lòng đợi. </p>
      </Modal>
    );
  }

  return (
    <div className='p-5'>
      <Title title='KẾT NỐI MỤC LỤC LIÊN HỢP' />

      {isConnected ? (
        <SettingPermission
          onDisconnect={refecthUnitConcectionStatus}
          loading={isRefetching || loadingUnitConnectionStatus}
        />
      ) : (
        <ConnectionPage onConnect={refecthUnitConcectionStatus} loading={isRefetching || loadingUnitConnectionStatus} />
      )}
    </div>
  );
};

export default Connection;
