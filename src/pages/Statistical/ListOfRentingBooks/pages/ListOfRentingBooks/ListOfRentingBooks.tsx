import { TabsProps } from 'antd';
import { Tabs, Title } from 'components';
import { useState } from 'react';
import ListOfRentingBookTable from '../../components/ListOfRentingBookTable';
import { useLocation } from 'react-router-dom';

const ListOfRentingBooks = () => {
  const [activeTab, setActiveTab] = useState<string>('month');
  const { state } = useLocation();
  const statusBook: 0 | 1 | 2 | 3 | 4 | 5 = state ? state?.TinhTrang : undefined;

  const items: TabsProps['items'] = [
    {
      key: 'month',
      label: `Thống kê tháng`,
      children: <ListOfRentingBookTable picker='month' statusBook={statusBook} />,
      forceRender: true
    },
    {
      key: 'day',
      label: `Thống kê ngày`,
      children: <ListOfRentingBookTable picker='date' statusBook={statusBook} />,
      forceRender: false
    }
  ];

  const handleChangeTab = (activeTab: string) => setActiveTab(activeTab);

  return (
    <div className='p-5'>
      <Title title='Danh mục mượn sách về nhà' />

      <div className='mt-3'>
        <Tabs items={items} activeKey={activeTab} onChange={handleChangeTab} destroyInactiveTabPane={true} />
      </div>
    </div>
  );
};

export default ListOfRentingBooks;
