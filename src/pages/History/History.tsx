import { useMutation, useQuery } from '@tanstack/react-query';
import { DatePicker, Table, Tooltip } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { historyApis } from 'apis';
import classNames from 'classnames';
import { Button, Empty, SizeChanger, Title } from 'components';
import dayjs from 'dayjs';
import { uniqueId } from 'lodash';
import { useMemo, useState } from 'react';
import { toast } from 'react-toastify';
import { getSerialNumber } from 'utils/utils';

const { RangePicker } = DatePicker;

type RangeValue = Parameters<NonNullable<React.ComponentProps<typeof DatePicker.RangePicker>['onChange']>>[0];

const History = () => {
  const [totalPage, setTotalPage] = useState(0);
  const [sorter, setSorter] = useState(true);

  const [searchParams, setSearchParams] = useState<{
    TuNgay: string;
    DenNgay: string;
    page: number;
    pageSize: number;
  }>({
    TuNgay: dayjs().startOf('year').startOf('day').toISOString(),
    DenNgay: dayjs().endOf('day').toISOString(),
    page: 1,
    pageSize: 30
  });

  const { data, isLoading } = useQuery({
    queryFn: () =>
      historyApis.Index(searchParams.TuNgay, searchParams.DenNgay, searchParams.page, searchParams.pageSize, sorter),
    queryKey: ['historyIndex', searchParams, sorter]
  });
  const { mutate: downloadFile, isLoading: loadingFile } = useMutation({
    mutationFn: () => historyApis.downloadExcelHistory(searchParams.TuNgay, searchParams.DenNgay),
    onSuccess: (data) => {
      toast.success('Tải sổ tổng quát thành công');

      const blob = new Blob([data.data, { type: 'application/xls' }]);
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = 'LichSuHoatDong.xls';
      link.click();
    }
  });

  const listHistory = useMemo(() => {
    if (!data?.data?.Item?.ListData) return [];
    setTotalPage(data?.data?.Item?.NumberRow);
    return data?.data?.Item?.ListData;
  }, [data?.data?.Item]);

  const handleChangeRangePicker = (values: RangeValue) => {
    if (!values) return;

    setSearchParams({
      page: 1,
      pageSize: 30,
      TuNgay: values?.[0]?.startOf('day').toISOString() as string,
      DenNgay: values?.[1]?.endOf('day').toISOString() as string
    });
  };

  const columns: ColumnsType<{ TaiKhoan: string; ChucNang: string; SuKien: string; NoiDung: string }> = [
    {
      key: 'STT',
      title: 'STT',
      dataIndex: 'stt',
      render: (value, record, index) => getSerialNumber(searchParams.page, searchParams.pageSize, index),
      width: 80
    },
    {
      key: 'userName',
      title: 'Tài khoản',
      dataIndex: 'TaiKhoan',
      render: (value, { TaiKhoan }) =>
        TaiKhoan.length > 25 ? (
          <Tooltip title={TaiKhoan} arrow={true}>
            <p className='text-center'>{TaiKhoan.substring(0, 25).concat('...')}</p>
          </Tooltip>
        ) : (
          <p className='text-center'>{TaiKhoan}</p>
        )
    },
    {
      key: 'method',
      title: 'Chức năng',
      dataIndex: 'ChucNang',
      render: (value, { ChucNang }) =>
        ChucNang.length > 25 ? (
          <Tooltip title={ChucNang} arrow={true}>
            <p className='text-left'>{ChucNang.substring(0, 25).concat('...')}</p>
          </Tooltip>
        ) : (
          <p className='text-left'>{ChucNang}</p>
        )
    },
    {
      key: 'event',
      title: 'Sự kiện',
      dataIndex: 'SuKien',
      render: (value, { SuKien }) =>
        SuKien.length > 25 ? (
          <Tooltip title={SuKien} arrow={true}>
            <p className='text-left'>{SuKien.substring(0, 25).concat('...')}</p>
          </Tooltip>
        ) : (
          <p className='text-left'>{SuKien}</p>
        )
    },
    {
      key: 'content',
      title: 'Nội dung',
      dataIndex: 'NoiDung',
      width: 500,
      render: (value, { NoiDung }) =>
        NoiDung.length > 61 ? (
          <Tooltip title={NoiDung} arrow={true}>
            <p className='text-left'>{NoiDung.substring(0, 61).concat('...')}</p>
          </Tooltip>
        ) : (
          <p className='text-left'>{NoiDung}</p>
        )
    },
    {
      key: 'CreateTime',
      title: 'Thời gian',
      dataIndex: 'ThoiGian',
      sorter: true
    }
  ];

  const handleTableChange = (pagination: any, filters: any, sorter: any) => {
    if (sorter.order === 'ascend') setSorter(false);
    else setSorter(true);
  };

  return (
    <div>
      <Title title='Lịch sử truy cập' />

      <form className='my-3 flex flex-1 items-center gap-1 bg-primary-50/30 p-2.5'>
        <RangePicker
          className='w-1/4 border border-gray-300 py-[0.8rem] px-2 outline-none'
          format={['DD/MM/YYYY', 'DD-MM-YYYY']}
          defaultValue={[dayjs().startOf('year'), dayjs()]}
          placeholder={['Ngày bắt đầu', 'Ngày kết thúc']}
          disabledDate={(current) => {
            return current && current > dayjs().endOf('day');
          }}
          onChange={handleChangeRangePicker}
        />
      </form>

      <Button onClick={() => downloadFile()} loading={loadingFile}>
        Tải file Excel
      </Button>

      <Table
        loading={isLoading}
        columns={columns}
        pagination={{
          onChange(current, pageSize) {
            setSearchParams((prevState) => {
              return {
                ...prevState,
                pageSize,
                page: current
              };
            });
          },
          pageSize: searchParams.pageSize,
          hideOnSinglePage: true,
          showSizeChanger: false,
          showQuickJumper: true,
          locale: { jump_to: '', page: '' },
          total: totalPage
        }}
        dataSource={listHistory}
        scroll={{ x: 980 }}
        rowKey={(record) => uniqueId()}
        className='custom-table mt-3'
        bordered
        locale={{
          emptyText: () => <Empty />
        }}
        onChange={handleTableChange}
        showSorterTooltip={{
          title: ''
        }}
      />

      <div
        className={classNames('relative', {
          'mt-[64px]': (searchParams.page && searchParams.pageSize) >= totalPage
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger
            visible={!!listHistory?.length}
            value={searchParams.pageSize + ''}
            currentPage={searchParams.page.toString()}
            total={totalPage + ''}
          />
        </div>
      </div>
    </div>
  );
};

export default History;
