import { useMutation } from '@tanstack/react-query';
import { teacherApis } from 'apis';
import classnames from 'classnames';
import { Button, Input, Loading, Title } from 'components';
import { path } from 'constants/path';
import { useEffect, useState } from 'react';
import { useLocation } from 'react-router';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { download } from 'utils/utils';
const ExportCardPage = () => {
  const navigate = useNavigate();
  const { state } = useLocation();
  const selectedTeachers = state ? state.selectedTeachers : undefined;
  const [type, setType] = useState<'1Mat' | '2Mat'>('1Mat');

  const [mau, setMau] = useState<'mau1' | 'mau2'>('mau1');

  const { mutate: ExportCard, isLoading: isLoadingExportCard } = useMutation({
    mutationFn: (payload: object) => teacherApis.downloadMauThe(payload),
    onSuccess: (res) => {
      download(`MauTheGV-${mau}.doc.doc`, res.data);
      toast.success('Xuất thẻ thư viện thành công');
    }
  });

  const handleChooseType = (type: '1Mat' | '2Mat') => () => {
    setType(type);
  };

  const handleExportCard = (mau: string) => {
    ExportCard({ lstMSGV: selectedTeachers, mauThe: mau, CachIn: type });
  };

  useEffect(() => {
    if (!selectedTeachers || !selectedTeachers?.length) {
      return navigate(path.giaovien);
    }
  }, [navigate, selectedTeachers]);

  const handleBack = () => {
    navigate(path.giaovien);
  };

  return (
    <div className='py-5'>
      <Title title='Xuất thẻ thư viện' />
      <div className='mt-10 flex'>
        <p className='mr-10 font-bold'>Cách in:</p>

        <div>
          <button className='flex' onClick={handleChooseType('1Mat')}>
            <Input checked={type === '1Mat'} type='radio' />
            <label className='ml-2'>In 1 mặt (mặt trước và mặt sau của thẻ nằm cùng 1 trang)</label>
          </button>

          <button className='flex' onClick={handleChooseType('2Mat')}>
            <Input checked={type === '2Mat'} type='radio' />
            <label className='ml-2'>In 2 mặt (mặt trước và mặt sau của thẻ nằm 2 trang đối xứng nhau)</label>
          </button>
        </div>
      </div>

      <div className='mt-10 flex'>
        <p className='mr-10 font-bold'>Mẫu 1:</p>

        <button
          className={classnames(
            `transform rounded-lg p-2 transition duration-200 ease-linear ${mau === 'mau1' ? 'bg-primary-10/95' : ''}`
          )}
          onClick={(event) => {
            setMau('mau1');
          }}
        >
          <img src='/content/MauTheGV1.png' alt='Thẻ thư viện mẫu 1' className='h-[210px] w-[700px]' />
        </button>
      </div>

      <div className='mt-10 flex'>
        <p className='mr-10 font-bold'>Mẫu 2:</p>

        <button
          className={classnames(
            `transform rounded-lg p-2 transition duration-200 ease-linear ${mau === 'mau2' ? 'bg-primary-10/95' : ''}`
          )}
          onClick={() => {
            setMau('mau2');
          }}
        >
          <img src='/content/MauTheGV2.png' alt='Thẻ thư viện mẫu 2' className='h-[210px] w-[700px]' />
        </button>
      </div>
      <div className='mr-10 flex items-center justify-end'>
        <Button variant='secondary' onClick={handleBack}>
          Quay về
        </Button>
        <Button
          variant='default'
          className='ml-2 font-semibold'
          onClick={(e) => {
            handleExportCard(mau);
          }}
        >
          In thẻ thư viện
        </Button>
      </div>
      <Loading open={isLoadingExportCard} />
    </div>
  );
};

export default ExportCardPage;
