import { omit } from 'lodash';
import httpThuVien from 'utils/httpThuVien';
import { serialize } from 'utils/utils';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/QuanLyKiemKe`;

const auditApis = {
  getAudits: (
    payload?: Partial<{
      MaPhieuKiemKe: SearchInputField;
      TextForSearch: string;
      NgayVaoSo: SearchRangeField;
      NgayChotSo: SearchRangeField;
      page: number;
      pageSize: number;
    }>
  ) => {
    return httpThuVien.post<ResponseApi<{ List_SoKiemKe: Audit[]; TotalCount: number }>>(
      `/${endPoint}/Index`,
      payload || {}
    );
  },
  getAudit: (id: string) => {
    return httpThuVien.get<ResponseApi<{ soKiemKe: Audit; ListKiemKe: BookAudit[] }>>(
      `/${endPoint}/ChiTietKiemKe?Id=${id}`
    );
  },
  downloadExcel: (id: string) => {
    return httpThuVien.post(`/${endPoint}/DownloadExcel/${id}`, {}, { responseType: 'arraybuffer' });
  },
  createAudit: (payload: {
    Id?: string;
    TenPhieu: string;
    IdKho?: string;
    NgayVaoSo: string;
    NgayChotSo: string;
    IdUserAdmin: string;
    UserName: string;
    GhiChu?: string;
    books: Array<{
      Id: string;
      IdSachCaBiet: string;
      MaDKCB: string;
      TenSach: string;
      IdKho: string;
      IdTinhTrang: string;
      GhiChu: string;
      GiaBia: string;
      Final?: boolean;
    }>;
  }) =>
    httpThuVien.post<ResponseApi>(`/${endPoint}/TaoPhieuKiemKe?${serialize(omit(payload, ['books']))}`, payload.books),
  deleteAudit: (id: string) => httpThuVien.delete<ResponseApi>(`/${endPoint}/DeletePhieu/${id}`),
  removeListSCBPhieuKiemKe: (ids: string[], IdPhieu: string) =>
    httpThuVien.post<ResponseApi>(`/${endPoint}/RemoveChiTiet?IdPhieu=${IdPhieu}`, ids)
};

export default auditApis;
