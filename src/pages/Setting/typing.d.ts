declare module '@ckeditor/ckeditor5-react' {
  import Event from '@ckeditor/ckeditor5-utils/src/eventinfo';
  import { EditorConfig } from '@ckeditor/ckeditor5-core/src/editor/editorconfig';
  import CustomEditor from 'ckeditor5-custom-build';
  import * as React from 'react';
  const CKEditor: React.FunctionComponent<{
    disabled?: boolean;
    editor: typeof CustomEditor;
    data?: string;
    id?: string;
    config?: EditorConfig;
    onReady?: (editor: CustomEditor) => void;
    onChange?: (event: Event, editor: CustomEditor) => void;
    onBlur?: (event: Event, editor: CustomEditor) => void;
    onFocus?: (event: Event, editor: CustomEditor) => void;
    onError?: (event: Event, editor: CustomEditor) => void;
  }>;
  export { CKEditor };
}
declare module 'ckeditor5-custom-build' {
  const CustomEditorBuild: any;

  export = CustomEditorBuild;
}
