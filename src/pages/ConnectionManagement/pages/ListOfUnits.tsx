import { useMutation, useQuery } from '@tanstack/react-query';
import { DatePicker, Table, Tooltip } from 'antd';
import locale from 'antd/es/date-picker/locale/vi_VN';
import { type ColumnsType } from 'antd/es/table';
import { connectionApis } from 'apis';
import { type GetListOfUnitsVar } from 'apis/connection.api';
import { EditIcon } from 'assets';
import classNames from 'classnames';
import { Button, Input, Select, SizeChanger, Title } from 'components';
import { FORMAT_DATE_PICKER } from 'constants/config';
import { KEY_VALIDATION_STATUS_OPTIONS } from 'constants/options';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import dayjs, { type Dayjs } from 'dayjs';
import { useStateAsync } from 'hooks';
import { NotFoundPage } from 'pages/NotFound';
import { ChangeEvent, FormEvent, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { getSerialNumber } from 'utils/utils';
import { GenerateKeyModal, KeyConfigurationModal, UpdateKeyModal } from './components';

const ListOfUnits = () => {
  const [searchFormValues, setSearchFormValue] = useStateAsync<{
    name: string;
    status: string;
    connectionTime?: string;
    page: number;
    pageSize: number;
    sortConnectedTime?: boolean;
    sortExpiredTime?: boolean;
    sortTotalRecord?: boolean;
  }>({
    name: '',
    status: '',
    page: 1,
    pageSize: 30
  });
  const [modalVisible, setModalVisible] = useState<'generation' | 'configuration'>();
  const [selectedKey, setSelectedKey] = useState<string>();

  const { userType, permission } = useUser();

  const {
    mutateAsync: getListOfSchools,
    data: listOfKeysData,
    isLoading: loadingListsOfKey
  } = useMutation({
    mutationFn: () => {
      const { page, connectionTime, name, pageSize, status, sortConnectedTime, sortExpiredTime, sortTotalRecord } =
        searchFormValues;

      const reqBody: Omit<GetListOfUnitsVar, 'page' | 'pageSize'> = {};

      if (name) {
        reqBody.TextForSearch = name;
      }

      if (status) {
        switch (status) {
          case 'Ngắt kết nối':
            reqBody.Status = status;
            break;

          case 'Đang kết nối':
            reqBody.Status = 'Đang kết nối';
            break;

          case 'Chưa kết nối':
            reqBody.Status = 'Chưa kết nối';
            break;

          default:
            break;
        }
      }

      if (connectionTime) {
        reqBody.ConnectionTime = connectionTime;
      }

      if (sortConnectedTime) {
        reqBody.SortConnectionTime = sortConnectedTime;
      } else {
        reqBody.SortConnectionTime = sortConnectedTime;
      }

      if (sortExpiredTime) {
        reqBody.SortExpiredTime = sortExpiredTime;
      } else {
        reqBody.SortExpiredTime = sortExpiredTime;
      }

      if (sortTotalRecord) {
        reqBody.SortTotalRecord = sortTotalRecord;
      } else {
        reqBody.SortTotalRecord = sortTotalRecord;
      }

      return connectionApis.getListOfKeys({ page, pageSize, ...reqBody });
    }
  });

  useEffect(() => {
    getListOfSchools();
  }, []);

  if (userType === 3000 && !permission?.Enable) {
    return <NotFoundPage />;
  }

  const { Key, Count } = listOfKeysData?.data.Item || { Key: [], Count: 0 };

  const colums: ColumnsType<Key> = [
    {
      key: 'STT',
      title: 'STT',
      render: (value, record, index) => getSerialNumber(searchFormValues.page, searchFormValues.pageSize, index),
      width: 80
    },
    {
      key: 'NameMember',
      title: 'Tên đơn vị',
      dataIndex: 'NameMember',
      render: (value, { NameMember, IdMember }) =>
        NameMember.length > 60 ? (
          <Tooltip placement='topLeft' title={NameMember} arrow={true}>
            <Link to={`${path.memberManagement}?unit=${NameMember}&workingId=${IdMember}`}>
              <p className='text-left text-primary-30 hover:cursor-pointer hover:underline'>
                {NameMember.substring(0, 60).concat('...')}
              </p>
            </Link>
          </Tooltip>
        ) : (
          <Link to={`${path.memberManagement}?unit=${NameMember}&workingId=${IdMember}`}>
            <p className='text-left text-primary-30 hover:cursor-pointer hover:underline'>{NameMember}</p>
          </Link>
        )
    },
    {
      key: 'Value',
      title: 'Key',
      dataIndex: 'Value'
    },
    {
      key: 'ConnectionTime',
      title: 'Thời gian kết nối',
      dataIndex: 'ConnectionTime',
      render: (value, record) =>
        record.ConnectionTime ? dayjs(record.ConnectionTime).format('HH:mm:ss DD/MM/YYYY') : '--',
      sorter: true
    },
    {
      key: 'ExpiredTime',
      title: 'Ngày hết hiệu lực',
      dataIndex: 'ExpiredTime',
      render: (value, record) => {
        const timeLeft = dayjs(record.ExpiredTime).diff(dayjs());
        const daysLeft = dayjs(record.ExpiredTime).diff(dayjs(), 'days');

        let seconds = Math.floor(timeLeft / 1000);
        let minutes = Math.floor(seconds / 60);
        let hours = Math.floor(minutes / 60);

        minutes = minutes % 60;
        hours = hours % 24;

        return record.ExpiredTime ? (
          <p>
            {dayjs(record.ExpiredTime).format('HH:mm DD/MM/YYYY')}{' '}
            <span className={timeLeft < 0 ? 'text-danger-10' : ''}>
              {timeLeft < 0
                ? '(Hết hiệu lực)'
                : `(Còn ${daysLeft < 0 ? `${hours} giờ ${minutes} phút` : `${daysLeft} ngày)`}`}
            </span>
          </p>
        ) : (
          'Vĩnh viễn'
        );
      },
      sorter: true
    },
    {
      key: 'TotalRecords',
      title: 'Tổng số biểu ghi',
      dataIndex: 'TotalRecords',
      sorter: true
    },
    {
      key: 'Status',
      title: 'Trạng thái kết nối',
      dataIndex: 'Status',
      render: (value, { Status, ConnectionTime }) => {
        let label = '';
        if (Status === 'Chưa kết nối' || !ConnectionTime) {
          label = 'Chưa kết nối';
        }

        if (Status === 'Ngắt kết nối' && !!ConnectionTime) {
          label = 'Ngắt kết nối';
        }

        if (Status === 'Đang kết nối') {
          label = 'Đang kết nối';
        }

        return (
          <p
            className={classNames('rounded-full  border-[1px] p-2', {
              'border-[#D9C510] bg-[#D9C510]/10 text-[#D9C510] shadow-[#D9C510]/80': Status === 'Đang kết nối',
              'border-[#119757] bg-[#119757]/10 text-[#119757] shadow-[#119757]/80':
                Status === 'Chưa kết nối' || !ConnectionTime,
              'border-[#A7A7A7] bg-[#A7A7A7]/10 text-[#A7A7A7] shadow-[#A7A7A7]/90':
                Status === 'Ngắt kết nối' && !!ConnectionTime
            })}
          >
            {label}
          </p>
        );
      }
    },
    {
      key: 'action',
      title: '',
      render: (value, record) => (
        // <Tooltip title='Cập nhật key'>
        <button
          className='mx-2'
          onClick={(e) => {
            setSelectedKey(record.IdKey);
          }}
        >
          <EditIcon style={{ fontSize: '20px', color: 'red' }} />
        </button>
        // </Tooltip>
      )
    }
  ];

  const handleSearchKeyByName = (e: ChangeEvent<HTMLInputElement>) => {
    if (!e) return;

    const { value } = e.target;

    setSearchFormValue((prevState) => {
      return { ...prevState, name: value };
    });
  };

  const handleChangeKeyStatus = (e: ChangeEvent<HTMLSelectElement>) => {
    if (!e) return;

    const { value } = e.target;

    setSearchFormValue((prevState) => {
      return { ...prevState, status: value };
    });
  };

  const handleChangeConnectionTime = (e: Dayjs | null) => {
    setSearchFormValue((prevState) => {
      return { ...prevState, connectionTime: e ? dayjs(e).startOf('day').toISOString() : undefined };
    });
  };

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    setSearchFormValue((prevState) => {
      return { ...prevState, page: 1, pageSize: 30 };
    });

    getListOfSchools();
  };

  const handleTableChange = async (pagination: any, filters: any, sorter: any) => {
    if (sorter.columnKey === 'ConnectionTime') {
      if (sorter.order === 'ascend') {
        await setSearchFormValue((prevState) => {
          return {
            ...prevState,
            sortConnectedTime: true
          };
        });
      }

      if (sorter.order === 'descend') {
        await setSearchFormValue((prevState) => {
          return {
            ...prevState,
            sortConnectedTime: false
          };
        });
      }

      if (!sorter.order) {
        delete searchFormValues.sortConnectedTime;

        await setSearchFormValue((prevState) => {
          return {
            ...prevState
          };
        });
      }
    }

    if (sorter.columnKey === 'ExpiredTime') {
      if (sorter.order === 'ascend') {
        await setSearchFormValue((prevState) => {
          return {
            ...prevState,
            sortExpiredTime: true
          };
        });
      }
      if (sorter.order === 'descend') {
        await setSearchFormValue((prevState) => {
          return {
            ...prevState,
            sortExpiredTime: false
          };
        });
      }
      if (!sorter.order) {
        delete searchFormValues.sortExpiredTime;

        await setSearchFormValue((prevState) => {
          return {
            ...prevState
          };
        });
      }
    }

    if (sorter.columnKey === 'TotalRecords') {
      if (sorter.order === 'ascend') {
        await setSearchFormValue((prevState) => {
          return {
            ...prevState,
            sortTotalRecord: true
          };
        });
      }

      if (sorter.order === 'descend') {
        await setSearchFormValue((prevState) => {
          return {
            ...prevState,
            sortTotalRecord: false
          };
        });
      }

      if (!sorter.order) {
        delete searchFormValues.sortTotalRecord;

        await setSearchFormValue((prevState) => {
          return {
            ...prevState
          };
        });
      }
    }

    await getListOfSchools();
  };

  return (
    <div className='p-5'>
      <Title title='Quản lý kết nối' />

      <div className='my-3 flex items-center gap-2'>
        <Button onClick={() => setModalVisible('generation')}>Thêm key</Button>

        <Link to={path.listOfKeys}>
          <Button>Danh sách key</Button>
        </Link>

        <Button onClick={() => setModalVisible('configuration')}>Cấu hình key</Button>
      </div>

      <form className='w-full bg-primary-50/20 p-2.5' onSubmit={handleSubmit}>
        <div className='flex w-full flex-1 flex-col items-center gap-1 md:flex-row'>
          <Input
            placeholder={'Nhập tên đơn vị'}
            containerClassName='w-full'
            name='TextForSearch'
            onChange={handleSearchKeyByName}
            value={searchFormValues.name}
          />

          <Select
            items={KEY_VALIDATION_STATUS_OPTIONS}
            className='w-full'
            onChange={handleChangeKeyStatus}
            value={searchFormValues.status}
          />

          <DatePicker
            placeholder='Chọn thời gian kết nối'
            className={'w-full border border-gray-300 py-[0.8rem] px-2 outline-none'}
            format={FORMAT_DATE_PICKER}
            locale={locale}
            onChange={handleChangeConnectionTime}
            value={!!searchFormValues.connectionTime ? dayjs(searchFormValues.connectionTime) : null}
          />

          <Button
            variant='secondary'
            type='button'
            className='w-full flex-shrink-0 flex-grow md:w-auto'
            onClick={async () => {
              await setSearchFormValue({
                name: '',
                status: '',
                page: 1,
                pageSize: 30
              });

              await getListOfSchools();
            }}
          >
            Làm mới
          </Button>

          <Button variant='default' type='submit' className='w-full flex-shrink-0 flex-grow md:w-auto'>
            Tìm kiếm
          </Button>
        </div>
      </form>

      <Table
        className='custom-table my-3'
        bordered
        columns={colums}
        loading={loadingListsOfKey}
        dataSource={Key}
        rowKey={(row) => row.NameMember}
        pagination={{
          current: searchFormValues.page,
          defaultPageSize: 30,
          pageSize: searchFormValues.pageSize,
          total: Count,
          onChange: async (page, pageSize) => {
            await setSearchFormValue((prevState) => {
              return { ...prevState, page, pageSize };
            });

            await getListOfSchools();
          },
          hideOnSinglePage: true,
          showSizeChanger: false,
          showQuickJumper: true,
          locale: { jump_to: '', page: '' }
        }}
        onChange={handleTableChange}
        locale={{
          triggerDesc: 'Click để sắp xếp tăng dần',
          triggerAsc: 'Click để sắp xếp giảm dần',
          cancelSort: 'Click để hủy sắp xếp'
        }}
      />

      <div
        className={classNames('relative', {
          'mt-[64px]': searchFormValues.pageSize >= Count
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger
            visible={!!Count}
            value={searchFormValues.pageSize + ''}
            currentPage={searchFormValues.page.toString()}
            total={Count + ''}
            onChange={async (pageSize) => {
              await setSearchFormValue((prevState) => {
                return { ...prevState, page: 1, pageSize: +pageSize };
              });

              await getListOfSchools();
            }}
          />
        </div>
      </div>

      <GenerateKeyModal
        open={modalVisible === 'generation'}
        onClose={() => setModalVisible(undefined)}
        onSuccess={getListOfSchools}
      />

      <KeyConfigurationModal open={modalVisible === 'configuration'} onCancel={() => setModalVisible(undefined)} />

      <UpdateKeyModal
        open={Boolean(selectedKey)}
        onClose={() => setSelectedKey(undefined)}
        id={selectedKey}
        onSuccess={getListOfSchools}
      />
    </div>
  );
};

export default ListOfUnits;
