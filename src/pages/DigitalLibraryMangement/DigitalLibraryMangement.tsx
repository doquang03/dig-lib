import { Outlet } from 'react-router-dom';

const DigitalLibraryMangement = () => {
  return <Outlet />;
};

export default DigitalLibraryMangement;
