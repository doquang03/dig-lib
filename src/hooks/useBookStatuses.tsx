import { useQuery } from '@tanstack/react-query';
import { rentBack } from 'apis';
import { useMemo } from 'react';

const useBookStatuses = () => {
  const { data, isLoading, refetch, ...rest } = useQuery({
    queryKey: ['status'],
    queryFn: rentBack.getAllStatusBooks,
    staleTime: Infinity
  });

  const statusOptions = useMemo(() => {
    if (!data?.data.Item) return [];

    return data.data.Item.map(({ TenTT, Id }) => {
      return {
        label: TenTT,
        value: Id
      };
    });
  }, [data?.data?.Item]);

  return { data: statusOptions, isLoading, refetch, ...rest };
};

export default useBookStatuses;
