module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        primary: {
          10: "#3472A2",
          20: "#0F74A6",
          30: "#17a2b8",
          40: "#138496",
          50: "#80bdff",
          60: "#3D9BE3",
        },
        secondary: {
          10: "#3d9be326",
          20: "#494d51e0",
          30: "#C8C8C8",
        },
        danger: {
          10: "#dc3545",
          20: "#c82333",
          30: "#A23434",
          40: "#F35B3F",
          50: "#ff16165e",
        },
        tertiary: {
          10: "#E2F0FB",
          20: "#f35b3fc7",
          30: "#119757",
          40: "#f35b3f4f",
          50: "#6622f730",
          60: "#EFF3D6",
          70: "#F7C411",
        },
      },
    },
  },
  plugins: [
    require("@tailwindcss/line-clamp"),
  ],
};
