type DigitalDocument = {
  Id: string;
  TenSach: string;
  IdTacGia: string[];
  IdNhaXuatBan: string;
  NgayXuatBan: string;
  IdNgonNgu: string;
  IsPublish: boolean;
  PermissionView: number;
  PermissionDownload: number;
};

type DetailDigitalDocument = {
  Id: string;
  TenSach: string;
  IdNhaXuatBan: string;
  IdThuMucSach: string;
  IdNgonNgu: string;
  ISBN: string;
  DDC: string;
  listIdTacGia: string[];
  listIdTacGiaPhu: string[];
  Day19: string;
  NgayXuatBan: string;
  TomTat: string;
  LienKet: string;
  NguonTaiLieu: string;
  MoTaVatLy: string;
  DienMaoBaoQuat: string;
  SoBanSao: number;
  QuyenDoc: number;
  PhiDocSach: string;
  QuyenTai: number;
  KhoMau: string;
  TiLeTaiXuong: string;
  GiaBia: string;
  LinkBiaSach: string;
  ListMedia: {
    ListFile: {
      LinkFile: string;
      FileName: string;
      FileNameRoot: string;
      PageNumber: string;
    }[];
    ListImage: string[];
    isImage: boolean;
  };
};
