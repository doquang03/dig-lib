type Book = {
  Id?: string;
  MaKiemSoat?: string;
  TenSach?: string;
  IdBoSuuTap?: string;
  IdKeSach?: string;
  IdNhaXuatBan?: string;
  IdNgonNgu?: string;
  IdNoiXuatBan?: string;
  IdThuMucSach?: string;
  DDC?: string;
  Day19?: string;
  IdMaMau?: string;
  NamXuatBan?: string;
  TaiBan?: string;
  XuatXu?: string;
  SoTrang?: string;
  NguoiBienDich?: string;
  TomTat?: string;
  TaiLieuDinhKem?: string;
  CongKhai?: boolean;
  NoiXuatBan?: string;
  GiaBia?: string;
  PhiMuonSach?: string;
  SoLuongTong?: number;
  SoLuongConLai?: number;
  SoLanDuocMuon?: number;
  LinkBiaSach?: string;
  MARC21?: string;
  QRlink?: string;
  QRData?: string;
  IsDeleted?: boolean;
  MaSach_BoGiaoDucCap?: string;
  Ten_NhaXuatBan?: string;
  Ten_TacGia?: string;
  Ten_NgonNgu?: string;
  Ten_ThuMucSach?: string;
  Ten_BoSuuTap?: string;
  listIdTacGia?: string[];
  listIdTacGiaPhu?: string[];

  TacGia?: string;
  TacGiaPhu?: string;
  TenSongNgu?: string;
  Id_TacGia?: string[];
  Price?: string;
  PhiMuonSach?: string;
  NguoiBienDich?: string;
  TaiLieuDinhKem?: string;
  CoSach?: string;

  MinhHoa?: string;
  IdChuDiem?: string;
  IdMonHoc?: string;
  PhuChu?: string;
  KhoiLop?: string[];

  ISBN?: string;
  SKU?: string;
  LLC?: string;
  MoTaVatLy?: string;
  KhoSach?: string;
  TungThu?: string;
  SoTrang?: string;

  // permission
  QuyenDoc?: number;
  QuyenTai?: number;
  QuyenMuon?: number;

  PhiDocSach?: string;
  PhiMuonSach?: string;
  TiLeTaiXuong?: string;

  // Digital document
  IsDigitalResource?: boolean;
  IsPublic?: boolean;
};

type InvidiualBook = {
  Id: string;
  IdSach: string;
  IdTrangThai: string;
  DangChoMuon: boolean;
  MaCaBiet: string;
} & Pick<Book, 'MaKiemSoat' | 'TenSach' | 'LinkBiaSach'>;

type GetImportMarc21Data = {
  CountInsert: number;
  ListAdded: Book[];
  CountFail: number;
};

type NguonCungCap = {
  Code: string;
  CreateDateTime: string;
  Id: string;
  TenNguonCungCap: string;
  TinhChat: number;
};

type TrangThaiSachVM = {
  Id: string;
  IdNguonCungCap: string;
  IdTinhChatNguonCungCap: string;
  IdSach: string;
  IdTrangThai: string;
  MaCaBiet: string;
  MaCaBiet_That: string;
  NgayVaoSo_String: string;
  SoChungTu: string;
  SoLuong: number;
  SoVaoSoTongQuat?: string;
  TrangThai: string;
  IsXuatQR: boolean;
  IsMuon: boolean;
  LinkBiaSach?: string;
};

type SachCaBiet = {
  Id?: string;
  IdSach?: string;
  IdTrangThai?: string;
  MaKSCB?: string;
  MaCaBienCu?: string;
  TenSach?: string;
  QRlink?: string;
  QRData?: string;
  IdChiTietNhap?: string;
  IsDeleted?: boolean;
  IsMua?: boolean;
  NgayVaoSo?: string | Date;
  SoChungTu?: number | string;
  IdNguonCungCap?: string;
  IdKho?: string;
  IdPhieuMuon?: string;
  Sort?: string;
  SoVaoSoTongQuat?: string;
  IdTinhChatNguonCungCap?: string;
  IdChiTietNhapKho?: string;
  IdChiTietXuatKho?: string;
  IsXuatQR?: boolean;
  CreateDateTime?: string;
  MaKiemSoat?: string;
  GiaBia?: string;
  LinkBiaSach?: string;
};

type DanhSachCaBietKho = {
  STT: number;
  IdSCB: string;
  MaSach: string;
  MaKSCB: string;
  IdSach: string;
  TenSach: string;
  BiaSach: string;
  isCheck: boolean;
  IsNhapKho: boolean;
  IsXuatKho: boolean;
  Sort: number;
  TrangThai: string;
  SoChungTu: string;
  SoVaoSoTongQuat: string;
  NgayVaoSo: string;
  NguonCungCap: string;
  SachCaBiet: SachCaBiet;
  IsDangDuocMuon: boolean;
  IsXuatQR: boolean;
};

type EditArraySach = {
  ListKhoSach: BookStoreType[];
  List_NguonCungCap: NguonCungCap[];
  List_Sach: Book[];
  List_SachCB: Array<SachCaBiet & { LinkBiaSach?: string }>;
  List_TrangThai: StatusBook[];
  List_ViTriSach: number;
  Position_Kho: number;
  TotalCount: number;
};

type TrangThaiSach = StatusBook[];

type GetWareHouseData = {
  Kho: number;
  List_NguonCungCap: NguonCungCap[];
  List_SoLuongTrangThaiSachVM: TrangThaiSachVM[];
  List_TrangThaiSach: StatusBook[];
};

type FileContent = {
  FileName: string;
  FileNameRoot: string;
  FileSize: number;
  LinkFile: string;
  PageNumber: string;
};

type GetBookContent = {
  ListFile: FileContent[];
  ListImage: string[];
  isImage: boolean;
};

type GetPageCover = {
  ListImage: string[];
};

type CreateReceiptVars = {
  IdSach: string;
  IdKho: string;
  SoLuong: number;
  IdTrangThai: string;
  IdNCC: string;
  SCT: string;
  SVS: string;
  NVS: string;
};

type RentingBook = {
  Id: string;
  IdUser: stirng;
  NgayMuon: string;
  NgayTra: string;
  IdTinhTrangSach: string;
} & Pick<SachCabiet, 'MaKSCB'> &
  Pick<Book, 'TenSach' | 'LinkBiaSach'>;

type BooksInStoreAudit = {
  Id: string;
  MaDKCB: string;
  IdTinhTrang: string;
  GiaTien: number | string;
  GhiChu?: string;
} & Pick<Book, 'MaKiemSoat' | 'TenSach' | 'TacGia' | 'NamXuatBan' | 'ISBN'> &
  Pick<SachCaBiet, 'IdKho'>;

type BookAudit = {
  MaKiemSoat?: string;
  Id: string;
  IdSachCaBiet: string;
  MaDKCB: string;
  TenSach: string;
  IdKho: string;
  IdTinhTrang: string;
  GhiChu: string;
  GiaBia: string;
  Final: boolean;
  IdPhieuMuon: string;
};

type HoldingBook = {
  IdSachCaBiet: string;
  IdTrangThai: string;
  LinkBia: string;
  MaKSCB: string;
  NgayMuon?: string;
  NgayTra: string;
  TenSach: string;
  IsDangMuon: boolean;
};

type StaticRentingBook = {
  Id: string;
  CreateDateTime: string;
  idUser: string;
  idSach: string;
  CachMuon: number;
  NgayGioMuon: string;
  NgayPhaiTra: string;
  NgayTraThucTe: string;
  DaTra: boolean;
  TrangThaiTra: null;
  LoaiTK: string;
  IsSynced: boolean;
  IsTaiCho: boolean;
  TrangThai: number;
  TrangThaiString: string;
  MaMau: string;
  TenTrangThai: string;
  SoSachTong: number;
  SoNgayGiaoDong: null;
  NgayMuonTemp: string;
  NgayTraTemp: string;
  NgayTraThucTeTemp: string;
  TenThanhVien: string;
  TenSach: string;
  MaCaBiet: string;
  MaKiemSoat: string;
  TrangThaiSach: string;
  TheLoaiSach: string;
  Lop: string;
  ChucVu: string;
  IsGlobal: boolean;
};

type OnlineRentingBook = {
  Id: string;
  CreateDateTime: string;
  idUser: string;
  idSach: string;
  CachMuon: number;
  NgayGioMuon: string;
  NgayPhaiTra: string;
  NgayTraThucTe: string;
  DaTra: boolean;
  TrangThaiTra: null;
  LoaiTK: string;
  IsSynced: boolean;
  IsTaiCho: boolean;
  TrangThai: number;
  TrangThaiString: string;
  MaMau: string;
  TenTrangThai: string;
  SoSachTong: number;
  SoNgayGiaoDong: null;
  NgayMuonTemp: string;
  NgayTraTemp: string;
  NgayTraThucTeTemp: string;
  TenThanhVien: string;
  TenSach: string;
  MaCaBiet: string;
  MaKiemSoat: string;
  TrangThaiSach: string;
  TheLoaiSach: string;
  Lop: string;
  ChucVu: string;
};

type BookReserving = {
  idMember: string;
  MaThanhVien: string;
  TenBanDoc: string;
  DonVi: string;
  LopTo: string;
  SoLuong: string;
  ThoiGianHetHieuLucGanNhat: string;
};

type StatisticalBookReserving = {
  Id: string;
  IdSach: string;
  MaKSCB: string;
  IdUser: string;
  TenSach: string;
  TenBanDoc: string;
  MaThanhVien: string;
  LopTo: string;
  SoLuong: string;
  ThoiGianDatMuon: string;
  ThoiGianHetHieuLuc: string;
  TrangThai: string;
  IsGlobal: boolean;
};

type BookApproval = {
  Id: string;
  NameMember: string;
  IdMember: string;
  Title: string;
  Author: string;
  UserName: string;
  CreateDateTime: string;
  IsApprove: boolean;
  BookType: number;
  CompleteTime: string;
};

type SuggestionDocument = Pick<
  Book,
  | 'Id'
  | 'TenSach'
  | 'TacGia'
  | 'TaiBan'
  | 'NoiXuatBan'
  | 'NamXuatBan'
  | 'SoTrang'
  | 'MinhHoa'
  | 'KhoMau'
  | 'TaiLieu'
  | 'TungThu'
  | 'ISBN'
  | 'TomTat'
  | 'LinkBiaSach'
> & { NhaXuatBan: string; KhoMau: string; Nguon: string[] };

type PullRequestBook = Pick<Book, 'TenSach' | 'Id' | 'TacGia' | 'ISBN' | 'NamXuatBan' | 'GiaBia'> & {
  AnhBia: string;
  NgonNgu: string;
  NhaXuatBan: string;
  LanXuatBan: string;
  LinkMedia: string;
  LienKet: string;
};

type ErrorSyncDocument = {
  CountDigital: number;
  CountErrorDigital: number;
  CountErrorPaper: number;
  CountPaper: number;
  CreateDateTime: string;
  Id: string;
  IdUser: string;
  UserName: string;
};

type ErrorSyncDocumentById = SuggestionDocument &
  Partial<{ ErrorMessage: string; LoaiSach: number; IdSach: string; Type: string }>;
