import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/MuonSach`;
const endpointGiaHan = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/GiaHan`;
const endpointReturn = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/TraSach`;
const endpointBooksByMember = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/SachTheoThanhVien`;
const enpointRentingTicket = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/PhieuMuonSach`;

type UpdateListBookVars = {
  idUser: string;
  listSachCB: {
    IdSCB: string;
    NgayTra: string;
    NgayMuon?: string;
  }[];
};

type ReturnBookVars = {
  Id: string;
  IdUser: string;
  IdTrangThai: string;
}[];

export type ExtentRentingVars = {
  Id: string;
  NgayTraNew: string;
}[];

const rentBackApis = {
  updateListBook: (payload: UpdateListBookVars) => {
    const { idUser, listSachCB } = payload;
    return httpThuVien.post<ResponseApi<{ ListFail: string[] }>>(
      `${endPoint}/UpdateListBook?idUser=${idUser}`,
      listSachCB
    );
  },
  updateListBookInDay: (payload: UpdateListBookVars) => {
    const { idUser, listSachCB } = payload;
    return httpThuVien.post<ResponseApi<{ ListFail: string[] }>>(
      `${endPoint}/UpdateListBookTaiCho?idUser=${idUser}`,
      listSachCB
    );
  },
  getRentingBook: (idUser: string) =>
    httpThuVien.post<ResponseApi<RentingBook[]>>(`${endPoint}/GetListBook_IdUser?IdUser=${idUser}`),
  getRentingBookInDay: (idUser: string) =>
    httpThuVien.post<ResponseApi<RentingBook[]>>(`${endPoint}/GetListBook_IdUser_TaiCho?IdUser=${idUser}`),
  getAllStatusBooks: () => httpThuVien.get<ResponseApi<StatusBook[]>>(`${endpointGiaHan}/GetAllTrangThaiSach`),
  returnBook: (payload: ReturnBookVars) => httpThuVien.post<ResponseApi>(`${endpointReturn}/UpdateListBook`, payload),
  extendRengting: (books: ExtentRentingVars, isTaiCho: boolean = false) =>
    httpThuVien.post<ResponseApi>(`${endpointGiaHan}/UpdateListBook?IsTaiCho=${isTaiCho}`, books),
  getBooksByMember: (userId: string) =>
    httpThuVien.get<
      ResponseApi<{
        ListSachDangDuocChon: HoldingBook[];
      }>
    >(`${endpointBooksByMember}/GetSachDaChon?userId=${userId}`),
  addBookToQueue: (books: HoldingBook[], userId: string) => {
    return httpThuVien.post<ResponseApi>(`${endpointBooksByMember}/UpdateSachDaChon?userId=${userId}`, books);
  },
  lostBook: (idPhieuMuon: string) => httpThuVien.get<ResponseApi>(`${endpointReturn}/MatSach/${idPhieuMuon}`),
  getRentingTickets: (payload: {
    page: number;
    pageSize: number;
    TextForSearch?: string;
    ThoiGianTao?: SearchRangeField;
    TinhTrang?: string;
    IsTaiCho?: boolean | null;
  }) =>
    httpThuVien.post<
      ResponseApi<{
        ListPhieuMuonSachUnit: {
          Id: string;
          MaPhieu: string;
          UserName: string;
          SoLuong: string;
          ThoiGianMuon: string;
          NgayHenTraGanNhat: string;
          TinhTrang: string;
        }[];
        TotalCount: number;
      }>
    >(`${enpointRentingTicket}/Index`, payload),
  getAdminRentingTicket: (id: string) =>
    httpThuVien.get<
      ResponseApi<{
        Id: string;
        MaPhieu: string;
        IdUserAdmin: string;
        UserNameAdmin: string;
        ThoiGian: string;
      }>
    >(`${enpointRentingTicket}/ChiTietPhieuMuonSach?Id=${id}`),
  getRentingBooksInTicket: (id: string) =>
    httpThuVien.get<
      ResponseApi<{
        ListThongTinMuonSach: Array<StaticRentingBook & { BiaSach: string }>;
        NgaySinh: string;
        Id: string;
        TenThanhVien: string;
        ToLop: string;
        MaSoThanhVien: string;
        LoaiTK: string;
      }>
    >(`${enpointRentingTicket}/DanhSachDangMuon?Id=${id}`),
  printTicket: (id: string) =>
    httpThuVien.post<ResponseApi>(
      `${enpointRentingTicket}/Print?id=${id}`,
      {},
      {
        responseType: 'arraybuffer'
      }
    )
};

export default rentBackApis;
