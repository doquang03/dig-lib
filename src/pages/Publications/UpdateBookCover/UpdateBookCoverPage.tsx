import { DeleteFilled } from '@ant-design/icons';
import { useMutation } from '@tanstack/react-query';
import { Table, Tooltip } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { coverBooksApis } from 'apis';
import { Button, Loading, ProgressBar, Title } from 'components';
import { path } from 'constants/path';
import 'css/AddTeacherAvatarPage.css';
import { useMemo, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';

type Mock = {
  STT: number;
  MaKiemSoat: string;
  TenSach: string;
  HinhChanDung: string;
  HinhHienTai: string;
};

type NotMatch = { STT: string; fileName: string };

const UpdateBookCoverPage = () => {
  const [step, setStep] = useState<number>(1);

  const fileInutRef = useRef<HTMLInputElement | null>(null);

  const ref = useRef<HTMLDivElement>(null);

  const [listAvatar, setListAvatar] = useState<Array<Mock>>([]);

  const [listError, setListError] = useState<Array<NotMatch>>([]);

  const [totalEntry, setTotalEntry] = useState();

  const [page, setPage] = useState(1);
  const [paginationSize, setPaginationSize] = useState(10);

  const navigate = useNavigate();

  const { mutate: uploadCover_preview, isLoading: isLoadingEnable } = useMutation({
    mutationFn: (fileCurrent: File) => {
      let bodyFormData = new FormData();
      bodyFormData.append('file', fileCurrent as Blob);
      return coverBooksApis.uploadCover(bodyFormData);
    },
    onSuccess: (res) => {
      let tempData = res?.data?.Item;
      let RawDataList = tempData.RawDataList;
      let tempUnit: Mock;
      let result: Array<Mock> = [];
      for (let i = 0; i < RawDataList?.length; i++) {
        tempUnit = {
          STT: i,
          TenSach: RawDataList[i][0],
          MaKiemSoat: RawDataList[i][1],
          HinhChanDung: RawDataList[i][2],
          HinhHienTai: RawDataList[i][3]
        };
        result.push(tempUnit);
      }
      setListAvatar(result);

      let tempError = tempData?.ErrorName;
      let resultError: Array<NotMatch> = [];
      let ErrorUnit: NotMatch;
      for (let i = 0; i < tempError.length; i++) {
        ErrorUnit = {
          STT: (i + 1).toString(),
          fileName: tempError[i]
        };
        resultError.push(ErrorUnit);
      }
      setListError(resultError);

      setStep(2);
    }
  });

  const { mutate: UploadSave, isLoading: isLoadingUploadSave } = useMutation({
    mutationFn: (payload: Array<string[]>) => coverBooksApis.saveCovers(payload),
    onSuccess: (res) => {
      setStep(3);
      setTotalEntry(res?.data?.Item?.ListSuccess?.length);
    }
  });

  const handleUploadSave = () => {
    let tempData;
    let result = [];
    for (let i = 0; i < listAvatar.length; i++) {
      tempData = [];
      tempData[0] = i;

      for (let param in listAvatar[i]) {
        switch (param) {
          case 'TenSach':
            tempData[0] = listAvatar[i].TenSach;
            break;
          case 'MaKiemSoat':
            tempData[1] = listAvatar[i].MaKiemSoat;
            break;
          case 'HinhChanDung':
            tempData[2] = listAvatar[i].HinhChanDung;
            break;
          case 'HinhHienTai':
            tempData[3] = listAvatar[i].HinhChanDung;
            break;
        }
      }

      result.push(tempData);
    }

    // @ts-ignore
    UploadSave(result);
  };

  const columnsTeacherData: ColumnsType<Mock> = useMemo(() => {
    const handleDeleteRow = (STT: number) => {
      let result: Array<Mock> = [];
      for (let i = 0; i < listAvatar.length; i++) {
        if (STT === listAvatar[i].STT) {
          continue;
        }
        result.push(listAvatar[i]);
      }
      setListAvatar(result);
    };

    let column: ColumnsType<Mock> = [
      {
        title: 'STT',
        dataIndex: 'STT',
        key: 'STT',
        render: (value: any, record: any, j: number) => (page - 1) * paginationSize + j + 1
      },
      {
        title: 'Mã kiểm soát',
        dataIndex: 'MaKiemSoat',
        key: 'MaKiemSoat'
      },
      {
        title: 'Tên sách',
        dataIndex: 'TenSach',
        key: 'TenSach',
        render: (value, { TenSach }) =>
          TenSach.length > 25 ? (
            <Tooltip placement='topLeft' title={TenSach} arrow={true}>
              <p className='text-center'>{TenSach.substring(0, 25).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{TenSach}</p>
          )
      }
    ];
    if (step === 2)
      column.push({
        title: 'Ảnh hiện tại',
        dataIndex: 'currentAvatar',
        render: (value, record) => {
          return (
            <>
              {step === 2 && (
                <div className='flex items-center justify-center'>
                  <img
                    src={record?.HinhHienTai ? record?.HinhHienTai : '/content/Book.png'}
                    alt=''
                    className='h-[80px] w-[80px]'
                  />
                </div>
              )}
            </>
          );
        }
      });
    column.push({
      title: (step === 2 && 'Ảnh cập nhật') || (step === 3 && 'Ảnh đại diện'),
      dataIndex: 'actions',
      key: 'actions',
      render: (value, record) => {
        return (
          <>
            <div className='flex items-center justify-center'>
              <img src={record?.HinhChanDung} alt='' className='h-[80px] w-[80px]' />
            </div>
          </>
        );
      }
    });
    if (step === 2)
      column.push({
        title: 'Hành động',
        dataIndex: 'actions',
        key: 'actions',
        render: (value, record) => {
          return (
            <>
              {step === 2 && (
                <button
                  className='mx-2'
                  onClick={(e) => {
                    e.stopPropagation();
                    handleDeleteRow(record.STT);
                  }}
                >
                  <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
                </button>
              )}
            </>
          );
        }
      });
    return column;
  }, [listAvatar, page, paginationSize, step]);

  const handleOnFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files?.[0];

    if (fileFromLocal) {
      uploadCover_preview(fileFromLocal);
    }
  };

  const handleAccessFileInputRef = () => {
    fileInutRef.current?.click();
  };

  const handleBack = () => {
    navigate(path.sach);
  };

  const handleBackCurrent = () => {
    ref.current?.scrollIntoView({ behavior: 'smooth' });
    setListAvatar([]);
    setStep(1);
  };

  const handleSubmit = () => {
    // TODO: Integrate api to submit data
    ref.current?.scrollIntoView({ behavior: 'smooth' });
    handleUploadSave();
  };

  const columnsNotMatchTable: ColumnsType<NotMatch> = [
    {
      title: 'STT',
      dataIndex: 'STT',
      key: 'STT',
      width: '100px'
    },
    {
      title: 'Tên file',
      dataIndex: 'fileName',
      key: 'fileName'
    }
  ];

  return (
    <div className='p-5'>
      <Title title='Cập nhật ảnh bìa sách' />

      <div className='my-10'>
        <ProgressBar activeStep={step} type='avatar' />
      </div>

      {step === 1 && (
        <div className='flex items-center justify-center rounded-md bg-tertiary-10 py-4'>
          <input
            type='file'
            accept='.zip,.rar,.7zip'
            className='hidden'
            ref={fileInutRef}
            onChange={handleOnFileChange}
          />

          <Button variant='default' className='ml-2' onClick={handleAccessFileInputRef}>
            Chọn tệp
          </Button>
        </div>
      )}

      <div>
        {/* show when data invalid is not undefined */}
        {step === 2 && !!listError?.length && (
          <div className='w-min'>
            <div className='w-full rounded-md bg-tertiary-40 px-3 py-4'>
              <span className='p-0'>
                Tổng số file không trùng khớp với mã sách: <span className='font-bold'>{listError.length}</span>
              </span>
            </div>

            <Table
              rootClassName='w-[350px]'
              loading={isLoadingEnable}
              columns={columnsNotMatchTable}
              pagination={false}
              // mock data
              dataSource={listError}
              rowKey={(record) => record.STT}
              className='custom-table'
              scroll={{ y: 165 }}
              bordered
            />
          </div>
        )}
        {step === 2 && (
          <>
            <p className='mt-4'>
              Tổng số hình ảnh được nhận diện:
              <span className='font-bold'> {listAvatar?.length}</span>
            </p>

            {listAvatar?.length > 0 && <h3 className='text-primary-10'>DANH SÁCH ĐƯỢC CẬP NHẬT ẢNH ĐẠI DIỆN</h3>}
          </>
        )}

        {step === 3 && listAvatar.length > 0 && (
          <>
            <p className='text-tertiary-30'>
              Cập nhật thành công: <span className='font-bold'>{' ' + totalEntry} sách</span>
            </p>

            <p className='text-danger-10'>
              Cập nhật thất bại: <span className='font-bold'>{' ' + listError.length} sách</span>
            </p>

            <h3 className='text-primary-10'>DANH SÁCH CẬP NHẬT ẢNH BÌA THÀNH CÔNG</h3>
          </>
        )}

        {!!listAvatar.length && (
          <>
            <div className='mt-6'>
              <Table
                loading={isLoadingEnable}
                columns={columnsTeacherData}
                pagination={{
                  onChange(current, pageSize) {
                    setPage(current);
                    setPaginationSize(pageSize);
                  },
                  defaultPageSize: 10,
                  hideOnSinglePage: true,
                  showSizeChanger: false
                }}
                dataSource={listAvatar || []}
                scroll={{ x: 980 }}
                rowKey={(record) => record.MaKiemSoat}
                className='custom-table'
                bordered
              />
            </div>
          </>
        )}

        <div className='mt-6 mr-10 flex items-center justify-end'>
          {step === 1 && (
            <>
              <Button variant='secondary' onClick={handleBack}>
                Quay về
              </Button>
            </>
          )}

          <>
            {step === 2 && !isLoadingEnable && (
              <>
                <Button variant='secondary' onClick={handleBackCurrent}>
                  Quay về
                </Button>
                {listAvatar?.length > 0 && (
                  <Button variant='default' className='ml-2' onClick={handleSubmit}>
                    Tiếp tục
                  </Button>
                )}
              </>
            )}

            {step === 3 && !isLoadingUploadSave && (
              <Button variant='default' className='ml-2' onClick={handleBack}>
                Hoàn tất
              </Button>
            )}
          </>
        </div>
        <Loading open={isLoadingEnable || isLoadingUploadSave}></Loading>
      </div>
    </div>
  );
};

export default UpdateBookCoverPage;
