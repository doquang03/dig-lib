import { DeleteFilled, DownloadOutlined } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Radio, Table, Tooltip } from 'antd';
import Column from 'antd/es/table/Column';
import ColumnGroup from 'antd/es/table/ColumnGroup';
import { bookStoreApis, rentBack } from 'apis';
import bookStoreTypeApis from 'apis/bookStoreType.apis';
import classNames from 'classnames';
import { Button, Loading, ProgressBar, Select, Title } from 'components';
import { MacDinhThuTuKhoSach_SCB_SGK } from 'constants/bookstore';
import { path } from 'constants/path';
import { UserConText } from 'contexts/user.context';
import 'css/AddBookNormalByExcel.css';
import { useContext, useMemo, useRef, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { download, saveByteArray } from 'utils/utils';

type BookColumsType = Book & {
  TenSach?: string;
  STT?: number;
  ListError?: Array<string>;
  cellError?: Array<number>;
  SoLuong?: number;
  TrangThai?: string;
  MaNCC?: string;
  SoChungTu?: string;
  SoVaoSoTongQuat?: string;
};

const AddBookSCBSGKByExcel = () => {
  type exampleCheckDelete = {
    [key: string]: Array<number>;
  };
  type exampleRawData = {
    [key: string]: Array<Array<string>>;
  };
  const { storeId } = useParams();
  const [currentStep, setCurrentStep] = useState<number>(1);
  const [rawDataList, setRawDataList] = useState<exampleRawData>({});
  // Mock state.
  const [listBookExcel, setListBookExcel] = useState<Array<BookColumsType>>([]);
  const [saveError, setSaveError] = useState(0);
  const [saveSuccess, setSaveSuccess] = useState(0);
  const [SaveSuccessSCB, setSaveSuccessSCB] = useState(0);
  const [listFail, setListFail] = useState([]);
  const [link, setLink] = useState<string>('');
  const [isCreate, setIsCreate] = useState<boolean>(true);
  const [fileName, setFileName] = useState('');
  const [statuses, setStatuses] = useState<Selections>([]);

  const fileInputRef = useRef<HTMLInputElement | null>(null);
  const ref = useRef<HTMLDivElement | null>(null);

  const [IdTrangThai, setIdTrangThai] = useState('');

  const user = useContext(UserConText);

  const navigate = useNavigate();

  const { data, isLoading } = useQuery({
    queryFn: () => bookStoreTypeApis.EditByID(storeId as string),
    queryKey: ['bookStoreTypeEditByID'],
    enabled: !!storeId
  });

  const { isLoading: isLoadingTT } = useQuery({
    queryKey: ['status'],
    queryFn: rentBack.getAllStatusBooks,
    onSuccess: (res) => {
      const statusesResults = res.data.Item.map(({ TenTT, Id }) => {
        return {
          label: TenTT,
          value: Id
        };
      });
      setStatuses([{ label: 'Chọn tình trạng', value: '' }, ...statusesResults]);
    }
  });

  const TenKho = useMemo(() => {
    return data?.data.Item.Ten;
  }, [data]);

  const { mutateAsync: PreviewImport, isLoading: isLoadingPreviewImport } = useMutation({
    mutationFn: (file: File) => {
      let bodyFormData = new FormData();
      bodyFormData.append('file', file as Blob);
      return bookStoreApis.PreviewExcel_SCB_SGK(bodyFormData, IdTrangThai);
    },
    onSuccess: (res) => {
      let DictData = res?.data?.Item?.DictData;
      setRawDataList(DictData);
      let UnitData: BookColumsType;
      let result: Array<BookColumsType> = [];
      let count: number;
      for (const param in DictData) {
        const tempData = DictData[param];
        result.push({
          TenSach: param
        });
        for (let i = 0; i < tempData.length; i++) {
          count = 0;
          UnitData = { STT: i };
          tempData[i].STT = i;
          // eslint-disable-next-line no-loop-func
          tempData[i].forEach((element: string, index: number) => {
            // @ts-ignore
            UnitData[MacDinhThuTuKhoSach_SCB_SGK[count++]] = element;
          });
          UnitData.STT = i;
          UnitData.TenSach = param;
          result.push(UnitData);
        }
      }
      setListBookExcel(result);
      setCurrentStep(2);
    }
  });

  const { mutate: ImportSave, isLoading: isLoadingImportSave } = useMutation({
    mutationFn: (payload: exampleRawData) =>
      bookStoreApis.ImportSave_SCB_SGK(
        payload,
        storeId as string,
        user.profile?.Id as string,
        user.profile?.UserName as string,
        isCreate,
        fileName,
        IdTrangThai
      ),
    onSuccess: (res) => {
      let tempData = res?.data?.Item;
      setSaveError(tempData?.SaveError);
      setSaveSuccess(tempData?.SaveSuccess);
      setSaveSuccessSCB(tempData?.SaveSuccess_SachCaBiet);
      setListFail(tempData?.ListFail);
      setLink(tempData?.MemoryStream);
      let listShow = tempData?.ListFail;
      let resultFinal: Array<BookColumsType> = [];
      let tempFor: BookColumsType;
      let checkParent = [];
      let count = 0;
      for (let i = 0; i < listShow.length; i++) {
        tempFor = listShow[i];
        if (checkParent.indexOf(tempFor.TenSach) === -1) {
          resultFinal.push({
            TenSach: tempFor.TenSach
          });
          checkParent.push(tempFor.TenSach);
          count = 0;
        }

        tempFor.STT = count++;
        resultFinal.push(tempFor);
      }
      setListBookExcel(resultFinal);
      setCurrentStep(3);
    }
  });

  const { mutate: DownloadTemplate, isLoading: isLoadingTemplate } = useMutation({
    mutationFn: () => bookStoreApis.TemplateExcel_SCB_SGK(),
    onSuccess: (res, fileName) => {
      download('MauSoKhoSach_DKCB_SGK.xls', res.data);
    }
  });

  const handleSubmit = () => {
    // TODO: Integrate api to submit data
    ImportSave(rawDataList);
    ref.current?.scrollIntoView({ behavior: 'smooth' });
    setCurrentStep(3);
  };

  const handleOnFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files?.[0];
    setFileName(fileFromLocal?.name as string);
    PreviewImport(fileFromLocal as File);
  };

  const handleAccessFileInputRef = () => {
    fileInputRef.current?.click();
  };

  const handleBack = () => {
    if (saveError > 0) {
      handleBackCurrent();
    } else navigate(path.khosach);
  };

  const handleBackCurrent = () => {
    setListBookExcel([]);
    setCurrentStep(1);
  };

  const handleDeleteRawData = (stringCheck: string) => {
    for (const param in rawDataList) {
      for (let i = 0; i < rawDataList[param].length; i++) {
        // @ts-ignore
        const tempData = rawDataList[param][i].STT + '_' + param;
        if (tempData === stringCheck) {
          rawDataList[param].splice(i, 1);
        }
      }
      if (rawDataList[param].length === 0) delete rawDataList[param];
    }
  };

  const handleDeleteRow = (STT: string) => {
    let result: Array<BookColumsType> = [];
    let tenSachDelete;
    let listSTT = [];
    let resetSTTTenSach;
    for (let i = 0; i < listBookExcel.length; i++) {
      if (listBookExcel[i].STT === undefined && STT === listBookExcel[i].STT + '_' + listBookExcel[i].TenSach) {
        tenSachDelete = listBookExcel[i].TenSach;
        continue;
      } else if (STT === listBookExcel[i].STT + '_' + listBookExcel[i].TenSach) {
        listBookExcel[i].STT !== undefined &&
          handleDeleteRawData(listBookExcel[i].STT + '_' + listBookExcel[i].TenSach);
        resetSTTTenSach = listBookExcel[i].TenSach;
        continue;
      } else if (tenSachDelete === listBookExcel[i].TenSach) {
        listBookExcel[i].STT !== undefined && listSTT.unshift(listBookExcel[i].STT + '_' + listBookExcel[i].TenSach);
        continue;
      }
      result.push(listBookExcel[i]);
    }
    listSTT.forEach((element) => {
      handleDeleteRawData(element);
    });
    if (resetSTTTenSach) {
      let count = 0;
      for (let i = 0; i < result.length; i++) {
        if (result[i].TenSach === resetSTTTenSach && result[i].STT !== undefined) {
          result[i].STT = count++;
        }
      }
    }
    for (let i = 0; i < result.length; i++) {
      if (rawDataList[result[i].TenSach as string] === undefined) result.splice(i, 1);
    }
    setListBookExcel(result);
  };

  return (
    <div className='excel-normal my-5' ref={ref}>
      <Title title={'THÊM SÁCH TỪ FILE EXCEL SỔ SÁCH GIÁO KHOA - ' + (TenKho === undefined ? '' : TenKho)} />
      {currentStep === 1 && (
        <div className='flex'>
          <div className='thongtin-group col-span-5 mt-5 w-[320px]'>
            <h5 className='title-box'>
              Thêm vào sổ nhập kho
              <div className='background-fake'></div>
            </h5>

            <Radio.Group
              className='form-group form-group-sm row'
              style={{ paddingLeft: '15px', paddingRight: '15px', marginBottom: 0, marginTop: '15px' }}
              onChange={(e: any) => {
                setIsCreate(e?.target?.value);
              }}
              value={isCreate}
            >
              <Radio value={true} className='text-inbox grow justify-center'>
                Thêm
              </Radio>
              <Radio value={false} className='text-inbox grow justify-center'>
                Không thêm
              </Radio>
            </Radio.Group>
          </div>
          <div className='thongtin-group col-span-5 mt-5 ml-4 w-[320px]'>
            <h5 className='title-box'>
              Tình trạng sách cá biệt
              <div className='background-fake'></div>
            </h5>
            <Select
              items={statuses}
              className='w-full'
              name='status'
              disabled={isLoadingTT}
              value={IdTrangThai}
              onChange={(e) => {
                setIdTrangThai(e.target.value);
              }}
            />
          </div>
        </div>
      )}
      <div className='my-10'>
        <ProgressBar activeStep={currentStep} type='excel' />
      </div>

      {/* TODO: Condition are currentStep > 1 and data is not undefined*/}
      {currentStep === 1 && (
        <div className='flex items-center justify-center rounded-md bg-tertiary-10 py-4'>
          <Button
            className='bg-tertiary-20 hover:bg-tertiary-20/30'
            onClick={() => {
              DownloadTemplate();
            }}
          >
            <DownloadOutlined />
            File excel mẫu
          </Button>

          <input
            type='file'
            accept='.csv,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel'
            className='hidden'
            ref={fileInputRef}
            onChange={handleOnFileChange}
            // @ts-ignore
            onClick={(e: React.MouseEvent<HTMLInputElement, MouseEvent>) => (e.target.value = null)}
          />

          <Button variant='default' className='ml-2' onClick={handleAccessFileInputRef}>
            Chọn tệp
          </Button>
        </div>
      )}

      {currentStep > 1 && (
        <div>
          {currentStep === 2 && (
            <>
              <p>
                Tổng số đầu sách:{' '}
                <span className='font-bold'>
                  {
                    listBookExcel?.filter((element) => {
                      if (element.STT === undefined) return true;
                      return false;
                    }).length
                  }
                </span>
              </p>

              {!!listBookExcel?.length && <h3 className='text-primary-10'>DANH SÁCH ĐÃ NHẬN DIỆN</h3>}
            </>
          )}

          {currentStep === 3 && (
            <>
              <p className='text-tertiary-30'>
                Lưu thành công:{' '}
                <span className='font-bold'>
                  {saveSuccess} đầu sách - {SaveSuccessSCB} sách cá biệt
                </span>
              </p>

              <p className='flex items-center gap-1 text-danger-10'>
                Lưu thất bại: <span className='font-bold'> {' ' + saveError} đầu sách</span>{' '}
                {listFail.length > 0 && (
                  <Button
                    variant='danger'
                    className='ml-2'
                    onClick={() => {
                      saveByteArray(link, 'DSSachLoi.xls', 'application/xls');
                    }}
                  >
                    <DownloadOutlined />
                    Tải file lỗi
                  </Button>
                )}
              </p>
              {listFail.length > 0 && <h3 className='uppercase text-primary-10'>Danh sách sách bị lỗi</h3>}
            </>
          )}

          {((currentStep === 2 && !!listBookExcel?.length) || (currentStep === 3 && listFail.length > 0)) && (
            <div className='relative mt-6'>
              <Table
                loading={isLoadingPreviewImport && isLoadingImportSave}
                pagination={{
                  defaultPageSize: 10,
                  hideOnSinglePage: true,
                  showSizeChanger: false
                }}
                dataSource={listBookExcel}
                scroll={{ x: 980 }}
                rowKey={(record) => {
                  return record.STT?.toString() + '_' + record.TenSach;
                }}
                className='custom-table'
                bordered
              >
                <ColumnGroup title='NĂM HỌC' rowSpan={2}>
                  <ColumnGroup rowSpan={0}>
                    <Column
                      width={150}
                      onCell={(record: BookColumsType) => ({
                        className: classNames(
                          currentStep === 3 &&
                            record?.cellError &&
                            record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('NgayVaoSo_String')) !== -1
                            ? 'min-h-38 errorBackGround py-2'
                            : '',
                          record.STT !== undefined ? '' : 'text-left font-bold text-lg'
                        ),
                        style: {
                          textAlign: record.STT !== undefined ? 'center' : 'left'
                        },
                        colSpan: record.STT !== undefined ? 1 : MacDinhThuTuKhoSach_SCB_SGK.length
                      })}
                      title='Ngày vào sổ'
                      dataIndex='NgayVaoSo_String'
                      key='NgayVaoSo_String'
                      render={(value: any, record: any) =>
                        record.STT === undefined ? (
                          record.TenSach
                        ) : record?.NgayVaoSo_String?.length > 15 ? (
                          <Tooltip placement='topLeft' title={record.NgayVaoSo_String} arrow={true}>
                            <p className='text-center'>{record.NgayVaoSo_String.substring(0, 15).concat('...')}</p>
                          </Tooltip>
                        ) : (
                          <p className='text-center'>{record.NgayVaoSo_String}</p>
                        )
                      }
                    />
                  </ColumnGroup>
                </ColumnGroup>
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('STT')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Số đăng kí (STT)'
                  dataIndex='STT'
                  key='STT'
                  render={(value: any, record: any, j: number) => record.STT + 1}
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('SoChungTu')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Số chứng từ'
                  dataIndex='SoChungTu'
                  key='SoChungTu'
                  render={(value: any, record: any) =>
                    record?.SoChungTu?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.SoChungTu} arrow={true}>
                        <p className='text-center'>{record.SoChungTu.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.SoChungTu}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('SoVaoSoTongQuat')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Số vào sổ tổng quát'
                  dataIndex='SoVaoSoTongQuat'
                  key='SoVaoSoTongQuat'
                  render={(value: any, record: any) =>
                    record?.SoVaoSoTongQuat?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.SoVaoSoTongQuat} arrow={true}>
                        <p className='text-center'>{record.SoVaoSoTongQuat.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.SoVaoSoTongQuat}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('MonLoai')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Môn loại (7)'
                  dataIndex='MonLoai'
                  key='MonLoai'
                  render={(value: any, record: any) => {
                    if (record?.MonLoai?.length > 0)
                      return record?.MonLoai?.length > 15 ? (
                        <Tooltip placement='topLeft' title={record.MonLoai} arrow={true}>
                          <p className='text-center'>{record.MonLoai.substring(0, 15).concat('...')}</p>
                        </Tooltip>
                      ) : (
                        <p className='text-center'>{record.MonLoai}</p>
                      );
                    if (record?.DDC?.length > 0)
                      return record?.DDC?.length > 15 ? (
                        <Tooltip placement='topLeft' title={record.DDC} arrow={true}>
                          <p className='text-center'>{record.DDC.substring(0, 15).concat('...')}</p>
                        </Tooltip>
                      ) : (
                        <p className='text-center'>{record.DDC}</p>
                      );
                    if (record?.Day19?.length > 0)
                      return record?.Day19?.length > 15 ? (
                        <Tooltip placement='topLeft' title={record.Day19} arrow={true}>
                          <p className='text-center'>{record.Day19.substring(0, 15).concat('...')}</p>
                        </Tooltip>
                      ) : (
                        <p className='text-center'>{record.Day19}</p>
                      );
                  }}
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('NamXuatBan')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Năm xuất bản'
                  dataIndex='NamXuatBan'
                  key='NamXuatBan'
                  render={(value: any, record: any) =>
                    record?.NamXuatBan?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.NamXuatBan} arrow={true}>
                        <p className='text-center'>{record.NamXuatBan.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.NamXuatBan}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('SoLuong')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Tổng số bản'
                  dataIndex='SoLuong'
                  key='SoLuong'
                  render={(value: any, record: any) =>
                    record?.SoLuong?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.SoLuong} arrow={true}>
                        <p className='text-center'>{record.SoLuong.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.SoLuong}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('GiaBia')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Giá bìa'
                  dataIndex='GiaBia'
                  key='GiaBia'
                  render={(value: any, record: any) =>
                    record?.GiaBia?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.GiaBia} arrow={true}>
                        <p className='text-center'>{record.GiaBia.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.GiaBia}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('ToTalPrice')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Thành tiền'
                  dataIndex='ToTalPrice'
                  key='ToTalPrice'
                  render={(value: any, record: any) =>
                    record?.ToTalPrice?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.ToTalPrice} arrow={true}>
                        <p className='text-center'>{record.ToTalPrice.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.ToTalPrice}</p>
                    )
                  }
                />
                <ColumnGroup title='Kiểm kê'>
                  <ColumnGroup title='20…'>
                    <Column
                      width={150}
                      onCell={(record: BookColumsType) => ({
                        className:
                          currentStep === 3 &&
                          record?.cellError &&
                          record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('KiemKe1')) !== -1
                            ? 'min-h-38 errorBackGround py-2'
                            : '',
                        colSpan: record.STT !== undefined ? 1 : 0
                      })}
                      title='Mất'
                      dataIndex='KiemKe1'
                      key='KiemKe1'
                      render={(value: any, record: any) =>
                        record?.KiemKe1?.length > 15 ? (
                          <Tooltip placement='topLeft' title={record.KiemKe1} arrow={true}>
                            <p className='text-center'>{record.KiemKe1.substring(0, 15).concat('...')}</p>
                          </Tooltip>
                        ) : (
                          <p className='text-center'>{record.KiemKe1}</p>
                        )
                      }
                    />
                    <Column
                      width={150}
                      onCell={(record: BookColumsType) => ({
                        className:
                          currentStep === 3 &&
                          record?.cellError &&
                          record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('KiemKe2')) !== -1
                            ? 'min-h-38 errorBackGround py-2'
                            : '',
                        colSpan: record.STT !== undefined ? 1 : 0
                      })}
                      title='Còn'
                      dataIndex='KiemKe2'
                      key='KiemKe2'
                      render={(value: any, record: any) =>
                        record?.KiemKe2?.length > 15 ? (
                          <Tooltip placement='topLeft' title={record.KiemKe2} arrow={true}>
                            <p className='text-center'>{record.KiemKe2.substring(0, 15).concat('...')}</p>
                          </Tooltip>
                        ) : (
                          <p className='text-center'>{record.KiemKe2}</p>
                        )
                      }
                    />
                  </ColumnGroup>
                  <ColumnGroup title='20…'>
                    <Column
                      width={150}
                      onCell={(record: BookColumsType) => ({
                        className:
                          currentStep === 3 &&
                          record?.cellError &&
                          record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('KiemKe3')) !== -1
                            ? 'min-h-38 errorBackGround py-2'
                            : '',
                        colSpan: record.STT !== undefined ? 1 : 0
                      })}
                      title='Mất'
                      dataIndex='KiemKe3'
                      key='KiemKe3'
                      render={(value: any, record: any) =>
                        record?.KiemKe3?.length > 15 ? (
                          <Tooltip placement='topLeft' title={record.KiemKe3} arrow={true}>
                            <p className='text-center'>{record.KiemKe3.substring(0, 15).concat('...')}</p>
                          </Tooltip>
                        ) : (
                          <p className='text-center'>{record.KiemKe3}</p>
                        )
                      }
                    />
                    <Column
                      width={150}
                      onCell={(record: BookColumsType) => ({
                        className:
                          currentStep === 3 &&
                          record?.cellError &&
                          record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('KiemKe4')) !== -1
                            ? 'min-h-38 errorBackGround py-2'
                            : '',
                        colSpan: record.STT !== undefined ? 1 : 0
                      })}
                      title='Còn'
                      dataIndex='KiemKe4'
                      key='KiemKe4'
                      render={(value: any, record: any) =>
                        record?.KiemKe4?.length > 15 ? (
                          <Tooltip placement='topLeft' title={record.KiemKe4} arrow={true}>
                            <p className='text-center'>{record.KiemKe4.substring(0, 15).concat('...')}</p>
                          </Tooltip>
                        ) : (
                          <p className='text-center'>{record.KiemKe4}</p>
                        )
                      }
                    />
                  </ColumnGroup>
                  <ColumnGroup title='20…'>
                    <Column
                      width={150}
                      onCell={(record: BookColumsType) => ({
                        className:
                          currentStep === 3 &&
                          record?.cellError &&
                          record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('KiemKe5')) !== -1
                            ? 'min-h-38 errorBackGround py-2'
                            : '',
                        colSpan: record.STT !== undefined ? 1 : 0
                      })}
                      title='Mất'
                      dataIndex='KiemKe5'
                      key='KiemKe5'
                      render={(value: any, record: any) =>
                        record?.KiemKe5?.length > 15 ? (
                          <Tooltip placement='topLeft' title={record.KiemKe5} arrow={true}>
                            <p className='text-center'>{record.KiemKe5.substring(0, 15).concat('...')}</p>
                          </Tooltip>
                        ) : (
                          <p className='text-center'>{record.KiemKe5}</p>
                        )
                      }
                    />
                    <Column
                      width={150}
                      onCell={(record: BookColumsType) => ({
                        className:
                          currentStep === 3 &&
                          record?.cellError &&
                          record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('KiemKe6')) !== -1
                            ? 'min-h-38 errorBackGround py-2'
                            : '',
                        colSpan: record.STT !== undefined ? 1 : 0
                      })}
                      title='Còn'
                      dataIndex='KiemKe6'
                      key='KiemKe6'
                      render={(value: any, record: any) =>
                        record?.KiemKe6?.length > 15 ? (
                          <Tooltip placement='topLeft' title={record.KiemKe6} arrow={true}>
                            <p className='text-center'>{record.KiemKe6.substring(0, 15).concat('...')}</p>
                          </Tooltip>
                        ) : (
                          <p className='text-center'>{record.KiemKe6}</p>
                        )
                      }
                    />
                  </ColumnGroup>
                  <ColumnGroup title='20…'>
                    <Column
                      width={150}
                      onCell={(record: BookColumsType) => ({
                        className:
                          currentStep === 3 &&
                          record?.cellError &&
                          record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('KiemKe7')) !== -1
                            ? 'min-h-38 errorBackGround py-2'
                            : '',
                        colSpan: record.STT !== undefined ? 1 : 0
                      })}
                      title='Mất'
                      dataIndex='KiemKe7'
                      key='KiemKe7'
                      render={(value: any, record: any) =>
                        record?.KiemKe7?.length > 15 ? (
                          <Tooltip placement='topLeft' title={record.KiemKe7} arrow={true}>
                            <p className='text-center'>{record.KiemKe7.substring(0, 15).concat('...')}</p>
                          </Tooltip>
                        ) : (
                          <p className='text-center'>{record.KiemKe7}</p>
                        )
                      }
                    />
                    <Column
                      width={150}
                      onCell={(record: BookColumsType) => ({
                        className:
                          currentStep === 3 &&
                          record?.cellError &&
                          record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('KiemKe8')) !== -1
                            ? 'min-h-38 errorBackGround py-2'
                            : '',
                        colSpan: record.STT !== undefined ? 1 : 0
                      })}
                      title='Còn'
                      dataIndex='KiemKe8'
                      key='KiemKe8'
                      render={(value: any, record: any) =>
                        record?.KiemKe8?.length > 15 ? (
                          <Tooltip placement='topLeft' title={record.KiemKe8} arrow={true}>
                            <p className='text-center'>{record.KiemKe8.substring(0, 15).concat('...')}</p>
                          </Tooltip>
                        ) : (
                          <p className='text-center'>{record.KiemKe8}</p>
                        )
                      }
                    />
                  </ColumnGroup>
                  <ColumnGroup title='20…'>
                    <Column
                      width={150}
                      onCell={(record: BookColumsType) => ({
                        className:
                          currentStep === 3 &&
                          record?.cellError &&
                          record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('KiemKe9')) !== -1
                            ? 'min-h-38 errorBackGround py-2'
                            : '',
                        colSpan: record.STT !== undefined ? 1 : 0
                      })}
                      title='Mất'
                      dataIndex='KiemKe9'
                      key='KiemKe9'
                      render={(value: any, record: any) =>
                        record?.KiemKe9?.length > 15 ? (
                          <Tooltip placement='topLeft' title={record.KiemKe9} arrow={true}>
                            <p className='text-center'>{record.KiemKe9.substring(0, 15).concat('...')}</p>
                          </Tooltip>
                        ) : (
                          <p className='text-center'>{record.KiemKe9}</p>
                        )
                      }
                    />
                    <Column
                      width={150}
                      onCell={(record: BookColumsType) => ({
                        className:
                          currentStep === 3 &&
                          record?.cellError &&
                          record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('KiemKe10')) !== -1
                            ? 'min-h-38 errorBackGround py-2'
                            : '',
                        colSpan: record.STT !== undefined ? 1 : 0
                      })}
                      title='Còn'
                      dataIndex='KiemKe10'
                      key='KiemKe10'
                      render={(value: any, record: any) =>
                        record?.KiemKe10?.length > 15 ? (
                          <Tooltip placement='topLeft' title={record.KiemKe10} arrow={true}>
                            <p className='text-center'>{record.KiemKe10.substring(0, 15).concat('...')}</p>
                          </Tooltip>
                        ) : (
                          <p className='text-center'>{record.KiemKe10}</p>
                        )
                      }
                    />
                  </ColumnGroup>
                </ColumnGroup>
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('GhiChu')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Ghi chú'
                  dataIndex='GhiChu'
                  key='GhiChu'
                  render={(value: any, record: any) =>
                    record?.GhiChu?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.GhiChu} arrow={true}>
                        <p className='text-center'>{record.GhiChu.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.GhiChu}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('TacGia')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Tác giả chính'
                  dataIndex='TacGia'
                  key='TacGia'
                  render={(value: any, record: any) =>
                    record?.TacGia?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.TacGia} arrow={true}>
                        <p className='text-center'>{record.TacGia.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.TacGia}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('IdNguonCungCap')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Mã nguồn cung cấp'
                  dataIndex='IdNguonCungCap'
                  key='IdNguonCungCap'
                  render={(value: any, record: any) =>
                    record?.IdNguonCungCap?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.IdNguonCungCap} arrow={true}>
                        <p className='text-center'>{record.IdNguonCungCap.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.IdNguonCungCap}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('IdNhaXuatBan')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Nhà xuất bản'
                  dataIndex='IdNhaXuatBan'
                  key='IdNhaXuatBan'
                  render={(value: any, record: any) =>
                    record?.IdNhaXuatBan?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.IdNhaXuatBan} arrow={true}>
                        <p className='text-center'>{record.IdNhaXuatBan.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.IdNhaXuatBan}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('IdNoiXuatBan')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Nơi xuất bản'
                  dataIndex='IdNoiXuatBan'
                  key='IdNoiXuatBan'
                  render={(value: any, record: any) =>
                    record?.IdNoiXuatBan?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.IdNoiXuatBan} arrow={true}>
                        <p className='text-center'>{record.IdNoiXuatBan.substring(0, 10).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.IdNoiXuatBan}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('TenSongNgu')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Tên song ngữ'
                  dataIndex='TenSongNgu'
                  key='TenSongNgu'
                  render={(value: any, record: any) =>
                    record?.TenSongNgu?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.TenSongNgu} arrow={true}>
                        <p className='text-center'>{record.TenSongNgu.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.TenSongNgu}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('ISBN')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Mã ISBN'
                  dataIndex='ISBN'
                  key='ISBN'
                  render={(value: any, record: any) =>
                    record?.ISBN?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.ISBN} arrow={true}>
                        <p className='text-center'>{record.ISBN.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.ISBN}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('TacGiaPhu')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Tác giả phụ'
                  dataIndex='TacGiaPhu'
                  key='TacGiaPhu'
                  render={(value: any, record: any) =>
                    record?.TacGiaPhu?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.TacGiaPhu} arrow={true}>
                        <p className='text-center'>{record.TacGiaPhu.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.TacGiaPhu}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('IdMonHoc')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Môn học'
                  dataIndex='IdMonHoc'
                  key='IdMonHoc'
                  render={(value: any, record: any) =>
                    record?.IdMonHoc?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.IdMonHoc} arrow={true}>
                        <p className='text-center'>{record.IdMonHoc.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.IdMonHoc}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('KhoiLopString')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Khối lớp'
                  dataIndex='KhoiLopString'
                  key='KhoiLopString'
                  render={(value: any, record: any) =>
                    record?.KhoiLopString?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.KhoiLopString} arrow={true}>
                        <p className='text-center'>{record.KhoiLopString.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.KhoiLopString}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('IdChuDiem')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Chủ điểm'
                  dataIndex='IdChuDiem'
                  key='IdChuDiem'
                  render={(value: any, record: any) =>
                    record?.IdChuDiem?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.IdChuDiem} arrow={true}>
                        <p className='text-center'>{record.IdChuDiem.substring(0, 10).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.IdChuDiem}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('IdKeSach')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Kệ sách'
                  dataIndex='IdKeSach'
                  key='IdKeSach'
                  render={(value: any, record: any) =>
                    record?.IdKeSach?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.IdKeSach} arrow={true}>
                        <p className='text-center'>{record.IdKeSach.substring(0, 10).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.IdKeSach}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('IdThuMucSach')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Thư mục tài liệu'
                  dataIndex='IdThuMucSach'
                  key='IdThuMucSach'
                  render={(value: any, record: any) =>
                    record?.IdThuMucSach?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.IdThuMucSach} arrow={true}>
                        <p className='text-center'>{record.IdThuMucSach.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.IdThuMucSach}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('SoTrang')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Số trang'
                  dataIndex='SoTrang'
                  key='SoTrang'
                  render={(value: any, record: any) =>
                    record?.SoTrang?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.SoTrang} arrow={true}>
                        <p className='text-center'>{record.SoTrang.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.SoTrang}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('IdNgonNgu')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Ngôn ngữ'
                  dataIndex='IdNgonNgu'
                  key='IdNgonNgu'
                  render={(value: any, record: any) =>
                    record?.IdNgonNgu?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.IdNgonNgu} arrow={true}>
                        <p className='text-center'>{record.IdNgonNgu.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.IdNgonNgu}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('XuatXu')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Nước xuất xứ'
                  dataIndex='XuatXu'
                  key='XuatXu'
                  render={(value: any, record: any) =>
                    record?.XuatXu?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.XuatXu} arrow={true}>
                        <p className='text-center'>{record.XuatXu.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.XuatXu}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('NguoiBienDich')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Người biên dịch'
                  dataIndex='NguoiBienDich'
                  key='NguoiBienDich'
                  render={(value: any, record: any) =>
                    record?.NguoiBienDich?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.NguoiBienDich} arrow={true}>
                        <p className='text-center'>{record.NguoiBienDich.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.NguoiBienDich}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('TaiBan')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Lần xuất bản'
                  dataIndex='TaiBan'
                  key='TaiBan'
                  render={(value: any, record: any) =>
                    record?.TaiBan?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.TaiBan} arrow={true}>
                        <p className='text-center'>{record.TaiBan.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.TaiBan}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('SKU')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='SKU'
                  dataIndex='SKU'
                  key='SKU'
                  render={(value: any, record: any) =>
                    record?.SKU?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.SKU} arrow={true}>
                        <p className='text-center'>{record.SKU.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.SKU}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('LLC')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='LLC'
                  dataIndex='LLC'
                  key='LLC'
                  render={(value: any, record: any) =>
                    record?.LLC?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.LLC} arrow={true}>
                        <p className='text-center'>{record.LLC.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.LLC}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('IdMaMau')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Mã màu'
                  dataIndex='IdMaMau'
                  key='IdMaMau'
                  render={(value: any, record: any) =>
                    record?.IdMaMau?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.IdMaMau} arrow={true}>
                        <p className='text-center'>{record.IdMaMau.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.IdMaMau}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('PhiMuonSach')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Phí mượn sách'
                  dataIndex='PhiMuonSach'
                  key='PhiMuonSach'
                  render={(value: any, record: any) =>
                    record?.PhiMuonSach?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.PhiMuonSach} arrow={true}>
                        <p className='text-center'>{record.PhiMuonSach.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.PhiMuonSach}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('MoTaVatLy')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Mô tả vật lý'
                  dataIndex='MoTaVatLy'
                  key='MoTaVatLy'
                  render={(value: any, record: any) =>
                    record?.MoTaVatLy?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.MoTaVatLy} arrow={true}>
                        <p className='text-center'>{record.MoTaVatLy.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.MoTaVatLy}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('CoSach')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Khổ sách'
                  dataIndex='CoSach'
                  key='CoSach'
                  render={(value: any, record: any) =>
                    record?.CoSach?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.CoSach} arrow={true}>
                        <p className='text-center'>{record.CoSach.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.CoSach}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('TaiLieuDinhKem')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Tài liệu kèm theo'
                  dataIndex='TaiLieuDinhKem'
                  key='TaiLieuDinhKem'
                  render={(value: any, record: any) =>
                    record?.TaiLieuDinhKem?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.TaiLieuDinhKem} arrow={true}>
                        <p className='text-center'>{record.TaiLieuDinhKem.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.TaiLieuDinhKem}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('MinhHoa')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Minh họa'
                  dataIndex='MinhHoa'
                  key='MinhHoa'
                  render={(value: any, record: any) =>
                    record?.MinhHoa?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.MinhHoa} arrow={true}>
                        <p className='text-center'>{record.MinhHoa.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.MinhHoa}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('TungThu')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Tên tùng thư'
                  dataIndex='TungThu'
                  key='TungThu'
                  render={(value: any, record: any) =>
                    record?.TungThu?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.TungThu} arrow={true}>
                        <p className='text-center'>{record.TungThu.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.TungThu}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('PhuChu')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Phụ chú'
                  dataIndex='PhuChu'
                  key='PhuChu'
                  render={(value: any, record: any) =>
                    record?.PhuChu?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.PhuChu} arrow={true}>
                        <p className='text-center'>{record.PhuChu.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.PhuChu}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  onCell={(record: BookColumsType) => ({
                    className:
                      currentStep === 3 &&
                      record?.cellError &&
                      record?.cellError.indexOf(MacDinhThuTuKhoSach_SCB_SGK.indexOf('TomTat')) !== -1
                        ? 'min-h-38 errorBackGround py-2'
                        : '',
                    colSpan: record.STT !== undefined ? 1 : 0
                  })}
                  title='Tóm tắt'
                  dataIndex='TomTat'
                  key='TomTat'
                  render={(value: any, record: any) =>
                    record?.TomTat?.length > 15 ? (
                      <Tooltip placement='topLeft' title={record.TomTat} arrow={true}>
                        <p className='text-center'>{record.TomTat.substring(0, 15).concat('...')}</p>
                      </Tooltip>
                    ) : (
                      <p className='text-center'>{record.TomTat}</p>
                    )
                  }
                />
                <Column
                  width={150}
                  title={currentStep === 2 ? 'Hành động' : 'Nội dung lỗi'}
                  dataIndex='actions'
                  key='actions'
                  render={(value: any, record: any) => {
                    return (
                      <>
                        {currentStep === 2 && (
                          <button
                            className='mx-2'
                            onClick={(e) => {
                              e.stopPropagation();
                              handleDeleteRow(record.STT + '_' + record.TenSach);
                            }}
                          >
                            <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
                          </button>
                        )}

                        {currentStep === 3 && <p className='text-danger-10'>{record.ListError?.join(', ')}</p>}
                      </>
                    );
                  }}
                />
              </Table>
            </div>
          )}
        </div>
      )}

      <div className='mr-10 mt-6 flex items-center justify-end'>
        {currentStep === 1 && (
          <>
            <Button
              variant='secondary'
              onClick={() => {
                navigate(path.khosach);
              }}
            >
              Quay về
            </Button>
          </>
        )}

        <>
          {currentStep === 2 && !isLoadingPreviewImport && (
            <>
              <Button variant='secondary' onClick={handleBackCurrent}>
                Quay về
              </Button>
              <Button variant='default' className='ml-2' onClick={handleSubmit}>
                Tiếp tục
              </Button>
            </>
          )}

          {currentStep === 3 && !isLoadingImportSave && (
            <Button variant='default' className='ml-2' onClick={handleBack}>
              Hoàn tất
            </Button>
          )}
        </>
      </div>
      <Loading open={isLoading || isLoadingTemplate || isLoadingPreviewImport || isLoadingImportSave} />
    </div>
  );
};

export default AddBookSCBSGKByExcel;
