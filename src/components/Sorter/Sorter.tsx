import { CloseCircleOutlined } from '@ant-design/icons';
import classNames from 'classnames';
import { Button } from 'components/Button';
import { DatePicker } from 'components/DatePicker';
import { Input } from 'components/Input';
import { OPERATORS } from 'constants/options';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import { usePaginationNavigate, useQueryParams } from 'hooks';
import { isEmpty, merge, omitBy } from 'lodash';
import { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { CSSProperties } from 'react';

dayjs.extend(utc);

type Props = {
  column: Option;
  type?: 'text' | 'date' | 'range';
  handleEnter: VoidFunction;
  enabledOptions?: Array<'equals' | 'contains' | 'startWiths' | 'endWiths' | 'isEmpty' | 'inNotEmpty'>;
  style?: CSSProperties;
};

const Sorter = (props: Props) => {
  const {
    column: { label, value = '' },
    type = 'text',
    enabledOptions = [],
    handleEnter,
    style
  } = props;

  const { register, handleSubmit, setValue, watch, resetField, setFocus, control } = useForm({
    mode: 'onSubmit'
  });

  const _navigate = usePaginationNavigate();

  let queryParams = useQueryParams();

  useEffect(() => {
    if (!queryParams) {
      return;
    }

    if (queryParams.hasOwnProperty(`${value}Operator`)) {
      setValue(`${value}Operator`, queryParams[`${value}Operator`]);
    }

    if (queryParams.hasOwnProperty(`${value}RangeMin` && `${value}RangeMax`)) {
      setValue(`${value}RangeMin`, queryParams[`${value}RangeMin`]);
      setValue(`${value}RangeMax`, queryParams[`${value}RangeMax`]);
    }

    if (!value) {
      setValue(value as string, '');

      _navigate();
    }

    setValue(value as string, queryParams[`${value}`]);
    setFocus(value as string, { shouldSelect: true });
  }, [_navigate, queryParams, setFocus, setValue, type, value]);

  const startDateValue = watch(`${value}Min`);
  const endDateValue = watch(`${value}Max`);

  useEffect(() => {
    if (!Boolean(startDateValue)) setValue(`${value}Max`, null);
  }, [startDateValue, endDateValue, value, setValue]);

  const onSubmit = handleSubmit((data) => {
    handleEnter();

    if (!data[value]) {
      resetField(`${value}Operator` as string);
      _navigate();
    }

    let obj1 = {};
    if (type === 'date') {
      let obj = {
        [`${value}Min`]: dayjs(data[`${value}Min`]).startOf('day').utc().format(),
        [`${value}Max`]: dayjs(data[`${value}Max`]).endOf('day').utc().format()
      };

      if (!Boolean(endDateValue)) {
        setValue(`${value}Max`, startDateValue);
      }

      obj1 = obj;
    }

    queryParams = { ...queryParams, ...data };

    const paramsMerged = merge(queryParams, data, obj1);

    return _navigate(omitBy(paramsMerged, isEmpty));
  });

  const handleSetValue = (name: string) => () => setValue(name, '');

  return (
    <form
      className='absolute top-9 z-20 flex transform rounded-md bg-white p-2 shadow-lg'
      onSubmit={onSubmit}
      style={style}
    >
      <button onClick={handleEnter} type='button'>
        <CloseCircleOutlined style={{ color: 'black', position: 'absolute', right: 0, top: 0 }} />
      </button>

      <div className='flex w-[100px] flex-col items-center justify-start gap-1'>
        <span className='text-left text-xs text-gray-400'>Cột</span>
        <span className='text-black'>{label}</span>
      </div>

      {type === 'text' && (
        <>
          <div className='flex flex-col items-center justify-start gap-1'>
            <span className='text-left text-xs text-gray-400 '>Phương thức</span>

            <select className='text-black outline-none' {...register(`${value}Operator`)}>
              {OPERATORS.map(({ value, label }) => (
                // @ts-ignore
                <option value={value} key={value} disabled={!enabledOptions?.includes(value)}>
                  {label}
                </option>
              ))}
            </select>
          </div>

          <div className='flex flex-col items-center justify-start gap-1'>
            <span className='text-left text-xs text-gray-400'>Giá trị</span>

            <Input
              type='text'
              className='border-b-2 text-black outline-none'
              placeholder='Nhập giá trị'
              name={value + ''}
              register={register}
            />
          </div>
        </>
      )}

      {type === 'range' && (
        <>
          <div className='mr-2 ml-2 flex flex-col items-center gap-1'>
            <span className='text-left text-xs text-gray-400'>Từ</span>

            <Input
              type='number'
              containerClassName='flex item-center border-b-2 relative'
              className=' text-black outline-none'
              placeholder='Từ'
              name={value + 'RangeMin'}
              register={register}
              right={
                <button
                  onClick={handleSetValue(value + 'RangeMin')}
                  type='button'
                  className={classNames('absolute right-0 bottom-1 transition-all delay-100 duration-200 ease-linear', {
                    'opacity-0': !watch(value + 'RangeMin'),
                    'opacity-40': watch(value + 'RangeMin')
                  })}
                >
                  <CloseCircleOutlined style={{ color: 'black' }} />
                </button>
              }
            />
          </div>

          <div className='flex flex-col items-center justify-start gap-1'>
            <span className='text-left text-xs text-gray-400'>Đến</span>

            <Input
              type='number'
              containerClassName='flex item-center border-b-2 relative'
              className=' text-black outline-none'
              placeholder='Đến'
              name={value + 'RangeMax'}
              register={register}
              right={
                <button
                  onClick={handleSetValue(value + 'RangeMax')}
                  type='button'
                  className={classNames('absolute right-0 bottom-1 transition-all delay-100 duration-200 ease-linear', {
                    'opacity-0': !watch(value + 'RangeMax'),
                    'opacity-40': watch(value + 'RangeMax')
                  })}
                >
                  <CloseCircleOutlined style={{ color: 'black' }} />
                </button>
              }
            />
          </div>

          <Button
            type='button'
            onClick={onSubmit}
            className={classNames('ml-2 shrink-0 transition-all delay-100 duration-200 ease-linear', {
              'invisible opacity-0': !Boolean(watch(value + 'RangeMax') && Boolean(watch(value + 'RangeMin'))),
              visible: Boolean(watch(value + 'RangeMax') && Boolean(watch(value + 'RangeMin')))
            })}
          >
            Áp dụng
          </Button>
        </>
      )}

      {type === 'date' && (
        <div className='flex w-[350px] flex-col items-center justify-start gap-1'>
          <span className='text-left text-xs text-gray-400'>Ngày bắt đầu - Ngày kết thúc</span>

          <div className='flex flex-grow items-center justify-center gap-1'>
            <DatePicker name={`${value}Min`} placeholder='Chọn ngày bắt đầu' control={control} />

            <DatePicker
              name={`${value}Max`}
              placeholder='Chọn ngày kết thúc'
              control={control}
              disabledDate={(current) => {
                return (current && current) < dayjs(startDateValue).startOf('day');
              }}
              disabled={!Boolean(startDateValue)}
            />

            <Button
              type='button'
              onClick={onSubmit}
              variant={Boolean(endDateValue) ? 'primary-outline' : 'disabled'}
              className='shrink-0 '
              disabled={!Boolean(endDateValue)}
            >
              Áp dụng
            </Button>
          </div>
        </div>
      )}
    </form>
  );
};

export default Sorter;
