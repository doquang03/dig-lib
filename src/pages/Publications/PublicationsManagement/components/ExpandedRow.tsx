import { Tooltip } from 'antd';
import { Button } from 'components';
import { useLanguages } from 'hooks';
import { useMemo } from 'react';
import { useNavigate } from 'react-router-dom';

type Props = {
  book: Book;
  options:
    | {
        optionsBST: Selections;
        optionsChuDiem: Selections;
        optionsMonHoc: Selections;
        optionsNXB: Selections;
        optionsTacGia: Selections;
        optionsTMS: Selections;
      }
    | undefined;
};

const ExpandedRow = (props: Props) => {
  const {
    book: {
      Id,
      ISBN,
      NamXuatBan,
      SoLuongTong,
      Ten_NhaXuatBan,
      SoLuongConLai,
      LinkBiaSach,
      IdBoSuuTap,
      IdThuMucSach,
      Id_TacGia,
      Ten_NgonNgu
    },

    options
  } = props;

  const { data: languageData } = useLanguages();

  const collection = options?.optionsBST?.find(({ value }) => value === IdBoSuuTap)?.label || '--';
  const authors = options?.optionsTacGia?.filter(({ value }) => Id_TacGia?.includes(value + ''));
  const nhaXuatban =
    options?.optionsNXB?.find(({ value }) => value === Ten_NhaXuatBan)?.value !== ''
      ? options?.optionsNXB?.find(({ value }) => value === Ten_NhaXuatBan)?.label
      : '--';

  const document =
    options?.optionsTMS?.find(({ value }) => value === IdThuMucSach)?.value !== ''
      ? options?.optionsTMS?.find(({ value }) => value === IdThuMucSach)?.label
      : '--';

  const language = useMemo(() => {
    if (!languageData?.length) return;

    return languageData?.find(({ Id }) => Id === Ten_NgonNgu)?.Ten || '--';
  }, [Ten_NgonNgu, languageData]);

  const navigate = useNavigate();

  const handleUpdateBook = (id: string) => navigate(`CapNhat/${id}`);

  return (
    <>
      <div className='flex items-start justify-around gap-5'>
        <div className='w-1/3'>
          <img src={LinkBiaSach || '/content/Book.png'} alt='Bìa sách' className='ml-auto mr-auto block w-[150px]' />
        </div>
        <div className='flex w-1/2 flex-col items-start gap-2'>
          <p className='m-0 font-semibold'>
            ISBN: <span className='font-bold'>{ISBN || '--'}</span>
          </p>
          <div className='w-full border-[0.5px] border-dashed border-gray-500/80' />
          <p className='m-0 font-semibold'>
            Bộ sưu tập:{' '}
            {collection && collection.length > 40 ? (
              <Tooltip placement='topLeft' title={collection} arrow={true}>
                <span className=' font-bold'>{collection.substring(0, 40).concat('...')}</span>
              </Tooltip>
            ) : (
              <span className=' font-bold'>{collection}</span>
            )}
          </p>
          <div className='w-full border-[0.5px] border-dashed border-gray-500/80' />
          <p className='m-0 font-semibold'>
            Tác giả:{' '}
            {authors?.length ? (
              authors?.length > 2 ? (
                <Tooltip
                  title={authors?.map(({ label }) => (
                    <p key={label}>{label}</p>
                  ))}
                >
                  {authors?.slice(0, 2).map((author, index) => (
                    <span className='font-bold' key={index}>
                      {author.label}
                      {authors.length - 1 === index ? '' : ', '}
                    </span>
                  ))}
                  ...
                </Tooltip>
              ) : (
                authors?.map((author, index) => (
                  <span className='font-bold' key={index}>
                    {author.label}
                    {authors.length - 1 === index ? '' : ', '}
                  </span>
                ))
              )
            ) : (
              <span className='font-bold'>--</span>
            )}
          </p>
          <div className='w-full border-[0.5px] border-dashed border-gray-500/80' />
          <p className='m-0 font-semibold'>
            Thư mục tài liệu: <span className='font-bold'>{document}</span>
          </p>
        </div>

        <div className='flex w-1/2 flex-col items-start gap-2'>
          <p className='m-0 font-semibold'>
            Ngôn ngữ: <span className='font-bold'>{language}</span>
          </p>
          <div className='w-full border-[0.5px] border-dashed border-gray-500/80' />
          <p className='m-0 font-semibold'>
            Năm xuất bản: <span className='font-bold'>{NamXuatBan}</span>
          </p>
          <div className='w-full border-[0.5px] border-dashed border-gray-500/80' />
          <p className='m-0 font-semibold'>
            Nhà xuất bản: <span className='font-bold'>{nhaXuatban}</span>
          </p>
          <div className='w-full border-[0.5px] border-dashed border-gray-500/80' />

          <div className='flex items-center gap-9'>
            <p className='font-semibold'>
              Tổng: <span className='font-bold'>{SoLuongTong}</span>
            </p>
            <p className='font-semibold'>
              Kho: <span className='font-bold'>{SoLuongConLai}</span>
            </p>
          </div>
        </div>
      </div>

      <div className='flex flex-row justify-end'>
        <Button variant='primary' onClick={() => handleUpdateBook(Id + '')}>
          Xem chi tiết
        </Button>
      </div>
    </>
  );
};

export default ExpandedRow;
