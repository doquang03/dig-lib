import { TabsProps } from 'antd';
import { Tabs, Title } from 'components';
import { useState } from 'react';
import ReaderStatisticsTable from './components/ReaderStatisticsTable';

const ReaderStatistics = () => {
  const [activeTab, setActiveTab] = useState<string>('month');

  const items: TabsProps['items'] = [
    {
      key: 'month',
      label: `Thống kê tháng`,
      children: <ReaderStatisticsTable picker='month' />,
      forceRender: true
    },
    {
      key: 'day',
      label: `Thống kê ngày`,
      children: <ReaderStatisticsTable picker='date' />,
      forceRender: false
    }
  ];

  const handleChangeTab = (activeTab: string) => setActiveTab(activeTab);

  return (
    <div className='p-5'>
      <Title title='Thống kê bạn đọc' />

      <div className='mt-3'>
        <Tabs items={items} activeKey={activeTab} onChange={handleChangeTab} destroyInactiveTabPane={true} />
      </div>
    </div>
  );
};

export default ReaderStatistics;
