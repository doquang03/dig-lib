import { Table, Tooltip } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { DeleteFilled, FileSyncOutlined, DownloadOutlined } from '@ant-design/icons';
import { Button, Loading, ModalDelete, Title, TitleDelete } from 'components';
import { useMemo, useRef, useState } from 'react';
import { useMutation, useQuery } from '@tanstack/react-query';
import { settingApis } from 'apis';
import { toast } from 'react-toastify';
import { uniqueId } from 'lodash';
import { useUser } from 'contexts/user.context';

const BackUpPage = () => {
  const [visiableRestore, setVisiableRestore] = useState(false);
  const [visiableDelete, setVisiableDelete] = useState(false);
  const [visiableConvert, setVisiableConvert] = useState(false);
  const [nameFile, setNameFile] = useState('');
  const fileInutRef = useRef<HTMLInputElement | null>(null);
  const fileInutRefOld = useRef<HTMLInputElement | null>(null);

  const { userType, isAllowedAdjustment } = useUser();

  const {
    data,
    isFetching: isLoading,
    refetch
  } = useQuery({
    queryKey: ['GetBackupFiles'],
    queryFn: () => settingApis.GetBackupFiles(),
    onSuccess(data) {
      setVisiableRestore(false);
      setVisiableDelete(false);
      setNameFile('');
    }
  });

  const { data: dataDay, isLoading: isLoadingDay } = useQuery({
    queryKey: ['GetDayBackup'],
    queryFn: () => settingApis.GetDayBackup(),
    onSuccess(data) {
      setVisiableRestore(false);
      setVisiableDelete(false);
      setNameFile('');
    }
  });

  const { mutate, isLoading: isLoadingCreate } = useMutation({
    mutationFn: settingApis.CreateBackupFile,
    mutationKey: ['CreateBackup'],
    onSuccess: () => {
      refetch();
      toast.success('Tạo sao lưu thành công thành công dữ liệu');
    }
  });

  const { mutate: mutateRestore, isLoading: isLoadingRestore } = useMutation({
    mutationFn: () => settingApis.RestoreBackupFile(nameFile),
    mutationKey: ['RestoreBackup'],
    onSuccess: () => {
      refetch();
      setVisiableRestore(false);
      toast.success('Phục hồi thành công dữ liệu');
    }
  });

  const { mutate: mutateDelete, isLoading: isLoadingDelete } = useMutation({
    mutationFn: () => settingApis.RemoveBackupFile(nameFile),
    mutationKey: ['RemoveBackupFile'],
    onSuccess: () => {
      refetch();
      setVisiableDelete(false);
      toast.success('Xóa thành công dữ liệu');
    }
  });

  const { mutate: mutateDownload, isLoading: isLoadingDownload } = useMutation({
    mutationFn: (name: string) => settingApis.DownloadBackupFile(name),
    mutationKey: ['DownloadBackup'],
    onSuccess: (data, name) => {
      refetch();
      toast.success('Tải sổ tổng quát thành công');
      const blob = new Blob([data.data, { type: 'application/xls' }]);
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = name;
      link.click();
    }
  });

  const { mutate: UploadBackUp, isLoading: isLoadingUpLoad } = useMutation({
    mutationFn: (file: File) => {
      let bodyFormData = new FormData();
      bodyFormData.append('file', file as Blob);
      return settingApis.UploadBackupFile(bodyFormData);
    },
    onSuccess: (data, name) => {
      refetch();
      toast.success('Upload file dữ liệu thành công');
    }
  });

  const { mutate: UploadBackUpOld, isLoading: isLoadingUpLoadOld } = useMutation({
    mutationFn: (file: File) => {
      let bodyFormData = new FormData();
      bodyFormData.append('file', file as Blob);
      return settingApis.UploadBackupFileOld(bodyFormData);
    },
    onSuccess: (data, name) => {
      refetch();
      toast.success('Phục hồi dữ liệu từ file thư viện cũ thành công');
    }
  });

  const { mutate: ConvertDataOld, isLoading: isLoadingConvertDataOld } = useMutation({
    mutationFn: () => settingApis.ConvertDataOld(),
    onSuccess: (data, name) => {
      setVisiableConvert(false);
      toast.success('Chuyển dữ liệu sang thư viện số thành công');
    },
    onError(error, variables, context) {
      setVisiableConvert(false);
    }
  });

  const handleOnFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files?.[0];
    UploadBackUp(fileFromLocal as File);
  };

  const handleOnFileChangeOld = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files?.[0];
    UploadBackUpOld(fileFromLocal as File);
  };

  const handleAccessFileInputRef = () => {
    fileInutRef.current?.click();
  };

  const handleAccessFileInputRefOld = () => {
    fileInutRefOld.current?.click();
  };

  const columns: ColumnsType<any> = useMemo(() => {
    return [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        className: 'min-content',
        render: (value, record, index) => {
          return index + 1;
        }
      },
      {
        title: 'Tên file',
        dataIndex: 'Name',
        key: 'Name'
      },
      {
        title: 'Kích thước',
        dataIndex: 'Size',
        key: 'Size'
      },
      {
        title: 'Thời gian cập nhật',
        dataIndex: 'Date',
        key: 'Date'
      },
      {
        title: 'Hành động',
        dataIndex: 'actions',
        key: 'actions',
        className: 'min-content',
        render: (value, record) => {
          return (
            <>
              <Tooltip title='Tải về'>
                <button
                  className='mx-2'
                  onClick={(e) => {
                    e.stopPropagation();
                    mutateDownload(record.Name);
                  }}
                >
                  <DownloadOutlined style={{ fontSize: '20px', color: '#08c' }} />
                </button>
              </Tooltip>
              <Tooltip title='Phục hồi'>
                <button
                  className='mx-2'
                  onClick={(e) => {
                    e.stopPropagation();
                    setNameFile(record.Name);
                    setVisiableRestore(true);
                  }}
                >
                  <FileSyncOutlined style={{ fontSize: '20px', color: '#08c' }} />
                </button>
              </Tooltip>
              <Tooltip title='Xóa'>
                <button
                  className='mx-2'
                  onClick={(e) => {
                    e.stopPropagation();
                    setNameFile(record.Name);
                    setVisiableDelete(true);
                  }}
                >
                  <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
                </button>
              </Tooltip>
            </>
          );
        }
      }
    ];
  }, [mutateDownload]);

  const listBackUp = useMemo(() => {
    if (!data?.data.Item) return [];
    return data?.data.Item;
  }, [data?.data.Item]);

  if (userType === 2001 && !isAllowedAdjustment) {
    return <h2 className='p-2 '>Bạn không có quyền truy cập chức năng này</h2>;
  }

  let locale = {
    emptyText: 'Không có dữ liệu backup!'
  };

  return (
    <div className='p-5'>
      <Title title='Sao lưu và phục hồi' />
      <div className='my-4 flex-col flex-wrap gap-1 md:flex md:md:flex-row'>
        <Button
          type='button'
          variant='default'
          className='font-semibold'
          loading={isLoadingCreate}
          onClick={() => mutate()}
        >
          Tạo sao lưu
        </Button>
        <input
          type='file'
          accept='.zip,.rar,.7zip'
          className='hidden'
          ref={fileInutRef}
          onChange={handleOnFileChange}
        />
        <input
          type='file'
          accept='.zip,.rar,.7zip'
          className='hidden'
          ref={fileInutRefOld}
          onChange={handleOnFileChangeOld}
        />
        <Button
          type='button'
          variant='default'
          className='font-semibold'
          onClick={() => handleAccessFileInputRef()}
          loading={isLoadingUpLoad}
        >
          Upload file
        </Button>
        <Button type='button' variant='default' className='font-semibold' onClick={() => handleAccessFileInputRefOld()}>
          Phục hồi dữ liệu từ file thư viện cũ
        </Button>
        <Button type='button' variant='default' className='font-semibold' onClick={() => setVisiableConvert(true)}>
          Chuyển dữ liệu sang thư viện số
        </Button>
      </div>
      <p className='mt-3 text-base text-danger-10'>{`Lưu ý: Số lượng file cho phép là ${
        listBackUp?.length
      }/3. File backup sẽ tự động xóa trong vòng ${dataDay?.data?.Item?.Days || 14} ngày`}</p>
      <div className='mt-6'>
        <Table
          loading={isLoading}
          columns={columns}
          dataSource={listBackUp || []}
          pagination={{ defaultPageSize: 10, hideOnSinglePage: true, showSizeChanger: false }}
          scroll={{ x: true }}
          rowKey={() => uniqueId()}
          className='custom-table'
          locale={locale}
        />
      </div>
      <ModalDelete
        labelOK='Xác nhận'
        className='model-delete'
        open={visiableRestore}
        closable={false}
        loading={isLoadingRestore || isLoading}
        handleCancel={() => {
          setVisiableRestore(false);
        }}
        handleOk={() => mutateRestore()}
        title={<TitleDelete firstText={`Bạn xác nhận phục hồi dữ liệu từ file ${nameFile}`}></TitleDelete>}
      ></ModalDelete>
      <ModalDelete
        labelOK='Xác nhận'
        className='model-delete'
        open={visiableConvert}
        closable={false}
        loading={isLoadingConvertDataOld || isLoading}
        handleCancel={() => {
          setVisiableConvert(false);
        }}
        handleOk={() => ConvertDataOld()}
        title={<TitleDelete firstText={`Bạn xác nhận chuyển dữ liệu sang thư viện số?`} thirdText=''></TitleDelete>}
      ></ModalDelete>
      <ModalDelete
        labelOK='Xác nhận'
        className='model-delete'
        open={visiableDelete}
        closable={false}
        loading={isLoadingDelete || isLoading}
        handleCancel={() => {
          setVisiableDelete(false);
        }}
        handleOk={() => mutateDelete()}
        title={<TitleDelete firstText={`Bạn xác nhận xóa dữ liệu file ${nameFile}?`}></TitleDelete>}
      ></ModalDelete>
      <Loading open={isLoadingDownload || isLoadingDay || isLoadingUpLoadOld || isLoadingConvertDataOld}></Loading>
    </div>
  );
};

export default BackUpPage;
