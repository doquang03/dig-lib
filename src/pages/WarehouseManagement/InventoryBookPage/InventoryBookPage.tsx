import { DeleteFilled } from '@ant-design/icons';
import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery } from '@tanstack/react-query';
import { bookApis, receiptApis } from 'apis';
import { Button, DatePicker, Input, Loading, Select, Title } from 'components';
import { AutoComplete } from 'components/AutoComplete';
import { path } from 'constants/path';
import { UserConText } from 'contexts/user.context';
import dayjs from 'dayjs';
import { useContext, useEffect, useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useLocation, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { taoSoNhapKhoSchema } from 'utils/rules';
import * as yup from 'yup';

type NhapKhoForm = yup.InferType<typeof taoSoNhapKhoSchema>;

type Book = SachCaBiet & {
  TenSach: string;
  MaKiemSoat: string;
  MaCaBiet: string;
  GiaBia: string;
  TrangThai: string;
};

const InventoryBookPage = () => {
  const [booksResult, setBooksResult] = useState<Book[]>([]);

  const user = useContext(UserConText);

  const { state } = useLocation();

  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    reset,
    setValue,
    control,
    formState: { errors }
  } = useForm<NhapKhoForm>({
    resolver: yupResolver(taoSoNhapKhoSchema)
  });

  const { data: optionsData, isLoading: isLoadingForm } = useQuery({
    queryKey: ['GetInitCreateForm'],
    queryFn: bookApis.GetInitCreateForm
  });

  useEffect(() => {
    if (!state.selectedSCB.length) {
      return;
    }

    setBooksResult(state.selectedSCB);
    setValue('KhoSach', state.selectedSCB?.[0]?.IdKho);

    state.selectedSCB.forEach((item: Book, index: number) =>
      setValue(`trangThai.${index}.TrangThai`, item.IdTrangThai)
    );
  }, [setValue, state.selectedSCB]);

  const { mutate: saveSoNhapKho, isLoading: isLoadingSaveSoNhapKho } = useMutation({
    mutationFn: receiptApis.saveSoNhapKho,
    onSuccess: () => {
      reset();
      toast.success('Tạo phiếu nhập kho thành công');
      navigate(path.quanlynhapkho);
    }
  });

  const options = useMemo(() => {
    if (!optionsData?.data.Item) return;

    const { Options_KhoSach, Options_TrangThaiSach, Options_NguonCungCap } = optionsData?.data.Item;

    const khoSach = [{ label: 'Chọn kho sách', value: '' }, ...Options_KhoSach];
    const trangThai = [{ label: 'Chọn tình trạng', value: '' }, ...Options_TrangThaiSach];
    const nguonCungCap = [{ label: 'Chọn nguồn cung cấp', value: '' }, ...Options_NguonCungCap];

    return { khoSach, trangThai, nguonCungCap };
  }, [optionsData?.data.Item]);

  const handleDelete = (index: number) => () => {
    const array = [...booksResult];
    array.splice(index, 1);
    setBooksResult(array);
  };

  const handleBack = () => navigate(path.quanlynhapkho);

  const onSubmit = handleSubmit((data) => {
    if (!data.trangThai?.length) {
      return;
    }

    saveSoNhapKho({
      IdUserAdmin: user?.profile?.Id + '',
      UserName: user?.profile?.UserName + '',
      GhiChu: data.GhiChu + '',
      IdKho: data.KhoSach + '',
      ListSoNhapXuatKho: data.trangThai.map((book, index) => {
        return {
          Id: booksResult[index].Id,
          IdSach: booksResult[0].IdSach + '',
          IdKho: booksResult[index].IdKho + '',
          TenSach: booksResult[index].TenSach + '',
          MaKSCB: booksResult[index].MaCaBiet + '',
          IdNguonCungCap: data.NguonCungCap + '',
          NgayVaoSo: new Date(data.NgayNhap),
          SoChungTu: data.SoCT + '',
          IdTrangThai: book.TrangThai + '',
          SoVaoSoTongQuat: data.VSTQ + ''
        };
      })
    });
  });

  console.log('booksResult', booksResult);

  return (
    <div className='p-5'>
      <Title title='Tạo phiếu nhập kho' />

      <div className='mt-3 grid grid-cols-1 md:grid-cols-12'>
        <div className='col-span-6 flex flex-col gap-2 px-2'>
          <label className='font-bold'>
            Kho sách <span className='text-danger-10'>*</span>
          </label>

          <AutoComplete
            className='w-full grow'
            name={'KhoSach'}
            placeholder='Nhập kho'
            control={control}
            items={options?.khoSach || []}
            disabled={true}
            // errorMessage={errors.KhoSach?.message}
            showSearch
          />

          <label className='font-bold'>Số chứng từ</label>
          <Input
            placeholder='CT12/2'
            name={`SoCT` as const}
            register={register}
            errorMessage={errors?.SoCT?.message}
            maxLength={100}
          />

          <label className='font-bold'>
            Ngày vào sổ <span className='text-danger-10'>*</span>
          </label>
          <DatePicker
            control={control}
            name='NgayNhap'
            errorMessage={errors?.NgayNhap?.message}
            disabledDate={(current) => {
              return current && current > dayjs().endOf('day');
            }}
          />
        </div>

        <div className='col-span-6 flex flex-col gap-2 px-2'>
          <label className='font-bold'>
            Nguồn cung cấp <span className='text-danger-10'>*</span>
          </label>

          <Select
            items={options?.nguonCungCap || []}
            name={`NguonCungCap` as const}
            className='w-full'
            register={register}
            errorMessage={errors?.NguonCungCap?.message}
          />

          <label className='font-bold'>Số vào sổ tổng quát</label>

          <Input
            placeholder='Số vào sổ tổng quát'
            name={`VSTQ` as const}
            register={register}
            errorMessage={errors?.VSTQ?.message}
            maxLength={100}
          />

          <label className='font-bold'>Ghi chú</label>

          <Input placeholder='Nhập ghi chú' name={`GhiChu` as const} register={register} maxLength={100} />
        </div>
      </div>

      <div className='relative mt-3'>
        <h3 className='font-bold text-primary-10'>Danh sách sách cá biệt nhập kho</h3>

        {!!booksResult.length && (
          <table className='mt-5 table'>
            <thead className='bg-primary-10 text-white'>
              <tr>
                <th className='text-center'>STT</th>
                <th className='text-center'>Mã kiểm soát</th>
                <th className='text-center'>Số đăng kí cá biệt</th>
                <th className='text-center'>Tên sách</th>
                <th className='text-center'>Tình trạng</th>
                <th className='text-center'>Giá tiền</th>
                <th className='text-center'></th>
              </tr>
            </thead>

            <tbody className='bg-white'>
              {booksResult.map((row, index) => {
                return (
                  <tr key={row.Id} className='text-center'>
                    <td>{index + 1}</td>
                    <td className='w-1/12'>{row.MaKiemSoat}</td>
                    <td className='w-1/12'>{row.MaKSCB}</td>
                    <td>{row.TenSach}</td>
                    <td>
                      <Select
                        items={options?.trangThai || []}
                        name={`trangThai.${index}.TrangThai` as const}
                        register={register}
                        errorMessage={errors?.trangThai?.[index]?.TrangThai?.message}
                      />
                    </td>
                    <td>
                      {new Intl.NumberFormat('vi-VN', {
                        style: 'currency',
                        currency: 'VND',
                        maximumFractionDigits: 9
                      }).format(+row.GiaBia)}
                    </td>

                    <td>
                      <button className='mx-2' onClick={handleDelete(index)}>
                        <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        )}
      </div>

      <div className='flex items-center justify-end gap-3'>
        <Button onClick={handleBack} variant='secondary'>
          Quay về
        </Button>

        <Button onClick={onSubmit}>Thêm mới</Button>
      </div>

      <Loading open={isLoadingForm || isLoadingSaveSoNhapKho} />
    </div>
  );
};

export default InventoryBookPage;
