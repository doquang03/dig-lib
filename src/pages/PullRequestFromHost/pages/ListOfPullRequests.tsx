import { TabsProps } from 'antd';
import { Tabs, Title } from 'components';
import { useState } from 'react';
import { MediaTab, NormalTab } from '../components';

const ListOfPullRequests = () => {
  const [activeKey, setActiveKey] = useState<'normal' | 'media'>('normal');

  const items: TabsProps['items'] = [
    { key: 'normal', label: 'Tài liệu', children: <NormalTab /> },
    { key: 'media', label: 'Đa phương tiện', children: <MediaTab /> }
  ];

  return (
    <div className='p-5'>
      <Title title='ĐỒNG BỘ DỮ LIỆU MỤC LỤC LIÊN HỢP' />

      <div className=' mt-3'>
        <Tabs activeKey={activeKey} items={items} onChange={(key) => setActiveKey(key as 'normal' | 'media')} />
      </div>
    </div>
  );
};

export default ListOfPullRequests;
