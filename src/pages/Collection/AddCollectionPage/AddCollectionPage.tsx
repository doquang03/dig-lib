import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery } from '@tanstack/react-query';
import { collectionApis } from 'apis';
import { AxiosError } from 'axios';
import { Button, Input, Loading, Title } from 'components';
import { path } from 'constants/path';
import { useEffect, useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { collectionSchema } from 'utils/rules';
import * as yup from 'yup';
type collectionForm = yup.InferType<typeof collectionSchema>;

const AddCollectionPage = () => {
  const navigate = useNavigate();

  const { collectionId } = useParams();

  const [currentSchema, setCurrentSchema] = useState(collectionSchema);
  const [duplicateCollection, setDuplicateCollection] = useState<Array<string>>([]);
  const [isTrigger, setIsTrigger] = useState('idle');

  const {
    register,
    handleSubmit,
    trigger,
    setValue,
    formState: { errors }
  } = useForm<collectionForm>({
    defaultValues: {
      name: ''
    },
    resolver: yupResolver(currentSchema)
  });

  const { isFetching } = useQuery({
    queryKey: ['BoSuuTapEditByID', collectionId],
    queryFn: () => collectionApis.EditByID(collectionId as string),
    onSuccess: (res) => {
      setValue('name', res?.data?.Item?.Name);
    },
    enabled: !!collectionId
  });

  const { mutate: handleCreate, isLoading: isLoadingCreate } = useMutation({
    mutationFn: (payload: object) => collectionApis.Create(payload),
    onSuccess: () => {
      toast.success('Thêm bộ sưu tập thành công');
      navigate(path.bosuutap);
    },
    onError: (
      ex: AxiosError<
        ResponseApi<{
          Collection: string;
        }>
      >
    ) => {
      if (ex?.response?.data?.ExMessage === 'Collection') {
        let tempCollection = duplicateCollection.concat([ex?.response?.data?.Item?.Collection]);
        setDuplicateCollection(tempCollection);
        let collectionSchemaClone = collectionSchema.clone();
        let collectionSchemaObject = yup.object({
          name: yup
            .string()
            .trim()
            .notOneOf(tempCollection, ex?.response?.data?.Message)
            .required('Bộ sưu tập không được bỏ trống.')
        });
        collectionSchemaClone = collectionSchemaClone.concat(collectionSchemaObject);
        setCurrentSchema(collectionSchemaClone);
        setIsTrigger('name');
      }
    }
  });

  const { mutate: handleEdit, isLoading: isLoadingEdit } = useMutation({
    mutationFn: (payload: object) => collectionApis.EditModel(payload),
    onSuccess: () => {
      toast.success('Cập nhật bộ sưu tập thành công');
    },
    onError: (
      ex: AxiosError<
        ResponseApi<{
          Collection: string;
        }>
      >
    ) => {
      if (ex?.response?.data?.ExMessage === 'Collection') {
        let tempCollection = duplicateCollection.concat([ex?.response?.data?.Item?.Collection]);
        setDuplicateCollection(tempCollection);
        let collectionSchemaClone = collectionSchema.clone();
        let collectionSchemaObject = yup.object({
          name: yup
            .string()
            .trim()
            .notOneOf(tempCollection, ex?.response?.data?.Message)
            .required('Bộ sưu tập không được bỏ trống.')
        });
        collectionSchemaClone = collectionSchemaClone.concat(collectionSchemaObject);
        setCurrentSchema(collectionSchemaClone);
        setIsTrigger('name');
      }
    }
  });

  useEffect(() => {
    if (isTrigger === 'name') {
      trigger(isTrigger);
      setIsTrigger('idle');
    }
  }, [isTrigger, trigger]);

  const handleCompelete = handleSubmit((data) => {
    data.name = data.name.replace(/ +(?= )/g, '');
    setValue('name', data.name);
    if (!collectionId) {
      handleCreate({ Name: data.name });
    } else {
      if (isFetching) return;
      handleEdit({ Id: collectionId, Name: data.name });
    }
  });

  function backCollection() {
    navigate(path.bosuutap);
  }

  const isDisable = useMemo(() => !!collectionId && isFetching, [collectionId, isFetching]);

  return (
    <div className='p-5'>
      <Title title={!!collectionId ? 'CẬP NHẬT BỘ SƯU TẬP' : 'THÊM MỚI BỘ SƯU TẬP'} />

      <div className='relative my-6'>
        <div>
          <div className='form-group row' style={{ marginBottom: '10px' }}>
            <label
              className='col-form-label col-sm-2 font-bold'
              style={{ marginBottom: '0', paddingTop: '5px', width: '100%' }}
            >
              Tên bộ sưu tập <span style={{ color: 'red' }}>*</span>
            </label>
          </div>
          <div className='form-group row'>
            <div className='component-form-group'>
              <Input
                className='form-control form-control-sm'
                placeholder='Nhập tên bộ sưu tập'
                register={register}
                name='name'
                errorMessage={errors?.name?.message}
                maxLength={256}
                disabled={isDisable}
              ></Input>
            </div>
          </div>
          <div className='form-group row'>
            <div className='component-form-group flex' style={{ textAlign: 'end' }}>
              <Button className='gray-button' onClick={backCollection}>
                Quay về
              </Button>
              {!collectionId ? (
                <Button
                  className='btn btn-primary btn-sm blue-button'
                  onClick={handleCompelete}
                  style={{ marginLeft: '10px' }}
                >
                  Thêm mới
                </Button>
              ) : (
                <Button
                  className='btn btn-primary btn-sm blue-button'
                  onClick={handleCompelete}
                  style={{ marginLeft: '10px' }}
                  disabled={isDisable}
                >
                  Cập nhật
                </Button>
              )}
            </div>
          </div>
        </div>
      </div>
      <Loading open={isLoadingCreate || isLoadingEdit}></Loading>
    </div>
  );
};

export default AddCollectionPage;
