type Receipt = {
  Id: string;
  CreateDateTime: string;
  MaNhapKho: string;
  IdUserAdmin: string;
  UserName: string;
  GhiChu: string;
  NgayTao_String: string;
  Count: number;
  isXuatQR: boolean;
  NgayVaoSo: string[];
  ListIdSachCaBiet: string[];
};

type ReceiptById = {
  IdSDKCB: string;
  STT: number;
  MaKiemSoat: string;
  MaDKCB: string;
  TenSach: string;
  NgayVaoSo: string;
  NguonCungCap: string;
  TongSach: number;
  GiaTien: number;
  TenKho: string;
  SoLuong_VN: number;
  SoLuong_EN: number;
  NgonNguKhac: number;
  SoChungTu: string;
  SoVaoSoTongQuat: string;
  IsCheck: boolean;
  IdChiTiet: string;
};

type ExportItem = {
  Id: string;
  CreateDateTime: string;
  Version: {};
  MaXuatKho: string;
  IdUserAdmin: string;
  UserName: string;
  GhiChu: string;
  IdKho: string;
  NgayTao_String: string;
  NgayVaoSo: string;
  Count: number;
  GiaBia: number;
};

type StatisticEpxort = {
  IdPhieuXuat: string;
  IdSach: string;
  IdKhoSach: string;
  TenSach: string;
  IdTinhTrang: string;
  TenTinhTrang: string;
  MaCB: string;
  GiaTien: number;
  NgayThanhLy: string;
  CreateTime: string;
  GiaTien: number;
  MaXuatKho: string;
};

type NumberOfBook = {
  idKho: string;
  Name: string;
  SoLuongSCB: string;
  SoLuongMuonSach: string;
  SoLuongSach: string;
};

type NumberOfReading = {
  idKho: string;
  TenSach: string;
  SoLuongSCB: string;
  MuonVe: string;
  DocTaiCho: string;
  TenKho: string;
};
