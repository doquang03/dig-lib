import { Component, ErrorInfo, ReactNode } from 'react';
import { toast } from 'react-toastify';

type Props = {
  children?: ReactNode;
};

type State = {
  hasError: boolean;
};

export default class ErrorBoundary extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(erro: Error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    // You can also log the error to an error reporting service
    console.error(error, errorInfo);

    toast(error.message, {
      pauseOnHover: true
    });
  }
  render() {
    if (this.state.hasError) {
      return (
        <main className='flex h-screen w-full flex-col items-center justify-center bg-primary-10'>
          <h1 className='text-9xl font-extrabold tracking-widest text-white'>500</h1>
          <div className='absolute rotate-12 rounded bg-primary-10 px-2 text-sm text-white'>Internal server error</div>
          <button className='mt-5'>
            <a
              href='/'
              className='group relative inline-block text-sm font-medium text-primary-10 focus:outline-none focus:ring active:text-primary-10/75'
            >
              <span className='absolute inset-0 translate-x-0.5 translate-y-0.5 bg-primary-10 transition-transform group-hover:translate-y-0 group-hover:translate-x-0'></span>

              <span className='relative block border border-current bg-white px-8 py-3'>Quay về trang chủ</span>
            </a>
          </button>
        </main>
      );
    }
    return this.props.children;
  }
}
