import { Outlet } from 'react-router-dom';

const Day19Page = () => {
  return <Outlet />;
};

export default Day19Page;
