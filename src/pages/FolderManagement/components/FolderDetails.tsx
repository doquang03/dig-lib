import { useMutation, useQuery } from '@tanstack/react-query';
import { folderApis } from 'apis';
import { Empty, Loading, ModalDelete, TitleDelete } from 'components';
import { path } from 'constants/path';
import { useOutsideClick, useQueryParams, useWindowDimensions } from 'hooks';
import { useEffect, useState, type RefObject } from 'react';
import { Outlet, useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useFolderManagement } from '../contexts/useFolderManagement';
import ContextMenu from './ContextMenu';
import GridView from './GridView';
import ListingView from './ListingView';

type ActionContextMenu = 'delete' | 'move' | 'delete_multi' | 'remove_selected_ids';

type Props = {
  onClick: (action: ActionContextMenu) => void;
};

const DEFAULT_UNDETERMINED_IDS = [
  'document-chua-xac-dinh',
  'paper-chua-xac-dinh',
  'audio-chua-xac-dinh',
  'video-chua-xac-dinh',
  'image-chua-xac-dinh'
];

function BookContextMenu(props: Props) {
  const {
    contextBook: { book },
    setSelectedBookIds,
    selectedBookIds
  } = useFolderManagement();

  function handleMoveBookToFolder() {
    if (!book) return;

    props.onClick('move');

    selectedBookIds.length <= 1 && setSelectedBookIds([book?.Id || '']);
  }

  return (
    <div className='flex flex-col gap-1'>
      {selectedBookIds.length <= 1 ? (
        <>
          <button className='text-left hover:bg-[#3D9BE326]' onClick={handleMoveBookToFolder}>
            Di chuyển tài liệu
          </button>

          <button className='text-left text-danger-10 hover:bg-[#3D9BE326]' onClick={() => props.onClick('delete')}>
            Xóa tài liệu
          </button>
        </>
      ) : (
        <>
          <button className='text-left hover:bg-[#3D9BE326]' onClick={handleMoveBookToFolder}>
            Di chuyển tài liệu đã chọn
          </button>

          <button className='text-left hover:bg-[#3D9BE326]' onClick={() => props.onClick('remove_selected_ids')}>
            Bỏ chọn
          </button>

          <button
            className='text-left text-danger-10 hover:bg-[#3D9BE326]'
            onClick={() => props.onClick('delete_multi')}
          >
            Xóa tài liệu đã chọn
          </button>
        </>
      )}
    </div>
  );
}

function FolderDetails() {
  const [visibleModal, setVisibleModal] = useState<ActionContextMenu>();
  const [visibleToastify, setVisibleToastify] = useState<boolean>(false);

  const { id } = useParams();

  const { height } = useWindowDimensions();

  const navigate = useNavigate();

  const { search } = useQueryParams();

  const {
    viewMode,
    setSelectedBook,
    setContextBook,
    isPreviewMode,
    contextBook: { x, y, book },
    activeFolder,
    selectedBookIds,
    setSelectedBookIds
  } = useFolderManagement();

  const ref = useOutsideClick(() => setSelectedBook(undefined));

  const contextBookMenuRef = useOutsideClick(() => setContextBook({ book: undefined, x: 0, y: 0 }));

  const {
    data: bookData,
    isLoading,
    refetch
  } = useQuery({
    queryKey: ['getBooksInFolders', id, search],
    queryFn: () =>
      folderApis.getBooksInFolder({
        page: 1,
        pageSize: 100000,
        IdThuMucSach: id && DEFAULT_UNDETERMINED_IDS.includes(id) ? '' : id + '',
        Type: activeFolder?.type as FolderType,
        value: search
      }),
    enabled: !!id && !!activeFolder,
    onError: () => {
      navigate(`${path.folderManagement}/${id}`);
    }
  });

  const { ListModel } = bookData?.data.Item || { ListModel: [] };

  const { mutate: deleteBookInFolder, isLoading: loadingDeleteBookInFolder } = useMutation({
    mutationFn: folderApis.deleteBookInFolder,
    onSuccess: (data) => {
      toast.success(data.data.Message);

      setVisibleModal(undefined);

      setContextBook({ x: 0, y: 0 });

      refetch();
    }
  });

  const { mutate: deleteBooks, isLoading: loadingDeleteBooksInfolder } = useMutation({
    mutationFn: folderApis.deleteBooksInfolder,
    onSuccess: (data) => {
      toast.success(data.data.Message);

      setVisibleModal(undefined);

      setContextBook({ x: 0, y: 0 });

      setSelectedBookIds([]);

      refetch();
    }
  });

  useEffect(() => {
    if (!visibleToastify) {
      return;
    }

    const timer = setTimeout(() => setVisibleToastify(false), 4000);

    return () => clearTimeout(timer);
  }, [visibleToastify]);

  function handleContextMenuClick(action: ActionContextMenu) {
    setVisibleModal(action);

    if (action === 'move') {
      setVisibleToastify(true);
    }

    action === 'remove_selected_ids' && setSelectedBookIds([]);

    setContextBook((prevState) => ({ ...prevState, x: 0, y: 0 }));
  }

  function handleDeleteSeletedBooks() {
    deleteBooks(selectedBookIds);
  }

  return (
    <div className='relative flex w-full'>
      <div className={`relative ${isPreviewMode ? 'w-[75%]' : 'w-full'}`} ref={ref as RefObject<HTMLDivElement>}>
        {isLoading && !ListModel.length ? (
          <div className='grid h-full place-items-center'>Đang tải. đợi xíu nha</div>
        ) : (
          <>
            {viewMode === 'listing' && <ListingView data={ListModel || []} />}

            {viewMode === 'grid' && <GridView data={ListModel || []} />}
          </>
        )}

        {/* list */}
        {viewMode === 'listing' ? (
          <ContextMenu
            show={!!book && !!x && !!y}
            x={358}
            y={y - 29 * 6}
            ref={contextBookMenuRef as RefObject<HTMLDivElement>}
          >
            <BookContextMenu onClick={handleContextMenuClick} />
          </ContextMenu>
        ) : null}

        {/* grid */}
        {viewMode === 'grid' ? (
          <ContextMenu
            show={!!book && !!x && !!y}
            x={x - 64 * 8}
            y={y - 257}
            ref={contextBookMenuRef as RefObject<HTMLDivElement>}
          >
            <BookContextMenu onClick={handleContextMenuClick} />
          </ContextMenu>
        ) : null}

        <div
          className={`sticky bottom-1 right-0 left-0 z-20 grid place-items-center  rounded-md bg-white text-center shadow-md transition-transform ${
            visibleToastify ? 'visible h-[2rem] -translate-y-1 duration-150' : 'invisible translate-y-1 duration-150'
          }`}
        >
          Hãy nhấp chuột phải vào thư mục mà bạn muốn di chuyển những quyển sách đã chọn đến!
        </div>
      </div>

      <div
        className={`overflow-y-auto pl-1 transition-all ${
          isPreviewMode
            ? 'visible w-1/2 opacity-100 duration-200 ease-linear'
            : 'invisible h-0 w-0 opacity-0 duration-75 ease-linear'
        }`}
        style={{
          maxHeight: height - 50 - 42 - 150
        }}
      >
        <Outlet />
      </div>

      <ModalDelete
        labelOK='Xác nhận'
        open={visibleModal === 'delete'}
        closable={false}
        loading={loadingDeleteBookInFolder}
        handleCancel={() => {
          setVisibleModal(undefined);
          setContextBook({ x: 0, y: 0 });
        }}
        handleOk={() => !!book && deleteBookInFolder(book?.Id || '')}
        title={<TitleDelete firstText={`Bạn có chắc chắn muốn xóa tài liệu`} secondText={book?.TenSach} />}
      />

      <ModalDelete
        labelOK='Xác nhận'
        open={visibleModal === 'delete_multi'}
        closable={false}
        loading={loadingDeleteBookInFolder}
        handleCancel={() => {
          setVisibleModal(undefined);

          setContextBook({ x: 0, y: 0 });
        }}
        handleOk={() => !!selectedBookIds.length && handleDeleteSeletedBooks()}
        title={
          <TitleDelete
            firstText={`Bạn có chắc chắn muốn xóa`}
            secondText={`${selectedBookIds.length} tài liệu đã chọn`}
          />
        }
      />

      <Loading open={loadingDeleteBooksInfolder} />
    </div>
  );
}

export default FolderDetails;
