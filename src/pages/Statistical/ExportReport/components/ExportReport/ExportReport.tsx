import { DownloadOutlined } from '@ant-design/icons';
import { useMutation } from '@tanstack/react-query';
import { DatePicker } from 'antd';
import { reportApis } from 'apis';
import { Button, Title } from 'components';
import { FORMAT_DATE_PICKER } from 'constants/config';
import { UserConText, useUser } from 'contexts/user.context';
import dayjs, { Dayjs } from 'dayjs';
import { ReactNode, useContext, useState } from 'react';
import { toast } from 'react-toastify';
import { download } from 'utils/utils';

const ExportReportBox = ({
  title,
  children,
  loading,
  onClick
}: {
  title: string;
  children?: ReactNode;
  loading?: boolean;
  onClick: VoidFunction;
}) => {
  const { isAllowedAdjustment } = useUser();

  return (
    <div className='w-full rounded-md border border-primary-20/50 px-2 py-2.5'>
      <h3 className='mt-0 text-center text-primary-10'>{title}</h3>

      <div className='flex items-center justify-center gap-2'>
        {children}

        <Button
          onClick={onClick}
          loading={loading}
          disabled={loading || !isAllowedAdjustment}
          variant={isAllowedAdjustment ? 'default' : 'disabled'}
        >
          <DownloadOutlined /> Tải xuống
        </Button>
      </div>
    </div>
  );
};

const CURRENT_YEAR = new Date().getFullYear();
const ExportReport = () => {
  const [year, setYear] = useState<number>(CURRENT_YEAR);
  const user = useContext(UserConText);

  const { mutate: downloadSoSachDuocMuon, isLoading: loadingDownloadSoSachDuocMuon } = useMutation({
    mutationFn: reportApis.exportTTSach,
    onSuccess: (data) => {
      toast.success('Tải số lượng sách được mượn thành công');
      download('SoLuonSachDuocMuon_DKCB.xls', data.data);
    }
  });

  const { mutate: downloadSoSachTheoTinhTrang, isLoading: loadingDownloadSoSaschDuocMuon } = useMutation({
    mutationFn: reportApis.exportSLSachTT,
    onSuccess: (data) => {
      toast.success('Tải số lượng sách theo tình trạng thành công');
      download('SoLuonSachTheoTinhTrang_DKCB.xls', data.data);
    }
  });

  const { mutate: downloadBBKTTV, isLoading: loadingDownloadBBKTTV } = useMutation({
    mutationFn: () =>
      reportApis.exportBBKTTV(
        user.profile?.IdDVCT_TamThoi != null ? user.profile?.IdDVCT_TamThoi : user.profile?.IdDVCT_Chinh || ''
      ),
    onSuccess: (data) => {
      toast.success('Tải số lượng sách theo tình trạng thành công');
      download('BienBanKiemTraThuVien.doc', data.data);
    }
  });

  const handleExportSoSachDuocMuon = () => {
    downloadSoSachDuocMuon(year);
  };

  const handleOnChangeDatePicker = (value: Dayjs | null) => {
    if (!value) return;
    setYear(value.get('year'));
  };
  return (
    <div>
      <Title title='Xuất báo cáo' />

      <div className='mt-3 flex flex-col items-center justify-around gap-3 md:flex-row'>
        <ExportReportBox
          title='Số lượng sách được mượn'
          onClick={handleExportSoSachDuocMuon}
          children={
            <DatePicker
              picker='year'
              onChange={handleOnChangeDatePicker}
              placeholder='Chọn năm'
              className='py-2'
              defaultValue={dayjs(dayjs(), FORMAT_DATE_PICKER[4])}
            />
          }
          loading={loadingDownloadSoSachDuocMuon}
        />

        <ExportReportBox
          title='Số lượng sách theo tình trạng'
          onClick={downloadSoSachTheoTinhTrang}
          loading={loadingDownloadSoSaschDuocMuon}
        />

        <ExportReportBox title='Biên bản kiểm tra thư viện' onClick={downloadBBKTTV} loading={loadingDownloadBBKTTV} />
      </div>
    </div>
  );
};

export default ExportReport;
