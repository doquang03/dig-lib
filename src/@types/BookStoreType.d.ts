type CreateBookStoreTypeVars = {
  Id?: string;
} & Omit<BookStoreType, 'Id'>;

type BookStoreType = {
  Id: string;
  IdParent: string;
  Ten: string;
  MaKho: string;
  CachTangMaCB?: string;
  children?: BookStoreType[];
  IsEdit?: boolean;
  CountBook?: number;
  MaCBCount?: number;
};
