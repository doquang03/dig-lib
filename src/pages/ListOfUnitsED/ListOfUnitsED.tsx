import { TabsProps } from 'antd';
import { Tabs } from 'components';
import EducationDepartment from 'pages/TrangChu/components/EducationDepartment';
import EducationStatictial from 'pages/TrangChu/components/EducationStatictial';
import { useState } from 'react';

const ListOfUnitsED = () => {
  const [activeTab, setActiveTab] = useState<string>('table');

  const items: TabsProps['items'] = [
    {
      key: 'table',
      label: `Danh sách`,
      children: <EducationDepartment />,
      forceRender: true
    },
    {
      key: 'statictal',
      label: `Biểu đồ`,
      children: <EducationStatictial />,
      forceRender: false
    }
  ];

  const handleChangeTab = (activeTab: string) => setActiveTab(activeTab);

  return (
    <div className='mt-3'>
      <Tabs items={items} activeKey={activeTab} onChange={handleChangeTab} destroyInactiveTabPane={true} />
    </div>
  );
};

export default ListOfUnitsED;
