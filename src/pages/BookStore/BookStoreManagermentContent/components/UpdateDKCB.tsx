import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation } from '@tanstack/react-query';
import { Modal, ModalProps } from 'antd';
import { bookStoreApis } from 'apis';
import { AxiosError } from 'axios';
import { Button, Input } from 'components';
import { useMemo } from 'react';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import { MaSachCaBiet } from 'utils/rules';

type Props = {
  book: SachCaBiet;
  onHide: VoidFunction;
  onSuccess: VoidFunction;
} & ModalProps;

type FormInput = {
  MaCBMoi: string;
};

const UpdateDKCB = (props: Props) => {
  const {
    register,
    handleSubmit,
    setError,
    reset,
    formState: { errors }
  } = useForm<FormInput>({
    defaultValues: {
      MaCBMoi: ''
    },
    resolver: yupResolver(MaSachCaBiet)
  });
  const { book, open, onHide, onSuccess } = props;

  const { mutate: updateMaDKCB, isLoading } = useMutation({
    mutationFn: (maCaBietMoi: string) => bookStoreApis.UpdateSCB(book.Id as string, maCaBietMoi),
    onSuccess: (res, data) => {
      toast.success('Thay đổi mã cá biệt thành công');
      book.MaKSCB = data;
      onHide();
      onSuccess();
      reset();
    },
    onError: (error: AxiosError<ResponseApi<{ MaCaBietMoi: string }>>) => {
      setError('MaCBMoi', { type: 'duplicate', message: error.response?.data.Message });
    }
  });

  const preMSCB = useMemo(() => {
    if (book?.MaKSCB?.split('.') && book?.MaKSCB?.split('.').length > 1) return book?.MaKSCB?.split('.')[0];
    return '';
  }, [book?.MaKSCB]);

  const handleComplete = handleSubmit((data: any) => {
    if (!book) return;
    let str = '' + data.MaCBMoi;
    const pad = '0000';
    if (preMSCB === '') str = pad.substring(0, pad.length - str.length) + str;
    else str = preMSCB + '.' + pad.substring(0, pad.length - str.length) + str;
    updateMaDKCB(str);
  });

  return (
    <Modal
      open={open}
      title={<h3 className='border-b-2 pb-2 font-bold uppercase text-primary-10'>Cập nhật số Đăng kí cá biệt</h3>}
      closable={false}
      footer={null}
    >
      <form onSubmit={handleComplete}>
        <div className='mb-4 flex flex-col'>
          <label className='col-span-3 mb-4 text-base font-bold'>Mã cá biệt cũ</label>
          <div className='flex items-center justify-between gap-1'>
            <div className='text-note grow pl-2.5 pr-2.5 text-left'>
              <label>{book?.MaKSCB}</label>
            </div>
          </div>
        </div>
        <div className='mb-4 flex flex-col'>
          <label className='col-span-3 mb-4 text-base font-bold'>Mã cá biệt mới</label>
          <div className='col-span-9 flex items-center justify-between gap-1'>
            <div className='text-note shrink-0'>
              <label>{preMSCB}</label>
            </div>
            <Input
              containerClassName='grow'
              name='MaCBMoi'
              onKeyPress={(event) => {
                if (!/[0-9]/.test(event.key)) {
                  event.preventDefault();
                }
              }}
              placeholder='Nhập mã cá biệt'
              register={register}
              maxLength={6}
            />
          </div>
          {!!errors && <p className='mt-1 text-[14px] text-red-500'>{errors.MaCBMoi?.message}</p>}
        </div>

        <div className='mt-3 flex items-center justify-end gap-2'>
          <Button type='button' variant='secondary' onClick={onHide} loading={isLoading} disabled={isLoading}>
            Quay về
          </Button>

          <Button loading={isLoading} disabled={isLoading}>
            Cập nhật
          </Button>
        </div>
      </form>
    </Modal>
  );
};

export default UpdateDKCB;
