import { DeleteFilled, EditOutlined } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Table, Tooltip } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { bookshelfApis } from 'apis';
import classNames from 'classnames';
import { Button, Empty, ModalDelete, SearchForm, SizeChanger, Title, TitleDelete } from 'components';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import { usePaginationNavigate, useQueryConfig } from 'hooks';
import { useCallback, useMemo, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getSerialNumber } from 'utils/utils';

const BookshelfManagementPage = () => {
  const [selectedBookshelf, setSelectedBookshelf] = useState<Bookshelf | undefined>();

  const queryConfig = useQueryConfig();

  const navigate = useNavigate();

  const { isAllowedAdjustment } = useUser();

  const {
    data: bookshelfsData,
    isLoading: gettingBookshelf,
    refetch,
    isRefetching
  } = useQuery({
    queryKey: ['bookshelfs', queryConfig],
    queryFn: () => bookshelfApis.getBookshelfs(queryConfig)
  });

  const bookshelfs = useMemo(() => {
    if (!bookshelfsData?.data.Item) {
      return;
    }

    return bookshelfsData?.data?.Item;
  }, [bookshelfsData?.data?.Item]);

  const { mutate: deleteBookshelf, isLoading: loadingDeleteBookshelf } = useMutation({
    mutationFn: (id: string) => bookshelfApis.deleteBookshelf(id),
    onSuccess: () => {
      refetch();
      toast.success('Xóa kệ sách thành công');
      setSelectedBookshelf(undefined);
    }
  });

  const columns = useMemo(() => {
    const _columns: ColumnsType<Bookshelf> = [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        render: (value, record, index) => {
          return (
            queryConfig.page && queryConfig.pageSize && getSerialNumber(+queryConfig.page, +queryConfig.pageSize, index)
          );
        },
        width: 100
      },
      {
        title: 'Tên kệ sách',
        dataIndex: 'TenKe',
        key: 'TenKe',
        render: (value, { TenKe }) => (
          <Tooltip placement='topLeft' title={TenKe} arrow={true}>
            <p>{TenKe}</p>
          </Tooltip>
        ),
        onCell: (record) => ({
          className: 'text-left'
        }),
        ellipsis: true
      },
      {
        title: 'Vị trí kệ sách',
        dataIndex: 'ViTri',
        key: 'ViTri',
        render: (value, { ViTri }) => (
          <Tooltip placement='topLeft' title={ViTri} arrow={true}>
            <p>{ViTri}</p>
          </Tooltip>
        ),
        onCell: (record) => ({
          className: 'text-left'
        }),
        ellipsis: true
      },
      {
        title: 'Mô tả',
        dataIndex: 'GhiChu',
        key: 'GhiChu',
        render: (value, { GhiChu }) => (
          <Tooltip placement='topLeft' title={GhiChu} arrow={true}>
            <p>{GhiChu}</p>
          </Tooltip>
        ),
        onCell: (record) => ({
          className: 'text-left'
        }),
        ellipsis: true
      }
    ];

    if (isAllowedAdjustment) {
      _columns.push({
        title: 'Hành động',
        dataIndex: 'actions',
        key: 'actions',
        width: 200,
        render: (value, record) => {
          return (
            <>
              <button
                className='mx-2'
                onClick={(e) => {
                  navigate(`CapNhat/${record.Id}`);
                }}
              >
                <EditOutlined style={{ fontSize: '20px', color: '#08c' }} />
              </button>

              <button
                className='mx-2'
                onClick={(e) => {
                  setSelectedBookshelf(record);
                }}
              >
                <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
              </button>
            </>
          );
        }
      });
    }

    return _columns;
  }, [isAllowedAdjustment, navigate, queryConfig.page, queryConfig.pageSize]);

  const handleNavigation = usePaginationNavigate();

  const onPaginate = useCallback(
    (page: number, pageSize: number) => {
      handleNavigation({ ...queryConfig, page: page + '', pageSize: pageSize + '' });
    },
    [handleNavigation, queryConfig]
  );

  const pagination = useMemo(() => {
    let size = 30;
    let currentPage = 1;

    if (queryConfig.pageSize) {
      size = +queryConfig.pageSize;
    }
    if (queryConfig.page) {
      currentPage = +queryConfig.page;
    }

    return {
      current: currentPage,
      pageSize: size,
      total: bookshelfs?.TotalRecord,
      onChange: onPaginate,
      hideOnSinglePage: true,
      showSizeChanger: false,
      showQuickJumper: true,
      locale: { jump_to: '', page: '' }
    };
  }, [bookshelfs?.TotalRecord, onPaginate, queryConfig.page, queryConfig.pageSize]);

  const handleAddBookshelf = () => navigate(path.themKeSach);

  return (
    <div className='p-5'>
      <Title title='Danh sách kệ sách' />

      <div>
        <SearchForm placeholder='Nhập tên kệ sách' />

        <Button
          className={classNames('', {
            hidden: !isAllowedAdjustment,
            'my-2 w-full md:w-auto': isAllowedAdjustment
          })}
          onClick={handleAddBookshelf}
        >
          Thêm kệ sách
        </Button>

        <div className='mt-6'>
          <Table
            loading={gettingBookshelf || isRefetching}
            columns={columns}
            dataSource={bookshelfs?.lst || []}
            pagination={pagination}
            scroll={{ x: 1500 }}
            rowKey={(record) => record.Id}
            className='custom-table'
            bordered
            locale={{
              emptyText: () => <Empty />
            }}
          />
        </div>

        {bookshelfs && (
          <div
            className={classNames('relative', {
              'mt-[86px]': Number(queryConfig.pageSize) >= bookshelfs?.TotalRecord
            })}
          >
            <div className='absolute bottom-2'>
              <SizeChanger
                visible={bookshelfs?.TotalRecord > 0}
                value={bookshelfs?.lst.length + ''}
                total={bookshelfs?.TotalRecord + ''}
              />
            </div>
          </div>
        )}
      </div>

      <ModalDelete
        open={Boolean(selectedBookshelf)}
        closable={false}
        title={<TitleDelete firstText='Bạn có muốn xóa kệ sách này' secondText={selectedBookshelf?.TenKe} />}
        handleCancel={() => setSelectedBookshelf(undefined)}
        handleOk={() => {
          deleteBookshelf(selectedBookshelf?.Id + '');
          setSelectedBookshelf(undefined);
        }}
        loading={loadingDeleteBookshelf}
      />
    </div>
  );
};

export default BookshelfManagementPage;
