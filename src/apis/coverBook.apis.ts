import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/UpdateCoverBook`;

const coverBooksApis = {
  uploadCover: (file: FormData) =>
    httpThuVien.post<ResponseApi<UploadFileData<Book>>>(`${endPoint}/UpLoadCover_Preview`, file, {
      headers: { 'Content-Type': 'multipart/form-data', Accept: '*/*' }
    }),
  saveCovers: (payload: string[][]) => {
    return httpThuVien.post(`/${endPoint}/UpLoadCover_Save`, payload);
  }
};

export default coverBooksApis;
