import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/HocSinh`;

type GetStudentData = {
  ListThanhVien: Student[];
} & ResponseList;

type GetOptionsData = {
  ListNienKhoa: [{ label: string; value: string }];
  ListTrangThai: [{ label: string; value: number }];
  ListGioiTinh: [{ label: string; value: string }];
  ExMessage: string;
};

export type CreateStudentVars = {
  Id?: string;
  Ten: string;
  GioiTinh: string;
  NgaySinh: string;
  LopHoc: string;
  NienKhoa: string;
  DiaChi?: string;
  SDT: string;
  HinhChanDung?: string;
  TrangThai?: number;
  ThoiHanThe: string;
};

const studentApis = {
  getOptionsFilterData: () => httpThuVien.get<ResponseApi<GetOptionsData>>(`/${endPoint}/GetDataFilter`),
  getStudents: (payload: SearchQueryParams) => {
    const { TextForSearch = '', gender = '', page = 1, pageSize = 30, schoolYear = '', status = -1 } = payload;
    return httpThuVien.post<ResponseApi<GetStudentData>>(
      `/${endPoint}/GetDataPagingBySearch`,
      JSON.stringify({
        PageSize: +pageSize,
        Page: +page,
        TextForSearch: TextForSearch,
        GioiTinh: gender,
        NamHoc: schoolYear,
        TrangThai: +status
      })
    );
  },
  createStudent: (payload: FormData) => {
    return httpThuVien.post<ResponseApi<{}>>(`${endPoint}/CreateUser`, payload, {
      headers: { 'Content-Type': 'multipart/form-data', Accept: '*/*' }
    });
  },
  getStudent: (id: string) => httpThuVien.post<ResponseApi<Student>>(`${endPoint}/GetDataById?id=${id}`),
  updateStudent: (payload: FormData) => {
    return httpThuVien.post<ResponseApi<{}>>(`${endPoint}/EditUser`, payload, {
      headers: { 'Content-Type': 'multipart/form-data', Accept: '*/*' }
    });
  },
  UpdateSchoolYearJuniorByList: (payload: string[], targetClass: string) => {
    return httpThuVien.post<ResponseApi<{}>>(
      `${endPoint}/UpdateSchoolYearJuniorByList?targetClass=${targetClass}`,
      payload
    );
  },
  deleteStudent: (ids: string[]) => httpThuVien.post<ResponseApi<{}>>(`${endPoint}/DeleteByListId`, ids),
  activeStudent: (id: string) => httpThuVien.post<ResponseApi<{}>>(`${endPoint}/ActiveMember?id=${id}`),
  deactiveStudent: (id: string) => httpThuVien.post<ResponseApi<{}>>(`${endPoint}/DeactiveMember?id=${id}`),
  UploadFileData: (file: FormData) =>
    httpThuVien.post<ResponseApi<UploadFileData<Student>>>(`${endPoint}/PreviewImport`, file, {
      headers: { 'Content-Type': 'multipart/form-data', Accept: '*/*' }
    }),
  saveExcelFile: (payload: string[][]) =>
    httpThuVien.post<ResponseApi<UploadFileData<Student>>>(`${endPoint}/ImportSave`, payload),
  recoveryPassword: (id: string) => httpThuVien.post<ResponseApi<{}>>(`${endPoint}/ResetPassword?idThanhVien=${id}`),
  exportCard: (payload: { mauThe: string; CachIn: string; listIdTV: string[] }) =>
    httpThuVien.post(`${endPoint}/MauThe`, { ...payload }, { responseType: 'blob' }),
  uploadAvatarFileZip: (file: FormData) =>
    httpThuVien.post<ResponseApi<UploadFileData<Student>>>(`${endPoint}/UploadAvatar_Preview`, file, {
      headers: { 'Content-Type': 'multipart/form-data', Accept: '*/*' }
    }),
  saveAvatar: (data: string[][]) =>
    httpThuVien.post<ResponseApi<UploadFileData<Student>>>(`${endPoint}/UpLoadAvatar_Save?update=true`, data),
  downloadFileStudents: () =>
    httpThuVien.get(`${endPoint}/DownFileExcel`, {
      responseType: 'blob'
    })
};

export default studentApis;
