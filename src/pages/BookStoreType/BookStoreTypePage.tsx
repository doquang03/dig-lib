import { DeleteFilled, EditOutlined, PlusSquareFilled } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Tooltip } from 'antd';
import type { ItemType } from 'antd/es/breadcrumb/Breadcrumb';
import Table, { type ColumnsType } from 'antd/es/table';
import { bookstoreTypeApis } from 'apis';
import classNames from 'classnames';
import { Button, Empty, ModalDelete, SelectSearchForm, SizeChanger, Title, TitleDelete } from 'components';
import { CACH_TANG_MA_CA_BIET_Value } from 'constants/bookstoreType';
import { useUser } from 'contexts/user.context';
import 'css/BookStoreTypePage.css';
import { usePaginationNavigate, useQueryConfig } from 'hooks';
import { useCallback, useMemo, useState } from 'react';
import { toast } from 'react-toastify';
import { getSerialNumber } from 'utils/utils';
import AddBookStoreType from './component/AddBookStoreType';

const BookStoreType = () => {
  const handleNavigation = usePaginationNavigate();

  const queryConfig = useQueryConfig();
  const [originalData, setOriginalData] = useState<BookStoreType[]>([]);
  const [totalListKhoSach, setTotalLoaiKhoSach] = useState<number>(0);
  const [selectedDelete, setSelectedDelete] = useState<BookStoreType>();
  const [isVisiable, setIsVisiable] = useState<boolean>(false);
  const [visibleAddBookstoreType, setVisibleAddBookstoreType] = useState<boolean>(false);
  const [isAddNew, setIsAddNew] = useState<boolean>(false);

  const [parentId, setParentId] = useState<string>('');
  const [routes, setRoutes] = useState<ItemType[]>([
    {
      breadcrumbName: 'Kho sách chính',
      path: ''
    }
  ]);

  const { isAllowedAdjustment } = useUser();

  const {
    data: dataKhoSach,
    isFetching,
    refetch
  } = useQuery({
    queryKey: ['bookStoreType', queryConfig.OrderBy, queryConfig.pageSize, queryConfig.page, queryConfig.TextForSearch],
    queryFn: () =>
      bookstoreTypeApis.getBookStoreTypes({
        page: queryConfig?.page + '',
        sizeNumber: queryConfig?.pageSize + '',
        CachTangMaCBKS: queryConfig?.OrderBy,
        TextForSearch: queryConfig.TextForSearch
      })
  });

  const filterListKhoSach = (data: BookStoreType[]) => {
    type dicnationary = {
      [key: string]: any;
    };

    let checkParent: dicnationary = {};
    let final: BookStoreType[] = [];
    for (let i = 0; i < data.length; i++) {
      let tempData = {
        Id: data[i].Id,
        IdParent: data[i].IdParent,
        CachTangMaCB: data[i].CachTangMaCB,
        Ten: data[i].Ten,
        MaKho: data[i].MaKho
      };

      if (checkParent[data[i].Id] === undefined) checkParent[data[i].Id] = tempData;

      if (data[i].IdParent === '') {
        final.push(tempData);
      } else {
        if (checkParent[data[i].IdParent]?.children === undefined) checkParent[data[i].IdParent].children = [];
        checkParent[data[i].IdParent].children.push(tempData);
      }
    }

    return final;
  };

  const listLoaiKhoSach = useMemo(() => {
    setTotalLoaiKhoSach(dataKhoSach?.data?.Item?.count || 0);
    setOriginalData(dataKhoSach?.data?.Item?.ListKhoSach || []);
    return filterListKhoSach(dataKhoSach?.data.Item.ListKhoSach || []);
  }, [dataKhoSach?.data.Item.ListKhoSach, dataKhoSach?.data.Item?.count]);

  const { mutate: deleteKhoSach, isLoading: isLoadingXoaKhoSach } = useMutation({
    mutationFn: bookstoreTypeApis.deleteBookStore,
    onSuccess: () => {
      setIsVisiable(false);
      setSelectedDelete(undefined);
      refetch();
      toast.success('Xóa kho sách thành công');
      setParentId('');
    }
  });

  const onPaginate = useCallback(
    (page: number, pageSize: number) => {
      handleNavigation({ ...queryConfig, page: page + '', pageSize: pageSize + '' });
    },
    [handleNavigation, queryConfig]
  );

  const taoRoutes = useCallback(
    (parentId: string) => {
      if (!parentId) {
        return;
      }

      for (let index = 0; index < originalData.length; index++) {
        const element = originalData[index];

        if (parentId === element.Id) {
          if (element.IdParent) {
            taoRoutes(element.IdParent);
          }
          setRoutes((prevState) => [...prevState, { breadcrumbName: element.Ten, path: '' }]);
        }
      }
    },
    [originalData]
  );

  const pagination = useMemo(() => {
    return {
      current: (!!queryConfig.page && +queryConfig.page) || 1,
      pageSize: (!!queryConfig.pageSize && +queryConfig.pageSize) || 30,
      total: totalListKhoSach,
      onChange: onPaginate,
      hideOnSinglePage: true,
      showSizeChanger: false,
      showQuickJumper: true,
      locale: { jump_to: '', page: '' }
    };
  }, [onPaginate, queryConfig.page, queryConfig.pageSize, totalListKhoSach]);

  const columns = useMemo(() => {
    const _columns: ColumnsType<BookStoreType> = [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        className: 'min-content',
        width: 100,
        render: (value, record, index) => {
          return (
            queryConfig.page && queryConfig.pageSize && getSerialNumber(+queryConfig.page, +queryConfig.pageSize, index)
          );
        }
      },
      {
        title: 'Tên kho sách',
        dataIndex: 'Ten',
        render: (value, { Ten }) => (
          <Tooltip placement='topLeft' title={Ten} arrow={true} className='truncate text-left'>
            <p>{Ten}</p>
          </Tooltip>
        ),
        onCell: (record) => ({
          className: 'text-left'
        }),
        ellipsis: true
      },
      {
        title: 'Mã kho',
        dataIndex: 'MaKho',
        key: 'MaKho',
        width: 200,
        render: (value, { MaKho }) => <p className='text-left'>{MaKho}</p>,
        onCell: (record) => ({
          className: 'text-left'
        })
      },
      {
        title: 'Cách tăng mã cá biệt',
        dataIndex: 'CachTangMaCB',
        key: 'CachTangMaCB',
        // @ts-ignore
        render: (value, { CachTangMaCB }) => <p className='text-left'>{CACH_TANG_MA_CA_BIET_Value[CachTangMaCB]}</p>,
        width: 250
      }
    ];

    if (isAllowedAdjustment) {
      _columns.push({
        title: 'Hành động',
        dataIndex: 'actions',
        key: 'actions',
        width: 240,
        render: (value, record) => {
          return (
            <>
              <Tooltip title='Thêm kho sách'>
                <button
                  className='mx-2'
                  onClick={() => {
                    taoRoutes(record.Id);
                    setParentId(record.Id);
                    setSelectedDelete(record);
                    setIsAddNew(true);
                    setVisibleAddBookstoreType(true);
                  }}
                >
                  <PlusSquareFilled style={{ fontSize: '20px', color: '#08c' }} />
                </button>
              </Tooltip>

              <Tooltip title='Chỉnh sửa kho sách'>
                <button
                  className='mx-2'
                  onClick={() => {
                    taoRoutes(record.IdParent);
                    setSelectedDelete(record);
                    setParentId(record.IdParent);
                    setVisibleAddBookstoreType(true);
                    setIsAddNew(false);
                  }}
                >
                  <EditOutlined style={{ fontSize: '20px', color: '#08c' }} />
                </button>
              </Tooltip>

              <Tooltip title='Xóa kho sách'>
                <button
                  className='mx-2'
                  onClick={(e) => {
                    setSelectedDelete(record);
                    setIsVisiable(true);
                  }}
                >
                  <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
                </button>
              </Tooltip>
            </>
          );
        }
      });
    }

    return _columns;
  }, [isAllowedAdjustment, queryConfig.page, queryConfig.pageSize, taoRoutes]);

  const handleOpenAddBookstoreType = () => {
    setParentId('');
    setVisibleAddBookstoreType(true);
    setIsAddNew(true);
  };

  const handleDeleteTypeBook = () => {
    if (!selectedDelete?.Id) {
      return;
    }

    deleteKhoSach(selectedDelete?.Id);
  };

  const handleSuccess = () => {
    setSelectedDelete(undefined);
    setVisibleAddBookstoreType(false);
    setRoutes([{ breadcrumbName: 'Kho sách chính', path: '' }]);
    setParentId('');
    refetch();
  };

  return (
    <div className='p-5' id='Manager-BookType'>
      <Title title='Quản lý loại kho sách' />

      <div className='mt-3'>
        <SelectSearchForm
          placeholder='Nhập tên kho sách'
          items={[
            { value: '', label: 'Chọn cách tăng mã cá biệt' },
            { value: 3, label: 'Theo mã kho sách' },
            { value: 2, label: 'Theo nhan đề sách' },
            { value: 1, label: 'Theo tổng số sách' }
          ]}
        />
      </div>

      <Button
        variant={'default'}
        className={isAllowedAdjustment ? 'mt-3 w-[100%] md:w-auto' : 'hidden'}
        onClick={handleOpenAddBookstoreType}
      >
        Thêm mới
      </Button>

      <div className='mt-3'>
        <Table
          loading={isFetching}
          columns={columns}
          dataSource={listLoaiKhoSach || []}
          pagination={pagination}
          scroll={{ x: 980 }}
          rowKey={(record) => record.Id}
          className='custom-table'
          indentSize={40}
          bordered
          locale={{
            emptyText: () => <Empty />
          }}
        />
      </div>

      <div
        className={classNames('relative', {
          'mt-[64px]': Number(queryConfig.pageSize) >= totalListKhoSach
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger
            visible={!!listLoaiKhoSach?.length}
            value={queryConfig.pageSize}
            total={totalListKhoSach.toString()}
          />
        </div>
      </div>

      <ModalDelete
        open={isVisiable}
        closable={false}
        title={
          <TitleDelete firstText={'Bạn có chắn chắn muốn xóa kho sách'} secondText={selectedDelete?.Ten}></TitleDelete>
        }
        handleCancel={() => {
          setSelectedDelete(undefined);
          setIsVisiable(false);
        }}
        handleOk={handleDeleteTypeBook}
        loading={isLoadingXoaKhoSach}
      />

      <AddBookStoreType
        bookStoreType={selectedDelete}
        open={visibleAddBookstoreType}
        onSuccess={handleSuccess}
        onHide={() => {
          setParentId('');
          setSelectedDelete(undefined);
          setVisibleAddBookstoreType(false);
          setRoutes([{ breadcrumbName: 'Kho sách chính', path: '' }]);
        }}
        IdParent={parentId}
        routes={routes}
        isAdd={isAddNew}
      />
    </div>
  );
};

export default BookStoreType;
