import { DeleteFilled, EditOutlined } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Table, Tag, Tooltip } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { supplierApis } from 'apis';
import classNames from 'classnames';
import { Button, Empty, ModalDelete, SelectSearchForm, SizeChanger, Title, TitleDelete } from 'components';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import { usePaginationNavigate, useQueryConfig } from 'hooks';
import { useCallback, useMemo, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getSerialNumber } from 'utils/utils';

let locale = {
  emptyText: 'Không tìm được kết quả phù hợp'
};

const SupplierManagementPage = () => {
  const [selectedSupplier, setSelectedSupplier] = useState<Supplier | undefined>();

  const queryConfig = useQueryConfig();

  const navigate = useNavigate();

  const { isAllowedAdjustment } = useUser();

  const {
    data: suppliersData,
    isLoading: loadingSupplierData,
    refetch,
    isRefetching
  } = useQuery({
    queryKey: ['suppliers', queryConfig.OrderBy, queryConfig.pageSize, queryConfig.page, queryConfig.TextForSearch],
    queryFn: () =>
      supplierApis.getSuppliers({
        PageNumber: (queryConfig?.page && +queryConfig?.page) || 1,
        PageSize: (queryConfig?.pageSize && +queryConfig?.pageSize) || 30,
        TinhChat: queryConfig?.OrderBy || -1,
        Keyword: queryConfig.TextForSearch
      })
  });

  const suppliers = useMemo(() => {
    if (!suppliersData?.data.Item) {
      return;
    }

    return suppliersData?.data?.Item;
  }, [suppliersData?.data?.Item]);

  const { mutate: deleteBookshelf, isLoading: loadingDeleteBookshelf } = useMutation({
    mutationFn: (id: string) => supplierApis.deleteSupplier(id),
    onSuccess: () => {
      refetch();
      toast.success('Xóa nguồn cung cấp thành công');
      setSelectedSupplier(undefined);
    }
  });

  const columns = useMemo(() => {
    const _columns: ColumnsType<Supplier> = [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        render: (value, record, index) => {
          return (
            queryConfig.page && queryConfig.pageSize && getSerialNumber(+queryConfig.page, +queryConfig.pageSize, index)
          );
        },
        width: 100
      },
      {
        title: 'Tên nguồn cung cấp',
        dataIndex: 'TenNguonCungCap',
        key: 'TenNguonCungCap',
        render: (value, { TenNguonCungCap }) => (
          <Tooltip placement='topLeft' title={TenNguonCungCap} arrow={true}>
            <p>{TenNguonCungCap}</p>
          </Tooltip>
        ),
        onCell: (record) => ({
          className: 'text-left'
        }),
        ellipsis: true
      },
      {
        title: 'Mã nguồn cung cấp',
        dataIndex: 'Code',
        key: 'Code',
        width: 400
      },
      {
        title: 'Tính chất',
        dataIndex: 'TinhChat',
        key: 'TinhChat',
        width: 200,
        render: (value, record, index) => {
          const type = record.TinhChat === 1 ? 'Mua' : 'Miễn phí';

          return <Tag color={record.TinhChat === 1 ? 'success' : 'processing'}>{type}</Tag>;
        }
      }
    ];

    if (isAllowedAdjustment) {
      _columns.push({
        title: 'Hành động',
        dataIndex: 'actions',
        key: 'actions',
        width: 200,
        render: (value, record) => {
          return (
            <>
              <button
                className='mx-2'
                onClick={(e) => {
                  navigate(`CapNhat/${record.Id}`);
                }}
              >
                <EditOutlined style={{ fontSize: '20px', color: '#08c' }} />
              </button>

              <button
                className='mx-2'
                onClick={(e) => {
                  setSelectedSupplier(record);
                }}
              >
                <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
              </button>
            </>
          );
        }
      });
    }

    return _columns;
  }, [isAllowedAdjustment, navigate, queryConfig.page, queryConfig.pageSize]);

  const handleNavigation = usePaginationNavigate();

  const onPaginate = useCallback(
    (page: number, pageSize: number) => {
      handleNavigation({ ...queryConfig, page: page + '', pageSize: pageSize + '' });
    },
    [handleNavigation, queryConfig]
  );

  const pagination = useMemo(() => {
    let size = 30;
    let currentPage = 1;

    if (queryConfig.pageSize) {
      size = +queryConfig.pageSize;
    }
    if (queryConfig.page) {
      currentPage = +queryConfig.page;
    }

    return {
      current: currentPage,
      pageSize: size,
      total: suppliers?.TotalRecord,
      onChange: onPaginate,
      hideOnSinglePage: true,
      showSizeChanger: false
    };
  }, [suppliers?.TotalRecord, onPaginate, queryConfig.page, queryConfig.pageSize]);

  const handleAddBookshelf = () => navigate(path.themNguonCungCap);

  return (
    <div className='p-5'>
      <Title title='Danh sách nguồn cung cấp' />

      <div className='mt-4'>
        <SelectSearchForm
          placeholder='Nhập tên, mã nguồn cung cấp'
          items={[
            { label: 'Chọn tính chất', value: '' },
            { label: 'Mua', value: 1 },
            { label: 'Miễn phí', value: 0 }
          ]}
        />

        <Button
          className={classNames('', {
            hidden: !isAllowedAdjustment,
            'my-2 w-full md:w-auto': isAllowedAdjustment
          })}
          onClick={handleAddBookshelf}
        >
          Thêm nguồn cung cấp
        </Button>

        <div>
          <Table
            loading={loadingSupplierData || isRefetching}
            columns={columns}
            dataSource={suppliers?.ListNguonCungCap || []}
            pagination={pagination}
            scroll={{ x: 1500 }}
            rowKey={(record) => record.Id}
            className='custom-table mt-3'
            bordered
            locale={{
              emptyText: () => <Empty label={locale.emptyText} />
            }}
          />

          {suppliers && (
            <div
              className={classNames('relative', {
                'mt-[64px]': Number(queryConfig.pageSize) >= suppliers?.TotalRecord
              })}
            >
              <div className='absolute bottom-2'>
                <SizeChanger
                  visible={suppliers?.TotalRecord > 0}
                  value={suppliers?.ListNguonCungCap.length + ''}
                  total={suppliers?.TotalRecord + ''}
                />
              </div>
            </div>
          )}
        </div>
      </div>

      <ModalDelete
        open={Boolean(selectedSupplier)}
        closable={false}
        title={
          <TitleDelete
            firstText='Bạn có chắn chắn muốn xóa nguồn cung cấp'
            secondText={selectedSupplier?.TenNguonCungCap}
          />
        }
        handleCancel={() => setSelectedSupplier(undefined)}
        handleOk={() => {
          deleteBookshelf(selectedSupplier?.Id + '');
        }}
        loading={loadingDeleteBookshelf}
      />
    </div>
  );
};

export default SupplierManagementPage;
