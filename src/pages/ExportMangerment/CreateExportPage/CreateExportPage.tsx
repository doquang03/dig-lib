import { DeleteFilled, SearchOutlined } from '@ant-design/icons';
import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Spin, Tooltip } from 'antd';
import { bookApis, bookStoreApis, liquidationApis } from 'apis';
import classNames from 'classnames';
import { Button, DatePicker, Input, Loading, Title } from 'components';
import { path } from 'constants/path';
import { UserConText } from 'contexts/user.context';
import dayjs from 'dayjs';
import { useOutsideClick } from 'hooks';
import { debounce } from 'lodash';
import { RefObject, useContext, useEffect, useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useLocation, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { createExportSchema } from 'utils/rules';
import * as yup from 'yup';

type ExportForm = yup.InferType<typeof createExportSchema>;

const CreateExportPage = () => {
  const [visibleSearchResult, setVisibleSearchResult] = useState<boolean>(true);
  const [searchBookValue, setSearchBookValue] = useState<string>('');
  const [selectedBooks, setSelectedBooks] = useState<Array<SachCaBiet & { LinkBiaSach?: string }>>([]);
  const [errorSachCaBiet, setErrorSachCaBiet] = useState<boolean>(false);
  const { state } = useLocation();
  const selectedSCB = state ? state.selectedSCB : undefined;
  const user = useContext(UserConText);

  const [TextForSearch, setTextForSearch] = useState('');

  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    control,
    formState: { errors }
  } = useForm<ExportForm>({
    resolver: yupResolver(createExportSchema),
    defaultValues: {
      GhiChu: '',
      // @ts-ignore
      NgayXuatKho: dayjs()
    }
  });

  const { data: dataListSachCaBiet, isFetching: isFetchingSachCaBiet } = useQuery({
    queryKey: ['SachCaBiet', TextForSearch],
    queryFn: () => {
      return bookStoreApis.GetListSCB(
        {
          idKho: '',
          TextForSearch: TextForSearch,
          DaTra: true
        },
        false
      );
    },
    enabled: !!TextForSearch
  });

  const booksResult = useMemo(() => {
    if (!dataListSachCaBiet?.data?.Item) return [];
    return dataListSachCaBiet?.data?.Item.List_SachCB;
  }, [dataListSachCaBiet?.data?.Item]);

  const List_TrangThai = useMemo(() => {
    if (!dataListSachCaBiet?.data?.Item) return [];
    return dataListSachCaBiet?.data?.Item.List_TrangThai;
  }, [dataListSachCaBiet?.data?.Item]);

  const { mutate: apiThanhLy, isLoading: isLoadingThanhLy } = useMutation({
    mutationFn: (payload: {
      listCaBiet: Array<SachCaBiet & { LinkBiaSach?: string }>;
      ghiChu: string;
      ngayVaoXo: string;
    }) =>
      liquidationApis.TaoPhieuXuatSach(
        payload.listCaBiet,
        user.profile?.Id as string,
        user.profile?.UserName as string,
        payload.ghiChu,
        payload.ngayVaoXo
      ),
    onSuccess() {
      toast.success('Thanh lý sách thành công');
      navigate(path.quanlyxuatkho);
    }
  });

  useEffect(() => {
    const handleSearch = (e: KeyboardEvent) => {
      if (e.key === 'Enter') {
        if (searchBookValue) {
          setVisibleSearchResult(true);
        } else {
          setVisibleSearchResult(false);
        }
      }
    };

    window.addEventListener('keydown', handleSearch);

    return () => {
      window.removeEventListener('keydown', handleSearch);
    };
  }, [searchBookValue]);

  const handleSearch = debounce((e: React.ChangeEvent<HTMLInputElement>) => {
    setVisibleSearchResult(true);
    if (e.target.value.trim()) {
      setErrorSachCaBiet(false);
      setTextForSearch(e.target.value || ' ');
    } else {
      setErrorSachCaBiet(true);
    }
    setSearchBookValue(e.target.value);
  }, 700);

  const handleAdd = (book: SachCaBiet & { LinkBiaSach?: string }) => {
    const index = selectedBooks.findIndex((_book) => _book.Id === book.Id);

    if (index !== -1) {
      setVisibleSearchResult(false);
      toast.warning(`Sách ${book.TenSach} đã được thêm`);
    } else {
      setSelectedBooks((prevState) => [...prevState, book]);
      setVisibleSearchResult(false);
    }
  };

  const handleDelete = (index: number) => () => {
    const array = [...selectedBooks];
    array.splice(index, 1);
    setSelectedBooks(array);
  };

  const handleClickOutside = () => setVisibleSearchResult(false);

  const handleBack = () => {
    if (selectedSCB) {
      navigate(path.khosach);
    } else navigate(path.quanlyxuatkho);
  };

  const ref = useOutsideClick<HTMLDivElement>(handleClickOutside);

  const onSubmit = handleSubmit((data) => {
    if (!selectedBooks.length) {
      setErrorSachCaBiet(true);
      return;
    }
    apiThanhLy({ listCaBiet: selectedBooks, ghiChu: data.GhiChu, ngayVaoXo: new Date(data.NgayXuatKho).toISOString() });
  });

  useEffect(() => {
    if (selectedSCB) {
      setSelectedBooks(selectedSCB);
    }
  }, [selectedSCB]);

  return (
    <div className='p-5'>
      <Title title='Tạo phiếu xuất kho' />

      <div className='mt-2 grid grid-cols-1 gap-10 md:grid-cols-12'>
        <div className='col-span-6'>
          <label className='mb-1 font-bold'>
            Lý do <span className='text-danger-10'>*</span>
          </label>

          <Input
            placeholder='Nhập lý do'
            maxLength={255}
            register={register}
            name='GhiChu'
            errorMessage={errors?.GhiChu?.message}
          />
        </div>

        <div className='col-span-6'>
          <label className='mb-1 font-bold'>
            Ngày xuất kho <span className='text-danger-10'>*</span>
          </label>

          <DatePicker
            control={control}
            name={`NgayXuatKho` as const}
            errorMessage={errors?.NgayXuatKho?.message}
            disabledDate={(current) => {
              return current && current > dayjs().endOf('day');
            }}
          />
        </div>
      </div>

      <div className='relative mt-3' ref={ref as RefObject<HTMLDivElement>}>
        <h3 className='font-bold text-primary-10'>Danh sách sách cá biệt xuất kho</h3>

        {selectedSCB === undefined && (
          <Input
            placeholder='Nhập mã ĐKCB, tên sách cần xuất kho'
            className='w-full bg-transparent p-3 outline-none'
            containerClassName={classNames('border w-1/2 bg-white rounded-full flex items-center', {
              'border-danger-10': !Boolean(selectedBooks.length) && errorSachCaBiet,
              'border-gray-300': Boolean(selectedBooks.length) && !errorSachCaBiet
            })}
            right={
              <button
                onClick={() => {
                  if (searchBookValue) {
                    setVisibleSearchResult(true);
                  }
                }}
              >
                <SearchOutlined style={{ color: 'black', opacity: 0.3, marginRight: 10 }} />
              </button>
            }
            onChange={handleSearch}
          />
        )}

        {!Boolean(selectedBooks.length) && errorSachCaBiet && (
          <span className='text-danger-10'>Hãy thêm sách cần tạo phiếu xuất kho!</span>
        )}

        {visibleSearchResult && Boolean(searchBookValue) && (
          <div className='absolute z-30 max-h-[150px] w-1/2 overflow-y-auto rounded-md bg-white px-3 shadow-md'>
            {!!booksResult.length ? (
              booksResult.map((book) => (
                <button
                  className='flex w-full items-center border-b-2 py-3'
                  onClick={() => handleAdd(book)}
                  key={book.Id}
                >
                  {book.MaKSCB} - {book.TenSach}
                </button>
              ))
            ) : (
              <>
                {isFetchingSachCaBiet ? (
                  <div className='py-2 text-center'>
                    Đang tìm kiếm sách <Spin />{' '}
                  </div>
                ) : (
                  <div className='py-2 text-center'>
                    Không tìm thấy dữ liệu với từ khóa <span className='font-bold'>{searchBookValue}</span>
                  </div>
                )}
              </>
            )}
          </div>
        )}

        {!!selectedBooks.length && (
          <>
            <table className='mt-5 table overflow-scroll'>
              <thead className='bg-primary-10 text-white'>
                <tr>
                  <th className='text-center'>STT</th>
                  <th className='text-center'>Mã kiểm soát</th>
                  <th className='text-center'>Số đăng ký cá biệt</th>
                  <th className='text-center'>Tên sách</th>
                  <th className='text-center'>Tình trạng</th>
                  <th className='text-center'>Giá tiền</th>
                  <th className='text-center'></th>
                </tr>
              </thead>

              <tbody className='bg-white'>
                {selectedBooks.map((row, index) => {
                  const { MaKiemSoat, TenSach, MaKSCB, IdTrangThai, GiaBia } = selectedBooks[index];

                  return (
                    <tr key={row.Id} className='text-center'>
                      <td>{index + 1}</td>
                      <td>{MaKiemSoat}</td>
                      <td>{MaKSCB}</td>
                      <td>
                        {' '}
                        {TenSach && TenSach.length > 20 ? (
                          <Tooltip placement='bottomRight' title={TenSach} arrow={true}>
                            {TenSach.substring(0, 20).concat('...')}
                          </Tooltip>
                        ) : (
                          TenSach
                        )}
                      </td>
                      <td>{List_TrangThai.filter((_) => _.Id === IdTrangThai)[0]?.TenTT}</td>

                      <td>
                        {GiaBia && GiaBia !== 'null'
                          ? new Intl.NumberFormat('vi-VN', {
                              style: 'currency',
                              currency: 'VND',
                              maximumFractionDigits: 9
                            }).format(Number(GiaBia))
                          : ''}
                      </td>

                      <td>
                        <Tooltip title='Xóa'>
                          <button className='mx-2' onClick={handleDelete(index)}>
                            <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
                          </button>
                        </Tooltip>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>

            <div className='flex items-center justify-between'>
              <p className='font-bold text-primary-10'>
                Tổng số sách xuất kho: <span> {selectedBooks.length} quyến sách</span>
              </p>

              <p className='font-bold text-primary-10'>
                Tổng giá tiền:{' '}
                <span>
                  {' '}
                  {new Intl.NumberFormat('vi-VN', {
                    style: 'currency',
                    currency: 'VND',
                    maximumFractionDigits: 9
                  }).format(
                    Number(
                      selectedBooks.reduce(
                        (total, book) => (total += Number(book.GiaBia && book.GiaBia !== 'null' ? book.GiaBia : 0)),
                        0
                      )
                    )
                  )}
                </span>
              </p>
            </div>
          </>
        )}
      </div>

      <div className='mt-3 flex items-center justify-end gap-3'>
        <Button onClick={handleBack} variant='secondary'>
          Quay về
        </Button>

        <Button onClick={onSubmit}>Thêm mới</Button>
      </div>

      <Loading open={isLoadingThanhLy} />
    </div>
  );
};

export default CreateExportPage;
