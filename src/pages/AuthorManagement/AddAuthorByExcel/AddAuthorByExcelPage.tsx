import { DeleteFilled, DownloadOutlined } from '@ant-design/icons';
import { useMutation } from '@tanstack/react-query';
import { Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { authorApis } from 'apis';
import { Button, Loading, ProgressBar, Title } from 'components';
import { path } from 'constants/path';
import 'css/AddAuthorByExcelPage.css';
import { useMemo, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { GenerateVietnameseAuthorCode, saveByteArray } from 'utils/utils';

type TacGia = {
  STT: number;
  TenTacGia: string;
  MaTacGia: string;
  QuocTich: string;
  MoTa: string;
  actions: string;
};

const AddAuthorByExcelPage = () => {
  const [step, setStep] = useState<number>(1);
  // Mock state.
  const [listTacGiaExcel, setListTacGiaExcel] = useState<Array<TacGia>>([]);
  const [listSuccess, setListSuccess] = useState([]);
  const [listFail, setlistFail] = useState([]);
  const [tempFile, setTempFile] = useState<File>();
  const [cellError, setCellError] = useState<Array<string>>([]);

  const fileInutRef = useRef<HTMLInputElement | null>(null);
  const ref = useRef<HTMLDivElement | null>(null);

  const [page, setPage] = useState(1);
  const [paginationSize, setPaginationSize] = useState(10);

  const navigate = useNavigate();

  const { mutate: PreviewImport, isLoading: isLoadingPreviewImport } = useMutation({
    mutationFn: (file: File) => {
      let bodyFormData = new FormData();
      bodyFormData.append('file', file as Blob);
      return authorApis.PreviewImport(bodyFormData);
    },
    onSuccess: (res) => {
      let tempData = res?.data?.Item?.RawDataList;

      let result: Array<TacGia> = [];
      for (let i = 0; i < tempData.length; i++) {
        result.push({
          STT: i,
          TenTacGia: tempData[i][1] || '',
          MaTacGia: tempData[i][2]
            ? tempData[i][2]
            : tempData[i][1] && tempData[i][1].length < 255
            ? GenerateVietnameseAuthorCode(tempData[i][1])
            : '',
          QuocTich: tempData[i][3] || '',
          MoTa: tempData[i][4] || '',
          actions: ''
        });
      }
      setListTacGiaExcel(result);
      setStep(2);
    }
  });

  const { mutate: ImportSave, isLoading: isLoadingImportSave } = useMutation({
    mutationFn: (payload: Array<string>) => authorApis.ImportSave(payload),
    onSuccess: (res) => {
      let tempData = res?.data?.Item;
      setListSuccess(tempData?.ListSuccess);
      setlistFail(tempData?.ListFail);
      setTempFile(tempData?.MemoryStream);

      setCellError(tempData?.CellError);
      let listShow = tempData.ListShow;
      let resultFinal: Array<TacGia> = [];
      let tempFor: TacGia;
      for (let i = 0; i < listShow.length; i++) {
        tempFor = {
          STT: listShow[i][0] - 1,
          TenTacGia: listShow[i][1],
          MaTacGia: listShow[i][2],
          QuocTich: listShow[i][3],
          MoTa: listShow[i][4],
          actions: listShow[i][5]
        };
        resultFinal.push(tempFor);
      }
      setListTacGiaExcel(resultFinal);
    }
  });
  const handleSubmit = () => {
    // TODO: Integrate api to submit data
    let tempData;
    let result = [];
    for (let i = 0; i < listTacGiaExcel.length; i++) {
      tempData = [];
      tempData[0] = i;
      for (let param in listTacGiaExcel[i]) {
        switch (param) {
          case 'TenTacGia':
            tempData[1] = listTacGiaExcel[i].TenTacGia;
            break;
          case 'MaTacGia':
            tempData[2] = listTacGiaExcel[i].MaTacGia;
            break;
          case 'QuocTich':
            tempData[3] = listTacGiaExcel[i].QuocTich;
            break;
          case 'MoTa':
            tempData[4] = listTacGiaExcel[i].MoTa;
            break;
        }
      }
      result.push(JSON.stringify(tempData));
    }
    ImportSave(result);
    ref.current?.scrollIntoView({ behavior: 'smooth' });
    setStep(3);
  };

  const columns: ColumnsType<TacGia> = useMemo(() => {
    const handleDeleteRow = (STT: number) => {
      let result: Array<TacGia> = [];
      for (let i = 0; i < listTacGiaExcel.length; i++) {
        if (STT === listTacGiaExcel[i].STT) {
          continue;
        }
        result.push(listTacGiaExcel[i]);
      }
      setListTacGiaExcel(result);
    };
    return [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        render: (value: any, record: any, j: number) => (page - 1) * paginationSize + j + 1
      },
      {
        title: 'Tên tác giả',
        dataIndex: 'TenTacGia',
        key: 'TenTacGia',
        onCell: (record) => ({
          className:
            step === 3 && cellError[record.STT] && cellError[record.STT].indexOf('1') !== -1
              ? 'min-h-38 errorBackGround py-2'
              : ''
        })
      },
      {
        title: 'Mã tác giả',
        dataIndex: 'MaTacGia',
        key: 'MaTacGia',
        onCell: (record) => ({
          className:
            step === 3 && cellError[record.STT] && cellError[record.STT].indexOf('2') !== -1
              ? 'min-h-38 errorBackGround py-2'
              : ''
        })
      },
      {
        title: 'Quốc tịch',
        dataIndex: 'QuocTich',
        key: 'QuocTich',
        onCell: (record) => ({
          className:
            step === 3 && cellError[record.STT] && cellError[record.STT].indexOf('3') !== -1
              ? 'min-h-38 errorBackGround py-2'
              : ''
        })
      },
      {
        title: 'Mô tả',
        dataIndex: 'MoTa',
        key: 'MoTa',
        onCell: (record) => ({
          className:
            step === 3 && cellError[record.STT] && cellError[record.STT].indexOf('4') !== -1
              ? 'min-h-38 errorBackGround py-2'
              : ''
        })
      },
      {
        title: (step === 2 && 'Hành động') || (step === 3 && 'Nội dung lỗi'),
        dataIndex: 'actions',
        key: 'actions',
        render: (value, record) => {
          return (
            <>
              {step === 2 && (
                <button
                  className='mx-2'
                  onClick={(e) => {
                    e.stopPropagation();
                    handleDeleteRow(record.STT);
                  }}
                >
                  <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
                </button>
              )}
              {step === 3 && <div style={{ color: 'red' }}>{record.actions}</div>}
            </>
          );
        }
      }
    ];
  }, [cellError, listTacGiaExcel, page, paginationSize, step]);

  const handleOnFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files?.[0];
    PreviewImport(fileFromLocal as File);
  };

  const handleAccessFileInputRef = () => {
    fileInutRef.current?.click();
  };

  const handleBack = () => {
    navigate(path.tacgia);
  };

  const handleBackCurrent = () => {
    setListTacGiaExcel([]);
    setStep(1);
  };

  return (
    <div className='p-5' ref={ref}>
      <Title title='Thêm tác giả từ file Excel' />

      <div className='my-10'>
        <ProgressBar activeStep={step} type='excel' />
      </div>

      {/* TODO: Condition are step > 1 and data is not undefined*/}
      {step === 1 && (
        <div className='flex items-center justify-center rounded-md bg-tertiary-10 py-4'>
          <a href={`/Tempalates/MauTG.xls`} download='MauTG'>
            <Button className='bg-tertiary-20 hover:bg-tertiary-20/30'>
              <DownloadOutlined />
              File excel mẫu
            </Button>
          </a>

          <input
            type='file'
            accept='.csv,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel'
            className='hidden'
            ref={fileInutRef}
            onChange={handleOnFileChange}
          />

          <Button variant='default' className='ml-2' onClick={handleAccessFileInputRef}>
            Chọn tệp
          </Button>
        </div>
      )}

      {step > 1 && (
        <div>
          {step === 2 && (
            <>
              <p>
                Tổng số tác giả: <span className='font-bold'>{listTacGiaExcel?.length}</span>
              </p>

              {!!listTacGiaExcel?.length && <h3 className='text-primary-10'>DANH SÁCH TÁC GIẢ</h3>}
            </>
          )}

          {step === 3 && (
            <>
              <p className='text-tertiary-30'>
                Lưu thành công: <span className='font-bold'>{listSuccess.length} tác giả</span>
              </p>

              <p className='flex items-center gap-1 text-danger-10'>
                Lưu thất bại: <span className='font-bold'> {' ' + listFail.length} tác giả</span>{' '}
                {listFail.length > 0 && (
                  <Button
                    variant='danger'
                    className='ml-2'
                    onClick={() => {
                      saveByteArray(tempFile, 'DsTacGiaBiLoi.xls', 'application/xls');
                    }}
                  >
                    <DownloadOutlined />
                    Tải file lỗi
                  </Button>
                )}
              </p>
              {listFail.length > 0 && <h3 className='uppercase text-primary-10'>Danh sách tác giả bị lỗi</h3>}
            </>
          )}

          {((step === 2 && !!listTacGiaExcel?.length) || (step === 3 && listFail.length > 0)) && (
            <div className='relative mt-6'>
              <Table
                loading={isLoadingPreviewImport && isLoadingImportSave}
                columns={columns}
                pagination={{
                  onChange(current, pageSize) {
                    setPage(current);
                    setPaginationSize(pageSize);
                  },
                  defaultPageSize: 10,
                  hideOnSinglePage: true,
                  showSizeChanger: false
                }}
                dataSource={listTacGiaExcel}
                scroll={{ x: 980 }}
                rowKey={(record) => record.STT}
                className='custom-table'
                bordered
              />
            </div>
          )}
        </div>
      )}

      <div className='mr-10 mt-6 flex items-center justify-end'>
        {step === 1 && (
          <>
            <Button variant='secondary' onClick={handleBack}>
              Quay về
            </Button>
          </>
        )}

        <>
          {step === 2 && !isLoadingPreviewImport && (
            <>
              <Button variant='secondary' onClick={handleBackCurrent}>
                Quay về
              </Button>
              <Button variant='default' className='ml-2' onClick={handleSubmit}>
                Tiếp tục
              </Button>
            </>
          )}

          {step === 3 && !isLoadingImportSave && (
            <Button variant='default' className='ml-2' onClick={handleBack}>
              Hoàn tất
            </Button>
          )}
        </>
      </div>
      <Loading open={isLoadingPreviewImport || isLoadingImportSave} />
    </div>
  );
};

export default AddAuthorByExcelPage;
