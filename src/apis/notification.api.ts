import { isEmpty, omitBy } from 'lodash';
import httpThuVien from 'utils/httpThuVien';

const endpoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/ThongBao`;

const notificationApis = {
  addNewNotification: (payload: FormData) => {
    return httpThuVien.post<ResponseApi>(`/${endpoint}/Create`, payload, {
      headers: { 'Content-Type': 'multipart/form-data', Accept: '*/*' }
    });
  },
  updateNewNotification: (payload: FormData) => {
    return httpThuVien.post<ResponseApi>(`/${endpoint}/Edit`, payload, {
      headers: { 'Content-Type': 'multipart/form-data', Accept: '*/*' }
    });
  },
  getNotifications: (payload: {
    page: number;
    pageSize: number;
    TextForSearch?: string;
    day?: number;
    month?: number;
    year?: number;
    IsActive?: string | boolean;
  }) => {
    return httpThuVien.post<ResponseApi<{ ListThongBao: NotificationApp[]; count: number }>>(
      `/${endpoint}/IndexByModel`,
      payload
    );
  },
  getNotification: (id: string) => httpThuVien.post<ResponseApi<NotificationApp>>(`/${endpoint}/EditByID?Id=${id}`),
  changeStatus: (payload: { Id: string; IsActive: boolean }) =>
    httpThuVien.post<ResponseApi>(`/${endpoint}/ChangeStatus`, payload),
  removeNoti: (id: string) => httpThuVien.post<ResponseApi>(`/${endpoint}/Delete?id=${id}`)
};

export default notificationApis;
