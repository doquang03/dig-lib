import classNames from 'classnames';
import { ModalDelete } from 'components';
import { InfoCircleOutlined } from '@ant-design/icons';

type Props = {
  books?: Book[];
  onCancel: () => void;
  onOk: () => void;
  loading: boolean;
  visible: boolean;
  title?: string;
  subtitle?: string;
  isThanhLy?: boolean;
  labelOK?: string;
};

const DeleteModal = (props: Props) => {
  const {
    title = 'Bạn có muốn chắc chắn xóa những sách này không?',
    subtitle = ' Bạn sẽ không thể khôi phục sau khi xóa, những sách đang được mượn sẽ không thể xóa.',
    isThanhLy = false,
    books,
    onCancel,
    onOk,
    loading,
    visible,
    labelOK = 'Xóa sách'
  } = props;

  return (
    <ModalDelete
      open={visible}
      closable={false}
      title={
        <>
          <h3 className='text-bold'>{title}</h3>
          <p className='text-[16px] font-extralight leading-normal'>{subtitle}</p>
        </>
      }
      handleCancel={onCancel}
      handleOk={onOk}
      loading={loading}
      labelOK={labelOK}
    >
      <div className='max-h-[300px] overflow-x-auto'>
        {books?.map(({ TenSach, MaKiemSoat, SoLuongConLai = 0, SoLuongTong = 0 }, index) => {
          const isPossibleDelete =
            (!isThanhLy ? SoLuongTong >= 0 && SoLuongConLai === SoLuongTong : SoLuongTong >= 0 && SoLuongConLai >= 0) &&
            SoLuongConLai === 0;

          return (
            <div key={MaKiemSoat} className='grid grid-cols-12 items-center border-b-2 px-2 py-2 font-bold'>
              <div className='col-span-1'>
                <p className={!isPossibleDelete ? 'text-danger-10' : ''}>{index + 1} </p>
              </div>
              <div className='col-span-5'>
                <p
                  className={classNames('truncate text-left', {
                    'text-danger-10': !isPossibleDelete
                  })}
                >
                  {TenSach}
                </p>
              </div>
              <div className='col-span-6 grid grid-cols-12'>
                <div className='col-span-6'>
                  <p
                    className={classNames('mr-4', {
                      'text-danger-10': !isPossibleDelete
                    })}
                  >
                    {MaKiemSoat}
                  </p>
                </div>
                <div className='col-span-6'>
                  {!isPossibleDelete && (
                    <p className='flex items-center gap-1 font-light text-danger-10'>
                      <InfoCircleOutlined style={{ color: 'red' }} />

                      <span className='text-[12px]'>Không thể xóa</span>
                    </p>
                  )}
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </ModalDelete>
  );
};

export default DeleteModal;
