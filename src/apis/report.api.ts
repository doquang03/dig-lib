import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/ExportReport`;

const reportApis = {
  exportTTSach: (year: number) =>
    httpThuVien.post(`${endPoint}/ExportTTSach?year=${year}`, {}, { responseType: 'arraybuffer' }),
  exportSLSachTT: () => httpThuVien.post(`${endPoint}/ExportSLSachTT`, {}, { responseType: 'arraybuffer' }),
  exportBBKTTV: (_workPlaceId?: string) =>
    httpThuVien.post(`${endPoint}/ExportBBKTTV?_workPlaceId=${_workPlaceId}`, {}, { responseType: 'arraybuffer' })
};

export default reportApis;
