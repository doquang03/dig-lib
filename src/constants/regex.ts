export const regex = {
  excludeNumber: /^\D+$/,
  excludeSpecialCharacters: /^[A-Za-z0-9 ]+$/,
  vietnamesePhoneNumber: /^(\s*|(84|0[3|5|7|8|9])+([0-9]{8}))$/,
  emailAccess: /^(\s*|[\w-\.]+@([\w-]+\.)+[\w-]{2,4})$/,
  excludeString:
    /^[^a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$/
};
