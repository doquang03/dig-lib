import { CheckOutlined } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { DatePicker, Modal, Table, Tag } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { authApis, statistic } from 'apis';
import { Button, Empty, Loading, Title } from 'components';
import { useUser } from 'contexts/user.context';
import dayjs from 'dayjs';
import { useRef, useState } from 'react';
import { useReactToPrint } from 'react-to-print';
import { toast } from 'react-toastify';
import { LocalStorageEventTartget } from 'utils/auth';
import { download, saveUnitToSS } from 'utils/utils';

const { RangePicker } = DatePicker;

type RangeValue = Parameters<NonNullable<React.ComponentProps<typeof DatePicker.RangePicker>['onChange']>>[0];

const EducationDepartment = () => {
  const [searchParams, setSearchParams] = useState<{
    TuNgay: string;
    DenNgay: string;
  }>({
    TuNgay: dayjs().startOf('year').startOf('day').toISOString(),
    DenNgay: dayjs().endOf('day').toISOString()
  });

  const ref = useRef(null);

  const { userType, profile } = useUser();

  const [openModal, setOpenModal] = useState(false);

  const [selectDatabase, setSelectDatabase] = useState<Unit>();

  const handlePrint = useReactToPrint({
    content: () => ref.current
  });

  const { data, isLoading } = useQuery({
    queryKey: ['subdomains'],
    queryFn: () => authApis.ListChildActivated(profile?.IdDVCT_Working + ''),
    enabled: Boolean(profile?.IdDVCT_Working)
  });

  const { data: dataLibrary, isLoading: isLoadingLibrary } = useQuery({
    queryKey: ['dataLibrary', searchParams],
    queryFn: () =>
      statistic.GetDataFromDatabaseName(
        data?.data.Item.map((x: any) => x?.DatabaseName),
        searchParams.TuNgay,
        searchParams.DenNgay
      ),
    enabled: !!data
  });
  const { mutateAsync: setDbName, isLoading: loadingSetDbName } = useMutation({
    mutationFn: statistic.setDatabaseName
  });

  const { mutate: downloadData, isLoading: loadingDownloadData } = useMutation({
    mutationFn: () =>
      statistic.DownloadDataFromDatabaseName({
        ListDatabase: data?.data.Item.map((x: any) => x?.DatabaseName),
        ListName: data?.data.Item.map((x: any) => x?.Ten),
        TuNgay: searchParams.TuNgay,
        DenNgay: searchParams.DenNgay
      }),
    onSuccess: (res, fileName) => {
      download('Danh sách đơn vị con.xls', res.data);
    }
  });

  const getDataFromRecord = (record: Unit | undefined) => {
    if (record) {
      const dataBase = dataLibrary?.data?.Item?.filter((x) => x.DataBaseName === record?.DatabaseName);
      if (dataBase?.length === 1) return dataBase[0];
    }
    return undefined;
  };

  const columns: ColumnsType<Unit> = [
    {
      key: 'STT',
      title: 'STT',
      dataIndex: 'stt',
      render: (value, record, index) => index + 1
    },
    {
      key: 'Ten',
      title: 'Tên đơn vị',
      dataIndex: 'Ten',
      sorter: (a, b) => ('' + a?.Ten).localeCompare(b?.Ten),
      sortDirections: ['descend', 'ascend']
    },
    {
      key: 'TongSoSachGiay',
      title: 'Tổng số sách giấy',
      dataIndex: 'TongSoSachGiay',
      sorter: (a, b) => (getDataFromRecord(b)?.TongSoSach || 0) - (getDataFromRecord(a)?.TongSoSach || 0),
      render: (value, record) => {
        const data = getDataFromRecord(record);
        return data ? data?.TongSoSach : '--';
      }
    },
    {
      key: 'TongSoSachSo',
      title: 'Tổng số sách số',
      dataIndex: 'TongSoSachSo',
      sorter: true,
      render: (value, record) => {
        return '--';
      }
    },
    {
      key: 'Thông tin mượn sách',
      title: 'Thông tin mượn sách',
      dataIndex: 'Thông tin mượn sách',
      children: [
        {
          key: 'Lượt mượn sách',
          title: 'Lượt mượn sách',
          dataIndex: 'Lượt mượn sách',
          sorter: (a, b) =>
            (getDataFromRecord(b)?.TongLuotMuonHS || 0) +
            (getDataFromRecord(b)?.TongLuotMuonGV || 0) -
            (getDataFromRecord(a)?.TongLuotMuonHS || 0) +
            (getDataFromRecord(a)?.TongLuotMuonGV || 0),
          render: (value, record) => {
            const data = getDataFromRecord(record);
            return data ? data?.TongLuotMuonHS + ' hs | ' + data?.TongLuotMuonGV + ' gv' : '--';
          }
        },
        {
          key: 'Đang mượn trễ hạn',
          title: 'Đang mượn trễ hạn',
          dataIndex: 'Đang mượn trễ hạn',
          sorter: (a, b) =>
            (getDataFromRecord(b)?.DangMuonTreHanHS || 0) +
            (getDataFromRecord(b)?.DangMuonTreHanGV || 0) -
            (getDataFromRecord(a)?.DangMuonTreHanHS || 0) +
            (getDataFromRecord(a)?.DangMuonTreHanGV || 0),
          render: (value, record) => {
            const data = getDataFromRecord(record);
            return data ? data?.DangMuonTreHanHS + ' hs | ' + data?.DangMuonTreHanGV + ' gv' : '--';
          }
        },
        {
          key: 'Trả sách đúng hạn',
          title: 'Trả sách đúng hạn',
          dataIndex: 'Trả sách đúng hạn',
          sorter: (a, b) =>
            (getDataFromRecord(b)?.TraSachDungHanHS || 0) +
            (getDataFromRecord(b)?.TraSachDungHanGV || 0) -
            (getDataFromRecord(a)?.TraSachDungHanHS || 0) +
            (getDataFromRecord(a)?.TraSachDungHanGV || 0),
          render: (value, record) => {
            const data = getDataFromRecord(record);
            return data ? data?.TraSachDungHanHS + ' hs | ' + data?.TraSachDungHanGV + ' gv' : '--';
          }
        },
        {
          key: 'Trả sách trễ hạn',
          title: 'Trả sách trễ hạn',
          dataIndex: 'Trả sách trễ hạn',
          sorter: (a, b) =>
            (getDataFromRecord(b)?.TraSachTreHanHS || 0) +
            (getDataFromRecord(b)?.TraSachTreHanGV || 0) -
            (getDataFromRecord(a)?.TraSachTreHanHS || 0) +
            (getDataFromRecord(a)?.TraSachTreHanGV || 0),
          render: (value, record) => {
            const data = getDataFromRecord(record);
            return data ? data?.TraSachTreHanHS + ' hs | ' + data?.TraSachTreHanGV + ' gv' : '--';
          }
        },
        {
          key: 'Mượn quá hạn',
          title: 'Mượn quá hạn',
          dataIndex: 'Mượn quá hạn',
          sorter: (a, b) =>
            (getDataFromRecord(b)?.MuonSachQuaHanHS || 0) +
            (getDataFromRecord(b)?.MuonSachQuaHanGV || 0) -
            (getDataFromRecord(a)?.MuonSachQuaHanHS || 0) +
            (getDataFromRecord(a)?.MuonSachQuaHanGV || 0),
          render: (value, record) => {
            const data = getDataFromRecord(record);
            return data ? data?.MuonSachQuaHanHS + ' hs | ' + data?.MuonSachQuaHanGV + ' gv' : '--';
          }
        },
        {
          key: 'Làm mất sách',
          title: 'Làm mất sách',
          dataIndex: 'Làm mất sách',
          sorter: (a, b) =>
            (getDataFromRecord(b)?.LamMatSachHS || 0) +
            (getDataFromRecord(b)?.LamMatSachGV || 0) -
            (getDataFromRecord(a)?.LamMatSachHS || 0) +
            (getDataFromRecord(a)?.LamMatSachGV || 0),
          render: (value, record) => {
            const data = getDataFromRecord(record);
            return data ? data?.LamMatSachHS + ' hs | ' + data?.LamMatSachGV + ' gv' : '--';
          }
        }
      ]
    },
    {
      key: 'details',
      title: '',
      dataIndex: 'details',
      render: (value, record) => (
        <Button
          variant='primary-outline'
          onClick={(e) => {
            e.stopPropagation();
            handleChooseUnit(record);
          }}
        >
          Xem chi tiết
        </Button>
      )
    }
  ];

  const handleChangeRangePicker = (values: RangeValue) => {
    if (!values) return;

    setSearchParams({
      TuNgay: values?.[0]?.startOf('day').toISOString() as string,
      DenNgay: values?.[1]?.endOf('day').toISOString() as string
    });
  };

  const handleChooseUnit = (unit: Unit) => {
    if (!profile) return;

    setDbName({
      idUser: profile?.Id,
      DatabaseName: unit.DatabaseName,
      EnableEdit: unit.Id === profile?.IdDVCT_TamThoi,
      tenDonVi: unit.Ten
    }).then(() => {
      const addUnitEvent = new Event('addUnit');
      LocalStorageEventTartget.dispatchEvent(addUnitEvent);

      saveUnitToSS(Number(userType), {
        ...unit,
        isAllowedAdjustment: unit.Id === (profile?.IdDVCT_TamThoi || profile?.IdDVCT_Chinh)
      });

      toast.success(`Truy cập ${unit?.Ten} thành công`);
    });
  };

  return (
    <div>
      <form className='mt-3 flex flex-1 items-center gap-1 bg-primary-50/30 p-2.5'>
        <RangePicker
          className='w-1/4 border border-gray-300 py-[0.8rem] px-2 outline-none'
          format={['DD/MM/YYYY', 'DD-MM-YYYY']}
          defaultValue={[dayjs().startOf('year'), dayjs()]}
          placeholder={['Ngày bắt đầu', 'Ngày kết thúc']}
          disabledDate={(current) => {
            return current && current > dayjs().endOf('day');
          }}
          onChange={handleChangeRangePicker}
        />
      </form>

      <div className='mt-3 flex items-center gap-2'>
        <Button>Tổng hợp sổ sách</Button>

        <Button onClick={handlePrint}>In danh sách</Button>

        <Button
          onClick={() => {
            downloadData();
          }}
        >
          Tải file excel
        </Button>
      </div>

      <Table
        ref={ref}
        columns={columns}
        className='custom-table mt-3'
        bordered
        locale={{
          emptyText: () => <Empty />
        }}
        scroll={{ x: true }}
        loading={isLoading || isLoadingLibrary}
        dataSource={data?.data.Item || []}
        rowKey={(row) => row.Id}
        onRow={(record, rowIndex) => {
          return {
            onClick: (event) => {
              setOpenModal(true);
              setSelectDatabase(record);
            } // click row
          };
        }}
        pagination={{ hideOnSinglePage: true }}
        showSorterTooltip={{
          title: ''
        }}
      />
      <Modal open={openModal} title={<Title title={selectDatabase?.Ten || ''} />} closable={false} footer={null}>
        <div className='mb-4 flex flex-col'>
          <div className='mt-3 flex flex-row items-center gap-2 text-base'>
            <p className='flex items-center text-danger-10'>
              <CheckOutlined className='mr-1' />
              Tổng số kho :{' '}
              <span className='ml-1 font-bold'>{getDataFromRecord(selectDatabase)?.listKhoSach?.length || 0}</span>
            </p>
            <p className='flex grow items-center justify-end text-danger-10'>
              <CheckOutlined className='mr-1' />
              Tổng số sách cá biệt :{' '}
              <span className='ml-1 font-bold'>{getDataFromRecord(selectDatabase)?.TongSoSach || 0}</span>
            </p>
          </div>
        </div>

        {getDataFromRecord(selectDatabase)?.listKhoSach.map((x) => {
          return (
            <div className='mt-3 flex flex-row items-center gap-2 border-b-2 pb-2'>
              <span className='grow text-base'>{x.Ten}</span>
              <Tag className='mx-2.5 rounded-3xl px-5 text-base' color='#3b5999'>
                {x.CountBook}
              </Tag>
            </div>
          );
        })}

        <div className='mt-3 flex items-center justify-end gap-2'>
          <Button type='button' variant='secondary' onClick={() => setOpenModal(false)}>
            Quay về
          </Button>
        </div>
      </Modal>
      <Loading open={loadingSetDbName || loadingDownloadData} />
    </div>
  );
};

export default EducationDepartment;
