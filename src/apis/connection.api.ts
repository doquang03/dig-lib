import httpThuVien from 'utils/httpThuVien';
import { serialize } from 'utils/utils';

const endPoint = `${process.env.REACT_APP_SERVICE_MLLH}/api/HostLibrary`;
const enpointUnit = `${process.env.REACT_APP_SERVICE_MLLH}/api/MemberConnection`;

type Common = { page: number; pageSize: number };

export type GetListOfUnitsVar = Common &
  Partial<{
    TextForSearch: string;
    Available: number;
    Status: string;
    ConnectionTime: string;
    SortConnectionTime: boolean;
    SortExpiredTime: boolean;
    SortTotalRecord: boolean;
  }>;

export type GetListOfKeysVar = Common & Partial<{ TextForSearch?: string; Available: boolean }>;

type InteractKeyVars = {
  Id?: string;
  NameMember: string;
  WorkingId: string;
  Key: string;
  ListTimePackages: { ExtensionTime: number | null }[];
  IsActive: boolean;
};

export type DetailedKeyData = {
  ExpiredTime: string;
  UnitWorkingId: string;
} & Pick<UnitKey, 'IdKey' | 'IsActive' | 'NameMember' | 'Value'>;

const connectionApis = {
  /* Host zone */
  getListOfUnitDeparmentEdu: () => {
    return httpThuVien.get<ResponseApi<{ DonVi: Option[] }>>(`/${endPoint}/ListDonVi`);
  },
  getListOfUnitDeparmentEduFull: () => {
    return httpThuVien.get<ResponseApi<{ DonVi: Option[] }>>(`/${endPoint}/ListHostLibrary`);
  },
  getListModelUnitWorkingId: () => {
    return httpThuVien.get<ResponseApi<{ DonVi: Option[] }>>(`/${endPoint}/ListModelUnitWorkingId`);
  },
  GetHostLibrary: () => {
    return httpThuVien.get<ResponseApi<{ WorkingId: string }>>(`/${endPoint}/GetHostLibrary`);
  },
  SetHostLibrary: (WorkingId: string) => {
    return httpThuVien.post<ResponseApi>(`/${endPoint}/SetHostLibrary?WorkingId=${WorkingId}`);
  },
  generateKey: () => {
    return httpThuVien.post<ResponseApi<{ Key: string }>>(`/${endPoint}/GenerateKey`);
  },
  assignKeyForSpecificUnit: (params: InteractKeyVars) =>
    httpThuVien.post<ResponseApi>(`/${endPoint}/CreateKey`, params),
  getListOfKeys: (params: GetListOfUnitsVar) =>
    httpThuVien.post<ResponseApi<{ Key: Key[]; Count: number }>>(`/${endPoint}/ListUnit`, params),
  getListOfUnitKeys: (params: GetListOfKeysVar) =>
    httpThuVien.post<ResponseApi<{ Key: UnitKey[]; Count: number }>>(`/${endPoint}/ListKey`, params),
  getDetailedKey: (id: string) => httpThuVien.get<ResponseApi<DetailedKeyData>>(`/${endPoint}/Detail/${id}`),
  updateDetailedKey: (params: InteractKeyVars) => httpThuVien.put<ResponseApi>(`/${endPoint}/UpdateKey`, params),
  getDetailsUnitPermission: (params: { Id: string }) =>
    httpThuVien.get<ResponseApi<{ ListModel: UnitPermission[] }>>(`/${endPoint}/DetailPermission?${serialize(params)}`),
  updateApprovalRecordPermission: (params: { WorkingId: string; MemberName: string; Enable: boolean }) =>
    httpThuVien.post<ResponseApi>(`/${endPoint}/ChangePermissionUpload`, params),
  getPermissionApprovalUpload: (params: { WorkingId: string; MemberName: string }) =>
    httpThuVien.post<ResponseApi<Pick<UnitInHost, 'WorkingId' | 'MemberName'> & { Enble: boolean }>>(
      `/${endPoint}/GetPermissionUpload?${serialize(params)}`
    ),

  /* Unit zone */
  connectToHost: (key: string) => httpThuVien.post<ResponseApi>(`/${enpointUnit}/ConnectKey`, key),
  getUnitConnectionStatus: () => httpThuVien.get<ResponseApi<{ connection: boolean }>>(`/${enpointUnit}/GetConnection`),
  getUnitsInHost: () =>
    httpThuVien.get<ResponseApi<{ ListModel: UnitInHost[] }>>(`/${enpointUnit}/ListOfUnitsInCentralLibrary`),
  disconnect: () => httpThuVien.get<ResponseApi>(`/${enpointUnit}/Disconnect`),
  adjustPermission: (params: { WorkingId: string; MemberName: string; Passive?: boolean; Proactive?: boolean }) =>
    httpThuVien.post<ResponseApi>(`/${enpointUnit}/ChangePermission`, params),
  GetPermissionHostLibrary: () => {
    return httpThuVien.get<ResponseApi<{ Enable: boolean; EnableDigital: boolean }>>(
      `/${enpointUnit}/GetPermissionHostLibrary`
    );
  },
  getListExtensionTimePackages: (id: string) =>
    httpThuVien.post<
      ResponseApi<{
        Value: string;
        CreateDateTime: string;
        ListModel: { ExtensionTime: number; ExpiredTime: any; Id: string }[];
      }>
    >(`${endPoint}/ListTimePackages/${id}`),
  cancelExtensionTimePackage: (id: string) => httpThuVien.post<ResponseApi>(`${endPoint}/CancelTimePackage/${id}`),
  getKeyType: () => httpThuVien.get<ResponseApi<{ TypeKey: string }>>(`${endPoint}/GetTypeKey`),
  setKeyType: (type: string) => httpThuVien.post<ResponseApi>(`${endPoint}/SettingTypeKey?type=${type}`)
};

export default connectionApis;
