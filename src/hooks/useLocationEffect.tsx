import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';

function useLocationEffect(callback: () => void) {
  const location = useLocation();

  useEffect(() => {
    callback();

    return () => {
      callback();
    };
  }, [location.pathname]);
}

export default useLocationEffect;
