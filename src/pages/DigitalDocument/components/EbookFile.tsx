import { CloseSquareFilled } from '@ant-design/icons';
import { memo } from 'react';
import { useParams } from 'react-router-dom';

type Props = {
  listOfFiles?: DetailDigitalDocument['ListMedia'];
  filesPptPdf?: string[];
  onDelete: VoidFunction;
  onDeleteExistedImage: (index: number) => void;
  onDeletePreviewImage: (index: number) => void;
  filesPreview?: File[];
};

function EbookFile(props: Props) {
  const { listOfFiles, filesPptPdf, filesPreview, onDelete, onDeleteExistedImage, onDeletePreviewImage } = props;

  const { documentType } = useParams<{ documentType: 'SachDienTu' | 'AlbumAnh' | 'Audio' | 'Video' }>() || {};

  return (
    <div className='mb-3 grid w-full grid-cols-12 gap-3'>
      {/* data from db */}
      {/* type of file is ppt or pdf */}
      {!!listOfFiles?.ListFile?.length && !listOfFiles?.isImage && documentType === 'SachDienTu' ? (
        <div
          className='relative col-span-6 h-[170px] w-[full] shadow-md md:col-span-4 md:col-start-4'
          key={listOfFiles.ListFile?.[0].LinkFile}
        >
          <button className='absolute right-0 -top-1 shadow-lg' type='button' onClick={onDelete}>
            <CloseSquareFilled style={{ color: 'red' }} />
          </button>

          {filesPptPdf && ['pptx', 'ppt'].includes(filesPptPdf?.[filesPptPdf?.length - 1].toLocaleLowerCase()) && (
            <a href={listOfFiles.ListFile?.[0].LinkFile} target='_blank' rel='noreferrer'>
              <img
                src={'/content/ppt.png'}
                alt={listOfFiles.ListFile?.[0]?.FileName}
                className='h-full w-full shadow-md'
              />
            </a>
          )}

          {filesPptPdf && ['pdf'].includes(filesPptPdf?.[filesPptPdf?.length - 1].toLocaleLowerCase()) && (
            <a href={listOfFiles.ListFile?.[0].LinkFile} target='_blank' rel='noreferrer'>
              <img
                src={'/content/pdf.png'}
                alt={listOfFiles.ListFile?.[0]?.FileName}
                className='h-full w-full shadow-md'
              />
            </a>
          )}
        </div>
      ) : (
        <>
          {/* list of images */}
          {documentType === 'AlbumAnh' &&
            listOfFiles?.ListFile.map((file, index) => (
              <div className='relative col-span-6 h-[170px] w-[full] md:col-span-4' key={file.PageNumber}>
                <button
                  className='absolute right-0 -top-1 shadow-lg'
                  type='button'
                  onClick={() => onDeleteExistedImage(index)}
                >
                  <CloseSquareFilled style={{ color: 'red' }} />
                </button>

                <a href={file.LinkFile} target='_blank' rel='noreferrer'>
                  <img src={file.LinkFile} alt={file.FileName} className='h-full w-full shadow-md' />
                </a>
              </div>
            ))}
        </>
      )}

      {/* files from local */}
      {filesPreview?.map((file, index) => {
        const src = URL.createObjectURL(file);
        const fileType = file.name.split('.');

        return (
          <>
            {documentType === 'AlbumAnh' && (
              <div className='relative col-span-6 h-[170px] w-[full] md:col-span-4' key={index}>
                <button
                  className='absolute right-0 -top-1 shadow-lg'
                  type='button'
                  onClick={() => onDeletePreviewImage(index)}
                >
                  <CloseSquareFilled style={{ color: 'red' }} />
                </button>

                {/* image */}
                {Boolean(URL.createObjectURL(file)) && file.type.includes('image') && (
                  <a href={URL.createObjectURL(file)} target='_blank' rel='noreferrer'>
                    <img
                      src={URL.createObjectURL(file)}
                      alt={file.name}
                      className='h-full w-full shadow-md'
                      key={file.name}
                    />
                  </a>
                )}
              </div>
            )}
            {documentType === 'SachDienTu' && (
              <div className='relative col-span-6 h-[170px] w-[full] md:col-span-4 md:col-start-4' key={index}>
                <button
                  className='absolute right-0 -top-1 shadow-lg'
                  type='button'
                  onClick={() => onDeletePreviewImage(index)}
                >
                  <CloseSquareFilled style={{ color: 'red' }} />
                </button>
                <>
                  {/* pdf */}
                  {Boolean(src) && file.type.toLocaleLowerCase().includes('pdf') && (
                    <a href={src} target='_blank' rel='noreferrer'>
                      <img src={'/content/pdf.png'} alt={file.name} className='h-full w-full shadow-md' />
                    </a>
                  )}
                  {/* ppt */}
                  {Boolean(src) &&
                    (fileType?.[fileType.length - 1] === 'pptx' || fileType?.[fileType.length - 1] === 'ppt') && (
                      <a href={src} target='_blank' rel='noreferrer'>
                        <img src={'/content/ppt.png'} alt={file.name} className='h-full w-full shadow-md' />
                      </a>
                    )}
                </>
              </div>
            )}
          </>
        );
      })}
    </div>
  );
}

export default memo(EbookFile);
