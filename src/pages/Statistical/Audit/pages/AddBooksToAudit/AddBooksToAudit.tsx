import { FilterFilled } from '@ant-design/icons';
import { useQuery } from '@tanstack/react-query';
import { Table, Tooltip } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { bookStoreApis, bookstoreTypeApis } from 'apis';
import classNames from 'classnames';
import { Button, ColumnsTitleSorter, Input, Select, SizeChanger, Title } from 'components';
import { path } from 'constants/path';
import { AuditContext } from 'contexts/audit';
import { useBookStatuses, useOutsideClick, useQueryParams } from 'hooks';
import { debounce, isEmpty, omitBy } from 'lodash';
import { ChangeEvent, Key, useContext, useMemo, useState } from 'react';
import { createSearchParams, useLocation, useNavigate } from 'react-router-dom';
import { getSerialNumber } from 'utils/utils';

type SorterColumns = 'MaDKCB' | 'IdTinhTrang' | 'TenSach' | 'TacGia' | 'NamXuatBan' | 'ISBN' | 'IdKho';

const initialOrderBy = {
  MaDKCB: true,
  GiaTien: true,
  TenSach: true,
  TacGia: true,
  IsBN: true,
  NamXuatban: true
};

const AddBooksToAudit = () => {
  const [selectedBooks, setSelectedBooks] = useState<BooksInStoreAudit[]>([]);
  const [{ page, pageSize }, setPaging] = useState<{ page: number; pageSize: number }>({ page: 1, pageSize: 30 });
  const [visibleSorter, setVisibleSorter] = useState<boolean>(false);
  const [sorterColumns, setSorterColumns] = useState<SorterColumns>();
  const [visibleTrangThaiSorter, setVisibleTrangThaiSorter] = useState<boolean>(false);
  const [visibleKhoSorter, setVisibleKhoSorter] = useState<boolean>(false);
  const [orderBy, setOrderBy] = useState<Partial<typeof initialOrderBy>>(initialOrderBy);
  const [searchForm, setSearchForm] = useState<{
    searchText?: string;
    MaDKCB?: string;
    TenSach?: string;
    TacGia?: string;
    idKho?: string;
    NamXuatban?: string;
    ISBN?: string;
    IdKho?: string;
    IdTinhTrang?: string;
  }>();

  const refTrangThai = useOutsideClick<HTMLDivElement | null>(() => setVisibleTrangThaiSorter(false));
  const refKho = useOutsideClick<HTMLDivElement | null>(() => setVisibleKhoSorter(false));

  const { state } = useLocation();
  const auditId: string = state ? state : undefined;

  const { selectedBooks: _selectedBooksContext, setSelectedBooks: setSelectedBooksContext } = useContext(AuditContext);

  const { data: bookStatuses, isLoading: loadingBookStatuses } = useBookStatuses();

  const navigate = useNavigate();

  const queryParams = useQueryParams();

  const {
    data: bookInStoreData,
    isLoading: loadingBooksInStore,
    isFetching: fetchingBooksInStore
  } = useQuery({
    queryKey: ['booksInStoreToAudit', orderBy, searchForm, page, pageSize, _selectedBooksContext],
    queryFn: () => {
      let bodyReq: Partial<{
        MaDKCB: SearchInputField;
        TenSach: SearchInputField;
        TacGia: SearchInputField;
        NamXuatBan: SearchInputField;
        ISBN: SearchInputField;
        searchText: string;
      }> = {};

      if (searchForm?.searchText) {
        bodyReq.searchText = searchForm.searchText;
      }

      // if (searchForm?.TenSach) {
      //   bodyReq.TenSach = {
      //     truongSearch: searchForm?.TenSach,
      //     operatorSearch: 'contains'
      //   };
      // }

      // if (searchForm?.TacGia) {
      //   bodyReq.TacGia = {
      //     truongSearch: searchForm?.TacGia,
      //     operatorSearch: queryParams['TacGiaOperator']
      //   };
      // }

      if (searchForm?.NamXuatban) {
        bodyReq.NamXuatBan = {
          truongSearch: searchForm?.NamXuatban,
          operatorSearch: 'equals'
        };
      }

      // if (searchForm?.ISBN) {
      //   bodyReq.ISBN = {
      //     truongSearch: searchForm.ISBN,
      //     operatorSearch: 'contains'
      //   };
      // }

      return bookStoreApis.danhSachTrongKho({
        SortDKCB: orderBy.MaDKCB,
        SortTenSach: orderBy.TenSach,
        SortTacGia: orderBy.TacGia,
        SortNamXuatBan: orderBy.NamXuatban,
        SortISBN: orderBy.IsBN,
        IdKhoSach: searchForm?.IdKho,
        IdTinhTrang: searchForm?.IdTinhTrang,
        page,
        pageSize,
        currentId:
          _selectedBooksContext && _selectedBooksContext?.length
            ? _selectedBooksContext.map(({ IdSachCaBiet }) => IdSachCaBiet)
            : [],
        ...bodyReq
      });
    }
  });

  const booksInstore = useMemo(() => {
    if (!bookInStoreData?.data.Item.ListSCB.length) return;

    return bookInStoreData?.data.Item;
  }, [bookInStoreData]);

  const { data: storesData, isLoading: loadingStores } = useQuery({
    queryKey: ['stores'],
    queryFn: bookstoreTypeApis.getAllStores,
    staleTime: Infinity
  });

  const storesResult = useMemo(() => {
    if (!storesData?.data.Item) return [];

    const storeResults = storesData?.data.Item.map(({ Ten, Id }) => {
      return {
        label: Ten,
        value: Id
      };
    });

    return storeResults;
  }, [storesData?.data.Item]);

  const items = (field: SorterColumns) => {
    const fieldName: Record<SorterColumns, string> = {
      MaDKCB: 'Số ĐKCB',
      TenSach: 'Tên sách',
      IdKho: 'Kho sách',
      IdTinhTrang: 'Tình trạng sách',
      ISBN: 'ISBN',
      NamXuatBan: 'Năm xuất bản',
      TacGia: 'Tác giả'
    };

    const handleOrderby = (value: boolean) => {
      setOrderBy({ [field]: value });
    };

    return [
      {
        key: 'decrease',
        label: (
          <button className='w-full flex-1' onClick={() => handleOrderby(false)}>
            <span>{fieldName[field]} giảm dần</span>
          </button>
        )
      },
      {
        key: 'increase',
        label: (
          <button className='w-full flex-1' onClick={() => handleOrderby(true)}>
            <span>{fieldName[field]} tăng dần</span>
          </button>
        )
      },
      {
        key: 'filter',
        label: (
          <button
            className='w-full flex-1'
            onClick={() => {
              setVisibleSorter(true);
            }}
          >
            <span>Bộ lọc</span>
          </button>
        )
      }
    ];
  };

  const handleClickFilterIcon = (columns: SorterColumns) => () => {
    setSorterColumns(columns);
    setVisibleSorter(false);
  };

  const handleFilterByStatus = (event: ChangeEvent<HTMLSelectElement>) => {
    if (!event) return;

    const { value, name } = event.target;

    setSearchForm((prevState) => {
      return {
        ...prevState,
        [name]: value
      };
    });

    // navigate({
    //   pathname: window.location.pathname,
    //   search: createSearchParams(omitBy({ ...queryParams, [name]: value }, isEmpty)).toString()
    // });
  };

  const columns: ColumnsType<BooksInStoreAudit> = [
    {
      key: 'stt',
      dataIndex: 'stt',
      title: 'STT',
      render: (value, record, index) => getSerialNumber(page, pageSize, index)
    },
    {
      key: 'MaDKCB',
      dataIndex: 'MaDKCB',
      title: 'Mã ĐKCB'
    },
    {
      key: 'TenSach',
      dataIndex: 'TenSach',
      title: 'Tên sách',
      onCell: () => ({ className: 'text-left' })
    },
    {
      key: 'IdKho',
      dataIndex: 'IdKho',
      title: 'Kho Sách',
      render: (value, record) => storesData?.data.Item.find(({ Id }) => Id === record.IdKho)?.Ten || '--'
    },
    {
      key: 'TacGia',
      dataIndex: 'TacGia',
      title: 'Tác giả',
      render: (value, { TacGia }) =>
        TacGia && TacGia?.length > 25 ? (
          <Tooltip title={TacGia} arrow={true}>
            <p className='text-center'>{TacGia.substring(0, 25).concat('...')}</p>
          </Tooltip>
        ) : (
          <p className='text-center'>{TacGia || ''}</p>
        )
    },
    {
      key: 'NamXB',
      dataIndex: 'NamXuatBan',
      title: 'Năm xuất bản'
    },
    {
      key: 'ISBN',
      dataIndex: 'ISBN',
      title: 'ISBN'
    },
    {
      key: 'IdTinhTrang',
      dataIndex: 'IdTinhTrang',
      title: 'Tình trạng',
      render: (value, record) => bookStatuses.find(({ value }) => value === record.IdTinhTrang)?.label
    }
  ];

  const handleRefresh = () => {
    navigate({});
    setPaging({ page: 1, pageSize: 30 });
    setSelectedBooks([]);
  };

  const rowSelections = useMemo(() => {
    return {
      selectedRowKeys: selectedBooks.map((item) => item.Id),
      preserveSelectedRowKeys: true,
      onChange: (_: Key[], selectedRowValue: BooksInStoreAudit[]) => {
        setSelectedBooks(selectedRowValue);
      }
    };
  }, [selectedBooks]);

  const handleAudit = () => {
    // @ts-ignore
    setSelectedBooksContext?.((prevState) => [
      ...selectedBooks.map((book) => {
        return {
          MaKiemSoat: '',
          Id: '',
          IdSachCaBiet: book.Id,
          MaDKCB: book.MaDKCB,
          TenSach: book.TenSach,
          IdKho: book.IdKho,
          IdTinhTrang: book.IdTinhTrang,
          GhiChu: book.GhiChu,
          GiaBia: book.GiaTien.toString(),
          Final: true
        };
      }),
      ...prevState
    ]);

    if (Boolean(auditId)) {
      return navigate('/ThongKe/KiemKeTaiSan/CapNhat/' + auditId);
    }

    navigate(path.addAudit);
  };
  const handleBack = () => {
    if (Boolean(auditId)) {
      return navigate('/ThongKe/KiemKeTaiSan/CapNhat/' + auditId);
    }

    navigate(path.addAudit);
  };

  const handleSearch = debounce((e: ChangeEvent<HTMLInputElement>) => {
    if (!e) return;

    const { value, name } = e.target;

    setSearchForm((prevState) => {
      return {
        ...prevState,
        [name]: value
      };
    });
  }, 300);

  return (
    <div>
      <Title title='Chọn sách kiểm kê' />

      <form action='' className='mt-3 flex flex-1 items-center gap-1 bg-primary-50/20 p-2.5'>
        <Input
          placeholder='Số ĐKCB, tên sách, tác giả, ISBN'
          name='searchText'
          onChange={handleSearch}
          containerClassName='w-full'
        />
        {/* <Input placeholder='Tên sách' name='TenSach' onChange={handleSearch} containerClassName='w-full' /> */}
        <Select
          name='IdKho'
          items={[{ label: 'Tất cả kho', value: '' }, ...storesResult]}
          onChange={(e) => handleFilterByStatus(e)}
          disabled={loadingStores}
          className='w-full'
        />
        {/* <Input placeholder='Tác giả' name='TacGia' onChange={handleSearch} containerClassName='w-full' />
        <Input placeholder='ISBN' name='ISBN' onChange={handleSearch} containerClassName='w-full' /> */}
        <Select
          name='IdTinhTrang'
          items={[{ label: 'Tất cả tình trạng', value: '' }, ...bookStatuses]}
          onChange={(e) => handleFilterByStatus(e)}
          disabled={loadingBookStatuses}
          className='w-full'
        />
      </form>

      <div className='mt-3 flex items-center justify-between'>
        <p className='font-semibold text-tertiary-30'>Đã chọn {selectedBooks.length} sách để kiểm kê.</p>

        <Button onClick={handleRefresh}>Làm mới</Button>
      </div>

      <Table
        columns={columns}
        className='custom-table mt-3'
        bordered
        rowSelection={rowSelections}
        loading={loadingBooksInStore || fetchingBooksInStore}
        dataSource={booksInstore?.ListSCB || []}
        pagination={{
          onChange(current, pageSize) {
            setPaging({ page: current, pageSize: pageSize });
          },
          current: page,
          pageSize: pageSize,
          total: booksInstore?.count || 0,
          hideOnSinglePage: true,
          showSizeChanger: false,
          showQuickJumper: true,
          locale: { jump_to: '', page: '' }
        }}
        rowKey={(row) => row.Id}
      />

      <div
        className={classNames('relative', {
          'mt-[64px]': Number(pageSize) >= (booksInstore?.count || 0)
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger
            visible={!!booksInstore?.ListSCB}
            value={booksInstore?.ListSCB.length + ''}
            total={booksInstore?.count + ''}
            currentPage={page.toString()}
            onChange={(pageSize) => {
              setPaging({ page: 1, pageSize: +pageSize });
            }}
          />
        </div>
      </div>

      <div className='flex items-center justify-end gap-2'>
        <Button variant='secondary' onClick={handleBack}>
          Quay về
        </Button>

        <Button
          onClick={handleAudit}
          disabled={!selectedBooks.length}
          variant={!selectedBooks.length ? 'secondary' : 'default'}
        >
          Chọn
        </Button>
      </div>
    </div>
  );
};

export default AddBooksToAudit;
