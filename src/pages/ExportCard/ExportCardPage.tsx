import { useMutation } from '@tanstack/react-query';
import { studentApis } from 'apis';
import classNames from 'classnames';
import { Button, Input, Loading, Title } from 'components';
import { path } from 'constants/path';
import { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router';
import { toast } from 'react-toastify';

const ExportCardPage = () => {
  const { state } = useLocation();
  const selectedStudents = state ? state.selectedStudents : undefined;

  const navigate = useNavigate();

  const [type, setType] = useState<'1Mat' | '2Mat'>('1Mat');
  const [format, setFormat] = useState<'mau1' | 'mau2'>('mau1');

  const { mutate, isLoading } = useMutation({
    mutationFn: studentApis.exportCard,
    onSuccess: (data) => {
      const blob = new Blob([data.data]);
      const link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob);
      link.download = `TheHocSinh.${format}.doc`;
      link.click();

      toast.success('Xuất thẻ thư viện thành công!');
    }
  });

  useEffect(() => {
    if (!selectedStudents || !selectedStudents?.length) {
      return navigate(path.hocsinh);
    }
  }, [navigate, selectedStudents]);

  const handleChooseType = (type: '1Mat' | '2Mat') => () => {
    setType(type);
  };

  const handleExportCard = (format: 'mau1' | 'mau2') => () => {
    setFormat(format);
  };

  const handlePrintCard = () => {
    if (!selectedStudents?.length) {
      return toast.warning('Hãy chọn sinh viên để xuất thẻ!');
    }
    mutate({ mauThe: format, CachIn: type, listIdTV: selectedStudents });
  };

  const handleBack = () => navigate(path.hocsinh);

  return (
    <div className='py-5'>
      <Title title='Xuất thẻ thư viện' />
      <div className='mt-10 flex'>
        <p className='mr-10 font-bold'>Cách in:</p>

        <div>
          <div className='flex' onClick={handleChooseType('1Mat')}>
            <Input checked={type === '1Mat'} type='radio' />
            <label className='ml-2'>In 1 mặt (mặt trước và mặt sau của thẻ nằm cùng 1 trang)</label>
          </div>

          <button className='flex' onClick={handleChooseType('2Mat')}>
            <Input checked={type === '2Mat'} type='radio' />
            <label className='ml-2'>In 2 mặt (mặt trước và mặt sau của thẻ nằm 2 trang đối xứng nhau)</label>
          </button>
        </div>
      </div>

      <div className='mt-10 flex'>
        <p className='mr-10 font-bold'>Mẫu 1:</p>

        <button
          className={classNames('transform rounded-lg p-2', {
            'bg-primary-10': format === 'mau1'
          })}
          onClick={handleExportCard('mau1')}
        >
          <img src='/content/MauTheHS1.png' alt='Thẻ thư viện mẫu 1' className='h-[210px] w-[700px]' />
        </button>
      </div>

      <div className='mt-10 flex'>
        <p className='mr-10 font-bold'>Mẫu 2:</p>

        <button
          className={classNames('transform rounded-lg p-2', {
            'bg-primary-10': format === 'mau2'
          })}
          onClick={handleExportCard('mau2')}
        >
          <img src='/content/MauTheHS2.png' alt='Thẻ thư viện mẫu 2' className='h-[210px] w-[700px]' />
        </button>
      </div>

      <div className='mt-10 flex items-center justify-end gap-1'>
        <Button variant='secondary' onClick={handleBack}>
          Quay về
        </Button>

        <Button variant='default' onClick={handlePrintCard}>
          In thẻ thư viện
        </Button>
      </div>

      <Loading open={isLoading} />
    </div>
  );
};

export default ExportCardPage;
