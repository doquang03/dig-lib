import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery } from '@tanstack/react-query';
import { languageApis } from 'apis';
import { AxiosError } from 'axios';
import { Button, Input, Loading, Title } from 'components';
import { path } from 'constants/path';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { languageSchema } from 'utils/rules';
import * as yup from 'yup';
type languageForm = yup.InferType<typeof languageSchema>;

const AddLanguagePage = () => {
  const {
    register,
    handleSubmit,
    setValue,
    setError,
    formState: { errors }
  } = useForm<languageForm>({
    defaultValues: {
      name: ''
    },
    resolver: yupResolver(languageSchema)
  });

  const { languageId } = useParams();

  const navigate = useNavigate();

  const { isFetching } = useQuery({
    queryKey: ['EditByID', languageId],
    queryFn: () => languageApis.EditByID(languageId as string),
    onSuccess: (res) => {
      setValue('name', res?.data?.Item?.Ten);
      setValue('shortname', res?.data?.Item?.TenNgan);
    },
    onError: () => {
      navigate(path.ngonngu);
    },
    enabled: !!languageId
  });

  const { mutate: handleCreate, isLoading: isLoadingCreate } = useMutation({
    mutationFn: (payload: object) => languageApis.Create(payload),
    onSuccess: () => {
      toast.success('Thêm ngôn ngữ thành công');
      navigate(path.ngonngu);
    },
    onError: (error: AxiosError<ResponseApi>) => {
      setError('name', { message: error.response?.data.Message, type: 'validate' });
    }
  });

  const { mutate: handleEdit, isLoading: isLoadingEdit } = useMutation({
    mutationFn: (payload: object) => languageApis.EditModel(payload),
    onSuccess: () => {
      toast.success('Cập nhật ngôn ngữ thành công');
    }
  });

  const handleCompelete = handleSubmit((data) => {
    let input = data.name.replace(/\s+/g, ' ');
    let shortname = data?.shortname?.replace(/\s+/g, ' ');

    setValue('name', input);
    setValue('shortname', shortname);

    if (!languageId) {
      handleCreate({ Ten: input, TenNgan: shortname });
    } else {
      handleEdit({ Id: languageId, Ten: input, TenNgan: shortname });
    }
  });

  function backLanguage() {
    navigate(path.ngonngu);
  }
  return (
    <div className='p-5'>
      <Title title={!!languageId ? 'CẬP NHẬT NGÔN NGỮ' : 'THÊM MỚI NGÔN NGỮ'} />

      <div className='relative my-6'>
        <div>
          <div className='form-group row' style={{ marginBottom: '10px' }}>
            <label
              className='col-form-label col-sm-2 font-bold'
              style={{ marginBottom: '0', paddingTop: '5px', width: '100%' }}
            >
              Tên ngôn ngữ <span style={{ color: 'red' }}>*</span>
            </label>
          </div>
          <div className='form-group row'>
            <div className='component-form-group'>
              <Input
                placeholder='Nhập tên ngôn ngữ'
                className='form-control form-control-sm'
                register={register}
                name='name'
                errorMessage={errors?.name?.message}
                disabled={!!languageId && isFetching}
                maxLength={256}
              ></Input>
            </div>
          </div>
          <div className='form-group row' style={{ marginBottom: '10px' }}>
            <label
              className='col-form-label col-sm-2 font-bold'
              style={{ marginBottom: '0', paddingTop: '5px', width: '100%' }}
            >
              Tên viết tắt
            </label>
          </div>
          <div className='form-group row'>
            <div className='component-form-group'>
              <Input
                placeholder='Nhập tên viết tắt'
                className='form-control form-control-sm'
                register={register}
                name='shortname'
                errorMessage={errors?.shortname?.message}
                disabled={!!languageId && isFetching}
                maxLength={256}
              ></Input>
            </div>
          </div>
          <div className='form-group row'>
            <div className='component-form-group flex' style={{ textAlign: 'end' }}>
              <Button className='gray-button' onClick={backLanguage}>
                Quay về
              </Button>
              {!languageId ? (
                <Button
                  className='btn btn-primary btn-sm blue-button'
                  onClick={handleCompelete}
                  style={{ marginLeft: '10px' }}
                >
                  Thêm mới
                </Button>
              ) : (
                <Button
                  className='btn btn-primary btn-sm blue-button'
                  onClick={handleCompelete}
                  style={{ marginLeft: '10px' }}
                >
                  Cập nhật
                </Button>
              )}
            </div>
          </div>
        </div>
      </div>
      <Loading open={isLoadingCreate || isLoadingEdit}></Loading>
    </div>
  );
};

export default AddLanguagePage;
