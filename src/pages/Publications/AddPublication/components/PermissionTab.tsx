import { Input } from 'components';
import { ChangeEvent } from 'react';

const exceptThisSymbols = ['e', 'E', '+', '-', '.'];

type Props = {
  onChangeAllowPermission: (event: ChangeEvent<HTMLInputElement>) => void;
  onChangPermissionOfLib: (event: ChangeEvent<HTMLInputElement>) => void;
  onChangeAllowFee: (event: ChangeEvent<HTMLInputElement>) => void;
  formValues: {
    allowReading: boolean;
    libraryAllowReading: number;
    readingFee: string;
    allowDownloading: boolean;
    libraryAllowDownloading: number;
    downloadPercent: string;
    allowBorrowing: boolean;
    libraryAllowBorrowing: number;
    borrowingFee: string;
  };
};

const PermissionTab = (props: Props) => {
  const { formValues, onChangeAllowPermission, onChangPermissionOfLib, onChangeAllowFee } = props;

  return (
    <div className='w-1/3'>
      {/* read */}
      <label className='font-bold hover:cursor-pointer'>
        <input
          type='checkbox'
          className='mr-1'
          name='allowReading'
          onChange={onChangeAllowPermission}
          checked={formValues.allowReading}
        />
        Cho phép đọc
      </label>

      <div className='mt-2 ml-5 flex items-center justify-between'>
        <label className='hover:cursor-pointer'>
          <input
            type='radio'
            className='mr-1'
            name='libraryAllowReading'
            value={1}
            checked={formValues.libraryAllowReading === 1}
            onChange={onChangPermissionOfLib}
            disabled={!formValues.allowReading}
          />
          Thư viện nội bộ
        </label>

        <label className='hover:cursor-pointer'>
          <input
            type='radio'
            className='mr-1'
            name='libraryAllowReading'
            value={2}
            checked={formValues.libraryAllowReading === 2}
            onChange={onChangPermissionOfLib}
            disabled={!formValues.allowReading}
          />
          Tất cả thư viện liên thông
        </label>
      </div>

      <div className='mt-2 ml-5'>
        <label className='font-bold hover:cursor-pointer'>Phí đọc sách</label>

        <Input
          type='text'
          containerClassName='pl-2 bg-white w-full border rounded-lg flex mb-1'
          className='mr-2 w-full outline-none'
          right={
            <div className='rounded-br-md rounded-tr-md bg-secondary-20/30 py-3 px-3'>
              <span className='text-xs'>đồng</span>
            </div>
          }
          name='readingFee'
          value={formValues.readingFee}
          onKeyDown={(e) => {
            exceptThisSymbols.includes(e.key) && e.preventDefault();
          }}
          onKeyPress={(event) => {
            if (!/[0-9]/.test(event.key)) {
              event.preventDefault();
            }
          }}
          maxLength={11}
          onChange={onChangeAllowFee}
          disabled={!formValues.allowReading}
        />
      </div>

      {/* download */}
      <label className='mt-3 font-bold hover:cursor-pointer'>
        <input
          type='checkbox'
          className='mr-1'
          name='allowDownloading'
          onChange={onChangeAllowPermission}
          checked={formValues.allowDownloading}
        />
        Cho phép tải về
      </label>

      <div className='mt-2 ml-5 flex items-center justify-between'>
        <label className='hover:cursor-pointer'>
          <input
            type='radio'
            className='mr-1'
            name='libraryAllowDownloading'
            value={1}
            checked={formValues.libraryAllowDownloading === 1}
            onChange={onChangPermissionOfLib}
            disabled={!formValues.allowDownloading}
          />
          Thư viện nội bộ
        </label>

        <label className='hover:cursor-pointer'>
          <input
            type='radio'
            className='mr-1'
            name='libraryAllowDownloading'
            value={2}
            checked={formValues.libraryAllowDownloading === 2}
            onChange={onChangPermissionOfLib}
            disabled={!formValues.allowDownloading}
          />
          Tất cả thư viện liên thông
        </label>
      </div>

      <Input
        type='text'
        containerClassName='pl-2 bg-white w-full border rounded-lg flex mb-1 ml-5'
        className='mr-2 w-full outline-none'
        right={
          <div className='rounded-br-md rounded-tr-md bg-secondary-20/30 py-3 px-3'>
            <span className='whitespace-nowrap text-xs'>% tác phẩm</span>
          </div>
        }
        name='downloadPercent'
        value={formValues.downloadPercent}
        onKeyDown={(e) => {
          exceptThisSymbols.includes(e.key) && e.preventDefault();
        }}
        onKeyPress={(event) => {
          if (!/[0-9]/.test(event.key)) {
            event.preventDefault();
          }
        }}
        maxLength={3}
        onChange={onChangeAllowFee}
        disabled={!formValues.allowDownloading}
      />

      {/* borrow */}
      <label className='mt-3 font-bold hover:cursor-pointer'>
        <input
          type='checkbox'
          className='mr-1'
          name='allowBorrowing'
          onChange={onChangeAllowPermission}
          checked={formValues.allowBorrowing}
        />
        Cho phép mượn
      </label>

      <div className='mt-2 ml-5 flex items-center justify-between'>
        <label className='hover:cursor-pointer'>
          <input
            type='radio'
            className='mr-1'
            name='libraryAllowBorrowing'
            value={1}
            checked={formValues.libraryAllowBorrowing === 1}
            onChange={onChangPermissionOfLib}
            disabled={!formValues.allowBorrowing}
          />
          Thư viện nội bộ
        </label>

        <label className='hover:cursor-pointer'>
          <input
            type='radio'
            className='mr-1'
            name='libraryAllowBorrowing'
            value={2}
            checked={formValues.libraryAllowBorrowing === 2}
            onChange={onChangPermissionOfLib}
            disabled={!formValues.allowBorrowing}
          />
          Tất cả thư viện liên thông
        </label>
      </div>

      <div className='mt-2 ml-5'>
        <label className='font-bold'>Phí mượn sách</label>

        <Input
          type='text'
          containerClassName='pl-2 bg-white w-full border rounded-lg flex mb-1'
          className='mr-2 w-full outline-none'
          right={
            <div className='rounded-br-md rounded-tr-md bg-secondary-20/30 py-3 px-3'>
              <span className='text-xs'>đồng</span>
            </div>
          }
          name='borrowingFee'
          value={formValues.borrowingFee}
          onKeyDown={(e) => {
            exceptThisSymbols.includes(e.key) && e.preventDefault();
          }}
          onKeyPress={(event) => {
            if (!/[0-9]/.test(event.key)) {
              event.preventDefault();
            }
          }}
          maxLength={11}
          onChange={onChangeAllowFee}
          disabled={!formValues.allowBorrowing}
        />
      </div>
    </div>
  );
};

export default PermissionTab;
