type Collection = {
  Id: string;
  CreateDateTime: string;
  Name: string;
  Code: object;
  Status: boolean;
  DangSuDung: boolean;
};

type ResponseApi<T = {}> = {
  Code: number;
  ExMessage: string;
  ExcelErrorMessage: string;
  Item: T;
  ItemExcelError: {};
  Message: string;
};

type ResponseList = {
  PageSize: number;
  PageNumber: number;
  TotalRecord: number;
};

type SearchQueryParams = Partial<{
  page: string;
  pageSize: string;
  TextForSearch: string;
  gender: string;
  schoolYear: string;
  status: string;
  GioiTinh: string;
  TrangThai: string;
  PhanLoai: string;
  OrderBy: string;
  ThuMucSach: string;
  KhoiLop: string;
  TacGia: string;
  NXB: string;
  BST: string;
  MonHoc: string;
  KhoiLop: string;
  ChuDiem: string;
  MaNhapKho: string;
  Count: string;
  NgayVaoSo: string;
  UserName: string;
  CreateDateTime: string;
  GiaBia: string;
  MaXuatKho: string;
  KeSach: string;
  TaiLieuGiay: string;
  IdKho: string;
  IsPublish: string;
  Type: string;
}>;

type Option = {
  label: string;
  value: string | number;
  children?: string;
};

type Selections = Option[];

type UploadFileData<T = Student> = {
  RawDataList: string[][];
  TotalEntry: number;
  ListSuccess: T[];
  ListFail: [];
  ListShow: [];
  ListUpdate: [];
  FileName: string;
  FilePath: string;
  ArrRowsMSTV: string;
  ArrRowDate: string;
  MemoryStream: [];
  ArrRowSchool: string;
  ErrorName: [];
};

type SearchInputField = {
  truongSearch: string;
  operatorSearch: string;
};

type SearchRangeField = {
  Min: string;
  Max: string;
};

type ListResponse<TData> = {
  Count: number;
  ListModel: Array<TData>;
};
