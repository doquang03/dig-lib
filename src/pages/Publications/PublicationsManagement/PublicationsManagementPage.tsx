import { FilterTwoTone } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Modal, Pagination, Skeleton, Table, Tooltip } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { bookApis } from 'apis';
import classNames from 'classnames';
import { BasedModal, Button, Empty, Loading, SizeChanger, Title } from 'components';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import { usePaginationNavigate, useQueryConfig } from 'hooks';
import { Key, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useLocation, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { convertDate, download, getSerialNumber } from 'utils/utils';
import FailureConvertationModal from '../AddPublication/components/FailureConvertationModal';
import BookItem from './components/BookItem';
import DeleteModal from './components/DeleteModal';
import ExpandedRow from './components/ExpandedRow';
import SearchForm from './components/SearchForm';
import UpdateModal from './components/UpdateModal';
import useOptionsSeach from './hooks/useOptionsSeach';
import FailurePublishingModelModal from '../AddPublication/components/FailurePublishingModelModal';

const initialUpdateValues = {
  collection: '',
  folder: '',
  topic: '',
  subject: ''
};

type ModalUpdate = 'collection' | 'folder' | 'topic' | 'subject';

const locale = {
  emptyText: 'Không tìm được kết quả phù hợp'
};

const PublicationsManagementPage = () => {
  const [viewMode, setViewMode] = useState<string>('grid');
  const [selectedBooks, setSelectedBooks] = useState<Book[]>([]);
  const [visibleForm, setVisibleForm] = useState<boolean>(false);
  const [visibleModalUpdate, setVisibleModalUpdate] = useState<ModalUpdate>();
  const [visibleModalDelete, setVisibleModalDelete] = useState<boolean>(false);
  const [visibleModal, setVisibleModal] = useState<
    'convertation' | 'permission' | 'publishing' | 'failureConvertation' | 'failurePublishing'
  >();
  const [visibleModalThanhLy, setVisibleModalThanhly] = useState<boolean>(false);
  const [selectAll, setSelectAll] = useState<boolean>(false);
  const [failureConvertationListing, setFailureConvertationListing] = useState<string[]>([]);
  const [failurePublishingListing, setFailurePublishingListing] = useState<Array<FailureMessage>>([]);

  const queryConfig = useQueryConfig('24');

  const { isAllowedAdjustment, permission } = useUser();

  const navigate = useNavigate();

  const options = useOptionsSeach();

  const handleNavigation = usePaginationNavigate();

  const ref = useRef<HTMLDivElement | null>(null);

  const {
    register: regiterUpdate,
    control,
    handleSubmit: handlesubmitUpdate,
    reset: resetUpdateValues,
    setError: setErrorUpdateValues,
    setValue: setValueUpdate,
    formState: { errors: errorsUpdateValues }
  } = useForm<typeof initialUpdateValues>({
    defaultValues: initialUpdateValues
  });

  const {
    data: BooksData,
    isLoading,
    isFetching,
    refetch
  } = useQuery({
    queryKey: ['books', queryConfig],
    queryFn: () => bookApis.GetBooksList(queryConfig),
    onSuccess: (data) => {
      setVisibleModalDelete(false);
      setVisibleModalUpdate(undefined);
      let count = 0;
      for (let index = 0; index < data.data.Item.ListBook.length; index++) {
        const element = data.data.Item.ListBook[index];
        const i = selectedBooks.indexOf(element);
        if (i !== -1) {
          count++;
        } else {
          break;
        }
      }

      if (count === Number(queryConfig.pageSize)) {
        setSelectAll(true);
      } else {
        setSelectAll(false);
      }
    }
  });

  const { mutate: exportExcel, isLoading: isLoadingExportExcel } = useMutation({
    mutationFn: bookApis.exportExcel,
    onSuccess: (data) => {
      toast.success('Xuất file Excel thành công');
      download('DanhSachSach.xls', data.data);
    }
  });

  const { mutate: exportMarc21, isLoading: isLoadingExportMarc21 } = useMutation({
    mutationFn: bookApis.exportMarc21,
    onSuccess: (data) => {
      const date = new Date();
      const fileName = `FileMarc_${convertDate(
        date + ''
      )}_${date.getHours()}${date.getMinutes()}${date.getSeconds()}.mrc`;

      download(fileName, data.data);

      toast.success('Xuất marc21 thành công');
    }
  });

  const { mutate: updateBooksCollection, isLoading: isLoadingUpdateBookCollections } = useMutation({
    mutationFn: bookApis.updateBooksCollection,
    onSuccess: () => {
      refetch();
      toast.success('Cập nhật bộ sưu tập cho sách thành công');
      resetUpdateValues();
      setSelectedBooks([]);
    }
  });

  const { mutate: updateBooksTopic, isLoading: isLoadingBooksTopic } = useMutation({
    mutationFn: bookApis.updateBooksTopic,
    onSuccess: () => {
      refetch();
      toast.success('Cập nhật chủ điểm cho sách thành công');
      resetUpdateValues();
      setSelectedBooks([]);
    }
  });

  const { mutate: updateBooksSubject, isLoading: isLoadingUpdateBooksSubject } = useMutation({
    mutationFn: bookApis.updateBooksSubject,
    onSuccess: () => {
      refetch();
      toast.success('Cập nhật môn học cho sách thành công');
      resetUpdateValues();
      setSelectedBooks([]);
    }
  });

  const { mutate: updateBooksDocument, isLoading: isLoadingUpdateBookDocument } = useMutation({
    mutationFn: bookApis.updateBooksDocumentFolder,
    onSuccess: () => {
      refetch();
      toast.success('Cập nhật thư mục cho sách thành công');
      resetUpdateValues();
      setSelectedBooks([]);
    }
  });

  const { mutate: deleteBooks, isLoading: isLoadingDeleteBooks } = useMutation({
    mutationFn: bookApis.deleteBooks,
    onSuccess: () => {
      refetch();

      toast.success(
        `Xóa thành công ${
          selectedBooks
            .filter(({ MaKiemSoat, SoLuongConLai = 0, SoLuongTong = 0 }) => SoLuongTong === 0 && SoLuongConLai === 0)
            .map(({ Id }) => Id + '').length
        } sách`
      );

      setSelectedBooks([]);
    }
  });

  const { mutate: digitizingBooks, isLoading: loadingDigitizingBooks } = useMutation({
    mutationFn: bookApis.digitizingBooks,
    onSuccess: (data) => {
      const { ListFail, ListSuccess } = data.data.Item || { ListFail: [], ListSuccess: [] };

      refetch();

      !!ListSuccess.length && toast.success(`Chuyển đổi thành công ${ListSuccess.length} sách`);

      setVisibleModal(undefined);

      if (!!ListFail.length) {
        setFailureConvertationListing(ListFail);
        setVisibleModal('failureConvertation');
      } else {
        setSelectedBooks([]);
      }
    }
  });

  const { mutate: publishToHost, isLoading: loadingPublishToHost } = useMutation({
    mutationFn: bookApis.publishToHost,
    onSuccess: (data) => {
      const { ListFail, ListSuccess } = data.data.Item || { ListFail: [], ListSuccess: [] };

      !!ListSuccess.length && toast.success(`Đã gửi thành công ${ListSuccess.length} sách`);
      if (ListFail.length > 0) {
        setFailurePublishingListing(ListFail);
        setVisibleModal('failurePublishing');
      } else {
        setSelectedBooks([]);
        setFailurePublishingListing([]);
        setVisibleModal(undefined);
      }
    }
  });

  const { ListBook = [], count = 0 } = useMemo(() => {
    if (!BooksData?.data.Item) {
      return {} as { ListBook: Book[]; count: number };
    }

    return BooksData.data.Item;
  }, [BooksData?.data?.Item]);

  const columns: ColumnsType<Book> = useMemo(() => {
    return [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        render: (value: any, record: any, index: number) => {
          return (
            queryConfig.page && queryConfig.pageSize && getSerialNumber(+queryConfig.page, +queryConfig.pageSize, index)
          );
        },
        width: 100
      },
      Table.EXPAND_COLUMN,
      {
        title: 'Tên sách',
        dataIndex: 'Ten',
        key: 'Ten',
        render: (value: any, { TenSach }: any) => (
          <Tooltip placement='topLeft' title={TenSach} arrow={true}>
            <p>{TenSach}</p>
          </Tooltip>
        ),
        onCell: (record: any) => ({
          className: 'text-left'
        }),
        ellipsis: true
      },
      {
        title: 'Mã kiểm soát',
        dataIndex: 'MaKiemSoat',
        key: 'MaKiemSoat'
      },
      {
        title: '',
        dataIndex: 'IsDigitalResource',
        key: 'IsDigitalResource',
        render: (value: any, { IsDigitalResource }: { IsDigitalResource: boolean }) => (
          <p
            className={
              !IsDigitalResource
                ? 'hidden'
                : `rounded-md border border-[#D9C510]/80 bg-[#D9C510]/10 p-2 font-bold text-[#D9C510]`
            }
          >
            Tài liệu giấy & số
          </p>
        ),
        width: 200
      }
    ];
  }, [queryConfig.page, queryConfig.pageSize]);

  const isDisable = selectedBooks.length === 0;

  const onSelectChange = (_: Key[], selectedRowValue: Book[]) => {
    setSelectedBooks(selectedRowValue);
  };

  const handleAddPublication = () => {
    navigate(path.themSach);
  };

  // hàm chọn từng item
  const handleSelectBook = (book: Book) => {
    const index = selectedBooks.findIndex((x) => x.Id === book.Id);

    if (index === -1) {
      setSelectedBooks((prevState) => [...prevState, book]);
    } else {
      setSelectedBooks((prevState) => prevState.filter((x) => x.Id !== book.Id));
    }
  };

  const rowSelections = useMemo(() => {
    return {
      selectedRowKeys: selectedBooks.map((item) => item.Id + ''),
      preserveSelectedRowKeys: true,
      onChange: onSelectChange
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedBooks]);

  const onPaginate = useCallback(
    (page: number, pageSize: number) => {
      handleNavigation({ ...queryConfig, page: page + '', pageSize: pageSize + '' });
      ref.current?.scrollIntoView({ behavior: 'smooth' });
    },
    [handleNavigation, queryConfig]
  );

  const pagination = useMemo(() => {
    let size = 24;
    let currentPage = 1;

    if (queryConfig.pageSize) {
      size = +queryConfig.pageSize;
    }
    if (queryConfig.page) {
      currentPage = +queryConfig.page;
    }

    return {
      current: currentPage,
      pageSize: size,
      total: count,
      onChange: onPaginate,
      hideOnSinglePage: true,
      showSizeChanger: false,
      showQuickJumper: true,
      locale: { jump_to: '', page: '' }
    };
  }, [onPaginate, queryConfig.page, queryConfig.pageSize, count]);

  const handleChangeViewMode = () => {
    setViewMode((preState) => (preState === 'grid' ? 'list' : 'grid'));
  };

  const handleLiquidation = () => {
    setVisibleModalThanhly(true);
  };

  const handleResetField = () => {
    setSelectedBooks([]);
  };

  const functionCheckAll = () => {
    let count = 0;

    for (let index = 0; index < ListBook.length; index++) {
      const element = ListBook[index];
      let value = -1;
      selectedBooks.forEach((item, i) => {
        if (item.Id === element.Id) {
          value = i;
          return;
        }
      });
      if (value !== -1) {
        count++;
      } else {
        break;
      }
    }

    if (count === Number(ListBook.length)) {
      setSelectAll(true);
    } else {
      setSelectAll(false);
    }
  };

  useEffect(() => {
    functionCheckAll();
  });

  const folder = useLocation();
  useEffect(() => {
    if (folder?.state?.folder) {
      setViewMode('list');
    }
  }, [folder?.state?.folder]);

  useEffect(() => {
    if (selectedBooks.length && selectedBooks.length === 1 && visibleModalUpdate) {
      let fieldValue = '';

      if (visibleModalUpdate === 'subject') {
        fieldValue = selectedBooks[0].IdMonHoc?.includes('--') ? '' : selectedBooks[0].IdMonHoc + '';
      }

      if (visibleModalUpdate === 'topic') {
        fieldValue = selectedBooks[0].IdChuDiem?.includes('--') ? '' : selectedBooks[0].IdChuDiem + '';
      }
      setValueUpdate(visibleModalUpdate, fieldValue);
    }
  }, [selectedBooks, setValueUpdate, visibleModalUpdate]);

  // chọn tất cả
  const handleSelectAll = () => {
    let arr = [...selectedBooks];
    if (!selectAll) {
      for (let index = 0; index < ListBook.length; index++) {
        const element = ListBook[index];
        let _index = -1;
        selectedBooks.forEach((item, i) => {
          if (item.Id === element.Id) {
            _index = i;
            return;
          }
        });

        if (_index === -1) {
          arr.push(element);
        }
      }
      setSelectedBooks(arr);
      setSelectAll(true);
    } else {
      let arrCheck: Array<number> = [];
      for (let index = 0; index < ListBook.length; index++) {
        const element = ListBook[index];
        let _index = -1;
        selectedBooks.forEach((item, i) => {
          if (item.Id === element.Id) {
            _index = i;
            return;
          }
        });

        if (_index !== -1) {
          arrCheck.push(_index);
        }
      }
      arrCheck.sort((a, b) => b - a);
      arrCheck.forEach((element) => {
        arr.splice(element, 1);
      });
      setSelectedBooks(arr);
      setSelectAll(false);
    }
  };

  const onSubmit = () => setVisibleForm(false);

  const toggleVisibleForm = () => setVisibleForm((prevState) => !prevState);

  const toogleVisibleModalUpdate = (option: ModalUpdate) => () => setVisibleModalUpdate(option);

  const onUpdate = handlesubmitUpdate((data) => {
    if (!selectedBooks.length) return;

    const ids = selectedBooks.map((item) => item.Id + '');

    if (!data.collection && !data.folder && !data.topic && !data.subject && visibleModalUpdate) {
      let message: Record<ModalUpdate, string> = {
        collection: 'Bộ sưu tập không được để trống',
        folder: 'Tài liệu thư mục không được để trống',
        topic: 'Chủ điểm không được để trống',
        subject: 'Môn học không được để trống'
      };

      setErrorUpdateValues(visibleModalUpdate, { message: message[visibleModalUpdate] });
      return;
    }

    if (Boolean(data.collection)) {
      return updateBooksCollection({ bstId: data.collection, ids });
    } else if (Boolean(data.folder)) {
      return updateBooksDocument({ documentId: data.folder, ids });
    } else if (Boolean(data.topic)) {
      return updateBooksTopic({ ids, idTopic: data.topic });
    } else if (Boolean(data.subject)) {
      return updateBooksSubject({ ids, idSubject: data.subject });
    }
  });

  const handleDeleteBooks = () => {
    const ids = selectedBooks
      .filter(({ SoLuongConLai = 0, SoLuongTong = 0 }) => SoLuongTong === 0 && SoLuongConLai === 0)
      .map(({ Id }) => Id + '');

    if (!ids.length) {
      return toast.warning('Không có sách được chọn để xóa!');
    }

    deleteBooks(ids);
  };

  const handleThanhLyBooks = () => {
    navigate(path.thanhLySach, {
      state: {
        liquidBooks: selectedBooks.filter(
          ({ SoLuongConLai = 0, SoLuongTong = 0 }) => SoLuongTong >= 0 && SoLuongConLai >= 0
        )
      }
    });
  };

  // delete seleted books
  const handleDeleteBook = () => {
    setVisibleModalDelete(true);
  };

  const handleAddByMarc21 = () => {
    navigate(path.marc21);
  };

  const handleAddBooksByExcel = () => navigate(path.themSachExcel);

  const handleAddCoverBook = () => navigate(path.capNhatAnhBia);

  const handleVisibleModal = (modal: 'convertation' | 'permission' | 'publishing') => () => setVisibleModal(modal);

  const handleExportExcel = () => {
    if (!selectedBooks.length) return;

    const ids = selectedBooks.map((book) => book.Id + '');

    exportExcel(ids);
  };

  const handleExportMarc21 = () => {
    const ids = selectedBooks.map((book) => {
      return {
        Id: book.Id + ''
      };
    });

    exportMarc21(ids);
  };

  return (
    <div className='p-5' ref={ref}>
      <Title title='Danh sách ấn phẩm' />

      <div
        className={classNames('', {
          hidden: !isAllowedAdjustment,
          'mt-2 flex flex-col items-center gap-2 md:flex-row': isAllowedAdjustment
        })}
      >
        <Button className='w-full md:w-[200px]' onClick={handleAddPublication}>
          Thêm sách
        </Button>
        <Button className='w-full md:w-[200px]' onClick={handleAddByMarc21}>
          Thêm từ Marc 21
        </Button>
        <Button className='w-full md:w-[200px]' onClick={handleAddBooksByExcel}>
          Thêm từ Excel
        </Button>
        <Button className='w-full md:w-[200px]' onClick={handleAddCoverBook}>
          Bổ sung ảnh bìa
        </Button>
        {/* <Button className='w-full md:w-[200px]' onClick={handleVisibleModal('permission')}>
          Phân quyền dữ liệu
        </Button> */}
        {permission?.EnableDigital && (
          <Button
            className='w-full md:w-[200px]'
            onClick={handleVisibleModal('convertation')}
            disabled={!selectedBooks.length}
            variant={!selectedBooks.length ? 'disabled' : 'default'}
          >
            Chuyển đổi tài liệu số
          </Button>
        )}{' '}
        {permission?.EnableDigital && (
          <Button
            className='w-full md:w-[200px]'
            onClick={handleVisibleModal('publishing')}
            disabled={!selectedBooks.length}
            variant={!selectedBooks.length ? 'disabled' : 'default'}
          >
            Gửi vào MLLH
          </Button>
        )}{' '}
      </div>

      {/* mobile */}
      <Button className='my-2 flex items-center gap-2 md:hidden' variant='primary-outline' onClick={toggleVisibleForm}>
        <FilterTwoTone />
        <span className='text-primary-50'>Bộ lọc tìm kiếm</span>
      </Button>

      {/* mobile */}
      <Modal open={visibleForm} closable={true} centered footer={null} destroyOnClose onCancel={toggleVisibleForm}>
        <SearchForm
          viewMode={viewMode}
          onChangeViewMode={handleChangeViewMode}
          onResetForm={handleResetField}
          onSubmit={onSubmit}
          isLargeView={false}
        />
      </Modal>

      {/* tablet & desktop */}
      <SearchForm
        viewMode={viewMode}
        onChangeViewMode={handleChangeViewMode}
        onResetForm={handleResetField}
        onSubmit={onSubmit}
      />
      <div
        className={classNames('', {
          hidden: !isAllowedAdjustment,
          'mt-2 flex-col flex-wrap gap-1 md:flex md:md:flex-row': isAllowedAdjustment
        })}
      >
        <Button
          className='my-2 w-full md:w-auto'
          onClick={toogleVisibleModalUpdate('collection')}
          variant={isDisable ? 'disabled' : 'default'}
          disabled={isDisable}
        >
          Đổi bộ sưu tập
        </Button>

        <Button
          className='my-2 w-full md:w-auto'
          onClick={toogleVisibleModalUpdate('folder')}
          variant={isDisable ? 'disabled' : 'default'}
          disabled={isDisable}
        >
          Đổi thư mục
        </Button>

        <Button
          className='my-2 w-full md:w-auto'
          variant={isDisable ? 'disabled' : 'default'}
          disabled={isDisable}
          onClick={toogleVisibleModalUpdate('topic')}
        >
          Đổi chủ điểm
        </Button>

        <Button
          className='my-2 w-full md:w-auto'
          variant={isDisable ? 'disabled' : 'default'}
          disabled={isDisable}
          onClick={toogleVisibleModalUpdate('subject')}
        >
          Đổi môn học
        </Button>

        <Button
          className='my-2 w-full md:w-auto'
          variant={isDisable ? 'disabled' : 'default'}
          disabled={isDisable}
          onClick={handleExportMarc21}
        >
          Xuất Marc21
        </Button>

        <Button
          className='my-2 w-full md:w-auto'
          variant={isDisable ? 'disabled' : 'default'}
          disabled={isDisable}
          onClick={handleExportExcel}
        >
          Xuất Excel
        </Button>

        <Button
          className='my-2 w-full md:w-auto'
          variant={isDisable ? 'disabled' : 'default'}
          disabled={isDisable}
          onClick={handleLiquidation}
        >
          Thanh lý
        </Button>

        <Button
          className='my-2 w-full md:w-auto'
          variant={isDisable ? 'disabled' : 'danger'}
          disabled={isDisable}
          onClick={handleDeleteBook}
        >
          Xóa sách
        </Button>
      </div>
      {/* grid view */}
      {viewMode === 'grid' && (
        <>
          {Boolean(count) ? (
            <>
              <input type='checkbox' onChange={handleSelectAll} checked={selectAll} />
              <label className='ml-2'>Chọn tất cả</label>

              <div className='mt-2 grid grid-cols-12'>
                {ListBook.map((book, index) => {
                  const _index = selectedBooks.findIndex((x) => x.Id === book.Id);

                  const serialNumber =
                    queryConfig.page &&
                    queryConfig.pageSize &&
                    getSerialNumber(+queryConfig.page, +queryConfig.pageSize, index);

                  return (
                    <div className='col-span-12 px-2 py-2 md:col-span-6 lg:col-span-6 xl:col-span-3' key={book.Id}>
                      <BookItem
                        book={book}
                        onSelect={handleSelectBook}
                        index={_index}
                        serialNumber={serialNumber}
                        options={options}
                      />
                    </div>
                  );
                })}
              </div>
              <div className='flex flex-col items-center justify-between gap-3 md:flex-row'>
                <SizeChanger
                  visible={!!ListBook?.length}
                  value={ListBook.length + ''}
                  total={count + ''}
                  pageSizeDefault={'24'}
                />

                <Pagination
                  current={Number(queryConfig.page)}
                  total={count}
                  onChange={onPaginate}
                  showSizeChanger={false}
                  pageSize={Number(queryConfig.pageSize)}
                  showQuickJumper={true}
                  locale={{ jump_to: '', page: '' }}
                />
              </div>
            </>
          ) : (
            <>
              {isLoading ? (
                <div className='flex items-center justify-between'>
                  {[...Array(3)].map((_, index) => (
                    <Skeleton.Button active size={'large'} style={{ width: 500, height: 300 }} key={index} />
                  ))}
                </div>
              ) : (
                <div className='flex items-center justify-center bg-white p-2'>
                  <Empty label='Không tìm được kết quả phù hợp' />
                </div>
              )}
            </>
          )}
        </>
      )}
      {/* table view */}
      {viewMode === 'list' && (
        <>
          <div className='mt-6'>
            {Number.isInteger(queryConfig.page && +queryConfig.page) ? (
              <Table
                rowSelection={rowSelections}
                columns={columns}
                dataSource={ListBook}
                pagination={pagination}
                scroll={{ x: 980 }}
                rowKey={(record) => record.Id + ''}
                className='custom-table'
                locale={{
                  emptyText: () => <Empty label={locale.emptyText} />
                }}
                loading={isLoading}
                expandable={{
                  expandedRowRender: (record) => <ExpandedRow book={record} options={options} />
                }}
                bordered
              />
            ) : (
              <div className='rounded-md bg-white p-2 shadow-md'>
                <p className=' text-center'>
                  Đã có lỗi xảy ra.
                  <button onClick={handleResetField}>Vui lòng làm mới tìm kiếm.</button>
                </p>
              </div>
            )}
          </div>

          <div
            className={classNames('relative', {
              'mt-[64px]': Number(queryConfig.pageSize) >= count
            })}
          >
            <div className='absolute bottom-1'>
              <SizeChanger
                visible={!!ListBook?.length}
                value={ListBook.length + ''}
                total={count + ''}
                pageSizeDefault={'24'}
              />
            </div>
          </div>
        </>
      )}
      {/* update collection */}
      <UpdateModal
        loading={isLoadingUpdateBookCollections || isFetching}
        register={regiterUpdate}
        name='collection'
        label='Bộ sưu tập'
        title='Cập nhật bộ sưu tập'
        open={visibleModalUpdate === 'collection'}
        items={options?.optionsBST || []}
        onBack={() => {
          resetUpdateValues();
          setVisibleModalUpdate(undefined);
        }}
        onUpdate={onUpdate}
        error={errorsUpdateValues.collection?.message}
      />
      {/* update folder */}
      <UpdateModal
        loading={isLoadingUpdateBookDocument || isFetching}
        control={control}
        viewModel='treeview'
        name='folder'
        label='Thư mục tài liệu'
        title='Cập nhật thư mục tài liệu'
        open={visibleModalUpdate === 'folder'}
        items={options?.optionsTMS || []}
        onBack={() => {
          resetUpdateValues();
          setVisibleModalUpdate(undefined);
        }}
        onUpdate={onUpdate}
        error={errorsUpdateValues.folder?.message}
      />
      {/* update topic */}
      <UpdateModal
        loading={isLoadingBooksTopic || isFetching}
        register={regiterUpdate}
        name='topic'
        label='Chủ điểm'
        title='Cập nhật chủ điểm'
        open={visibleModalUpdate === 'topic'}
        items={options?.optionsChuDiem || []}
        onBack={() => {
          resetUpdateValues();
          setVisibleModalUpdate(undefined);
        }}
        onUpdate={onUpdate}
        error={errorsUpdateValues.topic?.message}
      />
      {/* update subject */}
      <UpdateModal
        loading={isLoadingUpdateBooksSubject || isFetching}
        register={regiterUpdate}
        name='subject'
        label='Môn học'
        title='Cập nhật môn học'
        open={visibleModalUpdate === 'subject'}
        items={options?.optionsMonHoc || []}
        onBack={() => {
          resetUpdateValues();
          setVisibleModalUpdate(undefined);
        }}
        onUpdate={onUpdate}
        error={errorsUpdateValues.subject?.message}
      />
      {/* delete */}
      <DeleteModal
        visible={visibleModalDelete}
        books={selectedBooks || []}
        onCancel={() => {
          setVisibleModalDelete(false);
        }}
        onOk={handleDeleteBooks}
        loading={isLoadingDeleteBooks || isFetching}
      />
      {/* thanh lý (Thanh lý tiếng Anh là gì ko biết nên comment bằng tiếng Việt <("") ) */}
      <DeleteModal
        visible={visibleModalThanhLy}
        books={selectedBooks || []}
        onCancel={() => {
          setVisibleModalThanhly(false);
        }}
        onOk={handleThanhLyBooks}
        loading={false}
        title='Bạn có muốn chắc chắn thanh lý những sách này không?'
        subtitle='Bạn sẽ không thể khôi phục sau khi thanh lý, những sách đang được mượn sẽ không thể thanh lý.'
        isThanhLy
        labelOK='Thanh lý'
      />

      <BasedModal
        open={visibleModal === 'convertation'}
        title={
          <p className='mb-3 w-[500px] whitespace-pre-wrap text-[20px]'>
            Bạn có chắc chắn muốn chuyển đổi <span className='font-bold'>{selectedBooks.length} tài liệu</span> này sang
            tài liệu số không?
          </p>
        }
        onCancel={() => setVisibleModal(undefined)}
        onSubmit={() => selectedBooks.length && digitizingBooks(selectedBooks.map(({ Id }) => Id + ''))}
        loading={loadingDigitizingBooks || isFetching}
        labelSubmit='Chuyển đổi'
      />

      <FailureConvertationModal
        open={visibleModal === 'failureConvertation'}
        books={selectedBooks.filter((book) => failureConvertationListing.includes(book.Id + '')) || []}
        onCancel={() => {
          setSelectedBooks([]);
          setFailureConvertationListing([]);
          setVisibleModal(undefined);
        }}
      />

      {/* publish to host */}
      <BasedModal
        open={visibleModal === 'publishing'}
        title={
          <p className='mb-2 w-[500px] whitespace-pre-wrap text-[20px] text-primary-20'>
            Bạn có chắc chắn muốn gửi <span className='font-bold'>{selectedBooks.length} quyển sách này</span> vào MLLH
            không?
          </p>
        }
        onCancel={() => setVisibleModal(undefined)}
        onSubmit={() => selectedBooks.length && publishToHost(selectedBooks.map(({ Id }) => Id + ''))}
        loading={loadingPublishToHost || isFetching}
        labelSubmit='Gửi'
      />

      <FailurePublishingModelModal
        open={visibleModal === 'failurePublishing'}
        books={selectedBooks.filter((book) => failurePublishingListing.find((_) => _.Id === book.Id)) || []}
        failures={failurePublishingListing}
        onCancel={() => {
          setSelectedBooks([]);
          setFailurePublishingListing([]);
          setVisibleModal(undefined);
        }}
      />

      <Loading open={isLoadingExportExcel || isLoadingExportMarc21} />
    </div>
  );
};

export default PublicationsManagementPage;
