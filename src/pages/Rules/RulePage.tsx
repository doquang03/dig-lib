import { useQuery } from '@tanstack/react-query';
import { settingApis } from 'apis';
import { Button, Title } from 'components';
import { path } from 'constants/path';
import { useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useReactToPrint } from 'react-to-print';
import 'css/Ckeditor5.css';

const RulePage = () => {
  const navigate = useNavigate();
  const componentRef = useRef(null);

  const [noiQuy, setNoiQuy] = useState('');
  useQuery({
    queryKey: ['IndexNoiQuy'],
    queryFn: () => settingApis.IndexNoiQuy(),
    onSuccess: (res) => {
      let Item = res?.data?.Item;
      setNoiQuy(Item.NoiQuy);
    },
    onError: () => {
      navigate(path.home);
    }
  });

  const handlePrint = useReactToPrint({
    content: () => componentRef.current
  });

  return (
    <div className='p-5'>
      <Title title='NỘI QUY THƯ VIỆN' />

      <div
        className='ck-content'
        ref={componentRef}
        dangerouslySetInnerHTML={{
          __html: noiQuy
        }}
      ></div>

      <div className='mt-5 flex justify-end'>
        <Button variant='default' type='button' className='ml-2 font-semibold' onClick={handlePrint}>
          In nội quy
        </Button>
      </div>
    </div>
  );
};

export default RulePage;
