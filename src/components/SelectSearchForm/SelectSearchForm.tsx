import { Button } from 'components/Button';
import { Input } from 'components/Input';
import Select from 'components/Select/Select';
import { usePaginationNavigate, useQueryConfig } from 'hooks';
import { isEmpty, omitBy } from 'lodash';
import { InputHTMLAttributes, SelectHTMLAttributes, useEffect } from 'react';
import { useForm } from 'react-hook-form';

type Props = {
  items: Selections;
} & InputHTMLAttributes<HTMLInputElement> &
  SelectHTMLAttributes<HTMLSelectElement>;

const SelectSearchForm = (props: Props) => {
  const { placeholder = 'Nhập ...', items } = props;

  const navigate = usePaginationNavigate();

  const queryConfig = useQueryConfig();

  const { register, reset, handleSubmit, setValue } = useForm<{ TextForSearch: string; OrderBy: string | number }>({
    defaultValues: {
      TextForSearch: '',
      OrderBy: ''
    }
  });

  useEffect(() => {
    Object.keys(queryConfig).forEach((key) => {
      if (!key) {
        return;
      }
      // @ts-ignore
      setValue(key, queryConfig[key]);
    });
  }, [queryConfig, setValue]);

  const handleRefresh = () => {
    reset();
    navigate();
  };

  const onSubmit = handleSubmit((data) => {
    const { TextForSearch, OrderBy } = data;

    navigate(omitBy({ TextForSearch, OrderBy }, isEmpty));
  });

  return (
    <form className='flex flex-1 flex-col items-center gap-1 bg-primary-50/20 p-3.5 md:flex-row' onSubmit={onSubmit}>
      <Input placeholder={placeholder} containerClassName='w-full md:w-1/4' name='TextForSearch' register={register} />

      <Select
        items={items || []}
        className='w-full md:w-1/4'
        name='OrderBy'
        register={register}
        disabled={!Boolean(items.length)}
      />

      <Button variant='secondary' type='button' className='w-full md:w-auto' onClick={handleRefresh}>
        Làm mới
      </Button>

      <Button variant='default' type='submit' className='w-full md:w-auto'>
        Tìm kiếm
      </Button>
    </form>
  );
};

export default SelectSearchForm;
