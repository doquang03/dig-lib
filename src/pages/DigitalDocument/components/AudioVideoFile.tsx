import { CloseSquareFilled } from '@ant-design/icons';
import { AUDIO_FILE_TYPE, VIDEO_FILE_TYPE } from 'constants/config';
import { useMemo } from 'react';
import { useParams } from 'react-router-dom';
import { getEmbed } from 'utils/utils';

type Props = {
  listOfFiles?: DetailDigitalDocument['ListMedia'];
  onRemove: VoidFunction;
  file?: File;
  reference?: string;
};

function AudioVideoFile(props: Props) {
  const { listOfFiles, onRemove, file, reference } = props;
  const src = useMemo(() => file && URL.createObjectURL(file), [file]);

  const { documentType } = useParams<{ documentType: 'SachDienTu' | 'AlbumAnh' | 'Audio' | 'Video' }>() || {};

  const fileName = listOfFiles?.ListFile?.[0]?.FileName?.split('.') || [];
  return (
    <>
      {/* file from db */}
      {!!listOfFiles?.ListFile.length ? (
        <div className='mb-3 grid w-full grid-cols-12 gap-3'>
          <div className='relative col-span-9 col-start-4'>
            <button className='absolute right-0 -top-1 z-10 shadow-lg' type='button' onClick={onRemove}>
              <CloseSquareFilled style={{ color: 'red', fontSize: '24px' }} />
            </button>

            {documentType === 'Video' && VIDEO_FILE_TYPE.includes(fileName[fileName.length - 1]) && (
              <video width={'100%'} height={700} controls muted key={src}>
                <source src={listOfFiles?.ListFile?.[0].LinkFile} type='video/mp4' />
              </video>
            )}

            {documentType === 'Audio' && AUDIO_FILE_TYPE.includes(fileName[fileName.length - 1]) && (
              <audio controls className='w-full'>
                <source src={listOfFiles?.ListFile?.[0].LinkFile} />
              </audio>
            )}
          </div>
        </div>
      ) : !!file ? (
        <div className='mb-3 grid w-full grid-cols-12 gap-3'>
          <div className='relative col-span-9 col-start-4'>
            <button className='absolute right-0 -top-1 z-10 shadow-lg' type='button' onClick={onRemove}>
              <CloseSquareFilled style={{ color: 'red', fontSize: '24px' }} />
            </button>

            {file.type.includes('video') && (
              <video width={'100%'} height={700} controls autoPlay key={src}>
                <source src={src} type='video/mp4' />
              </video>
            )}

            {file.type.includes('audio') && (
              <audio controls key={src} className='w-full'>
                <source src={src} type={file.type} />
              </audio>
            )}
          </div>
        </div>
      ) : (
        documentType === 'Video' &&
        !!reference &&
        getEmbed(reference) && (
          <div className='mb-3 grid w-full grid-cols-12 gap-3'>
            <div className='col-span-9 col-start-4'>
              <iframe
                width={'100%'}
                height={250}
                src={`//www.youtube.com/embed/${getEmbed(reference)}`}
                frameBorder='0'
                allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
                allowFullScreen
                title='Embedded youtube'
              />
            </div>
          </div>
        )
      )}
    </>
  );
}

export default AudioVideoFile;
