import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/NguonCungCap`;

type GetSupplierData = {
  ListNguonCungCap: Supplier[];
} & ResponseList;

type GetSuppliersVars = {
  PageSize: number;
  PageNumber: number;
  Keyword?: string;
  TinhChat?: number | string;
};

const supplierApis = {
  getSuppliers: (payload: GetSuppliersVars) => {
    return httpThuVien.post<ResponseApi<GetSupplierData>>(`${endPoint}/GetDataPagingBySearch`, {
      ...payload,
      TinhChat: Number(payload.TinhChat)
    });
  },

  getSupplier: (id: string) => httpThuVien.get<ResponseApi<Supplier>>(`${endPoint}/${id}`),
  deleteSupplier: (id: string) => httpThuVien.delete<ResponseApi<Supplier>>(`${endPoint}/${id}`),
  createSupplier: (payload: Omit<Supplier, 'Id'>) => httpThuVien.post<ResponseApi<{}>>(`${endPoint}/Create`, payload),
  updateSupplier: (payload: Supplier) => httpThuVien.put<ResponseApi<{}>>(`${endPoint}`, payload)
};

export default supplierApis;
