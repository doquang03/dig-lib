import { LoadingOutlined } from '@ant-design/icons';
import classnames from 'classnames';
import { ButtonHTMLAttributes, MouseEvent, ReactNode } from 'react';

export type ButtonCustomProps = {
  children: ReactNode;
  variant?: 'default' | 'danger' | 'primary' | 'primary-outline' | 'secondary' | 'disabled' | 'custom';
  loading?: boolean;
} & ButtonHTMLAttributes<HTMLButtonElement>;

const Button = (props: ButtonCustomProps) => {
  const { className, children, variant = 'default', disabled, onClick, loading = false, ...rest } = props;

  const handleOnClick = (e: MouseEvent<HTMLButtonElement>) => {
    if (disabled || loading) {
      return;
    }

    onClick?.(e);
  };

  return (
    <button
      className={classnames(
        `flex transform items-center justify-center gap-1 rounded py-2.5 px-2.5 font-semibold shadow-md transition duration-300 ${className}`,
        {
          'cursor-not-allowed': disabled || loading,
          'bg-primary-20 text-white hover:bg-primary-30': variant === 'default',
          'bg-primary-30 text-white hover:bg-primary-40': variant === 'primary',
          'border border-primary-30 bg-white text-primary-30 hover:bg-primary-30 hover:text-white':
            variant === 'primary-outline',
          'bg-danger-10 text-white hover:bg-danger-20': variant === 'danger',
          'bg-secondary-20 text-white hover:bg-secondary-20/70': variant === 'secondary',
          'cursor-not-allowed bg-gray-400 text-white opacity-70': variant === 'disabled' || loading
        }
      )}
      onClick={handleOnClick}
      {...rest}
      disabled={loading}
    >
      {loading && <LoadingOutlined style={{ fontSize: 24, marginLeft: 2 }} spin />}
      {children}
    </button>
  );
};

export default Button;
