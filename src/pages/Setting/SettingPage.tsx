import { CKEditor } from '@ckeditor/ckeditor5-react';
import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Radio, Tooltip } from 'antd';
import { settingApis } from 'apis';
import LinkIcon from 'assets/svgs/Link';
import CustomEditorBuild from 'ckeditor5-custom-build';
import { Button, Input, Loading, Select, Title } from 'components';
import {
  DAY28_OPTIONS,
  DAY30_OPTIONS,
  DAY_OPTIONS,
  MONTH30_OPTIONS,
  MONTH31_OPTIONS,
  MONTH_OPTIONS
} from 'constants/options';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import 'css/SettingPage.css';
import { useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { settingSchema } from 'utils/rules';
import * as yup from 'yup';

type SettingForm = yup.InferType<typeof settingSchema> & { TenMien: string };
const initialSettingValue = {
  TenThuVien: '',
  SoNgayMuon: 7,
  SoNgayMuonGiaoVien: 365,
  SLSachDuocDatMuon: 1,
  ThoiGianHieuLucDatSach: 24
};

const SettingPage = () => {
  const [classify, setClassify] = useState('Day19');
  const [dayOptions, setDayOptions] = useState(DAY_OPTIONS);
  const [monthOptions, setMonthOptions] = useState(MONTH_OPTIONS);
  const [item, setItem] = useState({});
  const [noiQuy, setNoiQuy] = useState('');
  const [editor, setEditor] = useState<typeof CustomEditorBuild>();
  const [lockReader, setLockReader] = useState<boolean>(false);

  const navigate = useNavigate();

  const { userType, isAllowedAdjustment, profile } = useUser();

  const {
    register,
    handleSubmit,
    setValue,
    getValues,
    formState: { errors }
  } = useForm<SettingForm>({
    defaultValues: initialSettingValue,
    resolver: yupResolver(settingSchema)
  });

  const { data, isFetching } = useQuery({
    queryKey: ['GeneralSetting'],
    queryFn: () => settingApis.Index(),
    onSuccess: (res) => {
      let Item = res?.data?.Item;
      setItem(Item);
      setValue('TenThuVien', Item?.TenThuVien);
      setValue('DiaChi', Item?.DiaChi);
      setValue('SDT', Item?.SDT);
      setValue('Email', Item?.Email);
      if (Item?.TheHeader1 !== '') setValue('TheHeader1', Item?.TheHeader1);
      else setValue('TheHeader1', 'thuviensohcm');
      setValue('TheHeader2', Item?.TheHeader2 || profile?.NameSchool);
      setValue('SoNgayMuon', Item?.SoNgayMuon);
      setValue('SoNgayMuonGiaoVien', Item?.SoNgayMuonGiaoVien);
      setValue('SoNgayMuonMax', Item?.SoNgayMuonMax);
      setValue('SoNgayMuonGiaoVienMax', Item?.SoNgayMuonGiaoVienMax);
      setValue('NgaySachMoi', Item?.NgaySachMoi);
      setValue('ThangSachMoi', Item?.ThangSachMoi);
      setClassify(Item?.CaiDatMonLoai);
      setValue('SLSachDuocDatMuon', Item?.SLSachDuocDatMuon);
      setValue('ThoiGianHieuLucDatSach', Item?.ThoiGianHieuLucDatSach);
      setNoiQuy(Item?.NoiQuy);
      setValue('TenMien', Item.TenMien);
      setLockReader(Item.IsActiveTenMien);
    },
    onError: () => {
      navigate(path.home);
    }
  });

  const totalMemory = data?.data?.Item?.TongDungLuong / 1073741824 || 0;

  const dung_luong_image = data?.data?.Item?.DungLuong[0] / 1073741824 || 0;
  const dung_luong_document = data?.data?.Item?.DungLuong[1] / 1073741824 || 0;
  const dung_luong_audio = data?.data?.Item?.DungLuong[2] / 1073741824 || 0;
  const dung_luong_video = data?.data?.Item?.DungLuong[3] / 1073741824 || 0;

  const isDisable = useMemo(() => isFetching, [isFetching]);

  const { mutate: handleComplete, isLoading: isLoadingComplete } = useMutation({
    mutationFn: (payload: object) => settingApis.Edit(payload),
    onSuccess: (data) => {
      toast.success('Cập nhật cài đặt thành công');
      setItem(data);
    },
    onError: () => {}
  });

  const onSubmit = handleSubmit((data) => {
    let finalData = {
      TenThuVien: data.TenThuVien,
      DiaChi: data.DiaChi,
      TheHeader1: data.TheHeader1,
      TheHeader2: data.TheHeader2,
      SoNgayMuon: data.SoNgayMuon,
      SoNgayMuonGiaoVien: data.SoNgayMuonGiaoVien,
      SoNgayMuonMax: data.SoNgayMuonMax,
      SoNgayMuonGiaoVienMax: data.SoNgayMuonGiaoVienMax,
      NgaySachMoi: data.NgaySachMoi,
      ThangSachMoi: data.ThangSachMoi,
      CaiDatMonLoai: classify,
      SLSachDuocDatMuon: data.SLSachDuocDatMuon,
      ThoiGianHieuLucDatSach: data.ThoiGianHieuLucDatSach,
      TenMien: data.TenMien,
      IsActiveTenMien: lockReader,
      NoiQuy: editor?.getData(),
      ResetMCB: '0',
      SDT: data.SDT,
      Email: data.Email
    };

    handleComplete(Object.assign(item, finalData));
  });

  if (userType === 2001 && !isAllowedAdjustment) {
    return <h2 className='p-2 '>Bạn không có quyền truy cập chức năng này</h2>;
  }

  const onChangeClassify = (e: any) => {
    setClassify(e?.target?.value);
  };

  const ruleChangeMonth = (value: string) => {
    switch (value) {
      case '31':
        setMonthOptions(MONTH31_OPTIONS);
        break;
      case '30':
        setMonthOptions(MONTH30_OPTIONS);
        break;
      default:
        setMonthOptions(MONTH_OPTIONS);
        break;
    }
  };

  const ruleChangeDay = (value: string) => {
    switch (value) {
      case '01':
      case '03':
      case '05':
      case '07':
      case '08':
      case '10':
      case '12':
        setDayOptions(DAY_OPTIONS);
        break;
      case '04':
      case '06':
      case '09':
      case '11':
        setDayOptions(DAY30_OPTIONS);
        break;
      case '02':
        setDayOptions(DAY28_OPTIONS);
        break;
    }
  };

  return (
    <div className='setting-page p-5'>
      <Title title='Cài đặt thư viện' />

      <form className='container mt-5' onSubmit={onSubmit}>
        <div className='thongtin-group'>
          <h5 className='title-box'>
            Thông tin chung
            <div className='background-fake'></div>
          </h5>
          <div className='form-group form-group-sm row'>
            <label className='text-inbox col-sm-3 font-weight'>
              Tên thư viện<span className='text-danger-10'>*</span>
            </label>
            <div className='col-sm-8 '>
              <Input
                placeholder='Nhập tên thư viện'
                name='TenThuVien'
                register={register}
                errorMessage={errors?.TenThuVien?.message}
                disabled={isDisable}
                maxLength={256}
              />
            </div>
          </div>
          <div className='form-group form-group-sm row'>
            <label className='text-inbox col-sm-3 font-weight'>
              Tên miền<span className='text-danger-10'>*</span>
            </label>
            <div className='col-sm-8 flex items-center gap-2'>
              <Input
                placeholder='Nhập tên thư viện'
                name='TenMien'
                register={register}
                errorMessage={errors?.TenMien?.message}
                disabled={isDisable}
                maxLength={256}
                containerClassName='flex-grow'
              />
              <div onClick={() => setLockReader(!lockReader)} className='flex items-center gap-2'>
                <Input
                  type='checkbox'
                  onChange={() => setLockReader(!lockReader)}
                  checked={lockReader}
                  className='flex'
                />{' '}
                <label htmlFor=''>Bật/Tắt Cổng thông tin</label>
              </div>
              <a className='flex' href={'http://' + getValues('TenMien')} target='_blank' rel='noopener noreferrer'>
                <LinkIcon />
              </a>
            </div>
          </div>
          <div className='form-group form-group-sm row'>
            <label className='text-inbox col-sm-3 font-weight'>
              Địa chỉ<span className='text-danger-10'>*</span>
            </label>
            <div className='col-sm-8'>
              <Input
                placeholder='Nhập địa chỉ'
                name='DiaChi'
                register={register}
                errorMessage={errors?.DiaChi?.message}
                disabled={isDisable}
                maxLength={256}
              />
            </div>
          </div>
          <div className='form-group form-group-sm row'>
            <label className='text-inbox col-sm-3 font-weight'>Số điện thoại</label>
            <div className='col-sm-8'>
              <Input
                placeholder='Nhập số diện thoại'
                name='SDT'
                register={register}
                errorMessage={errors?.SDT?.message}
                disabled={isDisable}
                maxLength={25}
              />
            </div>
          </div>
          <div className='form-group form-group-sm row'>
            <label className='text-inbox col-sm-3 font-weight'>Email</label>
            <div className='col-sm-8'>
              <Input
                placeholder='Nhập email'
                name='Email'
                register={register}
                errorMessage={errors?.Email?.message}
                disabled={isDisable}
                maxLength={256}
              />
            </div>
          </div>
        </div>
        <div className='thongtin-group'>
          <h5 className='title-box'>
            Thẻ thư viện
            <div className='background-fake'></div>
          </h5>
          <div className='form-group form-group-sm row'>
            <label className='text-inbox col-sm-3 font-weight'>Thông tin Sở Phòng</label>
            <div className='col-sm-8'>
              <Input
                placeholder='Nhập thông tin Sở phòng'
                name='TheHeader1'
                register={register}
                errorMessage={errors?.TheHeader1?.message}
                maxLength={256}
              />
            </div>
          </div>
          <div className='form-group form-group-sm row'>
            <label className='text-inbox col-sm-3 font-weight'>Thông tin trường</label>
            <div className='col-sm-8'>
              <Input
                placeholder='Nhập thông tin trường'
                name='TheHeader2'
                register={register}
                errorMessage={errors?.TheHeader2?.message}
                maxLength={256}
              />
            </div>
          </div>
        </div>

        <div className='grid grid-cols-1 md:grid-cols-12'>
          <div className='thongtin-group col-span-7 md:mr-5'>
            <h5 className='title-box'>
              Mượn trả sách
              <div className='background-fake'></div>
            </h5>

            <div className='flex flex-1 items-center justify-center'>
              <div>
                <div className='form-group form-group-sm row'>
                  <label className='title-inbox font-weight' style={{ height: '20px', lineHeight: '20px' }}>
                    Số ngày tối đa
                  </label>
                </div>

                <div className='form-group form-group-sm row flex items-center'>
                  <label className='col-sm-4'>
                    Học sinh <span className='text-danger-10'>*</span>
                  </label>
                  <div className='col-sm-8 flex items-center'>
                    <Input
                      containerClassName='grow'
                      onKeyPress={(event) => {
                        if (!/[0-9]/.test(event.key)) {
                          event.preventDefault();
                        }
                      }}
                      name='SoNgayMuonMax'
                      style={{ textAlign: 'right' }}
                      register={register}
                      errorMessage={errors?.SoNgayMuon?.message}
                      disabled={isDisable}
                      maxLength={3}
                    />
                    <div className='text-note shrink-0'>
                      <label>ngày</label>
                    </div>
                  </div>
                </div>
                <div className='form-group form-group-sm row flex items-center'>
                  <label className='col-sm-4'>
                    Giáo viên <span className='text-danger-10'>*</span>
                  </label>
                  <div className='col-sm-8 flex'>
                    <Input
                      containerClassName='grow'
                      onKeyPress={(event) => {
                        if (!/[0-9]/.test(event.key)) {
                          event.preventDefault();
                        }
                      }}
                      name='SoNgayMuonGiaoVienMax'
                      style={{ textAlign: 'right' }}
                      register={register}
                      errorMessage={errors?.SoNgayMuonGiaoVien?.message}
                      disabled={isDisable}
                      maxLength={3}
                    />
                    <div className='text-note shrink-0'>
                      <label>ngày</label>
                    </div>
                  </div>
                </div>
              </div>

              <div>
                <div className='form-group form-group-sm row flex items-center'>
                  <label className='title-inbox font-weight' style={{ height: '20px', lineHeight: '20px' }}>
                    Số ngày mặc định
                  </label>
                </div>
                <div className='form-group form-group-sm row flex items-center'>
                  <label className='col-sm-4'>
                    Học sinh <span className='text-danger-10'>*</span>
                  </label>
                  <div className='col-sm-8 flex'>
                    <Input
                      containerClassName='grow'
                      onKeyPress={(event) => {
                        if (!/[0-9]/.test(event.key)) {
                          event.preventDefault();
                        }
                      }}
                      name='SoNgayMuon'
                      style={{ textAlign: 'right' }}
                      register={register}
                      errorMessage={errors?.SoNgayMuon?.message}
                      disabled={isDisable}
                      maxLength={3}
                    />
                    <div className='text-note shrink-0'>
                      <label>ngày</label>
                    </div>
                  </div>
                </div>
                <div className='form-group form-group-sm row flex items-center'>
                  <label className='col-sm-4'>
                    Giáo viên <span className='text-danger-10'>*</span>
                  </label>
                  <div className='col-sm-8 flex'>
                    <Input
                      containerClassName='grow'
                      onKeyPress={(event) => {
                        if (!/[0-9]/.test(event.key)) {
                          event.preventDefault();
                        }
                      }}
                      name='SoNgayMuonGiaoVien'
                      style={{ textAlign: 'right' }}
                      register={register}
                      errorMessage={errors?.SoNgayMuonGiaoVien?.message}
                      disabled={isDisable}
                      maxLength={3}
                    />
                    <div className='text-note shrink-0'>
                      <label>ngày</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='thongtin-group col-span-5'>
            <h5 className='title-box'>
              Thời gian bắt đầu năm học
              <div className='background-fake'></div>
            </h5>
            <div className='form-group form-group-sm row'>
              <label className='title-inbox font-weight' style={{ height: '20px' }}>
                {' '}
              </label>
            </div>
            <div className='form-group form-group-sm row'>
              <label className='text-inbox col-sm-8'>
                Ngày <span className='text-danger-10'>*</span>
              </label>
              <div className='col-sm-4'>
                <Select
                  items={dayOptions}
                  className='w-full'
                  name='NgaySachMoi'
                  register={register}
                  onChange={(event) => {
                    ruleChangeMonth(event?.target?.value);
                  }}
                  errorMessage={errors?.NgaySachMoi?.message}
                  disabled={isDisable}
                />
              </div>
            </div>
            <div className='form-group form-group-sm row'>
              <label className='text-inbox col-sm-8'>
                Tháng <span className='text-danger-10'>*</span>
              </label>
              <div className='col-sm-4'>
                <Select
                  items={monthOptions}
                  className='w-full'
                  name='ThangSachMoi'
                  register={register}
                  onChange={(event) => {
                    ruleChangeDay(event?.target?.value);
                  }}
                  errorMessage={errors?.ThangSachMoi?.message}
                  disabled={isDisable}
                />
              </div>
            </div>
          </div>
        </div>
        <div className='grid grid-cols-1 md:grid-cols-12'>
          <div className='thongtin-group col-span-5'>
            <h5 className='title-box'>
              Khung phân loại
              <div className='background-fake'></div>
            </h5>
            <Radio.Group className='form-group form-group-sm row' onChange={onChangeClassify} value={classify}>
              <Radio value={'Day19'} className='text-inbox'>
                Phân loại 19 dãy
              </Radio>
              <Radio value={'DDC'} className='text-inbox'>
                Phân loại DDC
              </Radio>
            </Radio.Group>
          </div>
          <div className='thongtin-group col-span-7 md:ml-5'>
            <h5 className='title-box'>
              Cấu hình đặt mượn sách
              <div className='background-fake'></div>
            </h5>
            <div className='form-group form-group-sm row'>
              <label className='text-inbox col-sm-8'>
                Số lượng sách được mượn tối đa <span className='text-danger-10'>*</span>
              </label>
              <div className='col-sm-4 flex '>
                <Input
                  containerClassName='grow'
                  onKeyPress={(event) => {
                    if (!/[0-9]/.test(event.key)) {
                      event.preventDefault();
                    }
                  }}
                  name='SLSachDuocDatMuon'
                  style={{ textAlign: 'right' }}
                  register={register}
                  errorMessage={errors?.SLSachDuocDatMuon?.message}
                  disabled={isDisable}
                  maxLength={2}
                />
                <div className='text-note shrink-0'>
                  <label>Quyển</label>
                </div>
              </div>
            </div>
            <div className='form-group form-group-sm row'>
              <label className='text-inbox col-sm-8'>
                Thời gian giữ sách đặt mượn <span className='text-danger-10'>*</span>
              </label>
              <div className='col-sm-4 flex'>
                <Input
                  containerClassName='grow'
                  onKeyPress={(event) => {
                    if (!/[0-9]/.test(event.key)) {
                      event.preventDefault();
                    }
                  }}
                  name='ThoiGianHieuLucDatSach'
                  style={{ textAlign: 'right' }}
                  register={register}
                  errorMessage={errors?.ThoiGianHieuLucDatSach?.message}
                  disabled={isDisable}
                  maxLength={2}
                />
                <div className='text-note shrink-0'>
                  <label>Giờ</label>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className=' mb-5'>
          <div className='flex items-center justify-between'>
            <h3 className='flex gap-1 font-bold text-primary-10'>
              THÔNG TIN DUNG LƯỢNG LƯU TRỮ{' '}
              <Tooltip title={'Thầy/Cô hãy liên hệ với Phòng CSKH để mua thêm dung lượng lưu trữ'}>
                <svg width='11' height='11' viewBox='0 0 11 11' fill='none' xmlns='http://www.w3.org/2000/svg'>
                  <path
                    d='M5.42648 0.000499844C2.38929 0.041152 -0.0397994 2.5366 0.000494124 5.57403C0.0411463 8.61015 2.53635 11.04 5.57355 10.9995C8.61027 10.9587 11.04 8.46321 10.9995 5.42601C10.9589 2.38965 8.46344 -0.0400328 5.42648 0.000499844ZM6.03615 1.83331C6.59523 1.83331 6.76035 2.15781 6.76035 2.52859C6.76035 2.99214 6.38922 3.42066 5.75648 3.42066C5.22681 3.42066 4.97488 3.15415 4.98959 2.71403C4.98959 2.34314 5.30034 1.83331 6.03615 1.83331ZM4.60292 8.93752C4.22091 8.93752 3.94089 8.7058 4.20823 7.68603L4.64656 5.87761C4.72272 5.58778 4.73552 5.47192 4.64656 5.47192C4.5319 5.47192 4.03594 5.67208 3.74301 5.86912L3.55194 5.55622C4.48132 4.78012 5.55011 4.32481 6.00829 4.32481C6.3903 4.32481 6.45391 4.77665 6.26308 5.47192L5.76103 7.37301C5.67207 7.70911 5.71033 7.82485 5.79905 7.82485C5.91395 7.82485 6.2895 7.68543 6.65848 7.39561L6.87502 7.68543C5.97098 8.58935 4.98469 8.93752 4.60292 8.93752Z'
                    fill='#184785'
                  />
                </svg>
              </Tooltip>
            </h3>

            <div className='flex items-center gap-1'>
              <svg width={20} height={19} viewBox='0 0 20 19' fill='none' xmlns='http://www.w3.org/2000/svg'>
                <path
                  d='M9.90126 0.00012207C4.43313 0.00012207 0 4.2536 0 9.50012C0 14.7466 4.43313 19.0001 9.90126 19.0001C15.3694 19.0001 19.8025 14.7466 19.8025 9.50012C19.8025 4.2536 15.3694 0.00012207 9.90126 0.00012207Z'
                  fill='#A23434'
                />
                <path
                  d='M4.64548 14.8226C10.5975 14.8226 15.4228 10.1928 15.4228 4.48208C15.4241 3.40144 15.2487 2.32729 14.903 1.29884C13.3863 0.445557 11.6594 -0.00288853 9.90127 1.40008e-05C4.43315 1.40008e-05 1.12604e-05 4.25349 1.12604e-05 9.50001C-0.00267166 11.2029 0.474119 12.8748 1.38 14.3391C2.43608 14.6605 3.53754 14.8236 4.64548 14.8226Z'
                  fill='#AF727B'
                />
                <path
                  d='M9.89718 15.9442C9.57895 15.9442 9.26786 15.8536 9.00324 15.684C8.73862 15.5144 8.53235 15.2734 8.41049 14.9913C8.28864 14.7092 8.25668 14.3988 8.31865 14.0993C8.38062 13.7999 8.53375 13.5247 8.75866 13.3087C8.98358 13.0927 9.27019 12.9455 9.58227 12.8858C9.89435 12.826 10.2179 12.8564 10.512 12.973C10.8061 13.0897 11.0575 13.2874 11.2345 13.5411C11.4116 13.7948 11.5062 14.0932 11.5065 14.3986C11.5067 14.6015 11.4653 14.8024 11.3845 14.9899C11.3037 15.1774 11.1852 15.3478 11.0357 15.4914C10.8862 15.6349 10.7088 15.7488 10.5134 15.8265C10.3181 15.9042 10.1087 15.9442 9.89718 15.9442ZM11.457 10.6264C11.4299 10.865 11.3176 11.0872 11.1391 11.2554C10.9582 11.4247 10.7229 11.5303 10.4715 11.5549H10.4642C10.3065 11.5722 10.1479 11.5808 9.98923 11.5808C9.79899 11.5818 9.60882 11.5731 9.41952 11.5549H9.41179C9.16053 11.5302 8.92537 11.4247 8.74461 11.2554C8.5661 11.0872 8.45377 10.865 8.42669 10.6264C8.38801 10.2779 8.36868 7.85803 8.36868 7.31883C8.36868 6.77963 8.3884 4.35342 8.42669 4.00496C8.45334 3.7657 8.5657 3.54273 8.74461 3.3741C8.92553 3.20507 9.1606 3.09955 9.41179 3.07463H9.41952C9.6088 3.05649 9.79916 3.05091 9.98923 3.05793C10.1476 3.0509 10.3064 3.05649 10.4638 3.07463H10.4715C10.7232 3.10028 10.9585 3.20687 11.1391 3.37707C11.318 3.5457 11.4304 3.76867 11.457 4.00793C11.4957 4.35639 11.515 6.78297 11.515 7.3218C11.515 7.86063 11.4953 10.2794 11.457 10.6264Z'
                  fill='white'
                />
              </svg>

              <p className='text-sm font-semibold text-[#A23434]'>
                Còn trống{' '}
                {(totalMemory - (dung_luong_video + dung_luong_audio + dung_luong_document + dung_luong_image)).toFixed(
                  2
                )}{' '}
                GB trong tổng số {totalMemory} GB
              </p>
            </div>
          </div>

          <div className='relative mt-2 h-[1.5rem] w-full rounded-md' style={{}}>
            <Tooltip title={'Tài liệu ' + dung_luong_document.toFixed(2) + 'GB'} arrow={true}>
              <div
                className='absolute left-0 h-[1.5rem] rounded-l-md bg-[#1D396A]'
                style={{
                  width: `${(dung_luong_document / totalMemory) * 100}%`
                }}
              />
            </Tooltip>
            <Tooltip title={'Video ' + dung_luong_video.toFixed(2) + 'GB'} arrow={true}>
              <div
                className={`absolute h-[1.5rem] bg-[#A23434]`}
                style={{
                  width: `${(dung_luong_video / totalMemory) * 100}%`,
                  left: `${(dung_luong_document / totalMemory) * 100}%`
                }}
              />
            </Tooltip>
            <Tooltip title={'Audio ' + dung_luong_audio.toFixed(2) + 'GB'} arrow={true}>
              <div
                className='absolute h-[1.5rem] bg-[#119757]'
                style={{
                  width: `${(dung_luong_audio / totalMemory) * 100}%`,
                  left: `${((dung_luong_video + dung_luong_document) / totalMemory) * 100}%`
                }}
              />
            </Tooltip>
            <Tooltip title={'Hình ảnh ' + dung_luong_image.toFixed(2) + 'GB'} arrow={true}>
              <div
                className='absolute h-[1.5rem] bg-[#892DA9]'
                style={{
                  width: `${(dung_luong_image / totalMemory) * 100}%`,
                  left: `${((dung_luong_video + dung_luong_document + dung_luong_audio) / totalMemory) * 100}%`
                }}
              />
            </Tooltip>
            <Tooltip
              title={
                'Còn trống ' +
                (totalMemory - (dung_luong_video + dung_luong_audio + dung_luong_document + dung_luong_image)).toFixed(
                  2
                ) +
                'GB'
              }
              arrow={true}
            >
              <div
                className='absolute h-[1.5rem] bg-[#E7E6E694]'
                style={{
                  width: `${
                    ((totalMemory - (dung_luong_video + dung_luong_audio + dung_luong_document + dung_luong_image)) /
                      totalMemory) *
                    100
                  }%`,
                  left: `${
                    ((dung_luong_video + dung_luong_document + dung_luong_audio + dung_luong_image) / totalMemory) * 100
                  }%`
                }}
              />
            </Tooltip>
          </div>
          <div className='mt-2 flex items-center gap-5'>
            <Tooltip title={'Tài liệu ' + dung_luong_document.toFixed(2) + 'GB'} arrow={true}>
              <div className='inline-flex items-center gap-1'>
                <div className='h-4 w-4 rounded-full bg-[#1D396A]' />

                <p>Tài liệu</p>
              </div>
            </Tooltip>
            <Tooltip title={'Video ' + dung_luong_video.toFixed(2) + 'GB'} arrow={true}>
              <div className='inline-flex items-center gap-1'>
                <div className='h-4 w-4 rounded-full bg-[#A23434]' />

                <p>Video</p>
              </div>
            </Tooltip>
            <Tooltip title={'Audio ' + dung_luong_audio.toFixed(2) + 'GB'} arrow={true}>
              <div className='inline-flex items-center gap-1'>
                <div className='h-4 w-4 rounded-full bg-[#119757]' />

                <p>Audio</p>
              </div>
            </Tooltip>
            <Tooltip title={'Hình ảnh ' + dung_luong_image.toFixed(2) + 'GB'} arrow={true}>
              <div className='inline-flex items-center gap-1'>
                <div className='h-4 w-4 rounded-full bg-[#892DA9]' />

                <p>Hình ảnh</p>
              </div>
            </Tooltip>
            <Tooltip
              title={
                'Còn trống ' +
                (totalMemory - (dung_luong_video + dung_luong_audio + dung_luong_document + dung_luong_image)).toFixed(
                  2
                ) +
                'GB'
              }
              arrow={true}
            >
              <div className='inline-flex items-center gap-1'>
                <div className='h-4 w-4 rounded-full bg-[#E7E6E694]' />

                <p>Còn trống</p>
              </div>
            </Tooltip>
          </div>
        </div>

        <div className='thongtin-group'>
          <h5 className='title-box'>
            Nội quy thư viện
            <div className='background-fake'></div>
          </h5>
          <div className='form-group form-group-sm row ckeditor-container' style={{ margin: 'auto' }}>
            <CKEditor
              editor={CustomEditorBuild}
              data={noiQuy}
              disabled={isDisable}
              onReady={(editor) => {
                setEditor(editor);
              }}
            ></CKEditor>
          </div>
        </div>
        <div className='mt-5 flex justify-end'>
          <Button variant='default' type='submit' className='ml-2 font-semibold'>
            Cập nhật
          </Button>
        </div>
      </form>
      <Loading open={isLoadingComplete}></Loading>
    </div>
  );
};

export default SettingPage;
