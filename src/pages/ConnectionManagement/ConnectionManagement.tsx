import { Outlet } from 'react-router-dom';

const ConnectionManagement = () => <Outlet />;

export default ConnectionManagement;
