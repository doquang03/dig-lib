import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/BanDocVangLai`;

const visitingReaderApis = {
  GetAll: (params: { TextForSearch?: string; LoaiTK?: string; WorkingId?: string; page: number; pageSize: number }) =>
    httpThuVien.post<ResponseApi<{ ListModel: VisitingReader[]; Count: number }>>(`/${endPoint}/IndexByModel`, params)
};

export default visitingReaderApis;
