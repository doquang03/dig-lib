import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { supplierApis } from 'apis';
import { AxiosError } from 'axios';
import { Button, Input, Loading, Title } from 'components';
import { path } from 'constants/path';
import { useEffect, useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { supplierSchema } from 'utils/rules';

const initalFormValues: Omit<Supplier, 'Id' | 'TinhChat'> = {
  Code: '',
  TenNguonCungCap: ''
};

const AddSupplierPage = () => {
  const [type, setType] = useState<number>(1);

  const { id } = useParams();

  const navigate = useNavigate();

  const queryClient = useQueryClient();

  const {
    register,
    handleSubmit,
    setValue,
    setError,
    formState: { errors }
  } = useForm<typeof initalFormValues>({
    defaultValues: initalFormValues,
    resolver: yupResolver(supplierSchema)
  });

  const { data: supplierData, refetch } = useQuery({
    queryKey: ['supplier', id],
    queryFn: () => supplierApis.getSupplier(id + ''),
    enabled: !!id,
    onError: () => {
      navigate(path.nguoncungcap);
    }
  });

  const supplier = useMemo(() => {
    if (!supplierData?.data.Item) {
      return;
    }

    return supplierData.data.Item;
  }, [supplierData?.data.Item]);

  const { mutate: createSupplier, isLoading: loadingCreateSupplier } = useMutation({
    mutationFn: supplierApis.createSupplier,
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ['suppliers'] });
      toast.success('Tạo nguồn cung cấp thành công');
      navigate(path.nguoncungcap);
    },
    onError: (error: AxiosError<ResponseApi<{}>>) => {
      setError('Code', { type: 'validate', message: error.response?.data?.Message }, { shouldFocus: true });
    }
  });

  const { mutate: updateSupplier, isLoading: loadingUpdateSupplier } = useMutation({
    mutationFn: supplierApis.updateSupplier,
    onSuccess: () => {
      refetch();
      toast.success('Cập nhật nguồn cung cấp thành công');
    }
  });

  useEffect(() => {
    if (Boolean(id) && supplier) {
      setValue('TenNguonCungCap', supplier?.TenNguonCungCap + '');
      setValue('Code', supplier?.Code + '');
      setType(supplier?.TinhChat);
    }
  }, [supplier?.TenNguonCungCap, supplier?.Code, id, setValue, supplier?.TinhChat, supplier]);

  const onSubmit = handleSubmit((data) => {
    let input = data.TenNguonCungCap.replace(/\s+/g, ' ');
    let code = data?.Code?.replace(/\s+/g, ' ');

    if (!Boolean(id)) {
      !loadingCreateSupplier && createSupplier({ ...data, Code: code, TenNguonCungCap: input, TinhChat: type });
    } else {
      !loadingUpdateSupplier &&
        updateSupplier({ ...data, TenNguonCungCap: input, Code: code, Id: supplier?.Id + '', TinhChat: type });
    }
  });

  const handleOnChange = (type: number) => () => {
    setType(type);
  };

  const handleBack = () => navigate(path.nguoncungcap);

  return (
    <div className='p-5'>
      <Title title={Boolean(id) ? 'Cập nhật nguồn cung cấp' : 'Thêm mới nguồn cung cấp'} />

      <form className='w-1/2 px-3' onSubmit={onSubmit}>
        <label className='mt-3 font-bold'>
          Tên nguồn cung cấp <span className='text-danger-10'>*</span>
        </label>
        <Input
          placeholder='Nhập tên nguồn cung cấp'
          name='TenNguonCungCap'
          register={register}
          errorMessage={errors?.TenNguonCungCap?.message}
          maxLength={256}
        />

        <label className='mt-4 font-bold'>
          Mã nguồn cung cấp <span className='text-danger-10'>*</span>
        </label>
        <Input
          placeholder='Nhập mã nguồn cung cấp'
          name='Code'
          register={register}
          errorMessage={errors?.Code?.message}
          maxLength={256}
        />

        <label className='mt-4 font-bold'>
          Tính chất <span className='text-danger-10'>*</span>
        </label>

        <button className='flex gap-2' onClick={handleOnChange(1)} type='button'>
          <Input type={'radio'} checked={type === 1} />

          <label>Mua</label>
        </button>

        <button className='flex gap-2' onClick={handleOnChange(0)} type='button'>
          <Input type={'radio'} checked={type === 0} />

          <label>Miễn phí</label>
        </button>

        <div className='flex items-center justify-end gap-2'>
          <Button variant='secondary' onClick={handleBack} type='button'>
            Quay về
          </Button>

          <Button variant='default' type='submit' disabled={loadingCreateSupplier || loadingUpdateSupplier}>
            {Boolean(id) ? 'Cập nhật' : 'Thêm mới'}
          </Button>
        </div>
      </form>
      <Loading open={loadingCreateSupplier || loadingUpdateSupplier} />
    </div>
  );
};

export default AddSupplierPage;
