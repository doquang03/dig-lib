export { default as NewsPapgerMagazine } from './NewsPapgerMagazine';

export * from './NewsPaperMagazineListing';
export * from './RegisterNewsPaperMagazine';
export * from './AddNewspaper';
export * from './AddMagazine';
