import { useQuery } from '@tanstack/react-query';
import { ddcApis } from 'apis';
import classNames from 'classnames';
import { Button, Loading, SelectSearchForm, Title } from 'components';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import { useQueryConfig } from 'hooks';
import { useMemo } from 'react';
import { Link, useNavigate } from 'react-router-dom';

const DDCManagementPage = () => {
  const navigate = useNavigate();

  const queryConfig = useQueryConfig();

  const { isAllowedAdjustment } = useUser();

  const { data: ddcData, isLoading } = useQuery({
    queryKey: ['ddcs', queryConfig.OrderBy, queryConfig.TextForSearch],
    queryFn: () =>
      ddcApis.getDDCList({
        Keyword: queryConfig.TextForSearch,
        Sort: Number(queryConfig?.OrderBy) || 0
      })
  });

  const ddc = useMemo(() => {
    if (!ddcData?.data.Item) {
      return;
    }

    return ddcData?.data?.Item;
  }, [ddcData?.data?.Item]);

  const handleAddDDC = () => navigate(path.themDDC);
  const handleAddDDCByExcel = () => navigate(path.themDDCBangExcel);

  return (
    <div className='p-5'>
      <Title title='Danh sách DDC' />

      <div className='mt-4'>
        <SelectSearchForm
          placeholder='Nhập tên, mã DDC'
          items={[
            { label: 'Sắp xếp theo', value: '' },
            { label: 'Mã DDC tăng dần', value: 1 },
            { label: 'Mã DDC giảm dần', value: 2 },
            { label: 'Tên DDC tăng dần', value: 3 },
            { label: 'Tên DDC giảm dần', value: 4 }
          ]}
        />
      </div>

      <div
        className={classNames('', {
          'my-2 flex items-center gap-2': isAllowedAdjustment,
          hidden: !isAllowedAdjustment
        })}
      >
        <Button variant='default' onClick={handleAddDDC}>
          Thêm DDC
        </Button>

        <Button variant='default' onClick={handleAddDDCByExcel}>
          Thêm từ excel
        </Button>
      </div>

      <div className='mt-4 grid grid-cols-12 px-3'>
        <div className='col-span-6'>
          <ul className='list-inside list-disc'>
            {ddc?.slice(0, 500)?.map(({ Ten, MaDDC, Id }) => {
              const isBold = MaDDC === '000' || Number(MaDDC) % 100 === 0;

              return (
                <li key={Id} className={isBold ? 'text-[1.2rem] font-bold' : ''}>
                  <Link to={isAllowedAdjustment ? `CapNhat/${Id}` : ''}>
                    {MaDDC} - {Ten}
                  </Link>
                </li>
              );
            })}
          </ul>
        </div>

        <div className='col-span-6'>
          <ul className='list-inside list-disc'>
            {ddc?.slice(500, ddc?.length)?.map(({ Ten, MaDDC, Id }) => {
              const isBold = MaDDC === '000' || Number(MaDDC) % 100 === 0;

              return (
                <li key={Id} className={isBold ? 'text-[1.2rem] font-bold' : ''}>
                  <Link to={`CapNhat/${Id}`}>
                    {MaDDC} - {Ten}
                  </Link>
                </li>
              );
            })}
          </ul>
        </div>
      </div>

      <Loading open={isLoading} />
    </div>
  );
};

export default DDCManagementPage;
