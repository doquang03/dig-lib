import { DownChev } from 'assets';
import { ChangeEvent, forwardRef, type MouseEvent } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Menu } from './Toolbar';
import { useFolderManagement } from '../contexts/useFolderManagement';
import { type MapFolder } from '../FolderManagementPage';

type Props = Menu & {
  icon?: JSX.Element;
  selectedMenuIds?: string[];
  selectedMenuContexItem?: Menu;
  isModify: boolean;
  editable?: Menu;
  totalBooks?: number;
  mapFolder?: MapFolder;
  onDropdownClick?: (menuItemId: string) => void;
  onContextMenu?: (e: MouseEvent<HTMLAnchorElement, globalThis.MouseEvent>, item: Menu) => void;
  onSubmitChange?: VoidFunction;
  onChangeFolder?: (e: ChangeEvent<HTMLInputElement>, folder: Menu) => void;
};

const PARENT_IDS = ['paper', 'audio', 'document', 'image', 'video'];

const MenuItem = forwardRef<HTMLAnchorElement, Props>((props: Props, ref) => {
  const {
    id,
    idParent,
    icon,
    label,
    type,
    children = [],
    selectedMenuIds,
    selectedMenuContexItem,
    isModify,
    editable,
    totalBooks,
    mapFolder,
    onDropdownClick,
    onContextMenu,
    onSubmitChange,
    onChangeFolder
  } = props;

  const { id: idMenuFromParams } = useParams();

  const { setActiveFolder } = useFolderManagement();

  const isExpanding = selectedMenuIds?.includes(id);

  const isParent = PARENT_IDS.includes(id);

  return (
    <div className='relative'>
      {editable?.id === id ? (
        <div key={id} className='w-full rounded-md bg-gradient-to-r from-[#11277C] to-[#0000C5] p-[1px]'>
          <input
            placeholder='Nhập tên thư mục'
            defaultValue={label || ''}
            className='border-gradient-to-r w-full rounded-md border bg-white  p-1 outline-none'
            onChange={(e) => !!editable && onChangeFolder?.(e, editable)}
            onBlur={onSubmitChange}
            onKeyDown={(e) => {
              if (e.key === 'Enter') {
                onSubmitChange?.();
              }
            }}
            autoFocus
          />
        </div>
      ) : (
        <Link
          ref={ref}
          to={id}
          onClick={() => setActiveFolder({ id, label, children, type, idParent })}
          className={`flex w-full items-center gap-x-1 py-1  ${isModify ? 'bg-[#B1DDFF]/30' : ''} ${
            idMenuFromParams === id ? 'bg-[#B1DDFF] hover:bg-[#B1DDFF]' : 'hover:bg-[#B1DDFF]/30'
          } ${!children?.length ? 'ml-2' : ''} ${isParent ? 'font-bold' : 'font-normal'}`}
          key={id}
          onContextMenu={(e) =>
            onContextMenu?.(e, {
              id,
              idParent: idParent || '',
              label,
              type: type as FolderType,
              children: children || []
            })
          }
        >
          {!!children?.length && (
            <button
              type='button'
              onClick={(e) => {
                e.stopPropagation();
                e.preventDefault();
                onDropdownClick?.(id);
              }}
            >
              <DownChev
                className={`transition-all duration-300 ease-linear ${isExpanding ? 'rotate-0' : '-rotate-90'}`}
              />
            </button>
          )}{' '}
          {icon}{' '}
          <span className='whitespace-nowrap'>
            {label} {!isParent && !!totalBooks && `(${totalBooks})`}{' '}
          </span>{' '}
        </Link>
      )}

      {!!children?.length &&
        children.map((item) => (
          <div
            className={`ml-2 transition-all  ${
              isExpanding
                ? 'visible h-auto translate-x-1 opacity-100 delay-200 duration-200 ease-linear'
                : 'invisible h-0 -translate-x-1 opacity-0 duration-75 ease-linear'
            }`}
            key={item.id}
          >
            <div className={`flex flex-col`}>
              <MenuItem
                totalBooks={mapFolder?.[item.id]?.totalBooks || 0}
                editable={editable}
                {...item}
                onDropdownClick={onDropdownClick}
                selectedMenuIds={selectedMenuIds}
                onContextMenu={onContextMenu}
                isModify={selectedMenuContexItem?.id === item.id}
                onSubmitChange={() => onSubmitChange?.()}
                onChangeFolder={onChangeFolder}
              />
            </div>
          </div>
        ))}
    </div>
  );
});

export default MenuItem;
