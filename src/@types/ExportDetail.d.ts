type ExportDetail = {
  STT: number;
  IdChiTiet: number;
  IdSDKCB: string;
  MaKiemSoat: string;
  MaDKCB: string;
  TenSach: string;
  TrangThai: string;
  GiaTien: string;
};
