import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery } from '@tanstack/react-query';
import { topicApis } from 'apis';
import { AxiosError } from 'axios';
import { Button, Input, Loading, Title } from 'components';
import { path } from 'constants/path';
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { topicSchema } from 'utils/rules';
import * as yup from 'yup';
type TopicForm = yup.InferType<typeof topicSchema>;

const AddTopicPage = () => {
  const [currentSchema, setCurrentSchema] = useState(topicSchema);
  const [duplicateTen, setDuplicateTen] = useState<Array<string>>([]);
  const [isTrigger, setIsTrigger] = useState('idle');

  const {
    register,
    handleSubmit,
    setValue,
    trigger,
    formState: { errors }
  } = useForm<TopicForm>({
    defaultValues: {
      Ten: ''
    },
    resolver: yupResolver(currentSchema)
  });

  const { topicId } = useParams();

  const navigate = useNavigate();

  const { isFetching } = useQuery({
    queryKey: ['EditByID', topicId],
    queryFn: () => topicApis.EditByID(topicId as string),
    onSuccess: (res) => {
      setValue('Ten', res?.data?.Item?.Ten);
    },
    onError: () => {
      navigate(path.chudiem);
    },
    enabled: !!topicId
  });

  const { mutate: handleCreate, isLoading: isLoadingCreate } = useMutation({
    mutationFn: (payload: object) => topicApis.Create(payload),
    onSuccess: () => {
      toast.success('Thêm chủ điểm thành công');
      navigate(path.chudiem);
    },
    onError: (ex: AxiosError<ResponseApi<{ Ten: string }>>) => {
      checkValidate(ex);
    }
  });

  const { mutate: handleEdit, isLoading: isLoadingEdit } = useMutation({
    mutationFn: (payload: object) => topicApis.EditModel(payload),
    onSuccess: () => {
      toast.success('Cập nhật chủ điểm thành công');
    },
    onError: (ex: AxiosError<ResponseApi<{ Ten: string }>>) => {
      checkValidate(ex);
    }
  });

  const checkValidate = (ex: AxiosError<ResponseApi<{ Ten: string }>>) => {
    if (ex?.response?.data?.ExMessage === 'Ten') {
      let tempMonHoc = duplicateTen.concat([ex?.response?.data?.Item?.Ten]);
      setDuplicateTen(tempMonHoc);
      let topicSchemaClone = currentSchema.clone();
      let topicSchemaObject = yup.object({
        Ten: yup
          .string()
          .trim()
          .notOneOf(tempMonHoc, ex?.response?.data?.Message)
          .required('Tên chủ điểm không được để trống')
      });
      topicSchemaClone = topicSchemaClone.concat(topicSchemaObject);
      setCurrentSchema(topicSchemaClone);
      setIsTrigger('Ten');
    }
  };

  useEffect(() => {
    if (isTrigger === 'Ten') {
      trigger(isTrigger);
      setIsTrigger('idle');
    }
  }, [isTrigger, trigger]);

  const handleCompelete = handleSubmit((data) => {
    data.Ten = data.Ten.replace(/ +(?= )/g, '');
    setValue('Ten', data.Ten);
    if (!topicId) {
      handleCreate({ Ten: data.Ten });
    } else {
      handleEdit({ Id: topicId, Ten: data.Ten });
    }
  });

  function backTopic() {
    navigate(path.chudiem);
  }
  return (
    <div className='p-5'>
      <Title title={!!topicId ? 'CẬP NHẬT CHỦ ĐIỂM' : 'THÊM MỚI CHỦ ĐIỂM'} />

      <div className='relative my-6'>
        <div>
          <div className='form-group row' style={{ marginBottom: '10px' }}>
            <label
              className='col-form-label col-sm-2 font-bold'
              style={{ marginBottom: '0', paddingTop: '5px', width: '100%' }}
            >
              Tên chủ điểm <span style={{ color: 'red' }}>*</span>
            </label>
          </div>
          <div className='form-group row'>
            <div className='component-form-group'>
              <Input
                placeholder='Nhập tên chủ điểm'
                className='form-control form-control-sm'
                register={register}
                name='Ten'
                errorMessage={errors?.Ten?.message}
                disabled={!!topicId && isFetching}
                maxLength={256}
              ></Input>
            </div>
          </div>
          <div className='form-group row'>
            <div className='component-form-group flex' style={{ textAlign: 'end' }}>
              <Button className='gray-button' onClick={backTopic}>
                Quay về
              </Button>
              {!topicId ? (
                <Button
                  className='btn btn-primary btn-sm blue-button'
                  onClick={handleCompelete}
                  style={{ marginLeft: '10px' }}
                >
                  Thêm mới
                </Button>
              ) : (
                <Button
                  className='btn btn-primary btn-sm blue-button'
                  onClick={handleCompelete}
                  style={{ marginLeft: '10px' }}
                >
                  Cập nhật
                </Button>
              )}
            </div>
          </div>
        </div>
      </div>
      <Loading open={isLoadingCreate || isLoadingEdit}></Loading>
    </div>
  );
};

export default AddTopicPage;
