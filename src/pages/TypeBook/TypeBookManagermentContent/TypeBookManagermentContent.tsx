import { DeleteFilled, EditOutlined } from '@ant-design/icons';
import { useQuery } from '@tanstack/react-query';
import Table, { type ColumnsType } from 'antd/es/table';
import { typeBookApis } from 'apis';
import classNames from 'classnames';
import { Button, Input, Loading, ModalDelete, SizeChanger, Title } from 'components';
import type { ButtonCustomProps } from 'components/Button/Button';
import { path } from 'constants/path';
import { usePaginationNavigate, useQueryConfig } from 'hooks';
import { isEmpty, omitBy, trim } from 'lodash';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import { createSearchParams, useNavigate } from 'react-router-dom';
import { getSerialNumber } from 'utils/utils';

// TODO: Delete as soon as api implement
type Mock = {
  Id: string;
  IdParent: string;
  TenTheLoai: number;
  MoTa: string;
  MaDDC: number;
  MaTheLoai: string;
};

type FormInput = {
  TextForSearch: string;
};

const TypeBookManagermentContent = () => {
  const { register, handleSubmit, reset, setValue } = useForm<FormInput>({
    defaultValues: {
      TextForSearch: ''
    }
  });

  const navigate = useNavigate();

  const handleNavigation = usePaginationNavigate();

  const queryConfig = useQueryConfig();
  const [listTheLoaiSach, setListTheLoaiSach] = useState<any>();
  const [totalListTheLoaiSach, setTotalListTheLoaiSach] = useState(0);
  const [selectedDelete, setSelectedDelete] = useState<Mock>();
  const [isVisiable, setIsVisiable] = useState(false);
  const [loading, setLoading] = useState(false);
  const [expland, setExpland] = useState<Array<string>>([]);

  const { isLoading, refetch } = useQuery({
    queryKey: ['TheLoaiSach', queryConfig.page, queryConfig.pageSize, queryConfig.TextForSearch],
    queryFn: () => {
      let page = 1;
      if (typeof queryConfig.page === 'string') page = parseInt(queryConfig.page);
      let queryString = `?page=${page}&sizeNumber=${queryConfig.pageSize}`;
      if (queryConfig.TextForSearch !== undefined) queryString += `&TextForSearch=${queryConfig.TextForSearch}`;
      let request = typeBookApis.IndexByTree(queryString);
      request.then((res) => {
        let tempExpland = res?.data?.Item?.openRow;
        let check = [];
        let final = [];
        for (let i = 0; i < tempExpland.length; i++) {
          for (let j = 0; j < tempExpland[i].length; j++) {
            if (check[tempExpland[i][j]] === undefined) {
              final.push(tempExpland[i][j]);
              check[tempExpland[i][j]] = true;
            }
          }
        }
        setExpland(final);

        setListTheLoaiSach(filterListTheLoaiSach(res?.data?.Item?.ListTheLoaiSach));
        setTotalListTheLoaiSach(res?.data?.Item?.count);
      });
      return request;
    }
  });

  const onPaginate = useCallback(
    (page: number, pageSize: number) => {
      handleNavigation({ ...queryConfig, page: page + '', pageSize: pageSize + '' });
    },
    [handleNavigation, queryConfig]
  );

  const filterListTheLoaiSach = (data: any) => {
    type dicnationary = {
      [key: string]: any;
    };
    let checkParent: dicnationary = {};
    let final = [];
    for (let i = 0; i < data.length; i++) {
      let tempData = {
        Id: data[i].Id,
        IdParent: data[i].IdParent,
        MoTa: data[i].MoTa,
        TenTheLoai: data[i].TenTheLoai,
        MaTheLoai: data[i].MaTheLoai
      };
      if (checkParent[data[i].Id] === undefined) checkParent[data[i].Id] = tempData;

      if (data[i].IdParent == null) final.push(tempData);
      else {
        if (checkParent[data[i].IdParent].children === undefined) checkParent[data[i].IdParent].children = [];
        checkParent[data[i].IdParent].children.push(tempData);
      }
    }
    return final;
  };

  const pagination = useMemo(() => {
    return {
      current: (!!queryConfig.page && +queryConfig.page) || 1,
      pageSize: (!!queryConfig.pageSize && +queryConfig.pageSize) || 30,
      total: totalListTheLoaiSach,
      onChange: onPaginate,
      hideOnSinglePage: true,
      showSizeChanger: false,
      showQuickJumper: true,
      locale: { jump_to: '', page: '' }
    };
  }, [onPaginate, queryConfig.page, queryConfig.pageSize, totalListTheLoaiSach]);

  const columns: ColumnsType<Mock> = useMemo(() => {
    return [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        className: 'min-content',
        render: (value, record, index) => {
          return (
            queryConfig.page && queryConfig.pageSize && getSerialNumber(+queryConfig.page, +queryConfig.pageSize, index)
          );
        }
      },
      {
        title: 'Tên thể loại',
        dataIndex: 'TenTheLoai',
        key: 'TenTheLoai'
      },
      {
        title: 'Mã thể loại',
        dataIndex: 'MaTheLoai',
        key: 'MaTheLoai'
      },
      {
        title: 'Mô tả',
        dataIndex: 'MoTa',
        key: 'MoTa'
      },
      {
        title: 'Hành động',
        dataIndex: 'actions',
        key: 'actions',
        render: (value, record) => {
          return (
            <>
              <button
                className='mx-2'
                onClick={(e) => {
                  e.stopPropagation();
                  navigate(`CapNhat/${record.Id}`);
                }}
              >
                <EditOutlined style={{ fontSize: '20px', color: '#08c' }} />
              </button>

              <button
                className='mx-2'
                onClick={(e) => {
                  e.stopPropagation();
                  setSelectedDelete(() => {
                    return record;
                  });
                  setIsVisiable(true);
                }}
              >
                <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
              </button>
            </>
          );
        }
      }
    ];
  }, [navigate, queryConfig.page, queryConfig.pageSize]);

  useEffect(() => {
    Object.keys(queryConfig).forEach((key) => {
      if (!key) {
        return;
      }
      // @ts-ignore
      setValue(key, queryConfig[key]);
    });
  }, [queryConfig, setValue]);

  const onSubmit = handleSubmit((data) => {
    handleNavigation({
      ...omitBy({ ...data, TextForSearch: trim(data.TextForSearch, ' ').replace(/\s\s+/g, ' ') }, isEmpty),
      page: '1'
    });
  });

  const handleResetField = () => {
    const { pathname } = window.location;

    navigate({
      pathname: pathname,
      search: createSearchParams({}).toString()
    });

    reset();
  };

  // TODO: Do something with selected TypeBooks
  const handleSelectedTypeBooks = () => {
    navigate(path.themtheloaisach);
  };

  const handleSelectedExcelTypeBooks = () => {
    navigate(path.themtheloaisachexcel);
  };

  const handleDeleteTypeBook = () => {
    let request = typeBookApis.Xoa(selectedDelete?.Id as string);
    setLoading(true);
    setIsVisiable(false);
    request.then((res) => {
      setLoading(false);
      if (listTheLoaiSach.length === 1) handleResetField();
      else refetch();
    });
    request.catch((ex) => {
      setLoading(false);
    });
    return request;
  };
  return (
    <div className='p-5'>
      <Title title='DANH SÁCH THỂ LOẠI SÁCH' />

      <form className='my-5 flex flex-col flex-wrap gap-2 bg-secondary-10 py-3 px-3.5 md:flex-row' onSubmit={onSubmit}>
        <Input
          placeholder='Nhập tên thể loại sách'
          containerClassName='w-[100%] md:w-[920px]'
          name='TextForSearch'
          register={register}
        />

        <Button type='button' variant='secondary' className='font-semibold' onClick={handleResetField}>
          Làm mới
        </Button>

        <Button type='submit' variant='default' className='font-semibold'>
          Tìm kiếm
        </Button>
      </form>

      <div className='my-4 flex-col flex-wrap gap-1 md:flex md:md:flex-row'>
        {[
          { label: 'Thêm mới', variant: 'default', onClick: handleSelectedTypeBooks, disable: false },
          { label: 'Thêm từ excel', variant: 'default', onClick: handleSelectedExcelTypeBooks, disable: false }
        ].map(({ label, variant, disable, onClick }) => (
          <Button
            variant={variant as ButtonCustomProps['variant']}
            className='w-[100%] md:w-auto'
            key={label}
            onClick={onClick}
            disabled={disable}
          >
            {label}
          </Button>
        ))}
      </div>

      <div className='mt-6'>
        <Table
          loading={isLoading}
          columns={columns}
          dataSource={listTheLoaiSach || []}
          pagination={pagination}
          expandedRowKeys={expland}
          scroll={{ x: 980 }}
          rowKey={(record) => record.Id}
          className='custom-table'
          indentSize={40}
          bordered
        />
      </div>

      <div
        className={classNames('relative', {
          // @ts-ignore
          'mt-[64px]': (queryConfig.page && +queryConfig.pageSize) >= totalListTheLoaiSach
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger
            visible={!!listTheLoaiSach?.length}
            value={queryConfig.pageSize}
            total={totalListTheLoaiSach.toString()}
          />
        </div>
      </div>

      <ModalDelete
        open={isVisiable}
        closable={false}
        title={
          <>
            Bạn có chắn chắn muốn xóa thể loại sách <span className='text-danger-30'>{selectedDelete?.TenTheLoai}</span>{' '}
            này không?
          </>
        }
        // TODO: Remove inline funtion, integrate api
        handleCancel={() => {
          setSelectedDelete(undefined);
          setIsVisiable(false);
        }}
        handleOk={() => {
          handleDeleteTypeBook();
        }}
      />
      <Loading open={loading}></Loading>
    </div>
  );
};

export default TypeBookManagermentContent;
