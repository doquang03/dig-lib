import { DeleteFilled, EditOutlined } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Tooltip } from 'antd';
import Table, { type ColumnsType } from 'antd/es/table';
import { authorApis } from 'apis';
import classNames from 'classnames';
import { Button, Empty, Input, Loading, ModalDelete, SizeChanger, Title, TitleDelete } from 'components';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import { usePaginationNavigate, useQueryConfig } from 'hooks';
import { isEmpty, omitBy, trim } from 'lodash';
import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useForm } from 'react-hook-form';
import { Link, createSearchParams, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getSerialNumber } from 'utils/utils';

// TODO: Delete as soon as api implement
type Mock = {
  Id: string;
  TenTacGia: string;
  MaTacGia: string;
  QuocTich: string;
  MoTa: string;
};

type FormInput = {
  TextForSearch: string;
  GioiTinh: string;
  TrangThai: string;
};

const AuthorManagermentContent = () => {
  const [selectedAuthor, setSelectedAuthor] = useState<Mock>();
  const [totalTacGia, setTotalTacGia] = useState(0);

  const { register, handleSubmit, reset, setValue } = useForm<FormInput>({
    defaultValues: {
      TextForSearch: '',
      GioiTinh: '',
      TrangThai: ''
    }
  });

  const navigate = useNavigate();

  const ref = useRef<HTMLDivElement | null>(null);

  const handleNavigation = usePaginationNavigate();

  const [visibleModal, setVisibleModal] = useState(false);

  const queryConfig = useQueryConfig();

  const { isAllowedAdjustment } = useUser();

  const {
    data: dataAuthor,
    refetch,
    isFetching
  } = useQuery({
    queryKey: ['authors', queryConfig],
    queryFn: () => {
      let page = 1;
      if (typeof queryConfig.page === 'string') page = parseInt(queryConfig.page);
      let queryString = `?page=${page}&sizeNumber=${queryConfig.pageSize}`;
      if (queryConfig.TextForSearch !== undefined) queryString += `&TextForSearch=${queryConfig.TextForSearch}`;
      let request = authorApis.getAuthors(queryString);
      return request;
    }
  });

  const listTacGia = useMemo(() => {
    if (!dataAuthor?.data?.Item) return;
    const { ListTacGia, count } = dataAuthor?.data?.Item;
    setTotalTacGia(count);
    return ListTacGia;
  }, [dataAuthor?.data?.Item]);

  const { mutate: handleDeleteSingle, isLoading: isLoadingDeleteSingle } = useMutation({
    mutationFn: (Id: string) => authorApis.Delete(Id),
    onSuccess: (res) => {
      if (listTacGia?.length === 1) handleResetField();
      else refetch();
      toast.success('Xóa tác giả thành công');
    }
  });

  const columns = useMemo(() => {
    const _columns: ColumnsType<Mock> = [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        render: (value, record, index) => {
          return (
            queryConfig.page && queryConfig.pageSize && getSerialNumber(+queryConfig.page, +queryConfig.pageSize, index)
          );
        },
        width: 100
      },
      {
        title: 'Tên tác giả',
        dataIndex: 'TenTacGia',
        key: 'TenTacGia',
        render: (value, { TenTacGia }) => (
          <Tooltip placement='topLeft' title={TenTacGia} arrow={true} className='truncate'>
            <p>{TenTacGia}</p>
          </Tooltip>
        ),
        onCell: (record) => ({
          className: 'text-left'
        }),
        ellipsis: true
      },
      {
        title: 'Mã tác giả',
        dataIndex: 'MaTacGia',
        key: 'MaTacGia',
        width: 200
      },
      {
        title: 'Quốc tịch',
        dataIndex: 'QuocTich',
        key: 'QuocTich'
      },
      {
        title: 'Mô tả',
        dataIndex: 'MoTa',
        key: 'MoTa',
        render: (value, { MoTa }) =>
          MoTa && MoTa.length > 50 ? (
            <Tooltip placement='topLeft' title={MoTa} arrow={true}>
              <p className='text-center'>{MoTa.substring(0, 50).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{MoTa}</p>
          )
      }
    ];

    if (isAllowedAdjustment) {
      _columns.push({
        title: 'Hành động',
        dataIndex: 'actions',
        key: 'actions',
        width: 150,
        render: (value, record) => {
          return (
            <>
              <Tooltip title='Cập nhật'>
                <Link to={`CapNhat/${record.Id}`} className='mx-2'>
                  <EditOutlined style={{ fontSize: '20px', color: '#08c' }} />
                </Link>
              </Tooltip>

              <Tooltip title='Xóa'>
                <button
                  className='mx-2'
                  onClick={(e) => {
                    e.stopPropagation();
                    setSelectedAuthor(() => {
                      return { ...record };
                    });
                    setVisibleModal(true);
                  }}
                >
                  <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
                </button>
              </Tooltip>
            </>
          );
        }
      });
    }

    return _columns;
  }, [isAllowedAdjustment, queryConfig.page, queryConfig.pageSize]);

  const onPaginate = useCallback(
    (page: number, pageSize: number) => {
      ref.current?.scrollIntoView({ behavior: 'smooth' });
      handleNavigation({ ...queryConfig, page: page + '', pageSize: pageSize + '' });
    },
    [handleNavigation, queryConfig]
  );

  const pagination = useMemo(() => {
    return {
      current: (!!queryConfig.page && +queryConfig.page) || 1,
      pageSize: (!!queryConfig.pageSize && +queryConfig.pageSize) || 30,
      total: totalTacGia,
      onChange: onPaginate,
      hideOnSinglePage: true,
      showSizeChanger: false,
      showQuickJumper: true,
      locale: { jump_to: '', page: '' }
    };
  }, [onPaginate, queryConfig.page, queryConfig.pageSize, totalTacGia]);

  useEffect(() => {
    Object.keys(queryConfig).forEach((key) => {
      if (!key) {
        return;
      }
      // @ts-ignore
      setValue(key, queryConfig[key]);
    });
  }, [queryConfig, setValue]);

  const onSubmit = handleSubmit((data) => {
    handleNavigation({
      ...omitBy({ ...data, TextForSearch: trim(data.TextForSearch, ' ').replace(/\s\s+/g, ' ') }, isEmpty),
      page: '1'
    });
  });

  const handleResetField = () => {
    const { pathname } = window.location;
    navigate({
      pathname: pathname,
      search: createSearchParams({}).toString()
    });
    reset();
  };

  // TODO: Do something with selected Authors
  const handleSelectedAuthors = () => {
    navigate(path.themtacgia);
  };

  const handleAddAuthorByExcel = () => {
    navigate(path.themtacgiaexcel);
  };

  const handleNoteAuthor = () => {
    navigate(path.chuthichtacgia);
  };

  return (
    <div className='p-5' ref={ref}>
      <Title title='DANH SÁCH TÁC GIẢ' />

      <form className='my-5 flex flex-col flex-wrap gap-2 bg-secondary-10 py-3 px-3.5 md:flex-row' onSubmit={onSubmit}>
        <div className='flex w-1/2 flex-col flex-wrap gap-2 md:flex-row'>
          <Input
            placeholder='Nhập mã, tên tác giả ...'
            containerClassName='w-[100%] md:w-[unset] md:grow'
            name='TextForSearch'
            register={register}
          />

          <Button type='button' variant='secondary' className='font-semibold' onClick={handleResetField}>
            Làm mới
          </Button>

          <Button type='submit' variant='default' className='font-semibold'>
            Tìm kiếm
          </Button>
        </div>
      </form>

      <div className='my-4 flex flex-col flex-wrap gap-2 md:flex md:md:flex-row'>
        <Button
          className={classNames('w-[100%] md:w-auto', {
            hidden: !isAllowedAdjustment,
            'w-[100%] md:w-auto': isAllowedAdjustment
          })}
          onClick={handleSelectedAuthors}
        >
          Thêm tác giả
        </Button>

        <Button
          className={classNames('w-[100%] md:w-auto', {
            hidden: !isAllowedAdjustment,
            'w-[100%] md:w-auto': isAllowedAdjustment
          })}
          onClick={handleAddAuthorByExcel}
        >
          Thêm từ Excel
        </Button>

        <Button className={classNames('w-[100%] gap-1 md:w-auto')} onClick={handleNoteAuthor}>
          Bảng kí hiệu tác giả
        </Button>
      </div>

      <div className='mt-6'>
        <Table
          loading={isFetching && !listTacGia?.length}
          columns={columns}
          dataSource={listTacGia || []}
          pagination={pagination}
          scroll={{ x: 980 }}
          rowKey={(record) => record.Id}
          className='custom-table'
          locale={{
            emptyText: () => <Empty />
          }}
          bordered
        />
      </div>

      <div
        className={classNames('relative', {
          // @ts-ignore
          'mt-[64px]': Number(queryConfig.pageSize) >= totalTacGia
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger visible={!!listTacGia?.length} value={listTacGia?.length + ''} total={totalTacGia.toString()} />
        </div>
      </div>

      {/* Deleted Modal for one Author */}
      <ModalDelete
        open={visibleModal}
        closable={false}
        title={
          <TitleDelete
            firstText={'Bạn có chắn chắn muốn xóa tác giả'}
            secondText={selectedAuthor?.TenTacGia}
          ></TitleDelete>
        }
        // TODO: Remove inline funtion, integrate api
        handleCancel={() => {
          setVisibleModal(false);
          setSelectedAuthor(undefined);
        }}
        handleOk={() => {
          setVisibleModal(false);
          handleDeleteSingle(selectedAuthor?.Id as string);
        }}
      />
      <Loading open={isLoadingDeleteSingle}></Loading>
    </div>
  );
};

export default AuthorManagermentContent;
