import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation } from '@tanstack/react-query';
import { Modal, ModalProps } from 'antd';
import userManualApis from 'apis/usermanual.api';
import { UploadIcon } from 'assets';
import { AxiosError } from 'axios';
import { Button, Input } from 'components';
import { useEffect, useRef, useState } from 'react';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import { HuongDanSuDung } from 'utils/rules';
import * as yup from 'yup';

type Props = {
  model: UserManual | undefined;
  onHide: VoidFunction;
  onSuccess: VoidFunction;
} & ModalProps;

type HuongDanSuDungForm = yup.InferType<typeof HuongDanSuDung>;

const ModelUpdateManualPage = (props: Props) => {
  const {
    register,
    handleSubmit,
    reset,
    setValue,
    setError,
    formState: { errors }
  } = useForm<HuongDanSuDungForm>({
    defaultValues: {
      Ten: ''
    },
    resolver: yupResolver(HuongDanSuDung)
  });
  const { model, open, onHide, onSuccess } = props;

  const [file, setFile] = useState<File | null>(null);

  const [isSubmit, setIsSubmit] = useState<boolean>(false);

  const fileInutRef = useRef<HTMLInputElement | null>(null);

  const { mutate, isLoading } = useMutation({
    mutationFn: model ? userManualApis.updateNewUserManual : userManualApis.addNewUserManual,
    onSuccess: () => {
      if (model) {
        toast.success('Cập nhật hướng dẫn sử dụng thành công');
      } else toast.success('Tạo hướng dẫn sử dụng thành công');
      reset();
      setFile(null);
      setIsSubmit(false);
      onHide();
      onSuccess();
    },
    onError: (error: AxiosError<ResponseApi>) => {
      console.log('error', error);
      setError(
        error?.response?.data?.ExMessage as 'Ten',
        { type: 'validate', message: error.response?.data.Message },
        { shouldFocus: true }
      );
    }
  });

  const handleComplete = handleSubmit((data: any) => {
    if (!file && !model) {
      return;
    }

    const formData = new FormData();

    file && formData.append('LinkFile', file);
    formData.append(`Ten`, data.Ten.replace(/\s\s+/g, ' '));
    if (model) formData.append(`Id`, model.Id);

    mutate(formData);
  });

  const handleAccessFileInputRef = () => {
    fileInutRef.current?.click();
  };

  useEffect(() => {
    if (model) setValue('Ten', model.Ten);
    else setValue('Ten', '');
  }, [model]);

  const handleOnFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files;

    console.log('fileFromLocal', fileFromLocal);

    if (!fileFromLocal) {
      return;
    }

    if (fileFromLocal?.[0].size >= 102400 * 1000) {
      return toast.warning('Dung lượng file tối đa là 100Mb');
    }

    setFile(fileFromLocal[0]);
  };

  return (
    <Modal
      open={open}
      title={
        <h3 className='border-b-2 pb-2 font-bold uppercase text-primary-10'>
          {model ? 'Chỉnh sửa hướng dẫn sử dụng' : 'Thêm mới hướng dẫn sử dụng'}
        </h3>
      }
      closable={false}
      footer={null}
    >
      <form onSubmit={handleComplete}>
        <label className='text-base font-bold'>
          Nội dung <span className='text-danger-10'>*</span>{' '}
        </label>
        <Input
          placeholder='Nhập nội dung hướng dẫn'
          containerClassName='mt-4'
          name='Ten'
          register={register}
          errorMessage={errors.Ten?.message}
          maxLength={256}
        />
        <label className='mt-4 text-base font-bold'>
          File hướng dẫn <span className='text-danger-10'>*</span>
        </label>
        <div className='mt-4 flex gap-2'>
          <Button type='button' variant={'default'} onClick={handleAccessFileInputRef} className='shrink-0'>
            <UploadIcon />
            <span className='ml-2 text-sm'>Chọn tệp</span>
          </Button>
          <span className='flex items-center italic text-[#3472A2] underline underline-offset-4'>
            {file && file.name}
          </span>
          {model && !file && (
            <a
              href={model.LinkFile}
              target='_blank'
              rel='noreferrer'
              className='ml-6 flex items-center italic text-[#3472A2] underline underline-offset-4'
            >
              {model.LinkFile &&
                model.LinkFile.slice(model.LinkFile.lastIndexOf('/') + 1, model.LinkFile.lastIndexOf('?'))}
            </a>
          )}
          <input
            type='file'
            accept={'application/pdf'}
            className='hidden'
            ref={fileInutRef}
            onChange={handleOnFileChange}
            multiple={false}
            // @ts-ignore
            onClick={(e: React.MouseEvent<HTMLInputElement, MouseEvent>) => (e.target.value = null)}
          />
          {!model && isSubmit && !file && (
            <p className='flex items-center text-[14px] text-red-500'>File hướng dẫn sử dụng không được để trống</p>
          )}
        </div>

        <div className='mt-3 flex items-center justify-end gap-2'>
          <Button
            type='button'
            variant='secondary'
            onClick={() => {
              reset();
              setFile(null);
              setIsSubmit(false);
              onHide();
            }}
            loading={isLoading}
          >
            Quay về
          </Button>

          <Button loading={isLoading} onClick={() => setIsSubmit(true)}>
            {model ? 'Cập nhật' : 'Thêm mới'}
          </Button>
        </div>
      </form>
    </Modal>
  );
};

export default ModelUpdateManualPage;
