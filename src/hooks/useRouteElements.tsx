/* eslint-disable react-hooks/exhaustive-deps */
import { Modal, Spin } from 'antd';
import { EDUCATION_DEPARTMENT_PATHNAME, IDENTITY_CONFIG } from 'constants/config';
import { path } from 'constants/path';
import { SelectedBooksAuditProvider } from 'contexts/audit';
import { useUser } from 'contexts/user.context';
import {
  AddAuditPage,
  AddAuthorByExcelPage,
  AddAuthorPage,
  AddBookExcelPage,
  AddBookNormalByExcel,
  AddBookSCBByExcel,
  AddBookSCBSGKByExcel,
  AddBooksToAuditPage,
  AddBookshelfPage,
  AddCollectionPage,
  AddColorPage,
  AddDDCByExcelPage,
  AddDDCPage,
  AddDay19Page,
  AddDigitalDocument,
  AddExpense,
  AddLanguagePage,
  AddLibraryMonitorPage,
  AddMagazine,
  AddMagazineExcel,
  AddNewspaper,
  AddNewspaperExcel,
  AddNotification,
  AddPlacePublicationPage,
  AddPublicationPage,
  AddPublishingCompanyPage,
  AddReceipt,
  AddStatusBookPage,
  AddStudentAvatarPage,
  AddStudentByExcelFilePage,
  AddStudentPage,
  AddSubjectPage,
  AddSupplierPage,
  AddTeacherAvatarPage,
  AddTeacherByExcelPage,
  AddTeacherPage,
  AddTopicPage,
  AddTypeBookByExcelPage,
  AddTypeBookPage,
  ApprovedDetails,
  AuditListPage,
  AuditPage,
  AuthorManagementPage,
  BackUpPage,
  BookShelfPage,
  BookStoreManagementPage,
  BookStoreTypePage,
  BookshelfManagementPage,
  CollectionManagementPage,
  ColorManagementContent,
  ColorPage,
  Connection,
  ConnectionManagement,
  CreateExportPage,
  DDCManagementPage,
  DDCPage,
  Day19ManagementPage,
  Day19Page,
  DigitalDocument,
  DocumentFolderPage,
  ErrorSyncDocument,
  ExpenseChart,
  ExpenseList,
  ExpensePage,
  ExportCardPage,
  ExportCardTeacherPage,
  ExportDetailPage,
  ExportListPage,
  ExportManagement,
  ExportReportManagementPage,
  ExportReportPage,
  FolderManagementPage,
  GeneralBookPage,
  GuidePage,
  HistoryPage,
  HomePage,
  HostDocumentPage,
  IntroducePage,
  InventoryBookPage,
  LanguageManagementPage,
  LayoutApp,
  LibraryMonitorManagementPage,
  LiquidationPage,
  ListOfErrorDocument,
  ListOfKeys,
  ListOfPullRequests,
  ListOfRentingBooksMangement,
  ListOfRentingBooksPage,
  ListOfRentingTicket,
  ListOfUnits,
  ListOfUnitsED,
  ListReserveBooksPage,
  Marc21Page,
  MemberManagement,
  NewsPaperMagazineListing,
  NotFoundPage,
  NoteAuthorPage,
  Notification,
  NumberOfBooksPage,
  NumberRegisterationUniquePage,
  PlacePublicationManagementPage,
  PrintCategory,
  PrintExportPage,
  PrintReceipt,
  PublicationsManagementPage,
  PublicationsPage,
  PublishingCompanyManagementPage,
  PullRequestDetail,
  ReaderStatistics,
  ReceiptDetail,
  ReceiptListpage,
  RedirectPage,
  RegisterNewsPaperMagazine,
  RegisterationBookNumberPage,
  RentBackBookPage,
  RentBooksInDayPage,
  RentingTicket,
  RentingTicketDetail,
  ReturnBookPage,
  ReviewDocument,
  RulesPage,
  SearchDay19ByNamePage,
  SettingHostLibrary,
  SettingPage,
  StaticalReservingBooks,
  StatisticExportBooksPage,
  StatisticReadingPage,
  StatisticalManagementPage,
  StatisticalReservingBooksChart,
  StatusBookManagementPage,
  StatusBookPage,
  StudentManagementPage,
  SubjectManagementPage,
  SupplierManagementPage,
  SupplierPage,
  TeacherManagementPage,
  TitleApproval,
  TitleApprovalListing,
  TopicManagementPage,
  TypeBookManagementPage,
  UpdateBookCoverPage,
  UserManualPage,
  VisitingReaders,
  WarehouseManagement
} from 'pages';
import { BookFilesSection, FolderDetails } from 'pages/FolderManagement/components';
import { ViewModeProvider } from 'pages/FolderManagement/contexts/useFolderManagement';
import { useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { useAuth } from 'react-oidc-context';
import { Outlet, useLocation, useNavigate, useRoutes } from 'react-router-dom';
import { getUnitFromSS } from 'utils/utils';

export const AccessDenied = () => (
  <LayoutApp>
    <Helmet>
      <meta charSet='utf-8' />
      <title>Forbidden - Thư viện số</title>
      <link rel='Trang chủ' href={window.location.href} />
    </Helmet>

    <h2 className='p-2 '>Bạn không có quyền truy cập chức năng này</h2>
  </LayoutApp>
);

const ProtectedRouted = () => {
  const auth = useAuth();

  const { userType, isLoading } = useUser();

  const { pathname } = useLocation();

  const unit = getUnitFromSS(Number(userType));

  // tài khoản với vai trò là SỞ && chưa chọn đơn vị (được lưu trong session storage) hoặc vai trò là Trường
  const userHasRequiredRole = (userType === 2001 && Boolean(unit)) || userType === 3000;

  if (auth.isAuthenticated) {
    if (!isLoading) {
      // sở || phòng sẽ được truy cập các màn hình Trang chủ, Lịch sử truy cập
      // Sở || phòng || Thư viện chủ trì sẽ được truy cập màn hình Quản lý kết nối
      // console.log('pathname', '/DanhSachLoiDongBoTaiLieu'.includes(pathname));

      if (
        // @ts-ignore
        EDUCATION_DEPARTMENT_PATHNAME.includes('/' + pathname.split('/').filter((path) => path)[0]) ||
        pathname === path.home
      ) {
        return <Outlet />;
      }
    } else {
      return (
        <>
          <Helmet>
            <meta charSet='utf-8' />
            <title>Đang tải - Thư viện số</title>
            <link rel='Trang chủ' href={window.location.href} />
          </Helmet>

          <Modal open={isLoading} footer={null} centered closable={false}>
            <div className='flex items-center justify-center gap-2'>
              Đang tải dữ liệu <Spin />
            </div>
          </Modal>
        </>
      );
    }

    return !userHasRequiredRole ? <AccessDenied /> : <Outlet />;
  } else {
    return <RedirectPage auth={auth} />;
  }
};

function useRouteElements() {
  const navigate = useNavigate();

  useEffect(() => {
    if (IDENTITY_CONFIG.redirect_uri.endsWith(window.location.pathname)) {
      if (window.name === '' || window.name.endsWith(window.location.pathname)) navigate(path.home);
      else {
        navigate(window.name);
        window.name = '';
      }
    }
  }, [window.location.pathname]);

  const routesElement = useRoutes([
    {
      path: '',
      element: <ProtectedRouted />,
      children: [
        {
          path: path.home,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Trang chủ - Thư viện số</title>
                <link rel='Trang chủ' href={window.location.href} />
              </Helmet>
              <HomePage />
            </LayoutApp>
          )
        },
        {
          path: path.noiquythuvien,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Nội quy thư viện - Thư viện số</title>
                <link rel='Nội quy thư viện' href={window.location.href + path.noiquythuvien} />
              </Helmet>
              <RulesPage />
            </LayoutApp>
          )
        },
        {
          path: path.listOfUnits,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Đơn vị con - Thư viện số</title>
                <link rel='Đơn vị con - Thư viện số' href={window.location.href + path.listOfUnits} />
              </Helmet>
              <ListOfUnitsED />
            </LayoutApp>
          )
        },
        {
          path: path.hocsinh,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title> Quản lý học sinh - Thư viện số</title>
                <link rel='Quản lý học sinh' href={window.location.href + path.hocsinh} />
              </Helmet>
              <StudentManagementPage />
            </LayoutApp>
          ),
          children: [
            {
              path: path.capnhathocsinh,
              element: <AddStudentPage />
            },
            {
              path: path.themhocsinh,
              element: <AddStudentPage />
            },
            {
              path: path.xuatthethuvien,
              element: <ExportCardPage />
            },
            {
              path: path.themhocsinhexcel,
              element: <AddStudentByExcelFilePage />
            },
            {
              path: path.themhinhdaidienhocsinh,
              element: <AddStudentAvatarPage />
            }
          ]
        },
        {
          path: path.giaovien,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title> Quản lý giáo viên - Thư viện số</title>
                <link rel='Quản lý giáo viên' href={window.location.href + path.giaovien} />
              </Helmet>
              <TeacherManagementPage />
            </LayoutApp>
          ),
          children: [
            {
              path: path.themgiaovien,
              element: <AddTeacherPage />
            },
            {
              path: path.capnhatgiaovien,
              element: <AddTeacherPage />
            },
            {
              path: path.xuatthethuviengiaovien,
              element: <ExportCardTeacherPage />
            },
            {
              path: path.themgiaovienexcel,
              element: <AddTeacherByExcelPage />
            },
            {
              path: path.themhinhdaidiengiaovien,
              element: <AddTeacherAvatarPage />
            }
          ]
        },
        {
          path: path.canbothuvien,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title> Quản lý cán bộ thư viện - Thư viện số</title>
                <link rel='Quản lý cán bộ thư viện' href={window.location.href + path.canbothuvien} />
              </Helmet>
              <LibraryMonitorManagementPage />
            </LayoutApp>
          ),
          children: [
            {
              path: path.themcanbothuvien,
              element: <AddLibraryMonitorPage />
            }
          ]
        },
        {
          path: path.bandocltv,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Bạn đọc liên thư viện - Thư viện số</title>
                <link rel='Bạn đọc liên thư viện' href={window.location.href + path.bandocltv} />
              </Helmet>
              <VisitingReaders />
            </LayoutApp>
          )
        },
        {
          path: path.monhoc,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title> Quản lý môn học - Thư viện số</title>
                <link rel='Quản lý môn học' href={window.location.href + path.monhoc} />
              </Helmet>
              <SubjectManagementPage />
            </LayoutApp>
          ),
          children: [
            {
              path: path.themmonhoc,
              element: <AddSubjectPage />
            },
            {
              path: path.capnhatmonhoc,
              element: <AddSubjectPage />
            }
          ]
        },
        {
          path: path.chudiem,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Quản lý chủ điểm - Thư viện số</title>
                <link rel='Quản lý chủ điểm' href={window.location.href + path.chudiem} />
              </Helmet>
              <TopicManagementPage />
            </LayoutApp>
          ),
          children: [
            {
              path: path.themchudiem,
              element: <AddTopicPage />
            },
            {
              path: path.capnhatchudiem,
              element: <AddTopicPage />
            }
          ]
        },
        {
          path: path.bosuutap,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Bộ sưu tập - Thư viện số</title>
                <link rel='Bộ sưu tập' href={window.location.href + path.bosuutap} />
              </Helmet>
              <CollectionManagementPage />
            </LayoutApp>
          ),
          children: [
            {
              path: path.thembosuutap,
              element: <AddCollectionPage />
            },
            {
              path: path.capnhatbosuutap,
              element: <AddCollectionPage />
            }
          ]
        },
        {
          path: path.nhaxuatban,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Nhà xuất bản - Thư viện số</title>
                <link rel='Nhà xuất bản' href={window.location.href + path.nhaxuatban} />
              </Helmet>
              <PublishingCompanyManagementPage />
            </LayoutApp>
          ),
          children: [
            {
              path: path.themnhaxuatban,
              element: <AddPublishingCompanyPage />
            },
            {
              path: path.capnhatnhaxuatban,
              element: <AddPublishingCompanyPage />
            }
          ]
        },
        {
          path: path.noixuatban,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title> Nơi xuất bản - Thư viện số</title>
                <link rel='Nơi xuất bản' href={window.location.href + path.noixuatban} />
              </Helmet>
              <PlacePublicationManagementPage />
            </LayoutApp>
          ),
          children: [
            {
              path: path.themnoixuatban,
              element: <AddPlacePublicationPage />
            },
            {
              path: path.capnhatnoixuatban,
              element: <AddPlacePublicationPage />
            }
          ]
        },
        {
          path: path.ngonngu,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Ngôn ngữ - Thư viện số</title>
                <link rel='Ngôn ngữ' href={window.location.href + path.ngonngu} />
              </Helmet>
              <LanguageManagementPage />
            </LayoutApp>
          ),
          children: [
            {
              path: path.themngonngu,
              element: <AddLanguagePage />
            },
            {
              path: path.capnhatngonngu,
              element: <AddLanguagePage />
            }
          ]
        },
        {
          path: path.theloaisach,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Thể loại sách - Thư viện số</title>
                <link rel='Thể loại sách' href={window.location.href + path.theloaisach} />
              </Helmet>
              <TypeBookManagementPage />
            </LayoutApp>
          ),
          children: [
            {
              path: path.themtheloaisach,
              element: <AddTypeBookPage />
            },
            {
              path: path.capnhattheloaisach,
              element: <AddTypeBookPage />
            },
            {
              path: path.themtheloaisachexcel,
              element: <AddTypeBookByExcelPage />
            }
          ]
        },
        {
          path: path.tacgia,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Tác giả - Thư viện số</title>
                <link rel='Tác giả' href={window.location.href + path.tacgia} />
              </Helmet>
              <AuthorManagementPage />
            </LayoutApp>
          ),
          children: [
            {
              path: path.themtacgia,
              element: <AddAuthorPage />
            },
            {
              path: path.capnhattacgia,
              element: <AddAuthorPage />
            },
            {
              path: path.themtacgiaexcel,
              element: <AddAuthorByExcelPage />
            },
            {
              path: path.chuthichtacgia,
              element: <NoteAuthorPage />
            }
          ]
        },
        {
          path: path.kesach,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Kệ sách - Thư viện số</title>
                <link rel='Kệ sách' href={window.location.href + path.kesach} />
              </Helmet>
              <BookShelfPage />
            </LayoutApp>
          ),
          children: [
            {
              path: path.kesach,
              element: <BookshelfManagementPage />
            },
            {
              path: path.themKeSach,
              element: <AddBookshelfPage />
            },
            {
              path: path.capNhatKeSach,
              element: <AddBookshelfPage />
            }
          ]
        },
        {
          path: path.trangThaiSach,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Tình trạng sách - Thư viện số</title>
                <link rel='Tình trạng sách' href={window.location.href + path.trangThaiSach} />
              </Helmet>
              <StatusBookPage />
            </LayoutApp>
          ),
          children: [
            {
              path: path.trangThaiSach,
              element: <StatusBookManagementPage />
            },
            {
              path: path.themTrangThaiSach,
              element: <AddStatusBookPage />
            },
            {
              path: path.capNhatTrangThaiSach,
              element: <AddStatusBookPage />
            }
          ]
        },
        {
          path: path.nguoncungcap,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Nguồn cung cấp - Thư viện số</title>
                <link rel='Nguồn cung cấp' href={window.location.href + path.nguoncungcap} />
              </Helmet>
              <SupplierPage />
            </LayoutApp>
          ),
          children: [
            {
              path: path.nguoncungcap,
              element: <SupplierManagementPage />
            },
            {
              path: path.themNguonCungCap,
              element: <AddSupplierPage />
            },
            {
              path: path.capNhatNguonCungCap,
              element: <AddSupplierPage />
            }
          ]
        },
        {
          path: path.ddc,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>DDC - Thư viện số</title>
                <link rel='DDC' href={window.location.href + path.ddc} />
              </Helmet>
              <DDCPage />
            </LayoutApp>
          ),
          children: [
            {
              path: path.ddc,
              element: <DDCManagementPage />
            },
            {
              path: path.themDDC,
              element: <AddDDCPage />
            },
            {
              path: path.capNhatDDC,
              element: <AddDDCPage />
            },
            {
              path: path.themDDCBangExcel,
              element: <AddDDCByExcelPage />
            }
          ]
        },
        {
          path: path.day19,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>19 Dãy - Thư viện số</title>
                <link rel='19 Dãy' href={window.location.href + path.day19} />
              </Helmet>
              <Day19Page />
            </LayoutApp>
          ),
          children: [
            {
              path: path.day19,
              element: <Day19ManagementPage />
            },
            {
              path: path.bangTraCuuTen,
              element: <SearchDay19ByNamePage />
            },
            {
              path: path.themDay19,
              element: <AddDay19Page />
            },
            {
              path: path.capNhatDay19,
              element: <AddDay19Page />
            }
          ]
        },
        {
          path: path.mamau,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Mã màu - Thư viện số</title>
                <link rel='Mã màu' href={window.location.href + path.mamau} />
              </Helmet>
              <ColorPage />
            </LayoutApp>
          ),
          children: [
            {
              path: path.mamau,
              element: <ColorManagementContent />
            },
            {
              path: path.themMaMau,
              element: <AddColorPage />
            },
            {
              path: path.capNhatMaMau,
              element: <AddColorPage />
            }
          ]
        },
        {
          path: path.sach,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Sách - Thư viện số</title>
                <link rel='Sách' href={window.location.href + path.sach} />
              </Helmet>
              <PublicationsPage />
            </LayoutApp>
          ),
          children: [
            {
              path: path.sach,
              element: <PublicationsManagementPage />
            },
            {
              path: path.themSach,
              element: <AddPublicationPage />
            },
            {
              path: path.capNhatSach,
              element: <AddPublicationPage />
            },
            {
              path: path.capNhatAnhBia,
              element: <UpdateBookCoverPage />
            },
            {
              path: path.themSachExcel,
              element: <AddBookExcelPage />
            },
            {
              path: path.capNhatSachExcel,
              element: <AddBookExcelPage />
            },
            {
              path: path.thanhLySach,
              element: <LiquidationPage />
            },
            {
              path: path.marc21,
              element: <Marc21Page />
            }
          ]
        },
        {
          path: path.thuMucTaiLieu,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Thư mục tài liệu - Thư viện số</title>
                <link rel='Thư mục tài liệu' href={window.location.href + path.thuMucTaiLieu} />
              </Helmet>
              <DocumentFolderPage />
            </LayoutApp>
          )
        },
        {
          path: path.loaikho,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Loại kho - Thư viện số</title>
                <link rel='Loại kho' href={window.location.href + path.loaikho} />
              </Helmet>
              <BookStoreTypePage />
            </LayoutApp>
          )
        },
        {
          path: path.khosach,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Kho sách - Thư viện số</title>
                <link rel='Kho sách' href={window.location.href + path.khosach} />
              </Helmet>
              <BookStoreManagementPage />
            </LayoutApp>
          ),
          children: [
            {
              path: path.inDanhMuc,
              element: <PrintCategory />
            },
            {
              path: path.importExcel_Thuong,
              element: <AddBookNormalByExcel />
            },
            {
              path: path.importExcel_SCB,
              element: <AddBookSCBByExcel />
            },
            {
              path: path.importExcel_SCB_SGK,
              element: <AddBookSCBSGKByExcel />
            }
          ]
        },
        {
          path: path.quanlynhapkho,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Quản lý nhập kho - Thư viện số</title>
                <link rel='Quản lý nhập kho' href={window.location.href + path.quanlynhapkho} />
              </Helmet>
              <WarehouseManagement />
            </LayoutApp>
          ),
          children: [
            {
              path: path.quanlynhapkho,
              element: <ReceiptListpage />
            },
            {
              path: path.taoPhieuNhapKho,
              element: <AddReceipt />
            },
            {
              path: path.chiTietPhieuNhap,
              element: <ReceiptDetail />
            },
            {
              path: path.inPhieuNhap,
              element: <PrintReceipt />
            },
            {
              path: path.taoSoNhapKho,
              element: <InventoryBookPage />
            }
          ]
        },
        {
          path: path.quanlyxuatkho,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Quản lý xuất kho - Thư viện số</title>
                <link rel='Quản lý xuất kho' href={window.location.href + path.quanlyxuatkho} />
              </Helmet>
              <ExportManagement />
            </LayoutApp>
          ),
          children: [
            {
              path: path.quanlyxuatkho,
              element: <ExportListPage />
            },
            {
              path: path.taoPhieuXuatKho,
              element: <CreateExportPage />
            },
            {
              path: path.chiTietPhieuXuat,
              element: <ExportDetailPage />
            },
            {
              path: path.inPhieuXuat,
              element: <PrintExportPage />
            }
          ]
        },
        {
          path: path.newspaperMagazineListing,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Báo, tạp chí - Thư viện số</title>
                <link rel='Kho sách' href={window.location.href + path.newspaperMagazineListing} />
              </Helmet>
              <BookStoreManagementPage />
            </LayoutApp>
          ),
          children: [
            {
              path: path.newspaperMagazineListing,
              element: <NewsPaperMagazineListing />
            },
            {
              path: path.addNewspaper,
              element: <AddNewspaper />
            },
            {
              path: path.addNewspaperExcel,
              element: <AddNewspaperExcel />
            },
            {
              path: path.updateNewspaper,
              element: <AddNewspaper />
            },
            {
              path: path.addMagazine,
              element: <AddMagazine />
            },
            {
              path: path.updateMagazine,
              element: <AddMagazine />
            },
            {
              path: path.addMagazineExcel,
              element: <AddMagazineExcel />
            }
          ]
        },
        {
          path: path.registerNewspaperMagazine,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Đăng ký báo, tạp chí - Thư viện số</title>
                <link rel='Kho sách' href={window.location.href + path.registerNewspaperMagazine} />
              </Helmet>
              <RegisterNewsPaperMagazine />
            </LayoutApp>
          )
        },
        {
          path: path.muonTraSach,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Mượn trả sách - Thư viện số</title>
                <link rel='Mượn trả sách' href={window.location.href + path.muonTraSach} />
              </Helmet>
              <RentBackBookPage />
            </LayoutApp>
          )
        },
        {
          path: path.muonTrongNgay,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title> Đọc sách tại chỗ - Thư viện số</title>
                <link rel='Đọc sách tại chỗ' href={window.location.href + path.muonTrongNgay} />
              </Helmet>
              <RentBooksInDayPage />
            </LayoutApp>
          )
        },
        {
          path: path.danhSachPhieuMuon,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Danh sách phiếu mượn - Thư viện số</title>
                <link rel='Danh sách phiếu mượn' href={window.location.href + path.danhSachPhieuMuon} />
              </Helmet>
              <RentingTicket />
            </LayoutApp>
          ),
          children: [
            { path: path.danhSachPhieuMuon, element: <ListOfRentingTicket /> },
            {
              path: path.chiTietPhieuMuon,
              element: (
                <>
                  <Helmet>
                    <meta charSet='utf-8' />
                    <title>Chi tiết phiếu mượn - Thư viện số</title>
                    <link rel='Chi tiết phiếu mượn' href={window.location.href + path.danhSachPhieuMuon} />
                  </Helmet>
                  <RentingTicketDetail />
                </>
              )
            }
          ]
        },

        {
          path: path.danhSachDatMuon,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Danh sách đặt mượn - Thư viện số</title>
                <link rel='Danh sách đặt mượn' href={window.location.href + path.danhSachDatMuon} />
              </Helmet>
              <ListReserveBooksPage />
            </LayoutApp>
          )
        },
        {
          path: path.thongke,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Biểu đồ thống kê - Thư viện số</title>
                <link rel='Biểu đồ thống kê' href={window.location.href + path.thongke} />
              </Helmet>
              <StatisticalManagementPage />
            </LayoutApp>
          )
        },
        {
          path: path.thongkemuonsach,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Thống kê sách mượn - Thư viện số</title>
                <link rel='Thống kê sách mượn' href={window.location.href + path.thongkemuonsach} />
              </Helmet>
              <ListOfRentingBooksMangement />
            </LayoutApp>
          ),
          children: [
            {
              path: path.thongkemuonsach,
              element: <ListOfRentingBooksPage />
            },
            {
              path: path.traSachDanhMucSach,
              element: <ReturnBookPage />
            }
          ]
        },
        {
          path: path.thongkeluotdoc,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Thống kê sách theo lượt đọc, mượn</title>
                <link
                  rel='Thống kê sách theo lượt đọc, mượn - Thư viện số'
                  href={window.location.href + path.thongkeluotdoc}
                />
              </Helmet>
              <StatisticReadingPage />
            </LayoutApp>
          )
        },
        {
          path: path.thongkedanhsachdatmuon,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Thống kê danh sách đặt mượn - Thư viện số</title>
                <link
                  rel='Thống kê danh sách đặt mượn - Thư viện số'
                  href={window.location.href + path.thongkedanhsachdatmuon}
                />
              </Helmet>
              <StaticalReservingBooks />
            </LayoutApp>
          )
        },
        {
          path: path.thongkedanhsachdatmuonbieudo,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>[Biểu đồ] Thống kê danh sách đặt mượn - Thư viện số</title>
                <link
                  rel='[Biểu đồ] Thống kê danh sách đặt mượn - Thư viện số'
                  href={window.location.href + path.thongkedanhsachdatmuonbieudo}
                />
              </Helmet>

              <StatisticalReservingBooksChart />
            </LayoutApp>
          )
        },
        {
          path: path.thongkesoluonganpham,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Thống kê số lượng ấn phẩm - Thư viện số</title>
                <link
                  rel='Thống kê số lượng ấn phẩm - Thư viện số'
                  href={window.location.href + path.thongkesoluonganpham}
                />
              </Helmet>
              <NumberOfBooksPage />
            </LayoutApp>
          )
        },
        {
          path: path.thongkebandoc,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Thống kê bạn đọc - Thư viện số</title>
                <link rel='Thống kê sách mượn - Thư viện số' href={window.location.href + path.thongkebandoc} />
              </Helmet>
              <ReaderStatistics />
            </LayoutApp>
          )
        },
        {
          path: path.thongkexuatsach,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Thống kê thanh lý - Thư viện số</title>
                <link rel='Thống kê thanh lý - Thư viện số' href={window.location.href + path.thongkebandoc} />
              </Helmet>
              <StatisticExportBooksPage />
            </LayoutApp>
          )
        },
        {
          path: path.thongkexuatbaocao,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Xuất báo cáo - Thư viện số</title>
                <link rel='Xuất báo cáo - Thư viện số' href={window.location.href + path.thongkexuatbaocao} />
              </Helmet>
              <ExportReportPage />
            </LayoutApp>
          ),
          children: [
            {
              path: path.thongkexuatbaocao,
              element: <ExportReportManagementPage />
            },
            {
              path: path.soTongQuat,
              element: <GeneralBookPage />
            },
            {
              path: path.soDangKyCaBiet,
              element: <NumberRegisterationUniquePage />
            },
            {
              path: path.soDangKySachGiaoKhoa,
              element: <RegisterationBookNumberPage />
            }
          ]
        },
        {
          path: path.thongkekiemketaisan,
          element: (
            <SelectedBooksAuditProvider>
              <LayoutApp>
                <Helmet>
                  <meta charSet='utf-8' />
                  <title>Kiểm kê tài sản - Thư viện số</title>
                  <link rel='Kiểm kê tài sản' href={window.location.href + path.thongkekiemketaisan} />
                </Helmet>
                <AuditPage />
              </LayoutApp>
            </SelectedBooksAuditProvider>
          ),
          children: [
            {
              path: path.thongkekiemketaisan,
              element: <AuditListPage />
            },
            {
              path: path.addAudit,
              element: <AddAuditPage />
            },
            {
              path: path.addBooksToAudit,
              element: <AddBooksToAuditPage />
            },
            {
              path: path.updateAudit,
              element: <AddAuditPage />
            }
          ]
        },
        {
          path: path.quanlythuvien,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Quản lý thư viện - Thư viện số</title>
                <link rel='Quản lý thư viện' href={window.location.href + path.quanlythuvien} />
              </Helmet>
              <SettingPage />
            </LayoutApp>
          )
        },
        {
          path: path.hoTro,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Hỗ trợ - Thư viện số</title>
                <link rel='Quản lý thư viện' href={window.location.href + path.hoTro} />
              </Helmet>
              <GuidePage />
            </LayoutApp>
          )
        },
        {
          path: path.gioiThieu,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Giới thiệu thư viện</title>
                <link rel='Quản lý thư viện' href={window.location.href + path.gioiThieu} />
              </Helmet>
              <IntroducePage />
            </LayoutApp>
          )
        },
        {
          path: path.thongBao,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Thông báo</title>
                <link rel='Quản lý thư viện' href={window.location.href + path.thongBao} />
              </Helmet>
              <Notification />
            </LayoutApp>
          )
        },
        {
          path: path.addNotification,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Thông báo</title>
                <link rel='Quản lý thư viện' href={window.location.href + path.addNotification} />
              </Helmet>
              <AddNotification />
            </LayoutApp>
          )
        },
        {
          path: path.updateNotification,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Thông báo</title>
                <link rel='Quản lý thư viện' href={window.location.href + path.addNotification} />
              </Helmet>
              <AddNotification />
            </LayoutApp>
          )
        },
        {
          path: path.huongDanSuDung,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Hướng dẫn sử dụng</title>
                <link rel='Quản lý thư viện' href={window.location.href + path.huongDanSuDung} />
              </Helmet>
              <UserManualPage />
            </LayoutApp>
          )
        },
        {
          path: path.lichsutruycap,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Lịch sử hoạt động - Thư viện số</title>
                <link rel='Quản lý thư viện' href={window.location.href + path.lichsutruycap} />
              </Helmet>
              <HistoryPage />
            </LayoutApp>
          )
        },
        {
          path: path.quanlythuviencsdl,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Quản lý cơ sở dữ liệu - Thư viện số</title>
                <link rel='Quản lý cơ sở dữ liệu' href={window.location.href + path.quanlythuviencsdl} />
              </Helmet>
              <BackUpPage />
            </LayoutApp>
          )
        },
        {
          path: path.sotheodoikinhphi,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Kinh phí - Thư viện số</title>
                <link rel='Quản lý cơ sở dữ liệu' href={window.location.href + path.sotheodoikinhphi} />
              </Helmet>
              <ExpensePage />
            </LayoutApp>
          ),
          children: [
            {
              path: path.sotheodoikinhphi,
              element: <ExpenseList />
            },
            {
              path: path.addExpense,
              element: <AddExpense />
            },
            {
              path: path.updateExpense,
              element: <AddExpense />
            },
            {
              path: path.expenseChart,
              element: <ExpenseChart />
            }
          ]
        },
        {
          path: path.connectionManagement,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Quản lý kết nối - Thư viện số</title>
                <link rel='Quản lý cơ sở dữ liệu' href={window.location.href + path.connectionManagement} />
              </Helmet>
              <ConnectionManagement />
            </LayoutApp>
          ),
          children: [
            {
              path: path.connectionManagement,
              element: <ListOfUnits />
            },
            {
              path: path.listOfKeys,
              element: <ListOfKeys />
            }
          ]
        },
        {
          path: path.hostLibraryManagement,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Cài đặt đơn vị chủ trì - Thư viện số</title>
                <link rel='Kết nối Mục Lục Liên Hợp' href={window.location.href + path.hostLibraryManagement} />
              </Helmet>
              <SettingHostLibrary />
            </LayoutApp>
          )
        },
        {
          path: path.digitalDocument1,
          element: (
            <LayoutApp>
              <Outlet />
            </LayoutApp>
          ),
          children: [
            {
              path: path.digitalDocument1,
              element: <DigitalDocument />
            },
            {
              path: path.digitalDocument2,
              element: <AddDigitalDocument />
            },
            {
              path: path.digitalDocument3,
              element: <AddDigitalDocument />
            }
          ]
        },

        {
          path: path.titleApproval,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Phê duyệt bản ghi - Thư viện số</title>
                <link rel='Kết nối Mục Lục Liên Hợp' href={window.location.href + path.titleApproval} />
              </Helmet>
              <TitleApproval />
            </LayoutApp>
          ),
          children: [
            {
              path: path.titleApproval,
              element: <TitleApprovalListing />
            },
            {
              path: path.approvedDetails,
              element: <ApprovedDetails />
            },
            {
              path: path.reviewDocument,
              element: <ReviewDocument />
            }
          ]
        },
        {
          path: path.connection,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Kết nối Mục Lục Liên Hợp - Thư viện số</title>
                <link rel='Kết nối Mục Lục Liên Hợp' href={window.location.href + path.connection} />
              </Helmet>
              <Connection />
            </LayoutApp>
          )
        },

        {
          path: path.pullRequest,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Đồng bộ dữ liệu MLLH - Thư viện số</title>
                <link rel='Kết nối Mục Lục Liên Hợp' href={window.location.href + path.pullRequest} />
              </Helmet>
              <Outlet />
            </LayoutApp>
          ),
          children: [
            {
              path: path.pullRequest,
              element: <ListOfPullRequests />
            },
            {
              path: path.pullRequestDetail,
              element: <PullRequestDetail />
            }
          ]
        },

        // {
        //   path: path.media,
        //   element: (
        //     <LayoutApp>
        //       <Helmet>
        //         <meta charSet='utf-8' />
        //         <title>Tài nguyên Audio | Video - Thư viện số</title>
        //         <link rel='Tài nguyên Audio' href={window.location.href + path.media} />
        //       </Helmet>
        //       <Media />
        //     </LayoutApp>
        //   ),
        //   children: [
        //     {
        //       path: path.media,
        //       element: <ListOfAudioDocument />
        //     },
        //     {
        //       path: path.addDigitalMedia,
        //       element: <AddDigitalMedia />
        //     },
        //     {
        //       path: path.updateDigitalMedia,
        //       element: <AddDigitalMedia />
        //     },
        //     {
        //       path: path.addDigitalMediaByExcel,
        //       element: <AddDigitalDocumentByExcelPage />
        //     }
        //   ]
        // },
        {
          path: path.memberManagement,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Quản lý thành viên - Thư viện số</title>
                <link rel='Quản lý cơ sở dữ liệu' href={window.location.href + path.memberManagement} />
              </Helmet>

              <MemberManagement />
            </LayoutApp>
          )
        },
        {
          path: path.errorSyncDocument,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Danh sách lỗi đồng bộ tài liệu - Thư viện số</title>
                <link rel='Quản lý cơ sở dữ liệu' href={window.location.href + path.memberManagement} />
              </Helmet>

              <Outlet />
            </LayoutApp>
          ),
          children: [
            {
              path: path.errorSyncDocument,
              element: <ErrorSyncDocument />
            },
            {
              path: path.listOfErrorDocument,
              element: <ListOfErrorDocument />
            }
          ]
        },
        {
          path: path.hostDocument,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Tài liệu mục lục liên hợp - Thư viện số</title>
                <link rel='Quản lý cơ sở dữ liệu' href={window.location.href + path.hostDocument} />
              </Helmet>

              <HostDocumentPage />
            </LayoutApp>
          )
        },
        {
          path: path.folderManagement,
          element: (
            <LayoutApp>
              <Helmet>
                <meta charSet='utf-8' />
                <title>Quản lý thư mục tài liệu - Thư viện số</title>
                <link rel='Quản lý thư mục tài liệu' href={window.location.href + path.folderManagement} />
              </Helmet>
              <ViewModeProvider>
                <Outlet />
              </ViewModeProvider>
            </LayoutApp>
          ),
          children: [
            {
              path: path.folderManagement,
              element: <FolderManagementPage />,
              children: [
                {
                  path: path.folderDetails,
                  element: <FolderDetails />,
                  children: [
                    {
                      path: path.bookInFolder,
                      element: <BookFilesSection />
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    },
    {
      path: path.notFound,
      element: (
        <LayoutApp>
          <Helmet>
            <meta charSet='utf-8' />
            <title>Không tìm thấy trang - Thư viện số</title>
            <link rel='Not found page' href={window.location.href} />
          </Helmet>
          <NotFoundPage />
        </LayoutApp>
      )
    }
  ]);

  return routesElement;
}

export default useRouteElements;
