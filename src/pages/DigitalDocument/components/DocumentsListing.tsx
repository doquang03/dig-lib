import { useMutation, useQuery } from '@tanstack/react-query';
import { DatePicker, Table, Tag, Tooltip, TreeSelect } from 'antd';
import locale from 'antd/es/date-picker/locale/vi_VN';
import { ColumnsType, TableProps } from 'antd/es/table';
import { SorterResult } from 'antd/es/table/interface';
import { digitalDocumentApis } from 'apis';
import classNames from 'classnames';
import { BasedModal, Button, Input, ModalDelete, Select, SizeChanger, TitleDelete } from 'components';
import { FORMAT_DATE_PICKER } from 'constants/config';
import dayjs, { Dayjs } from 'dayjs';
import { useStateAsync } from 'hooks';
import FailurePublishingModelModal from 'pages/Publications/AddPublication/components/FailurePublishingModelModal';
import { useMemo, useState, type ChangeEvent, useEffect, useCallback } from 'react';
import { useCookies } from 'react-cookie';
import { Link, useBeforeUnload, useLocation, useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getSerialNumber } from 'utils/utils';
import { DeleteMultiRecordModal, DisplaySettingModal } from '../components';
import UpdateModal from 'pages/Publications/PublicationsManagement/components/UpdateModal';
import { useForm } from 'react-hook-form';
import { BaseOptionType, DefaultOptionType } from 'antd/es/select';

const PAGE_SIZE = 30;

const digitalDocuemt = {
  SachDienTu: 'document',
  AlbumAnh: 'image',
  Audio: 'audio',
  Video: 'video'
};

const initialUpdateValues = {
  folder: ''
};

function DocumentsListing() {
  const [params, setParams] = useStateAsync<{
    title?: string;
    author?: string;
    publisher?: string;
    publishDate?: string;
    language?: string;
    isPubliced?: string;
    IdDocument?: string;
    page: number;
    pageSize: number;
    sortPublishedDate?: SorterResult<DigitalDocument>['order'];
  }>({ page: 1, pageSize: PAGE_SIZE });

  const {
    register: regiterUpdate,
    control,
    handleSubmit: handlesubmitUpdate,
    reset: resetUpdateValues,
    setError: setErrorUpdateValues,
    formState: { errors: errorsUpdateValues }
  } = useForm<typeof initialUpdateValues>({
    defaultValues: initialUpdateValues
  });

  const [selectRecords, setSelectRecords] = useState<DigitalDocument[]>([]);
  const [selectedRecord, setSelectedRecord] = useState<DigitalDocument>();
  const [visibleModal, setVisibleModal] = useState<
    'single' | 'multi' | 'setting' | 'publish' | 'folder' | 'failurePublishing'
  >();
  const [arrAvaliable, setArrAvaliable] = useState<Array<boolean>>([]);
  const [failurePublishingListing, setFailurePublishingListing] = useState<Array<FailureMessage>>([]);

  const [cookies, setCookie] = useCookies(['displaySetting']);

  const { documentType } = useParams<{ documentType: 'SachDienTu' | 'AlbumAnh' | 'Audio' | 'Video' }>() || {};

  const { data: optionsForSearchingData } = useQuery({
    queryKey: ['getOptionsForSearching', documentType],
    // @ts-ignore
    queryFn: () => digitalDocumentApis.getOptionsForSearching(digitalDocuemt?.[documentType]),
    staleTime: 10 * 60
  });

  useMemo(() => {
    if (arrAvaliable?.length === 0) {
      if (cookies.displaySetting) {
        setArrAvaliable(cookies.displaySetting);
      } else {
        setArrAvaliable([true, true, true, true, true, true, true, true, true]);
      }
    }
  }, [arrAvaliable?.length, cookies.displaySetting]);

  const { Options_NXB, Options_NgonNgu, Options_TacGia, Options_ThuMucSach } = optionsForSearchingData?.data.Item || {
    Options_NgonNgu: [],
    Options_NXB: [],
    Options_TacGia: [],
    Options_ThuMucSach: []
  };

  const {
    data: digitalDocumentData,
    isLoading: loadingDigitalDocument,
    isRefetching,
    refetch
  } = useQuery({
    queryKey: ['getDigitalDocument', params.page, params.pageSize, params.sortPublishedDate],
    queryFn: async () => {
      const reqBody: Partial<{
        TextForSearch: string;
        IdAuthor: string;
        IdPublishingCompany: string;
        PublicationDate: string;
        IdDocument: string;
        IdLanguage: string;
        IsPublish: boolean;
        SortPublicationDate: boolean;
      }> = {};

      // if (location.key !== 'default') {
      //   delete params.title;
      //   delete params.author;
      //   delete params.publishDate;
      //   delete params.publisher;
      //   delete params.IdDocument;
      //   delete params.isPubliced;
      //   delete params.sortPublishedDate;
      // }

      if (params.title) reqBody.TextForSearch = params.title;
      if (params.author) reqBody.IdAuthor = params.author;
      if (params.language) reqBody.IdLanguage = params.language;
      if (params.publishDate) reqBody.PublicationDate = params.publishDate;
      if (params.publisher) reqBody.IdPublishingCompany = params.publisher;
      if (params.IdDocument) reqBody.IdDocument = params.IdDocument;
      if (params.isPubliced) {
        if (params.isPubliced === '1') reqBody.IsPublish = true;
        if (params.isPubliced === '2') reqBody.IsPublish = false;
      }

      if (params.sortPublishedDate) {
        if (params.sortPublishedDate === 'ascend') reqBody.SortPublicationDate = true;
        if (params.sortPublishedDate === 'descend') reqBody.SortPublicationDate = false;
      }

      return digitalDocumentApis.getDigitalDocuments({
        page: params.page || 1,
        pageSize: params.pageSize || 30,
        // @ts-ignore
        Type: digitalDocuemt?.[documentType],
        ...reqBody
      });
    }
  });

  const { mutate: publishToHost, isLoading: loadingPublishToHost } = useMutation({
    mutationFn: digitalDocumentApis.publishToHost,
    onSuccess: (data) => {
      const { ListFail, ListSuccess } = data.data.Item || { ListFail: [], ListSuccess: [] };

      !!ListSuccess.length && toast.success(`Đã gửi thành công ${ListSuccess.length} sách`);
      refetch();
      if (ListFail.length > 0) {
        setFailurePublishingListing(ListFail);
        setVisibleModal('failurePublishing');
      } else {
        setSelectRecords([]);
        setFailurePublishingListing([]);
        setVisibleModal(undefined);
      }
    }
  });

  const { Count, ListModel } = digitalDocumentData?.data.Item || { Count: 0, ListModel: [] };

  const { mutate: deleteRecord, isLoading: loadingDelete } = useMutation({
    mutationFn: digitalDocumentApis.deleteSingle,
    onSuccess: () => {
      refetch();

      setVisibleModal(undefined);
    }
  });

  const { mutate: updateBooksDocument, isLoading: isLoadingUpdateBookDocument } = useMutation({
    mutationFn: digitalDocumentApis.updateBooksDocumentFolder,
    onSuccess: () => {
      refetch();
      toast.success('Cập nhật thư mục cho tài liệu số thành công');
      resetUpdateValues();
      setSelectRecords([]);
      setVisibleModal(undefined);
    }
  });

  const columns: ColumnsType<DigitalDocument> = useMemo(() => {
    const cols: ColumnsType<DigitalDocument> = [];

    arrAvaliable?.[0] &&
      cols.push({
        key: 'STT',
        title: 'STT',
        render: (value, record, index) => getSerialNumber(params.page, params.pageSize, index),
        width: 80
      });

    arrAvaliable?.[1] &&
      cols.push({
        key: 'title',
        title: 'Nhan đề',
        dataIndex: 'TenSach',
        onCell: (record) => ({
          className: 'text-primary-30 hover:underline text-left'
        }),
        render: (value, { TenSach, Id }) =>
          TenSach?.length > 60 ? (
            <Tooltip placement='topLeft' title={TenSach} arrow={true}>
              <Link to={`/${documentType}/${Id}`}>
                <p className='text-left'>{TenSach.substring(0, 60).concat('...')}</p>
              </Link>
            </Tooltip>
          ) : (
            <Link to={`/${documentType}/${Id}`}>
              <p className='text-left'>{TenSach}</p>
            </Link>
          ),
        width: 300
      });

    arrAvaliable?.[2] &&
      cols.push({
        key: 'IdTacGia',
        title: 'Tác giả',
        dataIndex: 'IdTacGia',
        render: (value, record) => {
          const authorHaha = Options_TacGia.filter((author) =>
            record.IdTacGia.find((authorId) => authorId === author.value)
          );

          return (
            <div className='flex items-center justify-start'>
              {!!authorHaha.length
                ? authorHaha.length > 3
                  ? authorHaha.slice(0, 3).map((item, index) => (
                      <span key={item.value} className='text-left'>
                        {item.label}

                        {authorHaha.slice(0, 3).length - 1 === index ? ' ...' : ', '}
                      </span>
                    ))
                  : authorHaha.map((item, index) => (
                      <span key={item.value} className='text-left'>
                        {item.label}

                        {authorHaha.length - 1 === index ? '' : ', '}
                      </span>
                    ))
                : '--'}
            </div>
          );
        },
        width: 200
      });

    arrAvaliable?.[3] &&
      cols.push({
        key: 'IdNhaXuatBan',
        title: 'Nhà xuất bản',
        dataIndex: 'IdNhaXuatBan',
        render: (value, record) => (
          <p className='text-left'>
            {Options_NXB.find((author) => author.value === record.IdNhaXuatBan)?.label || '--'}
          </p>
        )
      });

    arrAvaliable?.[4] &&
      cols.push({
        key: 'NgayXuatBan',
        title: 'Ngày ban hành',
        dataIndex: 'NgayXuatBan',
        render: (value, record) =>
          record?.NgayXuatBan === '0001-01-01T00:00:00Z' ? '--' : dayjs(record.NgayXuatBan).format('DD/MM/YYYY'),
        sorter: true
      });

    arrAvaliable?.[5] &&
      cols.push({
        key: 'IdNgonNgu',
        title: 'Ngôn ngữ',
        dataIndex: 'IdNgonNgu',
        render: (value, record) => Options_NgonNgu.find((author) => author.value === record.IdNgonNgu)?.label || '--'
      });

    arrAvaliable?.[6] &&
      cols.push({
        key: 'Gửi vào MLLH',
        title: 'Gửi vào MLLH',
        dataIndex: 'IsPublic',
        render: (value, record) =>
          record.IsPublish ? <Tag color='green'>Đã gửi vào MLLH</Tag> : <Tag color='red'>Chưa gửi vào MLLH</Tag>,
        width: 200
      });

    arrAvaliable?.[7] &&
      cols.push({
        key: 'Phân quyền xem',
        title: 'Phân quyền xem',
        dataIndex: 'PermissionView',
        render: (value, record) =>
          record.PermissionView === 1 ? (
            <Tag color='yellow'>Thư viện nội bộ</Tag>
          ) : record.PermissionView === 2 ? (
            <Tag color='green'>Thư viện liên thông</Tag>
          ) : (
            <Tag color='red'>Không cho xem</Tag>
          ),
        width: 200
      });

    arrAvaliable?.[8] &&
      cols.push({
        key: 'Phân quyền tải về',
        title: 'Phân quyền tải về',
        dataIndex: 'PermissionDownload',
        render: (value, record) =>
          record.PermissionDownload === 1 ? (
            <Tag color='yellow'>Thư viện nội bộ</Tag>
          ) : record.PermissionDownload === 2 ? (
            <Tag color='green'>Thư viện liên thông</Tag>
          ) : (
            <Tag color='red'>Không cho xem</Tag>
          ),
        width: 200
      });

    return [
      ...cols,
      {
        key: 'actions',
        title: 'Hành động',
        render: (value, record) => {
          return (
            <div className='space-x-4'>
              <button onClick={() => setSelectedRecord(record)}>
                <Link to={`/${documentType}/${record.Id}`}>
                  <svg width='16' height='16' viewBox='0 0 16 16' fill='none' xmlns='http://www.w3.org/2000/svg'>
                    <path
                      d='M12.5997 1.41421C12.8634 1.149 13.221 1 13.5939 1C13.9669 1 14.3245 1.149 14.5882 1.41421C14.8519 1.67943 15 2.03914 15 2.41421C15 2.78929 14.8519 3.149 14.5882 3.41422L8.29125 9.74756L5.63991 10.4142L6.30274 7.74755L12.5997 1.41421Z'
                      stroke='#3472A2'
                      strokeWidth='2'
                      strokeLinecap='round'
                      strokeLinejoin='round'
                    />
                    <path
                      d='M6.96552 3H2.32567C1.97408 3 1.63689 3.14048 1.38828 3.39052C1.13967 3.64057 1 3.97971 1 4.33333V13.6667C1 14.0203 1.13967 14.3594 1.38828 14.6095C1.63689 14.8595 1.97408 15 2.32567 15H11.6054C11.957 15 12.2942 14.8595 12.5428 14.6095C12.7914 14.3594 12.931 14.0203 12.931 13.6667V9.00001'
                      stroke='#3472A2'
                      strokeWidth='2'
                      strokeLinecap='round'
                      strokeLinejoin='round'
                    />
                  </svg>
                </Link>
              </button>

              <button
                onClick={() => {
                  setSelectedRecord(record);
                  setVisibleModal('single');
                }}
              >
                <svg width='13' height='17' viewBox='0 0 13 17' fill='none' xmlns='http://www.w3.org/2000/svg'>
                  <path
                    d='M0.928571 15.1111C0.928571 16.15 1.76429 17 2.78571 17H10.2143C11.2357 17 12.0714 16.15 12.0714 15.1111V3.77778H0.928571V15.1111ZM3.21286 8.38667L4.52214 7.055L6.5 9.05722L8.46857 7.055L9.77786 8.38667L7.80929 10.3889L9.77786 12.3911L8.46857 13.7228L6.5 11.7206L4.53143 13.7228L3.22214 12.3911L5.19071 10.3889L3.21286 8.38667ZM9.75 0.944444L8.82143 0H4.17857L3.25 0.944444H0V2.83333H13V0.944444H9.75Z'
                    fill='#A23434'
                  />
                </svg>
              </button>
            </div>
          );
        }
      }
    ];
  }, [Options_NXB, Options_NgonNgu, Options_TacGia, arrAvaliable, documentType, params.page, params.pageSize]);

  useEffect(() => {
    async function hello() {
      await setParams({ page: 1, pageSize: 30 });
      await refetch();
    }

    hello();
  }, [documentType]);

  const handleChange = (e: ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
    if (!e) return;

    const { value, name } = e.target;

    setParams((prevState) => {
      return { ...prevState, [name]: value };
    });
  };

  const handleChangePublishDate = (value: Dayjs | null) => {
    setParams((prevState) => {
      return { ...prevState, publishDate: value ? dayjs(value).startOf('day').toISOString() : undefined };
    });
  };

  const handleSortPublishDate: TableProps<DigitalDocument>['onChange'] = (pagination, filters, sorter, extra) => {
    setParams((prevState) => {
      // @ts-ignore
      return { ...prevState, sortPublishedDate: sorter?.order };
    });
  };

  const handleSearch = async () => {
    await setParams((prevState) => {
      return { ...prevState, page: 1, pageSize: 30 };
    });

    await refetch();
  };

  const onUpdate = handlesubmitUpdate((data) => {
    if (!selectRecords.length) return;

    const ids = selectRecords.map((item) => item.Id + '');

    if (!data.folder && visibleModal) {
      setErrorUpdateValues('folder', { message: 'Tài liệu thư mục không được để trống' });
      return;
    }

    return updateBooksDocument({ documentId: data.folder, ids });
  });

  return (
    <div>
      <div className='mt-3 flex flex-col items-center gap-2 md:flex-row'>
        <Link to={`/${documentType}/ThemMoi`}>
          <Button>Thêm mới</Button>
        </Link>

        {/* <Link to={`/${documentType}/ThemMoiTaiLieuBangExcel`}>
          <Button>Thêm từ Excel</Button>
        </Link>

        <Button>Xuất Excel</Button>

        <Button>Phân quyền đồng loạt</Button> */}

        <Button
          variant={!!selectRecords.length ? 'danger' : 'disabled'}
          disabled={!selectRecords.length}
          onClick={() => setVisibleModal('multi')}
        >
          Xóa tài liệu
        </Button>

        <Button
          onClick={() => setVisibleModal('publish')}
          disabled={!selectRecords.length}
          variant={!selectRecords.length ? 'disabled' : 'default'}
        >
          Gửi vào MLLH
        </Button>

        <Button
          onClick={() => setVisibleModal('folder')}
          disabled={!selectRecords.length}
          variant={!selectRecords.length ? 'disabled' : 'default'}
        >
          Đổi thư mục
        </Button>

        <Button onClick={() => setVisibleModal('setting')}>
          <svg width='20' height='19' viewBox='0 0 20 19' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path
              d='M19.5031 6.94857C19.3789 6.34109 18.6335 5.9766 18.0124 6.0981C17.1429 6.34109 16.5217 6.0981 16.3975 5.85511C16.2733 5.61211 16.3975 5.00463 17.0186 4.27565C17.5155 3.78967 17.5155 3.06069 17.0186 2.5747C15.7764 1.35974 14.2857 0.509265 12.5466 0.023279C11.9255 -0.0982175 11.1801 0.26627 11.0559 0.873753C10.8075 1.72423 10.3106 2.08872 9.93789 2.08872C9.56522 2.08872 9.19255 1.60273 8.81988 0.873753C8.57143 0.26627 7.95031 -0.0982175 7.32919 0.023279C5.59006 0.509265 4.09938 1.35974 2.85714 2.5747C2.36025 3.06069 2.36025 3.78967 2.85714 4.27565C3.47826 4.88314 3.60248 5.49062 3.47826 5.85511C3.35404 6.0981 2.73292 6.34109 1.86335 6.0981C1.24224 5.9766 0.496893 6.34109 0.37267 6.94857C0.124223 7.79905 0 8.64953 0 9.5C0 10.3505 0.124223 11.201 0.37267 12.0514C0.496893 12.6589 1.24224 13.0234 1.86335 12.9019C2.73292 12.6589 3.35404 12.9019 3.47826 13.1449C3.60248 13.3879 3.47826 13.9954 2.85714 14.7243C2.36025 15.2103 2.36025 15.9393 2.85714 16.4253C4.09938 17.6403 5.59006 18.4907 7.32919 18.9767C7.95031 19.0982 8.69565 18.7337 8.81988 18.1262C9.06832 17.2758 9.56522 16.9113 9.93789 16.9113C10.3106 16.9113 10.6832 17.3973 11.0559 18.1262C11.1801 18.6122 11.677 18.9767 12.2981 18.9767C12.4224 18.9767 12.5466 18.9767 12.6708 18.9767C14.4099 18.4907 15.9006 17.6403 17.1429 16.4253C17.6398 15.9393 17.6398 15.2103 17.1429 14.7243C16.5217 14.1169 16.3975 13.5094 16.5217 13.1449C16.646 12.9019 17.2671 12.6589 18.1366 12.9019C18.7578 13.0234 19.5031 12.6589 19.6273 12.0514C19.8758 11.201 20 10.3505 20 9.5C20 8.64953 19.7516 7.79905 19.5031 6.94857ZM9.93789 13.1449C7.82609 13.1449 6.21118 11.5654 6.21118 9.5C6.21118 7.43456 7.82609 5.85511 9.93789 5.85511C12.0497 5.85511 13.6646 7.43456 13.6646 9.5C13.6646 11.5654 12.0497 13.1449 9.93789 13.1449Z'
              fill='white'
            />
          </svg>
          Cấu hình hiển thị
        </Button>
      </div>

      <div className='my-3 grid grid-cols-12 gap-1 bg-primary-50/20 p-2'>
        <Input
          containerClassName='w-full col-span-3'
          placeholder='Nhập nhan đề sách'
          name='title'
          onChange={handleChange}
          value={params.title || ''}
        />

        <Select
          className='col-span-3 w-full'
          items={[{ label: 'Chọn tác giả', value: '' }, ...Options_TacGia]}
          name='author'
          onChange={handleChange}
          value={params.author || ''}
        />

        <Select
          className='col-span-3 w-full'
          items={[{ label: 'Chọn nhà xuất bản', value: '' }, ...Options_NXB]}
          name='publisher'
          onChange={handleChange}
          value={params.publisher || ''}
        />

        <DatePicker
          placeholder='Chọn ngày ban hành'
          className={'col-span-3 w-full border border-gray-300 py-[0.8rem] px-2 outline-none'}
          locale={locale}
          format={FORMAT_DATE_PICKER}
          onChange={handleChangePublishDate}
          value={!!params?.publishDate ? dayjs(params?.publishDate) : null}
        />

        <Select
          className='col-span-3 mt-1 w-full'
          items={[{ label: 'Chọn ngôn ngữ', value: '' }, ...Options_NgonNgu]}
          value={params.language || ''}
          onChange={handleChange}
          name='language'
        />

        <Select
          className='col-span-3 mt-1 w-full'
          items={[
            { label: 'Tất cả', value: '' },
            {
              label: 'Đã gửi vào MLLH',
              value: '1'
            },
            { label: 'Chưa gửi vào MLLH', value: '2' }
          ]}
          value={params.isPubliced || ''}
          onChange={handleChange}
          name='isPubliced'
        />

        {/* ???? */}
        <TreeSelect
          className={`select-antd-tags  col-span-3 mt-1 w-full`}
          style={{ width: '100%' }}
          treeDefaultExpandAll
          value={params.IdDocument || ''}
          onChange={(value) => {
            setParams((prevState) => {
              prevState.IdDocument = value;
              return { ...prevState };
            });
          }}
          treeData={[
            { label: 'Chọn thư mục', value: '' },
            ...(Options_ThuMucSach as (BaseOptionType | DefaultOptionType)[])
          ]}
        ></TreeSelect>

        <div className='col-span-3 flex items-center gap-1'>
          <Button
            variant='secondary'
            onClick={async () => {
              await setParams({ page: 1, pageSize: PAGE_SIZE });
              await refetch();
              setSelectRecords([]);
            }}
          >
            Làm mới
          </Button>

          <Button onClick={handleSearch}>Tìm kiếm</Button>
        </div>
      </div>

      <div className='mt-3'>
        <Table
          className='custom-table'
          bordered
          columns={columns}
          dataSource={ListModel}
          pagination={{
            current: params.page,
            pageSize: params.pageSize,
            total: Count,
            onChange: (page, pageSize) => {
              setParams((prevState) => {
                return { ...prevState, page, pageSize };
              });
            },
            hideOnSinglePage: true,
            showSizeChanger: false,
            showQuickJumper: true,
            locale: { jump_to: '', page: '' }
          }}
          locale={{
            triggerDesc: 'Click để sắp xếp giảm dần',
            triggerAsc: 'Click để sắp xếp tăng dần',
            cancelSort: 'Click để hủy sắp xếp'
          }}
          loading={loadingDigitalDocument || isRefetching}
          rowKey={(row) => row.Id}
          onChange={handleSortPublishDate}
          rowSelection={{
            selectedRowKeys: selectRecords.map((item) => item?.Id as string),
            preserveSelectedRowKeys: true,
            onChange(selectedRowKeys, selectedRows, info) {
              setSelectRecords(selectedRows);
            }
          }}
        />
      </div>

      <div
        className={classNames('relative', {
          'mt-[86px]': params.pageSize >= Count
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger
            visible={!!Count}
            value={params.pageSize + ''}
            currentPage={params.page.toString()}
            total={Count + ''}
            onChange={(pageSize) => {
              setParams((prevState) => {
                return {
                  ...prevState,
                  pageSize: +pageSize,
                  page: 1
                };
              });
            }}
          />
        </div>
      </div>

      {/* delete single */}
      <ModalDelete
        labelOK='Xác nhận'
        open={visibleModal === 'single' && !!selectedRecord}
        closable={false}
        loading={loadingDelete || isRefetching}
        handleCancel={() => {
          setVisibleModal(undefined);
        }}
        handleOk={() => {
          !!selectedRecord && deleteRecord(selectedRecord.Id);
        }}
        title={<TitleDelete firstText='Bạn chắc chắn muốn xóa' secondText={selectedRecord?.TenSach} />}
      />

      {/* delete multi */}
      <DeleteMultiRecordModal
        records={selectRecords}
        visible={visibleModal === 'multi'}
        onCancel={() => setVisibleModal(undefined)}
        onSuccess={() => {
          refetch();
          setSelectRecords([]);
          setVisibleModal(undefined);
        }}
      />

      {/* display setting */}
      <DisplaySettingModal
        arrAvaliable={arrAvaliable}
        open={visibleModal === 'setting'}
        setVisibleModal={setVisibleModal}
        setArrAvaliable={setArrAvaliable}
        setCookie={setCookie}
      />

      {/* update folder */}
      <UpdateModal
        loading={isLoadingUpdateBookDocument || isRefetching}
        register={regiterUpdate}
        control={control}
        viewModel='treeview'
        name='folder'
        label='Thư mục tài liệu'
        title='Cập nhật thư mục tài liệu'
        open={visibleModal === 'folder'}
        items={[{ label: 'Chọn thư mục', value: '' }, ...Options_ThuMucSach]}
        onBack={() => {
          resetUpdateValues();
          setVisibleModal(undefined);
        }}
        onUpdate={onUpdate}
        error={errorsUpdateValues.folder?.message}
      />

      {/* publish to host */}
      <BasedModal
        open={visibleModal === 'publish'}
        title={
          <p className='mb-2 w-[500px] whitespace-pre-wrap text-[20px] text-primary-20'>
            Bạn có chắc chắn muốn gửi <span className='font-bold'>{selectRecords.length} quyển sách này</span> vào MLLH
            không?
          </p>
        }
        onCancel={() => setVisibleModal(undefined)}
        onSubmit={() => selectRecords.length && publishToHost(selectRecords.map(({ Id }) => Id + ''))}
        loading={loadingPublishToHost || isRefetching}
        labelSubmit='Gửi'
      />
      <FailurePublishingModelModal
        open={visibleModal === 'failurePublishing'}
        books={selectRecords.filter((book) => failurePublishingListing.find((_) => _.Id === book.Id)) || []}
        failures={failurePublishingListing}
        onCancel={() => {
          setSelectRecords([]);
          setFailurePublishingListing([]);
          setVisibleModal(undefined);
        }}
      />
    </div>
  );
}

export default DocumentsListing;
