import { InfoCircleOutlined } from '@ant-design/icons';
import { Modal, type ModalProps } from 'antd';
import { Button } from 'components/Button';
import { type ButtonCustomProps } from 'components/Button/Button';
import 'css/ComponentAntD.css';
import { ReactNode } from 'react';

type Props = {
  labelCanel?: string;
  labelSubmit?: string;
  loading?: boolean;
  submitVariantButton?: ButtonCustomProps['variant'];
  onCancel: VoidFunction;
  onSubmit: VoidFunction;
  children?: ReactNode;
} & ModalProps;

const BasedModal = (props: Props) => {
  const {
    title,
    labelCanel = 'Quay về',
    labelSubmit = 'Xóa',
    loading = false,
    submitVariantButton = 'default',
    onCancel,
    onSubmit,
    children,
    ...rest
  } = props;

  return (
    <Modal centered closable={false} {...rest} className='model-delete' footer={null} width={700}>
      <InfoCircleOutlined className='warning-icon' />

      {title}

      {children}

      <div className='flex items-center justify-center gap-4'>
        <Button variant='secondary' loading={loading} onClick={onCancel}>
          {labelCanel}
        </Button>

        <Button loading={loading} variant={submitVariantButton} onClick={onSubmit}>
          {labelSubmit}
        </Button>
      </div>
    </Modal>
  );
};

export default BasedModal;
