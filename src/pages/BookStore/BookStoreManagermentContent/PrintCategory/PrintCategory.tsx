import { useMutation, useQuery } from '@tanstack/react-query';
import { Checkbox } from 'antd';
import { bookApis, subjectApis, topicApis } from 'apis';
import { Button, Loading, Title } from 'components';
import { useMemo, useState } from 'react';
import 'css/PrintCategory.css';
import { Link, useNavigate } from 'react-router-dom';
import { path } from 'constants/path';
import { download } from 'utils/utils';

const PrintCategory = () => {
  const [listGrade, setListGrade] = useState<Array<string>>([]);
  const [listBoolMonHoc, setListBoolMonHoc] = useState<Array<Subject>>([]);
  const [listBoolKhoiLop, setListBoolKhoiLop] = useState<Array<string>>([]);
  const [listBoolChuDiem, setListBoolChuDiem] = useState<Array<Topic>>([]);
  const [stateLoading, setStateLoading] = useState(false);
  const navigate = useNavigate();
  const { data: dataSubject, isLoading: isLoadingSubject } = useQuery({
    queryKey: ['subject'],
    queryFn: () => subjectApis.GetAll()
  });
  const { data: dataTopic, isLoading: isLoadingTopic } = useQuery({
    queryKey: ['topic'],
    queryFn: () => topicApis.GetAll()
  });

  const { mutateAsync: handlePrintDMTopic, isLoading: isLoadingPrintDMTopic } = useMutation({
    mutationFn: (topic: Topic) => bookApis.XuatDanhMucChuDiem(topic.Id),
    onSuccess: (res, topic) => {
      download('Indanhmuc-' + topic.Ten + '.doc', res.data);
    }
  });

  const { mutateAsync: handlePrintDMSubject, isLoading: isLoadingPrintDMSubject } = useMutation({
    mutationFn: (subject: Subject) => bookApis.XuatDanhMucMonHoc(subject.Id),
    onSuccess: (res, topic) => {
      download('Indanhmuc-' + topic.Ten + '.doc', res.data);
    }
  });

  const { mutateAsync: handlePrintDMGrade, isLoading: isLoadingPrintDMGrade } = useMutation({
    mutationFn: (grade: string) => bookApis.XuatDanhMucKhoiLop(grade),
    onSuccess: (res, grade) => {
      download('Indanhmuc-Khối ' + grade + '.doc', res.data);
    }
  });

  const listSubject = useMemo(() => {
    dataSubject?.data.Item.ListMonHoc.forEach((element) => {
      if (element.KhoiLop !== null) {
        element.KhoiLop.forEach((kl) => {
          if (listGrade.indexOf(kl) === -1) listGrade.push(kl);
        });
      }
    });
    listGrade.sort();
    setListGrade(listGrade);
    return dataSubject?.data.Item.ListMonHoc;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dataSubject?.data.Item.ListMonHoc]);

  const listTopic = useMemo(() => {
    return dataTopic?.data.Item.ListChuDiem;
  }, [dataTopic?.data.Item.ListChuDiem]);

  const handleBack = () => {
    navigate(path.khosach);
  };
  const handleComplete = () => {
    setStateLoading(true);
    let array: Array<Promise<any>> = [];
    listBoolMonHoc.forEach((element) => {
      array.push(handlePrintDMSubject(element));
    });
    listBoolKhoiLop.forEach((element) => {
      array.push(handlePrintDMGrade(element));
    });
    listBoolChuDiem.forEach((element) => {
      array.push(handlePrintDMTopic(element));
    });
    Promise.all(array).then(() => {
      setStateLoading(false);
    });
  };

  return (
    <div className='setting-page print-category-page flex h-full flex-col p-5'>
      <Title
        title='Tải danh mục sách'
        className='w-full shrink-0 border-b-2 border-primary-10 text-[32px] font-bold uppercase text-primary-10'
      />
      <div className='mt-5 grid h-full w-full grow grid-cols-1 md:grid-cols-12' style={{ marginBottom: '20px' }}>
        <div className='col-span-6 mr-8 flex h-full flex-col'>
          <div className='thongtin-group h-full grow'>
            <h5 className='title-box'>
              Theo môn học
              <div className='background-fake col-sm-3'></div>
            </h5>
            <div className='h-full w-full overflow-auto'>
              {listSubject?.map((element) => {
                return (
                  <div className='checkbox-modal-print' key={element.Id}>
                    <Checkbox
                      onChange={(e) => {
                        if (e.target.checked) listBoolMonHoc.push(element);
                        else {
                          const index = listBoolMonHoc.indexOf(element);
                          if (index !== -1) listBoolMonHoc.splice(index, 1);
                        }
                        setListBoolMonHoc(listBoolMonHoc);
                      }}
                    >
                      {element.Ten}
                    </Checkbox>
                    <Link
                      className='float-right mr-5 italic'
                      style={{ textUnderlineOffset: '2px' }}
                      onClick={() => {
                        handlePrintDMSubject(element);
                      }}
                      to={''}
                    >
                      Xem trước
                    </Link>
                  </div>
                );
              })}
            </div>
          </div>
          <div className='thongtin-group shrink-0' style={{ marginBottom: 0 }}>
            <h5 className='title-box he'>
              Theo khối lớp
              <div className='background-fake col-sm-3'></div>
            </h5>
            <div className='h-full w-full overflow-auto'>
              {listGrade?.map((element) => {
                return (
                  <div className='checkbox-modal-print' key={element}>
                    <Checkbox
                      onChange={(e) => {
                        if (e.target.checked) listBoolKhoiLop.push(element);
                        else {
                          const index = listBoolKhoiLop.indexOf(element);
                          if (index !== -1) listBoolKhoiLop.splice(index, 1);
                        }
                        setListBoolKhoiLop(listBoolKhoiLop);
                      }}
                    >
                      {'Khối ' + element}
                    </Checkbox>
                    <Link
                      className='float-right mr-5 italic'
                      style={{ textUnderlineOffset: '2px' }}
                      onClick={() => {
                        handlePrintDMGrade(element);
                      }}
                      to={''}
                    >
                      Xem trước
                    </Link>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
        <div className='thongtin-group col-span-6 ml-8 h-full'>
          <h5 className='title-box'>
            Theo chủ điểm
            <div className='background-fake col-sm-3'></div>
          </h5>
          <div className='h-full w-full overflow-auto'>
            {listTopic?.map((element) => {
              return (
                <div className='checkbox-modal-print' key={element.Id}>
                  <Checkbox
                    onChange={(e) => {
                      if (e.target.checked) listBoolChuDiem.push(element);
                      else {
                        const index = listBoolChuDiem.indexOf(element);
                        if (index !== -1) listBoolChuDiem.splice(index, 1);
                      }
                      setListBoolChuDiem(listBoolChuDiem);
                    }}
                  >
                    {element.Ten}
                  </Checkbox>
                  <Link
                    className='float-right mr-5 italic'
                    style={{ textUnderlineOffset: '2px' }}
                    onClick={() => {
                      handlePrintDMTopic(element);
                    }}
                    to={''}
                  >
                    Xem trước
                  </Link>
                </div>
              );
            })}
          </div>
        </div>
      </div>
      <div className='flex items-center justify-end gap-3'>
        <Button onClick={handleBack} variant='secondary'>
          Quay về
        </Button>

        <Button onClick={handleComplete}>Tải file</Button>
      </div>
      <Loading
        open={
          isLoadingSubject ||
          isLoadingTopic ||
          isLoadingPrintDMTopic ||
          isLoadingPrintDMSubject ||
          isLoadingPrintDMGrade ||
          stateLoading
        }
      ></Loading>
    </div>
  );
};

export default PrintCategory;
