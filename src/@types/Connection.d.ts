type Key = {
  IdKey: string;
  NameMember: string;
  IdMember: string;
  Value: string;
  ConnectionTime: string;
  ExpiredTime: string;
  TotalRecords: string;
  Status: string;
};

type UnitKey = {
  EffectiveTime: string;
  CreateDateTime: string;
  IsActive: boolean;
} & Pick<Key, 'IdKey' | 'NameMember' | 'IdMember' | 'Value'>;

type UnitInHost = {
  WorkingId: string;
  MemberName: string;
  ConnectionTime: string;
  TotalRecord: number;
  Passive: boolean;
  Proactive: boolean;
};

type UnitPermission = Pick<UnitInHost, 'WorkingId' | 'MemberName' | 'Passive' | 'Proactive'>;
