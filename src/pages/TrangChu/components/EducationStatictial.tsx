import { DatePicker } from 'antd';
import dayjs from 'dayjs';
import { useState } from 'react';
import { Bar } from 'react-chartjs-2';

const { RangePicker } = DatePicker;
type RangeValue = Parameters<NonNullable<React.ComponentProps<typeof DatePicker.RangePicker>['onChange']>>[0];

const EducationStatictial = () => {
  const [searchParams, setSearchParams] = useState<{
    TuNgay: string;
    DenNgay: string;
  }>({
    TuNgay: dayjs().startOf('year').startOf('day').toISOString(),
    DenNgay: dayjs().endOf('day').toISOString()
  });

  const handleChangeRangePicker = (values: RangeValue) => {
    if (!values) return;

    setSearchParams({
      TuNgay: values?.[0]?.startOf('day').toISOString() as string,
      DenNgay: values?.[1]?.endOf('day').toISOString() as string
    });
  };
  return (
    <div>
      <div className='flex h-[100%] flex-row gap-2 pt-8'>
        <div
          className='flex h-full grow flex-col py-6 pr-6'
          style={{
            border: '1px solid #3472A2',
            boxShadow: '0px 10px 15px rgba(0, 140, 249, 0.37)',
            borderRadius: '30px'
          }}
        >
          <div className='flex w-full shrink-0 flex-row text-xl text-[#3472A2]'>
            <span className='flex grow items-center pl-2 font-bold'>Biểu đồ thể hiện số lượng sách giấy theo kho</span>
            <RangePicker
              className='w-1/4 border border-gray-300 py-[0.8rem] px-2 outline-none'
              format={['DD/MM/YYYY', 'DD-MM-YYYY']}
              defaultValue={[dayjs().startOf('year'), dayjs()]}
              placeholder={['Ngày bắt đầu', 'Ngày kết thúc']}
              disabledDate={(current) => {
                return current && current > dayjs().endOf('day');
              }}
              onChange={handleChangeRangePicker}
            />
          </div>
          <div style={{ width: '100%', height: '100%' }} className='grow'>
            {/* <Bar
              options={{
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                  datalabels: {
                    display: false
                  },
                  legend: {
                    display: false
                  },
                  title: {
                    display: false
                  }
                }
              }}
              // data={[]}
            /> */}
          </div>
        </div>
      </div>
    </div>
  );
};

export default EducationStatictial;
