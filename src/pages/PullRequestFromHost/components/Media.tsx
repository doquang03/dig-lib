import { useMutation, useQuery } from '@tanstack/react-query';
import { Pagination } from 'antd';
import { bookApis, pullRequestApis } from 'apis';
import { Button, Loading, SizeChanger } from 'components';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import MediaCard from './MediaCard';
import SearchForm from './SearchForm';

const initialState = {
  page: 1,
  pageSize: 24
};

type Params = { page: number; pageSize: number } & Partial<{
  sortBy: string;
  libraryId: string;
  searchForms: {
    operator: '&' | '|' | '!';
    searchField: string;
    value: string;
  }[];
}>;

const Media = () => {
  const [selectedBooks, setSelectedBooks] = useState<string[]>([]);
  const [params, setParams] = useState<Params>(initialState);
  const {
    data: bookInPullRequestData,
    isLoading: loadingBookInPullRequest,
    isFetching,
    isRefetching,
    refetch
  } = useQuery({
    queryKey: ['getMediaBookInPullRequestData', params.page, params.pageSize, params.sortBy, params.libraryId],
    queryFn: () => {
      const req: Partial<{
        IdDonVi?: string;
        IsDocument?: boolean;
        ListQuery: { Operator: string; SearchField: string; Value: string }[];
        SortBookName: boolean;
        SortPublishDate: boolean;
        SortTime: boolean;
      }> = {};

      if (params.libraryId) {
        req.IdDonVi = params.libraryId;
      }

      if (params.sortBy) {
        switch (params.sortBy) {
          case 'sortTitleIncrease':
            req.SortBookName = true;
            break;

          case 'sortTitleDecrease':
            req.SortBookName = false;
            break;

          case 'sortPublishYearIncrease':
            req.SortPublishDate = true;
            break;

          case 'sortPublishYearDecrease':
            req.SortPublishDate = true;
            break;

          case 'sortCreateDateTimeIncrease':
            req.SortTime = true;
            break;

          case 'sortCreateDateTimeDecrease':
            req.SortTime = true;
            break;

          default:
            break;
        }
      }

      if (params.searchForms?.length) {
        req.ListQuery = params.searchForms.map((x) => {
          return {
            Operator: x.operator,
            SearchField: x.searchField,
            Value: x.value
          };
        });
      }

      return pullRequestApis.getBookInPullRequest({
        page: params.page,
        pageSize: params.pageSize,
        IsDocument: false,
        ...req
      });
    }
  });

  const { mutate: mergeSelectedBooks, isLoading: loadingMergeSelectedBooks } = useMutation({
    mutationFn: bookApis.mergePullRequest,
    onSuccess: () => {
      toast.success(`Tải thành công ${selectedBooks.length} sách`);

      setSelectedBooks([]);
    }
  });

  const { ListModel, count } = bookInPullRequestData?.data.Item || { count: 0, ListModel: [] };

  const handleSelectBook = (id: string) => {
    if (!!selectedBooks.includes(id)) {
      setSelectedBooks(selectedBooks.filter((x) => x !== id));
    } else {
      setSelectedBooks((prevState) => [...prevState, id]);
    }
  };

  const handleSearch = (value: {
    sortBy?: string;
    libraryId: string;
    searchForms: {
      operator: '&' | '|' | '!';
      searchField: string;
      value: string;
    }[];
  }) => {
    if (isRefetching) return;

    setParams((prevState) => {
      return { ...prevState, ...value };
    });

    refetch();
  };

  useEffect(() => {
    const handleEvent = (event: KeyboardEvent) => {
      if (event.key === 'Enter') {
        event.preventDefault();
        refetch();
      }
    };

    document.addEventListener('keydown', handleEvent);

    return () => document.removeEventListener('keydown', handleEvent);
  }, []);

  const handleMergeRequest = () => {
    mergeSelectedBooks(selectedBooks);
  };

  const handleChangeUnit = (value: string) => {
    setParams((prevState) => {
      return { ...prevState, libraryId: value };
    });
  };

  return (
    <div className='relative'>
      <Button
        className='absolute -top-12 right-0'
        disabled={!selectedBooks.length}
        variant={!selectedBooks.length ? 'disabled' : 'default'}
        onClick={handleMergeRequest}
      >
        Đồng bộ về thư viện
      </Button>

      <div className='my-3'>
        <SearchForm onSearch={handleSearch} loading={isRefetching} onChangeUnit={handleChangeUnit} />

        <div className='mt-4 grid grid-cols-12 gap-4'>
          {isFetching && !ListModel.length ? (
            <p className='col-start-6'>Đang tải dữ liệu</p>
          ) : (
            <>
              {ListModel.length ? (
                ListModel.map((book, index) => (
                  <div className='col-span-3' key={book.Id}>
                    <MediaCard
                      id={book.Id + ''}
                      name={book.TenSach + ''}
                      image={book.AnhBia}
                      isSelected={selectedBooks.includes(book.Id + '')}
                      onSelect={handleSelectBook}
                      linkFile={book.LinkMedia}
                      LienKet={book.LienKet}
                    />
                  </div>
                ))
              ) : (
                <p className='col-start-6'>Không có dữ liệu</p>
              )}
            </>
          )}
        </div>

        {(!loadingBookInPullRequest || !isFetching) && !!ListModel?.length && (
          <div className='my-3 flex items-center justify-between'>
            <SizeChanger
              currentPage={params.page + ''}
              total={count + ''}
              onChange={(pageSize) =>
                setParams((prevState) => {
                  return { ...prevState, pageSize: +pageSize, page: 1 };
                })
              }
              value={params.pageSize + ''}
              visible={true}
            />

            <Pagination
              current={params.page}
              total={count}
              onChange={(page, pageSize) => {
                setParams((prevState) => {
                  return { ...prevState, pageSize };
                });
              }}
              pageSize={params.pageSize}
              showSizeChanger={false}
              showQuickJumper={true}
              locale={{ jump_to: '', page: '' }}
            />
          </div>
        )}
      </div>
      <Loading open={loadingMergeSelectedBooks} />
    </div>
  );
};

export default Media;
