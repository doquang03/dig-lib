import { path } from 'constants/path';
import { Outlet, useMatch } from 'react-router-dom';
import BookStoreManagermentContent from './BookStoreManagermentContent/BookStoreManagermentContent';

const BookStoreManagementPage = () => {
  const match = useMatch(path.khosach);

  return <>{Boolean(match) ? <BookStoreManagermentContent /> : <Outlet />}</>;
};

export default BookStoreManagementPage;
