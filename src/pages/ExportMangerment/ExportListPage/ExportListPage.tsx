import { CloseCircleOutlined, DownloadOutlined, EyeFilled, PrinterFilled } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Table, Tooltip } from 'antd';
import { ColumnsType, TableProps } from 'antd/es/table';
import { liquidationApis } from 'apis';
import { type GetExportVars } from 'apis/liquidation.apis';
import { SettingIcon } from 'assets';
import classNames from 'classnames';
import { Button, Empty, Loading, ModalDelete, SizeChanger, Title } from 'components';
import { path } from 'constants/path';
import dayjs from 'dayjs';
import { useColumnFilterTableProps, usePaginationNavigate, useQueryConfig } from 'hooks';
import { useCallback, useMemo, useState } from 'react';
import { useCookies } from 'react-cookie';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getSerialNumber } from 'utils/utils';
import SettingModalImport from '../components/SettingModalImport';
import { useUser } from 'contexts/user.context';

const ExportListPage = () => {
  const navigate = useNavigate();

  const [visibleModal, setVisibleModal] = useState<boolean>(false);
  const [selectedId, setSelectedId] = useState<ExportItem>();
  const [arrAvaliable, setArrAvaliable] = useState<Array<boolean>>([]);
  const [visibleModalImport, setVisibleModalImport] = useState<boolean>(false);
  const [searchParams, setSearchParams] = useState<
    Partial<{
      MaXuatKho: [string];
      Count: [string];
      UserName: [string];
      CreateDateTime: [string, string];
      NgayVaoSo: [string, string];
      GiaBia: [string, string];
    }>
  >();

  const [cookies, setCookie] = useCookies(['settings_receipts']);
  const [col, setCol] = useState<keyof ExportItem>();

  const queryConfig = useQueryConfig();

  const { isAllowedAdjustment } = useUser();

  const handleNavigation = usePaginationNavigate();

  const { getColumnSearchProps, getColumnFilterDateProps, getColumnRangeProps } = useColumnFilterTableProps<ExportItem>(
    (key) => setCol(key)
  );

  const onPaginate = useCallback(
    (page: number, pageSize: number) => {
      handleNavigation({ ...queryConfig, page: page + '', pageSize: pageSize + '' });
    },
    [handleNavigation, queryConfig]
  );

  const {
    data: exportData,
    isLoading: isLoadingExportData,
    refetch,
    isRefetching
  } = useQuery({
    queryKey: ['export', queryConfig.page, queryConfig.pageSize, searchParams],
    queryFn: () => {
      let bodyReq: GetExportVars = {};

      if (!!searchParams?.MaXuatKho?.[0]) {
        bodyReq.MaPhieuXuat = {
          truongSearch: searchParams?.MaXuatKho?.[0],
          operatorSearch: 'contains'
        };
      }

      if (!!searchParams?.Count?.[0]) {
        bodyReq.SoLuong = {
          truongSearch: searchParams?.Count?.[0],
          operatorSearch: 'equals'
        };
      }

      if (!!searchParams?.GiaBia?.length) {
        bodyReq.GiaTriXuatKho = {
          Min: searchParams?.GiaBia?.[0],
          Max: searchParams?.GiaBia?.[1]
        };
      }

      if (!!searchParams?.CreateDateTime?.length) {
        bodyReq.ThoiGianTao = {
          Min: dayjs(searchParams.CreateDateTime[0]).startOf('day').toISOString(),
          Max: dayjs(searchParams.CreateDateTime[1]).endOf('day').toISOString()
        };
      }

      if (!!searchParams?.UserName?.[0]) {
        bodyReq.NguoiTao = {
          truongSearch: searchParams?.UserName?.[0],
          operatorSearch: 'contains'
        };
      }

      return liquidationApis.LayPhieuXuatSach({
        page: queryConfig.page || '1',
        pageSize: queryConfig?.pageSize || '30',
        ...bodyReq
      });
    },

    onSuccess: () => {
      setVisibleModal(false);
    }
  });

  const { List_SoXuatKho = [], TotalCount = 0 } = useMemo(() => {
    if (!exportData?.data.Item) {
      return {} as {
        List_SoXuatKho: ExportItem[];
        TotalCount: number;
      };
    }

    return exportData?.data.Item;
  }, [exportData?.data.Item]);

  const pagination = useMemo(() => {
    return {
      current: (!!queryConfig.page && +queryConfig.page) || 1,
      pageSize: (!!queryConfig.pageSize && +queryConfig.pageSize) || 30,
      total: TotalCount,
      onChange: onPaginate,
      hideOnSinglePage: true,
      showSizeChanger: false,
      showQuickJumper: true,
      locale: { jump_to: '', page: '' }
    };
  }, [TotalCount, onPaginate, queryConfig.page, queryConfig.pageSize]);

  const { mutate: downloadExport, isLoading: isLoadingDownloadExport } = useMutation({
    mutationFn: liquidationApis.DownLoadFile,
    onSuccess: (data) => {
      const blob = new Blob([data.data, { type: 'application/xls' }]);
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = 'DanhSachXuatKho.xls';
      link.click();
      toast.success('Tải phiếu xuất thành công');
    }
  });

  const { mutate: deleteExport, isLoading: isLoadingDeleteExport } = useMutation({
    mutationFn: liquidationApis.deleteExport,
    onSuccess: () => {
      refetch();
      toast.success('Xóa phiếu xuất thành công');
    }
  });

  useMemo(() => {
    if (arrAvaliable?.length === 0) {
      if (cookies.settings_receipts) {
        setArrAvaliable(cookies.settings_receipts);
      } else {
        setArrAvaliable([true, true, true, true]);
      }
    }
  }, [arrAvaliable?.length, cookies.settings_receipts]);

  const handleShowColumns = () => {
    setVisibleModalImport(true);
  };

  const handleSelecteReceipt = (record: ExportItem) => () => {
    setVisibleModal(true);
    setSelectedId(record);
  };

  const handlePrintReceipt = (receiptId: string) => () => navigate(path.inPhieuXuat, { state: receiptId });

  const handleDownloadFile = (record: ExportItem) => () => {
    setSelectedId(record);
    downloadExport(record.Id);
  };

  const onChange: TableProps<ExportItem>['onChange'] = (pagination, filters, sorter, extra) => {
    setSearchParams((prevState) => {
      return { ...prevState, ...filters };
    });
  };

  const columns: ColumnsType<ExportItem> = useMemo(() => {
    let cols: ColumnsType<ExportItem> = [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        render: (value, record, index) => {
          return (
            queryConfig.page && queryConfig.pageSize && getSerialNumber(+queryConfig.page, +queryConfig.pageSize, index)
          );
        }
      }
    ];

    arrAvaliable?.[0] &&
      cols.push({
        title: 'Mã phiếu xuất kho',
        dataIndex: 'MaXuatKho',
        key: 'MaXuatKho',
        ...getColumnSearchProps('Tìm mã phiếu nhập', 'MaXuatKho', col === 'MaXuatKho')
      });

    arrAvaliable?.[1] &&
      cols.push({
        title: 'Số lượng sách',
        dataIndex: 'Count',
        key: 'Count',
        ...getColumnSearchProps('Nhập số lượng', 'Count', col === 'Count')
      });

    arrAvaliable?.[2] &&
      cols.push({
        title: 'Giá trị phiếu xuất kho',
        dataIndex: 'GiaBia',
        key: 'GiaBia',
        render(value, record, index) {
          return record.GiaBia
            ? new Intl.NumberFormat('vi-VN', {
                style: 'currency',
                currency: 'VND',
                maximumFractionDigits: 9
              }).format(Number(record.GiaBia))
            : '';
        },
        ...getColumnRangeProps('GiaBia', col === 'GiaBia')
      });

    arrAvaliable?.[3] &&
      cols.push({
        title: 'Người tạo',
        dataIndex: 'UserName',
        key: 'UserName',
        ...getColumnSearchProps('Người tạo', 'UserName', col === 'UserName')
      });

    arrAvaliable?.[4] &&
      cols.push({
        title: 'Ngày vào sổ',
        dataIndex: 'CreateDateTime',
        key: 'CreateDateTime',
        render: (value, record) => dayjs(record.CreateDateTime).format('HH:mm DD/MM/YYYY'),
        ...getColumnFilterDateProps('CreateDateTime', col === 'CreateDateTime')
      });

    return [
      ...cols,
      {
        title: 'Ngày vào sổ',
        dataIndex: '',
        key: 'actions',
        render: (value, record) => {
          return dayjs(record.NgayVaoSo).format('DD/MM/YYYY');
        }
      },
      {
        title: 'Hành động',
        dataIndex: 'actions',
        key: 'actions',
        render: (value, record) => {
          return (
            <div className='flex items-center justify-center gap-3'>
              <Tooltip title='Chi tiết'>
                <button onClick={() => navigate(`ChiTietPhieuXuat/${record.Id}`, { state: { ghiChu: record.GhiChu } })}>
                  <EyeFilled style={{ fontSize: '20px', color: '#08c' }} />
                </button>
              </Tooltip>

              {isAllowedAdjustment && (
                <>
                  <Tooltip title='In phiếu xuất kho'>
                    <button onClick={handlePrintReceipt(record.Id)}>
                      <PrinterFilled style={{ fontSize: '20px', color: '#08c' }} />
                    </button>
                  </Tooltip>

                  <Tooltip title='Tải phiếu xuất kho'>
                    <button onClick={handleDownloadFile(record)}>
                      <DownloadOutlined style={{ fontSize: '20px', color: '#08c' }} />
                    </button>
                  </Tooltip>

                  <Tooltip title='Hủy phiếu xuất kho'>
                    <button onClick={handleSelecteReceipt(record)}>
                      <CloseCircleOutlined style={{ fontSize: '20px', color: 'red' }} />
                    </button>
                  </Tooltip>
                </>
              )}
            </div>
          );
        }
      }
    ];
  }, [arrAvaliable, getColumnSearchProps, queryConfig.page, queryConfig.pageSize]);

  const handleCreateExport = () => navigate(path.taoPhieuXuatKho);

  return (
    <div className='p-5'>
      <Title title='Danh sách phiếu xuất kho' />

      <div className={isAllowedAdjustment ? 'my-3 flex items-center justify-between gap-2' : 'hidden'}>
        <div className='flex items-center gap-2'>
          <Button onClick={handleCreateExport}>Thêm mới</Button>
        </div>

        <Button onClick={handleShowColumns}>
          <SettingIcon /> Cấu hình hiển thị
        </Button>
      </div>

      <div className='mt-1'>
        <Table
          loading={isLoadingExportData}
          columns={columns}
          dataSource={List_SoXuatKho}
          pagination={pagination}
          scroll={{ x: 980 }}
          className='custom-table'
          locale={{
            emptyText: () => <Empty />
          }}
          bordered
          rowKey={(record) => record.Id}
          onChange={onChange}
        />

        {!!List_SoXuatKho.length && (
          <div
            className={classNames('relative', {
              'mt-[86px]': Number(queryConfig.pageSize) >= TotalCount
            })}
          >
            <div className='absolute bottom-2'>
              <SizeChanger visible={TotalCount > 0} value={List_SoXuatKho.length + ''} total={TotalCount + ''} />
            </div>
          </div>
        )}
      </div>

      <ModalDelete
        open={visibleModal}
        closable={false}
        title={'Bạn có chắc chắn muốn hủy phiếu xuất kho này không?'}
        handleCancel={() => {
          setVisibleModal(false);
        }}
        handleOk={() => {
          Boolean(selectedId?.Id) && deleteExport(selectedId?.Id + '');
        }}
        loading={isLoadingDeleteExport || isRefetching}
      />

      <SettingModalImport
        arrAvaliable={arrAvaliable}
        open={visibleModalImport}
        setVisibleModal={setVisibleModalImport}
        setArrAvaliable={setArrAvaliable}
        setCookie={setCookie}
      />

      <Loading open={isLoadingDownloadExport} />
    </div>
  );
};

export default ExportListPage;
