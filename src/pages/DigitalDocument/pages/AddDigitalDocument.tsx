import { CameraFilled } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Checkbox, DatePicker, Radio, RadioChangeEvent } from 'antd';
import { CheckboxChangeEvent } from 'antd/es/checkbox';
import locale from 'antd/es/date-picker/locale/vi_VN';
import { digitalDocumentApis, errorSyncDocuments } from 'apis';
import { Button, Loading, ModalDelete, Title, TitleDelete } from 'components';
import { AUDIO_FILE_TYPE, FORMAT_DATE_PICKER, VIDEO_FILE_TYPE } from 'constants/config';
import dayjs, { Dayjs } from 'dayjs';
import AuthorModal from 'pages/Publications/AddPublication/components/AuthorModal';
import LanguageModal from 'pages/Publications/AddPublication/components/LanguageModal';
import PublishingCompanyModal from 'pages/Publications/AddPublication/components/PublishingCompanyModal';
import { Revert } from 'pages/TitleApproval/icons';
import { ChangeEvent, useMemo, useRef, useState } from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { AudioVideoFile, EbookFile, InputWithLable, SelectWithLabel, TagSelectWithLabel } from '../components';
import TreeSelectWithLabel from '../components/TreeSelectWithLabel';
import { DefaultOptionType } from 'antd/es/select';
import { getEmbed } from 'utils/utils';

const exceptThisSymbols = ['e', 'E', '+', '-', '.'];

const digitalDocuemt = {
  SachDienTu: {
    title: 'Sách điện tử',
    acceptions:
      'application/pdf,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation'
  },
  AlbumAnh: {
    title: 'Album ảnh',
    acceptions: 'image/jpeg,image/gif,image/jpg,image/png,image/bmp'
  },
  Audio: {
    title: 'Sách nói',
    acceptions: '.mp3,audio/*'
  },
  Video: {
    title: 'Video',
    acceptions: 'video/mp4,video/x-m4v,video/*'
  }
};

const digitalDocuemtType = {
  SachDienTu: 'document',
  AlbumAnh: 'image',
  Audio: 'audio',
  Video: 'video'
};

const AddDigitalDocument = () => {
  const [formValues, setFormValues] = useState<{
    title: string;
    readingFee?: string;
    mainAuthors: string[];
    viceAuthors: string[];
    allowedReading: boolean;
    readingPermission?: number;
    allowedDownload: boolean;
    downloadPermission?: number;
    documentPercent: string;
    idDocument?: string;
    publisher?: string;
    language?: string;
    publishDate?: string;
    numberOfCopies?: number;
    source: string;
    size: string;
    reference: string;
    price: string;
    summary: string;
    isbn: string;
    bookCover?: string;
    ddc_19day?: string;
  }>({
    title: '',
    readingFee: '',
    mainAuthors: [],
    viceAuthors: [],
    allowedReading: false,
    allowedDownload: false,
    documentPercent: '',
    source: '',
    size: '',
    reference: '',
    price: '',
    summary: '',
    isbn: '',
    readingPermission: 1,
    downloadPermission: 1
  });
  const [addableModal, setAddableModal] = useState<'mainAuthor' | 'viceAuthor' | 'nxb' | 'language' | 'idDocument'>();
  const [filesPreview, setFilesPreview] = useState<File[]>([]);
  const [isVisiable, setIsVisiable] = useState<boolean>(false);
  const [bookCoverPreview, setBookCoverPreview] = useState<File>();
  const [listOfFiles, setListOfFiles] = useState<DetailDigitalDocument['ListMedia']>();

  const navigate = useNavigate();

  const { state } = useLocation();

  const { id, documentType } = useParams<{ documentType: 'SachDienTu' | 'AlbumAnh' | 'Audio' | 'Video'; id: string }>();
  const {
    data: optionData,
    isLoading: loadingOptionsData,
    isRefetching,
    refetch: refetchGetOptions,
    status
  } = useQuery({
    queryKey: ['getOptionsCreationDigitalDocument'],
    //@ts-ignore
    queryFn: () => digitalDocumentApis.getOptions(digitalDocuemtType?.[documentType]),
    cacheTime: 0,
    staleTime: Infinity
  });

  const {
    Options_DDC_Day19 = [],
    Options_Language = [],
    Options_NXB = [],
    Options_TacGia = [],
    Options_ThuMucSach = [],
    IsDay19
  } = optionData?.data.Item || {
    IsDay19: false,
    Options_DDC_Day19: [],
    Options_Language: [],
    Options_NXB: [],
    Options_TacGia: [],
    Options_ThuMucSach: []
  };

  const { isLoading: loadingPaperDocument, refetch: refetchDocument } = useQuery({
    queryKey: ['getDetailDocument', id, status === 'success'],
    queryFn: () => digitalDocumentApis.getDetailDocument(id + ''),
    enabled: !!id && status === 'success',
    onSuccess: (data) => {
      const doc = data.data.Item;

      setFormValues({
        bookCover: doc.LinkBiaSach || '',
        title: doc.TenSach || '',
        mainAuthors: doc.listIdTacGia || [],
        viceAuthors: doc.listIdTacGiaPhu || [],
        publisher: doc.IdNhaXuatBan || '',
        language: doc.IdNgonNgu || '',
        idDocument: doc.IdThuMucSach || '',
        publishDate: doc.NgayXuatBan || '',
        numberOfCopies: doc.SoBanSao || 0,
        allowedReading: doc.QuyenDoc !== 0 ? true : false,
        readingPermission: doc.QuyenDoc || 1,
        allowedDownload: doc.QuyenTai !== 0 ? true : false,
        downloadPermission: doc.QuyenTai || 1,
        ddc_19day: (IsDay19 ? doc.Day19 : doc.DDC) || '',
        isbn: doc.ISBN || '',
        source: doc.NguonTaiLieu || '',
        size: doc.KhoMau || '',
        reference: doc.LienKet || '',
        price: !!doc.GiaBia
          ? doc.GiaBia.trim()
              .replace(/\D/g, '')
              .replace(/\B(?=(\d{3})+(?!\d))/g, ',')
          : '',
        summary: doc.TomTat || '',
        documentPercent: doc.TiLeTaiXuong || '',
        readingFee: doc.PhiDocSach
          ? doc.PhiDocSach.trim()
              .replace(/\D/g, '')
              .replace(/\B(?=(\d{3})+(?!\d))/g, ',')
          : ''
      });

      setListOfFiles(doc.ListMedia);
      setFilesPreview([]);
      setBookCoverPreview(undefined);
    }
  });

  // call api able to get book's old data
  const { data: oldBookData } = useQuery({
    queryKey: ['getBookOldData', id, state?.isFromErrorSyncedDocument, state?.errorSyncedDocumentId],
    queryFn: () => errorSyncDocuments.getOldDetailBookData(state?.errorSyncedDocumentId + ''),
    enabled: !!state && state?.isFromErrorSyncedDocument && !!state?.errorSyncedDocumentId && !!id
  });

  const oldBookValue = oldBookData?.data?.Item?.Model;

  const { mutate: createDigitalDocument, isLoading: loadingCreateDigitalDocument } = useMutation({
    mutationFn: digitalDocumentApis.createDigitalDocument,
    onSuccess: () => {
      setFormValues({
        title: '',
        readingFee: '',
        mainAuthors: [],
        viceAuthors: [],
        allowedReading: false,
        allowedDownload: false,
        documentPercent: '',
        source: '',
        size: '',
        reference: '',
        price: '',
        summary: '',
        isbn: ''
      });

      setFilesPreview([]);

      toast.success('Tạo tài liệu thành công');

      navigate(-1);
    }
  });

  const { mutate: update, isLoading: loadingUpDate } = useMutation({
    mutationFn: digitalDocumentApis.updateDigitalDocument,
    onSuccess: () => {
      toast.success('Cập nhật tài liệu thành công');
      refetchDocument();
      refetchGetOptions();
      setListOfFiles(undefined);
    }
  });

  const { mutate: deleteBook, isLoading: isLoadingDelete } = useMutation({
    mutationFn: digitalDocumentApis.deleteSingleAll,
    onSuccess: () => {
      setIsVisiable(false);
      toast.success('Xóa đầu sách thành công');
      navigate(-1);
    }
  });

  const fileInutRef = useRef<HTMLInputElement | null>(null);
  const bookCoverInputRef = useRef<HTMLInputElement | null>(null);

  const previewImage = useMemo(() => {
    if (bookCoverPreview) return URL.createObjectURL(bookCoverPreview);

    return '';
  }, [bookCoverPreview]);

  const resultPreview = useMemo(() => {
    return previewImage ? previewImage : '';
  }, [previewImage]);

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { value, name } = e.target;
    if (!e) {
      return;
    }

    setFormValues((prevState) => {
      return { ...prevState, [name]: value };
    });
  };

  const handleReadingFee = (e: ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;

    setFormValues((prevState) => {
      return {
        ...prevState,
        readingFee: value
          .trim()
          .replace(/\D/g, '')
          .replace(/\B(?=(\d{3})+(?!\d))/g, ',')
      };
    });
  };

  const handleDocunemtPercent = (e: ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;

    setFormValues((prevState) => {
      return {
        ...prevState,
        documentPercent: +value > 100 ? '100' : value
      };
    });
  };

  const handleAddableModal = (value: 'mainAuthor' | 'viceAuthor' | 'nxb' | 'language' | 'idDocument') =>
    setAddableModal(value);

  const handleCloseModal = () => setAddableModal(undefined);

  const handleAccessFileInputRef = () => fileInutRef.current?.click();

  const handleAddAuthorSuccess = (data: string, id: string) => {
    refetchGetOptions();

    addableModal === 'mainAuthor' &&
      setFormValues((prevState) => {
        return { ...prevState, mainAuthors: [id, ...formValues.mainAuthors] };
      });

    addableModal === 'viceAuthor' &&
      setFormValues((prevState) => {
        return { ...prevState, viceAuthors: [id, ...formValues.viceAuthors] };
      });
  };

  const hanldeAddSuccessfully = async (value: string, name: string) => {
    await refetchGetOptions();

    setFormValues((prevState) => {
      return { ...prevState, [name]: value };
    });
  };

  const handleChangeAuthor = (value: string[], name: string) => {
    // if (!value.length) {
    //   return;
    // }

    setFormValues((prevState) => {
      return { ...prevState, [name]: value };
    });
  };

  const handleOnFileChange = (e: ChangeEvent<HTMLInputElement>) => {
    const filesFromLocal = e.target?.files;
    const fileSizeInMb = Number(((e.target?.files?.[0].size || 0) / (1024 * 1024)).toFixed(2));
    if (documentType === 'AlbumAnh' || documentType === 'SachDienTu') {
      if (!filesFromLocal) {
        return;
      }

      if (documentType === 'SachDienTu') {
        const name = filesFromLocal[0].name;
        const fileType = name.split('.');
        if (fileSizeInMb > 300) {
          return toast.warning('Dung lượng file sách điện tử phải nhỏ hơn 300Mb');
        }
        if (['pptx', 'ppt', 'pdf'].includes(fileType?.[fileType.length - 1].toLocaleLowerCase())) {
          setFilesPreview([filesFromLocal?.[0]]);
          setListOfFiles(undefined);
        } else {
          return toast.warn('Không đúng định dạng file');
        }
      }

      if (documentType === 'AlbumAnh') {
        for (let index = 0; index < filesFromLocal.length; index++) {
          // image/jpeg,image/gif,image/jpg,image/png,image/bmp'
          const name = filesFromLocal[index].name;
          const fileType = name.split('.');

          if (['jpeg', 'gif', 'jpg', 'png', 'bmp'].includes(fileType?.[fileType.length - 1].toLocaleLowerCase())) {
            continue;
          } else {
            return toast.warn('Không đúng định dạng file');
          }
        }

        setFilesPreview((prevState) => {
          return [...prevState, ...filesFromLocal];
        });
      }
    }

    if (documentType === 'Audio' || documentType === 'Video') {
      const fileFromLocal = e.target?.files?.[0];

      if (!fileFromLocal) {
        return;
      }

      const name = fileFromLocal.name;
      const fileType = name.split('.');

      if (documentType === 'Audio') {
        if (!AUDIO_FILE_TYPE.includes(fileType?.[fileType.length - 1].toLocaleLowerCase())) {
          return toast.warn('Không đúng định dạng file');
        }

        if (fileFromLocal.type.includes('audio')) {
          if (fileSizeInMb > 300) {
            return toast.warning('Dung lượng file audio phải nhỏ hơn 300Mb');
          }
        }
      }

      if (documentType === 'Video') {
        if (!VIDEO_FILE_TYPE.includes(fileType?.[fileType.length - 1].toLocaleLowerCase())) {
          return toast.warn('Không đúng định dạng file');
        }

        if (fileFromLocal.type.includes('video')) {
          if (fileSizeInMb > 300) {
            return toast.warning('Dung lượng file video phải nhỏ hơn 300Mb');
          }
        }
      }

      setListOfFiles(undefined);
      setFilesPreview([fileFromLocal]);
    }
  };

  const handleAccessBookCoverFileInputRef = () => bookCoverInputRef.current?.click();

  const handleBookCoverChange = (e: ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files?.[0];
    if (!fileFromLocal) {
      return;
    }

    if (fileFromLocal && !fileFromLocal.type.startsWith('image')) return;

    setBookCoverPreview(fileFromLocal);
  };

  const handleChangePermission = (e: CheckboxChangeEvent) => {
    const { value, name } = e.target;

    if (!name) return;

    setFormValues((prevState) => {
      return { ...prevState, [name]: !value };
    });
  };

  const handleChangeReadingPermission = (e: RadioChangeEvent) => {
    if (!e) return;

    setFormValues((prevState) => {
      return { ...prevState, readingPermission: +e.target.value };
    });
  };

  const handleChangeSelect = (e: ChangeEvent<HTMLSelectElement>) => {
    const { value, name } = e.target;

    setFormValues((prevState) => {
      return { ...prevState, [name]: value };
    });
  };

  const handleChangeDownloadPermission = (e: RadioChangeEvent) => {
    if (!e) return;

    setFormValues((prevState) => {
      return { ...prevState, downloadPermission: +e.target.value };
    });
  };

  const handleRemovePreviewImage = (index: number) => {
    const array = [...filesPreview];

    array.splice(index, 1);
    setFilesPreview(array);
  };

  const handleChangePublishDate = (value: Dayjs | null) => {
    setFormValues((prevState) => {
      return { ...prevState, publishDate: value?.toISOString() };
    });
  };

  const handleSubmit = () => {
    const formData = new FormData();

    !!id && formData.append('Id', id);
    bookCoverPreview && formData.append('LinkBiaSach', bookCoverPreview);
    formData.append('TenSach', formValues.title.replace(/\s\s+/g, ' '));
    formValues?.mainAuthors?.length &&
      formValues.mainAuthors.forEach((value) => formData.append('ListIdTacGia', value));
    formValues?.viceAuthors?.length &&
      formValues.viceAuthors.forEach((value) => formData.append('ListIdTacGiaPhu', value));
    formValues.publisher && formData.append('IdNhaXuatBan', formValues.publisher);
    formValues.language && formData.append('IdNgonNgu', formValues.language);
    formValues.idDocument && formData.append('IdThuMucSach', formValues.idDocument);
    formValues.ddc_19day && formData.append(IsDay19 ? 'Day19' : 'DDC', formValues.ddc_19day);
    formValues.isbn && formData.append('ISBN', formValues.isbn);
    formValues.publishDate &&
      formValues.publishDate !== '0001-01-01T00:00:00Z' &&
      formData.append('NgayXuatBan', dayjs(formValues.publishDate).startOf('day').toISOString());
    formValues.summary && formData.append('TomTat', formValues.summary);
    formValues.reference && formData.append('LienKet', formValues.reference);
    formValues.size && formData.append('KhoMau', formValues.size);
    formValues.source && formData.append('NguonTaiLieu', formValues.source);
    formValues.numberOfCopies && formData.append('SoBanSao', formValues.numberOfCopies + '');
    formValues.allowedReading
      ? formData.append('QuyenDoc', formValues.readingPermission + '')
      : formData.append('QuyenDoc', 0 + '');
    formValues.allowedReading &&
      formValues.readingFee &&
      formData.append('PhiDocSach', formValues.readingFee.replaceAll(',', '').trim());

    formValues.allowedDownload
      ? formData.append('QuyenTai', formValues.downloadPermission + '')
      : formData.append('QuyenTai', 0 + '');
    formValues.allowedDownload &&
      formValues.documentPercent &&
      formData.append('TiLeTaiXuong', formValues.documentPercent);

    formValues.price && formData.append('GiaBia', formValues.price.replaceAll(',', '').trim());

    // update document
    if (!!id) {
      // document type is image
      // Nếu type document là hình thì sử dụng field ListLink và STT để handle
      // việc thứ tự của hình cũ
      if (listOfFiles?.isImage) {
        listOfFiles.ListFile.forEach((file, index) => {
          formData.append('ListLink', file.LinkFile);
          formData.append('STT', index + '');
        });

        // Nếu thêm file mới thì add
        filesPreview.length && filesPreview.forEach((file) => formData.append('ListMedia', file));
      } else {
        // Nếu document type không phải hình ảnh => pdf
        // mà file của document có sẵn mà không xóa thì truyền null
        // => không add file
        // ngược lại thì add file
        listOfFiles?.ListFile.length
          ? formData.append('ListMedia', null + '')
          : filesPreview.length && filesPreview.forEach((file) => formData.append('ListMedia', file));
      }
      // create a new document
    } else {
      filesPreview.length && filesPreview.forEach((file) => formData.append('ListMedia', file));
    }

    !!id ? update(formData) : createDigitalDocument(formData);
  };

  const handleRemoveExistedImage = (index: number) => {
    if (!listOfFiles?.ListFile.length) {
      return;
    }

    const array = [...listOfFiles?.ListFile];

    array.splice(index, 1);

    // @ts-ignore
    setListOfFiles((prevState) => {
      return { ...prevState, ListFile: array };
    });
  };

  function handleMergeOldValue(name: keyof typeof formValues) {
    if (!oldBookValue) return;

    const field: Record<keyof typeof formValues, keyof DetailDigitalDocument> = {
      title: 'TenSach',
      mainAuthors: 'listIdTacGia',
      viceAuthors: 'listIdTacGiaPhu',
      publisher: 'IdNhaXuatBan',
      idDocument: 'IdThuMucSach',
      language: 'IdNgonNgu',
      publishDate: 'NgayXuatBan',
      numberOfCopies: 'SoBanSao',
      ddc_19day: 'Day19',
      isbn: 'ISBN',
      source: 'NguonTaiLieu',
      size: 'KhoMau',
      reference: 'LienKet',
      price: 'GiaBia',
      summary: 'TomTat',
      readingFee: 'Id',
      allowedReading: 'Id',
      allowedDownload: 'Id',
      documentPercent: 'Id',
      readingPermission: 'Id',
      downloadPermission: 'Id',
      bookCover: 'Id'
    };

    if (name === 'price') {
      setFormValues((prevState) => {
        return {
          ...prevState,
          // @ts-ignore
          price: (oldBookValue[field[name]] || '')
            .trim()
            .replace(/\D/g, '')
            .replace(/\B(?=(\d{3})+(?!\d))/g, ',')
        };
      });
    }

    setFormValues((prevState) => {
      // @ts-ignore
      return { ...prevState, [name]: oldBookValue[field[name]] || '' };
    });
  }

  const dataMergeOldValue = (name: keyof typeof formValues) => {
    if (!oldBookValue) return;

    const field: Record<keyof typeof formValues, keyof DetailDigitalDocument> = {
      title: 'TenSach',
      mainAuthors: 'listIdTacGia',
      viceAuthors: 'listIdTacGiaPhu',
      publisher: 'IdNhaXuatBan',
      idDocument: 'IdThuMucSach',
      language: 'IdNgonNgu',
      publishDate: 'NgayXuatBan',
      numberOfCopies: 'SoBanSao',
      ddc_19day: 'Day19',
      isbn: 'ISBN',
      source: 'NguonTaiLieu',
      size: 'KhoMau',
      reference: 'LienKet',
      price: 'GiaBia',
      summary: 'TomTat',
      readingFee: 'Id',
      allowedReading: 'Id',
      allowedDownload: 'Id',
      documentPercent: 'Id',
      readingPermission: 'Id',
      downloadPermission: 'Id',
      bookCover: 'Id'
    };

    if (name === 'price') {
      const value =
        JSON.stringify(formValues[name] || '') ===
        JSON.stringify(
          //@ts-ignore
          (oldBookValue[field[name]] || '')
            .trim()
            .replace(/\D/g, '')
            .replace(/\B(?=(\d{3})+(?!\d))/g, ',')
        );
      return !value;
    }

    const value =
      //@ts-ignore
      JSON.stringify(formValues[name] || '') === JSON.stringify(oldBookValue[field[name]] || '');
    return !value;
  };

  if (!documentType) {
    return <div>Tôi là mến</div>;
  }
  // pdf or ppt
  const filesPptPdf = !listOfFiles?.isImage ? listOfFiles?.ListFile?.[0]?.FileName?.split('.') : [];

  return (
    <div>
      <Title title={`${!!id ? 'Cập nhật' : 'Thêm mới'} ${digitalDocuemt[documentType]?.title || ''}`} />

      <div className='mt-4 grid grid-cols-12 gap-8'>
        <div className='col-span-2 h-full'>
          <div className='relative mb-10 flex h-full w-full justify-center overflow-hidden'>
            <input
              type='file'
              accept='image/jpeg,image/gif,image/jpg,image/png,image/bmp'
              className='hidden'
              ref={bookCoverInputRef}
              onChange={handleBookCoverChange}
            />

            <div className='group relative h-44 w-44'>
              <div className='my-5 h-44 w-44'>
                <img
                  src={resultPreview || formValues.bookCover || '/content/Book.png'}
                  alt='Hình bìa tài liệu'
                  className='h-44 w-44 object-cover shadow-md transition-all duration-200 ease-in-out group-hover:blur-[2px]'
                />
              </div>

              <button
                type='button'
                onClick={handleAccessBookCoverFileInputRef}
                className='absolute inset-0 my-5 flex  h-44 w-44 scale-110 items-center justify-center p-5 opacity-0 transition-all duration-200 ease-out group-hover:scale-100 group-hover:bg-black/40 group-hover:opacity-100'
              >
                <span className='mr-2 text-white'>Chọn hình ảnh</span> <CameraFilled style={{ color: 'white' }} />
              </button>
            </div>
          </div>
        </div>

        <div className='col-span-5 flex flex-col gap-y-6'>
          <TreeSelectWithLabel
            label='Thư mục tài liệu'
            //@ts-ignore
            treeData={[{ label: 'Chọn thư mục tài liệu', value: '' }, ...Options_ThuMucSach] as DefaultOptionType[]}
            onClickAddableButton={() => handleAddableModal('idDocument')}
            disabled={loadingOptionsData || isRefetching}
            value={formValues.idDocument || ''}
            onChange={(value) => {
              setFormValues((prevState) => {
                prevState.idDocument = value;
                return { ...prevState };
              });
            }}
            hasMerge={!!state?.isFromErrorSyncedDocument && dataMergeOldValue('idDocument')}
            onMerge={() => handleMergeOldValue('idDocument')}
          />
          <InputWithLable
            key={'title'}
            label='Nhan đề'
            placeholder='Nhập tên nhan đề'
            required
            maxLength={255}
            name='title'
            onChange={handleChange}
            value={formValues.title}
            hasMerge={!!state?.isFromErrorSyncedDocument && dataMergeOldValue('title')}
            onMerge={() => handleMergeOldValue('title')}
          />

          <TagSelectWithLabel
            key={'mainAuthors'}
            label='Tác giả chính'
            placeholder='Chọn tác giả chính'
            items={Options_TacGia}
            hasAddableButton
            onClickAddableButton={() => handleAddableModal('mainAuthor')}
            value={formValues.mainAuthors}
            onChange={(value: string[]) => handleChangeAuthor(value, 'mainAuthors')}
            disabled={loadingOptionsData}
            hasMerge={!!state?.isFromErrorSyncedDocument && dataMergeOldValue('mainAuthors')}
            onMerge={() => handleMergeOldValue('mainAuthors')}
          />

          <TagSelectWithLabel
            key={'viceAuthors'}
            label='Tác giả phụ'
            placeholder='Chọn tác giả phụ'
            items={Options_TacGia}
            hasAddableButton
            value={formValues.viceAuthors}
            onClickAddableButton={() => handleAddableModal('viceAuthor')}
            onChange={(value: string[]) => handleChangeAuthor(value, 'viceAuthors')}
            disabled={loadingOptionsData}
            hasMerge={!!state?.isFromErrorSyncedDocument && dataMergeOldValue('viceAuthors')}
            onMerge={() => handleMergeOldValue('viceAuthors')}
          />

          <SelectWithLabel
            label='Nhà xuất bản'
            items={[{ label: 'Chọn nhà xuất bản', value: '' }, ...Options_NXB]}
            hasAddableButton
            onClickAddableButton={() => handleAddableModal('nxb')}
            disabled={loadingOptionsData || isRefetching}
            value={formValues.publisher || ''}
            name='publisher'
            onChange={handleChangeSelect}
            hasMerge={!!state?.isFromErrorSyncedDocument && dataMergeOldValue('publisher')}
            onMerge={() => handleMergeOldValue('publisher')}
          />

          <SelectWithLabel
            label='Ngôn ngữ'
            items={[{ label: 'Chọn ngôn ngữ', value: '' }, ...Options_Language]}
            hasAddableButton
            onClickAddableButton={() => handleAddableModal('language')}
            disabled={loadingOptionsData || isRefetching}
            value={formValues.language || ''}
            name='language'
            onChange={handleChangeSelect}
            hasMerge={!!state?.isFromErrorSyncedDocument && dataMergeOldValue('language')}
            onMerge={() => handleMergeOldValue('language')}
          />

          <div className='grid grid-cols-12'>
            <label className='col-span-3 flex items-center gap-1 font-bold'>Ngày ban hành</label>

            <div className='col-span-9 flex items-center gap-1'>
              <DatePicker
                className='w-full border border-gray-300 py-[0.8rem] px-2 outline-none'
                placeholder='Chọn ngày ban hành'
                locale={locale}
                format={FORMAT_DATE_PICKER}
                disabledDate={(current) => {
                  return current && current > dayjs().endOf('day');
                }}
                onChange={handleChangePublishDate}
                value={
                  formValues.publishDate && formValues.publishDate !== '0001-01-01T00:00:00Z'
                    ? dayjs(formValues.publishDate)
                    : null
                }
              />

              {state?.isFromErrorSyncedDocument && dataMergeOldValue('publishDate') ? (
                <button onClick={() => handleMergeOldValue('publishDate')}>
                  <Revert />
                </button>
              ) : null}
            </div>
          </div>

          <InputWithLable
            label='Số bản sao'
            placeholder='Nhập số bản sao'
            name='numberOfCopies'
            onChange={handleChange}
            value={formValues.numberOfCopies || ''}
            onKeyDown={(e) => exceptThisSymbols.includes(e.key) && e.preventDefault()}
            max={9990}
            maxLength={4}
            // @ts-ignore
            onWheel={(e) => e.target.blur()}
            onKeyPress={(event) => {
              if (!/[0-9]/.test(event.key)) {
                event.preventDefault();
              }
            }}
            hasMerge={!!state?.isFromErrorSyncedDocument && dataMergeOldValue('numberOfCopies')}
            onMerge={() => handleMergeOldValue('numberOfCopies')}
          />
          <div className='grid grid-cols-12'>
            <label className='col-span-3 flex items-center gap-1 font-bold'>
              File tài liệu <span className='text-danger-10'>*</span>
            </label>

            <div className='col-span-9 flex items-center gap-1'>
              <Button className='whitespace-nowrap' onClick={handleAccessFileInputRef}>
                Chọn tệp
              </Button>
              <span className='ml-5 text-xs italic text-[#A7A7A7]'>
                Lưu ý: Bạn có thể upload file:{' '}
                <span className='font-bold'>
                  {documentType === 'SachDienTu'
                    ? '*pdf, *ppt, *pptx'
                    : documentType === 'Video'
                    ? '*avi, *mp4, *wmv, *mkv, *vob, *flv, *mpeg, *webm'
                    : documentType === 'Audio'
                    ? '*mp3, *wma, *wav, *flac, *alac, *ogg, *pcm, *aac, *m4a, *aiff'
                    : '*jpeg, *gif, *jpg, *png, *bmp'}
                </span>{' '}
                với dung lượng tối đa: 300 MB
              </span>{' '}
              <input
                type='file'
                accept={digitalDocuemt[documentType]?.acceptions || ''}
                className='hidden'
                ref={fileInutRef}
                onChange={handleOnFileChange}
                multiple={documentType === 'AlbumAnh'}
                // @ts-ignore
                onClick={(e: React.MouseEvent<HTMLInputElement, MouseEvent>) => (e.target.value = null)}
              />
            </div>
          </div>

          {/* image and ebook */}

          <>
            {(documentType === 'SachDienTu' || documentType === 'AlbumAnh') && (
              <EbookFile
                listOfFiles={listOfFiles}
                onDelete={() => {
                  setListOfFiles(undefined);
                }}
                filesPptPdf={filesPptPdf}
                filesPreview={filesPreview}
                onDeleteExistedImage={handleRemoveExistedImage}
                onDeletePreviewImage={handleRemovePreviewImage}
              />
            )}

            {(documentType === 'Audio' || documentType === 'Video') && (
              <AudioVideoFile
                listOfFiles={listOfFiles}
                onRemove={() => {
                  setListOfFiles(undefined);
                  setFilesPreview([]);
                }}
                reference={formValues?.reference || ''}
                file={filesPreview?.[0]}
              />
            )}
          </>
        </div>

        <div className='col-span-5 flex flex-col gap-y-6'>
          <SelectWithLabel
            label='Khung phân loại'
            placeholder='Nhập tên nhan đề'
            items={[{ label: IsDay19 ? `Chọn 19 Dãy` : 'Chọn DDC', value: '' }, ...Options_DDC_Day19]}
            name='ddc_19day'
            value={formValues.ddc_19day || ''}
            onChange={handleChangeSelect}
            hasMerge={!!state?.isFromErrorSyncedDocument && dataMergeOldValue('ddc_19day')}
            onMerge={() => handleMergeOldValue('ddc_19day')}
          />

          <InputWithLable
            label='ISBN/ISSN'
            placeholder='Nhập ISBN/ISSN'
            name='isbn'
            onChange={handleChange}
            value={formValues.isbn || ''}
            maxLength={50}
            hasMerge={!!state?.isFromErrorSyncedDocument && dataMergeOldValue('isbn')}
            onMerge={() => handleMergeOldValue('isbn')}
          />

          <InputWithLable
            label='Nguồn tài liệu'
            placeholder='Nhập nguồn tài liệu'
            name='source'
            value={formValues.source || ''}
            onChange={handleChange}
            hasMerge={!!state?.isFromErrorSyncedDocument && dataMergeOldValue('source')}
            onMerge={() => handleMergeOldValue('source')}
          />

          <InputWithLable
            label='Khổ mẫu'
            placeholder='Nhập khổ mẫu'
            name='size'
            onChange={handleChange}
            value={formValues.size}
            hasMerge={!!state?.isFromErrorSyncedDocument && dataMergeOldValue('size')}
            onMerge={() => handleMergeOldValue('size')}
          />

          <InputWithLable
            label='Liên kết'
            placeholder='Nhập liên kết'
            name='reference'
            onChange={handleChange}
            value={formValues.reference}
            maxLength={100}
            hasMerge={!!state?.isFromErrorSyncedDocument && dataMergeOldValue('reference')}
            onMerge={() => handleMergeOldValue('reference')}
          />

          <InputWithLable
            label='Giá tiền'
            placeholder='Nhập giá tiền'
            name='price'
            onChange={(e: ChangeEvent<HTMLInputElement>) => {
              const { value } = e.target;

              setFormValues((prevState) => {
                return {
                  ...prevState,
                  price: value
                    .trim()
                    .replace(/\D/g, '')
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                };
              });
            }}
            value={formValues.price}
            maxLength={11}
            onKeyDown={(e) => exceptThisSymbols.includes(e.key) && e.preventDefault()}
            hasMerge={!!state?.isFromErrorSyncedDocument && dataMergeOldValue('price')}
            onMerge={() => handleMergeOldValue('price')}
          />

          <InputWithLable
            label='Tóm tắt'
            placeholder='Nhập tóm tắt'
            name='summary'
            onChange={handleChange}
            value={formValues.summary}
            hasMerge={!!state?.isFromErrorSyncedDocument && dataMergeOldValue('summary')}
            onMerge={() => handleMergeOldValue('summary')}
          />
          <div className='grid grid-cols-12'>
            <label className='items-top col-span-3 flex gap-1 font-bold'>Phân quyền</label>

            <div className='col-span-4 rounded-md bg-white p-2 shadow-md'>
              <Checkbox
                checked={formValues.allowedReading}
                onChange={handleChangePermission}
                name='allowedReading'
                value={formValues.allowedReading}
                className='font-bold'
              >
                Cho phép đọc
              </Checkbox>

              <div className='my-1 ml-6'>
                <Radio
                  disabled={!formValues.allowedReading}
                  onChange={handleChangeReadingPermission}
                  checked={formValues.readingPermission === 1}
                  value={1}
                >
                  Thư viện nội bộ
                </Radio>

                <Radio
                  disabled={!formValues.allowedReading}
                  checked={formValues.readingPermission === 2}
                  onChange={handleChangeReadingPermission}
                  value={2}
                >
                  Tất cả thư viện liên thông
                </Radio>
              </div>

              <p className='mb-1 font-bold'>Phí đọc sách</p>

              <div className='broder-[1px] relative ml-2 rounded-md border border-primary-10 py-2 px-1'>
                <input
                  className='font-bold outline-none'
                  onKeyDown={(e) => exceptThisSymbols.includes(e.key) && e.preventDefault()}
                  onChange={handleReadingFee}
                  value={formValues.readingFee}
                  disabled={!formValues.allowedReading}
                  type='text'
                  maxLength={11}
                />

                <p className='absolute top-0 right-0 bg-[#D9D9D9]/50 py-2 px-1'>đồng</p>
              </div>
            </div>

            <div className='col-span-4 col-start-9 rounded-md bg-white p-2 shadow-md'>
              <Checkbox
                checked={formValues.allowedDownload}
                onChange={handleChangePermission}
                name='allowedDownload'
                value={formValues.allowedDownload}
                className='font-bold'
              >
                Cho phép tải về
              </Checkbox>

              <div className='my-1 ml-6'>
                <Radio
                  disabled={!formValues.allowedDownload}
                  onChange={handleChangeDownloadPermission}
                  value={1}
                  checked={formValues.downloadPermission === 1}
                >
                  Thư viện nội bộ
                </Radio>

                <Radio
                  disabled={!formValues.allowedDownload}
                  onChange={handleChangeDownloadPermission}
                  value={2}
                  checked={formValues.downloadPermission === 2}
                >
                  Tất cả thư viện liên thông
                </Radio>
              </div>

              <div className='broder-[1px] relative ml-2 rounded-md border border-primary-10 py-2 px-1'>
                <input
                  className='font-bold outline-none'
                  onKeyDown={(e) => exceptThisSymbols.includes(e.key) && e.preventDefault()}
                  onChange={handleDocunemtPercent}
                  value={formValues.documentPercent}
                  maxLength={3}
                  max={100}
                  type='number'
                  disabled={!formValues.allowedDownload}
                />

                <p className='absolute top-0 right-0 bg-[#D9D9D9]/50 py-2 px-1'>% tác phẩm</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <AuthorModal
        open={addableModal === 'mainAuthor'}
        title='Thêm tác giả'
        onBack={handleCloseModal}
        onSuccess={handleAddAuthorSuccess}
      />

      <AuthorModal
        open={addableModal === 'viceAuthor'}
        title='Thêm tác giả'
        onBack={handleCloseModal}
        onSuccess={handleAddAuthorSuccess}
      />

      <PublishingCompanyModal
        open={addableModal === 'nxb'}
        title='Thêm nhà xuất bản'
        onBack={handleCloseModal}
        onSuccess={(value) => hanldeAddSuccessfully(value, 'publisher')}
      />

      <LanguageModal
        open={addableModal === 'language'}
        title='Thêm ngôn ngữ'
        onBack={handleCloseModal}
        onCreateSuccess={(value) => hanldeAddSuccessfully(value, 'language')}
      />
      <ModalDelete
        open={isVisiable}
        closable={false}
        title={
          <TitleDelete firstText={'Bạn có chắn chắn muốn xóa đầu sách'} secondText={formValues.title}></TitleDelete>
        }
        handleCancel={() => {
          setIsVisiable(false);
        }}
        handleOk={() => {
          deleteBook(id || '');
        }}
        loading={isLoadingDelete}
      />
      <div className='my-3 flex items-center justify-end gap-2'>
        <Button variant='secondary' onClick={() => navigate(-1)}>
          Quay về
        </Button>
        {state?.isFromErrorSyncedDocument && id && (
          <Button
            type='button'
            className='font-semibold'
            variant={'danger'}
            onClick={() => {
              setIsVisiable(true);
            }}
          >
            Xóa sách
          </Button>
        )}
        <Button
          disabled={
            !formValues.title.trim() ||
            (!filesPreview.length &&
              !listOfFiles?.ListFile.length &&
              !(documentType === 'Video' && formValues.reference && !!getEmbed(formValues.reference)))
          }
          variant={
            !formValues.title.trim() ||
            (!filesPreview.length &&
              !listOfFiles?.ListFile.length &&
              !(documentType === 'Video' && formValues.reference && !!getEmbed(formValues.reference)))
              ? 'disabled'
              : 'default'
          }
          onClick={handleSubmit}
        >
          {!!id ? 'Cập nhật' : 'Thêm mới'}
        </Button>
      </div>

      <Loading open={loadingCreateDigitalDocument || (!!id && loadingPaperDocument) || loadingUpDate || isRefetching} />
    </div>
  );
};

export default AddDigitalDocument;
