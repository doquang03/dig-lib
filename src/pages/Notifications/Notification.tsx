import { DatePicker, Switch, Table, Tooltip } from 'antd';
import locale from 'antd/es/date-picker/locale/vi_VN';
import { ColumnsType } from 'antd/es/table';
import classNames from 'classnames';
import { Button, Input, ModalDelete, Select, SizeChanger, Title, TitleDelete } from 'components';
import { FORMAT_DATE_PICKER } from 'constants/config';
import dayjs, { Dayjs } from 'dayjs';
import { ChangeEvent, useState } from 'react';

import { DeleteFilled } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { notificationApis } from 'apis';
import { EditIcon } from 'assets';
import { path } from 'constants/path';
import { debounce } from 'lodash';
import { Link, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getSerialNumber } from 'utils/utils';

const Notification = () => {
  const [searchParams, setSearchParams] = useState<{
    day?: number;
    month?: number;
    year?: number;
    title?: string;
    status?: string;
    search?: string;
    page: number;
    pageSize: number;
  }>({
    search: '',
    day: 0,
    month: 0,
    year: 0,
    status: '',
    page: 1,
    pageSize: 30
  });
  const [selectedNoti, setSelectedNoti] = useState<NotificationApp>();

  const navigate = useNavigate();

  const {
    data: notificationsData,
    isLoading: loadingNotifications,
    refetch
  } = useQuery({
    queryKey: ['getNotifications', searchParams],
    queryFn: () => {
      const req: any = {};

      if (searchParams.day) {
        req.day = searchParams.day;
        req.month = searchParams.month;
        req.year = searchParams.year;
      }

      if (searchParams.status === 'true') {
        req.IsActive = true;
      }
      if (searchParams.status === 'false') {
        req.IsActive = false;
      }

      if (searchParams.search) {
        req.TextForSearch = searchParams.search;
      }

      return notificationApis.getNotifications({
        page: searchParams.page,
        pageSize: searchParams.pageSize,
        ...req
      });
    }
  });

  const { mutate: changeStatus, isLoading: loadingChangeStatus } = useMutation({
    mutationFn: notificationApis.changeStatus,
    onSuccess: () => {
      refetch();
    }
  });

  const { mutate: removeNoti, isLoading: loadingRemove } = useMutation({
    mutationFn: notificationApis.removeNoti,
    onSuccess: () => {
      toast.success('Xóa thông báo thành công');
      refetch();
      setSelectedNoti(undefined);
    }
  });

  const notifications = notificationsData?.data.Item || { ListThongBao: [], count: 0 };

  const columns: ColumnsType<NotificationApp> = [
    {
      key: 'STT',
      title: 'STT',
      dataIndex: 'stt',
      render: (value, record, index) => {
        return (
          searchParams.page &&
          searchParams.pageSize &&
          getSerialNumber(+searchParams.page, +searchParams.pageSize, index)
        );
      },
      width: 80
    },
    {
      key: 'TieuDe',
      title: 'Tiêu đề thông báo',
      dataIndex: 'TieuDe',
      render: (value, { Id, TieuDe, ThoiGian }) => (
        <Tooltip placement='topLeft' title={TieuDe} arrow={true} className='truncate text-left'>
          {new Date(ThoiGian) < new Date() ? (
            <p className='text-left'>{TieuDe}</p>
          ) : (
            <Link to={`CapNhat/${Id}`} className='text-left italic text-primary-50 hover:underline'>
              {TieuDe}
            </Link>
          )}
        </Tooltip>
      ),
      onCell: (record) => ({
        className: 'text-left'
      }),
      ellipsis: true
    },
    {
      key: 'NoiDung',
      title: 'Nội dung thông báo',
      dataIndex: 'NoiDung',
      render: (value, record) => (
        <Tooltip placement='topLeft' arrow={true} className='truncate'>
          <div
            dangerouslySetInnerHTML={{
              __html: record.NoiDung.length > 60 ? record.NoiDung + '...' : record.NoiDung
            }}
            className='truncate text-left'
          />
        </Tooltip>
      ),
      onCell: (record) => ({
        className: 'text-left'
      }),
      ellipsis: true
    },
    {
      key: 'ThoiGian',
      title: 'Thời gian gửi',
      dataIndex: 'ThoiGian',
      render: (value, record, index) => {
        return dayjs(record.ThoiGian).format('HH:mm DD/MM/YYYY');
      },
      width: 200
    },
    {
      key: 'status',
      title: 'Trạng thái',
      dataIndex: 'status',
      render: (value, record) => {
        return (
          <Switch
            size='small'
            checked={record.IsActive}
            onChange={(value) => changeStatus({ Id: record.Id, IsActive: value })}
          />
        );
      },
      width: 100
    },
    {
      key: 'actions',
      title: 'Hành động',
      dataIndex: 'actions',
      width: 150,
      render: (value, record) => {
        return (
          <div>
            <button
              className='mx-2'
              onClick={(e) => {
                navigate(`CapNhat/${record.Id}`);
              }}
            >
              <EditIcon style={{ fontSize: '20px' }} />
            </button>

            <button
              className='mx-2'
              onClick={(e) => {
                e.stopPropagation();
                setSelectedNoti(record);
              }}
            >
              <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
            </button>
          </div>
        );
      }
    }
  ];

  const handleChangeDatePicker = (value: Dayjs | null) => {
    setSearchParams((prevState) => {
      return {
        ...prevState,
        day: value?.date(),
        month: (value?.month() || 1) + 1,
        year: value?.year()
      };
    });
  };

  const handleOnChangeStatus = (e: ChangeEvent<HTMLSelectElement>) => {
    if (!e) return;

    const { value } = e.target;

    setSearchParams((prevState) => {
      return {
        ...prevState,
        status: value
      };
    });
  };

  const handleChangeTitle = debounce((e: ChangeEvent<HTMLInputElement>) => {
    if (!e) return;

    const { value } = e.target;

    setSearchParams((prevState) => {
      return {
        ...prevState,
        search: value
      };
    });
  }, 300);

  return (
    <div>
      <Title title='DANH SÁCH THÔNG BÁO' />

      <form className='mt-3 flex flex-1 flex-col items-center gap-1 bg-primary-50/20 p-2.5 md:flex-row'>
        <Input placeholder='Nhập tiêu đề, nội dung' containerClassName='w-full' onChange={handleChangeTitle} />

        <DatePicker
          placeholder='Chọn thời gian gửi'
          onChange={handleChangeDatePicker}
          className={'w-full border border-gray-300 py-[0.8rem] px-2 outline-none'}
          format={FORMAT_DATE_PICKER}
          locale={locale}
        />

        <Select
          items={[
            { value: '', label: 'Chọn trạng thái' },
            { value: 'true', label: 'Đang hoạt động' },
            { value: 'false', label: 'Ngừng hoạt động' }
          ]}
          className='w-full'
          onChange={handleOnChangeStatus}
          value={searchParams.status}
        />
      </form>

      <Button className='mt-3' onClick={() => navigate(path.addNotification)}>
        Tạo thông báo
      </Button>

      <Table
        rowKey={(row) => row.Id}
        loading={loadingNotifications}
        dataSource={notifications.ListThongBao}
        className='custom-table mt-3'
        columns={columns}
        bordered
        pagination={{
          onChange(current, pageSize) {
            setSearchParams((prevState) => {
              return {
                ...prevState,
                pageSize: pageSize || 30,
                page: current
              };
            });
          },
          pageSize: searchParams.pageSize,
          hideOnSinglePage: false,
          showSizeChanger: false,
          total: notifications.count,
          showQuickJumper: true,
          locale: { jump_to: '', page: '' },
          current: searchParams.page
        }}
      />

      <div
        className={classNames('relative', {
          'mt-[0px]': searchParams.pageSize >= Number(notifications.count || 0)
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger
            visible={!!notifications.count}
            value={searchParams.pageSize + ''}
            currentPage={searchParams.page.toString()}
            total={notifications.count + ''}
            onChange={(pageSize) => {
              setSearchParams((prevState) => {
                return {
                  ...prevState,
                  pageSize: +pageSize,
                  page: 1
                };
              });
            }}
          />
        </div>
      </div>

      <ModalDelete
        open={Boolean(selectedNoti)}
        title={
          <TitleDelete firstText='Bạn có chắc chắn muốn xóa thông báo này không' secondText={selectedNoti?.TieuDe} />
        }
        handleOk={() => selectedNoti && removeNoti(selectedNoti?.Id)}
        handleCancel={() => setSelectedNoti(undefined)}
      />
    </div>
  );
};

export default Notification;
