import Icon from '@ant-design/icons';
import type { CustomIconComponentProps } from '@ant-design/icons/lib/components/Icon';

const BookSvg = ({ fill }: { fill: string }) => {
  return (
    <svg width='14' height='14' viewBox='0 0 16 16' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path
        d='M16 0H2C0.9 0 0 0.9 0 2V16C0 17.1 0.9 18 2 18H16C17.1 18 18 17.1 18 16V2C18 0.9 17.1 0 16 0ZM11 14H4V12H11V14ZM14 10H4V8H14V10ZM14 6H4V4H14V6Z'
        fill={fill}
      />
    </svg>
  );
};

const BookIcon = (props: Partial<CustomIconComponentProps & { fill: string }>) => {
  return <Icon component={() => <BookSvg fill={props.fill || '#3472A2'} />} {...props} />;
};

export default BookIcon;
