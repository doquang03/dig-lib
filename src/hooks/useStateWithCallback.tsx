import { SetStateAction, useEffect, useRef, useState } from 'react';

type InitialState<S> = S | (() => S);

type ActionCallBack<S> = (state: S) => void;

type SetStateWithCallBack<S> = (value: SetStateAction<S>, callback?: ActionCallBack<S>) => void;

type Return<S> = [S, SetStateWithCallBack<S>];

function useStateWithCallback<S>(initialState: InitialState<S>): Return<S> {
  const [state, setState] = useState(initialState);

  const callbackRef = useRef<ActionCallBack<S>>();

  useEffect(() => {
    if (callbackRef.current) {
      callbackRef.current?.(state);
      callbackRef.current = undefined;
    }
  }, [state]);

  const setStateWithCallback: SetStateWithCallBack<S> = (newState, callback) => {
    setState(newState);
    callbackRef.current = callback;
  };

  return [state, setStateWithCallback];
}

export default useStateWithCallback;
