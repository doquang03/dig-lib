import { path } from 'constants/path';
import { Outlet, useMatch } from 'react-router-dom';
import SubjectManagermentContent from './SubjectManagermentContent/SubjectManagermentContent';

const SubjectManagementPage = () => {
  const match = useMatch(path.monhoc);

  return <>{Boolean(match) ? <SubjectManagermentContent /> : <Outlet />}</>;
};

export default SubjectManagementPage;
