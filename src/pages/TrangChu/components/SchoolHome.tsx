import { useQuery } from '@tanstack/react-query';
import { settingApis } from 'apis';
import statisticApis from 'apis/statistic.apis';
import { YearSelection } from 'components';
import 'css/HomePage.css';
import 'css/bootstrap.min.css';
import dayjs from 'dayjs';
import { ChangeEvent, useMemo, useRef, useState } from 'react';
import CharacterBox from './CharacterBox';
import InteractionToDocumentLineChart from './InteractionToDocumentLineChart';
import NumberOfSUniqueBooks from './NumberOfSUniqueBooks';
import PercentageDonutBox from './PercentageDonutBox';
import ReturnBookTable from './ReturnBookTable';
import StatisticalBox from './StatisticalBox';
import TheMostBorrowingBooks from './TheMostBorrowingBooks';
import TheMostReadersBorrowBooks from './TheMostReadersBorrowBooks';

const SchoolHome = () => {
  const [yearValue, setYearValue] = useState<string>();
  const [params, setParams] = useState<{
    fromDate: string;
    toDate: string;
  }>({ fromDate: '', toDate: '' });
  const [isGlobal, setIsGlobal] = useState<boolean>(false);
  const [paramsBorrowingBooks, setParamsBorrowingBooks] = useState<{
    idWarehouse: string;
    top: string;
  }>({
    idWarehouse: '',
    top: '10'
  });

  const ref = useRef<HTMLDivElement>();

  const { data: startDateData, isLoading: loadingStartDateData } = useQuery({
    queryKey: ['getStartDate'],
    queryFn: settingApis.getStartDate,
    onSuccess: (data) => {
      const date = data.data.Item.NgayBatDauNamHoc.split('/');
      const dateResult = `${date[1]}/${date[0]}/${new Date().getFullYear()}`;

      let currentYear1 = '';
      let currentYear2 = '';

      // nếu ngày bắt đầu năm học > ngày hiện tại thì lấy năm hiện tại - năm hiện tại + 1
      // nếu ngày bắt đàu năm học < ngày hiện tại thì lấy năm hiện tại - 1 - năm hiện tại
      if (dayjs(dateResult).startOf('day').isAfter(dayjs())) {
        currentYear1 = dayjs(dateResult).subtract(1, 'day').startOf('day').toISOString();
        currentYear2 = dayjs(dateResult).subtract(1, 'day').startOf('day').add(1, 'year').toISOString();
      } else {
        currentYear1 = dayjs(dateResult).subtract(1, 'day').startOf('day').toISOString();
        currentYear2 = dayjs(dateResult).subtract(1, 'day').startOf('day').add(1, 'year').toISOString();
      }
      setYearValue(`${currentYear1} - ${currentYear2}`);

      setParams({ fromDate: currentYear1, toDate: currentYear2 });
    }
  });

  const { data: schoolDashboardData } = useQuery({
    queryKey: ['getSchoolDashboard', params],
    queryFn: () =>
      statisticApis.getSchoolDashboardData({
        TuNgay: params.fromDate,
        DenNgay: params.toDate
      }),
    enabled: !!params.fromDate && !!params.toDate
  });

  const schoolDashboard = schoolDashboardData?.data.Item;

  const { data: documentInteractingData, isLoading: loadingDocumentInteracting } = useQuery({
    queryKey: ['getDocumentInteracting', params, isGlobal],
    queryFn: () =>
      statisticApis.getSchoolDashboardInteraction({
        TuNgay: params.fromDate,
        DenNgay: params.toDate,
        isGlobal
      }),
    enabled: !!params.fromDate && !!params.toDate
  });

  const { LuotDoc, LuotMuon, LuotTai } = documentInteractingData?.data.Item || {};

  const documentInteractingLineCharrData = useMemo(() => {
    const labels = [];

    for (const key in LuotDoc) {
      labels.push(key);
    }

    const data = {
      labels: labels || [],
      datasets: [
        {
          label: 'Số lượt đọc tài liệu',
          data: labels?.map((label) => LuotDoc?.[label]) || [],
          borderColor: '#FF1616',
          backgroundColor: '#FF1616'
        },
        {
          label: 'Số lượt tải tài liệu',
          data: labels?.map((label) => LuotTai?.[label]) || [],
          borderColor: '#119757',
          backgroundColor: '#119757'
        },
        {
          label: 'Số lượt mượn tài liệu',
          data: labels?.map((label) => LuotMuon?.[label]) || [],
          borderColor: '#B60DF5',
          backgroundColor: '#B60DF5'
        }
      ]
    };

    return data;
  }, [LuotDoc, LuotMuon, LuotTai]);

  const { data: libNameData } = useQuery({
    queryFn: settingApis.getLibName,
    queryKey: ['libName']
  });

  const { data: theMostBorrowingBooksData, isLoading: theMostBorrowingBooks } = useQuery({
    queryKey: ['getTheMostBorringBooks', params, paramsBorrowingBooks],
    queryFn: () =>
      statisticApis.getTheMostBorrowingBooks({
        TuNgay: params.fromDate,
        DenNgay: params.toDate,
        IdKho: paramsBorrowingBooks.idWarehouse,
        Page: +paramsBorrowingBooks.top
      })
  });

  function toogleYearSelection() {
    ref && ref?.current?.focus();
  }

  function handleSelectSchoolYear(e: ChangeEvent<HTMLSelectElement>) {
    if (!e) return;

    const dateStart = startDateData?.data.Item.NgayBatDauNamHoc;

    if (!dateStart) return;

    const { value } = e.target;

    setYearValue(value);

    const years = value.split('-');

    const dateConvert = dateStart?.split('/');

    const currentYear = dayjs(`${dateConvert[1]}/${dateConvert[0]}/${years[1].trimEnd()}`)
      .subtract(1, 'day')
      .startOf('day')
      .toISOString();

    const prevYear = dayjs(`${dateConvert[1]}/${dateConvert[0]}/${years[0].trimStart()}`).endOf('day').toISOString();

    setParams({ fromDate: prevYear, toDate: currentYear });
  }

  const paperPercentage = Math.floor(
    ((schoolDashboard?.TongTaiLieuGiay || 0) /
      ((schoolDashboard?.TongTaiLieuSo || 0) + schoolDashboard?.TongTaiLieuGiay || 0)) *
      100
  );

  const totalDocumentPieChart = {
    labels: ['Tài liệu giấy', 'Tài liệu số'],
    datasets: [
      {
        data: [paperPercentage, 100 - paperPercentage],
        backgroundColor: ['#A23434', '#3472A2']
      }
    ]
  };

  const totalDigitalDocument =
    (schoolDashboard?.TongTaiLieu || 0) +
    (schoolDashboard?.TongHinhAnh || 0) +
    (schoolDashboard?.TongAmThanh || 0) +
    (schoolDashboard?.TongVideo || 0);

  const eBookPercentage = Math.floor(((schoolDashboard?.TongTaiLieu || 0) / totalDigitalDocument) * 100);
  const imageBookPercentage = Math.floor(((schoolDashboard?.TongHinhAnh || 0) / totalDigitalDocument) * 100);
  const audioBookPercentage = Math.floor(((schoolDashboard?.TongAmThanh || 0) / totalDigitalDocument) * 100);
  const videoBookPercentage = Math.floor(((schoolDashboard?.TongVideo || 0) / totalDigitalDocument) * 100);

  const digitalDocumentTypePieChart = {
    labels: ['Sách điện tử', 'Hình ảnh', 'Âm thanh', 'Video'],
    datasets: [
      {
        data: [eBookPercentage || 0, imageBookPercentage || 0, audioBookPercentage || 0, videoBookPercentage || 0],
        backgroundColor: ['#A23434', '#A7A7A7', '#D4CD16', '#119757', '#892DA9']
      }
    ]
  };

  function handleChangeLibraryType(e: ChangeEvent<HTMLSelectElement>) {
    const { value } = e.target;

    switch (value) {
      case '1':
        setIsGlobal(false);
        break;
      case '2':
        setIsGlobal(true);
        break;

      default:
        break;
    }
  }

  return (
    <div className='my-3'>
      <div className='flex items-center justify-end gap-1'>
        <p>Năm học: </p>

        <YearSelection
          onClick={toogleYearSelection}
          value={yearValue || ''}
          loading={loadingStartDateData}
          onChange={handleSelectSchoolYear}
        />
      </div>

      <div className='mt-6 grid grid-cols-12 gap-3'>
        <div className='col-span-8'>
          <p className='w-full rounded-md bg-white p-2 text-xl font-bold shadow-md'>
            <span className='text-primary-10'>Xin chào, </span>
            <span className='text-danger-30'>{libNameData?.data.Item.TenThuVien}!</span>
          </p>

          <div className='my-3 flex justify-around gap-3'>
            <CharacterBox character='student' value={schoolDashboard?.TongHocSinh} />
            <CharacterBox character='teacher' value={schoolDashboard?.TongGiaoVien} />
            <CharacterBox character='libraryMonitor' value={schoolDashboard?.TongCanBoThuVien} />
          </div>

          <div className='flex items-center justify-around gap-3'>
            <PercentageDonutBox
              title='BIỂU ĐỒ TỈ LỆ TÀI LIỆU'
              data={totalDocumentPieChart}
              position='bottom'
              totalValue={schoolDashboard?.TongTaiLieuGiay + schoolDashboard?.TongTaiLieuSo}
              dataAhaha={{
                haha: {
                  'Tài liệu giấy': schoolDashboard?.TongTaiLieuGiay || 0,
                  'Tài liệu số': schoolDashboard?.TongTaiLieuSo || 0
                }
              }}
            />

            {/*  labels: ['Sách điện tử', 'Hình ảnh', 'Âm thanh', 'Video'], */}

            <PercentageDonutBox
              title='BIỂU ĐỒ TỈ LỆ LOẠI TÀI LIỆU SỐ'
              data={digitalDocumentTypePieChart}
              totalValue={
                (schoolDashboard?.TongTaiLieu || 0) +
                (schoolDashboard?.TongHinhAnh || 0) +
                (schoolDashboard?.TongAmThanh || 0) +
                (schoolDashboard?.TongVideo || 0)
              }
              position='bottom'
              dataAhaha={{
                haha: {
                  'Sách điện tử': schoolDashboard?.TongTaiLieuGiay || 0,
                  'Hình ảnh': schoolDashboard?.TongHinhAnh || 0,
                  'Âm thanh': schoolDashboard?.TongAmThanh || 0,
                  Video: schoolDashboard?.TongVideo || 0
                }
              }}
            />
          </div>

          <InteractionToDocumentLineChart data={documentInteractingLineCharrData} onChange={handleChangeLibraryType} />

          <TheMostBorrowingBooks
            data={
              theMostBorrowingBooksData?.data?.Item?.ListModel?.map((item) => ({
                label: item.TenSach,
                value: item.Value
              })) || []
            }
            onTopChange={(e) => setParamsBorrowingBooks((prevState) => ({ ...prevState, top: e.target.value }))}
            onWarehouseChange={(e) =>
              setParamsBorrowingBooks((prevState) => ({ ...prevState, idWarehouse: e.target.value }))
            }
          />
        </div>

        <div className='col-span-4'>
          {/* <div className='my-3'> */}
          <ReturnBookTable data={schoolDashboard?.DanhSachDenHan || []} />

          <h3 className='my-3 text-xl font-bold text-primary-10'>THỐNG KÊ SỐ LƯỢNG</h3>

          <div className='grid grid-cols-4 gap-2'>
            <div className='col-span-2'>
              <StatisticalBox type='totalRentingBooks' value={schoolDashboard?.TongSachDangMuon || 0} />
            </div>

            <div className='col-span-2'>
              <StatisticalBox type='totalRentingReaders' value={schoolDashboard?.TongThanhVien || 0} />
            </div>

            <div className='col-span-2'>
              <StatisticalBox type='totalBookedReaders' value={schoolDashboard?.TongNguoiDatMuon || 0} />
            </div>
            <div className='col-span-2'>
              <StatisticalBox type='totalVisitingReaders' value={schoolDashboard?.TongBanDocVangLai || 0} />
            </div>
          </div>
          {/* </div> */}

          <NumberOfSUniqueBooks
            khoSach={schoolDashboard?.KhoSach}
            sachCaBietTheoKho={schoolDashboard?.SachCaBietTheoKho}
          />

          <TheMostReadersBorrowBooks data={schoolDashboard?.TopMuonThanhVien || []} />
        </div>
      </div>
    </div>
  );
};

export default SchoolHome;
