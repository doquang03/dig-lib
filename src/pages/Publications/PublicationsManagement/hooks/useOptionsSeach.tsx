import { useQuery } from '@tanstack/react-query';
import { bookApis } from 'apis';
import { useMemo } from 'react';

const useOptionsSeach = () => {
  const { data: optionsData } = useQuery({
    queryKey: ['options'],
    queryFn: bookApis.GetInitView,
    staleTime: 10000
  });

  const options = useMemo(() => {
    if (!optionsData?.data.Item) return;

    const {
      Options_BoSuuTap,
      Options_ChuDiem,
      Options_MonHoc,
      Options_NXB,
      Options_TacGia,
      Options_ThuMucSach,
      Options_KeSach
    } = optionsData?.data.Item;

    const optionsBST = [{ label: 'Chọn bộ sưu tập', value: '' }, ...Options_BoSuuTap];
    const optionsChuDiem = [{ label: 'Chọn chủ điểm', value: '' }, ...Options_ChuDiem];
    const optionsMonHoc = [{ label: 'Chọn môn học', value: '' }, ...Options_MonHoc];
    const optionsNXB = [{ label: 'Chọn nhà xuất bản', value: '' }, ...Options_NXB];
    const optionsTacGia = [{ label: 'Chọn tác giả', value: '' }, ...Options_TacGia];
    const optionsTMS = [{ label: 'Chọn thư mục tài liệu', value: '' }, ...Options_ThuMucSach];
    const optionsKeSach = [{ label: 'Chọn kệ sách', value: '' }, ...Options_KeSach];

    return { optionsBST, optionsChuDiem, optionsMonHoc, optionsNXB, optionsTacGia, optionsTMS, optionsKeSach };
  }, [optionsData?.data.Item]);

  return options;
};

export default useOptionsSeach;
