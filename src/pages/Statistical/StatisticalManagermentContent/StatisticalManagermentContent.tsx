import { useQuery } from '@tanstack/react-query';
import { getValue } from '@testing-library/user-event/dist/utils';
import { Dropdown, TabsProps } from 'antd';
import statisticApis from 'apis/statistic.apis';
import {
  ArcElement,
  BarElement,
  CategoryScale,
  ChartDataset,
  Chart as ChartJS,
  Legend,
  LineElement,
  LinearScale,
  PointElement,
  Title as TitleChart,
  Tooltip
} from 'chart.js';
import ChartDataLabels from 'chartjs-plugin-datalabels';
import { DatePicker, Loading, Select, Tabs, Title } from 'components';
import { TYPE_READER_OPTIONS } from 'constants/options';
import 'css/Statistical.css';
import dayjs from 'dayjs';
import { title } from 'process';
import { useMemo, useState } from 'react';
import { Bar, Line, Pie } from 'react-chartjs-2';
import { useForm } from 'react-hook-form';
import { convertDate, generateColor } from 'utils/utils';

ChartJS.register(
  ChartDataLabels,
  CategoryScale,
  LinearScale,
  BarElement,
  ArcElement,
  PointElement,
  LineElement,
  TitleChart,
  Tooltip,
  Legend
);

type FormInput = {
  year: string;
  month: string;
  day: string;
  typeAccountDay: string;
  typeAccountMonth: string;
  typeAccountYear: string;
};

const StatisticalManagermentContent = () => {
  const today = new Date();
  const yyyy = today.getFullYear();
  const mm = today.getMonth() + 1; // Months start at 0!
  const dd = today.getDate();
  let smm = mm + '';
  let sdd = dd + '';
  if (dd < 10) sdd = '0' + dd;
  if (mm < 10) smm = '0' + mm;

  const formattedToday = sdd + '/' + smm + '/' + yyyy;
  const formattedMonth = smm + '/' + yyyy;
  const {
    register,
    watch,
    control,
    formState: { errors }
  } = useForm<FormInput>({
    defaultValues: {
      year: new Date().getFullYear().toString(),
      month: formattedMonth,
      day: formattedToday,
      typeAccountDay: '',
      typeAccountMonth: '',
      typeAccountYear: ''
    }
  });
  const [activeTab, setActiveTab] = useState<string>('1');
  const [dataPie, setDataPie] = useState<{ labels: string[]; datasets: ChartDataset<'pie', number[]>[] }>();
  const [totalLabelPie, setTotalLabelPie] = useState<string[]>([]);
  const { data: dataInformation, isFetching: isLoadingDataInformation } = useQuery({
    queryKey: ['DataInformation'],
    queryFn: statisticApis.DataInformation
  });
  const sixMonthsBack = useMemo(() => {
    return dataInformation?.data?.Item?.sixMonthsBack;
  }, [dataInformation?.data?.Item?.sixMonthsBack]);
  const sixMonthsBackBTC = useMemo(() => {
    return dataInformation?.data?.Item?.sixMonthsBackBTC;
  }, [dataInformation?.data?.Item?.sixMonthsBackBTC]);
  const SumSoLuong = useMemo(() => {
    return dataInformation?.data?.Item?.SumSoLuong;
  }, [dataInformation?.data?.Item?.SumSoLuong]);
  const SumSoLuongBTC = useMemo(() => {
    return dataInformation?.data?.Item?.SumSoLuongBTC;
  }, [dataInformation?.data?.Item?.SumSoLuongBTC]);
  const { data: dataTTSach, isFetching: isLoadingdataTTSach } = useQuery({
    queryKey: ['TTSach', activeTab],
    queryFn: statisticApis.TTSach,
    enabled: activeTab === '4'
  });

  const year = watch('year');
  const month = watch('month');
  const day = watch('day');
  const typeAccountDay = watch('typeAccountDay');
  const typeAccountMonth = watch('typeAccountMonth');
  const typeAccountYear = watch('typeAccountYear');

  const { data: dataPhieuMuonTheoNam, isFetching: isLoadingdataPhieuMuonTheoNam } = useQuery({
    queryKey: ['BieuDoPhieuMuonTheoNam', year, typeAccountYear],
    queryFn: () => statisticApis.BieuDoPhieuMuonTheoNam(dayjs(year).year().toString(), typeAccountYear),
    enabled: activeTab === '3'
  });
  const { data: dataPhieuMuonTheoThang, isFetching: isLoadingdataPhieuMuonTheoThang } = useQuery({
    queryKey: ['BieuDoPhieuMuonTheoThang', year, month, typeAccountMonth],
    queryFn: () =>
      statisticApis.BieuDoPhieuMuonTheoThang(
        (dayjs(month, 'MM/YYYY').month() + 1).toString(),
        dayjs(month, 'MM/YYYY').year().toString(),
        typeAccountMonth
      ),
    enabled: activeTab === '2'
  });
  const { data: dataPhieuMuonTheoTuan, isFetching: isLoadingdataPhieuMuonTheoTuan } = useQuery({
    queryKey: ['dataPhieuMuonTheoTuan', day, typeAccountDay],
    queryFn: () => {
      let tempday;
      if (typeof day === 'string') tempday = day;
      else tempday = convertDate(day);
      return statisticApis.StartDayAndlastDay(tempday, typeAccountDay);
    },
    enabled: activeTab === '1'
  });

  const dataBar = useMemo(() => {
    const dataTemp = dataTTSach?.data?.Item;
    let result = [];
    let labels = [];
    let value = [];
    let prevValue = [];
    let sum = 0;
    for (let i = 0; i < dataTemp?.length; i += 2) {
      labels.push(dataTemp[i]);
      let unitValue = [...prevValue, dataTemp[i + 1]];
      sum += +dataTemp[i + 1];
      result.push({
        label: dataTemp[i],
        data: unitValue,
        backgroundColor: generateColor()
      });
      value.forEach((element) => {
        element.push(0);
      });
      prevValue.push(0);
      value.push(unitValue);
    }
    //data for pie chart
    let dataTotal = [];

    let percentTotal = [];
    let colorTotal = [];
    let orther = 0;

    for (let i = 0; i < result?.length; i++) {
      dataTotal.push(result[i].data[i]);
      if (i === result?.length - 1) percentTotal.push(100 - orther + '%');
      else percentTotal.push(Math.round((result[i].data[i] / sum) * 100) + '%');
      colorTotal.push(result[i].backgroundColor);
      orther += Math.round((result[i].data[i] / sum) * 100);
    }
    setTotalLabelPie(percentTotal);
    setDataPie({
      labels: labels,
      datasets: [
        {
          data: dataTotal,
          backgroundColor: colorTotal,
          borderColor: colorTotal,
          borderWidth: 1
        }
      ]
    });
    return {
      labels: labels,
      datasets: result
    };
  }, [dataTTSach?.data?.Item]);

  const dataLineYear = useMemo(() => {
    if (!dataPhieuMuonTheoNam) return;
    const dataTemp = dataPhieuMuonTheoNam?.data.Item;

    const labels = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];
    return {
      labels,
      datasets: [
        {
          label: 'Số bạn đọc mượn về nhà',
          data: dataTemp.lsoNguoiKhongTraTrongNam,
          borderColor: '#ab95e1',
          backgroundColor: '#ab95e1'
        },
        {
          label: 'Số bạn đọc tại chỗ',
          data: dataTemp.lsoNguoiMuonSachTaiChoTrongNam,
          borderColor: '#24282D',
          backgroundColor: '#24282D'
        },
        {
          label: 'Số lượt mượn sách',
          data: dataTemp.lsoSachDuocMuonTrongNam,
          borderColor: '#5dc9af',
          backgroundColor: '#5dc9af'
        },
        {
          label: 'Số lượt trả sách',
          data: dataTemp.lsoSachDuocTraTrongNam,
          borderColor: '#69afe1',
          backgroundColor: '#69afe1'
        },
        {
          label: 'Số sách trễ hạn',
          data: dataTemp.lsoNguoiTraTreTrongNam,
          borderColor: '#ec9970',
          backgroundColor: '#ec9970'
        },
        {
          label: 'Số người trả sách',
          data: dataTemp.soNguoiTraSachTrongNam,
          borderColor: '#f7c86a',
          backgroundColor: '#f7c86a'
        }
      ]
    };
  }, [dataPhieuMuonTheoNam]);

  const dataLineMonth = useMemo(() => {
    if (!dataPhieuMuonTheoThang) return;
    const dataTemp = dataPhieuMuonTheoThang?.data.Item;
    const SoNgayTrongThang = dataTemp.SoNgayTrongThang;
    let labels = [];
    for (let i = 1; i <= SoNgayTrongThang; i++) {
      labels.push(i);
    }
    return {
      labels,
      datasets: [
        {
          label: 'Số bạn đọc mượn về nhà',
          data: dataTemp.lsoNguoiKhongTraTrongNgay.filter((_, index) => {
            return index !== 0;
          }),
          borderColor: '#ab95e1',
          backgroundColor: '#ab95e1'
        },
        {
          label: 'Số bạn đọc tại chỗ',
          data: dataTemp.lsoNguoiMuonTaiChoTrongNgay.filter((_, index) => {
            return index !== 0;
          }),
          borderColor: '#24282D',
          backgroundColor: '#24282D'
        },
        {
          label: 'Số lượt mượn sách',
          data: dataTemp.lsoSachDuocMuonTrongNgay.filter((_, index) => {
            return index !== 0;
          }),
          borderColor: '#5dc9af',
          backgroundColor: '#5dc9af'
        },
        {
          label: 'Số lượt trả sách',
          data: dataTemp.lsoSachDuocTraTrongNgay.filter((_, index) => {
            return index !== 0;
          }),
          borderColor: '#69afe1',
          backgroundColor: '#69afe1'
        },
        {
          label: 'Số sách trễ hạn',
          data: dataTemp.lsoNguoiTraTreTrongNgay.filter((_, index) => {
            return index !== 0;
          }),
          borderColor: '#ec9970',
          backgroundColor: '#ec9970'
        },
        {
          label: 'Số người trả sách',
          data: dataTemp.lsoNguoiTraTrongNgay.filter((_, index) => {
            return index !== 0;
          }),
          borderColor: '#f7c86a',
          backgroundColor: '#f7c86a'
        }
      ]
    };
  }, [dataPhieuMuonTheoThang]);

  const dataLineDay = useMemo(() => {
    if (!dataPhieuMuonTheoTuan) return;
    const dataTemp = dataPhieuMuonTheoTuan?.data.Item;
    const labels = dataTemp.ListNgayTrongTuan.map((element: string) => {
      return convertDate(new Date(element));
    });
    return {
      labels,
      datasets: [
        {
          label: 'Số bạn đọc mượn về nhà',
          data: dataTemp.lsoNguoiMuonTrongTuan,
          borderColor: '#ab95e1',
          backgroundColor: '#ab95e1'
        },
        {
          label: 'Số bạn đọc tại chỗ',
          data: dataTemp.lsoNguoiMuonTaiChoTrongTuan,
          borderColor: '#24282D',
          backgroundColor: '#24282D'
        },
        {
          label: 'Số lượt mượn sách',
          data: dataTemp.lsoSachDuocMuonTrongTuan,
          borderColor: '#5dc9af',
          backgroundColor: '#5dc9af'
        },
        {
          label: 'Số lượt trả sách',
          data: dataTemp.lsoSachDuocTraTrongTuan,
          borderColor: '#69afe1',
          backgroundColor: '#69afe1'
        },
        {
          label: 'Số sách trễ hạn',
          data: dataTemp.lsoSachKhongTraTrongTuan,
          borderColor: '#ec9970',
          backgroundColor: '#ec9970'
        },
        {
          label: 'Số người trả sách',
          data: dataTemp.lsoNguoiTraSachTrongTuan,
          borderColor: '#f7c86a',
          backgroundColor: '#f7c86a'
        }
      ]
    };
  }, [dataPhieuMuonTheoTuan]);

  const items: TabsProps['items'] = [
    {
      key: '1',
      label: `Thống kê tuần`,
      children: (
        <div className='flex h-[100%] flex-col '>
          <div className='flex w-full shrink-0 flex-row justify-end'>
            <Select items={TYPE_READER_OPTIONS} className='mr-8 w-[250px]' register={register} name='typeAccountDay' />
            <DatePicker
              style={{
                width: '250px'
              }}
              name='day'
              placeholder='Nhập ngày'
              control={control}
              disabledDate={(current) => {
                return current && current > dayjs().endOf('day');
              }}
              errorMessage={errors?.month?.message}
            />
          </div>
          {dataLineDay && (
            <div style={{ width: '100%', height: '100%' }}>
              <Line
                options={{
                  maintainAspectRatio: false,
                  responsive: true,
                  plugins: {
                    legend: {
                      position: 'top' as const
                    },
                    title: {
                      display: false
                    }
                  },
                  scales: {
                    x: {
                      min: 0,
                      title: {
                        text: 'Ngày',
                        display: true,
                        font: {
                          size: 16,
                          weight: '700'
                        }
                      }
                    },
                    y: {
                      min: 0,
                      title: {
                        text: 'Số lượng',
                        display: true,
                        font: {
                          size: 16,
                          weight: '700'
                        }
                      }
                    }
                  }
                }}
                data={dataLineDay}
              />
            </div>
          )}
        </div>
      )
    },
    {
      key: '2',
      label: `Thống kê tháng`,
      children: (
        <div className='flex h-[100%] flex-col'>
          <div className='flex w-full shrink-0 flex-row justify-end'>
            <Select
              items={TYPE_READER_OPTIONS}
              className='mr-8 w-[250px]'
              register={register}
              name='typeAccountMonth'
            />
            <DatePicker
              style={{
                width: '250px'
              }}
              name='month'
              picker='month'
              placeholder='Nhập tháng'
              control={control}
              disabledDate={(current) => {
                return current && current > dayjs().endOf('day');
              }}
              errorMessage={errors?.month?.message}
            />
          </div>
          {dataLineMonth && (
            <div style={{ width: '100%', height: '100%' }} className='grow'>
              <Line
                options={{
                  responsive: true,
                  maintainAspectRatio: false,
                  plugins: {
                    legend: {
                      title: { padding: 100 },
                      position: 'top' as const
                    },
                    title: {
                      display: false
                    }
                  },
                  scales: {
                    x: {
                      min: 0,
                      title: {
                        text: 'Ngày',
                        display: true,
                        font: {
                          size: 16,
                          weight: '700'
                        }
                      }
                    },
                    y: {
                      min: 0,
                      title: {
                        text: 'Số lượng',
                        display: true,
                        font: {
                          size: 16,
                          weight: '700'
                        }
                      }
                    }
                  }
                }}
                data={dataLineMonth}
              />
            </div>
          )}
        </div>
      )
    },
    {
      key: '3',
      label: `Thống kê năm`,
      children: (
        <div className='flex h-[100%] flex-col'>
          <div className='flex w-full shrink-0 flex-row justify-end'>
            <Select items={TYPE_READER_OPTIONS} className='mr-8 w-[250px]' register={register} name='typeAccountYear' />
            <DatePicker
              style={{
                width: '250px'
              }}
              name='year'
              picker='year'
              placeholder='Nhập năm'
              control={control}
              disabledDate={(current) => {
                return current && current > dayjs().endOf('day');
              }}
              errorMessage={errors?.year?.message}
            />
          </div>
          {dataLineYear && (
            <div style={{ width: '100%', height: '100%' }}>
              <Line
                options={{
                  responsive: true,
                  maintainAspectRatio: false,
                  plugins: {
                    legend: {
                      position: 'top' as const
                    },
                    title: {
                      display: false
                    }
                  },
                  scales: {
                    x: {
                      min: 0,
                      title: {
                        text: 'Tháng',
                        display: true,
                        font: {
                          size: 16,
                          weight: '700'
                        }
                      }
                    },
                    y: {
                      min: 0,
                      title: {
                        text: 'Số lượng',
                        display: true,
                        font: {
                          size: 16,
                          weight: '700'
                        }
                      }
                    }
                  }
                }}
                data={dataLineYear}
              />
            </div>
          )}
        </div>
      )
    },
    {
      key: '4',
      label: `Thống kê tình trạng`,
      children: (
        <div className='flex h-[100%] flex-row gap-2 pt-8'>
          <div className='flex h-full grow flex-col'>
            <span className='block w-full shrink-0 text-center text-xl text-[#3472A2]'>
              Biểu đồ số lượng sách theo tình trạng
            </span>
            <div style={{ width: '100%', height: '100%' }} className='grow'>
              <Bar
                options={{
                  responsive: true,
                  maintainAspectRatio: false,
                  plugins: {
                    datalabels: {
                      display: false
                    },
                    legend: {
                      display: false
                    },
                    title: {
                      display: false
                    }
                  },
                  scales: {
                    x: {
                      stacked: true,
                      title: {
                        text: 'Tình trạng',
                        display: true,
                        font: {
                          size: 16,
                          weight: '700'
                        }
                      }
                    },
                    y: {
                      title: {
                        text: 'Số lượng',
                        display: true,
                        font: {
                          size: 16,
                          weight: '700'
                        }
                      }
                    }
                  }
                }}
                data={dataBar}
              />
            </div>
          </div>
          <div className='h-full w-[80px] shrink-0'></div>
          <div className='flex w-[25%] shrink-0 flex-col'>
            <span className='block w-full shrink-0 text-center text-xl text-[#3472A2]'>
              Biểu đồ số lượng sách theo tình trạng
            </span>
            <div className='flex grow items-center'>
              {dataPie && (
                <Pie
                  options={{
                    responsive: true,
                    plugins: {
                      datalabels: {
                        color: '#fdfdfe',
                        anchor: 'end',
                        align: 'start',
                        clamp: false,
                        formatter: function (value, context) {
                          return totalLabelPie[context.dataIndex];
                        },
                        font: {
                          size: 16
                        }
                      },
                      legend: {
                        display: false
                      },
                      title: {
                        display: false
                      }
                    }
                  }}
                  data={dataPie}
                />
              )}
            </div>
          </div>
        </div>
      )
    }
  ];

  const handleChangeTab = (activeTab: string) => {
    setActiveTab(activeTab);
  };

  return (
    <div className='flex flex-col p-5' id='static-page' style={{ height: '100%' }}>
      <Title title='BIỂU ĐỒ THỐNG KÊ' className='shrink-0' />
      <div className='my-[10px] flex shrink-0 flex-col flex-wrap gap-2 md:flex-row'>
        <div className='h-[100px] w-[250px] bg-[#3472A2] py-[18px] text-center'>
          <span className='text-xl text-white'>Tổng số sách</span>
          <div className='h-[8px]' />
          <span className='text-xl font-bold text-white'>{SumSoLuong}</span>
        </div>
        <div className='h-[100px] w-[250px] bg-[#F35B3F] py-[18px] text-center'>
          <span className='text-xl text-white'>Tổng số sách mới</span>
          <div className='h-[8px]' />
          <span className='text-xl font-bold text-white'>{sixMonthsBack}</span>
        </div>
        <div className='h-[100px] w-[250px] bg-[#119757] py-[18px] text-center'>
          <span className='text-xl text-white'>Tổng số báo, tạp chí</span>
          <div className='h-[8px]' />
          <span className='text-xl font-bold text-white'>{SumSoLuongBTC}</span>
        </div>
        <div className='h-[100px] w-[250px] bg-[#D9C510] py-[18px] text-center'>
          <span className='text-xl text-white'>Tổng số báo, tạp chí mới</span>
          <div className='h-[8px]' />
          <span className='text-xl font-bold text-white'>{sixMonthsBackBTC}</span>
        </div>
      </div>
      <Tabs className='grow' items={items} activeKey={activeTab} onChange={handleChangeTab} />
      <Loading
        open={
          isLoadingDataInformation ||
          (isLoadingdataTTSach && dataTTSach === undefined) ||
          (isLoadingdataPhieuMuonTheoNam && dataPhieuMuonTheoNam === undefined) ||
          (isLoadingdataPhieuMuonTheoThang && dataPhieuMuonTheoThang === undefined) ||
          (isLoadingdataPhieuMuonTheoTuan && dataPhieuMuonTheoTuan === undefined)
        }
      ></Loading>
    </div>
  );
};

export default StatisticalManagermentContent;
