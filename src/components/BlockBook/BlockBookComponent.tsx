import React, { BlockquoteHTMLAttributes } from 'react';

type Props = BlockquoteHTMLAttributes<any>;

function BlockBookComponent(props: Props) {
  const { title } = props;
  return (
    <div className='home-menu-group'>
      <div className='title'>${title}</div>
      <div className='btntag-box'></div>
    </div>
  );
}

export default BlockBookComponent;
