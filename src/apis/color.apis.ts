import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/MaMau`;

type GetColorVars = {
  Keyword: string;
  Sort: number;
};

const colorApis = {
  getColors: (payload: GetColorVars) => {
    const { Keyword, Sort } = payload;
    return httpThuVien.post<ResponseApi<Color[]>>(`${endPoint}/GetDataBySearch`, {
      Keyword,
      Sort
    });
  },

  getColor: (id: string) => httpThuVien.get<ResponseApi<Color>>(`${endPoint}/${id}`),
  deleteColor: (id: string) => httpThuVien.delete<ResponseApi<{}>>(`${endPoint}/${id}`),
  createColor: (
    payload: Omit<Color, 'Id' | 'TenLienKet' | 'TenKieuLienKet' | 'ColorList'> & { ListColor: { Name: string }[] }
  ) => httpThuVien.post<ResponseApi<{}>>(`${endPoint}`, payload),
  updateColor: (payload: Pick<Color, 'Id' | 'IdLienKet' | 'KieuLienKet' | 'ListColor' | 'TenMau'>) =>
    httpThuVien.put<ResponseApi<{}>>(`${endPoint}`, payload)
};

export default colorApis;
