import { DownloadOutlined } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Table } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { statistic } from 'apis';
import classNames from 'classnames';
import { Button, Empty, Loading, SizeChanger, Title } from 'components';
import { path } from 'constants/path';
import { usePaginationNavigate, useQueryParams } from 'hooks';
import { isEmpty, omitBy, uniqueId } from 'lodash';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

const NumberRegisterationUnique = () => {
  const navigate = useNavigate();
  const queryConfig = useQueryParams();
  const handleNavigation = usePaginationNavigate();
  const { state } = useLocation();
  const [TotalCount, setTotalCount] = useState(0);

  const Month = state ? state.Month : queryConfig['Month'] ? queryConfig['Month'] : undefined;

  const Year = state ? state.Year : queryConfig['Year'] ? queryConfig['Year'] : undefined;

  const IdKhoSach = state ? state.IdKhoSach : queryConfig['IdKhoSach'] ? queryConfig['IdKhoSach'] : '';

  const RangeYear = useMemo(() => {
    let result = [];
    for (
      var i = Math.floor((Year || new Date().getFullYear()) / 10) * 10 + 1;
      i <= Math.round((Year || new Date().getFullYear()) / 10) * 10 + 5;
      i++
    )
      result.push(i);
    return result;
  }, [Year]);

  const { data, isLoading } = useQuery({
    queryFn: () =>
      statistic.ViewA4SoDangKyCaBiet_GetData_Kho({
        Month: Month,
        Year: Year,
        IdKhoSach: IdKhoSach,
        CurrentPage: queryConfig.page,
        PageSize: queryConfig.pageSize
      }),
    queryKey: ['ViewA4SoDangKyCaBiet_GetData_Kho', Month, Year, IdKhoSach, queryConfig]
  });

  const { mutate: DownSoDangKyCaBiet_Kho, isLoading: loadingDownSoDangKyCaBiet_Kho } = useMutation({
    mutationFn: () => {
      let param: { Month?: string; Year?: string; IdKhoSach: string } = {
        Month: Month + '',
        Year: Year + '',
        IdKhoSach: IdKhoSach
      };
      if (!Month) delete param.Month;
      if (!Year) delete param.Year;
      return statistic.DownSoDangKyCaBiet_Kho(param);
    },
    onSuccess: (data) => {
      toast.success('Tải sổ tổng quát thành công');
      const blob = new Blob([data.data, { type: 'application/xls' }]);
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = 'SoDKTQ_Kho.xls';
      link.click();
    }
  });

  const dataRow = useMemo(() => {
    if (!data?.data.Item.List_SoDangKyCaBietModel) return [];
    setTotalCount(data?.data.Item.TotalCount);
    return data?.data.Item.List_SoDangKyCaBietModel;
  }, [data?.data.Item]);
  const columns: ColumnsType<any> = [
    { key: 'NgayVaoSo', dataIndex: 'NgayVaoSo', title: 'Ngày vào sổ (1)' },
    {
      key: 'MaKiemSoat',
      dataIndex: 'MaKiemSoat',
      title: 'Số TT tên sách (2)'
    },
    {
      key: 'MaSachCB',
      dataIndex: 'MaSachCB',
      title: 'Số TT bản sách (3)'
    },
    {
      key: 'TenSach',
      dataIndex: 'TenSach',
      title: 'Tên tác giả và tên sách (4)',
      colSpan: 2
    },
    {
      key: 'TenTacGia',
      dataIndex: 'TenTacGia',
      title: '',
      colSpan: 0
    },
    {
      key: 'ThongTinXuatban',
      dataIndex: 'ThongTinXuatban',
      title: 'Thông tin xuất bản (5)',
      children: [
        { key: 'TenNXB', dataIndex: 'TenNXB', title: 'Nhà XB' },
        { key: 'NoiXuatBan', dataIndex: 'NoiXuatBan', title: 'Nơi XB' },
        { key: 'NoiXuatBan', dataIndex: 'NoiXuatBan', title: 'Năm XB' }
      ]
    },
    {
      key: 'DonGia',
      dataIndex: 'DonGia',
      title: 'Đơn giá (6)',
      children: [
        { key: 'PhatKhong', dataIndex: 'PhatKhong', title: 'Phát không' },
        { key: 'Mua', dataIndex: 'Mua', title: 'Mua' }
      ]
    },
    { key: 'MonLoai', dataIndex: 'MonLoai', title: 'Môn loại (7)' },
    { key: 'SoVaoSoTQ', dataIndex: 'SoVaoSoTQ', title: 'Số vào sổ tổng quát (8)' },
    { key: 'NgayVaSoBBX', dataIndex: 'NgayVaSoBBX', title: 'Ngày vào sổ biên bản xuất (9)' },
    {
      key: 'KiemKe',
      dataIndex: 'KiemKe',
      title: 'Kiểm kê (10)',
      children:
        RangeYear.map((element) => {
          return {
            key: element,
            dataIndex: element,
            title: element,
            render: (value, record, index) => {
              if (+element < +(Year || new Date().getFullYear())) return '';
              if (+element > new Date().getFullYear()) return '';
              if (record.KiemKe20 && record.KiemKe20 !== '' && +record.KiemKe20 <= +element) return 0;
              return '+';
            }
          };
        }) || []
    },
    { key: 'Ghi chú', dataIndex: 'GhiChu', title: 'Ghi chú (11)' }
  ];

  const onPaginate = useCallback(
    (page: number, pageSize: number) => {
      handleNavigation(
        omitBy(
          {
            ...queryConfig,
            page: page + '',
            pageSize: pageSize + '',
            Month: Month ? Month + '' : undefined,
            Year: Year ? Year + '' : undefined,
            IdKhoSach: IdKhoSach
          },
          isEmpty
        )
      );
    },
    [IdKhoSach, Month, Year, handleNavigation, queryConfig]
  );

  const pagination = useMemo(() => {
    return {
      current: (!!queryConfig.page && +queryConfig.page) || 1,
      pageSize: (!!queryConfig.pageSize && +queryConfig.pageSize) || 30,
      total: TotalCount,
      onChange: onPaginate,
      hideOnSinglePage: true,
      showSizeChanger: false,
      showQuickJumper: true,
      locale: { jump_to: '', page: '' }
    };
  }, [TotalCount, onPaginate, queryConfig.page, queryConfig.pageSize]);
  return (
    <div className='py-5'>
      <Title title='Sổ đăng ký cá biệt' />
      <div className='mt-3'>
        <Table
          columns={columns}
          className='custom-table'
          bordered
          scroll={{ x: true }}
          locale={{
            emptyText: () => <Empty />
          }}
          dataSource={dataRow}
          pagination={pagination}
          rowKey={(_) => uniqueId()}
        />
        <div
          className={classNames('relative', {
            // @ts-ignore
            'mt-[64px]': ((!!queryConfig.pageSize && +queryConfig.pageSize) || 30) >= TotalCount
          })}
        >
          <div className='absolute bottom-1'>
            <SizeChanger
              visible={!!dataRow?.length}
              value={queryConfig.pageSize}
              total={TotalCount.toString()}
              onChange={(pageSize) => {
                handleNavigation(
                  omitBy(
                    {
                      ...queryConfig,
                      pageSize: pageSize + '',
                      Month: Month,
                      Year: Year,
                      IdKhoSach: IdKhoSach
                    },
                    isEmpty
                  )
                );
              }}
            />
          </div>
        </div>
      </div>
      <div className='mt-3 flex items-center justify-end gap-2'>
        <Button
          variant='secondary'
          onClick={() => navigate(path.thongkexuatbaocao)}
          loading={loadingDownSoDangKyCaBiet_Kho}
        >
          Quay về
        </Button>

        <Button onClick={() => DownSoDangKyCaBiet_Kho()} loading={loadingDownSoDangKyCaBiet_Kho}>
          <DownloadOutlined />
          Tải xuống
        </Button>
      </div>
      <Loading open={isLoading}></Loading>
    </div>
  );
};

export default NumberRegisterationUnique;
