import { useMutation } from '@tanstack/react-query';
import { Radio, RadioChangeEvent, Table, Tag, Tooltip } from 'antd';
import { type ColumnsType } from 'antd/es/table';
import { connectionApis } from 'apis';
import { type GetListOfKeysVar } from 'apis/connection.api';
import { BaseModal, Button, Input, Select, Title } from 'components';
import { KEY_STATUS_OPTION } from 'constants/options';
import dayjs from 'dayjs';
import { useStateAsync } from 'hooks';
import { ChangeEvent, FormEvent, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getSerialNumber } from 'utils/utils';
import { ExtendHistoryModal, KeyConfigurationModal } from './components';

const ListOfKeys = () => {
  const [params, setParams] = useStateAsync<{ page: number; pageSize: number; unitName: string; keyStatus: string }>({
    page: 1,
    pageSize: 30,
    unitName: '',
    keyStatus: ''
  });

  const [selectedRecord, setSelectedRecord] = useState<UnitKey>();

  const {
    mutateAsync: getListOfUnitKeys,
    isLoading: loadingGetListOfUnitKey,
    data: listOfUnitKeysData
  } = useMutation({
    mutationFn: () => {
      const { page, pageSize, keyStatus, unitName } = params;
      const reqBody: Omit<GetListOfKeysVar, 'page' | 'pageSize'> = {};

      if (unitName) {
        reqBody.TextForSearch = unitName;
      }

      if (keyStatus) {
        switch (keyStatus) {
          case 'active':
            reqBody.Available = true;
            break;

          case 'inactive':
            reqBody.Available = false;
            break;

          default:
            break;
        }
      }

      return connectionApis.getListOfUnitKeys({ page, pageSize, ...reqBody });
    }
  });

  const navigate = useNavigate();

  const { Key, Count } = listOfUnitKeysData?.data.Item || { Key: [], Count: 0 };

  useEffect(() => {
    getListOfUnitKeys();
  }, []);

  const colums: ColumnsType<UnitKey> = [
    {
      key: 'STT',
      title: 'STT',
      render: (value, record, index) => getSerialNumber(params.page, params.pageSize, index),
      width: 80
    },
    {
      key: 'Value',
      title: 'Key',
      dataIndex: 'Value',
      onCell: (record) => ({
        className: 'text-left'
      }),
      render: (value, { Value }) =>
        Value.length > 60 ? (
          <Tooltip placement='topLeft' title={Value} arrow={true}>
            <p
              className='flex items-center gap-1 hover:cursor-pointer'
              onClick={async () => {
                if ('clipboard' in navigator) {
                  toast.success('Copy thành công');
                  return await navigator.clipboard.writeText(Value);
                } else {
                  toast.success('Copy thành công');
                  return document.execCommand('copy', true, Value);
                }
              }}
            >
              {' '}
              <svg width='12' height='14' viewBox='0 0 12 14' fill='none' xmlns='http://www.w3.org/2000/svg'>
                <path
                  d='M8.8421 0H1.26316C0.565263 0 0 0.569545 0 1.27273V10.1818H1.26316V1.27273H8.8421V0ZM10.7368 2.54545H3.78947C3.09158 2.54545 2.52632 3.115 2.52632 3.81818V12.7273C2.52632 13.4305 3.09158 14 3.78947 14H10.7368C11.4347 14 12 13.4305 12 12.7273V3.81818C12 3.115 11.4347 2.54545 10.7368 2.54545ZM10.7368 12.7273H3.78947V3.81818H10.7368V12.7273Z'
                  fill='#1D396A'
                />
              </svg>{' '}
              {Value.substring(0, 60).concat('...')}
            </p>
          </Tooltip>
        ) : (
          <>
            <p className=''>
              {' '}
              <svg width='12' height='14' viewBox='0 0 12 14' fill='none' xmlns='http://www.w3.org/2000/svg'>
                <path
                  d='M8.8421 0H1.26316C0.565263 0 0 0.569545 0 1.27273V10.1818H1.26316V1.27273H8.8421V0ZM10.7368 2.54545H3.78947C3.09158 2.54545 2.52632 3.115 2.52632 3.81818V12.7273C2.52632 13.4305 3.09158 14 3.78947 14H10.7368C11.4347 14 12 13.4305 12 12.7273V3.81818C12 3.115 11.4347 2.54545 10.7368 2.54545ZM10.7368 12.7273H3.78947V3.81818H10.7368V12.7273Z'
                  fill='#1D396A'
                />
              </svg>{' '}
              {Value}
            </p>
          </>
        ),
      width: 200
    },
    {
      key: 'NameMember',
      title: 'Tên đơn vị',
      dataIndex: 'NameMember',
      onCell: (record) => ({
        className: 'text-left'
      }),
      render: (value, { NameMember }) =>
        NameMember.length > 60 ? (
          <Tooltip placement='topLeft' title={NameMember} arrow={true}>
            <p className=''>{NameMember.substring(0, 60).concat('...')}</p>
          </Tooltip>
        ) : (
          <p className=''>{NameMember}</p>
        ),
      width: 300
    },
    {
      key: 'CreateDateTime',
      title: 'Thời gian tạo key',
      dataIndex: 'CreateDateTime',
      render: (value, record) => dayjs(record.CreateDateTime).format('HH:mm DD/MM/YYYY')
    },
    {
      key: 'EffectiveTime',
      title: 'Thời gian hết hiệu lực',
      dataIndex: 'EffectiveTime',
      render: (value, record) => (!record.EffectiveTime.includes('/') ? 'Vĩnh viễn' : record.EffectiveTime)
    },
    {
      key: 'Trạng thái key',
      title: 'Trạng thái key',
      render: (value, { IsActive }) =>
        IsActive ? <Tag color='green'>Đang hoạt động</Tag> : <Tag color='red'>Ngưng hoạt động</Tag>
    },
    {
      key: 'history',
      title: '',
      render: (value, record) => (
        <button onClick={() => setSelectedRecord(record)}>
          <svg width='17' height='17' viewBox='0 0 17 17' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path
              d='M17 8.48394C17.0087 13.1672 13.1894 16.9967 8.50616 17C6.48347 17.0014 4.62554 16.2963 3.16549 15.118C2.78584 14.8116 2.75746 14.2426 3.10246 13.8976L3.48863 13.5114C3.7837 13.2163 4.25476 13.184 4.58167 13.4434C5.65736 14.297 7.0187 14.8065 8.49999 14.8065C11.9858 14.8065 14.8064 11.9853 14.8064 8.50002C14.8064 5.01415 11.9853 2.19355 8.49999 2.19355C6.82693 2.19355 5.30739 2.8437 4.17912 3.90493L5.91867 5.64449C6.26415 5.98997 6.01947 6.58066 5.53092 6.58066H0.548387C0.245506 6.58066 0 6.33515 0 6.03227V1.04972C0 0.561172 0.590681 0.316488 0.936164 0.661939L2.62835 2.35413C4.15434 0.895792 6.22254 0 8.49999 0C13.189 0 16.9913 3.7969 17 8.48394ZM10.7994 11.1842L11.1361 10.7513C11.415 10.3927 11.3504 9.87589 10.9918 9.597L9.59677 8.51198V4.93549C9.59677 4.48119 9.22849 4.11291 8.77419 4.11291H8.2258C7.7715 4.11291 7.40322 4.48119 7.40322 4.93549V9.58483L9.64506 11.3285C10.0037 11.6074 10.5205 11.5428 10.7994 11.1842Z'
              fill='#3472A2'
            />
          </svg>
        </button>
      )
    }
  ];

  const handleChangeUnitName = (e: ChangeEvent<HTMLInputElement>) => {
    if (!e) return;

    const { value } = e.target;

    setParams((prevState) => {
      return { ...prevState, unitName: value };
    });
  };

  const handleChangeKeyStatus = (e: ChangeEvent<HTMLSelectElement>) => {
    if (!e) return;

    const { value } = e.target;

    setParams((prevState) => {
      return { ...prevState, keyStatus: value };
    });
  };

  const handleSearch = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    getListOfUnitKeys();
  };

  return (
    <div className='p-5'>
      <Title title='Danh sách Key' />

      <Button variant='secondary' onClick={() => navigate(-1)} className='mt-3'>
        Quay về
      </Button>

      <form className='my-3 w-full bg-primary-50/20 p-2.5' onSubmit={handleSearch}>
        <div className='flex w-full flex-1 flex-col items-center gap-1 md:flex-row'>
          <Input placeholder={'Nhập tên đơn vị'} containerClassName='w-full' onChange={handleChangeUnitName} />

          <Select
            items={KEY_STATUS_OPTION}
            className='w-full'
            onChange={handleChangeKeyStatus}
            value={params.keyStatus}
          />

          <Button
            variant='secondary'
            type='button'
            className='w-full flex-shrink-0 flex-grow md:w-auto'
            onClick={async () => {
              await setParams({
                unitName: '',
                keyStatus: '',
                page: 1,
                pageSize: 30
              });

              await getListOfUnitKeys();
            }}
          >
            Làm mới
          </Button>

          <Button variant='default' type='submit' className='w-full flex-shrink-0 flex-grow md:w-auto'>
            Tìm kiếm
          </Button>
        </div>
      </form>

      <Table
        className='custom-table my-3'
        bordered
        columns={colums}
        loading={loadingGetListOfUnitKey}
        dataSource={Key}
        rowKey={(row) => row.NameMember}
        pagination={{
          current: params.page || 1,
          pageSize: params.pageSize || 30,
          total: Count,
          onChange: (page) => {
            setParams((prevState) => {
              return { ...prevState, page };
            });
          },
          hideOnSinglePage: true,
          showSizeChanger: false,
          showQuickJumper: true,
          locale: { jump_to: '', page: '' }
        }}
      />

      <ExtendHistoryModal
        onCancel={() => setSelectedRecord(undefined)}
        selectedRecord={selectedRecord as UnitKey}
        onSuccess={() => {
          getListOfUnitKeys();
        }}
      />
    </div>
  );
};

export default ListOfKeys;
