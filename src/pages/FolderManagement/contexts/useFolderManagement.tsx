import { BookInFolder } from 'apis/folder.apis';
import { Dispatch, ReactNode, SetStateAction, createContext, useContext, useState } from 'react';
import { Menu } from '../components/Toolbar';

type ViewMode = 'listing' | 'grid';

type FolderManagementValue = {
  viewMode: ViewMode;
  setViewMode: Dispatch<SetStateAction<ViewMode>>;

  selectedBook?: BookInFolder;
  setSelectedBook: Dispatch<SetStateAction<BookInFolder | undefined>>;

  isPreviewMode: boolean;
  setIsPreviewMode: Dispatch<SetStateAction<boolean>>;

  // Context menu
  contextBook: {
    book?: BookInFolder;
    x: number;
    y: number;
  };
  setContextBook: Dispatch<
    SetStateAction<{
      book?: BookInFolder | undefined;
      x: number;
      y: number;
    }>
  >;

  // active folder
  activeFolder?: Menu;
  setActiveFolder: Dispatch<SetStateAction<Menu | undefined>>;

  // selected book ids
  selectedBookIds: string[];
  setSelectedBookIds: Dispatch<SetStateAction<string[]>>;

  // map selected book ids
  // currently we havent used below state,
  // but in the nearest feture, I believe that it will be useful. trust me @>
  mapSelectedBookIds: Record<string, string[]>;
  setMapSelectedBookIds: Dispatch<SetStateAction<Record<string, string[]>>>;
};

export const ViewModeContext = createContext<FolderManagementValue>({} as FolderManagementValue);

const initialFolderMangement: FolderManagementValue = {
  viewMode: 'listing',
  isPreviewMode: true,
  contextBook: {
    book: undefined,
    x: 0,
    y: 0
  },
  selectedBookIds: [],
  mapSelectedBookIds: {} as FolderManagementValue['mapSelectedBookIds'],
  setViewMode: () => null,
  setSelectedBook: () => null,
  setIsPreviewMode: () => null,
  setContextBook: () => null,
  setActiveFolder: () => null,
  setSelectedBookIds: () => null,
  setMapSelectedBookIds: () => null
};

export function ViewModeProvider({ children }: { children: ReactNode }) {
  const [viewMode, setViewMode] = useState<ViewMode>(initialFolderMangement.viewMode);
  const [selectedBook, setSelectedBook] = useState<BookInFolder>();
  const [isPreviewMode, setIsPreviewMode] = useState<boolean>(initialFolderMangement.isPreviewMode);
  const [contextBook, setContextBook] = useState<FolderManagementValue['contextBook']>(
    initialFolderMangement.contextBook
  );
  const [activeFolder, setActiveFolder] = useState<Menu>();
  const [selectedBookIds, setSelectedBookIds] = useState<string[]>([]);
  const [mapSelectedBookIds, setMapSelectedBookIds] = useState<FolderManagementValue['mapSelectedBookIds']>(
    initialFolderMangement.mapSelectedBookIds
  );

  return (
    <ViewModeContext.Provider
      value={{
        viewMode,
        setViewMode,

        selectedBook,
        setSelectedBook,

        isPreviewMode,
        setIsPreviewMode,

        contextBook,
        setContextBook,

        activeFolder,
        setActiveFolder,

        selectedBookIds,
        setSelectedBookIds,

        mapSelectedBookIds,
        setMapSelectedBookIds
      }}
    >
      {children}
    </ViewModeContext.Provider>
  );
}

export function useFolderManagement() {
  return useContext(ViewModeContext);
}
