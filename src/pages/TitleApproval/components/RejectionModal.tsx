import { useMutation } from '@tanstack/react-query';
import { Modal } from 'antd';
import { approvalApis } from 'apis';
import { Button, Input } from 'components';
import { useState, type ChangeEvent } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

type Props = {
  open: boolean;
  onCancel: VoidFunction;
  params: {
    IdSubmit: string;
    IdBook: string;
    ListParam: string[];
    ListMedia: string[];
    LinkBiaSach: string;
    FailMessage?: string;
  };
};

const RejectionModal = (props: Props) => {
  const { open, params, onCancel } = props;

  const [value, setValue] = useState<string>('');

  const navigate = useNavigate();

  const { mutate, isLoading } = useMutation({
    mutationFn: () => approvalApis.approveDocument({ ...params, FailMessage: value }),
    onSuccess: () => {
      toast.success(`Đã từ chối phê duyệt biểu ghi`);
      setValue('');
      navigate(-1);
    }
  });

  const handleChangeReason = (event: ChangeEvent<HTMLInputElement>) => {
    if (!event) return;

    setValue(event.target.value);
  };

  const handleSubmit = () => {
    if (!value) return;

    mutate();
  };

  return (
    <Modal open={open} centered closable={false} footer={null}>
      <div className='flex w-full flex-col items-center justify-center gap-2'>
        <svg width={124} height={124} viewBox='0 0 124 124' fill='none' xmlns='http://www.w3.org/2000/svg'>
          <path
            d='M55.8 80.6H68.2V93H55.8V80.6ZM55.8 31H68.2V68.2H55.8V31ZM61.938 0C27.714 0 0 27.776 0 62C0 96.224 27.714 124 61.938 124C96.224 124 124 96.224 124 62C124 27.776 96.224 0 61.938 0ZM62 111.6C34.596 111.6 12.4 89.404 12.4 62C12.4 34.596 34.596 12.4 62 12.4C89.404 12.4 111.6 34.596 111.6 62C111.6 89.404 89.404 111.6 62 111.6Z'
            fill='#D9C510'
          />
        </svg>

        <p className='whitespace-nowrap text-[20px]'>
          Bạn có chắc chắn muốn từ chối biểu ghi này không? <span className='text-danger'>*</span>
        </p>

        <Input
          component={'textarea'}
          containerClassName='w-full'
          placeholder='Nhập lý do từ chối'
          onChange={handleChangeReason}
          value={value}
        />

        <div className='flex items-center justify-center gap-2'>
          <Button variant='secondary' onClick={onCancel} loading={isLoading}>
            Quay về
          </Button>

          <Button loading={isLoading} variant={!value ? 'disabled' : 'danger'} onClick={handleSubmit} disabled={!value}>
            Từ chối
          </Button>
        </div>
      </div>
    </Modal>
  );
};

export default RejectionModal;
