import { useMutation, useQuery } from '@tanstack/react-query';
import { DatePicker } from 'antd';
import locale from 'antd/es/date-picker/locale/vi_VN';
import Table, { ColumnsType } from 'antd/es/table';
import { bookReservingApis, rentBack } from 'apis';
import classNames from 'classnames';
import { Input, ModalDelete, Select, SizeChanger } from 'components';
import { LOCATION_READER_OPTIONS, TYPE_READER_OPTIONS } from 'constants/options';
import dayjs, { Dayjs } from 'dayjs';
import { debounce } from 'lodash';
import { PickerMode } from 'rc-picker/lib/interface';
import { useEffect, useMemo, useState, type ChangeEvent } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getSerialNumber } from 'utils/utils';

type Props = {
  picker: PickerMode;
};

const ListOfRentingBookTable = (props: Props) => {
  const [searchParams, setSearchParams] = useState<{
    day?: number;
    month?: number;
    year?: number;
    LoaiTK: '' | 'hs' | 'gv';
    page: number;
    pageSize: number;
    TextForSearch?: string;
  }>({
    month: new Date().getMonth() + 1,
    year: new Date().getFullYear(),
    LoaiTK: '',
    page: 1,
    pageSize: 30
  });

  const [lostBookId, setLostBookId] = useState<string>('');

  const {
    data: reservingBooksData,
    isLoading: loadingStatisticalBooks,
    refetch: refetchReservingBooks
  } = useQuery({
    queryKey: ['reservingBooks', searchParams],
    queryFn: () => {
      if (props.picker === 'month') delete searchParams.day;

      return bookReservingApis.getStatisticalBookReserving(searchParams);
    }
  });

  const { List_OrderBooks, countBook, countUser, count } = reservingBooksData?.data.Item || {
    List_OrderBooks: [],
    count: 0,
    countBook: 0,
    countUser: 0
  };

  useEffect(() => {
    if (!(props.picker === 'month')) {
      setSearchParams((prevState) => {
        return {
          ...prevState,
          day: new Date().getDate()
        };
      });
    } else {
      delete searchParams.day;
    }
  }, [props.picker]);

  const { mutate: lostBook, isLoading: loadingLostBook } = useMutation({
    mutationFn: rentBack.lostBook,
    onSuccess: () => {
      toast.success('Đã cập nhật tình trạng sách');
      setLostBookId('');
    }
  });

  const columns: ColumnsType<StatisticalBookReserving> = useMemo(() => {
    return [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        render: (value, record, index) => {
          return getSerialNumber(searchParams.page, searchParams.pageSize, index);
        },
        width: 80
      },
      {
        title: 'Mã cá biệt',
        dataIndex: 'MaKSCB',
        key: 'MaKSCB',
        onCell: (record) => ({
          className:
            record?.MaKSCB?.toLocaleLowerCase().includes('xóa') ||
            record?.MaKSCB?.toLocaleLowerCase().includes('thanh lý')
              ? 'text-danger-10 text-left'
              : 'text-left'
        }),
        width: 120
      },
      {
        title: 'Tên sách',
        dataIndex: 'TenSach',
        key: 'TenSach',
        onCell: (record) => ({
          className:
            record.TenSach.toLocaleLowerCase().includes('đã bị xóa') ||
            record.TenSach.toLocaleLowerCase().includes('đã bị thanh lý')
              ? 'text-danger-10 text-left'
              : 'text-left'
        }),
        render: (value, { TenSach, IdSach }) => {
          return !TenSach.includes('"sách đã bị xóa"') ? (
            <Link
              to={`/Sach/CapNhat/${IdSach}`}
              className='cursor-pointer truncate text-left text-primary-50 underline'
            >
              {TenSach}
            </Link>
          ) : (
            <p className='text-left'>{TenSach}</p>
          );
        },
        ellipsis: true,
        width: 200
      },
      {
        title: 'Tên bạn đọc',
        dataIndex: 'TenThanhVien',
        key: 'TenThanhVien',
        onCell: (record) => ({
          className: record.TenBanDoc?.toLocaleLowerCase().includes('đã xóa') ? 'text-danger-10 text-left' : 'text-left'
        }),
        render: (value, { TenBanDoc, IdUser, IsGlobal }) => {
          return !TenBanDoc?.toLocaleLowerCase().includes('đã xóa') && !IsGlobal ? (
            <Link
              to={'gv' ? `/GiaoVien/CapNhat/${IdUser}` : `/HocSinh/CapNhat/${IdUser}`}
              className='truncate text-left text-primary-30 underline'
            >
              {TenBanDoc}
            </Link>
          ) : (
            <p className='text-left'>{TenBanDoc}</p>
          );
        },
        ellipsis: true,
        width: 200
      },
      {
        title: 'Lớp/Tổ',
        dataIndex: 'LopTo',
        key: 'LopTo',
        width: 100
      },
      {
        title: 'Thời gian đặt mượn',
        dataIndex: 'ThoiGianDatMuon',
        key: 'ThoiGianDatMuon',
        render: (value, { ThoiGianDatMuon }) => dayjs(ThoiGianDatMuon).format('HH:mm DD/MM/YYYY')
      },
      {
        title: 'Thời gian hết hiệu lực',
        dataIndex: 'ThoiGianHetHieuLuc',
        key: 'ThoiGianHetHieuLuc',
        render: (value, { ThoiGianHetHieuLuc }) => dayjs(ThoiGianHetHieuLuc).format('HH:mm DD/MM/YYYY')
      },
      {
        title: 'Trạng thái',
        dataIndex: 'TrangThai',
        key: 'TrangThai',
        onCell: (record) => {
          switch (record.TrangThai) {
            case 'Hủy':
              return { className: 'bg-[#FF161659]' };

            case 'Đang đặt mượn':
              return { className: 'bg-[#5EA7DF3B]' };

            case 'Hết hiệu lực':
              return { className: 'bg-[#AEB2B56E]' };

            case 'Đặt mượn thành công':
              return { className: 'bg-[#B1E9CE]' };

            default:
              return { className: 'bg-tertiary-30/50' };
          }
        }
      }
    ];
  }, [searchParams.page, searchParams.pageSize]);

  const handleSearch = debounce((e: ChangeEvent<HTMLInputElement>) => {
    if (!e) return;

    const { value } = e.target;

    setSearchParams((prevState) => {
      return {
        ...prevState,
        TextForSearch: value,
        page: 1
      };
    });
  }, 300);

  const handleChangeSelection = (e: ChangeEvent<HTMLSelectElement>) => {
    if (!e) return;

    const { value, name } = e.target;
    if (name === 'IsGlobal') {
      setSearchParams((prevState) => {
        return {
          ...prevState,
          IsGlobal: value === '1' ? true : value === '0' ? false : undefined,
          page: 1
        };
      });
    } else
      setSearchParams((prevState) => {
        return {
          ...prevState,
          [name]: value as '' | 'hs' | 'gv',
          page: 1
        };
      });
  };

  const handleChangeDatePicker = (value: Dayjs | null) => {
    if (value) {
      props.picker === 'month'
        ? setSearchParams((prevState) => {
            return {
              ...prevState,
              month: value?.month() + 1,
              year: value?.year(),
              page: 1
            };
          })
        : setSearchParams((prevState) => {
            return {
              ...prevState,
              day: value?.date(),
              month: value?.month() + 1,
              year: value?.year(),
              page: 1
            };
          });
    }
  };

  return (
    <div className='mt-4'>
      <form className='flex w-full flex-col gap-2 bg-primary-50/20 p-2.5 md:flex-row'>
        <Input
          placeholder='Nhập tên bạn đọc, lớp/tổ, mã'
          containerClassName='w-full md:w-1/3'
          name='TextForSearch'
          onChange={handleSearch}
        />

        <Select
          items={TYPE_READER_OPTIONS}
          className='w-full md:w-1/6'
          onChange={handleChangeSelection}
          name='LoaiTK'
        />

        <Select
          items={[
            { value: '', label: 'Tất cả' },
            { value: 'Đang đặt mượn', label: 'Đang đặt mượn' },
            { value: 'Hết hiệu lực', label: 'Hết hiệu lực' },
            { value: 'Hủy', label: 'Hủy' },
            { value: 'Hoàn tất', label: 'Hoàn tất' }
          ]}
          className='w-full md:w-1/6'
          name='TrangThai'
          onChange={handleChangeSelection}
        />

        <DatePicker
          placeholder={props.picker === 'month' ? 'Chọn tháng' : 'Chọn ngày'}
          name='rentDate'
          picker={props.picker}
          onChange={handleChangeDatePicker}
          className={'w-full border border-gray-300 py-[0.8rem] px-2 outline-none md:w-1/6'}
          format={props.picker === 'month' ? 'MM/YYYY' : 'DD/MM/YYYY'}
          locale={locale}
          defaultValue={dayjs()}
        />
        <Select
          items={LOCATION_READER_OPTIONS}
          className='w-full md:w-1/6'
          name='IsGlobal'
          onChange={handleChangeSelection}
        />
      </form>

      <p className='mt-3 text-danger-10'>
        Tổng số sách được đặt mượn: <span className='font-bold'>{countBook || '--'} quyển</span>
      </p>

      <p className='mb-3 text-danger-10'>
        Tổng số người đặt mượn sách: <span className='font-bold'>{countUser || '--'} người</span>
      </p>

      <Table
        bordered
        rowKey={(row) => row.Id}
        className='custom-table'
        columns={columns}
        dataSource={List_OrderBooks || []}
        loading={loadingStatisticalBooks}
        pagination={{
          onChange(current, pageSize) {
            setSearchParams((prevState) => {
              return {
                ...prevState,
                pageSize: pageSize || 30,
                page: current
              };
            });
          },
          pageSize: searchParams.pageSize,
          hideOnSinglePage: false,
          showSizeChanger: false,
          total: count,
          showQuickJumper: true,
          locale: { jump_to: '', page: '' },
          current: searchParams.page
        }}
      />

      <div
        className={classNames('relative', {
          'mt-[0px]': searchParams.pageSize >= Number(count || 0)
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger
            visible={!!count}
            value={searchParams.pageSize + ''}
            currentPage={searchParams.page.toString()}
            total={count + ''}
            onChange={(pageSize) => {
              setSearchParams((prevState) => {
                return {
                  ...prevState,
                  pageSize: +pageSize,
                  page: 1
                };
              });
            }}
          />
        </div>
      </div>

      <ModalDelete
        open={!!lostBookId}
        closable={false}
        title={
          <h1 className='text-center'>
            CẢNH BÁO
            <p className='text-lg'>Sách được báo mất thì không thể phục hồi</p>
          </h1>
        }
        handleCancel={() => setLostBookId('')}
        handleOk={() => {
          lostBookId && lostBook(lostBookId);
        }}
        loading={loadingLostBook}
        labelOK='Xác nhận'
      />
    </div>
  );
};

export default ListOfRentingBookTable;
