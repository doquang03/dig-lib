import { Outlet } from 'react-router-dom';

const BookShelfPage = () => {
  return <Outlet />;
};

export default BookShelfPage;
