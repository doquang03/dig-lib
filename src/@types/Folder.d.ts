type FolderType = 'paper' | 'document' | 'audio' | 'video' | 'image';

type Folder = {
  Id: string;
  CreateDateTime: string;
  IdParent: string;
  IdReference: string;
  MaThuMuc: string;
  Type: FolderType;
  TenThuMuc: string;
  IsActive: boolean;
  GhiChu: string;
  CountBook: number;
};
