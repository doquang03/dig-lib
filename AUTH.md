## Tài liệu Authentication

## Cài đặt

Project này sử dụng identityServer4 để thực hiện nhiệm vụ xác thực. Cài đặt thư viện
`react-oidc-context` và `oidc-client-ts`.

Link: [https://www.npmjs.com/package/react-oidc-context](https://www.npmjs.com/package/react-oidc-context)

Thư viện này cung cấp các phương thức như `signinRedirect`, `signoutRedirect`, `signoutPopup`, `signoutSilent`, `removeUser`, ... -> dễ dàng thao tác các tác vụ như kiểm tra trạng thái xác thực, chuyển hướng trang đăng nhập, hay đăng xuất người dùng.

```
 Lưu ý: react-oidc-context chỉ giúp chúng ta thực hiện bước xác thực. Việc hiển thị tên thông tin người dùng thì phải gọi api.
```

## Code

### Cấu Hình

- Tạo object (oidcConfig) để cấu hình cho `AuthProvider` trong file `App.tsx`. Liên hệ team để được cung cấp nhé!

- Bọc toàn bộ `App` lại bằng `AuthProvider` -> điều này cho phép chúng ta sử dụng các đối tượng được export từ `hooks` `useAuth` cho toàn bộ ứng dụng.

### Kiểm tra trạng thái xác thực

- Để kiểm tra người dùng đã xác thực bằng identityServer4 hay chưa, truy cập file `useRouteElements`. Đây là hook để render các route được chỉ định.

- Ở đây sẽ có component `RedirectPage` sẽ thực hiện chức năng tự động `redirect` khi người dùng chưa xác thực hoặc `refresh-token` khi `access_token` hết hạn. Nếu có `lỗi` trong quá trình gọi đến `identity server 4` thì sẽ hiển thị `Modal` thông báo.

- Component `ProtectedRouted` ngăn chặn người dùng truy cập các page khác nếu chưa được xác thực

- Sử dụng hook `useAuth` của `react-oidc-context` truy cập đối tượng `isAuthticated` có kiểu dữ liệu là boolean để kiểm tra trạng thái xác thực. Nếu true thì có thể truy cập các trang trong project, false thì ngược lại.

- Khi người dùng đã xác thực thành công, `react-oidc-context` sẽ tự động lưu kết quả trả về vào `session storage` với key là `oidc.user:redirect_uri/:client_id`.

  ```
  (Lưu ý `redirect_uri` và `client_id` được lấy từ file `config.ts`)
  ```

- Chúng ta sẽ lấy kết quả ở `access_token` file `auth.ts` và gắn vào `headers` của `axios` trong file `http.ts`.

  ```
  Những kết quả đang được lưu dưới dạng `stringify` cho nên phải `parse` về `object` để sử dụng.
  ```

  ### Query APIs profile

- Sau khi hoàn thành cấu hình `interceptor` cho `axios` thì chúng ta có thể gọi api profile để có thông tin người dùng.
- Chúng ta sẽ tạo một `store` - `useContext` [https://beta.reactjs.org/reference/react/useContext](https://beta.reactjs.org/reference/react/useContext) để gọi api cũng như lưu trữ kết quả trả về.

- Chúng ta sẽ bọc `UserProvider` ở ngoài App để có sử dụng những đối tượng mà đã được `export` cho toàn ứng dụng.

- Cách sử dụng `user-context`

### Đăng xuất

- Hàm đăng xuất.
