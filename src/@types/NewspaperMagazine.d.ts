type NewspaperMagazine = {
  Id: string;
  CreateDateTime: string;
  Version: Version;
  MaBaoTapChi: string;
  Ten: string;
  ISSN: string;
  TenSongNgu: string;
  KyHan: string;
  IdNgonNgu: string;
  CQBienTap: string;
  CQPhatHanh: string;
  CoGiay: string;
  SoTrang: number;
  GiaBia: number;
  isBao: boolean;
  IsUsed: boolean;
};

type NewspaperMagazineRegisteration = {
  Id: string;
  CreateDateTime?: string;
  IdBaoTapChi: string;
  Ngay: string;
  SoLuong: number;
};
