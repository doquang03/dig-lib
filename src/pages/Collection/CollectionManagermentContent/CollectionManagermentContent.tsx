import { DeleteFilled, EditOutlined } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Tooltip } from 'antd';
import Table, { type ColumnsType } from 'antd/es/table';
import { collectionApis } from 'apis';
import { Button, Empty, Loading, Title, TitleDelete } from 'components';
import { ModalDelete } from 'components/Modal';
import { path } from 'constants/path';
import 'css/Collection.css';
import { useMemo, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

type Mock = {
  key: string;
  stt: string;
  name: string;
  controller: any;
};

const Collection = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectDelete, setSelectDelete] = useState({ Id: '', Name: '' });
  const navigate = useNavigate();
  const {
    data: collectionData,
    isLoading,
    refetch
  } = useQuery({
    queryKey: ['collection'],
    queryFn: () => collectionApis.Index()
  });

  const { mutate: DeleteCollection, isLoading: loadingDeleteCollection } = useMutation({
    mutationFn: () => collectionApis.Delete(selectDelete.Id),
    onSuccess: () => {
      setIsModalOpen(false);
      toast.success('Xóa bộ sưu tập thành công');
      refetch();
    },
    onError: () => {
      setIsModalOpen(false);
    }
  });

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const filterData = useMemo(() => {
    let Item: Array<Collection> = collectionData?.data?.Item;
    let result = [];
    let count = 0;
    if (isLoading === false && Item !== undefined) {
      for (let i = 0; i * 2 < Item.length; i++) {
        result[count] = {
          stt: (i * 2 + 1).toString(),
          key: Item[i * 2].Id,
          name: Item[i * 2].Name,
          controller: <div style={{ height: '30px' }}></div>
        };
        if (Item[i * 2].Status === false)
          result[count].controller = (
            <div>
              {/* <EditOutlined
                className='icon-blue'
                onClick={function ClickEdit(data: string, e: any) {
                  e.stopPropagation();
                  navigate(`CapNhat/${data}`);
                }.bind(undefined, Item[i * 2].Id)}
              ></EditOutlined>

              {Item[i * 2].DangSuDung ? (
                <Tooltip placement='topLeft' title={'Bộ sưu tập đang được sử dụng, không thể xóa!'} arrow={true}>
                  <DeleteFilled className='icon-default ml-2.5 cursor-not-allowed text-gray-400 hover:text-gray-400'></DeleteFilled>
                </Tooltip>
              ) : (
                <DeleteFilled
                  className='icon-red ml-2.5'
                  onClick={function ClickEdit(data: object, e: any) {
                    e.stopPropagation();
                    setSelectDelete((selectDelete) => ({
                      ...selectDelete,
                      ...data
                    }));
                    setIsModalOpen(true);
                  }.bind(undefined, Item[i * 2])}
                ></DeleteFilled>
              )} */}
            </div>
          );
        count++;
      }
    }
    return result;
  }, [collectionData, isLoading, navigate]);

  const filterData1 = useMemo(() => {
    let Item = collectionData?.data?.Item;
    let result: Array<Mock> = [];
    let count = 0;
    if (isLoading === false && Item !== undefined) {
      for (let i = 0; i * 2 < Item.length; i++) {
        if (Item[i * 2 + 1] === undefined) continue;
        result[count] = {
          stt: (i * 2 + 2).toString(),
          key: Item[i * 2 + 1].Id,
          name: Item[i * 2 + 1].Name,
          controller: <div style={{ height: '30px' }}></div>
        };
        if (Item[i * 2 + 1].Status === false)
          result[count].controller = (
            <div>
              {/* <EditOutlined
                className='icon-blue'
                onClick={function ClickEdit(data: string, e: any) {
                  e.stopPropagation();
                  navigate(`CapNhat/${data}`);
                }.bind(undefined, Item[i * 2 + 1].Id)}
              ></EditOutlined>
              {Item[i * 2 + 1].DangSuDung ? (
                <Tooltip placement='topLeft' title={'Bộ sưu tập đang được sử dụng, không thể xóa!'} arrow={true}>
                  <DeleteFilled className='icon-default ml-2.5 cursor-not-allowed text-gray-400 hover:text-gray-400'></DeleteFilled>
                </Tooltip>
              ) : (
                <DeleteFilled
                  className='icon-red ml-2.5'
                  onClick={function ClickEdit(data: object, e: any) {
                    e.stopPropagation();
                    setSelectDelete((selectDelete) => ({
                      ...selectDelete,
                      ...data
                    }));
                    setIsModalOpen(true);
                  }.bind(undefined, Item[i * 2 + 1])}
                ></DeleteFilled>
              )} */}
            </div>
          );
        count++;
      }
    }
    return result;
  }, [collectionData, isLoading, navigate]);

  const columns: ColumnsType<Mock> = useMemo(() => {
    return [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        width: 100
      },
      {
        title: 'Tên bộ sưu tập',
        dataIndex: 'name',
        key: 'name',
        render: (value, { name }) =>
          name.length > 60 ? (
            <Tooltip placement='topLeft' title={name} arrow={true}>
              <p className='text-center'>{name.substring(0, 60).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{name}</p>
          )
      }
      // {
      //   title: 'Hành động',
      //   dataIndex: 'controller',
      //   key: 'controller'
      // }
    ];
  }, []);

  const columns1: ColumnsType<Mock> = useMemo(() => {
    return [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        width: 100
      },
      {
        title: 'Tên bộ sưu tập',
        dataIndex: 'name',
        key: 'name',
        render: (value, { name }) =>
          name.length > 60 ? (
            <Tooltip placement='topLeft' title={name} arrow={true}>
              <p className='text-center'>{name.substring(0, 60).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{name}</p>
          )
      }
      // {
      //   title: 'Hành động',
      //   dataIndex: 'controller',
      //   key: 'controller'
      // }
    ];
  }, []);

  let locale = {
    emptyText: 'Không tìm được kết quả phù hợp'
  };

  return (
    <div className='collection-page p-5'>
      <Title title='DANH SÁCH BỘ SƯU TẬP' />

      {/* <div className='relative my-6 flex-col flex-wrap gap-1 md:flex md:md:flex-row'>
        <Button
          className='btn btn-primary btn-sm blue-button'
          onClick={function ClickCreate() {
            navigate(path.thembosuutap);
          }}
        >
          Thêm bộ sưu tập
        </Button>
      </div> */}

      <div className='relative my-6'>
        <Table
          loading={isLoading}
          columns={columns}
          pagination={false}
          dataSource={filterData}
          className='custom-table half-table'
          locale={{
            emptyText: () => <Empty label={locale.emptyText} />
          }}
          bordered
        />
        <Table
          loading={isLoading}
          columns={columns1}
          pagination={false}
          dataSource={filterData1}
          className='custom-table half-table'
          locale={{
            emptyText: () => <Empty label={locale.emptyText} />
          }}
          bordered
        />
        <ModalDelete
          className='model-delete'
          open={isModalOpen}
          closable={false}
          handleCancel={handleCancel}
          handleOk={DeleteCollection}
          title={
            <TitleDelete firstText='Bạn có chắn chắn muốn xóa bộ sưu tập' secondText={selectDelete?.Name}></TitleDelete>
          }
        ></ModalDelete>
      </div>
      <Loading open={loadingDeleteCollection}></Loading>
    </div>
  );
};

export default Collection;
