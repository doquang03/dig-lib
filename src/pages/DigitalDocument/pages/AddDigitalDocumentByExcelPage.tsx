import { DeleteFilled, DownloadOutlined } from '@ant-design/icons';
import { useMutation } from '@tanstack/react-query';
import { Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { digitalDocumentApis } from 'apis';
import { Button, Loading, ProgressBar, Title } from 'components';
import { path } from 'constants/path';
import 'css/AddAuthorByExcelPage.css';
import { useMemo, useRef, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { saveByteArray } from 'utils/utils';

type DigitalDocument = {
  STT: number;
  TenSach: string;
  TacGia: string;
  TacGiaPhu: string;
  IdNhaXuatBan: string;
  NgayBanHanh: string;
  IdNgonNgu: string;
  LienKet: string;
  KhungPhanLoai: string;
  ISBN: string;
  NguonTaiLieu: string;
  KhoMau: string;
  GiaBia: string;
  TomTat: string;
  SoBanSao: string;
  actions: string;
  cellError?: Array<number> | undefined;
  ListError?: Array<string> | undefined;
  Day19?: string | undefined;
  DDC?: string | undefined;
};

const AddDigitalDocumentByExcelPage = () => {
  const { pathname } = useLocation();
  let type: string;
  if (pathname.indexOf('TaiNguyenSo') !== -1) type = 'Document';
  else type = 'Media';
  const checkDocument = type === 'Document';
  const [step, setStep] = useState<number>(1);
  // Mock state.
  const [listDigitalDocumentExcel, setListDigitalDocumentExcel] = useState<Array<DigitalDocument>>([]);
  const [updateSuccess, setUpdateSuccess] = useState(0);
  const [listFail, setlistFail] = useState([]);
  const [tempFile, setTempFile] = useState<File>();

  const fileInutRef = useRef<HTMLInputElement | null>(null);
  const ref = useRef<HTMLDivElement | null>(null);

  const [page, setPage] = useState(1);
  const [paginationSize, setPaginationSize] = useState(10);

  const navigate = useNavigate();

  const { mutate: PreviewImport, isLoading: isLoadingPreviewImport } = useMutation({
    mutationFn: (file: File) => {
      let bodyFormData = new FormData();
      bodyFormData.append('type', type as string);
      bodyFormData.append('file', file as Blob);
      return digitalDocumentApis.PreviewImport(bodyFormData);
    },
    onSuccess: (res) => {
      let tempData = res?.data?.Item?.RawDataList;

      let result: Array<DigitalDocument> = [];
      for (let i = 0; i < tempData.length; i++) {
        result.push({
          STT: i,
          TenSach: tempData[i][1] || '',
          TacGia: tempData[i][2] || '',
          TacGiaPhu: tempData[i][3] || '',
          IdNhaXuatBan: tempData[i][4] || '',
          IdNgonNgu: tempData[i][5] || '',
          NgayBanHanh: tempData[i][6] || '',
          KhungPhanLoai: tempData[i][7] || '',
          ISBN: tempData[i][8] || '',
          NguonTaiLieu: tempData[i][9] || '',
          KhoMau: checkDocument ? tempData[i][10] || '' : '',
          LienKet: checkDocument ? tempData[i][11] || '' : tempData[i][10] || '',
          GiaBia: checkDocument ? tempData[i][12] || '' : tempData[i][11] || '',
          TomTat: checkDocument ? tempData[i][13] || '' : tempData[i][12] || '',
          SoBanSao: checkDocument ? tempData[i][14] || '' : tempData[i][13] || '',
          actions: ''
        });
      }

      setListDigitalDocumentExcel(result);
      setStep(2);
    }
  });

  const { mutate: ImportSave, isLoading: isLoadingImportSave } = useMutation({
    mutationFn: (payload: Array<Array<string>>) => digitalDocumentApis.ImportSave(payload, type as string),
    onSuccess: (res) => {
      let tempData = res?.data?.Item;
      setUpdateSuccess(tempData?.UpdateSuccess);
      setlistFail(tempData?.ListFail);
      setTempFile(tempData?.MemoryStream);
      setListDigitalDocumentExcel(tempData?.ListFail);
      setPage(1);
      setStep(3);
    }
  });
  const handleSubmit = () => {
    let tempData;
    let result = [];
    for (let i = 0; i < listDigitalDocumentExcel.length; i++) {
      tempData = [];
      tempData[0] = i.toString();
      for (let param in listDigitalDocumentExcel[i]) {
        switch (param) {
          case 'TenSach':
            tempData[1] = listDigitalDocumentExcel[i].TenSach;
            break;
          case 'TacGia':
            tempData[2] = listDigitalDocumentExcel[i].TacGia;
            break;
          case 'TacGiaPhu':
            tempData[3] = listDigitalDocumentExcel[i].TacGiaPhu;
            break;
          case 'IdNhaXuatBan':
            tempData[4] = listDigitalDocumentExcel[i].IdNhaXuatBan;
            break;
          case 'IdNgonNgu':
            tempData[5] = listDigitalDocumentExcel[i].IdNgonNgu;
            break;
          case 'NgayBanHanh':
            tempData[6] = listDigitalDocumentExcel[i].NgayBanHanh;
            break;
          case 'KhungPhanLoai':
            tempData[7] = listDigitalDocumentExcel[i].KhungPhanLoai;
            break;
          case 'ISBN':
            tempData[8] = listDigitalDocumentExcel[i].ISBN;
            break;
          case 'NguonTaiLieu':
            tempData[9] = listDigitalDocumentExcel[i].NguonTaiLieu;
            break;
          case 'KhoMau':
            tempData[10] = listDigitalDocumentExcel[i].KhoMau;
            break;
          case 'LienKet':
            tempData[11] = listDigitalDocumentExcel[i].LienKet;
            break;
          case 'GiaBia':
            tempData[12] = listDigitalDocumentExcel[i].GiaBia;
            break;
          case 'TomTat':
            tempData[13] = listDigitalDocumentExcel[i].TomTat;
            break;
          case 'SoBanSao':
            tempData[14] = listDigitalDocumentExcel[i].SoBanSao;
            break;
          default:
            break;
        }
      }
      result.push(tempData);
    }

    ImportSave(result);
    ref.current?.scrollIntoView({ behavior: 'smooth' });
  };

  const columns: ColumnsType<DigitalDocument> = useMemo(() => {
    const handleDeleteRow = (STT: number) => {
      let result: Array<DigitalDocument> = [];
      for (let i = 0; i < listDigitalDocumentExcel.length; i++) {
        if (STT === listDigitalDocumentExcel[i].STT) {
          continue;
        }
        result.push(listDigitalDocumentExcel[i]);
      }

      setListDigitalDocumentExcel(result);

      if (paginationSize * (page - 1) >= result.length) {
        setPage(page - 1);
      }
    };

    const columnParameters: ColumnsType<DigitalDocument> = [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        render: (value: any, record: any, j: number) => (page - 1) * paginationSize + j + 1
      },
      {
        title: 'Nhan đề',
        dataIndex: 'TenSach',
        key: 'TenSach',
        onCell: (record) => ({
          className:
            step === 3 && record.cellError && record.cellError.indexOf(1) !== -1 ? 'min-h-38 errorBackGround py-2' : ''
        })
      },
      {
        title: 'Tác giả chính',
        dataIndex: 'TacGia',
        key: 'TacGia',
        onCell: (record) => ({
          className:
            step === 3 && record.cellError && record.cellError.indexOf(2) !== -1 ? 'min-h-38 errorBackGround py-2' : ''
        })
      },
      {
        title: 'Tác giả phụ',
        dataIndex: 'TacGiaPhu',
        key: 'TacGiaPhu',
        onCell: (record) => ({
          className:
            step === 3 && record.cellError && record.cellError.indexOf(3) !== -1 ? 'min-h-38 errorBackGround py-2' : ''
        })
      },
      {
        title: 'Nhà xuất bản',
        dataIndex: 'IdNhaXuatBan',
        key: 'IdNhaXuatBan',
        onCell: (record) => ({
          className:
            step === 3 && record.cellError && record.cellError.indexOf(4) !== -1 ? 'min-h-38 errorBackGround py-2' : ''
        })
      },
      {
        title: 'Ngôn ngữ',
        dataIndex: 'IdNgonNgu',
        key: 'IdNgonNgu',
        onCell: (record) => ({
          className:
            step === 3 && record.cellError && record.cellError.indexOf(5) !== -1 ? 'min-h-38 errorBackGround py-2' : ''
        })
      },
      {
        title: 'Ngày ban hành',
        dataIndex: 'NgayBanHanh',
        key: 'NgayBanHanh',
        onCell: (record) => ({
          className:
            step === 3 && record.cellError && record.cellError.indexOf(6) !== -1 ? 'min-h-38 errorBackGround py-2' : ''
        })
      },
      {
        title: 'Khung phân loại',
        dataIndex: 'KhungPhanLoai',
        key: 'KhungPhanLoai',
        onCell: (record) => ({
          className:
            step === 3 && record.cellError && record.cellError.indexOf(7) !== -1 ? 'min-h-38 errorBackGround py-2' : ''
        }),
        render: (value, record) => {
          return (
            <>
              {step === 2 && record?.KhungPhanLoai}
              {step === 3 && record?.Day19 ? record?.Day19 : record?.DDC}
            </>
          );
        }
      },
      {
        title: 'ISBN/ISSN',
        dataIndex: 'ISBN',
        key: 'ISBN',
        onCell: (record) => ({
          className:
            step === 3 && record.cellError && record.cellError.indexOf(8) !== -1 ? 'min-h-38 errorBackGround py-2' : ''
        })
      },
      {
        title: 'Nguồn tài liệu',
        dataIndex: 'NguonTaiLieu',
        key: 'NguonTaiLieu',
        onCell: (record) => ({
          className:
            step === 3 && record.cellError && record.cellError.indexOf(9) !== -1 ? 'min-h-38 errorBackGround py-2' : ''
        })
      }
    ];
    if (checkDocument) {
      columnParameters.push({
        title: 'Khổ mẫu',
        dataIndex: 'KhoMau',
        key: 'KhoMau',
        onCell: (record) => ({
          className:
            step === 3 && record.cellError && record.cellError.indexOf(10) !== -1 ? 'min-h-38 errorBackGround py-2' : ''
        })
      });
    }

    columnParameters.push({
      title: 'Liên kết',
      dataIndex: 'LienKet',
      key: 'LienKet',
      onCell: (record) => ({
        className:
          step === 3 && record.cellError && record.cellError.indexOf(11) !== -1 ? 'min-h-38 errorBackGround py-2' : ''
      })
    });
    columnParameters.push({
      title: 'Giá tiền',
      dataIndex: 'GiaBia',
      key: 'GiaBia',
      onCell: (record) => ({
        className:
          step === 3 && record.cellError && record.cellError.indexOf(12) !== -1 ? 'min-h-38 errorBackGround py-2' : ''
      })
    });
    columnParameters.push({
      title: 'Tóm tắt',
      dataIndex: 'TomTat',
      key: 'TomTat',
      onCell: (record) => ({
        className:
          step === 3 && record.cellError && record.cellError.indexOf(13) !== -1 ? 'min-h-38 errorBackGround py-2' : ''
      })
    });
    columnParameters.push({
      title: 'Số bản sao',
      dataIndex: 'SoBanSao',
      key: 'SoBanSao',
      onCell: (record) => ({
        className:
          step === 3 && record.cellError && record.cellError.indexOf(14) !== -1 ? 'min-h-38 errorBackGround py-2' : ''
      })
    });
    columnParameters.push({
      title: (step === 2 && 'Hành động') || (step === 3 && 'Nội dung lỗi'),
      dataIndex: 'actions',
      key: 'actions',
      render: (value, record) => {
        return (
          <>
            {step === 2 && (
              <button
                className='mx-2'
                onClick={(e) => {
                  e.stopPropagation();
                  handleDeleteRow(record.STT);
                }}
              >
                <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
              </button>
            )}
            {step === 3 && <div style={{ color: 'red' }}>{record?.ListError?.join(', ') || ''}</div>}
          </>
        );
      }
    });
    return columnParameters;
  }, [checkDocument, listDigitalDocumentExcel, page, paginationSize, step, listDigitalDocumentExcel.length]);

  const handleOnFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files?.[0];
    PreviewImport(fileFromLocal as File);
  };

  const handleAccessFileInputRef = () => {
    fileInutRef.current?.click();
  };

  const handleBack = () => {
    if (checkDocument) navigate(path.digitalDocument);
    else navigate(path.media);
  };

  const handleBackCurrent = () => {
    setListDigitalDocumentExcel([]);
    setStep(1);
  };

  return (
    <div className='p-5' ref={ref}>
      <Title title={`Thêm mới  ${checkDocument ? 'tài nguyên số' : 'tài nguyên đa phương tiện'} từ file Excel`} />

      <div className='my-10'>
        <ProgressBar activeStep={step} type='excel' />
      </div>

      {/* TODO: Condition are step > 1 and data is not undefined*/}
      {step === 1 && (
        <div className='flex items-center justify-center rounded-md bg-tertiary-10 py-4'>
          <a
            href={checkDocument ? `/Tempalates/MauTaiLieu.xls` : `/Tempalates/MauDaPhuongTien.xls`}
            download={checkDocument ? `MauTaiLieu` : `MauDaPhuongTien`}
          >
            <Button className='bg-tertiary-20 hover:bg-tertiary-20/30'>
              <DownloadOutlined />
              File excel mẫu
            </Button>
          </a>

          <input
            type='file'
            accept='.csv,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel'
            className='hidden'
            ref={fileInutRef}
            onChange={handleOnFileChange}
          />

          <Button variant='default' className='ml-2' onClick={handleAccessFileInputRef}>
            Chọn tệp
          </Button>
        </div>
      )}

      {step > 1 && (
        <div>
          {step === 2 && (
            <>
              <p>
                Tổng số tài liệu số: <span className='font-bold'>{listDigitalDocumentExcel?.length}</span>
              </p>

              {!!listDigitalDocumentExcel?.length && <h3 className='text-primary-10'>DANH SÁCH TÀI LIỆU SỐ</h3>}
            </>
          )}

          {step === 3 && (
            <>
              <p className='text-tertiary-30'>
                Lưu thành công: <span className='font-bold'>{updateSuccess} tài liệu số</span>
              </p>

              <p className='flex items-center gap-1 text-danger-10'>
                Lưu thất bại: <span className='font-bold'> {' ' + listFail.length} tài liệu số</span>{' '}
                {listFail.length > 0 && (
                  <Button
                    variant='danger'
                    className='ml-2'
                    onClick={() => {
                      saveByteArray(tempFile, 'DsDigitalDocumentBiLoi.xls', 'application/xls');
                    }}
                  >
                    <DownloadOutlined />
                    Tải file lỗi
                  </Button>
                )}
              </p>
              {listFail.length > 0 && <h3 className='uppercase text-primary-10'>Danh sách tài liệu số bị lỗi</h3>}
            </>
          )}

          {((step === 2 && !!listDigitalDocumentExcel?.length) || (step === 3 && listFail.length > 0)) && (
            <div className='relative mt-6'>
              <Table
                loading={isLoadingPreviewImport && isLoadingImportSave}
                columns={columns}
                pagination={{
                  current: page,
                  pageSize: paginationSize,
                  total: listDigitalDocumentExcel.length,
                  onChange(current, pageSize) {
                    setPage(current);
                    setPaginationSize(pageSize);
                  },
                  defaultPageSize: 10,
                  hideOnSinglePage: true,
                  showSizeChanger: false
                }}
                dataSource={listDigitalDocumentExcel}
                scroll={{ x: 980 }}
                rowKey={(record) => record.STT}
                className='custom-table'
                bordered
              />
            </div>
          )}
        </div>
      )}

      <div className='mr-10 mt-6 flex items-center justify-end'>
        {step === 1 && (
          <>
            <Button variant='secondary' onClick={handleBack}>
              Quay về
            </Button>
          </>
        )}

        <>
          {step === 2 && !isLoadingPreviewImport && (
            <>
              <Button variant='secondary' onClick={handleBackCurrent}>
                Quay về
              </Button>
              <Button variant='default' className='ml-2' onClick={handleSubmit}>
                Tiếp tục
              </Button>
            </>
          )}

          {step === 3 && !isLoadingImportSave && (
            <Button variant='default' className='ml-2' onClick={handleBack}>
              Hoàn tất
            </Button>
          )}
        </>
      </div>
      <Loading open={isLoadingPreviewImport || isLoadingImportSave} />
    </div>
  );
};

export default AddDigitalDocumentByExcelPage;
