import { Outlet } from 'react-router-dom';

const RentingTicket = () => <Outlet />;

export default RentingTicket;
