import { useMutation } from '@tanstack/react-query';
import { Radio, RadioChangeEvent, Select } from 'antd';
import { newspaperMagazineApis } from 'apis';
import classNames from 'classnames';
import { BaseModal } from 'components';
import { SCHOOL_YEAR_OPTIONS } from 'constants/options';
import { useEffect, useState } from 'react';

type Props = {
  open: boolean;
  onInvisile: VoidFunction;
  onClose: VoidFunction;
  newspaperMagazines?: NewspaperMagazine[];
};

const PrintNoteBook = (props: Props) => {
  const { open, newspaperMagazines, onClose, onInvisile } = props;

  const [formValues, setFormValue] = useState<{
    printMode: 'day' | 'month';
    ids: string[];
    schoolYears: string[];
  }>({
    printMode: 'day',
    ids: [],
    schoolYears: []
  });
  const [error, setError] = useState<boolean>(false);

  const { mutate, isLoading } = useMutation({
    mutationFn: newspaperMagazineApis.printRegisteration,
    onSuccess: (data) => {
      const blob = new Blob([data.data, { type: 'application/xls' }]);
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = `SoDangKyBaoTapChiTheo${formValues.printMode === 'day' ? 'Ngay' : 'Thang'}.xls`;
      link.click();

      setFormValue({
        printMode: 'day',
        ids: [],
        schoolYears: []
      });

      onClose();
    }
  });

  useEffect(() => {
    if (formValues.ids.length || formValues.schoolYears.length) {
      setError(false);
    }
  }, [formValues.ids.length, formValues.schoolYears.length]);

  const handleChangePrintMode = (e: RadioChangeEvent) => {
    // setValue(e.target.value);
    if (!e) return;

    const { value } = e.target;

    setFormValue((prevState) => {
      return {
        ...prevState,
        printMode: value
      };
    });
  };

  const hanldeChangeIds = (value: string[]) => {
    setFormValue((prevState) => {
      return {
        ...prevState,
        ids: value
      };
    });
  };

  const handleChangeSchoolYear = (value: string) => {
    // @ts-ignore
    setFormValue((prevState) => {
      return {
        ...prevState,
        schoolYears: value
      };
    });
  };

  const _handlePrint = () => {
    if (!formValues.ids.length || !formValues.schoolYears.length) {
      setError(true);

      return;
    }

    formValues.ids.forEach((id) =>
      mutate({
        IdBaoTapChi: id,
        isNgay: formValues.printMode === 'day',
        Ngay: formValues.schoolYears.map((yearSchool) => {
          return {
            Min: new Date('07/01/' + yearSchool.split('-')[0].replace(/\s/g, '')).toISOString(),
            Max: new Date('06/31/' + yearSchool.split('-')[1].replace(/\s/g, '')).toISOString()
          };
        })
      })
    );
  };

  const handleBack = () => {
    setFormValue({
      printMode: 'day',
      ids: [],
      schoolYears: []
    });

    setError(false);

    onClose();
  };

  return (
    <BaseModal
      title={'Tải sổ đăng ký báo tạp chí'}
      open={open}
      onInvisile={handleBack}
      onOk={_handlePrint}
      loading={isLoading}
      labelOk='Tải'
    >
      <div className='my-2 flex flex-col gap-4'>
        <div className='flex flex-grow items-center'>
          <label className='basis-1/4 font-bold'>
            In theo <span className='text-danger-10'>*</span>
          </label>

          <div className='basis-full'>
            <Radio.Group
              onChange={handleChangePrintMode}
              value={formValues?.printMode || 'day'}
              name='printMode'
              disabled={isLoading}
            >
              <Radio value={'day'}>Ngày</Radio>
              <Radio value={'month'}>Tháng</Radio>
            </Radio.Group>
          </div>
        </div>

        <div className='flex flex-grow items-center'>
          <label className='basis-1/4 font-bold'>
            Báo, tạp chí <span className='text-danger-10'>*</span>
          </label>

          <div className='basis-full'>
            <Select
              onChange={hanldeChangeIds}
              mode='multiple'
              placeholder='Chọn báo tạp chí'
              style={{ width: '100%' }}
              options={newspaperMagazines?.map(({ Id, Ten }) => {
                return {
                  value: Id,
                  label: Ten
                };
              })}
              className={classNames(``, {
                'rounded-md border border-red-500': !!error && !formValues.ids.length
              })}
              disabled={isLoading}
              value={formValues.ids}
            />

            {error && !formValues.ids.length && <p className='text-danger-10'>Báo, tạp chí không được để trống</p>}
          </div>
        </div>

        <div className='flex flex-grow items-center'>
          <label className='basis-1/4 font-bold'>
            Năm học <span className='text-danger-10'>*</span>
          </label>

          <div className='basis-full'>
            <Select
              onChange={handleChangeSchoolYear}
              mode='multiple'
              placeholder='Chọn báo năm học'
              style={{ width: '100%' }}
              options={SCHOOL_YEAR_OPTIONS}
              className={classNames(``, {
                'rounded-md border border-red-500': !!error && !formValues.schoolYears.length
              })}
              disabled={isLoading}
              //@ts-ignore
              value={formValues?.schoolYears}
            />

            {error && !formValues.schoolYears.length && <p className='text-danger-10'>Năm học không được để trống</p>}
          </div>
        </div>
      </div>
    </BaseModal>
  );
};

export default PrintNoteBook;
