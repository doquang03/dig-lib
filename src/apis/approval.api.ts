import httpThuVien from 'utils/httpThuVien';

const endpoint = `${process.env.REACT_APP_SERVICE_MLLH}/api/HostLibrary`;
const endpointBook = `${process.env.REACT_APP_SERVICE_MLLH}/api/Books`;

export type ExtensionFile = {
  FileSize: number;
  LinkFile: string;
  FileName: string;
  PageNumber: number;
  FileNameRoot: string;
};

type ApprovalData = { ListParam: string[]; ListMedia: ExtensionFile[]; LinkBiaSach: ExtensionFile };

const approvalApis = {
  getListOfApprovals: (
    params?: { page: number; pageSize: number } & Partial<{
      TextForSearch: string;
      TimeSubmit: string;
      IsComplete: boolean;
    }>
  ) => httpThuVien.post<ResponseApi<{ Key: BookApproval[]; Count: number }>>(`/${endpoint}/ListApprove`, params),
  getListOfdublicationDocumentsSugestion: (id: string) =>
    httpThuVien.get<ResponseApi<{ ListModel: SuggestionDocument[]; MustMerge: boolean }>>(
      `${endpoint}/RelatedApprove/${id}`
    ),
  // get dữ liệu sách để merge vào host
  getBookDataToMerge: (id: string) => httpThuVien.get<ResponseApi<ApprovalData>>(`${endpoint}/DetailApprove/${id}`),
  // get dữ liệu sách có sẵn ở trên host
  getExistedBookDataAtHost: (id: string) =>
    httpThuVien.get<ResponseApi<ApprovalData & { Marc21: string }>>(`${endpointBook}/DetailBooks/${id}`),
  // approve document
  approveDocument: (params: {
    IdSubmit: string;
    IdBook: string;
    ListParam: string[];
    ListMedia: string[];
    LinkBiaSach: string;
    FailMessage?: string;
  }) => httpThuVien.post(`${endpointBook}/CreateBookBySubmit`, params),
  getApprovalDetails: (id: string) => httpThuVien.get<ResponseApi<ApprovalData>>(`${endpoint}/DetailApprove/${id}`)
};

export default approvalApis;
