import { Outlet } from 'react-router-dom';

const SupplierPage = () => {
  return <Outlet />;
};

export default SupplierPage;
