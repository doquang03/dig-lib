import Table, { type ColumnsType } from 'antd/es/table';
import { Button, Input, Title } from 'components';
import { path } from 'constants/path';
import { useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { VietnameseAuthorStrings, VietnameseAuthorCodes } from 'constants/author';
import { jsUcfirst, removeVietnameseTones } from 'utils/utils';

type Mock = {
  stt: string;
  NguyenAm?: string;
  MaSo?: string;
};

type FormInput = {
  TextForSearch: string;
};

const LibraryMonitorManagermentContent = () => {
  const { register } = useForm<FormInput>({
    defaultValues: {
      TextForSearch: ''
    }
  });

  const navigate = useNavigate();

  const columns: ColumnsType<Mock> = useMemo(() => {
    return [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        className: 'min-content',
        render: (value, record, index) => {
          return record.stt;
        },
        onCell: (_, index) => ({
          colSpan: _.NguyenAm === undefined ? 3 : 1,
          className: !_.NguyenAm ? 'blueBackground' : ''
        })
      },
      {
        title: 'Nguyên âm',
        dataIndex: 'NguyenAm',
        key: 'NguyenAm',
        onCell: (_, index) => ({
          colSpan: _.NguyenAm === undefined ? 0 : 1
        })
      },
      {
        title: 'Mã số',
        dataIndex: 'MaSo',
        key: 'MaSo',
        onCell: (_, index) => ({
          colSpan: _.MaSo === undefined ? 0 : 1
        })
      }
    ];
  }, []);

  const dataTable = useMemo(() => {
    let result: Array<Mock> = [];
    let tempData: Mock;
    let max =
      VietnameseAuthorStrings.length < VietnameseAuthorCodes.length
        ? VietnameseAuthorStrings.length
        : VietnameseAuthorCodes.length;
    let checkString = '0';
    for (let i = 0; i < max; i++) {
      if (
        checkString.toLocaleLowerCase() !== removeVietnameseTones(VietnameseAuthorStrings[i][0]).toLocaleLowerCase()
      ) {
        checkString = VietnameseAuthorStrings[i][0].toLocaleUpperCase();
        result.push({ stt: checkString });
      }
      tempData = {
        stt: (i + 1).toString(),
        NguyenAm: jsUcfirst(VietnameseAuthorStrings[i]),
        MaSo: VietnameseAuthorCodes[i]
      };
      result.push(tempData);
    }
    return result;
  }, []);

  const [dataSource, setDataSource] = useState(dataTable);

  let locale = {
    emptyText: 'Không tìm được kết quả phù hợp'
  };

  const handleBack = () => {
    navigate(path.tacgia);
  };

  return (
    <div className='p-5'>
      <Title title='BẢNG KÝ HIỆU TÁC GIẢ' />

      <form className='my-5 flex flex-col flex-wrap gap-2 bg-secondary-10 py-3 px-3.5 md:flex-row'>
        <Input
          placeholder='Nhập tên tác giả cần tìm kiếm'
          containerClassName='w-[100%] md:w-[unset] md:grow'
          name='TextForSearch'
          register={register}
          onKeyUp={(e) => {
            const target = e.target as HTMLTextAreaElement;
            const currValue = target.value;
            const filteredData = dataTable.filter(
              (entry) =>
                entry?.NguyenAm === undefined ||
                entry?.NguyenAm?.toLocaleLowerCase().includes(currValue.toLocaleLowerCase())
            );
            setDataSource(filteredData);
          }}
        />
      </form>

      <div className='relative my-6'>
        <Table
          columns={columns}
          pagination={false}
          dataSource={dataSource || []}
          scroll={{ x: 980 }}
          rowKey={(record) => record.stt + record.NguyenAm}
          className='custom-table'
          locale={locale}
          bordered
        />
        <div className='mt-5 flex justify-end px-2'>
          <Button type='button' variant='secondary' className='font-semibold' onClick={handleBack}>
            Quay về
          </Button>
        </div>
      </div>
    </div>
  );
};

export default LibraryMonitorManagermentContent;
