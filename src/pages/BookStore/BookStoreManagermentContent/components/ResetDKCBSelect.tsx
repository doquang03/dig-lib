import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation } from '@tanstack/react-query';
import { Modal, ModalProps } from 'antd';
import { bookStoreApis } from 'apis';
import { AxiosError } from 'axios';
import { Button, Input, InputNumber } from 'components';
import { useMemo } from 'react';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import { MaSachCaBiet } from 'utils/rules';

type Props = {
  khoSelect: BookStoreType;
  listIdSCB: Array<String>;
  onHide: VoidFunction;
  onSuccess: VoidFunction;
} & ModalProps;

type FormInput = {
  MaCBMoi: string;
};

const ResetDKCBSelect = (props: Props) => {
  const {
    control,
    handleSubmit,
    reset,
    formState: { errors }
  } = useForm<FormInput>({
    defaultValues: {
      MaCBMoi: ''
    },
    resolver: yupResolver(MaSachCaBiet)
  });
  const { khoSelect, listIdSCB, open, onHide, onSuccess } = props;

  const { mutate: ResetSachCaBietSelect, isLoading } = useMutation({
    mutationFn: (soDem: number) =>
      bookStoreApis.ResetSachCaBietSelect({ List_Choose: listIdSCB }, khoSelect?.Id as string, soDem),
    onSuccess() {
      toast.success('Reset mã cá biệt thành công');
      onHide();
      onSuccess();
      reset();
    }
  });

  const handleComplete = handleSubmit((data: any) => {
    ResetSachCaBietSelect(+data.MaCBMoi || 0);
  });

  return (
    <Modal
      open={open}
      title={<h3 className='border-b-2 pb-2 font-bold uppercase text-primary-10'>Vị trí đặt lại mã cá biệt</h3>}
      closable={false}
      footer={null}
    >
      <form onSubmit={handleComplete}>
        <div className='mb-4 flex flex-col'>
          <div className='col-span-9 flex items-center justify-between gap-1'>
            <InputNumber
              rootClassName='grow'
              name='MaCBMoi'
              placeholder='Nhập số đếm đăng kí cá biệt'
              control={control}
              maxLength={4}
            />
          </div>
          {!!errors && <p className='mt-1 text-[14px] text-red-500'>{errors.MaCBMoi?.message}</p>}
        </div>

        <div className='mt-3 flex items-center justify-end gap-2'>
          <Button type='button' variant='secondary' onClick={onHide} loading={isLoading} disabled={isLoading}>
            Quay về
          </Button>

          <Button loading={isLoading} disabled={isLoading}>
            Cập nhật
          </Button>
        </div>
      </form>
    </Modal>
  );
};

export default ResetDKCBSelect;
