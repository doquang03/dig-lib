import { DeleteFilled, EditOutlined } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Tooltip } from 'antd';
import Table, { type ColumnsType } from 'antd/es/table';
import { topicApis } from 'apis';
import classNames from 'classnames';
import { Button, Empty, Input, Loading, ModalDelete, SizeChanger, Title, TitleDelete } from 'components';
import type { ButtonCustomProps } from 'components/Button/Button';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import { usePaginationNavigate, useQueryConfig } from 'hooks';
import { isEmpty, omitBy, trim } from 'lodash';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import { createSearchParams, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getSerialNumber } from 'utils/utils';

// TODO: Delete as soon as api implement
type Mock = {
  Id: string;
  Ten: string;
  TenNgan: string;
};

type FormInput = {
  TextForSearch: string;
};

const TopicManagermentContent = () => {
  const { register, handleSubmit, reset, setValue } = useForm<FormInput>({
    defaultValues: {
      TextForSearch: ''
    }
  });

  const navigate = useNavigate();

  const handleNavigation = usePaginationNavigate();

  const queryConfig = useQueryConfig();
  const [totalListTopic, setTotalListTopic] = useState(0);
  const [selectedDelete, setSelectedDelete] = useState<Mock>();
  const [isVisiable, setIsVisiable] = useState(false);

  const { isAllowedAdjustment } = useUser();

  const {
    data: dataTopic,
    refetch,
    isFetching
  } = useQuery({
    queryKey: ['Topic', queryConfig.page, queryConfig.pageSize, queryConfig.TextForSearch],
    queryFn: () => {
      let page = 1;
      if (typeof queryConfig.page === 'string') page = parseInt(queryConfig.page);
      let queryString = `?page=${page}&sizeNumber=${queryConfig.pageSize}`;
      if (queryConfig.TextForSearch !== undefined) queryString += `&TextForSearch=${queryConfig.TextForSearch}`;
      let request = topicApis.Index(queryString);
      return request;
    }
  });

  const listTopic = useMemo(() => {
    if (!dataTopic?.data?.Item) return;
    const { ListChuDiem, count } = dataTopic?.data?.Item;
    setTotalListTopic(count);
    return ListChuDiem;
  }, [dataTopic?.data?.Item]);

  const { mutate: handleDelete, isLoading: isLoadingDelete } = useMutation({
    mutationFn: (Id: string) => topicApis.Delete(Id),
    onSuccess: () => {
      refetch();

      toast.success('Xóa chủ điểm thành công');
    }
  });

  const onPaginate = useCallback(
    (page: number, pageSize: number) => {
      handleNavigation({ ...queryConfig, page: page + '', pageSize: pageSize + '' });
    },
    [handleNavigation, queryConfig]
  );

  const pagination = useMemo(() => {
    return {
      current: (!!queryConfig.page && +queryConfig.page) || 1,
      pageSize: (!!queryConfig.pageSize && +queryConfig.pageSize) || 30,
      total: totalListTopic,
      onChange: onPaginate,
      hideOnSinglePage: true,
      showSizeChanger: false,
      showQuickJumper: true,
      locale: { jump_to: '', page: '' }
    };
  }, [onPaginate, queryConfig.page, queryConfig.pageSize, totalListTopic]);

  const columns = useMemo(() => {
    const _columns: ColumnsType<Mock> = [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        className: 'min-content',
        render: (value, record, index) => {
          return (
            queryConfig.page && queryConfig.pageSize && getSerialNumber(+queryConfig.page, +queryConfig.pageSize, index)
          );
        },
        width: 100
      },
      {
        title: 'Tên chủ điểm',
        dataIndex: 'Ten',
        key: 'Ten',
        render: (value, { Ten }) => (
          <Tooltip className='truncate' title={Ten} arrow={true}>
            <p>{Ten}</p>
          </Tooltip>
        ),
        onCell: (record) => ({
          className: 'text-left'
        }),
        ellipsis: true
      }
    ];

    if (isAllowedAdjustment) {
      _columns.push({
        title: 'Hành động',
        dataIndex: 'actions',
        key: 'actions',
        className: 'min-content',
        width: 150,
        render: (value, record) => {
          return (
            <>
              <Tooltip title='Chỉnh sửa'>
                <button
                  className='mx-2'
                  onClick={(e) => {
                    e.stopPropagation();
                    navigate(`CapNhat/${record.Id}`);
                  }}
                >
                  <EditOutlined style={{ fontSize: '20px', color: '#08c' }} />
                </button>
              </Tooltip>

              <Tooltip title='Xóa'>
                <button
                  className='mx-2'
                  onClick={(e) => {
                    e.stopPropagation();
                    setSelectedDelete(() => {
                      return record;
                    });
                    setIsVisiable(true);
                  }}
                >
                  <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
                </button>
              </Tooltip>
            </>
          );
        }
      });
    }

    return _columns;
  }, [isAllowedAdjustment, navigate, queryConfig.page, queryConfig.pageSize]);

  useEffect(() => {
    Object.keys(queryConfig).forEach((key) => {
      if (!key) {
        return;
      }
      // @ts-ignore
      setValue(key, queryConfig[key]);
    });
  }, [queryConfig, setValue]);

  const onSubmit = handleSubmit((data) => {
    handleNavigation({
      ...omitBy({ ...data, TextForSearch: trim(data.TextForSearch, ' ').replace(/\s\s+/g, ' ') }, isEmpty),
      page: '1'
    });
  });

  const handleResetField = () => {
    const { pathname } = window.location;

    navigate({
      pathname: pathname,
      search: createSearchParams({}).toString()
    });

    reset();
  };

  // TODO: Do something with selected Topics
  const handleSelectedTopics = () => {
    navigate(path.themchudiem);
  };

  let locale = {
    emptyText: 'Không tìm được kết quả phù hợp'
  };

  return (
    <div className='p-5'>
      <Title title='DANH SÁCH CHỦ ĐIỂM' />

      <form
        className='my-5 flex flex-col flex-nowrap gap-2 bg-secondary-10 py-3 px-3.5 md:flex-row'
        onSubmit={onSubmit}
      >
        <Input placeholder='Nhập chủ điểm' containerClassName='w-[100%]' name='TextForSearch' register={register} />

        <Button type='button' variant='secondary' className='shrink-0 font-semibold' onClick={handleResetField}>
          Làm mới
        </Button>

        <Button type='submit' variant='default' className='shrink-0 font-semibold'>
          Tìm kiếm
        </Button>
      </form>

      <Button
        className={classNames('w-[100%] md:w-auto', {
          hidden: !isAllowedAdjustment,
          'w-[100%] md:w-auto': isAllowedAdjustment
        })}
        onClick={handleSelectedTopics}
      >
        Thêm chủ điểm
      </Button>

      <div className='mt-6'>
        <Table
          loading={isFetching && !listTopic?.length}
          columns={columns}
          dataSource={listTopic || []}
          pagination={pagination}
          scroll={{ x: 980 }}
          rowKey={(record) => record.Id}
          className='custom-table'
          locale={{
            emptyText: () => <Empty label={locale.emptyText} />
          }}
          bordered
        />
      </div>

      <div
        className={classNames('relative', {
          // @ts-ignore
          'mt-[64px]': (queryConfig.page && +queryConfig.pageSize) >= totalListTopic
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger visible={!!listTopic?.length} value={queryConfig.pageSize} total={totalListTopic.toString()} />
        </div>
      </div>

      <ModalDelete
        open={isVisiable}
        closable={false}
        title={
          <TitleDelete firstText='Bạn có chắn chắn muốn xóa chủ điểm' secondText={selectedDelete?.Ten}></TitleDelete>
        }
        // TODO: Remove inline funtion, integrate api
        handleCancel={() => {
          setSelectedDelete(undefined);
          setIsVisiable(false);
        }}
        handleOk={() => {
          setIsVisiable(false);
          handleDelete(selectedDelete?.Id as string);
          setSelectedDelete(undefined);
        }}
      />
      <Loading open={isLoadingDelete}></Loading>
    </div>
  );
};

export default TopicManagermentContent;
