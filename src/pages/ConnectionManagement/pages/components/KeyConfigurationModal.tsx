import { useMutation, useQuery } from '@tanstack/react-query';
import { Radio, RadioChangeEvent } from 'antd';
import { connectionApis } from 'apis';
import { BaseModal, Loading } from 'components';
import { useMemo, useState } from 'react';
import { toast } from 'react-toastify';

type Props = {
  open: boolean;
  onCancel: VoidFunction;
};

const KeyConfigurationModal = (props: Props) => {
  const { open, onCancel } = props;

  const [value, setValue] = useState<'AES' | 'RSA' | 'MD5' | 'SHA256'>();

  const { isLoading, refetch, isRefetching } = useQuery({
    queryKey: ['getKeyType'],
    queryFn: connectionApis.getKeyType,
    onSuccess: (data) => {
      setValue(data.data.Item.TypeKey as 'AES' | 'RSA' | 'MD5' | 'SHA256');
    }
  });

  const { mutate, isLoading: loadingSetKeyType } = useMutation({
    mutationFn: connectionApis.setKeyType,
    onSuccess: () => {
      refetch();
      toast.success('Cấu hình định dạng key thành công');
    }
  });

  const note = useMemo(() => {
    switch (value) {
      // case 'base64':
      //   return [
      //     'Base64 là một tập hợp gồm các ký tự từ A đến Z, từ a đến z, từ 0 đến 9, dấu +, dấu /. Tổng cộng là 64 ký tự biểu diễn giá trị từ 0 đến 63.',
      //     'Một ký tự theo Base64 sẽ dùng 6 bits, tối thiểu 1 và tối đa 255 ký tự.',
      //     'rg7YwNlESKzMblpYWbngxw==bldkjhfsk'
      //   ];

      case 'AES':
        return [
          'AES là một thuật toán mã hóa đối xứng để bảo vệ dữ liệu trong các hệ thống máy tính và mạng được công bố bởi Viện Tiêu chuẩn và Công nghệ Quốc gia Hoa Kỳ vào năm 2001.',
          'Một ký tự theo AES sẽ dùng 128 bits, mỗi lần có thể mã hóa 16 ký tự.',
          'ImsmnK1PvhwehNqqoi8X3jIzVyH+5+gu1mxkOnJik74='
        ];

      // case 'des':
      //   return [
      //     'DES là một Tiêu chuẩn mã hóa dữ liệu bằng phương pháp khóa đối xứng.',
      //     'Một ký tự theo AES sẽ dùng 64 bits.',
      //     'ImsmnK1PvhwehNqqoi8X3jIzVyH+5+gu1mxkOnJik74='
      //   ];

      case 'RSA':
        return [
          'RSSA là một dạng mã hóa bất đối xứng, được sử dụng hai key khác nhau là public key và private key.',
          'Độ dài của key tầm 1024 đến 2048 bit.',
          'ImsmnK1PvhwehNqqoi8X3jIzVyH+5+gu1mxkOnJik74='
        ];

      case 'MD5':
        return [
          'MD5 là phương pháp mã hóa một chiều, được ứng dụng để kiểm tra tính toàn vẹn của dữ liệu.',
          'Độ dài của key dài 128 bit.',
          'ImsmnK1PvhwehNqqoi8X3jIzVyH+5+gu1mxkOnJik74='
        ];
      case 'SHA256':
        return [
          'SHA256 là cho phép tạo ra các hàm băm mà không thể đảo ngược và mang tính duy nhất.',
          'Độ dài của key dài 256 bit.',
          'ImsmnK1PvhwehNqqoi8X3jIzVyH+5+gu1mxkOnJik74='
        ];

      default:
        return [];
    }
  }, [value]);

  return (
    <BaseModal
      title={'CẤU HÌNH ĐỊNH DẠNG KEY'}
      open={open}
      onInvisile={onCancel}
      onOk={() => !!value && mutate(value as string)}
      loading={isRefetching || loadingSetKeyType || isLoading}
    >
      <label className='mt-3 mb-3 block font-bold'>Kiểu mã hóa</label>

      <Radio.Group
        options={[
          // { label: 'Base64', value: 'base64' },
          { label: 'AES', value: 'AES' },
          // { label: 'DES', value: 'des' },
          { label: 'RSA', value: 'RSA' },
          { label: 'MD5', value: 'MD5' },
          { label: 'SHA256', value: 'SHA256' }
        ]}
        onChange={({ target: { value } }: RadioChangeEvent) => {
          setValue(value);
        }}
        value={value}
        disabled={isLoading}
      />

      <div className='mt-3 block'>
        <div className='flex items-center gap-1'>
          <svg width={16} height={16} viewBox='0 0 16 16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <g clipPath='url(#clip0_8090_1240)'>
              <path
                d='M8 15.5C12.1421 15.5 15.5 12.1421 15.5 8C15.5 3.85786 12.1421 0.5 8 0.5C3.85786 0.5 0.5 3.85786 0.5 8C0.5 12.1421 3.85786 15.5 8 15.5Z'
                fill='#FF7D00'
              />
              <path
                d='M7.38875 9.52414C7.37958 9.46747 7.375 9.41289 7.375 9.36039C7.375 9.30789 7.375 9.25289 7.375 9.19539C7.3682 8.91883 7.41885 8.64387 7.52375 8.38789C7.61532 8.17187 7.74204 7.97251 7.89875 7.79789C8.04884 7.63487 8.21653 7.48898 8.39875 7.36289C8.57542 7.23872 8.74208 7.11372 8.89875 6.98789C9.03854 6.87578 9.16443 6.74737 9.27375 6.60539C9.37471 6.46892 9.42708 6.30258 9.4225 6.13289C9.42517 6.0159 9.39841 5.90011 9.34468 5.79615C9.29096 5.69219 9.21198 5.60338 9.115 5.53789C8.90917 5.38956 8.59042 5.31539 8.15875 5.31539C7.95319 5.31304 7.74869 5.34515 7.55375 5.41039C7.37709 5.47083 7.20637 5.5474 7.04375 5.63914C6.89895 5.72047 6.76062 5.81282 6.63 5.91539C6.50917 6.01122 6.40292 6.09081 6.31125 6.15414L5.625 5.26539C5.79717 5.07938 5.99154 4.91523 6.20375 4.77664C6.41884 4.63568 6.6485 4.51834 6.88875 4.42664C7.12969 4.33454 7.37868 4.26507 7.6325 4.21914C7.87997 4.17407 8.13096 4.15107 8.3825 4.15039C9.15417 4.15039 9.75417 4.31164 10.1825 4.63414C10.388 4.78184 10.554 4.97774 10.666 5.20463C10.7781 5.43153 10.8327 5.68246 10.825 5.93539C10.8352 6.24294 10.7789 6.54907 10.66 6.83289C10.5611 7.05771 10.4222 7.26271 10.25 7.43789C10.081 7.60142 9.89638 7.74796 9.69875 7.87539C9.50914 7.99902 9.3283 8.1356 9.1575 8.28414C8.9917 8.42731 8.8499 8.59612 8.7375 8.78414C8.61604 9.00859 8.55731 9.26163 8.5675 9.51664L7.38875 9.52414ZM7.17625 11.0329C7.17081 10.9236 7.18885 10.8143 7.22917 10.7126C7.26949 10.6108 7.33115 10.5188 7.41 10.4429C7.57921 10.2928 7.80033 10.2148 8.02625 10.2254C8.25891 10.2125 8.48748 10.2905 8.66375 10.4429C8.74253 10.5189 8.80415 10.6108 8.84446 10.7126C8.88478 10.8144 8.90286 10.9236 8.8975 11.0329C8.90218 11.1425 8.88379 11.2519 8.84353 11.354C8.80326 11.456 8.74201 11.5485 8.66375 11.6254C8.489 11.7809 8.2598 11.8609 8.02625 11.8479C7.91395 11.8531 7.80171 11.8362 7.69597 11.798C7.59022 11.7598 7.49305 11.7012 7.41 11.6254C7.33196 11.5484 7.27095 11.4559 7.2309 11.3538C7.19085 11.2517 7.17266 11.1424 7.1775 11.0329H7.17625Z'
                fill='white'
              />
            </g>
            <defs>
              <clipPath id='clip0_8090_1240'>
                <rect width={16} height={16} fill='white' />
              </clipPath>
            </defs>
          </svg>

          <p className='font-bold text-primary-10'>Chú thích</p>
        </div>
      </div>

      <ul className='list-inside list-disc text-primary-10'>
        <li>{note[0] || '--'}</li>

        <li>{note[1] || '--'}</li>
      </ul>

      <p className='mt-2 font-bold text-primary-10'>Ví dụ:</p>
      <p className='ml-6 italic text-primary-10'>{note[2] || '--'}</p>
    </BaseModal>
  );
};

export default KeyConfigurationModal;
