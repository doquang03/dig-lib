import { CaretDownOutlined, DeleteFilled, EditOutlined } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Dropdown, MenuProps, Table, Tooltip } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { newspaperMagazineApis } from 'apis';
import classNames from 'classnames';
import { Button, Empty, Input, ModalDelete, Select, SizeChanger, Title, TitleDelete } from 'components';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import { debounce } from 'lodash';
import { ChangeEvent, Key, useMemo, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getSerialNumber } from 'utils/utils';
import DeleteMulti from '../components/DeleteMulti';

const NewsPaperMagazineListing = () => {
  const [config, setConfig] = useState<{
    TextForSearch?: string;
    isBao?: boolean;
    KyHan?: string;
    page: number;
    pageSize: number;
  }>({
    page: 1,
    pageSize: 30
  });
  const [isDeleteMulti, setIsDeleteMulti] = useState<boolean>(false);
  const [visibleModal, setVisibleModal] = useState<boolean>(false);
  const [selectedRecord, setSelectedRecord] = useState<NewspaperMagazine>();
  const [selectRecords, setSelectRecords] = useState<NewspaperMagazine[]>([]);

  const { isAllowedAdjustment } = useUser();

  const navigate = useNavigate();

  const {
    data: newspaperMagazineData,
    refetch,
    isFetching: loadingNewspaperMagazineData
  } = useQuery({
    queryKey: ['getNewspaperMagazine', config],
    queryFn: () => {
      return newspaperMagazineApis.getData(config);
    }
  });

  const { mutate: deleteNewspaperMagazine, isLoading: loadingDelete } = useMutation({
    mutationFn: newspaperMagazineApis.delete,
    onSuccess: () => {
      toast.success('Xóa thành công');
      refetch();
      setVisibleModal(false);
      setSelectedRecord(undefined);
    }
  });

  const { mutate: deleteNewspaperMagazines, isLoading: loadingDeleteMultil } = useMutation({
    mutationFn: newspaperMagazineApis.deleteMultiRecords,
    onSuccess: () => {
      toast.success('Xóa thành công');
      refetch();
      setIsDeleteMulti(false);
      setSelectRecords([]);
    }
  });

  const newspaperMagazine = useMemo(() => {
    if (!newspaperMagazineData?.data.Item.TotalCount) return;

    return newspaperMagazineData?.data.Item;
  }, [newspaperMagazineData?.data.Item]);

  const columns: ColumnsType<NewspaperMagazine> = [
    {
      key: 'STT',
      title: 'STT',
      dataIndex: 'stt',
      render: (value, record, index) => getSerialNumber(config.page, config.pageSize, index),
      width: 100
    },
    {
      key: 'type',
      title: 'Loại',
      dataIndex: 'type',
      render: (value, record) => (record.isBao ? 'Báo' : 'Tạp chí'),
      ellipsis: true
    },
    {
      key: 'MaBaoTapChi',
      title: 'Mã báo, tạp chí',
      dataIndex: 'MaBaoTapChi',
      render: (value, { MaBaoTapChi }) => (
        <Tooltip title={MaBaoTapChi} arrow={true} placement='topLeft'>
          <p>{MaBaoTapChi}</p>
        </Tooltip>
      ),
      onCell: (record) => ({
        className: 'text-left'
      }),
      ellipsis: true
    },
    {
      key: 'Ten',
      title: 'Tên báo, tạp chí',
      dataIndex: 'Ten',
      render: (value, { Ten }) => (
        <Tooltip title={Ten} arrow={true} className='truncate'>
          <p>{Ten}</p>
        </Tooltip>
      ),
      onCell: (record) => ({
        className: 'text-left'
      }),
      ellipsis: true
    },
    {
      key: 'KyHan',
      title: 'Kỳ hạn phát hành',
      dataIndex: 'KyHan'
    },
    {
      key: 'CQBienTap',
      title: 'Cơ quan biên tập',
      dataIndex: 'CQBienTap',
      render: (value, { CQBienTap }) => (
        <Tooltip title={CQBienTap} arrow={true} className='truncate'>
          <p>{CQBienTap}</p>
        </Tooltip>
      ),
      onCell: (record) => ({
        className: 'text-left'
      }),
      ellipsis: true
    },
    {
      key: 'ISSN',
      title: 'ISSN',
      dataIndex: 'ISSN',
      render: (value, { ISSN }) => (
        <Tooltip title={ISSN} arrow={true} className='truncate'>
          <p>{ISSN}</p>
        </Tooltip>
      ),
      onCell: (record) => ({
        className: 'text-left'
      }),
      ellipsis: true
    },
    {
      key: 'GiaBia',
      title: 'Giá bìa',
      dataIndex: 'GiaBia',
      render: (value, record, index) =>
        new Intl.NumberFormat('vi-VN', {
          style: 'currency',
          currency: 'VND',
          maximumFractionDigits: 9
        }).format(Number(record.GiaBia || 0))
    },
    {
      key: 'actions',
      title: '',
      dataIndex: 'actions',
      width: 220,
      render: (value, record) => {
        return (
          <>
            <Tooltip title='Cập nhật'>
              <button
                className='mx-2'
                onClick={() => navigate(record?.isBao ? `CapNhatBao/${record.Id}` : `CapNhatTapChi/${record.Id}`)}
              >
                <EditOutlined style={{ fontSize: '20px', color: '#08c' }} />
              </button>
            </Tooltip>

            <Tooltip title={record.IsUsed ? 'Đang được đăng ký - Không thể xóa' : 'Xóa'}>
              <button
                className={classNames('', {
                  'mx-2': isAllowedAdjustment,
                  hidden: !isAllowedAdjustment
                })}
                onClick={() => {
                  setVisibleModal(true);
                  setSelectedRecord(record);
                }}
                disabled={record.IsUsed}
              >
                <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
              </button>
            </Tooltip>
          </>
        );
      }
    }
  ];

  const handleAdd = (type: 'newspaper' | 'magazine') => () => {
    type === 'magazine' && navigate(path.addMagazine);
    type === 'newspaper' && navigate(path.addNewspaper);
  };

  const items: MenuProps['items'] = [
    {
      label: 'THÊM MỚI BÁO TỪ EXCEL',
      key: '1',
      onClick: () => navigate(path.addNewspaperExcel)
    },
    {
      label: 'THÊM MỚI TẠP CHÍ TỪ EXCEL',
      key: '2',
      onClick: () => navigate(path.addMagazineExcel)
    }
  ];

  const toogleDeleteMulti = () => setIsDeleteMulti((prevState) => !prevState);

  const handleChange = debounce((e: ChangeEvent<HTMLSelectElement | HTMLInputElement>) => {
    if (!e) return;

    const { name, value } = e.target;

    if (name === 'isBao') {
      if (value) {
        if (value === '0') {
          config.isBao = true;
        }

        if (value === '1') {
          config.isBao = false;
        }

        setConfig((prevState) => {
          return {
            ...prevState,
            page: 1,
            pageSize: 30
          };
        });

        return;
      } else {
        delete config.isBao;

        setConfig(config);
        refetch();
        return;
      }
    }

    setConfig((prevState) => {
      return {
        ...prevState,
        page: 1,
        pageSize: 30,
        [name]: value
      };
    });
  }, 700);

  return (
    <div className='p-5'>
      <Title title='Danh sách báo - tạp chí' />

      <form className='mt-3 flex flex-1 items-center gap-1 bg-primary-50/20 p-2.5'>
        <Input
          placeholder='Nhập mã, tên báo tạp chí'
          containerClassName='md:w-1/4'
          name='TextForSearch'
          onChange={handleChange}
        />

        <Select
          onChange={handleChange}
          name='isBao'
          className='md:w-1/4'
          items={[
            { label: 'Chọn loại', value: '' },
            { label: 'Báo', value: 0 },
            { label: 'Tạp chí', value: 1 }
          ]}
        />

        <Select
          name='KyHan'
          onChange={handleChange}
          className='md:w-1/4'
          items={[
            { value: '', label: 'Chọn kỳ hạn phát hành' },
            { value: 'Hàng tuần', label: 'Hàng tuần' },
            { value: 'Hàng tháng', label: 'Hàng tháng' }
          ]}
        />
      </form>

      <div className='mt-3 flex items-center gap-2'>
        <Button onClick={handleAdd('newspaper')}>Thêm báo</Button>
        <Button onClick={handleAdd('magazine')}>Thêm tạp chí</Button>
        {/* <Dropdown menu={{ items }}>
          <Button>
            Thêm từ file Excel <CaretDownOutlined />
          </Button>
        </Dropdown> */}

        <Button
          variant={!selectRecords.length ? 'secondary' : 'danger'}
          onClick={toogleDeleteMulti}
          disabled={!selectRecords.length}
        >
          Xóa
        </Button>
      </div>

      <Table
        className='custom-table mt-3'
        bordered
        columns={columns}
        locale={{
          emptyText: () => <Empty />
        }}
        dataSource={newspaperMagazine?.ListBaoTapChi}
        loading={loadingNewspaperMagazineData || loadingDelete}
        rowKey={(row) => row.Id}
        pagination={{
          onChange(current, pageSize) {
            setConfig((prevState) => {
              return {
                ...prevState,
                page: current,
                pageSize
              };
            });
          },
          pageSize: config.pageSize,
          hideOnSinglePage: true,
          showSizeChanger: false
        }}
        rowSelection={{
          selectedRowKeys: selectRecords.map((item) => item?.Id as string),
          preserveSelectedRowKeys: true,
          onChange: (_: Key[], selectedRowValue: NewspaperMagazine[]) => {
            setSelectRecords(selectedRowValue);
          }
        }}
      />

      <div
        className={classNames('relative', {
          'mt-[64px]': config.pageSize >= (newspaperMagazine?.TotalCount || 0)
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger
            visible={Boolean(newspaperMagazine?.TotalCount)}
            value={config.pageSize + ''}
            total={newspaperMagazine?.TotalCount + ''}
          />
        </div>
      </div>

      <DeleteMulti
        array={selectRecords}
        visible={isDeleteMulti}
        loading={loadingDeleteMultil}
        onCancel={() => setIsDeleteMulti(false)}
        onOk={() => {
          deleteNewspaperMagazines(selectRecords.map((id) => id.Id));
        }}
      />

      <ModalDelete
        open={visibleModal}
        closable={false}
        title={
          <TitleDelete
            firstText={'Bạn có chắn chắn muốn xóa báo - tạp chí'}
            secondText={selectedRecord?.Ten}
          ></TitleDelete>
        }
        handleCancel={() => {
          setVisibleModal(false);
          setSelectedRecord(undefined);
        }}
        handleOk={() => {
          deleteNewspaperMagazine(selectedRecord?.Id as string);
        }}
        loading={loadingDelete}
      />
    </div>
  );
};

export default NewsPaperMagazineListing;
