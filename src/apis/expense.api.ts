import { omit } from 'lodash';
import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/SoTheoDoiKinhPhi`;

const expenseApis = {
  getMany: (Ngay: SearchRangeField, page: number, pageSize: number) =>
    httpThuVien.post<
      ResponseApi<{
        ListModel: { Id: string; ThoiGianTao: string; NoiDung: string; GiaTien: number; TrangThai: string }[];
        Sum_Thu: number;
        Sum_Chi: number;
        Sum_Ton: number;
        Sum_QuyNganSach: number;
        Total: number;
      }>
    >(`${endPoint}/Index`, {
      Ngay,
      page,
      pageSize
    }),
  create: (payload: {
    ThoiGianTao: string;
    NoiDung: string;
    GiaTien: number;
    TrangThai: string;
    TaoNganSach: boolean;
  }) => httpThuVien.post(`${endPoint}/Insert?TaoNganSach=${payload.TaoNganSach}`, omit(payload, ['TaoNganSach'])),
  update: (payload: { Id: string; ThoiGianTao: string; NoiDung: string; GiaTien: number; TrangThai: string }) =>
    httpThuVien.post(`${endPoint}/Edit`, payload),
  delete: (id: string) => httpThuVien.delete(`${endPoint}/Delete/${id}`),
  getOne: (id: string) =>
    httpThuVien.post<
      ResponseApi<{ Id: string; ThoiGianTao: string; NoiDung: string; GiaTien: number; TrangThai: string }>
    >(`${endPoint}/EditByID?Id=${id}`),
  downloadExcel: (Ngay: SearchRangeField, page: number, pageSize: number) =>
    httpThuVien.post(`${endPoint}/Print`, { Ngay, page, pageSize }, { responseType: 'arraybuffer' }),
  downloadExpenseWord: (Ngay: SearchRangeField, page: number, pageSize: number) =>
    httpThuVien.post(`${endPoint}/PrintSo`, { Ngay, page, pageSize }, { responseType: 'arraybuffer' })
};

export default expenseApis;
