import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import { ConfigProvider } from 'antd';
import { Empty, ErrorBoundary } from 'components';
import { IDENTITY_CONFIG } from 'constants/config';
import { path } from 'constants/path';
import { UserProvider } from 'contexts/user.context';
import { useRouteElements } from 'hooks';
import { useCallback, useEffect } from 'react';
import { CookiesProvider } from 'react-cookie';
import { HelmetProvider } from 'react-helmet-async';
import { AuthProvider } from 'react-oidc-context';
import { useNavigate } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { LocalStorageEventTartget } from 'utils/auth';
// create oidc config
const oidcConfig = {
  authority: IDENTITY_CONFIG.authority,
  client_id: IDENTITY_CONFIG.client_id,
  redirect_uri: IDENTITY_CONFIG.client_id,
  popup_redirect_uri: IDENTITY_CONFIG.popup_redirect_uri,
  silent_redirect_uri: IDENTITY_CONFIG.silent_redirect_uri, //signin-silent-oidc
  response_type: IDENTITY_CONFIG.response_type,
  scope: IDENTITY_CONFIG.scope,
  post_logout_redirect_uri: IDENTITY_CONFIG.post_logout_redirect_uri,
  automaticSilentRenew: IDENTITY_CONFIG.automaticSilentRenew
};

// Create a client
const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      // https://tanstack.com/query/v4/docs/react/guides/window-focus-refetching
      // If a user leaves your application and returns and the query data is stale, TanStack Query automatically requests fresh data for you in the background
      refetchOnWindowFocus: false, // default: true,
      retry: 0
    }
  }
});

const App = () => {
  const routeElements = useRouteElements();
  const navigate = useNavigate();

  if (window.name === '') window.name = window.location.pathname;

  // You must provide an implementation of onSigninCallback to oidcConfig
  // to remove the payload from the URL upon successful login.
  // Otherwise if you refresh the page and the payload is still there,
  // signinSilent - which handles renewing your token - won't work.
  const onSigninCallback = () => {
    window.history.replaceState({}, document.title, window.location.pathname);
  };

  const reset = useCallback(() => {
    navigate(path.home);
  }, [navigate]);

  useEffect(() => {
    LocalStorageEventTartget.addEventListener('clearSS', reset);
    LocalStorageEventTartget.addEventListener('addUnit', reset);

    return () => {
      LocalStorageEventTartget.removeEventListener('clearSS', reset);
      LocalStorageEventTartget.removeEventListener('addUnit', reset);
      // };
    };
  }, [reset]);

  // useEffect(() => {
  //   LocalStorageEventTartget.addEventListener('addUnit', reset);

  //   return () => {
  //     LocalStorageEventTartget.removeEventListener('addUnit', reset);
  //   };
  // }, [reset]);

  return (
    // pass oidc to global application
    <AuthProvider {...oidcConfig} onSigninCallback={onSigninCallback}>
      <ConfigProvider
        renderEmpty={() => <Empty />}
        theme={{
          token: {
            colorPrimary: '#3472A2'
          }
        }}
      >
        <HelmetProvider>
          <QueryClientProvider client={queryClient}>
            <CookiesProvider>
              <UserProvider>
                <ErrorBoundary>{routeElements}</ErrorBoundary>
              </UserProvider>
              {/* The rest of your application */}
              <ReactQueryDevtools initialIsOpen={false} />
              <ToastContainer autoClose={2000} />
            </CookiesProvider>
          </QueryClientProvider>
        </HelmetProvider>
      </ConfigProvider>
    </AuthProvider>
  );
};

export default App;
