import { useMutation } from '@tanstack/react-query';
import { exportApis, historyApis } from 'apis';
import { Button, Input, Loading, Title } from 'components';
import { path } from 'constants/path';
import { useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { download } from 'utils/utils';

const PrintReceipt = () => {
  const [mau, setMau] = useState<number>(1);
  const { state } = useLocation();
  const receiptId = state ? state : undefined;
  const navigate = useNavigate();

  const { mutate } = useMutation({
    mutationFn: historyApis.logger
  });

  const { mutate: downloadFile, isLoading: loadingFile } = useMutation({
    mutationFn: () => exportApis.PrintSo(receiptId, 'Mau' + mau),
    onSuccess: (data) => {
      toast.success('Tải biên bản xuất sách thành công');
      download('BienBanXuatSach.doc', data.data);
    }
  });

  const hanldeOnChangeMau = (mau: number) => () => setMau(mau);

  const handleBack = () => navigate(path.quanlyxuatkho);

  return (
    <div className='p-5'>
      <Title title='IN PHIẾU XUẤT KHO' />

      <p className='my-3 font-bold text-primary-10'>Chọn mẫu in phiếu xuất kho</p>

      <div className='grid grid-cols-12 items-center'>
        <button className='col-span-12 md:col-span-6' onClick={hanldeOnChangeMau(1)}>
          <div className='flex items-center gap-2'>
            <Input type='radio' checked={mau === 1} onChange={hanldeOnChangeMau(1)} />

            <label>Mẫu 1</label>
          </div>

          <img src='/content/MauXK1.png' alt='Mẫu nhập xuất 1' />
        </button>

        <button className='col-span-12 md:col-span-6' onClick={hanldeOnChangeMau(2)}>
          <div className='flex items-center gap-2'>
            <Input type='radio' checked={mau === 2} onChange={hanldeOnChangeMau(2)} />
            <label>Mẫu 2</label>
          </div>

          <img src='/content/MauXK2.png' alt='Mẫu nhập xuất 2' />
        </button>
      </div>

      <div className='mt-2 flex items-center justify-end gap-2'>
        <Button variant='secondary' onClick={handleBack}>
          Quay về
        </Button>

        {/* <a href={`/Tempalates/MauXK${mau}.pdf`} target='_blank' rel='noreferrer'>
          <Button
            onClick={() =>
              mutate({
                message: 'In phiếu xuất kho thành công',
                chucNang: 'Kho sách',
                suKien: 'In phiếu xuất kho',
                chiTiet: ''
              })
            }
          >
            In phiếu xuất kho
          </Button>
        </a> */}

        <Button onClick={() => downloadFile()}>Tải phiếu xuất kho</Button>
      </div>
      <Loading open={loadingFile}></Loading>
    </div>
  );
};

export default PrintReceipt;
