import { DatePicker } from 'antd';
import { Button, Input } from 'components';
import { FORMAT_DATE_PICKER } from 'constants/config';
import dayjs, { Dayjs } from 'dayjs';
import { ChangeEvent } from 'react';

type Props = {
  searchParams: {
    name: string;
    date: string;
  };
  onChangeName: (e: ChangeEvent<HTMLInputElement>) => void;
  onChangeDate: (value: Dayjs | null) => void;
  onRefresh: VoidFunction;
  onSearch: () => Promise<void>;
};

const SearchForm = (props: Props) => {
  const { onChangeDate, onChangeName, onRefresh, onSearch, searchParams } = props;

  return (
    <div className='flex gap-1 bg-[#3D9BE326] p-2'>
      <Input
        placeholder='Nhập tên, mã đơn vị'
        containerClassName='w-full'
        onChange={onChangeName}
        value={searchParams?.name || ''}
      />

      <DatePicker
        format={FORMAT_DATE_PICKER}
        className={'w-full border border-gray-300 py-[0.8rem] px-2 outline-none'}
        placeholder='Chọn ngày gửi bản ghi'
        onChange={onChangeDate}
        value={searchParams?.date ? dayjs(searchParams?.date) : null}
      />

      <div className='flex flex-shrink-0 gap-2'>
        <Button variant='secondary' onClick={onRefresh}>
          Làm mới
        </Button>

        <Button onClick={onSearch}>Tìm kiếm</Button>
      </div>
    </div>
  );
};

export default SearchForm;
