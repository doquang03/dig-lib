import { useQuery } from '@tanstack/react-query';
import { Empty, Table, Tooltip } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { approvalApis } from 'apis';
import classNames from 'classnames';
import { BaseModal, Button, SizeChanger } from 'components';
import { path } from 'constants/path';
import dayjs, { Dayjs } from 'dayjs';
import { useStateAsync } from 'hooks';
import { ChangeEvent, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { getSerialNumber } from 'utils/utils';
import SearchForm from './SearchForm';

const WairForApproval = () => {
  const [params, setParams] = useStateAsync<{
    page: number;
    pageSize: number;
    nameCode: string;
    publishedDate: string;
  }>({ page: 1, pageSize: 30, nameCode: '', publishedDate: '' });
  // selection at listing
  const [selectedRecord, setSelectedRecord] = useState<BookApproval>();
  // selection at modal
  const [selectedSuggestionRecord, setSelectedSuggestionRecord] = useState<string>('');

  const navigate = useNavigate();

  const {
    data: listOFApprovalsData,
    isLoading: loadingListOfApprovals,
    refetch,
    isRefetching
  } = useQuery({
    queryKey: ['getListOfApprovals', params.page, params.pageSize],
    queryFn: () => {
      const _params: { page: number; pageSize: number } & Partial<{
        TextForSearch: string;
        TimeSubmit: string;
      }> = {
        page: params.page,
        pageSize: params.pageSize
      };

      if (params.nameCode) {
        _params.TextForSearch = params.nameCode;
      }

      if (params.publishedDate) {
        _params.TimeSubmit = params.publishedDate;
      }

      return approvalApis.getListOfApprovals({ ..._params, IsComplete: false });
    },
    refetchOnWindowFocus: true
  });

  // get list of duplication documents suggestion
  const {
    data: listOfDuplicationDocumentSuggestionData,
    isLoading: loadingListOfDulicationDocumentSuggestions,
    isFetching
  } = useQuery({
    queryKey: ['listOfDuplicationDocumentSuggestionData', selectedRecord?.Id],
    queryFn: () => approvalApis.getListOfdublicationDocumentsSugestion(selectedRecord?.Id + ''),
    enabled: !!selectedRecord?.Id,
    onSuccess: (data) => {
      const { ListModel, MustMerge } = data.data.Item;

      if (!!ListModel.length) {
        if (ListModel.length === 1 && MustMerge) {
          navigate(path.reviewDocument, {
            state: {
              schoolName: selectedRecord?.NameMember,
              selectedRecordId: selectedRecord?.Id,
              bookId: ListModel?.[0]?.Id
            }
          });
        }
        // NOTE: Mr Thi kiu bỏ đi vì hong cần thiết, có gì cứ liên lạc ổng. many tks
        //  else {
        //   setSelectedSuggestionRecord(ListModel?.[0]?.Id + '');
        // }
      }
    }
  });

  // Key ????
  const listOfListOFApprovals = listOFApprovalsData?.data.Item || { Count: 0, Key: [] };

  // list of duplication documents suggestion

  const { ListModel } = listOfDuplicationDocumentSuggestionData?.data.Item || {
    ListModel: []
  };

  const colums: ColumnsType<BookApproval> = [
    {
      key: 'stt',
      dataIndex: 'stt',
      title: 'STT',
      render: (value, record, index) => getSerialNumber(params.page, params.pageSize, index)
    },
    {
      key: 'Tên đơn vị',
      dataIndex: 'Tên đơn vị',
      title: 'Tên đơn vị',
      render: (value, { NameMember = '' }) =>
        NameMember.length > 40 ? (
          <Tooltip title={NameMember} arrow={true}>
            <p>{NameMember?.substring(0, 40).concat('...')}</p>
          </Tooltip>
        ) : (
          <p>{NameMember}</p>
        ),
      onCell: (record) => ({
        className: 'text-left w-[230px]'
      })
    },
    {
      key: 'Tên tài liệu',
      dataIndex: 'Tên tài liệu',
      title: 'Tên tài liệu',
      render: (value, record) =>
        record?.Title.length > 60 ? (
          <Tooltip title={record?.Title} arrow={true}>
            <p onClick={() => setSelectedRecord(record)} className='text-left'>
              {record?.Title?.substring(0, 60).concat('...')}
            </p>
          </Tooltip>
        ) : (
          <p onClick={() => setSelectedRecord(record)} className='text-left'>
            {record?.Title}
          </p>
        ),
      onCell: (record) => ({
        className: 'text-left hover:underline text-primary-30 hover:cursor-pointer'
      })
    },
    {
      key: 'Loại tài liệu',
      dataIndex: 'BookType',
      title: 'Loại tài liệu',
      render: (value, { BookType }) => (
        <p>
          {BookType === 0 && 'Tài liệu giấy'}
          {BookType === 1 && 'Tài liệu số'}
          {BookType === 2 && 'Tài liệu giấy và số'}
        </p>
      )
    },
    {
      key: 'Author',
      dataIndex: 'Author',
      title: 'Tác giả',
      width: 200
    },
    {
      key: 'Người gửi',
      dataIndex: 'UserName',
      title: 'Người gửi'
    },
    {
      key: 'Thời gian gửi bản ghi',
      dataIndex: 'Thời gian gửi bản ghi',
      title: 'Thời gian gửi bản ghi',
      render: (value, record) =>
        record?.CreateDateTime === '0001-01-01T00:00:00Z'
          ? '--'
          : dayjs(record.CreateDateTime).format('HH:mm DD/MM/YYYY')
    },
    {
      key: 'Phê duyệt',
      dataIndex: 'Phê duyệt',
      title: 'Phê duyệt',
      render: (value, record) => (
        <>
          <button onClick={() => setSelectedRecord(record)} className='flex w-full items-center justify-center'>
            <svg width='15' height='18' viewBox='0 0 15 18' fill='none' xmlns='http://www.w3.org/2000/svg'>
              <path
                d='M12.1154 12.5481C11.7963 12.5481 11.5385 12.8059 11.5385 13.125V16.1538H1.15385V3.46154H11.5385V8.14615C11.5385 8.46518 11.7963 8.72307 12.1154 8.72307C12.4344 8.72307 12.6923 8.46518 12.6923 8.14615V2.88461V0.576923C12.6923 0.257884 12.4344 0 12.1154 0H0.576923C0.257884 0 0 0.257884 0 0.576923V2.88461V16.7308C0 17.0498 0.257884 17.3077 0.576923 17.3077H12.1154C12.4344 17.3077 12.6923 17.0498 12.6923 16.7308V13.125C12.6923 12.8059 12.4344 12.5481 12.1154 12.5481ZM1.15385 1.15385H11.5385V2.30769H1.15385V1.15385Z'
                fill='#3472A2'
              />
              <path
                d='M14.8443 8.25859C14.6262 8.02667 14.2616 8.01398 14.0285 8.23148L8.25585 13.632L5.58643 11.134C5.35451 10.9159 4.98817 10.9274 4.77124 11.1605C4.55317 11.3924 4.56528 11.7576 4.79778 11.9757L7.86182 14.8436C7.97259 14.9474 8.11451 14.9994 8.25643 14.9994C8.39835 14.9994 8.5397 14.9474 8.65105 14.8436L14.8178 9.07436C15.0503 8.85686 15.0618 8.49167 14.8443 8.25859Z'
                fill='#3472A2'
              />
              <path
                d='M2.56032 5.55228H5.15647C5.47551 5.55228 5.73339 5.2944 5.73339 4.97536C5.73339 4.65632 5.47551 4.39844 5.15647 4.39844H2.56032C2.24128 4.39844 1.9834 4.65632 1.9834 4.97536C1.9834 5.2944 2.24128 5.55228 2.56032 5.55228Z'
                fill='#3472A2'
              />
              <path
                d='M9.66339 6.8152C9.66339 6.49617 9.40551 6.23828 9.08647 6.23828H2.56032C2.24128 6.23828 1.9834 6.49617 1.9834 6.8152C1.9834 7.13424 2.24128 7.39213 2.56032 7.39213H9.08647C9.40551 7.39213 9.66339 7.13367 9.66339 6.8152Z'
                fill='#3472A2'
              />
              <path
                d='M2.56032 8.07617C2.24128 8.07617 1.9834 8.33406 1.9834 8.65309C1.9834 8.97213 2.24128 9.23002 2.56032 9.23002H7.03147C7.35051 9.23002 7.60839 8.97213 7.60839 8.65309C7.60839 8.33406 7.35051 8.07617 7.03147 8.07617H2.56032Z'
                fill='#3472A2'
              />
            </svg>
          </button>
        </>
      )
    }
  ];

  const handleChangeName = (e: ChangeEvent<HTMLInputElement>) => {
    if (!e) return;

    setParams((prevState) => {
      return { ...prevState, nameCode: e.target.value };
    });
  };

  const handleChangeDate = (value: Dayjs | null) => {
    setParams((prevState) => {
      return { ...prevState, publishedDate: value ? value?.toISOString() : '' };
    });
  };

  const handleRefresh = async () => {
    await setParams({ page: 1, pageSize: 30, nameCode: '', publishedDate: '' });
    await refetch();
  };

  const handleSearch = async () => {
    if (isRefetching || loadingListOfApprovals) {
      return;
    }

    await setParams((prevState) => {
      return { ...prevState, page: 1, pageSize: 30 };
    });

    await refetch();
  };

  return (
    <div className='mt-3'>
      <SearchForm
        onChangeName={handleChangeName}
        onChangeDate={handleChangeDate}
        onRefresh={handleRefresh}
        onSearch={handleSearch}
        searchParams={{ name: params.nameCode, date: params.publishedDate }}
      />

      <Table
        columns={colums}
        className='custom-table my-3'
        bordered
        pagination={{
          current: params.page,
          pageSize: params.pageSize,
          total: listOfListOFApprovals.Count,
          onChange: (page, pageSize) => {
            setParams((prevState) => {
              return { ...prevState, page, pageSize };
            });
          },
          hideOnSinglePage: true,
          showSizeChanger: false,
          showQuickJumper: true,
          locale: { jump_to: '', page: '' }
        }}
        loading={loadingListOfApprovals || isRefetching}
        rowKey={(row) => row.Id}
        dataSource={listOfListOFApprovals.Key}
      />

      <div
        className={classNames('relative', {
          'mt-[64px]': params.pageSize >= listOfListOFApprovals.Count
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger
            currentPage={params.page + ''}
            visible={!!listOfListOFApprovals.Count}
            value={params.pageSize + ''}
            total={listOfListOFApprovals.Count + ''}
            onChange={(pageSize) =>
              setParams((prevState) => {
                return { ...prevState, page: 1, pageSize: +pageSize };
              })
            }
          />
        </div>
      </div>

      <BaseModal
        title={'DANH SÁCH BẢN GHI ĐỀ XUẤT TRÙNG'}
        open={!!selectedRecord}
        loading={loadingListOfDulicationDocumentSuggestions || isFetching}
        onInvisile={() => {
          setSelectedSuggestionRecord('');
          setSelectedRecord(undefined);
        }}
        onOk={() => {
          setSelectedRecord(undefined);

          navigate(path.reviewDocument, {
            state: {
              schoolName: selectedRecord?.NameMember,
              selectedRecordId: selectedRecord?.Id,
              bookId: selectedSuggestionRecord || ''
            }
          });
        }}
        width={905}
        labelOk='Tiếp tục'
      >
        <div className='my-3 flex items-center gap-1'>
          <svg width='22' height='22' viewBox='0 0 22 22' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <g clipPath='url(#clip0_7198_600)'>
              <path
                d='M11 21.3125C16.6954 21.3125 21.3125 16.6954 21.3125 11C21.3125 5.30456 16.6954 0.6875 11 0.6875C5.30456 0.6875 0.6875 5.30456 0.6875 11C0.6875 16.6954 5.30456 21.3125 11 21.3125Z'
                fill='#FF7D00'
              />
              <path
                d='M10.1595 13.0952C10.1469 13.0173 10.1406 12.9422 10.1406 12.87C10.1406 12.7979 10.1406 12.7222 10.1406 12.6432C10.1313 12.2629 10.2009 11.8848 10.3452 11.5329C10.4711 11.2358 10.6453 10.9617 10.8608 10.7216C11.0672 10.4975 11.2977 10.2969 11.5483 10.1235C11.7912 9.95276 12.0204 9.78088 12.2358 9.60786C12.428 9.45371 12.6011 9.27714 12.7514 9.08192C12.8902 8.89428 12.9622 8.66556 12.9559 8.43224C12.9596 8.27137 12.9228 8.11216 12.8489 7.96921C12.7751 7.82627 12.6665 7.70417 12.5331 7.61411C12.2501 7.41015 11.8118 7.30817 11.2183 7.30817C10.9356 7.30494 10.6545 7.34909 10.3864 7.4388C10.1435 7.5219 9.90876 7.62719 9.68516 7.75333C9.48605 7.86515 9.29585 7.99214 9.11625 8.13317C8.9501 8.26494 8.80401 8.37437 8.67797 8.46145L7.73438 7.23942C7.97111 6.98366 8.23837 6.75796 8.53016 6.56739C8.8259 6.37358 9.14168 6.21223 9.47203 6.08614C9.80332 5.9595 10.1457 5.86398 10.4947 5.80083C10.835 5.73886 11.1801 5.70723 11.5259 5.7063C12.587 5.7063 13.412 5.92802 14.0009 6.37145C14.2835 6.57454 14.5118 6.84391 14.6658 7.15588C14.8198 7.46786 14.8949 7.8129 14.8844 8.16067C14.8984 8.58355 14.821 9.00448 14.6575 9.39474C14.5215 9.70387 14.3305 9.98573 14.0938 10.2266C13.8614 10.4515 13.6075 10.653 13.3358 10.8282C13.0751 10.9982 12.8264 11.186 12.5916 11.3902C12.3636 11.5871 12.1686 11.8192 12.0141 12.0777C11.8471 12.3863 11.7663 12.7343 11.7803 13.0849L10.1595 13.0952ZM9.86734 15.1697C9.85986 15.0194 9.88467 14.8692 9.94011 14.7293C9.99555 14.5893 10.0803 14.4629 10.1887 14.3585C10.4214 14.1522 10.7255 14.0448 11.0361 14.0594C11.356 14.0417 11.6703 14.1489 11.9127 14.3585C12.021 14.463 12.1057 14.5894 12.1611 14.7293C12.2166 14.8693 12.2414 15.0194 12.2341 15.1697C12.2405 15.3205 12.2152 15.4709 12.1599 15.6112C12.1045 15.7515 12.0203 15.8787 11.9127 15.9844C11.6724 16.1982 11.3572 16.3082 11.0361 16.2904C10.8817 16.2976 10.7274 16.2743 10.582 16.2218C10.4366 16.1693 10.3029 16.0886 10.1887 15.9844C10.0814 15.8785 9.99755 15.7513 9.94248 15.611C9.88742 15.4707 9.8624 15.3203 9.86906 15.1697H9.86734Z'
                fill='white'
              />
            </g>
            <defs>
              <clipPath id='clip0_7198_600'>
                <rect width='22' height='22' fill='white' />
              </clipPath>
            </defs>
          </svg>

          <p className='font-bold text-primary-10'>Hướng dẫn:</p>
        </div>

        <ul className='ml-6 list-inside list-disc'>
          <li className='text-primary-10'>
            Đánh dấu <span className='font-bold'>Chọn</span> vào tài liệu của thư viện mà bạn so sánh thông tin bản ghi
            và bấm Tiếp tục.
          </li>

          <li className='text-primary-10'>
            Nếu không <span className='font-bold'>Chọn</span> vào tài liệu nào và bấm{' '}
            <span className='font-bold'>Tiếp tục</span> thì hệ thống sẽ tiến hành thêm mới tài liệu này
          </li>
        </ul>

        <div className='max-h-[700px] overflow-y-auto'>
          {loadingListOfDulicationDocumentSuggestions || isFetching ? (
            <div className='grid place-items-center'>Đang tải dữ liệu</div>
          ) : (
            <>
              {ListModel.map((book) => (
                <div className='my-3 grid grid-cols-12 items-center gap-2 border-b-[0.5px] shadow' key={book.Id}>
                  <div className='col-span-3'>
                    <img src='/content/Book.png' alt='Hình nền' />
                  </div>

                  <div className='col-span-7'>
                    <p>
                      <span className='text-primary-20'>{book.TenSach} </span>

                      <span>
                        / {book.TacGia}. - Tái bản lần thứ {book.TaiBan}.- {book.NoiXuatBan}. {book.NhaXuatBan}
                        {book.NamXuatBan}. - {book.SoTrang}tr. {book.MinhHoa ? `:${book.MinhHoa};` : ''}{' '}
                        {book.KhoMau ? `${book.KhoMau}.` : ''}- {book.TungThu}
                      </span>
                    </p>

                    <p className='my-1'>
                      <span className='font-bold'>ISBN</span>
                      <span>: {book.ISBN || '--'}</span>
                    </p>

                    <p className='my-1'>
                      <span className='font-bold'>Tóm tắt</span>
                      <span>: {book.TomTat || '--'}</span>
                    </p>

                    <p className='my-1'>
                      <span className='font-bold'>Nguồn: </span>

                      {book?.Nguon?.length
                        ? book.Nguon.map((item) => (
                            <span className='text-primary-20' key={item}>
                              : {item}
                            </span>
                          ))
                        : ' --'}
                    </p>
                  </div>

                  <div className='col-span-2'>
                    {book.Id === selectedSuggestionRecord ? (
                      <Button onClick={() => setSelectedSuggestionRecord('')} variant='secondary'>
                        Bỏ chọn
                      </Button>
                    ) : (
                      <Button onClick={() => setSelectedSuggestionRecord(book.Id + '')}>Chọn</Button>
                    )}
                  </div>
                </div>
              ))}
            </>
          )}
        </div>

        {!loadingListOfDulicationDocumentSuggestions && !ListModel.length && (
          <Empty
            description={<span className='text-[#A7A7A7]'>Không tìm thấy bản ghi trùng. Bạn có muốn tạo mới?</span>}
            className='mt-3'
          />
        )}
      </BaseModal>
    </div>
  );
};

export default WairForApproval;
