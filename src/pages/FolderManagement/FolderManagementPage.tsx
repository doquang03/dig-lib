import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { Modal } from 'antd';
import { folderApis } from 'apis';
import { AudioIcon, EBookIcon, EGuidingIcon, ImageIcon, PaperBookIcon, VideoIcon } from 'assets';
import { Button, Loading, Title } from 'components';
import { path } from 'constants/path';
import { useOutsideClick, useQueryParams, useWindowDimensions } from 'hooks';
import { ChangeEvent, MouseEvent, MutableRefObject, useCallback, useEffect, useState } from 'react';
import { Outlet, useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { BottomBar, ContextMenu, MenuItem, Toolbar } from './components';
import { Menu } from './components/Toolbar';
import { useFolderManagement } from './contexts/useFolderManagement';

const MENU_ICONS = [<PaperBookIcon />, <EBookIcon />, <AudioIcon />, <ImageIcon />, <VideoIcon />, <EGuidingIcon />];

const DEFAULT_FOLDER_NAME = 'Thư mục mới';

const DEFAULT_UNDETERMINED_ROUTE = 'chua-xac-dinh';

export type MapFolder = Record<string, Menu & { children: Menu[] }>;

function FolderManagementPage() {
  const [menuData, setMenuData] = useState<Menu[]>([]);
  const [selectedMenuIds, setSelectedMenuIds] = useState<string[]>([]);
  const [{ show, selectedItem, y }, setContextMenu] = useState<{
    show: boolean;
    selectedItem?: Menu;
    y: number;
  }>({
    show: false,
    y: 0
  });
  const [itemEditable, setItemEditable] = useState<Menu>();
  const [existedFolder, setExistedFolder] = useState<(Menu & { show: boolean }) | undefined>();
  const [mapFolder, setMapFolder] = useState<MapFolder>();

  const { activeFolder, selectedBookIds, setActiveFolder, setSelectedBookIds, setMapSelectedBookIds } =
    useFolderManagement();

  const { id } = useParams();

  const { height } = useWindowDimensions();

  const { search } = useQueryParams();

  const queryClient = useQueryClient();

  const navigate = useNavigate();

  const ref = useOutsideClick<HTMLDivElement>(
    useCallback(() => {
      setContextMenu({ show: false, y: 0 });
    }, [])
  );

  const {
    data: foldersData,
    isLoading: loadingMenuData,
    refetch
  } = useQuery({
    queryKey: ['getFolder'],
    queryFn: folderApis.getFolders,
    onSuccess: (data) => {
      const { Count = 0, ListThuMucSach = [] } = data.data.Item;

      const folders: Menu[] = [
        {
          id: 'paper',
          label: 'Sách in',
          type: 'paper',
          children: []
        },
        {
          id: 'document',
          label: 'Sách điện tử',
          type: 'document',
          children: []
        },
        {
          id: 'audio',
          label: 'Sách nói',
          type: 'audio',
          children: []
        },
        {
          id: 'video',
          label: 'Video',
          type: 'video',
          children: []
        },
        {
          id: 'image',
          label: 'Album ảnh',
          type: 'image',
          children: []
        }
      ];

      function createTree(inputArray: Array<Menu & { children?: Menu[] }>) {
        const tree: Array<Menu & { children: Menu[] }> = [];
        const map: { [key: string]: Menu & { children: Menu[] } } = {};

        const mapSelectedBooks: Record<string, string[]> = {};

        // Create a map to efficiently look up objects by their id
        inputArray.forEach((item) => {
          map[item.id] = {
            id: item.id,
            label: item.label,
            idParent: item.idParent,
            type: item.type,
            totalBooks: item.totalBooks || 0,
            isDetermined: item.isDetermined,
            children: []
          };

          mapSelectedBooks[item.id] = [];
        });

        inputArray.forEach((item) => {
          if (item.idParent && map[item.idParent]) {
            map[item.idParent].children.push(map[item.id]);
          } else {
            tree.push(map[item.id]);
          }
        });

        setMapFolder(map);

        setMapSelectedBookIds(mapSelectedBooks);

        return tree;
      }

      const a = createTree(
        ListThuMucSach.map((item) => ({
          id: item.Id,
          idParent: item.IdParent,
          label: item.TenThuMuc,
          type: item.Type,
          totalBooks: item.CountBook,
          isDetermined: item.IsActive
        }))
      );

      const hello: Record<FolderType, Array<Menu & { children?: Menu[] }>> = {
        paper: a.map((item) => ({ ...item, idParent: 'paper' })).filter((item) => item.type === 'paper'),
        audio: a.map((item) => ({ ...item, idParent: 'audio' })).filter((item) => item.type === 'audio'),
        document: a.map((item) => ({ ...item, idParent: 'document' })).filter((item) => item.type === 'document'),
        image: a.map((item) => ({ ...item, idParent: 'image' })).filter((item) => item.type === 'image'),
        video: a.map((item) => ({ ...item, idParent: 'video' })).filter((item) => item.type === 'video')
      };

      for (let index = 0; index < folders.length; index++) {
        const element = folders[index];

        const children = [...hello[element.id as FolderType]];

        element.children = children.filter((item) => item.isDetermined);

        if (!children.length) continue;
        else {
          element.children.push({
            id: `${element.type}-${DEFAULT_UNDETERMINED_ROUTE}`,
            label: 'Chưa xác định',
            children: children.filter((item) => !item.isDetermined),
            idParent: element.idParent,
            isDetermined: element.isDetermined,
            totalBooks: 0,
            type: element.type
          });
        }
      }

      setMenuData(folders);
    }
  });

  const { mutate: addNewFolder, isLoading: loadingAddNewFolder } = useMutation({
    mutationFn: folderApis.addNewFolder,
    onSuccess: (data) => {
      refetch();

      const { Id, IdParent, Type, TenThuMuc } = data.data.Item;

      data.data.Message.includes('Tạo mới thành công.')
        ? setItemEditable({ id: Id, label: TenThuMuc, idParent: IdParent, type: Type })
        : setItemEditable(undefined);

      setSelectedMenuIds((prevState) => [...prevState, IdParent || Type]);
    },
    onError: () => {
      const { ListThuMucSach = [] } = foldersData?.data?.Item || { ListThuMucSach: [], Count: 0 };

      const _existedFolder = ListThuMucSach.find((item) => item.TenThuMuc === itemEditable?.label);

      _existedFolder &&
        setExistedFolder({
          ...{
            id: _existedFolder.Id,
            label: _existedFolder.TenThuMuc,
            idParent: _existedFolder.IdParent,
            type: _existedFolder.Type
          },
          show: true
        });
    }
  });

  const { mutateAsync: deleteFolder, isLoading: loadingDeleteFolder } = useMutation({
    mutationFn: folderApis.deleteFolder,
    onSuccess: (data) => {
      toast.success(data.data.Message);

      refetch();
    }
  });

  const { mutate: mergeFolders, isLoading: loadingMergeFolders } = useMutation({
    mutationFn: folderApis.mergeFoldersWithSameName,
    onSuccess: (data) => {
      toast.success(data.data.Message);

      setItemEditable(undefined);

      setExistedFolder(undefined);

      refetch();
    }
  });

  const rootId = ['paper', 'audio', 'document', 'image', 'video'].includes((selectedItem?.id as FolderType) || '')
    ? ''
    : selectedItem?.id || '';

  const { mutate: moveBooksToFolder, isLoading: loadingMoveBooksToFolder } = useMutation({
    mutationFn: () => folderApis.moveBooksToFolder(rootId, selectedBookIds),
    onSuccess: (data) => {
      toast.success(data.data.Message);

      refetch();

      setSelectedBookIds([]);

      setContextMenu({ show: false, y: 0 });

      queryClient.invalidateQueries(['getBooksInFolders', id, search]);
    }
  });

  useEffect(() => {
    if (!id && activeFolder) return;

    function getActiveFolder(array: Menu[]) {
      for (let index = 0; index < array.length; index++) {
        const element = array[index];

        if (element.id === id) {
          setActiveFolder(element);
        } else {
          if (element?.children?.length) {
            getActiveFolder(element?.children as Menu[]);
          }
        }
      }
    }

    function magic(arr: Menu[], targetId: string, parent: string[] = []): string[] {
      for (const item of arr) {
        if (item.id === targetId) {
          return [...parent, item.id];
        }

        if (item.children) {
          const result = magic(item.children, targetId, [...parent, item.id]);

          if (result.length > 0) {
            return result;
          }
        }
      }

      return [];
    }

    const result = magic(menuData, activeFolder?.id || '');

    setSelectedMenuIds(result);

    setSelectedBookIds([]);

    getActiveFolder(menuData);
  }, [id, menuData, activeFolder]);

  function handleDropdownMenu(menuKey: string) {
    if (!menuKey) {
      return;
    }

    const isExisting = selectedMenuIds.includes(menuKey);

    if (isExisting) {
      setSelectedMenuIds((prevState) => prevState.filter((item) => item !== menuKey));
    } else {
      setSelectedMenuIds((prevState) => [...prevState, menuKey]);
    }
  }

  function handleShowContextMenu(e: MouseEvent<HTMLAnchorElement, globalThis.MouseEvent>, item: Menu) {
    e.preventDefault();

    const { clientY: y } = e;

    setContextMenu({ show: true, selectedItem: item, y });
  }

  function handleAddNewFolder(selectedItem?: Menu) {
    setContextMenu({ show: false, y: 0 });

    navigate(`${path.folderManagement}/${selectedItem?.id}`);

    let count: number = 0;
    let array: number[] = [];

    if (selectedItem?.children?.length) {
      if (selectedItem.children.every((item) => item.label !== DEFAULT_FOLDER_NAME)) {
        return addNewFolder({
          Id: '',
          // @ts-ignore
          IdParent: ['paper', 'audio', 'document', 'image', 'video'].includes(itemEditable?.idParent || '')
            ? ''
            : itemEditable?.idParent,
          IsActive: true,
          TenThuMuc: DEFAULT_FOLDER_NAME,
          Type: selectedItem?.type as FolderType
        });
      }

      for (let index = 0; index < selectedItem?.children.length; index++) {
        const element = selectedItem?.children[index];
        const bienChoNoNgan = element.label.split(/[()]+/);

        if (
          bienChoNoNgan[0].trimEnd() === DEFAULT_FOLDER_NAME &&
          typeof +bienChoNoNgan[1] === 'number' &&
          bienChoNoNgan[2] === ''
        ) {
          array.push(+bienChoNoNgan[1]);
        }
      }

      array.sort((a: number, b: number) => a - b);

      if (array.length === 0) {
        count++;
      }

      for (let index = 0; index < array.length; index++) {
        const element = array[index];

        if (element !== index + 1) {
          count = index + 1;
          break;
        } else {
          if (index === array.length - 1) {
            count = index + 2;
          }
        }
      }
    }

    addNewFolder({
      Id: '',
      // @ts-ignore
      IdParent: ['paper', 'audio', 'document', 'image', 'video'].includes((selectedItem?.id as FolderType) || '')
        ? ''
        : selectedItem?.id || '',
      IsActive: true,
      TenThuMuc: count >= 1 ? `${DEFAULT_FOLDER_NAME} (${count})` : DEFAULT_FOLDER_NAME,
      Type: selectedItem?.type as FolderType
    });
  }

  function handleEditFolder() {
    if (!selectedItem) {
      return;
    }

    setContextMenu({ show: false, y: 0 });

    setItemEditable(selectedItem);
  }

  function handleSubmitFolderEditing() {
    if (!itemEditable) {
      return;
    }

    addNewFolder({
      Id: itemEditable?.id + '',
      IdParent: ['paper', 'audio', 'document', 'image', 'video'].includes(itemEditable.idParent + '')
        ? ''
        : itemEditable?.idParent || '',
      IsActive: true,
      TenThuMuc: itemEditable?.label.trimEnd().trimStart() || '',
      Type: itemEditable?.type as FolderType
    });
  }

  function handleChangeFolderName(e: ChangeEvent<HTMLInputElement>, folder: Menu) {
    const { value } = e.target;

    const { id, idParent, type } = folder;

    setItemEditable((prevState) => ({ ...prevState, id, label: value || '', idParent: idParent, type: type }));
  }

  async function handleDeleteFolder() {
    if (!selectedItem) {
      return;
    }

    await deleteFolder(selectedItem?.id);

    navigate(`${path.folderManagement}/${selectedItem?.idParent}`);

    setContextMenu({ show: false, y: 0 });
  }

  function handleMergeFolders() {
    mergeFolders({ idFrom: itemEditable?.id || '', idTarget: existedFolder?.id || '' });
  }

  return (
    <div className='relative mt-3 flex flex-1 flex-col'>
      <Title title='QUẢN LÝ THƯ MỤC TÀI LIỆU' />

      <Toolbar menu={menuData} />

      <div className='-mb-7 flex grow flex-col'>
        <div className='flex gap-2'>
          <div
            className='flex w-1/5 flex-col gap-y-1 overflow-y-auto'
            style={{
              maxHeight: height - 50 - 42 - 150
            }}
          >
            {menuData.map((item, index) => (
              <MenuItem
                {...item}
                mapFolder={mapFolder}
                totalBooks={mapFolder?.[item.id]?.totalBooks || 0}
                editable={itemEditable}
                key={item.id}
                onDropdownClick={handleDropdownMenu}
                selectedMenuIds={selectedMenuIds}
                icon={MENU_ICONS[index]}
                onContextMenu={handleShowContextMenu}
                selectedMenuContexItem={selectedItem}
                isModify={selectedItem?.id === item.id}
                onChangeFolder={handleChangeFolderName}
                onSubmitChange={handleSubmitFolderEditing}
              />
            ))}

            {/* 272 is menu item's width */}
            <ContextMenu show={show} x={272} y={y - 32 * 2} ref={ref as MutableRefObject<HTMLDivElement>}>
              {!selectedItem?.idParent ? (
                <ul className='list-none'>
                  <li className='hover:bg-[#CBE8F6]'>
                    <button onClick={() => handleAddNewFolder(selectedItem)}>Thêm thư mục con mới</button>
                  </li>
                </ul>
              ) : (
                <ul className='flex list-none flex-col gap-1'>
                  <li className='hover:bg-[#CBE8F6]'>
                    <button className='w-full text-left' onClick={() => handleAddNewFolder(selectedItem)}>
                      Thêm thư mục con mới
                    </button>
                  </li>

                  {!!selectedBookIds.length ? (
                    <li className='hover:bg-[#CBE8F6]'>
                      <button onClick={() => moveBooksToFolder()}>Dán sách đã chọn vào thư mục</button>
                    </li>
                  ) : null}

                  <li className='hover:bg-[#CBE8F6]'>
                    <button className='w-full text-left' onClick={handleEditFolder}>
                      Đổi tên
                    </button>
                  </li>

                  <li className='hover:bg-[#CBE8F6]'>
                    <button className='w-full text-left text-[#A23434]' onClick={handleDeleteFolder}>
                      Xóa thư mục
                    </button>
                  </li>
                </ul>
              )}
            </ContextMenu>
          </div>

          <Outlet />
        </div>
      </div>

      <Modal
        footer={null}
        closable={false}
        centered
        open={existedFolder?.show}
        onCancel={() => setExistedFolder(undefined)}
        onOk={() => {}}
      >
        <div className='flex flex-col items-center justify-center'>
          <svg width='124' height='124' viewBox='0 0 124 124' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path
              d='M55.8 80.6H68.2V93H55.8V80.6ZM55.8 31H68.2V68.2H55.8V31ZM61.938 0C27.714 0 0 27.776 0 62C0 96.224 27.714 124 61.938 124C96.224 124 124 96.224 124 62C124 27.776 96.224 0 61.938 0ZM62 111.6C34.596 111.6 12.4 89.404 12.4 62C12.4 34.596 34.596 12.4 62 12.4C89.404 12.4 111.6 34.596 111.6 62C111.6 89.404 89.404 111.6 62 111.6Z'
              fill='#D9C510'
            />
          </svg>

          <p className='mt-4 text-center text-[20px]'>
            Thư mục <span className='font-bold'>{existedFolder?.label}</span> đã tồn tại.
          </p>

          <p className='text-center text-[20px]'>Bạn có muốn hợp nhất thành một tài liệu không?</p>

          <p className='mt-2 text-center text-xs text-[#00000061]'>
            Lưu ý: Sau khi xác nhận thì tất cả tài liệu trong thư mục này sẽ được di chuyển vào thư mục kia
          </p>

          <div className='mt-3 flex items-center justify-center gap-2'>
            <Button
              variant='secondary'
              onClick={() => setExistedFolder(undefined)}
              type='button'
              loading={loadingMergeFolders}
            >
              Quay về
            </Button>

            <Button type='button' onClick={handleMergeFolders} loading={loadingMergeFolders}>
              Xác nhận
            </Button>
          </div>
        </div>
      </Modal>

      <BottomBar />

      <Loading open={loadingDeleteFolder || loadingAddNewFolder || loadingMenuData} />
    </div>
  );
}

export default FolderManagementPage;
