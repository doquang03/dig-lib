import { EditOutlined } from '@ant-design/icons';
import { useQuery } from '@tanstack/react-query';
import { Tooltip } from 'antd';
import Table, { type ColumnsType } from 'antd/es/table';
import { libraryMonitorApis } from 'apis';
import classNames from 'classnames';
import { Button, Empty, Input, Select, Title } from 'components';
import { CLASSIFY_OPTIONS } from 'constants/options';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import { usePaginationNavigate, useQueryConfig } from 'hooks';
import { isEmpty, omitBy, trim } from 'lodash';
import { useEffect, useMemo } from 'react';
import { useForm } from 'react-hook-form';
import { Link, createSearchParams, useNavigate } from 'react-router-dom';
import { convertDate } from 'utils/utils';

type Mock = {
  Mail: string;
  GioiTinh: string;
  Id: number;
  Ten: string;
  TrangThai: number;
  NgaySinh: string;
};

type FormInput = {
  TextForSearch: string;
  PhanLoai: string;
};

const LibraryMonitorManagermentContent = () => {
  const { register, handleSubmit, reset, setValue } = useForm<FormInput>({
    defaultValues: {
      TextForSearch: '',
      PhanLoai: ''
    }
  });

  const navigate = useNavigate();

  const handleNavigation = usePaginationNavigate();

  const { isAllowedAdjustment } = useUser();

  const queryConfig = useQueryConfig();

  // eslint-disable-next-line no-empty-pattern
  const {
    data: dataCanBo,
    isFetching,
    refetch
  } = useQuery({
    queryKey: ['CanBoThuVien', queryConfig.TextForSearch, queryConfig.PhanLoai],
    queryFn: () => {
      let queryString = `?`;
      if (queryConfig.TextForSearch !== undefined) queryString += `&TextForSearch=${queryConfig.TextForSearch}`;
      if (queryConfig.PhanLoai !== undefined) queryString += `&PhanLoai=${queryConfig.PhanLoai}`;
      let request = libraryMonitorApis.Index(queryString);
      return request;
    }
  });

  const listCanBo = useMemo(() => {
    if (!dataCanBo?.data?.Item) return;
    const { ListThanhVien } = dataCanBo?.data?.Item;
    return ListThanhVien;
  }, [dataCanBo?.data?.Item]);

  const columns: ColumnsType<Mock> = useMemo(() => {
    return [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        render: (value, record, index) => {
          return <>{index + 1}</>;
        }
      },
      {
        title: 'Tên cán bộ thư viện',
        dataIndex: 'Ten',
        key: 'Ten',
        render: (value, { Ten }) =>
          Ten.length > 25 ? (
            <Tooltip title={Ten} arrow={true} className='text-left'>
              <p className='text-left'>{Ten.substring(0, 25).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-left'>{Ten}</p>
          ),
        onCell: (record) => ({
          className: 'text-left w-[280px]'
        })
      },
      {
        title: 'Phân loại',
        dataIndex: 'PhanLoai',
        key: 'PhanLoai'
      },
      {
        title: 'Ngày sinh',
        dataIndex: 'NgaySinh',
        key: 'NgaySinh',
        render: (value, { NgaySinh }) => <p>{convertDate(NgaySinh)}</p>
      },
      {
        title: 'Số điện thoại',
        dataIndex: 'SDT',
        key: 'SDT'
      },
      {
        title: 'Trình độ chuyên môn',
        dataIndex: 'TrinhDoChuyenMon',
        key: 'TrinhDoChuyenMon'
      },
      {
        title: 'Email',
        dataIndex: 'Mail',
        key: 'Mail'
      },
      {
        title: 'Năm công tác',
        dataIndex: 'NamCongTac',
        key: 'NamCongTac'
      },
      {
        title: 'Hành động',
        dataIndex: 'actions',
        key: 'actions',
        render: (value, record) => {
          return (
            <>
              <Tooltip title='Cập nhật'>
                <Link to={path.giaovien + `/CapNhat/${record.Id}?type=Library`} className='mx-2'>
                  <EditOutlined style={{ fontSize: '20px', color: '#08c' }} />
                </Link>
              </Tooltip>
            </>
          );
        }
      }
    ];
  }, []);

  useEffect(() => {
    Object.keys(queryConfig).forEach((key) => {
      if (!key) {
        return;
      }
      // @ts-ignore
      setValue(key, queryConfig[key]);
    });
  }, [queryConfig, setValue]);

  const onSubmit = handleSubmit((data) => {
    handleNavigation({
      ...omitBy({ ...data, TextForSearch: trim(data.TextForSearch, ' ').replace(/\s\s+/g, ' ') }, isEmpty)
    });
  });

  const handleResetField = () => {
    const { pathname } = window.location;

    navigate({
      pathname: pathname,
      search: createSearchParams({}).toString()
    });

    reset();
  };

  // TODO: Do something with selected LibraryMonitors
  const handleSelectedLibraryMonitors = () => {
    navigate(path.themcanbothuvien);
  };

  let locale = {
    emptyText: 'Không tìm được kết quả phù hợp'
  };

  return (
    <div className='p-5'>
      <Title title='DANH SÁCH CÁN BỘ THƯ VIỆN' />

      <form className='my-5 flex flex-col flex-wrap gap-2 bg-secondary-10 py-3 px-3.5 md:flex-row' onSubmit={onSubmit}>
        <Input
          placeholder='Nhập tên cán bộ thư viện'
          containerClassName='w-[100%] md:w-[460px]'
          name='TextForSearch'
          register={register}
        />

        <Select items={CLASSIFY_OPTIONS} className='w-[100%] md:w-[460px]' name='PhanLoai' register={register} />

        <Button type='button' variant='secondary' className='font-semibold' onClick={handleResetField}>
          Làm mới
        </Button>

        <Button type='submit' variant='default' className='font-semibold'>
          Tìm kiếm
        </Button>
      </form>

      <Button
        variant={'default'}
        className={classNames('w-[100%] md:w-auto', {
          hidden: !isAllowedAdjustment,
          'w-[100%] md:w-auto': isAllowedAdjustment
        })}
        onClick={handleSelectedLibraryMonitors}
      >
        Chọn cán bộ
      </Button>

      <div className='relative my-6'>
        <Table
          loading={isFetching}
          columns={columns}
          pagination={false}
          dataSource={listCanBo || []}
          scroll={{ x: 980 }}
          rowKey={(record) => record.Id}
          className='custom-table'
          locale={{
            emptyText: () => <Empty label={locale.emptyText} />
          }}
          bordered
        />
      </div>
    </div>
  );
};

export default LibraryMonitorManagermentContent;
