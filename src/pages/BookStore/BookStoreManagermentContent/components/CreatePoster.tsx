import { useMutation } from '@tanstack/react-query';
import { Modal, ModalProps, Radio } from 'antd';
import { bookStoreApis } from 'apis';
import classNames from 'classnames';
import { Button } from 'components';
import { useState } from 'react';
import { toast } from 'react-toastify';

type Props = {
  ids: string[];
  IdKho: string;
  onHide: VoidFunction;
  onSuccess: VoidFunction;
} & ModalProps;

const CreatePoster = (props: Props) => {
  const [classify, setClassify] = useState('MonLoai');
  const [display, setDisplay] = useState('TenSach4');
  const [imageUrl, setImageUrl] = useState('MauPhich1.png');
  const [imageUrl1, setImageUrl1] = useState('MauPhich3.png');
  const { ids, IdKho, open, onHide, onSuccess } = props;

  const { mutate: exportPhich, isLoading } = useMutation({
    mutationFn: (listIdSachCaBiet: string[]) =>
      bookStoreApis.XuatMauPhichSach_ActionResult(listIdSachCaBiet, '_' + classify + '_' + display, IdKho),
    onSuccess: (data) => {
      const blob = new Blob([data.data]);
      const link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob);
      link.download = `Mẫu_Phích_Sách.doc`;
      link.click();

      toast.success('Xuất phích thành công');
      onHide();
      onSuccess();
    }
  });

  const handleExport = () => {
    if (!ids) return;
    exportPhich(ids);
  };

  const onChangeClassify = (e: any) => {
    setClassify(e?.target?.value);
    switch (e?.target?.value) {
      case 'TenSach':
        setImageUrl('MauPhich.png');
        break;
      case 'MonLoai':
        setImageUrl('MauPhich1.png');
        break;
      case 'TacGia':
        setImageUrl('MauPhich2.png');
        break;
    }
  };

  const onChangeDisplay = (e: any) => {
    setDisplay(e?.target?.value);
    switch (e?.target?.value) {
      case 'TenSach4':
        setImageUrl1('MauPhich3.png');
        break;
      case 'MonLoai4':
        setImageUrl1('MauPhich4.png');
        break;
    }
  };

  return (
    <Modal
      open={open}
      title={<h3 className='border-b-2 pb-2 font-bold uppercase text-primary-10'>Xuất phích sách</h3>}
      closable={false}
      footer={null}
    >
      <div className='flex flex-col'>
        <label className='mb-4 font-bold'>Đối với sách từ 1 - 3 tác giả:</label>
        <div className='flex items-center justify-between gap-1'>
          <Radio.Group
            className='form-group form-group-sm grid w-full grid-cols-12'
            onChange={onChangeClassify}
            value={classify}
          >
            <Radio value={'MonLoai'} className='text-inbox col-span-4 text-base'>
              Mã tác phẩm
            </Radio>
            <Radio value={'TenSach'} className='text-inbox col-span-4 text-base'>
              Mã môn loại
            </Radio>
            <Radio value={'TacGia'} className='text-inbox col-span-4 text-base'>
              Mã tác giả
            </Radio>
          </Radio.Group>
        </div>
      </div>
      <div className='flex flex-col'>
        <label className='mb-4 font-bold'>Đối với sách không có tác giả hoặc từ 4 tác giả trở lên:</label>
        <div className='flex items-center justify-between gap-1'>
          <Radio.Group
            className='form-group form-group-sm grid w-full grid-cols-12'
            onChange={onChangeDisplay}
            value={display}
          >
            <Radio value={'TenSach4'} className='text-inbox col-span-4 text-base'>
              Mã tác phẩm
            </Radio>
            <Radio value={'MonLoai4'} className='text-inbox col-span-8 text-base'>
              Mã môn loại
            </Radio>
          </Radio.Group>
        </div>
      </div>

      <div className='mt-5 flex grid-cols-12 flex-col'>
        <div className='mb-[15px]'>
          <label className='text-base font-bold'>Mẫu xuất phích sách</label>
        </div>

        <button className={classNames('m-auto mb-2 flex h-[200px] w-[320px] items-center gap-12 ')}>
          <img src={'/content/' + imageUrl} alt='Mẫu phích sách' />
        </button>
        <button className={classNames('m-auto flex h-[200px] w-[320px] items-center gap-12')}>
          <img src={'/content/' + imageUrl1} alt='Mẫu phích sách' />
        </button>
      </div>

      <div className='mt-3 flex items-center justify-end gap-2'>
        <Button variant='secondary' onClick={onHide} loading={isLoading} disabled={isLoading}>
          Quay về
        </Button>

        <Button onClick={handleExport} loading={isLoading} disabled={isLoading}>
          Xuất mã
        </Button>
      </div>
    </Modal>
  );
};

export default CreatePoster;
