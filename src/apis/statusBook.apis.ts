import { QueryConfig } from 'hooks/useQueryConfig';
import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/TrangThaiSach`;

type GetStatusBookData = {
  ListTrangThai: StatusBook[];
} & ResponseList;

const statusBooksApis = {
  getStatusBooks: (payload: QueryConfig) => {
    const { page, pageSize, TextForSearch } = payload;

    return httpThuVien.post<ResponseApi<GetStatusBookData>>(`${endPoint}/GetDataPagingBySearch`, {
      PageSize: pageSize,
      PageNumber: page,
      Keyword: TextForSearch
    });
  },

  getStatusBook: (id: string) => httpThuVien.get<ResponseApi<StatusBook>>(`${endPoint}/${id}`),
  deleteStatusBook: (id: string) => httpThuVien.delete<ResponseApi<{}>>(`${endPoint}/${id}`),
  createStatusBook: (payload: Omit<StatusBook, 'Id'>) =>
    httpThuVien.post<ResponseApi<{}>>(`${endPoint}/Create`, payload),
  updateStatusBook: (payload: StatusBook) => httpThuVien.put<ResponseApi<{}>>(`${endPoint}`, payload)
};

export default statusBooksApis;
