import { DownloadOutlined } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Table } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { statistic } from 'apis';
import { Button, Loading, Title } from 'components';
import { path } from 'constants/path';
import { uniqueId } from 'lodash';
import { useMemo } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
const RegisterationBookNumber = () => {
  const navigate = useNavigate();
  const { state } = useLocation();

  const Month = state ? state.Month : new Date().getMonth() + 1;

  const Year = state ? state.Year : new Date().getFullYear();
  const IdKhoSach = state ? state.IdKhoSach : '';

  const RangeYear = useMemo(() => {
    let result = [];
    for (
      var i = Math.floor((Year || new Date().getFullYear()) / 10) * 10 + 1;
      i <= Math.round((Year || new Date().getFullYear()) / 10) * 10 + 5;
      i++
    )
      result.push(i);
    return result;
  }, [Year]);

  const { data, isLoading } = useQuery({
    queryFn: () => {
      let param: { month?: string; year?: string; idKhoSach: string } = {
        month: Month + '',
        year: Year + '',
        idKhoSach: IdKhoSach
      };
      if (!Month) delete param.month;
      if (!Year) delete param.year;
      return statistic.PreviewSoDangKySGK_Kho(param);
    },
    queryKey: ['PreviewSoDangKySGK_Kho', Month, Year, IdKhoSach]
  });

  const { mutate: DownSoDangKyCaBietSGK_Kho, isLoading: loadingDownSoDangKyCaBietSGK_Kho } = useMutation({
    mutationFn: () => {
      let param: { month?: string; year?: string; IdKho: string } = {
        month: Month + '',
        year: Year + '',
        IdKho: IdKhoSach
      };
      if (!Month) delete param.month;
      if (!Year) delete param.year;
      return statistic.DownSoDangKyCaBietSGK_Kho(param);
    },
    onSuccess: (data) => {
      toast.success('Tải sổ tổng quát thành công');
      const blob = new Blob([data.data, { type: 'application/xls' }]);
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = 'SoDKTQ_Kho.xls';
      link.click();
    }
  });

  const dataRow = useMemo(() => {
    if (!data?.data.Item.ListBook) return [];
    return data?.data.Item.ListBook;
  }, [data?.data.Item]);
  const columns: ColumnsType<any> = [
    {
      key: 'NamHoc',
      dataIndex: 'NamHoc',
      title: 'Năm học',
      rowSpan: 2,
      children: [
        {
          rowSpan: 0,
          children: [
            {
              key: 'NgayVaoSo',
              dataIndex: 'NgayVaoSo',
              title: 'Ngày vào sổ'
            }
          ]
        }
      ]
    },
    {
      key: 'STT',
      dataIndex: 'STT',
      title: 'Sổ đăng kí(STT)'
    },
    {
      key: 'SoChungTu',
      dataIndex: 'SoChungTu',
      title: 'Số chứng từ'
    },
    {
      key: 'NamXuatBan',
      dataIndex: 'NamXuatBan',
      title: 'Năm xuất bản'
    },
    {
      key: 'TongSoBan',
      dataIndex: 'TongSoBan',
      title: 'Tổng số bản'
    },
    {
      key: 'DonGia',
      dataIndex: 'DonGia',
      title: 'Đơn giá'
    },
    {
      key: 'ThanhTien',
      dataIndex: 'ThanhTien',
      title: 'Thành tiền'
    },
    {
      key: 'KiemKe',
      dataIndex: 'KiemKe',
      title: 'Kiểm kê',
      children:
        RangeYear.map((element, index) => {
          return {
            key: element,
            dataIndex: element,
            title: element + ' - ' + (element + 1),
            children: [
              {
                key: 'KiemKeM' + element,
                dataIndex: 'KiemKeM' + element,
                title: 'Mất',
                render: (value: any, record: any) => record && record.KiemKe && record.KiemKe[index * 2]
              },
              {
                key: 'KiemKeC' + element,
                dataIndex: 'KiemKeC' + element,
                title: 'Còn',
                render: (value: any, record: any) => record && record.KiemKe && record.KiemKe[index * 2 + 1]
              }
            ]
          };
        }) || []
    },
    {
      key: 'GhiChu',
      dataIndex: 'GhiChu',
      title: 'Ghi chú'
    }
  ];
  return (
    <div className='py-5'>
      <Title title='SỔ ĐĂNG KÝ SÁCH GIÁO KHOA' />
      <div className='mt-3'>
        {dataRow.map((element: any) => {
          return (
            <div className='my-2.5'>
              {' '}
              <span className='ml-2 text-xl font-bold' style={{ lineHeight: '40px' }}>
                {'Tên sách : ' + element.TenSach}
              </span>
              <Table
                columns={columns}
                className='custom-table'
                bordered
                scroll={{ x: true }}
                locale={{ emptyText: 'Không có dữ liệu' }}
                dataSource={element.ListChiTiet}
                pagination={{
                  defaultPageSize: 10,
                  hideOnSinglePage: true,
                  showSizeChanger: false
                }}
                rowKey={() => uniqueId()}
              />
            </div>
          );
        })}
      </div>
      <div className='mt-3 flex items-center justify-end gap-2'>
        <Button
          variant='secondary'
          onClick={() => navigate(path.thongkexuatbaocao)}
          loading={loadingDownSoDangKyCaBietSGK_Kho}
        >
          Quay về
        </Button>

        <Button onClick={() => DownSoDangKyCaBietSGK_Kho()} loading={loadingDownSoDangKyCaBietSGK_Kho}>
          <DownloadOutlined />
          Tải xuống
        </Button>
      </div>
      <Loading open={isLoading}></Loading>
    </div>
  );
};

export default RegisterationBookNumber;
