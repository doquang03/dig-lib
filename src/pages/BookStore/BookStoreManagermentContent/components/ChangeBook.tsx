import { useMutation, useQuery } from '@tanstack/react-query';
import { Modal, ModalProps, Table, Tooltip } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { bookApis, bookStoreApis } from 'apis';
import classNames from 'classnames';
import { Button, Input, SizeChanger } from 'components';
import 'css/ChangeBook.css';
import { forEach } from 'lodash';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import { getSerialNumber } from 'utils/utils';

type Props = {
  listCaBiet: SachCaBiet[];
  idKhoSach: string;
  isLoadingSachCaBiet: boolean;
  onHide: VoidFunction;
  onSuccess: VoidFunction;
} & ModalProps;

type FormInput = {
  TextForSearch: string;
};

const ChangeBook = (props: Props) => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(30);

  const { listCaBiet, isLoadingSachCaBiet, open, onHide, onSuccess } = props;
  const IdProps = useMemo(() => {
    let Id: string | undefined = undefined;
    for (var param in listCaBiet) {
      const element = listCaBiet[param];
      if (Id === undefined) Id = element.IdSach;
      if (Id !== element.IdSach) return undefined;
    }
    return Id;
  }, [listCaBiet]);
  const [selectSach, setSelectSach] = useState<Book>();
  const [total, setTotal] = useState<number>(0);
  const [strSearch, setStrSearch] = useState('');

  const { register, handleSubmit, reset } = useForm<FormInput>({
    defaultValues: {
      TextForSearch: ''
    }
  });

  const { mutate: ChangeBook, isLoading } = useMutation({
    mutationFn: (selectSach: Book) =>
      bookStoreApis.ThayDoiDauSach(listCaBiet.map((item) => item.Id) as string[], selectSach.Id as string),
    onSuccess: (res, data) => {
      toast.success('Chuyển đầu sách thành công');
      onHide();
      listCaBiet.forEach((element) => {
        element.IdSach = data.Id;
        element.MaKiemSoat = data.MaKiemSoat;
        element.TenSach = data.TenSach;
      });
      reset();
      setSelectSach(undefined);
      onSuccess();
    }
  });

  const { data, isLoading: isLoadingBook } = useQuery({
    queryKey: ['listBook', page, pageSize, strSearch],
    queryFn: () => bookApis.GetListSachPure(page, pageSize, strSearch),
    enabled: !!open
  });

  const dataSource = useMemo(() => {
    setTotal(data?.data.Item.count || 0);
    if (IdProps !== undefined)
      data?.data.Item.ListSach.forEach((element) => {
        if (element.Id === IdProps) {
          setSelectSach(element);
          return;
        }
      });
    return data?.data.Item.ListSach;
  }, [IdProps, data]);

  const handleComplete = () => {
    if (selectSach === undefined) {
      toast.error('Chưa chọn sách');
      return;
    }
    ChangeBook(selectSach);
  };

  const onPaginate = useCallback((page: number, pageSize: number) => {
    setPage(page);
    setPageSize(pageSize);
  }, []);

  const pagination = useMemo(() => {
    return {
      current: (!!page && +page) || 1,
      pageSize: (!!pageSize && +pageSize) || 30,
      total: total,
      onChange: onPaginate,
      hideOnSinglePage: true,
      showSizeChanger: false,
      showQuickJumper: true,
      locale: { jump_to: '', page: '' }
    };
  }, [onPaginate, page, pageSize, total]);

  const columns: ColumnsType<Book> = useMemo(() => {
    return [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        className: 'min-content',
        render: (value, record, index) => {
          return page && pageSize && getSerialNumber(page, pageSize, index);
        }
      },
      {
        title: 'Mã kiểm soát',
        dataIndex: 'MaKiemSoat',
        key: 'MaKiemSoat'
      },
      {
        title: 'Tên sách',
        dataIndex: 'TenSach',
        key: 'TenSach',
        render: (value, { TenSach = '' }) =>
          TenSach?.length > 50 ? (
            <Tooltip placement='topLeft' title={TenSach} arrow={true}>
              <p className='text-center'>{TenSach.substring(0, 50).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{TenSach}</p>
          )
      },
      {
        title: '',
        dataIndex: 'actions',
        key: 'actions',
        render: (value, record) => {
          return selectSach?.Id !== record.Id ? (
            <Button
              variant={'default'}
              className='w-[100%] md:w-auto'
              style={{ display: 'inline-block', minWidth: '74px' }}
              key={'Chọn'}
              onClick={(e) => {
                setSelectSach(record);
              }}
            >
              Chọn
            </Button>
          ) : (
            <Button
              variant={'default'}
              className='gray-button w-[100%] md:w-auto'
              style={{ display: 'inline-block', minWidth: '74px' }}
              key={'Đang chọn'}
            >
              Đang chọn
            </Button>
          );
        }
      }
    ];
  }, [page, pageSize, selectSach]);

  let locale = {
    emptyText: 'Không tìm được kết quả phù hợp'
  };

  const onSubmit = handleSubmit((data) => {
    setPage(1);
    setStrSearch(data.TextForSearch);
  });

  return (
    <Modal
      open={open}
      title={<h3 className='border-b-2 pb-2 font-bold uppercase text-primary-10'>Chuyển sang sách khác</h3>}
      closable={false}
      footer={null}
      width={1080}
    >
      <form className='mb-4 flex flex-col' onSubmit={onSubmit}>
        <div className='my-5 flex flex-col flex-wrap gap-2 bg-secondary-10 py-3 px-3.5 md:flex-row'>
          <Input
            placeholder='Nhập tên sách, mã kiểm soát'
            containerClassName='grow'
            name='TextForSearch'
            register={register}
          />
          <Button
            variant='secondary'
            type='button'
            className='font-semibold'
            onClick={() => {
              reset();
              setStrSearch('');
            }}
          >
            Làm mới
          </Button>

          <Button variant='default' type='submit' className='font-semibold'>
            Tìm kiếm
          </Button>
        </div>
        <Table
          loading={isLoadingBook}
          columns={columns}
          dataSource={dataSource || []}
          pagination={pagination}
          scroll={{ y: 375 }}
          rowKey={(record) => record.Id || ''}
          className='custom-table'
          locale={locale}
          bordered
        />
        <div
          className={classNames('relative', {
            // @ts-ignore
            'mt-[64px]': (page && pageSize) >= total
          })}
        >
          <div className='absolute bottom-1'>
            <SizeChanger
              visible={!!total && Number.isInteger(page && +pageSize)}
              value={pageSize.toString()}
              currentPage={page.toString()}
              total={total?.toString() || ''}
              onChange={(pageSize) => {
                setPage(1);
                setPageSize(+pageSize);
              }}
            />
          </div>
        </div>
      </form>

      <div className='mt-3 flex items-center justify-end gap-2'>
        <Button type='button' variant='secondary' onClick={onHide} loading={isLoading} disabled={isLoading}>
          Quay về
        </Button>

        <Button loading={isLoading} disabled={isLoading || isLoadingSachCaBiet} onClick={handleComplete}>
          Cập nhật
        </Button>
      </div>
    </Modal>
  );
};

export default ChangeBook;
