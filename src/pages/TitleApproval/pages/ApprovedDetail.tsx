import { useQuery } from '@tanstack/react-query';
import { approvalApis } from 'apis';
import { Button, Title } from 'components';
import { useLocation, useNavigate, useParams } from 'react-router-dom';

const labels = [
  '020 # # a ISBN',
  '020 # # c Giá bìa',
  '041 0 # a Tên ngôn ngữ',
  '044 # # a Xuất xứ',
  '082 0 4 a Mã DDC',
  '084 0 4 a Mã Dãy 19',
  '100 1 # a Tác giả',
  '245 1 0 a Tên sách',
  '250 # # a Tái bản',
  '260 # # b Nhà xuất bản',
  '260 # # c Năm xuất bản',
  '300 # # a Số trang',
  '490 0 # a Tùng thư',
  '500 # # a Phụ chú',
  '520 # # a Tóm tắt',
  '740 0 # a Tiêu đề bổ sung',
  '856 # # u Liên kết (url)'
];

const ApprovedDetail = () => {
  const { id } = useParams();
  const { schoolName } = useLocation().state || '';

  const { data } = useQuery({
    queryKey: ['getApprovalDetails', id],
    queryFn: () => approvalApis.getApprovalDetails(id + ''),
    enabled: !!id
  });

  const navigate = useNavigate();

  const { ListMedia, ListParam } = data?.data.Item || { ListMedia: [], ListParam: [] };

  return (
    <div>
      <Title title='CHI TIẾT BIỂU GHI BIÊN MỤC' />

      <div className='mt-3 grid grid-cols-12'>
        <h3 className='col-start-3 whitespace-nowrap font-bold text-primary-10'>{schoolName}</h3>
      </div>

      {labels.map((label, index) => (
        <div className='mt-3 grid grid-cols-12 items-center gap-y-4' key={label}>
          <p className='col-span-2 whitespace-nowrap'>{label}:</p>

          <div className='col-span-10 flex h-[2rem] items-center rounded-md border-[1px] border-[#A7A7A7] bg-[#E7E6E6] px-1'>
            {ListParam[index]}
          </div>
        </div>
      ))}

      <div className='my-3 grid grid-cols-12'>
        <p className='col-span-2 whitespace-nowrap'>xxx ; Nội dung file:</p>

        <div className='col-span-10 flex h-[2rem] items-center rounded-md border-[1px] border-[#A7A7A7] bg-[#E7E6E6] px-1'>
          {ListMedia.map((file, index) => (
            <a key={index} href={file.LinkFile} target='_blank' rel='noreferrer'>
              {file.FileName}
              {ListMedia.length - 1 === index ? '.' : ', '}
            </a>
          ))}
        </div>
      </div>

      <div className='my-3 flex justify-end'>
        <Button variant='secondary' onClick={() => navigate(-1)}>
          Quay về
        </Button>
      </div>
    </div>
  );
};

export default ApprovedDetail;
