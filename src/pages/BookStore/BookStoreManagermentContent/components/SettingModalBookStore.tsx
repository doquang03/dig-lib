import { Checkbox, Modal, ModalProps } from 'antd';
import { Button, Title } from 'components';
import { useState } from 'react';

type Props = {
  arrAvaliable: Array<boolean>;
  setVisibleModal: Function;
  setArrAvaliable: Function;
  setCookie: Function;
} & ModalProps;

const SettingModalBookStore = (props: Props) => {
  const { arrAvaliable, setArrAvaliable, setVisibleModal, setCookie, open } = props;

  const [clone, setClone] = useState({ ...arrAvaliable });

  return (
    <Modal
      className='model-choses-library-monitor'
      open={open}
      closable={false}
      footer={[
        <Button
          key={'back'}
          className='gray-button'
          onClick={() => {
            setVisibleModal(false);
          }}
        >
          Quay về
        </Button>,
        <Button
          key={'submit'}
          className='btn btn-primary btn-sm blue-button'
          style={{ marginLeft: '10px' }}
          onClick={() => {
            setCookie('settings_bookstores', clone, {
              path: '/'
            });
            setArrAvaliable(clone);
            setVisibleModal(false);
          }}
        >
          Cập nhật
        </Button>
      ]}
    >
      <Title title={'CẤU HÌNH HIỂN THỊ'} />
      <div className='mt-8 grid grid-cols-1 md:grid-cols-12'>
        <div className='custom-row col-span-6 flex flex-col px-2'>
          <Checkbox
            className='checkbox-modal'
            checked={clone[0]}
            onChange={() => {
              let temp = { ...clone };
              temp[0] = !temp[0];
              setClone(temp);
            }}
          >
            STT
          </Checkbox>
          <Checkbox
            className='checkbox-modal'
            checked={clone[1]}
            onChange={() => {
              let temp = { ...clone };
              temp[1] = !temp[1];
              setClone(temp);
            }}
          >
            Mã kiểm soát
          </Checkbox>
          <Checkbox
            className='checkbox-modal'
            checked={clone[2]}
            onChange={() => {
              let temp = { ...clone };
              temp[2] = !temp[2];
              setClone(temp);
            }}
          >
            Số ĐKCB
          </Checkbox>
          <Checkbox
            className='checkbox-modal'
            checked={clone[3]}
            onChange={() => {
              let temp = { ...clone };
              temp[3] = !temp[3];
              setClone(temp);
            }}
          >
            Tên sách
          </Checkbox>
          <Checkbox
            className='checkbox-modal'
            checked={clone[4]}
            onChange={() => {
              let temp = { ...clone };
              temp[4] = !temp[4];
              setClone(temp);
            }}
          >
            Tình trạng
          </Checkbox>
        </div>
        <div className='custom-row col-span-6 flex flex-col px-2'>
          <Checkbox
            className='checkbox-modal'
            checked={clone[5]}
            onChange={() => {
              let temp = { ...clone };
              temp[5] = !temp[5];
              setClone(temp);
            }}
          >
            Số chứng từ
          </Checkbox>
          <Checkbox
            className='checkbox-modal'
            checked={clone[6]}
            onChange={() => {
              let temp = { ...clone };
              temp[6] = !temp[6];
              setClone(temp);
            }}
          >
            Số vào sổ tổng quát
          </Checkbox>
          <Checkbox
            className='checkbox-modal'
            checked={clone[7]}
            onChange={() => {
              let temp = { ...clone };
              temp[7] = !temp[7];
              setClone(temp);
            }}
          >
            Ngày vào sổ
          </Checkbox>
          <Checkbox
            className='checkbox-modal'
            checked={clone[8]}
            onChange={() => {
              let temp = { ...clone };
              temp[8] = !temp[8];
              setClone(temp);
            }}
          >
            Nguồn cung cấp
          </Checkbox>
        </div>
      </div>
    </Modal>
  );
};

export default SettingModalBookStore;
