import classnames from 'classnames';
import { SelectHTMLAttributes } from 'react';
import type { FieldPath, FieldValues } from 'react-hook-form';
import type { UseFormRegister } from 'react-hook-form/dist/types/form';

type Props<TFieldValues extends FieldValues> = {
  items: Selections;
  register?: UseFormRegister<TFieldValues>;
  name?: FieldPath<TFieldValues>;
  classNameError?: string;
  errorMessage?: string;
} & SelectHTMLAttributes<HTMLSelectElement>;

function Select<TFieldValues extends FieldValues>(props: Props<TFieldValues>) {
  const {
    className,
    register,
    name,
    onChange,
    errorMessage,
    classNameError = 'text-[14px] text-red-500 mt-1',
    items,
    ...rest
  } = props;

  const propsOnChange = onChange ? { onChange: onChange } : {};

  const registerResult = register && name ? register(name, propsOnChange) : null;

  return (
    <>
      <select
        name={name}
        className={classnames(
          `rounded-md border border-gray-300 py-[0.85rem] px-2 text-black outline-none focus:border-primary-30 focus:shadow-sm ${className}`,
          {
            'border border-red-500': !!errorMessage,
            'cursor-default bg-[#e3ebf4]': props?.disabled
          }
        )}
        {...rest}
        {...registerResult}
        {...propsOnChange}
        defaultValue={props.defaultValue}
      >
        {!!items?.length &&
          items?.map(({ label = '', value }) => (
            <option value={value} key={value} className='text-black'>
              {label ? (label.length > 60 ? label.substring(0, 60).concat('...') : label) : ''} {}
            </option>
          ))}
      </select>

      {!!errorMessage && <p className={classNameError}>{errorMessage}</p>}
    </>
  );
}

export default Select;
