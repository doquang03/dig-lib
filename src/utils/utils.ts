import axios, { AxiosError, HttpStatusCode } from 'axios';
import { Mixstart, VietnameseAuthorCodes, VietnameseAuthorSigns, VietnameseAuthorStrings } from 'constants/author';

export const isAxiosError = <T>(error: unknown): error is AxiosError<T> => {
  // eslint-disable-next-line import/no-named-as-default-member
  return axios.isAxiosError(error);
};

export const isAxiosUnprocessableEntityError = <FormError>(error: unknown): error is AxiosError<FormError> => {
  return isAxiosError(error) && error?.response?.status === HttpStatusCode.UnprocessableEntity;
};

export const isAxiosUnauthorizedError = <Unauthorized>(error: unknown): error is AxiosError<Unauthorized> => {
  return isAxiosError(error) && error?.response?.status === HttpStatusCode.Unauthorized;
};

/**
 * Return to serial numbers based on index
 * @param {Number} page
 * @param {Number} limit
 * @param {Number} index
 * @returns serial numbers
 */
export const getSerialNumber = (page: number, limit: number, index: number) => {
  return +page === 1 ? index + 1 : (+page - 1) * limit + (index + 1);
};

/**
 * get unit info from session storage
 * @return Unit | null
 */
export const getUnitFromSS = (key: number): (Unit & { isAllowedAdjustment: boolean }) | null => {
  if (!key) return null;

  const result = sessionStorage.getItem(`${key}`) || '';

  return result ? JSON.parse(result) : null;
};

/**
 * save info unit to session storage
 * only education department excute
 * remove item when sign out application
 * @param {Number} key
 * @param {Unit} unit
 */
export const saveUnitToSS = (key: number, unit: Unit & { isAllowedAdjustment: boolean }) => {
  sessionStorage.setItem(key + '', JSON.stringify(unit));
};

/**
 * Return to serial numbers based on index
 * @param {Number} avatarLink
 * @returns avartar link
 */
export const getAvatarUrl = (avatarLink?: string) =>
  // Mock link for testing
  // TODO: use right env
  Boolean(avatarLink) ? `` : '/content/default-avatar.jpeg';

export const getBookImageUrl = (avatarLink?: string) =>
  // Mock link for testing
  // TODO: use right env
  Boolean(avatarLink) ? `` : '/content/Book.png';

export const convertDate = (date: any) => {
  let _date = new Date(date),
    mnth = ('0' + (_date.getMonth() + 1)).slice(-2),
    day = ('0' + _date.getDate()).slice(-2);

  if (isNaN(_date.getDate())) {
    return date;
  }

  return [day, mnth, _date.getFullYear()].join('/');
};

export const saveByteArray = (arrayBuffer: any, fileName: string, fileType: string) => {
  let byteArray = new Uint8Array(arrayBuffer);
  let blob = new Blob([byteArray], { type: 'application/xls' });
  let link = document.createElement('a');
  link.href = window.URL.createObjectURL(blob);
  link.download = fileName;
  link.click();
};

export const download = (filename: string, text: any) => {
  let blob = new Blob([text], { type: 'application/xls' });
  let link = document.createElement('a');
  link.href = window.URL.createObjectURL(blob);
  link.download = filename;
  link.click();
};

export const removeVietnameseTones = (str: string) => {
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
  str = str.replace(/đ/g, 'd');
  str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, 'A');
  str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, 'E');
  str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, 'I');
  str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, 'O');
  str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, 'U');
  str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, 'Y');
  str = str.replace(/Đ/g, 'D');
  // Some system encode vietnamese combining accent as individual utf-8 characters
  // Một vài bộ encode coi các dấu mũ, dấu chữ như một kí tự riêng biệt nên thêm hai dòng này
  str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ''); // ̀ ́ ̃ ̉ ̣  huyền, sắc, ngã, hỏi, nặng
  str = str.replace(/\u02C6|\u0306|\u031B/g, ''); // ˆ ̆ ̛  Â, Ê, Ă, Ơ, Ư
  // Remove extra spaces
  // Bỏ các khoảng trắng liền nhau
  str = str.replace(/ + /g, ' ');
  str = str.trim();
  // Remove punctuations
  // Bỏ dấu câu, kí tự đặc biệt
  return str;
};

export const jsUcfirst = (string: string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

export const GenerateVietnameseAuthorCode = (AuthorNames: string) => {
  let specialCode = ConvertSpecialAuthorCode(AuthorNames);
  if (specialCode.length > 0) return specialCode;
  else return ConvertVietnameseAuthorCode(ConvertVietnameseAuthorString(AuthorNames));
};

const ConvertVietnameseAuthorString = (str: string) => {
  //Tiến hành thay thế , lọc bỏ dấu cho chuỗi
  for (let i = 1; i < VietnameseAuthorSigns.length; i++) {
    for (let j = 0; j < VietnameseAuthorSigns[i].length; j++)
      str = str.replace(VietnameseAuthorSigns[i][j], VietnameseAuthorSigns[0][i - 1]);
  }
  return str;
};

const ConvertSpecialAuthorCode = (str: string) => {
  switch (str.toLocaleLowerCase()) {
    case 'mác':
    case 'ăngghen':
    case 'mác và ăngghen':
    case 'karl marx':
    case 'friedrich engels':
    case 'karl marx & friedrich engels':
    case 'karl marx - friedrich engels':
    case 'karl marx and friedrich engels':
      return '3K1';
    case 'lênin':
    case 'vladimir ilyich lenin':
      return '3K2';
    case 'hồ chí minh':
      return '3K5H';
    default:
      return '';
  }
};

const ConvertVietnameseAuthorCode = (str: string) => {
  let containIndex = new Array<number>();
  let goodIndex = -1;

  let firstName = '';
  let surName = '';
  let temp = '';

  let strcomma = [];
  // restart:
  do {
    // Nếu nhiều tác giả
    if (str.includes(';')) {
      // ít hơn 4 tác giả thì lấy người đầu tiên
      if (str.split(';').length <= 4) str = str.split(';')[0];
      else return '';
    }

    // loại bỏ dấu phẩy cuối nếu có
    if (str.endsWith(',')) str = str.substring(0, str.length - 1);

    // Nếu tên có chứa dấu phẩy
    if (str.includes(',')) {
      // Nếu tên có chứa 1 dấu phẩy
      // Nguyễn, Văn A
      // Coulson, Phillip
      strcomma = str.split(',');
      if (strcomma.length === 2) {
        // Có phải tên phiên âm không
        // Câu-sân, phi-líp
        if (strcomma[0].includes('-')) {
          let strs2 = strcomma[0].split('-');
          surName = strs2[0];
          firstName = strs2[1];
        } else {
          surName = strcomma[0];
          firstName = strcomma[1];
        }
      } else {
        // hơn 2 dấu phẩy hoặc -> coi như nhiều tác giả
        str = str.replace(',', ';');
        // goto restart;
      }
    } else {
      let strs = str.split(' ');
      let len = strs.length;

      if (str.includes('.')) {
        // tên có dấu . tiếng anh viết tắc
        if (len > 1) {
          if (!strs[0].includes('.') && !strs[len - 1].includes('.')) {
            firstName = strs[0];
            surName = strs[len - 1];
          } else return '';
        } else surName = strs[0];
      } else if (str.includes('-')) {
        // Có phải tên phiên âm không
        // Câu-sân, phi-líp
        if (strs[len - 1].includes('-')) {
          let strs2 = strs[len - 1].split('-');
          surName = strs2[0];
          firstName = strs2[1];
        } else {
          firstName = strs[0];
          surName = strs[len - 1];
        }
      } else {
        // tiếng bình thường mặc định lấy họ là từ đầu tiên
        surName = strs[0];

        if (len > 1) firstName = strs[1];
        else surName = strs[0];
      }
    }
  } while (strcomma.length > 2);
  let startCode = '';
  let endCode = '';

  firstName = firstName.trim();

  if (firstName.length > 0) {
    let ff = firstName.toLocaleUpperCase();
    let zCode = '';

    for (let i = 0; i < Mixstart.length; i++) {
      if (ff.startsWith(Mixstart[i])) {
        zCode = Mixstart[i];
        break;
      }
    }

    endCode = zCode.length > 0 ? zCode : firstName.toLocaleUpperCase()[0].toString();
  }

  surName = surName.toLocaleLowerCase();
  if (surName.length > 2 && (surName.startsWith('gi') || surName.startsWith('qu'))) {
    temp = surName.substring(0, 2);
    surName = 'x' + surName.substring(2, surName.length - 2);
  }

  for (let i = 0; i < VietnameseAuthorStrings.length; i++) {
    if (surName.includes(VietnameseAuthorStrings[i])) {
      containIndex.push(i);
    }
  }
  if (containIndex.length > 0) {
    goodIndex = containIndex[0];
    containIndex.forEach((i) => {
      if (VietnameseAuthorStrings[i].length > VietnameseAuthorStrings[goodIndex].length) {
        goodIndex = i;
      }
    });
    startCode = surName.replace(VietnameseAuthorStrings[goodIndex], VietnameseAuthorCodes[goodIndex]);

    if (!Number.isInteger(Number(startCode[startCode.length - 1]))) return '';

    if (temp.length > 0) {
      startCode = temp + startCode.substring(1, startCode.length - 1);
    }

    if (Number.isInteger(Number(startCode[0]))) {
      startCode = surName[0] + startCode;
    }

    return startCode.toLocaleUpperCase() + endCode;
  }

  return '';
};

const editDistance = (s1: string, s2: string) => {
  s1 = s1.toLowerCase();
  s2 = s2.toLowerCase();

  let costs = [];
  for (let i = 0; i <= s1.length; i++) {
    let lastValue = i;
    for (let j = 0; j <= s2.length; j++) {
      if (i === 0) costs[j] = j;
      else {
        if (j > 0) {
          let newValue = costs[j - 1];
          if (s1.charAt(i - 1) !== s2.charAt(j - 1)) newValue = Math.min(Math.min(newValue, lastValue), costs[j]) + 1;
          costs[j - 1] = lastValue;
          lastValue = newValue;
        }
      }
    }
    if (i > 0) costs[s2.length] = lastValue;
  }
  return costs[s2.length];
};

export const similarity = (s1: string, s2: string) => {
  let longer = s1;
  let shorter = s2;
  if (s1.length < s2.length) {
    longer = s2;
    shorter = s1;
  }
  let longerLength = longer.length;
  if (longerLength === 0) {
    return 1.0;
  }
  return (longerLength - editDistance(longer, shorter)) / longerLength;
};

export const serialize = (obj: any) => {
  var str = [];
  for (var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
    }
  return str.join('&');
};

export const analysisColor = (
  obj: {
    Name: string;
  }[]
) => {
  if (obj.length === 0) return ['#0000', '#0000', '#0000', '#0000'];
  if (obj.length === 1) return [obj[0].Name, obj[0].Name, obj[0].Name, obj[0].Name];
  if (obj.length === 2) return [obj[0].Name, obj[0].Name, obj[1].Name, obj[1].Name];
  if (obj.length === 3) return [obj[0].Name, obj[0].Name, obj[1].Name, obj[2].Name];
  return [obj[0].Name, obj[1].Name, obj[2].Name, obj[3].Name];
};

export const generateColor = () => {
  return '#' + Math.random().toString(16).substr(-6);
};

export const OverallReview = (initialData: number[], lengthOfDataChunks: number, style: string) => {
  let unit = '';
  let sumUnit = '';
  switch (style) {
    case 'month':
      unit = 'ngày';
      sumUnit = 'tháng';
      break;
    case 'year':
      unit = 'tháng';
      sumUnit = 'năm';
      break;
    case 'week':
      unit = 'ngày';
      sumUnit = 'tuần';
      break;
  }
  const dataMedianLine = MedianLine(initialData, lengthOfDataChunks);

  let sumOrigin = 0;
  for (var i = 0; i < initialData.length; i++) {
    sumOrigin += initialData[i];
  }
  if (sumOrigin === 0) return '';
  let pointMarker = -1;
  let count = 0;
  let final: Array<{ trend: string; count: number }> = [];

  for (let i = 0; i < dataMedianLine.length; i++) {
    const point = dataMedianLine[i];
    if (pointMarker === -1) {
      pointMarker = point;
      continue;
    }

    if (point === pointMarker || (pointMarker !== 0 && point / pointMarker > 0.9 && point / pointMarker < 1.1)) {
      count++;
    } else {
      if (count > 0) {
        final.push({ trend: 'stable', count: count });
        pointMarker = dataMedianLine[--i];
        count = 0;
      } else {
        if (point > pointMarker) {
          final.push({ trend: 'ascending', count: 1 });
          pointMarker = point;
          count = 0;
        } else {
          final.push({ trend: 'decrease', count: 1 });
          pointMarker = point;
          count = 0;
        }
      }
    }
  }

  if (count > 0) {
    final.push({ trend: 'stable', count: count });
  }
  final = CollapseOverall(final);
  let sum = 0;
  for (let i = 0; i < final.length; i++) {
    sum += final[i].count;
  }
  let main = [];
  if (final.length <= 2) main = final;
  else
    for (let i = 0; i < final.length; i++) {
      if (final[i].count / sum > 0.3) main.push(final[i]);
    }
  if (main.length === 1) {
    if (main[0].trend === 'ascending') return ' có xu hướng tăng dần qua các ' + unit + ' trong ' + sumUnit;
    if (main[0].trend === 'decrease') return ' có xu hướng giảm dần qua các ' + unit + ' trong ' + sumUnit;
    if (main[0].trend === 'stable') return ' khá ổn định trong ' + sumUnit;
  } else if (main.length === 2) {
    let finalString = '';
    if (main[0].trend === 'ascending') finalString = ' có xu hướng tăng từ đầu ' + sumUnit + ' đến giữa ' + sumUnit;
    if (main[0].trend === 'decrease') finalString = ' có xu hướng giảm từ đầu ' + sumUnit + ' đến giữa ' + sumUnit;
    if (main[0].trend === 'stable') finalString = ' có xu hướng ổn định từ đầu ' + sumUnit + ' đến giữa ' + sumUnit;
    if (main[1].trend === 'ascending') finalString += ' và tăng dần vào cuối ' + sumUnit;
    if (main[1].trend === 'decrease') finalString += ' và giảm dần vào cuối ' + sumUnit;
    if (main[1].trend === 'stable') finalString += ' và ổn định vào cuối ' + sumUnit;
    return finalString;
  } else {
    let max = -1;
    let trend;
    for (let i = 0; i < final.length; i++) {
      if (max < final[i].count) {
        max = final[i].count;
        trend = final[i];
      }
      if (trend?.trend === 'ascending') return ' có xu hướng tăng dần qua các ' + unit + ' trong ' + sumUnit;
      if (trend?.trend === 'decrease') return ' có xu hướng giảm dần qua các ' + unit + ' trong ' + sumUnit;
      if (trend?.trend === 'stable') return ' khá ổn định trong ' + sumUnit;
    }
  }

  return '';
};

export const CollapseOverall = (final: Array<{ trend: string; count: number }>) => {
  for (let i = 0; i < final.length; i++) {
    if (final[i - 1] && final[i - 1].trend === 'stable' && final[i - 1].count === 1) {
      final[i].count++;
      final.splice(i - 1, 1);
      i--;
    }
  }
  for (let i = 0; i < final.length; i++) {
    if (final[i + 1] && final[i].trend === final[i + 1].trend) {
      final[i].count += final[i + 1].count;
      final.splice(i + 1, 1);
      i--;
    }
  }

  return final;
};

export const MedianLine = (initialData: number[], lengthOfDataChunks: number) => {
  const sumArray = (accumulator: number, currentValue: number) => accumulator + currentValue;
  const numOfChunks = Math.ceil(initialData.length / lengthOfDataChunks);
  const dataChunks: { [key: number]: number[] } = [];

  for (var i = 0; i < numOfChunks; i++) dataChunks[i] = [];

  initialData.forEach((entry, index) => {
    const chunkNumber = Math.floor(index / lengthOfDataChunks);
    if (!dataChunks[chunkNumber]) dataChunks[chunkNumber] = [];
    dataChunks[chunkNumber].push(entry);
  });
  const averagedChunks = [];
  for (const [key, chunkEntry] of Object.entries(dataChunks)) {
    const chunkAverage = chunkEntry.reduce(sumArray) / lengthOfDataChunks;
    averagedChunks.push(chunkEntry.map(() => chunkAverage));
  }

  return averagedChunks.flat();
};

export const getEmbed = (url: string) => {
  const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
  const match = url.match(regExp);

  return match && match[2].length === 11 ? match[2] : null;
};
