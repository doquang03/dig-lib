import { EditOutlined } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { DatePicker, DatePickerProps, Tooltip } from 'antd';
import { newspaperMagazineApis } from 'apis';
import { Button, Loading } from 'components';
import { FORMAT_DATE_PICKER } from 'constants/config';
import dayjs, { Dayjs } from 'dayjs';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import PrintNoteBook from './PrintNoteBook';
import RegisterModal from './RegisterModal';

const valueStartOfWeek = (value: Dayjs) => dayjs(value).startOf('week').format(FORMAT_DATE_PICKER[5]);
const valueEndOfWeek = (value: Dayjs) => dayjs(value).endOf('week').format(FORMAT_DATE_PICKER[0]);

const customWeekStartEndFormat: DatePickerProps['format'] = (value) =>
  `Tuần từ ${valueStartOfWeek(value)} ~ ${valueEndOfWeek(value)}`;

const StatisticByWeek = () => {
  const [isPrint, setIsPrint] = useState<boolean>(false);
  const [dateValue, setDateValue] = useState<string[]>([]);
  const [rangeDate, setRangeDate] = useState<{ Min: string; Max: string }>({
    Min: dayjs().startOf('week').toISOString(),
    Max: dayjs().endOf('week').toISOString()
  });
  const [isRegisterModalVisible, setIsRegisterModalVisible] = useState<boolean>(false);
  const [idNewspaperMagazine, setIdNewspaperMagazine] = useState<string>('');
  const [row, setRow] = useState<{
    [key: string]: { [key: string]: { Id: string; IdBaoTapChi: string; Ngay: string; SoLuong: number } };
  }>({});

  const { data: newspaperMagazineData } = useQuery({
    queryKey: ['getNewspaperMagazine'],
    queryFn: () => {
      return newspaperMagazineApis.getData({
        page: 1,
        pageSize: 0
      });
    }
  });

  const { data: newspaperMagazineRegisterationsData, refetch: refetchNewspaperMagazineRegisterationsData } = useQuery({
    queryKey: ['getNewspaperMagazineRegisterationsData', rangeDate],
    queryFn: () => {
      return newspaperMagazineApis.getNewspaperMagazineRegisteration({
        Min: rangeDate.Min,
        Max: rangeDate.Max
      });
    }
  });

  const { mutate: editCell, isLoading: loadingEditCell } = useMutation({
    mutationFn: newspaperMagazineApis.modifyCell,
    onSuccess: () => {
      refetchNewspaperMagazineRegisterationsData();
    }
  });

  useEffect(() => {
    if (
      !newspaperMagazineData?.data.Item.ListBaoTapChi.length &&
      !newspaperMagazineRegisterationsData?.data?.Item?.ListDangKyBaoTapChi.length
    ) {
      setRow({});

      return;
    }

    let cell: {
      [key: string]: { [key: string]: { Id: string; IdBaoTapChi: string; Ngay: string; SoLuong: number } };
    } = {};

    const newspaperMagazineRegiseration = newspaperMagazineRegisterationsData?.data.Item.ListDangKyBaoTapChi || [];

    // cột trước dòng sau
    newspaperMagazineRegiseration.forEach((item) => {
      // init dòng
      if (!cell[new Date(item.Ngay).toISOString()]) {
        cell[new Date(item.Ngay).toISOString()] = {};
      }

      // khởi tạo giá trị ô
      cell[new Date(item.Ngay).toISOString()][item?.IdBaoTapChi] = item;
    });

    setRow(cell);
  }, [newspaperMagazineData, newspaperMagazineRegisterationsData]);

  useEffect(() => {
    let array = [];
    for (let index = 0; index < 7; index++) {
      const elm = dayjs().startOf('week').add(index, 'day').format(FORMAT_DATE_PICKER[0]);
      array.push(elm);
    }

    setDateValue(array);
  }, []);

  const getCell = (idNewspaperMagazine: string, date: string) => {
    let cellValue = {
      Id: '',
      IdBaoTapChi: idNewspaperMagazine,
      Ngay: date,
      SoLuong: 0
    };

    // tìm kiếm trong dữ liệu
    if (!row?.[date] || !row[date][idNewspaperMagazine]) return cellValue;

    return row[date][idNewspaperMagazine];
  };

  const navigate = useNavigate();

  const handleBack = () => navigate(-1);

  const tooglePrintModal = () => setIsPrint((prevState) => !prevState);

  const toogleRegisterModal = (id: string) => () => {
    setIsRegisterModalVisible((prevState) => !prevState);
    setIdNewspaperMagazine(id);
  };

  const handleInvisibleRegisterModal = () => setIsRegisterModalVisible((prevState) => !prevState);

  const handleResiterSuccess = () => {
    refetchNewspaperMagazineRegisterationsData();
  };

  return (
    <div>
      <div className='flex items-center justify-between'>
        <div className='mt-6 flex items-center gap-2'>
          <Button className='hidden'>Thêm từ file Excel</Button>

          <Button onClick={tooglePrintModal}>Tải sổ đăng ký báo</Button>
        </div>

        <DatePicker
          picker={'week'}
          placeholder='Chọn tuần'
          format={customWeekStartEndFormat}
          defaultValue={dayjs().startOf('week')}
          onChange={(value) => {
            if (value) {
              let array = [];
              for (let index = 0; index < 7; index++) {
                const elm = dayjs(value).startOf('week').add(index, 'day').format(FORMAT_DATE_PICKER[0]);
                array.push(elm);
              }

              setDateValue(array);

              setRangeDate({
                Min: dayjs(value).startOf('week').toISOString(),
                Max: dayjs(value).endOf('week').toISOString()
              });
            }
          }}
        />
      </div>

      <div className='mt-3 max-h-[40rem] overflow-y-auto'>
        <table className='table-striped table-bordered table whitespace-normal' cellPadding='1' width={'100%'}>
          <thead className='sticky top-0 bg-[rgb(235,244,250)] text-center font-semibold text-primary-10'>
            <tr>
              <td className='text-left'>Tên báo, tạp chí</td>

              <td>
                <p>Thứ 2</p>
                <p>{dateValue[0]}</p>
              </td>
              <td>
                <p>Thứ 3</p>
                <p>{dateValue[1]}</p>
              </td>
              <td>
                <p>Thứ 4</p>
                <p>{dateValue[2]}</p>
              </td>
              <td>
                <p>Thứ 5</p>
                <p>{dateValue[3]}</p>
              </td>
              <td>
                <p>Thứ 6</p>
                <p>{dateValue[4]}</p>
              </td>
              <td>
                <p>Thứ 7</p>
                <p>{dateValue[5]}</p>
              </td>
              <td>
                <p>Chủ Nhật</p>
                <p>{dateValue[6]}</p>
              </td>
            </tr>
          </thead>

          {Boolean(newspaperMagazineData?.data.Item.TotalCount) && (
            <>
              <tbody className='text-center font-semibold text-primary-10'>
                {newspaperMagazineData?.data.Item.ListBaoTapChi.map(({ Id, Ten }) => {
                  return (
                    <tr key={Id}>
                      <td>
                        <button
                          onClick={toogleRegisterModal(Id)}
                          className='flex w-full items-center justify-between gap-1'
                        >
                          {Ten.length > 25 ? (
                            <Tooltip
                              placement='topLeft'
                              title={Ten}
                              arrow={true}
                              className='flex w-full items-center justify-between gap-1'
                            >
                              <p className='text-center'>{Ten.substring(0, 25).concat('...')}</p>
                              <EditOutlined />
                            </Tooltip>
                          ) : (
                            <>
                              <p className='text-center'>{Ten}</p>
                              <EditOutlined />
                            </>
                          )}
                        </button>
                      </td>

                      {dateValue.map((itemDate) => {
                        const resultDates = itemDate.split('/');

                        const cell = getCell(
                          Id,
                          new Date(resultDates[1] + '/' + resultDates[0] + '/' + resultDates[2]).toISOString()
                        );

                        return (
                          <td key={itemDate}>
                            <input
                              type='text'
                              placeholder='--'
                              value={cell?.SoLuong || ''}
                              className='bg-transparent text-center focus:outline-primary-10'
                              onChange={(e) => {
                                let clone = { ...cell };

                                clone.SoLuong = +e.target.value;
                                if (
                                  !row[
                                    new Date(resultDates[1] + '/' + resultDates[0] + '/' + resultDates[2]).toISOString()
                                  ]
                                ) {
                                  row[
                                    new Date(resultDates[1] + '/' + resultDates[0] + '/' + resultDates[2]).toISOString()
                                  ] = {};
                                }

                                row[
                                  new Date(resultDates[1] + '/' + resultDates[0] + '/' + resultDates[2]).toISOString()
                                ][Id] = clone;

                                setRow({ ...row });
                              }}
                              onBlur={(e) => {
                                let clone = { ...cell };

                                clone.SoLuong = +e.target.value;

                                row[
                                  new Date(resultDates[1] + '/' + resultDates[0] + '/' + resultDates[2]).toISOString()
                                ][Id] = clone;
                                if (!Number.isNaN(cell.SoLuong))
                                  editCell({ Id: cell.Id, IdBaoTapChi: Id, Ngay: cell.Ngay, SoLuong: cell.SoLuong });
                              }}
                              onKeyDown={(e) => {
                                if (e.key === 'Enter') {
                                  // @ts-ignore

                                  let clone = { ...cell };
                                  // @ts-ignore
                                  clone.SoLuong = +e.target.value;

                                  row[
                                    new Date(resultDates[1] + '/' + resultDates[0] + '/' + resultDates[2]).toISOString()
                                  ][Id] = clone;

                                  if (!Number.isNaN(cell.SoLuong))
                                    editCell({
                                      Id: cell.Id,
                                      IdBaoTapChi: Id,
                                      Ngay: cell.Ngay,
                                      SoLuong: cell.SoLuong
                                    });
                                }
                              }}
                              disabled={loadingEditCell}
                              min={0}
                              maxLength={5}
                            />
                          </td>
                        );
                      })}
                    </tr>
                  );
                })}
              </tbody>

              <tfoot className='sticky bottom-0 bg-[rgb(235,244,250)] text-center font-bold text-primary-10'>
                <tr>
                  <td className='text-left'>Tổng số báo, tạp chí</td>

                  {dateValue.map((itemDate) => {
                    let sum = 0;
                    const resultDates = itemDate.split('/');

                    for (const key in row) {
                      if (
                        key === new Date(resultDates[1] + '/' + resultDates[0] + '/' + resultDates[2]).toISOString()
                      ) {
                        for (const _key in row[key]) {
                          if (!Number.isNaN(row[key][_key].SoLuong)) sum += row[key][_key].SoLuong;
                        }
                        break;
                      }
                    }
                    return <td key={itemDate}>{sum}</td>;
                  })}
                </tr>
              </tfoot>
            </>
          )}
        </table>
      </div>

      <div className='mt-3 flex justify-end gap-2'>
        <Button variant='secondary' onClick={handleBack}>
          Quay về
        </Button>
      </div>

      <PrintNoteBook
        open={isPrint}
        onInvisile={tooglePrintModal}
        newspaperMagazines={newspaperMagazineData?.data.Item.ListBaoTapChi || []}
        onClose={() => setIsPrint(false)}
      />

      <RegisterModal
        open={isRegisterModalVisible}
        onInvisile={handleInvisibleRegisterModal}
        id={idNewspaperMagazine}
        onSuccess={handleResiterSuccess}
      />

      <Loading open={loadingEditCell} />
    </div>
  );
};

export default StatisticByWeek;
