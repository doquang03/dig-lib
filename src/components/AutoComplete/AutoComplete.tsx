import { Select } from 'antd';
import classNames from 'classnames';
import 'css/AutoComplete.css';
import { Control, Controller, FieldValues, Path } from 'react-hook-form';

interface RHFDatePickerFieldProps<FormValues extends FieldValues> {
  control: Control<any>;
  name: Path<FormValues>;
  errorMessage?: string;
  items: Selections;
  className?: string;
  mode?: 'multiple' | 'tags' | undefined;
  placeholder?: string;
  showSearch?: boolean;
  disabled?: boolean;
  listHeight?: number;
}

function RHFDatePickerField<FormValues extends FieldValues>(props: RHFDatePickerFieldProps<FormValues>) {
  const Option = Select.Option;
  const {
    control,
    name,
    errorMessage,
    items,
    className,
    mode,
    placeholder = 'Nhập...',
    showSearch = false,
    disabled = false,
    listHeight,
    ...rest
  } = props;
  return (
    <>
      <Controller
        control={control}
        name={name}
        render={({ field: { onChange, value } }) => {
          return (
            <Select
              mode={mode}
              className={classNames(`select-antd-tags  ${className}`, {
                'rounded-md border border-red-500': !!errorMessage
              })}
              placeholder={placeholder}
              filterOption={(inputValue, option?: Option) =>
                option?.children?.toLowerCase().includes(inputValue.toLowerCase()) || false
              }
              value={value}
              onChange={(value) => onChange(value)}
              showSearch={showSearch}
              disabled={disabled}
              listHeight={listHeight}
              {...rest}
            >
              {items.map((element) => {
                return (
                  <Option value={element.value} key={element.value}>
                    {element.label}
                  </Option>
                );
              })}
            </Select>
          );
        }}
      ></Controller>

      {!!errorMessage && <p className='text-[14px] text-red-500'>{errorMessage}</p>}
    </>
  );
}

export default RHFDatePickerField;
