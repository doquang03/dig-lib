type NotificationApp = {
  Id: string;
  CreateDateTime: string;
  TieuDe: string;
  HinhNen: string;
  NoiDung: string;
  IsAll: boolean;
  IsActive: boolean;
  ThoiGian: string;
};
