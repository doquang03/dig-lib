import { DeleteFilled, EditOutlined } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Tag, Tooltip } from 'antd';
import Table, { ColumnsType } from 'antd/es/table';
import { statusBooksApis } from 'apis';
import classNames from 'classnames';
import { Button, Empty, ModalDelete, SearchForm, SizeChanger, Title, TitleDelete } from 'components';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import { usePaginationNavigate, useQueryConfig } from 'hooks';
import { useCallback, useMemo, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getSerialNumber } from 'utils/utils';

let locale = {
  emptyText: 'Không tìm được kết quả phù hợp'
};

const StatusBookManagementPage = () => {
  const [seletedStatusBook, setSeletedStatusBook] = useState<StatusBook | undefined>();

  const queryConfig = useQueryConfig();

  const navigate = useNavigate();

  const { isAllowedAdjustment } = useUser();

  const {
    data: stattusBooksData,
    isLoading: loadingStatusBook,
    refetch,
    isRefetching
  } = useQuery({
    queryKey: ['statusBooks', queryConfig],
    queryFn: () => statusBooksApis.getStatusBooks(queryConfig)
  });

  const statusBooks = useMemo(() => {
    if (!stattusBooksData?.data.Item) {
      return;
    }

    return stattusBooksData?.data?.Item;
  }, [stattusBooksData?.data?.Item]);

  const { mutate: deleteStatusBook, isLoading: loadingDeleteStatusBook } = useMutation({
    mutationFn: (id: string) => statusBooksApis.deleteStatusBook(id),
    onSuccess: (data) => {
      if (data.data.Code === 200) {
        refetch();
        toast.success('Xóa tình trạng sách thành công');
        setSeletedStatusBook(undefined);

        return;
      }

      toast.error(data.data.Message);
      setSeletedStatusBook(undefined);
    }
  });

  const columns = useMemo(() => {
    const _columns: ColumnsType<StatusBook> = [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        render: (value, record, index) => {
          return (
            queryConfig.page && queryConfig.pageSize && getSerialNumber(+queryConfig.page, +queryConfig.pageSize, index)
          );
        },
        width: 100
      },
      {
        title: 'Tên tình trạng sách',
        dataIndex: 'TenTT',
        key: 'TenTT',
        render: (value, { TenTT }) => (
          <Tooltip placement='topLeft' title={TenTT} arrow={true}>
            <p>{TenTT}</p>
          </Tooltip>
        ),
        onCell: (record) => ({
          className: 'text-left'
        }),
        ellipsis: true
      },
      {
        title: 'Cho phép mượn sách',
        dataIndex: 'TrangThai',
        key: 'TrangThai',
        render: (value, record) => {
          const statusLabel = record.TrangThai ? 'Cho phép mượn' : 'Không cho phép mượn';
          return <Tag color={record.TrangThai ? 'success' : 'warning'}>{statusLabel}</Tag>;
        }
      }
    ];

    if (isAllowedAdjustment) {
      _columns.push({
        title: 'Hành động',
        dataIndex: 'actions',
        key: 'actions',
        width: 200,
        render: (value, record) => {
          return (
            <>
              <button
                className='mx-2'
                onClick={(e) => {
                  navigate(`CapNhat/${record.Id}`);
                }}
              >
                <EditOutlined style={{ fontSize: '20px', color: '#08c' }} />
              </button>

              {!record.DenBu && (
                <button
                  className='mx-2'
                  onClick={(e) => {
                    setSeletedStatusBook(record);
                  }}
                >
                  <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
                </button>
              )}
            </>
          );
        }
      });
    }

    return _columns;
  }, [isAllowedAdjustment, navigate, queryConfig.page, queryConfig.pageSize]);

  const handleNavigation = usePaginationNavigate();

  const onPaginate = useCallback(
    (page: number, pageSize: number) => {
      handleNavigation({ ...queryConfig, page: page + '', pageSize: pageSize + '' });
    },
    [handleNavigation, queryConfig]
  );

  const pagination = useMemo(() => {
    let size = 30;
    let currentPage = 1;

    if (queryConfig.pageSize) {
      size = +queryConfig.pageSize;
    }
    if (queryConfig.page) {
      currentPage = +queryConfig.page;
    }

    return {
      current: currentPage,
      pageSize: size,
      total: statusBooks?.TotalRecord,
      onChange: onPaginate,
      hideOnSinglePage: true,
      showSizeChanger: false
    };
  }, [statusBooks?.TotalRecord, onPaginate, queryConfig.page, queryConfig.pageSize]);

  const handleAddStatusBook = () => navigate(path.themTrangThaiSach);

  return (
    <div className='p-5'>
      <Title title='Danh sách tình trạng sách' />

      <div className='mt-4'>
        <SearchForm placeholder='Nhập tên tình trạng sách' />

        <Button
          className={classNames('', {
            hidden: !isAllowedAdjustment,
            'my-2 w-full md:w-auto': isAllowedAdjustment
          })}
          onClick={handleAddStatusBook}
        >
          Thêm tình trạng sách
        </Button>

        <div className='relative '>
          <Table
            loading={loadingStatusBook || isRefetching}
            columns={columns}
            dataSource={statusBooks?.ListTrangThai || []}
            pagination={pagination}
            scroll={{ x: 1500 }}
            rowKey={(record) => record.Id}
            className='custom-table mt-3'
            bordered
            locale={{
              emptyText: () => <Empty label={locale.emptyText} />
            }}
          />

          <div
            className={classNames('relative', {
              // @ts-ignore
              'mt-[64px]': (queryConfig.page && +queryConfig.pageSize) >= statusBooks?.TotalRecord
            })}
          >
            <div className='absolute bottom-1'>
              <SizeChanger
                visible={!!statusBooks && statusBooks?.TotalRecord > 0}
                value={statusBooks?.ListTrangThai.length + ''}
                total={statusBooks?.TotalRecord + ''}
              />
            </div>
          </div>
        </div>
      </div>

      <ModalDelete
        open={Boolean(seletedStatusBook)}
        closable={false}
        title={
          <TitleDelete firstText='Bạn có chắn chắn muốn xóa trạng thái sách' secondText={seletedStatusBook?.TenTT} />
        }
        handleCancel={() => setSeletedStatusBook(undefined)}
        handleOk={() => {
          deleteStatusBook(seletedStatusBook?.Id + '');
        }}
        loading={loadingDeleteStatusBook}
      />
    </div>
  );
};

export default StatusBookManagementPage;
