import { TreeSelect } from 'antd';
import { BaseOptionType, DefaultOptionType } from 'antd/es/select';
import classNames from 'classnames';
import 'css/AutoComplete.css';
import 'css/TreeSelect.css';
import { Control, Controller, FieldPath, FieldValues, Path } from 'react-hook-form';

interface TreeSelectProps<FormValues extends FieldValues> {
  control?: Control<any>;
  name: FieldPath<FormValues>;
  errorMessage?: string;
  items: (BaseOptionType | DefaultOptionType)[];
  className?: string;
  mode?: 'multiple' | 'tags' | undefined;
  placeholder?: string;
  showSearch?: boolean;
  disabled?: boolean;
  listHeight?: number;
}

function TreeSelectCustom<FormValues extends FieldValues>(props: TreeSelectProps<FormValues>) {
  const {
    control,
    name,
    errorMessage,
    items,
    className,
    mode,
    placeholder = 'Nhập...',
    showSearch = false,
    disabled = false,
    listHeight,
    ...rest
  } = props;
  return (
    <>
      <Controller
        control={control || undefined}
        name={name}
        render={({ field: { onChange, value } }) => {
          return (
            <TreeSelect
              className={classNames(`select-antd-tags  ${className}`, {
                'rounded-md border border-red-500': !!errorMessage
              })}
              style={{ width: '100%' }}
              treeDefaultExpandAll
              placeholder={placeholder}
              value={value}
              onChange={(value) => onChange(value)}
              showSearch={showSearch}
              disabled={disabled}
              listHeight={listHeight}
              treeData={items}
              {...rest}
            ></TreeSelect>
          );
        }}
      ></Controller>

      {!!errorMessage && <p className='text-[14px] text-red-500'>{errorMessage}</p>}
    </>
  );
}

export default TreeSelectCustom;
