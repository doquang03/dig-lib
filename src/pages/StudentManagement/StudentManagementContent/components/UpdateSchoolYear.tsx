import { useMutation } from '@tanstack/react-query';
import { Radio, RadioChangeEvent, Select, Tooltip } from 'antd';
import { newspaperMagazineApis, studentApis } from 'apis';
import { AlertIcon } from 'assets';
import classNames from 'classnames';
import { BaseModal, Input } from 'components';
import { SCHOOL_YEAR_OPTIONS } from 'constants/options';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';

type Props = {
  open: boolean;
  loading: boolean;
  onClose: VoidFunction;
  onSuccess: VoidFunction;
  listId: string[];
  countFail: Student[];
};

const UpdateSchoolYear = (props: Props) => {
  const { open, listId, onClose, countFail, onSuccess, loading } = props;

  const [formValues, setFormValue] = useState<{
    automation: boolean;
    targetClass: string;
  }>({
    automation: true,
    targetClass: ''
  });

  const { mutate, isLoading } = useMutation({
    mutationFn: (targetClass: string) => studentApis.UpdateSchoolYearJuniorByList(listId, targetClass),
    onSuccess: () => {
      toast.success('Cập nhật năm học thành công');
      onSuccess();
    }
  });

  const handleChangeAutomation = (e: RadioChangeEvent) => {
    if (!e) return;

    const { value } = e.target;

    setFormValue((prevState) => {
      return {
        ...prevState,
        automation: value
      };
    });
  };

  const _handleUpdate = () => {
    if (formValues.automation) mutate('');
    else mutate(formValues.targetClass);
  };

  const handleBack = () => {
    setFormValue({
      automation: true,
      targetClass: ''
    });

    onClose();
  };

  return (
    <BaseModal
      title={'Cập nhật năm học'}
      open={open}
      onInvisile={handleBack}
      onOk={_handleUpdate}
      loading={isLoading || loading}
      labelOk='Cập nhật'
    >
      <div className='my-2 flex flex-col gap-2 text-base'>
        <div className='flex flex-grow flex-col gap-3'>
          {countFail.length !== 0 && (
            <Tooltip placement='topLeft' title={countFail.map((_) => _.Ten).join(', ')}>
              <div className='basis-full'>
                <AlertIcon />
                <span className='ml-2 text-xs italic text-[#A23434]'>
                  Có <span className='font-extrabold'>{countFail.length} học sinh</span> không thể cập nhật năm học
                </span>
              </div>
            </Tooltip>
          )}
          <label className='basis-1/4 font-bold'>
            Chọn loại cập nhật: <span className='text-danger-10'>*</span>
          </label>

          <div className='flex-grow basis-full'>
            <Radio.Group
              onChange={handleChangeAutomation}
              value={formValues?.automation}
              name='automation'
              disabled={isLoading}
              className='flex flex-grow basis-full flex-col gap-3'
            >
              <Radio value={true} className='text-base'>
                Tự động
              </Radio>
              <Radio value={false} className='text-base'>
                <span className='flex items-center gap-3'>
                  Lớp khác{' '}
                  <Input
                    style={{ padding: '5px', textAlign: 'center' }}
                    value={formValues.targetClass}
                    onChange={(e) => {
                      setFormValue((prevState) => {
                        return {
                          ...prevState,
                          targetClass: e.target.value
                        };
                      });
                    }}
                  ></Input>
                </span>
              </Radio>
            </Radio.Group>
          </div>
        </div>
      </div>
    </BaseModal>
  );
};

export default UpdateSchoolYear;
