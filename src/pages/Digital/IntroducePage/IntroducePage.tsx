import { TabsProps } from 'antd';

import { Tabs, Title } from 'components';
import { useUser } from 'contexts/user.context';
import { useState } from 'react';
import Introduce from './Introduce';
import TimeServer from './TimeServer';
import ImageLogo from './ImageLogo';

const IntroducePage = () => {
  const { userType, isAllowedAdjustment } = useUser();
  const [activeTab, setActiveTab] = useState<string>('imagelogo');
  const handleChangeTab = (activeTab: string) => setActiveTab(activeTab);

  if (userType === 2001 && !isAllowedAdjustment) {
    return <h2 className='p-2 '>Bạn không có quyền truy cập chức năng này</h2>;
  }

  const items: TabsProps['items'] = [
    {
      key: 'imagelogo',
      label: `Hình ảnh, logo`,
      children: <ImageLogo activeTab={activeTab} />,
      forceRender: true
    },
    {
      key: 'timeserver',
      label: `Thời gian phục vụ`,
      children: <TimeServer activeTab={activeTab} />,
      forceRender: true
    },
    {
      key: 'introduce',
      label: `Nội dung giới thiệu`,
      children: <Introduce activeTab={activeTab} />,
      forceRender: true
    }
  ];

  return (
    <div className='setting-page p-5'>
      <Title title='Giới thiệu thư viện' />
      <Tabs
        items={items}
        activeKey={activeTab}
        onChange={handleChangeTab}
        destroyInactiveTabPane={false}
        className='mt-3'
      />
    </div>
  );
};

export default IntroducePage;
