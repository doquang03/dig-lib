import { path } from 'constants/path';
import { Outlet, useMatch } from 'react-router-dom';
import PublishingCompanyManagermentContent from './PublishingCompanyManagermentContent/PublishingCompanyManagermentContent';

const PublishingCompanyManagementPage = () => {
  const match = useMatch(path.nhaxuatban);

  return <>{Boolean(match) ? <PublishingCompanyManagermentContent /> : <Outlet />}</>;
};

export default PublishingCompanyManagementPage;
