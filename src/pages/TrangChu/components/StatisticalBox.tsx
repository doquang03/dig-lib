import { NetworkingIcon, NoteIcon, ReadingBookIcon, RentingBookIcon } from '../assets/icons';

type Props = {
  type: 'totalRentingBooks' | 'totalRentingReaders' | 'totalBookedReaders' | 'totalVisitingReaders';
  value: number;
};

const contentStatisticalBox = {
  label: {
    totalRentingBooks: 'Tổng số sách đang mượn',
    totalRentingReaders: 'Tổng số bạn đọc đang mượn',
    totalBookedReaders: 'Tổng số bạn đọc đang đặt mượn',
    totalVisitingReaders: 'Số bạn đọc liên TV đang mượn'
  },
  labelColor: {
    totalRentingBooks: '#119757',
    totalRentingReaders: '#1D396A',
    totalBookedReaders: '#A23434',
    totalVisitingReaders: '#D4CD16'
  },
  icon: {
    totalRentingBooks: <RentingBookIcon />,
    totalRentingReaders: <ReadingBookIcon />,
    totalBookedReaders: <NoteIcon />,
    totalVisitingReaders: <NetworkingIcon />
  }
};

function StatisticalBox(props: Props) {
  return (
    <div className='grid h-40 place-items-center items-center gap-3 rounded-md bg-white shadow-md'>
      {contentStatisticalBox.icon[props.type]}

      <div className='flex flex-col items-center justify-center'>
        <p className='text-center text-base'>{contentStatisticalBox.label[props.type]}</p>

        <p
          style={{
            color: contentStatisticalBox.labelColor[props.type],
            fontSize: 32,
            fontWeight: 'bold'
          }}
        >
          {props.value || '--'}
        </p>
      </div>
    </div>
  );
}

export default StatisticalBox;
