import { CKEditor } from '@ckeditor/ckeditor5-react';
import { useMutation, useQuery } from '@tanstack/react-query';
import { settingApis } from 'apis';
import CustomEditorBuild from 'ckeditor5-custom-build';
import { Button, Loading } from 'components';
import { useEffect, useMemo, useState } from 'react';
import { toast } from 'react-toastify';
type Props = {
  activeTab: string;
};
const TimeServer = (props: Props) => {
  const { activeTab } = props;

  const [thoiGianPhucVu, setThoiGianPhucVu] = useState('');
  const [editor, setEditor] = useState<typeof CustomEditorBuild>();

  const { isFetching, remove } = useQuery({
    queryKey: ['IndexThoiGianPhucVu', activeTab],
    queryFn: () => settingApis.IndexThoiGianPhucVu(),
    enabled: activeTab === 'timeserver',
    onSuccess: (res) => {
      let Item = res?.data?.Item;
      setThoiGianPhucVu(Item.ThoiGianPhucVu);
    }
  });

  const { mutate: ExcuteThoiGianPhucVu, isLoading: isLoadingSet } = useMutation({
    mutationKey: ['SetThoiGianPhucVu'],
    mutationFn: () => settingApis.SetThoiGianPhucVu(editor?.getData()),
    onSuccess: (res) => {
      toast.success('Cập nhật thành công thời gian phục vụ');
    }
  });

  const disiable = useMemo(() => {
    if (thoiGianPhucVu === '') return true;
    return false;
  }, [thoiGianPhucVu]);

  useEffect(() => {
    return () => remove();
  }, []);

  return (
    <>
      <div
        className='form-group form-group-sm row ckeditor-container'
        style={{ margin: 'auto', paddingTop: '1.25rem' }}
      >
        <CKEditor
          editor={CustomEditorBuild}
          data={thoiGianPhucVu}
          onReady={(editor) => {
            setEditor(editor);
          }}
          onChange={() => {
            setThoiGianPhucVu(editor?.getData());
          }}
        ></CKEditor>
      </div>
      <div className='mt-5 flex justify-end'>
        <Button
          type='button'
          className='ml-2 font-semibold'
          variant={disiable ? 'disabled' : 'default'}
          disabled={disiable}
          onClick={() => {
            if (thoiGianPhucVu.replaceAll(/<.*?>/g, '').length <= 500) ExcuteThoiGianPhucVu();
            else toast.error('Nội dung chứa tối đa 500 ký tự');
          }}
        >
          Cập nhật
        </Button>
      </div>
      <Loading open={isFetching || isLoadingSet}></Loading>
    </>
  );
};

export default TimeServer;
