import { Outlet } from 'react-router-dom';

const HostLibraryManagement = () => <Outlet />;

export default HostLibraryManagement;
