import httpThuVien from 'utils/httpThuVien';
import { serialize } from 'utils/utils';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/DatMuon`;

type AccountType = '' | 'gv' | 'hs';

const bookReserving = {
  getListOfBooksReserving: (
    params: Partial<{
      TextForSearch: string;
      LoaiTK: string;
      SortTenBanDoc: boolean;
      SortMaThanhVien: boolean;
      SortLopTo: boolean;
      SortThoiGianHetHieuLucGanNhat: boolean;
      IsGlobal?: boolean;
    }> & { page: number; pageSize: number }
  ) => {
    return httpThuVien.post<ResponseApi<{ List_OrderBooks: BookReserving[]; count: number }>>(
      `${endPoint}/DanhSachDatMuon`,
      params
    );
  },
  cancelReservation: (IdUser: string) =>
    httpThuVien.post<ResponseApi>(`${endPoint}/HuyDanhSachDatMuon?IdUser=${IdUser}`),
  getStatisticalBookReserving: (
    params: {
      page: number;
      pageSize: number;
    } & Partial<{
      year: number;
      month: number;
      day: number;
      TextForSearch: string;
      LoaiTK: string;
      SortTenSach: boolean;
      SortMaKSCB: boolean;
      SortTenBanDoc: boolean;
      SortLopTo: boolean;
      SortMaThanhVien: boolean;
      SortThoiGianDatMuon: boolean;
      SortThoiGianHetHieuLuc: boolean;
      SortTrangThai: boolean;
    }>
  ) =>
    httpThuVien.post<
      ResponseApi<{ List_OrderBooks: StatisticalBookReserving[]; countUser: number; countBook: number; count: number }>
    >(`${endPoint}/ThongKeDatMuon`, params),
  getDataByWeek: (params: { typeAccount?: AccountType; dateTime?: string }) =>
    httpThuVien.get<
      ResponseApi<{
        soDangDatMuonTrongTuan: number[];
        soHetHieuLucTrongTuan: number[];
        soHoanTatTrongTuan: number[];
        soHuyTrongTuan: number[];
        listDates: string[];
      }>
    >(`${endPoint}/StartDayAndlastDay?${serialize(params)}`),
  getDataByMonthAndYear: (params: { typeAccount?: AccountType; month?: number; year?: number }) =>
    httpThuVien.get<
      ResponseApi<{
        soDangDatMuonTrongThang: number[];
        soHetHieuLucTrongThang: number[];
        soHoanTatTrongThang: number[];
        soHuyTrongThang: number[];
        soDangDatMuonTrongNam: number[];
        soHetHieuLucTrongNam: number[];
        soHoanTatTrongNam: number[];
        soHuyTrongNam: number[];
      }>
    >(`${endPoint}/BieuDoPhieuDatMuon?${serialize(params)}`),
  exportByWeek: (params: { typeAccount?: AccountType; dateTime: string }) =>
    httpThuVien.get(`${endPoint}/ExportStartDayAndlastDay?${serialize(params)}`, {
      responseType: 'blob'
    }),
  exportByMonthYear: (params: { typeAccount?: AccountType; month?: number; year?: number }) =>
    httpThuVien.get(`${endPoint}/ExportBieuDoPhieuDatMuon?${serialize(params)}`, {
      responseType: 'blob'
    })
};

export default bookReserving;
