import { Outlet } from 'react-router-dom';

const ExportReportPage = () => {
  return (
    <div>
      <Outlet />
    </div>
  );
};

export default ExportReportPage;
