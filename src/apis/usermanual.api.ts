import httpThuVien from 'utils/httpThuVien';

const endpoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/HuongDanSuDung`;

const userManualApis = {
  addNewUserManual: (payload: FormData) => {
    return httpThuVien.post<ResponseApi>(`/${endpoint}/Create`, payload, {
      headers: { 'Content-Type': 'multipart/form-data', Accept: '*/*' }
    });
  },
  updateNewUserManual: (payload: FormData) => {
    return httpThuVien.post<ResponseApi>(`/${endpoint}/Edit`, payload, {
      headers: { 'Content-Type': 'multipart/form-data', Accept: '*/*' }
    });
  },
  getUserManuals: () => {
    return httpThuVien.post<ResponseApi<{ ListModel: UserManual[] }>>(`/${endpoint}/GetAll`);
  },
  removeUserManual: (id: string) => httpThuVien.post<ResponseApi>(`/${endpoint}/Delete?id=${id}`)
};

export default userManualApis;
