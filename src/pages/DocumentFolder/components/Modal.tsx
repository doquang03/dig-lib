import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { Breadcrumb, Modal, ModalProps, Switch } from 'antd';
import { ItemType } from 'antd/es/breadcrumb/Breadcrumb';
import { documentFolderApis } from 'apis';
import { AxiosError } from 'axios';
import { Button, Input, Select } from 'components';
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { NavLink } from 'react-router-dom';
import { toast } from 'react-toastify';
import { documentFolderSchema } from 'utils/rules';
type Props = {
  documentId?: string;
  parent?: DocumentFolder;
  routes: ItemType[];
  onBack: VoidFunction;
} & ModalProps;

type FormValues = {
  [key in keyof DocumentFolder]: string;
};

const DocumentFolderModal = (props: Props) => {
  const { documentId, parent, routes, onBack, open, ...rest } = props;
  const [isActive, setIsActive] = useState(true);

  const queryClient = useQueryClient();

  const {
    register,
    handleSubmit,
    reset,
    setValue,
    formState: { errors },
    setError
  } = useForm<FormValues>({
    defaultValues: {
      TenThuMuc: '',
      MaThuMuc: '',
      GhiChu: '',
      Type: ''
    },
    resolver: yupResolver(documentFolderSchema)
  });

  const { isLoading: isLoadingDocumentFolder } = useQuery({
    queryKey: ['DocumentFolder'],
    queryFn: () => documentFolderApis.getDocument(documentId + ''),
    enabled: Boolean(documentId),
    onSuccess(data) {
      const { TenThuMuc, MaThuMuc, Type, GhiChu, IsActive } = data.data.Item.ThuMucSach;
      setValue('TenThuMuc', TenThuMuc);
      setValue('MaThuMuc', MaThuMuc);
      setValue('Type', Type || 'paper');
      setValue('GhiChu', GhiChu);
      setIsActive(IsActive);
    }
  });

  const { mutate: updateDocumentFolder, isLoading: isLoadingUpdateDocumentFolder } = useMutation({
    mutationFn: documentFolderApis.updateDocument,
    onSuccess: () => {
      reset();
      if (!documentId) toast.success('Thêm mới thư mục thành công');
      else toast.success('Cập nhật thư mục thành công');
      queryClient.invalidateQueries(['documentFolders']);
      onBack();
    },
    onError: (error: AxiosError<ResponseApi<{ TenThuMuc: string }>>) => {
      if (error.response?.data.Message.includes('Mã thư mục'))
        setError('MaThuMuc', { type: 'duplicate', message: error.response?.data.Message });
      else setError('TenThuMuc', { type: 'duplicate', message: error.response?.data.Message });
    }
  });

  const isDisable = (isLoadingDocumentFolder && !!documentId) || isLoadingUpdateDocumentFolder;

  function itemRender(route: ItemType, params: any, routes: ItemType[], paths: string[]) {
    const last = routes.indexOf(route) === routes.length - 1;
    return last ? (
      <span>{route.breadcrumbName}</span>
    ) : (
      <NavLink to={paths[paths.length - 1]}>{route.breadcrumbName}</NavLink>
    );
  }

  useEffect(() => {
    if (!parent) return;
    if (parent) return setValue('Type', parent?.Type + '' || 'paper');
    else return setValue('Type', '');
  });

  const onSubmit = handleSubmit((data) => {
    data.MaThuMuc = data.MaThuMuc.replace(/\s\s+/g, ' ');
    data.TenThuMuc = data.TenThuMuc.replace(/\s\s+/g, ' ');
    data.GhiChu = data.GhiChu.replace(/\s\s+/g, ' ');
    setValue('MaThuMuc', data.MaThuMuc);
    setValue('TenThuMuc', data.TenThuMuc);
    setValue('GhiChu', data.GhiChu);

    updateDocumentFolder({
      Id: documentId || '',
      TenThuMuc: data.TenThuMuc,
      CreateDateTime: new Date().toISOString(),
      GhiChu: data.GhiChu,
      IdParent: parent?.Id || '',
      MaThuMuc: data.MaThuMuc,
      //@ts-ignore
      Type: data.Type,
      IsActive: isActive
    });
  });

  const handleBack = () => {
    onBack();
    reset();
  };

  return (
    <Modal
      closable={false}
      footer={null}
      open={open}
      {...rest}
      title={
        <>
          <h3 className='border-b-2 pb-2 font-bold uppercase text-primary-10'>
            {Boolean(documentId) ? 'Cập nhật thư mục tài liệu' : 'Thêm thư mục tài liệu'}
          </h3>
        </>
      }
    >
      <Breadcrumb
        className='rounded-sm bg-secondary-30 p-2 font-semibold text-secondary-20'
        separator='>'
        items={routes}
        itemRender={itemRender}
      />
      <div className='mt-3'>
        <label className='font-bold'>
          Loại tài liệu <span className='text-danger-10'>*</span>{' '}
        </label>

        <Select
          register={register}
          name='Type'
          className='w-full'
          errorMessage={errors.Type?.message}
          items={[
            { value: '', label: 'Chọn loại tài liệu' },
            { value: 'paper', label: 'Sách in' },
            { value: 'document', label: 'Sách điện tử' },
            { value: 'image', label: 'Album ảnh' },
            { value: 'audio', label: 'Sách nói' },
            { value: 'video', label: 'Video' }
          ]}
          disabled={Boolean(parent) || Boolean(documentId)}
        />
      </div>
      <div className='my-3'>
        <label className='font-bold'>
          Mã thư mục <span className='text-danger-10'>*</span>{' '}
        </label>

        <Input
          placeholder='Nhập mã thư mục'
          register={register}
          name='MaThuMuc'
          errorMessage={errors.MaThuMuc?.message}
          maxLength={101}
        />
      </div>
      <div className='my-3'>
        <label className='font-bold'>
          Tên thư mục <span className='text-danger-10'>*</span>{' '}
        </label>

        <Input
          placeholder='Nhập tên thư mục'
          register={register}
          name='TenThuMuc'
          errorMessage={errors.TenThuMuc?.message}
          maxLength={256}
        />
      </div>
      <div className='mt-3'>
        <label className='font-bold'>Ghi chú</label>
        <Input placeholder='Nhập nội dung ghi chú' register={register} name='GhiChu' maxLength={256} />
      </div>
      {documentId && (
        <div className='my-3 grid grid-cols-12 gap-3'>
          <p className='col-span-3 font-bold'>Trạng thái</p>

          <div className='col-span-8 flex items-center gap-1'>
            <Switch
              className='bg-primary-10'
              checked={isActive}
              onChange={(value: boolean) => {
                setIsActive(value);
              }}
            />
          </div>
        </div>
      )}{' '}
      <div className='mt-3 flex items-center justify-end gap-2'>
        <Button variant='secondary' disabled={isDisable} loading={isDisable} onClick={handleBack}>
          Quay về
        </Button>

        <Button onClick={onSubmit} disabled={isDisable} loading={isDisable}>
          {!documentId ? 'Thêm mới' : 'Cập nhật'}
        </Button>
      </div>
    </Modal>
  );
};

export default DocumentFolderModal;
