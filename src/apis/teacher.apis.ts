import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/GiaoVien`;

const teacherApis = {
  getTeachers: (queryString: string) => {
    return httpThuVien.post(`/${endPoint}/IndexByModel` + queryString);
  },
  downloadMauThe: (payload: any) => {
    return httpThuVien.post(`/${endPoint}/MauThe`, payload, { responseType: 'arraybuffer' });
  },
  EditByID: (id: string) => {
    return httpThuVien.post(`/${endPoint}/EditByID?id=${id}`);
  },
  ResetPassword: (id: string) => {
    return httpThuVien.post(`/${endPoint}/ResetPassword?idThanhVien=${id}`);
  },
  DeleteSingle: (id: string) => {
    return httpThuVien.post(`/${endPoint}/DeleteSingle?idThanhVien=${id}`);
  },
  UpLoadAvatar_Save: (payload: object) => {
    return httpThuVien.post(`/${endPoint}/UpLoadAvatar_Save`, payload);
  },
  ImportSave: (payload: object) => {
    return httpThuVien.post(`/${endPoint}/ImportSave`, payload);
  },
  DownFileExcel: (payload: any) => {
    return httpThuVien.post(`/${endPoint}/DownFileExcel`, payload, { responseType: 'arraybuffer' });
  },
  DeleteMulti: (payload: object) => {
    return httpThuVien.post(`/${endPoint}/DeleteMulti`, payload);
  },
  ChangeStatus: (id: string) => {
    return httpThuVien.post(`/${endPoint}/ChangeStatus?id=` + id);
  },
  EditAdvance: (payload: object) => {
    return httpThuVien.post(`/${endPoint}/EditAdvance`, payload);
  },
  IndexByCanBoThuVien: (queryString: string) => {
    return httpThuVien.post(`/${endPoint}/IndexByCanBoThuVien` + queryString);
  },
  CheckMail: (mail: string) => {
    return httpThuVien.post(`/${endPoint}/CheckMail?mail=` + mail);
  },
  Edit: (payload: object) => {
    return httpThuVien({
      method: 'post',
      url: `/${endPoint}/Edit`,
      data: payload,
      headers: { 'Content-Type': 'multipart/form-data' }
    });
  },
  CreateUserByModel: (payload: object) => {
    return httpThuVien({
      method: 'post',
      url: `/${endPoint}/CreateUserByModel`,
      data: payload,
      headers: { 'Content-Type': 'multipart/form-data' }
    });
  },
  UpLoadAvatar_Preview: (payload: object) => {
    return httpThuVien({
      method: 'post',
      url: `/${endPoint}/UpLoadAvatar_Preview`,
      data: payload,
      headers: { 'Content-Type': 'multipart/form-data' }
    });
  },
  PreviewImport: (payload: object) => {
    return httpThuVien({
      method: 'post',
      url: `/${endPoint}/PreviewImport`,
      data: payload,
      headers: { 'Content-Type': 'multipart/form-data' }
    });
  }
};

export default teacherApis;
