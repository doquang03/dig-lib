import { CameraFilled, CheckOutlined, FileExcelOutlined, SecurityScanOutlined } from '@ant-design/icons';
import classnames from 'classnames';

type Props = {
  activeStep: number;
  type: 'excel' | 'avatar';
};

/******************
 *  How to use ProgressBar Component
 *  activeStep is required!
 *  type 'excel' for Excel icon
 *  type 'avatar' for Camera icon
 * ****************/

const ProgressBar = (props: Props) => {
  const { activeStep = 1, type } = props;

  return (
    <div className='relative flex items-center justify-around overflow-hidden'>
      <div className={'absolute top-[38%] min-h-[1px] w-full bg-secondary-30'} />
      <div
        className={classnames(
          'absolute top-[38%] left-0 min-h-[2px] w-[15%] transform bg-danger-40 transition-all duration-300 ease-linear',
          {
            'w-[15%]': activeStep === 1,
            'w-[50%]': activeStep === 2,
            'w-[99.99%]': activeStep === 3
          }
        )}
      />
      <div className='z-40 flex flex-col items-center'>
        <div className='rounded-full bg-danger-40 p-4'>
          {type === 'excel' ? (
            <FileExcelOutlined className='text-[50px] text-white' />
          ) : (
            <CameraFilled className='text-[50px] text-white' />
          )}
        </div>
        <div className='mt-2 text-danger-40'>Tải lên</div>
      </div>

      <div className='z-40 flex flex-col items-center'>
        <div
          className={classnames('rounded-full  p-4 transition-all duration-300 ease-linear', {
            'bg-danger-40': activeStep > 1,
            'bg-secondary-30': activeStep === 1
          })}
        >
          <SecurityScanOutlined className='text-[50px] text-white' />
        </div>
        <div
          className={classnames('mt-2', {
            'text-danger-40': activeStep > 1,
            'text-secondary-30': activeStep === 1
          })}
        >
          Phân tích
        </div>
      </div>

      <div className='z-40 flex flex-col items-center'>
        <div
          className={classnames('rounded-full p-4 transition-all duration-300 ease-linear', {
            'bg-danger-40': activeStep > 2,
            'bg-secondary-30': activeStep === 1 || activeStep === 2
          })}
        >
          <CheckOutlined className='text-[50px] text-white' />
        </div>
        <div
          className={classnames('mt-2', {
            'text-danger-40': activeStep > 2,
            'text-secondary-30': activeStep === 1 || activeStep === 2
          })}
        >
          Kết quả
        </div>
      </div>
    </div>
  );
};

export default ProgressBar;
