import { Outlet } from 'react-router-dom';

const StatusBookPage = () => {
  return <Outlet />;
};

export default StatusBookPage;
