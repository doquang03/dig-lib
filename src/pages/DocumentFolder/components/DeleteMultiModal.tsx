import { InfoCircleOutlined } from '@ant-design/icons';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { documentFolderApis } from 'apis';
import classNames from 'classnames';
import { ModalDelete } from 'components';
import { toast } from 'react-toastify';

type Props = {
  documentFolders: DocumentFolder[];
  visible: boolean;
  onBack: VoidFunction;
  onSuccess: VoidFunction;
};

const DeleteMultiModal = (props: Props) => {
  const { documentFolders, visible, onBack, onSuccess } = props;

  const queryClient = useQueryClient();

  const { mutate, isLoading } = useMutation({
    mutationFn: () =>
      documentFolderApis.deleteDocuments(
        documentFolders.filter((document) => !document.IsUsed).map((documentFolder) => documentFolder.Id)
      ),
    onSuccess: () => {
      toast.success('Xóa thư mục tài liệu đã chọn thành công.');
      queryClient.invalidateQueries(['documentFolders']);
      onSuccess();
      onBack();
    }
  });

  const handleDeleteMulti = mutate;

  return (
    <ModalDelete
      open={visible}
      closable={false}
      title={
        <>
          <h3 className='text-bold'>Bạn có muốn chắc chắn xóa những thư mục tài liệu này không?</h3>
          <p className='text-[16px] font-extralight leading-normal'>Bạn sẽ không thể khôi phục sau khi xóa.</p>
        </>
      }
      handleCancel={onBack}
      handleOk={handleDeleteMulti}
      loading={isLoading}
    >
      <div className='max-h-[300px] overflow-x-auto'>
        {documentFolders?.map(({ Id, TenThuMuc, IsUsed }, index) => {
          return (
            <div key={Id} className='flex items-center border-b-2 px-2 py-2 font-bold'>
              <p className='mr-2'>{index + 1} </p>

              <p
                className={classNames('truncate text-left', {
                  'text-danger-10': IsUsed
                })}
              >
                {TenThuMuc}
              </p>

              {IsUsed && (
                <p className='flex flex-1 items-center justify-end gap-1 font-light text-danger-10'>
                  <InfoCircleOutlined style={{ color: 'red' }} />

                  <span className='text-[12px]'>Đang được sử dụng</span>
                </p>
              )}
            </div>
          );
        })}
      </div>
    </ModalDelete>
  );
};

export default DeleteMultiModal;
