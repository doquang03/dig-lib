import { Outlet } from 'react-router-dom';

const TitleApproval = () => <Outlet />;

export default TitleApproval;
