type Props = {
  document: ErrorSyncDocumentById;

  abbreviation?: string;
  errorMessage?: string;
  onClick?: VoidFunction;
};

function ErrorDocumentItem(props: Props) {
  const { document, abbreviation, errorMessage, onClick } = props;

  return (
    <button className='flex w-full items-center gap-2 border-b-[0.5px] pb-3 text-left' onClick={onClick}>
      <img
        src={!!document.LinkBiaSach ? `data:image/png;base64, ${document.LinkBiaSach}` : '/content/Book.png'}
        alt={document.TenSach}
        width={92}
        height={137}
      />

      <div className='flex flex-col gap-1'>
        <span>
          {document.TenSach} / {document.TacGia}. - Tái bản lần thứ {document.TaiBan}.- {document.NoiXuatBan || ''}.{' '}
          {document.NhaXuatBan || ''}
          {document.NamXuatBan || ''}. - {document.SoTrang || ''}tr. {document.MinhHoa ? `:${document.MinhHoa};` : ''}{' '}
          {document.KhoMau ? `${document.KhoMau}.` : ''}- {document.TungThu}
        </span>

        {!!abbreviation ? (
          <p className=''>
            <span className='font-bold'>Tóm tắt: </span>
            <span>{abbreviation}</span>
          </p>
        ) : null}
        {!!errorMessage ? (
          <p className=''>
            <span className='font-bold'>Danh sách lỗi: </span>

            <span className='mr-1 text-danger-10'>{errorMessage}</span>
          </p>
        ) : null}
      </div>
    </button>
  );
}

export default ErrorDocumentItem;
