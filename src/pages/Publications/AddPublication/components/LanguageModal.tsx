import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { Modal, type ModalProps } from 'antd';
import { languageApis } from 'apis';
import { AxiosError } from 'axios';
import { Button, Input } from 'components';
import { memo } from 'react';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import { languageSchema } from 'utils/rules';
import * as yup from 'yup';

type Props = {
  onBack: VoidFunction;
  onCreateSuccess: (languageValue: string) => void;
} & ModalProps;

type LanguageForm = yup.InferType<typeof languageSchema>;

const LanguageModal = (props: Props) => {
  const { title, onBack, onCreateSuccess, ...rest } = props;

  const queryClient = useQueryClient();

  const {
    register,
    handleSubmit,
    unregister,
    reset,
    setValue,
    setError,
    formState: { errors }
  } = useForm<LanguageForm>({
    defaultValues: {
      name: ''
    },
    resolver: yupResolver(languageSchema)
  });

  const { mutate: handleCreate, isLoading: isLoadingCreate } = useMutation({
    mutationFn: (payload: object) => languageApis.Create(payload),
    onSuccess: (data) => {
      toast.success('Thêm ngôn ngữ thành công');
      queryClient.invalidateQueries(['GetInitCreateForm']);
      reset();
      onBack();
      onCreateSuccess(data.data.Item.id);
    },
    onError: (error: AxiosError<ResponseApi>) => {
      setError('name', { message: error.response?.data.Message, type: 'validate' });
    }
  });

  const onSubmit = handleSubmit((data) => {
    let { name, shortname = '' } = data;

    name = name.replace(/ +(?= )/g, '');
    shortname = shortname.replace(/ +(?= )/g, '');

    setValue('name', name);
    setValue('shortname', shortname);

    handleCreate({ Ten: name, TenNgan: shortname });
  });

  const handleClose = () => {
    unregister('name');
    reset();
    onBack();
  };

  return (
    <Modal
      title={<h3 className='font-bold uppercase text-primary-10'>{title}</h3>}
      {...rest}
      footer={null}
      closable={false}
    >
      <form onSubmit={onSubmit}>
        <label className='mt-3 font-bold'>
          Tên ngôn ngữ <span className='text-danger-10'>*</span>
        </label>
        <Input
          placeholder='Nhập tên ngôn ngữ'
          register={register}
          name='name'
          errorMessage={errors?.name?.message}
          disabled={isLoadingCreate}
          maxLength={256}
          containerClassName='mt-1 mb-3'
        />

        <label className='font-bold'>Tên viết tắt</label>
        <Input
          placeholder='Nhập nội dung ghi chú'
          register={register}
          name='shortname'
          errorMessage={errors?.shortname?.message}
          disabled={isLoadingCreate}
          maxLength={256}
          containerClassName='mt-1 mb-3'
        />

        <div className='flex items-center justify-end gap-2'>
          <Button
            variant='secondary'
            onClick={handleClose}
            type='button'
            disabled={isLoadingCreate}
            loading={isLoadingCreate}
          >
            Quay về
          </Button>

          <Button variant='default' disabled={isLoadingCreate} loading={isLoadingCreate}>
            Thêm mới
          </Button>
        </div>
      </form>
    </Modal>
  );
};

export default memo(LanguageModal);
