import { DownloadOutlined } from '@ant-design/icons';
import { Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { Button, Loading, Title } from 'components';
import { useRef, useState } from 'react';
import { bookApis } from 'apis';
import { useMutation } from '@tanstack/react-query';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';
import { path } from 'constants/path';

const Marc21Page = () => {
  const [booksData, setBooksData] = useState<GetImportMarc21Data>({
    CountInsert: 0,
    ListAdded: [],
    CountFail: 0
  });
  const [page, setPage] = useState(1);
  const [paginationSize, setPaginationSize] = useState(10);

  const fileInutRef = useRef<HTMLInputElement | null>(null);

  const navigate = useNavigate();

  const columns: ColumnsType<Pick<Book, 'TenSach' | 'MaKiemSoat'>> = [
    {
      title: 'STT',
      dataIndex: 'stt',
      key: 'stt',
      render: (value, record, j: number) => (page - 1) * paginationSize + j + 1
    },
    {
      title: 'Mã kiểm soát',
      dataIndex: 'MaKiemSoat',
      key: 'MaKiemSoat'
    },
    {
      title: 'Tên sách',
      dataIndex: 'TenSach',
      key: 'TenSach'
    }
  ];

  const { mutate, isLoading } = useMutation({
    mutationFn: (file: File) => {
      let bodyFormData = new FormData();
      bodyFormData.append('filesMarc21', file as Blob);

      return bookApis.importMarc21(bodyFormData);
    },
    onSuccess: (data) => {
      if (data.data.Item.CountInsert === 0) toast.error('Sách đã tồn tại');
      else toast.success('Thêm sách từ Marc21 thành công');
      setBooksData(data.data.Item);
    }
  });

  const handleAccessFileInputRef = () => {
    fileInutRef.current?.click();
  };

  const handleOnFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files?.[0];

    if (!fileFromLocal) return;

    mutate(fileFromLocal);
  };

  const handleComplete = () => navigate(path.sach);

  return (
    <div className='p-5'>
      <Title title='Thêm dữ liệu từ file MARC21' />

      {!booksData.ListAdded.length && (
        <>
          <div className='my-7 flex items-center gap-2'>
            <Button variant='primary-outline' className='text-primary-40' onClick={handleAccessFileInputRef}>
              <DownloadOutlined />
              <span className='font-bold'>Chọn tệp</span>
            </Button>
          </div>

          <input
            name='Marc21'
            type='file'
            accept='.mrc'
            className='hidden'
            ref={fileInutRef}
            onChange={handleOnFileChange}
          />
        </>
      )}

      {!!booksData.ListAdded.length && (
        <>
          <p className=' font-semibold text-green-600'>
            Lưu thành công: <span className='font-normal'>{booksData.CountInsert} sách</span>
          </p>

          <p className=' font-semibold text-danger-10'>
            Lưu thất bại: <span className='font-normal'>{booksData.CountFail} sách</span>
          </p>

          <h3 className='font-bold uppercase text-primary-10'>Danh sách ấn phẩm</h3>
          <Table
            columns={columns}
            dataSource={booksData.ListAdded}
            className='custom-table'
            rowKey={(record) => record?.MaKiemSoat + ''}
            bordered
            pagination={{
              onChange(current, pageSize) {
                setPage(current);
                setPaginationSize(pageSize);
              },
              defaultPageSize: 10,
              hideOnSinglePage: true,
              showSizeChanger: false
            }}
          />
        </>
      )}

      <div className='flex justify-end gap-2'>
        <Button variant='secondary' onClick={handleComplete}>
          Quay về
        </Button>

        <Button onClick={handleComplete}>Hoàn tất</Button>
      </div>

      <Loading open={isLoading} />
    </div>
  );
};

export default Marc21Page;
