import { Tooltip } from 'antd';
import { CSSProperties, InsHTMLAttributes, ReactNode } from 'react';

type Props = {
  firstText?: string | ReactNode;
  secondText?: string | ReactNode;
  thirdText?: string | ReactNode;
  className?: string;
  style?: CSSProperties;
};

const TitleDelete = (props: Props) => {
  const { firstText, secondText = '', thirdText = 'này không?', className = '', style } = props;

  return (
    <div className={'m-0'} style={style}>
      <span style={{ whiteSpace: 'nowrap' }}>{firstText}</span>
      {(secondText !== '' || thirdText !== '') && <br></br>}
      <span className='inline-block text-danger-30' style={{ whiteSpace: 'nowrap' }}>
        {' '}
        {typeof secondText === 'string' ? (
          secondText?.length > 50 ? (
            <Tooltip placement='topLeft' title={secondText} arrow={true}>
              <p className='mb-0 text-center'>{secondText.substring(0, 50).concat('...')}</p>
            </Tooltip>
          ) : (
            <>{secondText}</>
          )
        ) : (
          secondText
        )}{' '}
      </span>{' '}
      {thirdText !== '' && thirdText}
    </div>
  );
};

export default TitleDelete;
