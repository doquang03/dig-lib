import { useQuery } from '@tanstack/react-query';
import { Table, Tooltip } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { bookstoreTypeApis, statistic } from 'apis';
import classNames from 'classnames';
import { Empty, Title } from 'components';
import { useMemo } from 'react';
import { useLocation } from 'react-router-dom';

type Props = {
  data: any;
};
const DocumentStoreBySchoolYear = (props: Props) => {
  const { data } = props;

  const PhanIII = useMemo(() => {
    return data?.data?.Item.PhanIII;
  }, [data?.data?.Item.PhanIII]);

  const List_KhoSach = useMemo(() => {
    if (!data?.data?.Item.List_KhoSach) return [];
    return data?.data?.Item.List_KhoSach;
  }, [data?.data?.Item.List_KhoSach]);
  const ListBoSuuTap = useMemo(() => {
    if (!data?.data?.Item.ListBoSuuTap) return [];
    return data?.data?.Item.ListBoSuuTap;
  }, [data?.data?.Item.ListBoSuuTap]);

  const columns: ColumnsType<any> = [
    {
      key: 'NoiDung',
      dataIndex: 'NoiDung',
      title: 'Thông tin',
      onCell: (_, index) => ({
        className: 'min-content'
      })
    },
    {
      key: 'TongSo',
      dataIndex: 'TongSo',
      title: 'Tổng số',
      children: [
        ...(ListBoSuuTap.map((store: Collection, index: number) => {
          return {
            key: store.Id,
            dataIndex: store.Id,
            title: store.Name,
            render: (value: any, record: any) => {
              if (record && record.List_BoSuuTap && record.List_BoSuuTap.length > 0) {
                const list = record.List_BoSuuTap.filter((_: any) => _.MaTL === store.Id);
                if (list.length > 0) return list[0].SoLuong;
                else return 0;
              } else return 0;
            }
          };
        }) || []),
        { key: 'GiaTien', dataIndex: 'GiaTien', title: 'Giá tiền' }
      ]
    },
    {
      key: 'PhanLoaiTaiLieu',
      dataIndex: 'PhanLoaiTaiLieu',
      title: 'Phân loại tài liệu',
      children: [
        {
          key: 'noiDung',
          dataIndex: 'noiDung',
          title: 'a) Nội Dung',
          children:
            List_KhoSach.map((store: BookStoreType, index: number) => {
              return {
                key: store.Id,
                dataIndex: store.Id,
                title: store.Ten,
                render: (value: any, record: any) => {
                  if (record && record.List_SLTLS && record.List_SLTLS.length > 0) {
                    const list = record.List_SLTLS.filter((_: any) => _.MaTL === store.Id);
                    if (list.length > 0) return list[0].SoLuong;
                    else return 0;
                  } else return 0;
                }
              };
            }) || []
        },
        {
          key: 'ngonNgu',
          dataIndex: 'ngonNgu',
          title: 'b) Ngôn ngữ',
          children: [
            { key: 'VN', dataIndex: 'VN', title: 'Tiếng Việt' },
            { key: 'EN', dataIndex: 'EN', title: 'Tiếng Anh' },
            { key: 'NgonNguKhac', dataIndex: 'NgonNguKhac', title: 'Khác' }
          ]
        }
      ]
    },
    {
      key: 'GhiChu',
      dataIndex: 'GhiChu',
      title: 'Ghi chú',
      render: (value: any, record: any) =>
        record?.GhiChu?.length > 30 ? (
          <Tooltip placement='topLeft' title={record.GhiChu} arrow={true}>
            <p className='text-center'>{record.GhiChu.substring(0, 30).concat('...')}</p>
          </Tooltip>
        ) : (
          <p className='text-center'>{record.GhiChu}</p>
        )
    }
  ];
  return (
    <div>
      <Title title='Phần III. Tình hình kho tài liệu từng năm học' />

      <div className='mt-3'>
        <Table
          locale={{
            emptyText: () => <Empty />
          }}
          columns={columns}
          className='custom-table'
          bordered
          dataSource={PhanIII}
          scroll={{ x: true }}
        />
      </div>
    </div>
  );
};

export default DocumentStoreBySchoolYear;
