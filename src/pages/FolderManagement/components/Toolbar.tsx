import { MagnifyingGlassIcon } from 'assets';
import { Input } from 'components';
import { ChangeEvent, useEffect, useState } from 'react';
import { createSearchParams, useNavigate, useParams } from 'react-router-dom';
import Breadcrumb from './Breadcrumb';
import { debounce } from 'lodash';
import { useQueryClient } from '@tanstack/react-query';
import { useQueryParams } from 'hooks';

export type Menu = {
  id: string;
  label: string;
  type?: FolderType;
  idParent?: string;
  totalBooks?: number;
  isDetermined?: boolean;
  children?: Menu[];
};

type Props = {
  menu: Menu[];
};

export type Route = { key: string; label: string };

function Toolbar(props: Props) {
  const { menu } = props;

  const [routes, setRoutes] = useState<Route[]>([]);

  const queryClient = useQueryClient();

  const navigate = useNavigate();

  const { id } = useParams();

  const { search } = useQueryParams();

  useEffect(() => {
    if (!id) {
      return;
    }

    function magic(arr: Menu[], targetId: string, parent: Route[] = []): Route[] {
      for (const item of arr) {
        if (item.id === targetId) {
          return [
            ...parent,
            {
              key: item.id,
              label: item.label
            }
          ];
        }

        if (item.children) {
          const result = magic(item.children, targetId, [
            ...parent,
            {
              key: item.id,
              label: item.label
            }
          ]);

          if (result.length > 0) {
            return result;
          }
        }
      }

      return [];
    }

    const result = magic(menu, id);

    setRoutes(result);
  }, [id, menu]);

  function handleRefresh() {
    queryClient.invalidateQueries(['getBooksInFolders', id, search]);
  }

  const handleChangeSearch = debounce((e: ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;

    navigate({
      search: createSearchParams({ search: value }).toString()
    });
  }, 500);

  return (
    <div className='my-3 flex w-full flex-col items-center gap-y-1 md:flex-row md:gap-x-2'>
      <Breadcrumb items={routes} onRefresh={handleRefresh} />

      <Input
        type='text'
        placeholder='Nhập tên sách, tác giả, ISBN cần tìm'
        containerClassName='w-1/2 border rounded-lg flex border-[#00000033]'
        className='w-full bg-inherit outline-none '
        left={
          <div className='py-3 px-3'>
            <MagnifyingGlassIcon />
          </div>
        }
        onChange={handleChangeSearch}
      />
    </div>
  );
}

export default Toolbar;
