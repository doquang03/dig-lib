import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/QuanLyThuVien`;
const endPointUpdateVersion = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/UpdateDBVersion`;

const settingApis = {
  Index: (queryString: string = '') => {
    return httpThuVien.post(`/${endPoint}/Index` + queryString);
  },
  Edit: (payload: object) => {
    return httpThuVien.post(`/${endPoint}/Edit`, payload);
  },
  IndexNoiQuy: () => {
    return httpThuVien.get(`/${endPoint}/IndexNoiQuy`);
  },
  IndexGioiThieu: () => {
    return httpThuVien.get(`/${endPoint}/IndexGioiThieu`);
  },
  SetGioiThieu: (html: string) => {
    return httpThuVien.post(`/${endPoint}/SetGioiThieu`, html);
  },
  IndexThoiGianPhucVu: () => {
    return httpThuVien.get(`/${endPoint}/IndexThoiGianPhucVu`);
  },
  SetThoiGianPhucVu: (html: string) => {
    console.log(html);
    return httpThuVien.post(`/${endPoint}/SetThoiGianPhucVu`, html);
  },
  GetBackupFiles: () => {
    return httpThuVien.post(`/${endPoint}/GetBackupFiles`);
  },
  UploadImageLogo: (file: FormData) => {
    return httpThuVien.post(`/${endPoint}/UploadImageLogo`, file, {
      headers: { 'Content-Type': 'multipart/form-data', Accept: '*/*' }
    });
  },
  GetImageData: () => httpThuVien.get<ResponseApi<GetPageCover>>(`/${endPoint}/GetImageData`),
  uploadImagesFile: (payload: FormData) =>
    httpThuVien.post<ResponseApi<GetPageCover>>(`/${endPoint}/UploadImageData`, payload, {
      headers: { 'Content-Type': 'multipart/form-data', Accept: '*/*' }
    }),
  GetImageLogo: () => {
    return httpThuVien.get(`/${endPoint}/GetImageLogo`);
  },
  CreateBackupFile: () => {
    return httpThuVien.post(`/${endPoint}/CreateBackupFile`);
  },
  RestoreBackupFile: (name: string) => {
    return httpThuVien.post(`/${endPoint}/RestoreBackupFile?name=${name}`);
  },
  RemoveBackupFile: (name: string) => {
    return httpThuVien.post(`/${endPoint}/RemoveBackupFile?name=${name}`);
  },
  DownloadBackupFile: (name: string) => {
    return httpThuVien.post(`/${endPoint}/DownloadBackupFile?name=${name}`, {}, { responseType: 'arraybuffer' });
  },
  GetDayBackup: () => {
    return httpThuVien.post(`/${endPoint}/GetDayBackup`);
  },
  UploadBackupFile: (file: FormData) => {
    return httpThuVien.post(`/${endPoint}/UploadBackupFile`, file, {
      headers: { 'Content-Type': 'multipart/form-data', Accept: '*/*' }
    });
  },
  UploadBackupFileOld: (file: FormData) => {
    return httpThuVien.post(`/${endPoint}/RestoreOldBackupFile`, file, {
      headers: { 'Content-Type': 'multipart/form-data', Accept: '*/*' }
    });
  },
  ConvertDataOld: () => {
    return httpThuVien.post(`/${endPointUpdateVersion}/ConvertDataOld`);
  },
  getNumberOfRentingDays: () =>
    httpThuVien.get<
      ResponseApi<{
        NgayMuonSach: number;
        NgayMuonSachGiaoVien: number;
        NgayMuonSachMax: number;
        NgayMuonSachGiaoVienMax: number;
        SLSachDuocMuon: number;
      }>
    >(`/${endPoint}/GetNgayMuonSach`),
  setNumberOfRentingDays: (payload: {
    NgayMuonSach: number;
    NgayMuonSachGiaoVien: number;
    NgayMuonSachMax: number;
    NgayMuonSachGiaoVienMax: number;
  }) => httpThuVien.post<ResponseApi>(`/${endPoint}/SetNgayMuonSach`, payload),
  getStartDate: () =>
    httpThuVien.get<
      ResponseApi<{
        NgayBatDauNamHoc: string;
      }>
    >(`/${endPoint}/GetNgayBatDauNamHoc`),
  getLibName: () =>
    httpThuVien.get<
      ResponseApi<{
        TenThuVien: string;
      }>
    >(`/${endPoint}/GetLibName`)
};

export default settingApis;
