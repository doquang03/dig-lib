import { InfoCircleOutlined } from '@ant-design/icons';
import { Button, Modal, type ModalProps } from 'antd';
import { ReactNode } from 'react';
import 'css/ComponentAntD.css';

type Props = {
  handleCancel?: () => void;
  handleOk?: () => void;
  children?: ReactNode;
  labelOK?: string;
} & ModalProps;

function ModalConfirm(props: Props) {
  const { handleCancel, handleOk, title, children, labelOK = 'Có', ...rest } = props;

  return (
    <Modal
      className='model-delete'
      {...rest}
      footer={[
        <Button className='btn btn-primary btn-sm blue-button' style={{ marginLeft: '10px' }} onClick={handleOk}>
          {labelOK}
        </Button>,
        <Button className='gray-button' onClick={handleCancel}>
          Không
        </Button>
      ]}
    >
      <InfoCircleOutlined className='warning-icon'></InfoCircleOutlined>
      <div className='title-h2'>{title}</div>
      {children}
    </Modal>
  );
}

export default ModalConfirm;
