import { path } from 'constants/path';
import { type ChangeEvent } from 'react';
import { Link } from 'react-router-dom';

type Props = {
  id: string;
  name: string;
  image: string;
  isSelected?: boolean;
  onSelect?: (bookId: string) => void;
};

const BookCard = (props: Props) => {
  const { id, name, image, isSelected, onSelect } = props;

  const handleSelect = (e: ChangeEvent<HTMLInputElement>) => {
    if (!e) return;

    e.stopPropagation();

    onSelect?.(id);
  };

  return (
    <div className='relative'>
      {onSelect && (
        <input
          type='checkbox'
          className='absolute top-0 right-0 z-40 w-[30px]'
          checked={isSelected}
          onChange={handleSelect}
        />
      )}

      <Link to={`${path.pullRequest}/${id}`}>
        <div className='overflow-hidden rounded-md bg-white shadow transition-transform duration-100 hover:translate-y-[-0.04rem] hover:shadow-md'>
          <div className='relative w-full pt-[100%]'>
            <img
              src={image || '/content/Book.png'}
              alt={image}
              className='absolute top-0 left-0 h-full w-full bg-white object-cover'
            />
          </div>

          <div className='relative'>
            <div className='overflow-hidden p-2'>
              <div className='relative min-h-[3rem] text-left text-base line-clamp-2'>
                <span className='text-[14px]'>{name}</span>
              </div>
            </div>
          </div>
        </div>
      </Link>
    </div>
  );
};

export default BookCard;
