import { useMutation, useQuery } from '@tanstack/react-query';
import { Table } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { liquidationApis } from 'apis';
import { BookIcon } from 'assets';
import classNames from 'classnames';
import { Button, Empty, Loading, SizeChanger, Title } from 'components';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import { usePaginationNavigate, useQueryConfig } from 'hooks';
import { useCallback, useMemo, useState } from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getSerialNumber } from 'utils/utils';

const ExportDetailPage = () => {
  const { id } = useParams();
  const queryConfig = useQueryConfig();
  const [TotalCount, setTotalCount] = useState(0);

  const [soXuatKho, setSoXuatKho] = useState<ExportItem>();
  const handleNavigation = usePaginationNavigate();

  const { state } = useLocation();

  const navigate = useNavigate();

  const { isAllowedAdjustment } = useUser();

  const { data, isLoading } = useQuery({
    queryKey: ['receipt', id, queryConfig.page, queryConfig.pageSize],
    queryFn: () =>
      liquidationApis.LayPhieuXuatSachById(id + '', queryConfig.page as string, queryConfig.pageSize as string),
    enabled: Boolean(id)
  });

  const { mutate: downloadExport, isLoading: isLoadingDownloadExport } = useMutation({
    mutationFn: liquidationApis.DownLoadFile,
    onSuccess: (data) => {
      const blob = new Blob([data.data, { type: 'application/xls' }]);
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = 'DanhSachXuatKho.xls';
      link.click();
      toast.success('Tải phiếu xuất thành công');
    }
  });

  const listChiTiet = useMemo(() => {
    if (!data?.data.Item) return [] as ExportDetail[];
    setTotalCount(data?.data.Item.TotalCount);
    setSoXuatKho(data?.data.Item.soXuatKho);

    return data?.data.Item?.ListChiTietXuatKho;
  }, [data?.data.Item]);
  const columns: ColumnsType<ExportDetail> = [
    {
      title: 'STT',
      dataIndex: 'STT',
      key: 'stt',
      render: (value, record, index) => {
        return (
          queryConfig.page && queryConfig.pageSize && getSerialNumber(+queryConfig.page, +queryConfig.pageSize, index)
        );
      }
    },
    {
      title: 'Mã kiểm soát',
      dataIndex: 'MaKiemSoat',
      key: 'MaKiemSoat'
    },
    {
      title: 'Số đăng ký cá biệt',
      dataIndex: 'MaDKCB',
      key: 'MaDKCB'
    },
    {
      title: 'Tên sách',
      dataIndex: 'TenSach',
      key: 'TenSach'
    },
    {
      title: 'Tình trạng',
      dataIndex: 'TrangThai',
      key: 'TrangThai'
    },
    {
      title: 'Giá tiền',
      dataIndex: 'GiaTien',
      key: 'GiaTien',
      render: (value, record) => (
        <>
          {record.GiaTien && record.GiaTien !== '' && record.GiaTien !== 'null'
            ? new Intl.NumberFormat('vi-VN', {
                style: 'currency',
                currency: 'VND',
                maximumFractionDigits: 9
              }).format(Number(record.GiaTien))
            : ''}
        </>
      )
    }
  ];
  const handlePintExport = () => () => navigate(path.inPhieuXuat, { state: id });

  const handleDownloadExport = () => {
    !!id && downloadExport(id + '');
  };

  const onPaginate = useCallback(
    (page: number, pageSize: number) => {
      handleNavigation({ ...queryConfig, page: page + '', pageSize: pageSize + '' });
    },
    [handleNavigation, queryConfig]
  );

  const pagination = useMemo(() => {
    return {
      current: (!!queryConfig.page && +queryConfig.page) || 1,
      pageSize: (!!queryConfig.pageSize && +queryConfig.pageSize) || 30,
      total: TotalCount,
      onChange: onPaginate,
      hideOnSinglePage: true,
      showSizeChanger: false,
      showQuickJumper: true,
      locale: { jump_to: '', page: '' }
    };
  }, [TotalCount, onPaginate, queryConfig.page, queryConfig.pageSize]);

  const handleBack = () => navigate(path.quanlyxuatkho);

  return (
    <div className='p-5'>
      <Title title='Chi tiết phiếu xuất' />

      <div className='mt-3 flex w-1/2 items-center gap-2'>
        <BookIcon />{' '}
        <span className='font-bold'>
          Ghi chú: {soXuatKho?.GhiChu || state?.ghiChu || 'Không có ghi chú cho phiếu xuất kho này'}
        </span>
      </div>

      <div className='my-4'>
        <Table
          columns={columns}
          dataSource={listChiTiet || []}
          className='custom-table'
          pagination={pagination}
          rowKey={(record) => record.IdChiTiet + ''}
          loading={isLoading}
          locale={{
            emptyText: () => <Empty />
          }}
        />
        <div
          className={classNames('relative', {
            // @ts-ignore
            'mt-[64px]': (queryConfig.page && +queryConfig.pageSize) >= TotalCount
          })}
        >
          <div className='absolute bottom-1'>
            <SizeChanger visible={!!listChiTiet?.length} value={queryConfig.pageSize} total={TotalCount.toString()} />
          </div>
        </div>
        {!!listChiTiet.length && (
          <div className='mt-2 flex items-center justify-between'>
            <p className='font-bold text-primary-10'>Tổng số sách xuất kho: {listChiTiet.length} quyển sách</p>
            <p className='font-bold text-primary-10'>
              Tổng giá tiền:{' '}
              {new Intl.NumberFormat('vi-VN', {
                style: 'currency',
                currency: 'VND',
                maximumFractionDigits: 9
              }).format(
                Number(
                  listChiTiet.reduce(
                    (total, book) => (total += Number(book.GiaTien && book.GiaTien !== 'null' ? book.GiaTien : 0)),
                    0
                  )
                )
              )}{' '}
            </p>
          </div>
        )}
      </div>

      <div className='flex items-center justify-end gap-2'>
        <Button variant='secondary' onClick={handleBack}>
          Quay về
        </Button>

        {isAllowedAdjustment && (
          <>
            <Button onClick={handlePintExport}>In phiếu xuất kho</Button>

            <Button onClick={handleDownloadExport}>Tải phiếu xuất kho</Button>
          </>
        )}
      </div>

      <Loading open={isLoading || isLoadingDownloadExport} />
    </div>
  );
};

export default ExportDetailPage;
