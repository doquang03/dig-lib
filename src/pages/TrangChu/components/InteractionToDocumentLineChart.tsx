import { Select } from 'components';
import { ChangeEvent } from 'react';
import { Line } from 'react-chartjs-2';

type Props = {
  data: {
    labels: string[];
    datasets: {
      label: string;
      data: (number | undefined)[];
      borderColor: string;
      backgroundColor: string;
    }[];
  };
  onChange: (e: ChangeEvent<HTMLSelectElement>) => void;
};

function InteractionToDocumentLineChart(props: Props) {
  const { data, onChange } = props;

  return (
    <div className='my-3 rounded-xl bg-white p-2 shadow-md'>
      <div className='flex items-center justify-between'>
        <h3 className='text-xl font-bold text-primary-10'>THEO DÕI LƯỢT TƯƠNG TÁC VỚI TÀI LIỆU</h3>

        <Select
          items={[
            { label: 'Thư viện nội bộ', value: '1' },
            {
              label: 'Bạn đọc vãng lai',
              value: '2'
            }
          ]}
          onChange={onChange}
        />
      </div>

      <Line
        data={data}
        options={{
          scales: {
            x: { offset: false },
            y: {
              beginAtZero: true
            }
          },
          responsive: true,
          maintainAspectRatio: true,
          plugins: {
            legend: {
              title: { padding: 100 },
              position: 'bottom' as const
            },
            title: {
              display: false
            },
            datalabels: {
              display: true,
              align: 'center',
              anchor: 'center'
            }
          }
        }}
      />
    </div>
  );
}

export default InteractionToDocumentLineChart;
