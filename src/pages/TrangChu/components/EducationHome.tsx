import { useQuery } from '@tanstack/react-query';
import { DatePicker } from 'antd';
import { authApis } from 'apis';
import statisticApis from 'apis/statistic.apis';
import { ArcElement, Chart as ChartJS, Legend, Tooltip } from 'chart.js';
import { Select, Title } from 'components';
import { useUser } from 'contexts/user.context';
import dayjs from 'dayjs';
import { useMemo, useState } from 'react';
import { Doughnut } from 'react-chartjs-2';
import BarChart from './BarChart';
import CharacterBox from './CharacterBox';

ChartJS.register(ArcElement, Tooltip, Legend);

const { RangePicker } = DatePicker;

type RangeValue = Parameters<NonNullable<React.ComponentProps<typeof DatePicker.RangePicker>['onChange']>>[0];

const EducationHome = () => {
  const [searchParams, setSearchParams] = useState<{
    schoolLevel: string;
    TuNgay: string;
    DenNgay: string;
  }>({
    schoolLevel: '1_TruongCap1',
    TuNgay: dayjs().startOf('year').startOf('day').toISOString(),
    DenNgay: dayjs().endOf('day').toISOString()
  });

  const { profile } = useUser();

  const { data } = useQuery({
    queryKey: ['subdomains'],
    queryFn: () => authApis.ListChildActivated(profile?.IdDVCT_Working + ''),
    enabled: Boolean(profile?.IdDVCT_Working)
  });

  const databaseNames = useMemo(() => {
    return (
      data?.data?.Item.filter((item: { LoaiTruong: string | string[] }) =>
        item.LoaiTruong.includes(searchParams.schoolLevel)
      ) || []
    );
  }, [data?.data?.Item, searchParams.schoolLevel]);

  const { data: dashboardData } = useQuery({
    queryKey: ['getDashboard', searchParams],
    queryFn: () =>
      statisticApis.getDashboarData({
        databaseNames: databaseNames.map((x: any) => x?.DatabaseName),
        TuNgay: searchParams.TuNgay,
        DenNgay: searchParams.DenNgay
      }),
    enabled: !!databaseNames.length
  });

  const { TongHocSinh, TongGiaoVien, TongTaiLieuGiay, TongTaiLieuSo, listModel } = dashboardData?.data?.Item || {
    TongHocSinh: 0,
    TongGiaoVien: 0,
    TongTaiLieuGiay: 0,
    TongTaiLieuSo: 0,
    listModel: []
  };

  const topTenOfHighest = (
    option: keyof {
      TongLuotMuon: number;
      TongTaiLieuSo: number;
    }
  ) => {
    return listModel
      .sort((a, b) => b[option] - a[option])
      .slice(0, 10)
      .map((item, index) => ({
        label: databaseNames?.[index]?.Ten,
        value: item[option]
      }));
  };

  const pieData = useMemo(() => {
    if (TongTaiLieuGiay === 0 && TongTaiLieuSo === 0)
      return {
        labels: ['Tài liệu giấy', 'Tài liệu số'],

        datasets: [
          {
            data: [0, 0],
            backgroundColor: ['#A23434', '#3472A2']
          }
        ]
      };

    return {
      labels: ['Tài liệu giấy', 'Tài liệu số'],
      datasets: [
        {
          data: [TongTaiLieuGiay, TongTaiLieuSo],
          backgroundColor: ['#A23434', '#3472A2']
        }
      ]
    };
  }, [TongTaiLieuGiay, TongTaiLieuSo]);

  function handleChangeRangePicker(values: RangeValue) {
    if (!values) return;

    setSearchParams((prevState) => ({
      ...prevState,
      TuNgay: values?.[0]?.startOf('day').toISOString() as string,
      DenNgay: values?.[1]?.endOf('day').toISOString() as string
    }));
  }

  function handleChangeSchoolLevel(value: string) {
    setSearchParams((prevState) => ({ ...prevState, schoolLevel: value }));
  }

  return (
    <div className='p-5'>
      <Title title='DASHBOARD' />

      {/* <div className='mt-3'>
        <Tabs items={items} activeKey={activeTab} onChange={handleChangeTab} destroyInactiveTabPane={true} />
      </div> */}

      <div className='my-3 flex items-center justify-end gap-3'>
        <button>
          <svg width={16} height={16} viewBox='0 0 16 16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path
              d='M13.64 2.35C12.19 0.9 10.2 0 7.99 0C3.57 0 0 3.58 0 8C0 12.42 3.57 16 7.99 16C11.72 16 14.83 13.45 15.72 10H13.64C12.82 12.33 10.6 14 7.99 14C4.68 14 1.99 11.31 1.99 8C1.99 4.69 4.68 2 7.99 2C9.65 2 11.13 2.69 12.21 3.78L8.99 7H15.99V0L13.64 2.35Z'
              fill='#3472A2'
            />
          </svg>
        </button>

        <p>Trường cấp: </p>

        <Select
          items={[
            // { label: 'Cấp mần non', value: '0_MamnonMaugiao' },
            { label: 'Cấp TH', value: '1_TruongCap1' },
            { label: 'Cấp THCS', value: '2_TruongCap2' },
            { label: 'Cấp THPT', value: '3_TruongCap3' }
            // { label: 'Cấp Đại học', value: '4' }
          ]}
          value={searchParams.schoolLevel}
          onChange={(e) => handleChangeSchoolLevel(e.target.value)}
          className='w-1/12'
        />

        <RangePicker
          className='w-1/4 border border-gray-300 py-[0.8rem] px-2 outline-none'
          format={['DD/MM/YYYY', 'DD-MM-YYYY']}
          defaultValue={[dayjs().startOf('year'), dayjs()]}
          placeholder={['Ngày bắt đầu', 'Ngày kết thúc']}
          disabledDate={(current) => {
            return current && current > dayjs().endOf('day');
          }}
          onChange={handleChangeRangePicker}
        />

        {/* <YearSelection /> */}
      </div>

      <div className='grid grid-cols-1 gap-6 2xl:grid-cols-12'>
        <div className='col-span-12 flex flex-col gap-y-3 2xl:col-span-6'>
          <div className='rounded-md bg-white p-2'>
            <p className='text-base font-bold text-primary-10'>
              Xin Chào, <span className='text-[#A23434]'>{profile?.FullName} !</span>{' '}
            </p>
          </div>

          <div className='flex items-center gap-x-2'>
            <CharacterBox character='library' value={listModel?.length || 0} />

            <CharacterBox character='student' value={TongHocSinh || '--'} />

            <CharacterBox character='teacher' value={TongGiaoVien || '--'} />
          </div>

          {/* <BarChart title='TOP 10 ĐƠN VỊ CÓ SỐ LƯỢNG TÀI LIỆU SỐ CAO NHẤT' data={mockData} /> */}
        </div>

        <div className='col-span-12 flex flex-col gap-y-3 2xl:col-span-6'>
          <div className='flex flex-wrap items-center gap-2 md:mt-0 md:flex-nowrap'>
            <div className='h-[213px] w-full rounded-md bg-white p-2 shadow-md'>
              <h3 className='truncate font-bold uppercase text-primary-10'>BIỂU ĐỒ TỈ LỆ TÀI LIỆU</h3>

              <div className='flex h-[213px] items-center justify-center'>
                <Doughnut
                  className='mb-11'
                  data={pieData}
                  options={{
                    plugins: {
                      datalabels: {
                        color: 'white'
                      },
                      legend: {
                        display: true,
                        position: 'right',
                        labels: {
                          pointStyle: 'circle',
                          borderRadius: 50,
                          useBorderRadius: true,
                          usePointStyle: true,
                          color: 'white'
                        }
                      }
                    },
                    maintainAspectRatio: false,
                    responsive: true
                  }}
                />
              </div>
            </div>

            <div className='h-[213px] w-full rounded-md bg-white p-2 shadow-md'>
              <h3 className='truncate font-bold uppercase text-primary-10'>BIỂU ĐỒ ĐÁNH GIÁ THƯ VIỆN</h3>

              <div className='flex h-[150px] items-center justify-center'>
                {/* <Doughnut
                  className='mb-11'
                  data={pieData}
                  options={{
                    plugins: {
                      legend: {
                        display: true,
                        position: 'right',
                        labels: {
                          pointStyle: 'circle',
                          borderRadius: 50,
                          useBorderRadius: true,
                          usePointStyle: true
                        }
                      }
                    },
                    maintainAspectRatio: false,
                    responsive: true
                  }}
                /> */}
                <p className='font-bold'>Đang phát triển</p>
              </div>
            </div>
          </div>

          {/* <BarChart title='TOP 10 ĐƠN VỊ CÓ SỐ LƯỢT LƯU THÔNG CAO NHẤT' data={mockData} /> */}
        </div>
      </div>

      <div className='my-3 grid grid-cols-1 gap-3 lg:grid-cols-12'>
        <div className='col-span-12 lg:col-span-6'>
          <BarChart title='TOP 10 ĐƠN VỊ CÓ SỐ LƯỢNG TÀI LIỆU SỐ CAO NHẤT' data={topTenOfHighest('TongTaiLieuSo')} />
        </div>

        <div className='col-span-12 lg:col-span-6'>
          <BarChart title='TOP 10 ĐƠN VỊ CÓ SỐ LƯỢT LƯU THÔNG CAO NHẤT' data={topTenOfHighest('TongLuotMuon')} />
        </div>
      </div>
    </div>
  );
};

export default EducationHome;
