import { useQuery } from '@tanstack/react-query';
import { Tooltip } from 'antd';
import { colorApis } from 'apis';
import classNames from 'classnames';
import { Button, SelectSearchForm, Title } from 'components';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import { useQueryConfig } from 'hooks';
import { uniqueId } from 'lodash';
import { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';

const ColorManagementContent = () => {
  const navigate = useNavigate();
  const [colors, setColors] = useState<Color[]>([]);

  const handleAddColor = () => navigate(path.themMaMau);

  const queryConfig = useQueryConfig();

  const { isAllowedAdjustment } = useUser();

  const { isRefetching } = useQuery({
    queryKey: ['colors', queryConfig],
    queryFn: () =>
      colorApis.getColors({
        Keyword: queryConfig.TextForSearch || '',
        Sort: (queryConfig?.OrderBy && +queryConfig?.OrderBy) || 0
      }),
    onSuccess: (data) => {
      setColors(data.data.Item);
    }
  });

  return (
    <div className='p-5'>
      <Title title='Danh sách mã màu' />

      <Button
        className={classNames('', {
          hidden: !isAllowedAdjustment,
          'my-2': isAllowedAdjustment
        })}
        variant='default'
        onClick={handleAddColor}
      >
        Thêm mã màu
      </Button>

      <SelectSearchForm
        placeholder='Nhập tên màu, tên kho'
        items={[
          { label: 'Sắp xếp theo', value: '' },
          { label: 'Tên màu giảm dần', value: 2 },
          { label: 'Tên màu tăng dần', value: 1 }
        ]}
      />

      <div className='mt-4 grid grid-cols-12'>
        {!!colors?.length && (
          <>
            {!!colors.slice(0, colors.length / 2).length && (
              <div className='col-span-6'>
                {colors.slice(0, colors.length / 2).map((color) => {
                  const width = 90 / color.ListColor.length;

                  return (
                    <div key={color.Id} className='my-2'>
                      <Link to={`CapNhat/${color.Id}`} className='flex items-center'>
                        {color.ListColor.map((x) => {
                          return (
                            <div
                              key={uniqueId()}
                              className='shadow-lg'
                              style={{ backgroundColor: x.Name, width: width, height: 45 }}
                            />
                          );
                        })}
                        <div className='ml-2'>
                          {color.TenMau} - {color.TenKieuLienKet} -{' '}
                          {color.TenLienKet.length > 40 ? (
                            <Tooltip placement='topLeft' title={color.TenLienKet} arrow={true}>
                              {color.TenLienKet.substring(0, 40).concat('...')}
                            </Tooltip>
                          ) : (
                            color.TenLienKet
                          )}
                        </div>
                      </Link>
                    </div>
                  );
                })}
              </div>
            )}

            <div className='col-span-6'>
              {colors.slice(colors.length / 2, colors.length).map((color) => {
                const width = 90 / color.ListColor.length;

                return (
                  <div key={color.Id} className='my-2'>
                    <Link to={`CapNhat/${color.Id}`} className='flex items-center'>
                      {color.ListColor.map((x) => {
                        return (
                          <div
                            key={uniqueId()}
                            className='shadow-lg'
                            style={{ backgroundColor: x.Name, width: width, height: 45 }}
                          />
                        );
                      })}
                      <div className='ml-2'>
                        {color.TenMau} - {color.TenKieuLienKet} -{' '}
                        {color.TenLienKet.length > 40 ? (
                          <Tooltip placement='topLeft' title={color.TenLienKet} arrow={true}>
                            {color.TenLienKet.substring(0, 40).concat('...')}
                          </Tooltip>
                        ) : (
                          color.TenLienKet
                        )}
                      </div>
                    </Link>
                  </div>
                );
              })}
            </div>
          </>
        )}
      </div>

      {!colors?.length && (
        <div className='mt-2 text-center'>{isRefetching ? 'Đang tải mã màu' : 'Không tìm thấy mã màu '}</div>
      )}

      <h3 className='font-semibold uppercase text-primary-10'>Bảng chú thích mã màu</h3>

      <table className='w-full overflow-x-auto'>
        <thead>
          <tr>
            <th className='border border-black p-2 text-center uppercase'>trình độ đọc</th>
            <th className='border border-black p-2 text-center uppercase'>Độ phức tạp câu từ</th>
            <th className='border border-black p-2 text-center uppercase'>Số câu/thang</th>
            <th className='border border-black p-2 text-center uppercase'>Số từ/câu*</th>
            <th className='border border-black p-2 text-center uppercase'>Nội dung</th>
            <th className='border border-black p-2 text-center uppercase'>Hình ảnh/thanh minh họa</th>
          </tr>
        </thead>

        <tbody>
          <tr className=' border-black bg-green-600'>
            <td className='border border-black px-2 py-7'>Xanh Lá</td>
            <td className='p-2'>Có thể là từ đơn, từ ghép, nhóm từ hoặc câu ngắn theo cấu trúc đơn giản.</td>
            <td className='border border-black p-2'>0-2 câu/trang</td>
            <td className='border border-black p-2'>0-7 từ/câu</td>
            <td className='border border-black p-2'>
              Khái niệm đơn giản; Không nhất thiết phải có cốt truyện cụ thể. Có thể chỉ là một chuỗi tranh về những sự
              vật hay hành động quen thuộc đối với trẻ.
            </td>
            <td className='border border-black p-2'>
              Ít nhất 90% diện tích trang có tranh, hình ảnh minh họa; Tranh/hình ảnh minh họa phục vụ cho nội dung
              chính; Đơn giản, rõ ràng.
            </td>
          </tr>

          <tr className=' bg-red-600'>
            <td className='border border-black px-2 py-7'>Đỏ</td>
            <td className='border border-black p-2'>Câu đơn giản và hoàn chỉnh.</td>
            <td className='border border-black p-2'>1-3 câu/trang</td>
            <td className='border border-black p-2'>1-10 từ/câu</td>
            <td className='border border-black p-2'>Khái niệm đơn giản; Các tình huống quen thuộc.</td>
            <td className='border border-black p-2'>
              Ít nhất 70% diện tích trang có tranh/hình ảnh minh họa; Tranh/hình ảnh minh họa phục vụ cho nội dung
              chính.
            </td>
          </tr>

          <tr className=' bg-orange-600'>
            <td className='border border-black px-2 py-7'>Cam</td>
            <td className='border border-black p-2'> Câu đơn giản, dài hơn (có thành phần phụ, trạng ngữ).</td>
            <td className='border border-black p-2'>2-5 câu/trang</td>
            <td className='border border-black p-2'>3-12 từ/câu</td>
            <td className='border border-black p-2'>
              Những khái niệm quen thuộc hoặc những khái niêm mới, giúp học sinh mở rộng hiểu biết; Chuỗi các sự kiện/sự
              việc đơn giản. Cốt truyện đơn giản.
            </td>
            <td className='border border-black p-2'>
              Ít nhất 60% trang có tranh/hình ảnh minh họa; Tranh/hình ảnh minh họa phục vụ cho nội dung chính nhưng có
              thể trừu tượng hơn hai trình độ trước.
            </td>
          </tr>

          <tr className='bg-white'>
            <td className='border border-black px-2 py-7'>Trắng</td>
            <td className='border border-black p-2'>
              Từ khó hơn, sử dụng các từ ghép, từ láy, các từ có ý nghĩa trừu tượng; Cấu trúc câu phức tạp hơn, có thể
              sử dụng câu ghép.
            </td>
            <td className='border border-black p-2'>3-8 câu/trang</td>
            <td className='border border-black p-2'>6-10 từ/câu</td>
            <td className='border border-black p-2'>
              Nội dung được mở rộng ra nhiều chủ đề khác nhau; Có thể giới thiệu những khái niệm trừu tượng.
            </td>
            <td className='border border-black p-2'>50% diện tích của trang nên có hình ảnh/tranh minh họa.</td>
          </tr>

          <tr className=' bg-blue-500'>
            <td className='border border-black px-2 py-7'>Xanh dương</td>
            <td className='border border-black p-2'>
              Có thể sử dụng một số từ tượng hình; Sử dụng nhiều câu ghép và có phân đoạn.
            </td>
            <td className='border border-black p-2'>8-15 câu/trang</td>
            <td className='border border-black p-2'>7-15 từ/câu</td>
            <td className='border border-black p-2'>
              Mạch truyện có thể phức tạp hơn, nhiều sự kiện, hoặc một sự kiện tiếp diễn; Có thể có khái niệm và ý tưởng
              trừu tượng.
            </td>
            <td className='border border-black p-2'>Không nhất thiết mỗi trang có hình ảnh minh họa.</td>
          </tr>

          <tr className=' bg-yellow-300'>
            <td className='border border-black px-2 py-7'>Vàng</td>
            <td className='border border-black p-2'>
              Có nhiều kiểu từ loại khác nhau, cấu trúc, dấu câu, ngôn ngữ phức tạp, nhưng phù hợp với học sinh.
            </td>
            <td className='border border-black p-2'>Một trang có thể toàn là chữ.</td>
            <td className='border border-black p-2'>Không giới hạn.</td>
            <td className='border border-black p-2'>Không giới hạn nhưng phù hợp với học sinh.</td>
            <td className='border border-black p-2'>Ít hình ảnh/tranh minh họa, có thể không có.</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default ColorManagementContent;
