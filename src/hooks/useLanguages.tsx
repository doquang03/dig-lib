import { useQuery } from '@tanstack/react-query';
import { languageApis } from 'apis';
import { useMemo } from 'react';

const useLanguages = () => {
  const {
    data: languageData,
    isLoading: loadingLanguageData,
    ...rest
  } = useQuery({
    queryKey: ['getAllLanguages'],
    queryFn: languageApis.getAll,
    staleTime: Infinity
  });

  const language = useMemo(() => {
    if (!languageData?.data.Item.length) return [];

    return languageData?.data?.Item;
  }, [languageData?.data?.Item]);

  return {
    data: language,
    isLoading: loadingLanguageData,
    ...rest
  };
};

export default useLanguages;
