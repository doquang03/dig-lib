import { useMutation, useQuery } from '@tanstack/react-query';
import { DatePicker, DatePickerProps } from 'antd';
import locale from 'antd/es/date-picker/locale/vi_VN';
import { bookReservingApis } from 'apis';
import {
  CategoryScale,
  Chart as ChartJS,
  Legend,
  LineElement,
  LinearScale,
  PointElement,
  Title,
  Tooltip
} from 'chart.js';
import { Button, Select } from 'components';
import { FORMAT_DATE_PICKER } from 'constants/config';
import { TYPE_READER_OPTIONS } from 'constants/options';
import 'css/Statistical.css';
import dayjs, { Dayjs } from 'dayjs';
import weekOfYear from 'dayjs/plugin/weekOfYear';
import { ChangeEvent, useCallback, useMemo, useState } from 'react';
import { Line, Pie } from 'react-chartjs-2';
import { useNavigate } from 'react-router-dom';
import { OverallReview, download } from 'utils/utils';

ChartJS.register(CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend);

dayjs.extend(weekOfYear);

type Props = {
  type: 'week' | 'month' | 'year';
};

const valueStartOfWeek = (value: Dayjs) => dayjs(value).startOf('week').format(FORMAT_DATE_PICKER[5]);
const valueEndOfWeek = (value: Dayjs) => dayjs(value).endOf('week').format(FORMAT_DATE_PICKER[0]);

const customWeekStartEndFormat: DatePickerProps['format'] = (value) =>
  `Tuần ${dayjs(value).week()} - từ  ${valueStartOfWeek(value)} ~ ${valueEndOfWeek(value)}`;

const Chart = (props: Props) => {
  const { type } = props;

  const [params, setParams] = useState<{
    userType: '' | 'gv' | 'hs';
    date: string;
    month?: number;
    year?: number;
  }>({ userType: '', date: dayjs().format('DD/MM/YYYY'), month: dayjs().month() + 1, year: dayjs().year() });

  const navigate = useNavigate();

  const { data: dataByWeek, isLoading: loadingByWeek } = useQuery({
    queryKey: ['getDataByWeek', type, params],
    queryFn: () =>
      bookReservingApis.getDataByWeek({
        typeAccount: params.userType,
        dateTime: params.date
      }),
    enabled: type === 'week'
  });

  const { data: dataByMonthYear, isLoading: loadingByMonthYear } = useQuery({
    queryKey: ['getDataByMonth', type, params],
    queryFn: () => {
      const req: Partial<{ month: number; year: number }> = {};

      if (type === 'month') {
        req.month = params.month;
        req.year = params.year;
      }

      if (type === 'year') {
        delete params.month;
        req.year = params.year;
      }

      return bookReservingApis.getDataByMonthAndYear({
        typeAccount: params.userType,
        ...req
      });
    },
    enabled: type === 'month' || type === 'year'
  });

  const { mutate: exportReportByWeek, isLoading: loadingExportReportByWeek } = useMutation({
    mutationFn: () => {
      return bookReservingApis.exportByWeek({
        typeAccount: params.userType,
        dateTime: params.date
      });
    },
    onSuccess: (data) => {
      download(`Thống kê lịch sử đặt mượn sách online_${params.date}.xls`, data.data);
    }
  });

  const { mutate: exportReportByMonth, isLoading: loadingExportReport } = useMutation({
    mutationFn: () => {
      const req: Partial<{ month: number; year: number }> = {};

      if (type === 'month') {
        req.month = params.month;
        req.year = params.year;
      }

      if (type === 'year') {
        delete params.month;
        req.year = params.year;
      }

      return bookReservingApis.exportByMonthYear({
        typeAccount: params.userType,
        ...req
      });
    },
    onSuccess: (data) => {
      let fileName = '';
      if (type === 'month') {
        fileName = `Thống kê lịch sử đặt mượn sách online_${params.month}/${params.year}.xls`;
      }

      if (type === 'year') {
        fileName = `Thống kê lịch sử đặt mượn sách online_${params.year}.xls`;
      }

      download(fileName, data.data);
    }
  });

  const label = useMemo(() => {
    let sumUnit = '';
    switch (type) {
      case 'month':
        sumUnit = 'tháng';
        break;
      case 'year':
        sumUnit = 'năm';
        break;
      case 'week':
        sumUnit = 'tuần';
        break;
    }
    return sumUnit;
  }, [type]);

  const resultByType = useMemo(() => {
    let working: number[] = [];
    let expired: number[] = [];
    let success: number[] = [];
    let cancel: number[] = [];
    let date: string[] = [];
    let total: number[] = [];

    if (type === 'week') {
      const { listDates, soDangDatMuonTrongTuan, soHetHieuLucTrongTuan, soHoanTatTrongTuan, soHuyTrongTuan } =
        dataByWeek?.data.Item || {
          listDates: [],
          soDangDatMuonTrongTuan: [],
          soHetHieuLucTrongTuan: [],
          soHoanTatTrongTuan: [],
          soHuyTrongTuan: []
        };

      for (let index = 0; index < listDates.length; index++) {
        const workingElm = soDangDatMuonTrongTuan[index];
        const expiredElm = soHetHieuLucTrongTuan[index];
        const successElm = soHoanTatTrongTuan[index];
        const cancelElm = soHuyTrongTuan[index];
        const _total = workingElm + expiredElm + successElm + cancelElm;
        total.push(_total);
      }
      working = soDangDatMuonTrongTuan;
      expired = soHetHieuLucTrongTuan;
      success = soHoanTatTrongTuan;
      cancel = soHuyTrongTuan;
      date = listDates.map((item) => dayjs(item).format('DD/MM/YYYY'));
    }

    if (type === 'month') {
      const { soDangDatMuonTrongThang, soHetHieuLucTrongThang, soHoanTatTrongThang, soHuyTrongThang } = dataByMonthYear
        ?.data.Item || {
        soDangDatMuonTrongThang: [],
        soHetHieuLucTrongThang: [],
        soHoanTatTrongThang: [],
        soHuyTrongThang: []
      };

      working = [];
      expired = [];
      success = [];
      cancel = [];
      const daysInMonth = dayjs(new Date(params.year as number, params.month as number, 0)).daysInMonth();
      for (let index = 1; index <= daysInMonth; index++) {
        const workingElm = soDangDatMuonTrongThang[index];
        const expiredElm = soHetHieuLucTrongThang[index];
        const successElm = soHoanTatTrongThang[index];
        const cancelElm = soHuyTrongThang[index];
        const _total = workingElm + expiredElm + successElm + cancelElm;
        working[index - 1] = workingElm;
        expired[index - 1] = expiredElm;
        success[index - 1] = successElm;
        cancel[index - 1] = cancelElm;
        total.push(_total);
        date.push(index + '');
      }
    }

    if (type === 'year') {
      const { soDangDatMuonTrongNam, soHetHieuLucTrongNam, soHoanTatTrongNam, soHuyTrongNam } = dataByMonthYear?.data
        .Item || {
        soDangDatMuonTrongNam: [],
        soHetHieuLucTrongNam: [],
        soHoanTatTrongNam: [],
        soHuyTrongNam: []
      };

      working = [];
      expired = [];
      success = [];
      cancel = [];

      for (let index = 1; index <= 12; index++) {
        const workingElm = soDangDatMuonTrongNam[index];
        const expiredElm = soHetHieuLucTrongNam[index];
        const successElm = soHoanTatTrongNam[index];
        const cancelElm = soHuyTrongNam[index];
        const _total = workingElm + expiredElm + successElm + cancelElm;
        working[index - 1] = workingElm;
        expired[index - 1] = expiredElm;
        success[index - 1] = successElm;
        cancel[index - 1] = cancelElm;
        total.push(_total);
        date.push(index + '');
      }
    }

    const totalPercent = total.reduce((total, item) => (total += item), 0);

    return { working, expired, success, cancel, date, total, totalPercent };
  }, [dataByMonthYear?.data.Item, dataByWeek?.data.Item, type]);

  const data1 = {
    labels: resultByType.date,
    datasets: [
      {
        label: 'Tổng số lượt đặt mượn',
        data: resultByType.total,
        borderColor: '#AB02D6',
        backgroundColor: '#AB02D6'
      },
      {
        label: 'Tổng số đang đặt mượn',
        data: resultByType.working,
        borderColor: '#3472A2',
        backgroundColor: '#3472A2'
      },
      {
        label: 'Số lượt đặt mượn hết hiệu lực',
        data: resultByType.expired,
        borderColor: '#A7A7A7',
        backgroundColor: '#A7A7A7'
      },
      {
        label: 'Số lượt đặt mượn thành công',
        data: resultByType.success,
        borderColor: '#119757',
        backgroundColor: '#119757'
      },
      {
        label: 'Số lượt hủy đặt mượn',
        data: resultByType.cancel,
        borderColor: '#E14E09',
        backgroundColor: '#E14E09'
      }
    ]
  };

  let max = 0;
  let maxValue = resultByType.working.reduce((total, item) => (total += item), 0);

  if (maxValue < resultByType.expired.reduce((total, item) => (total += item), 0)) {
    max = 1;
    maxValue = resultByType.expired.reduce((total, item) => (total += item), 0);
  }

  if (maxValue < resultByType.success.reduce((total, item) => (total += item), 0)) {
    max = 2;
    maxValue = resultByType.success.reduce((total, item) => (total += item), 0);
  }

  if (maxValue < resultByType.cancel.reduce((total, item) => (total += item), 0)) {
    max = 3;
    maxValue = resultByType.cancel.reduce((total, item) => (total += item), 0);
  }

  let workingPercent =
    max !== 0
      ? Math.round((resultByType.working.reduce((total, item) => (total += item), 0) / resultByType.totalPercent) * 100)
      : 0;

  let expiredPercent =
    max !== 1
      ? Math.round((resultByType.expired.reduce((total, item) => (total += item), 0) / resultByType.totalPercent) * 100)
      : 0;

  let successPercent =
    max !== 2
      ? Math.round((resultByType.success.reduce((total, item) => (total += item), 0) / resultByType.totalPercent) * 100)
      : 0;

  let canceledPercent =
    max !== 3
      ? Math.round((resultByType.cancel.reduce((total, item) => (total += item), 0) / resultByType.totalPercent) * 100)
      : 0;

  const totalPercent = workingPercent + expiredPercent + successPercent + canceledPercent;

  switch (max) {
    case 0:
      workingPercent = 100 - totalPercent;
      break;

    case 1:
      expiredPercent = 100 - totalPercent;
      break;

    case 2:
      successPercent = 100 - totalPercent;
      break;

    case 3:
      canceledPercent = 100 - totalPercent;
      break;

    default:
      break;
  }

  const data = {
    labels: ['Đang đặt mượn', 'Hết hiệu lực', 'Đặt mượn thành công', 'Hủy'],
    datasets: [
      {
        data: [workingPercent, expiredPercent, successPercent, canceledPercent],
        backgroundColor: ['#3472A2', '#A7A7A7', '#119757', '#E14E09'],
        borderWidth: 1
      }
    ]
  };

  const handleChangeTypeReader = (e: ChangeEvent<HTMLSelectElement>) => {
    if (!e) return;

    const { value } = e.target;

    setParams((prevState) => {
      return {
        ...prevState,
        userType: value as '' | 'gv' | 'hs'
      };
    });
  };

  const formatDatepicker = useCallback(
    (value: Dayjs) => {
      switch (type) {
        case 'week':
          return customWeekStartEndFormat(value);

        case 'month':
          return `Tháng ${dayjs(value).format('MM/YYYY')}`;

        case 'year':
          return `Năm ${dayjs(value).year()}`;

        default:
          return 'DD/MM/YYYY';
      }
    },
    [type]
  );

  const handleChangeDate = (value: Dayjs | null) => {
    if (!value) return;

    switch (type) {
      case 'week':
        setParams((prevState) => {
          return {
            ...prevState,
            date: dayjs(value).format('DD/MM/YYYY')
          };
        });
        break;

      case 'month':
        setParams((prevState) => {
          return {
            ...prevState,
            month: dayjs(value).month() + 1,
            year: dayjs(value).year()
          };
        });
        break;

      case 'year':
        setParams((prevState) => {
          return {
            ...prevState,
            year: dayjs(value).year()
          };
        });
        break;

      default:
        break;
    }
  };

  const handleExportReport = () => {
    switch (type) {
      case 'week':
        exportReportByWeek();
        break;

      case 'year':
      case 'month':
        exportReportByMonth();
        break;

      default:
        break;
    }
  };

  return (
    <div>
      <div className='absolute -top-10 right-0 ml-auto flex items-center gap-1'>
        <Button
          variant='secondary'
          className='ml-auto'
          onClick={() => navigate(-1)}
          loading={loadingExportReportByWeek || loadingExportReport}
        >
          Quay về
        </Button>

        <Button
          className='ml-auto'
          onClick={handleExportReport}
          loading={loadingExportReportByWeek || loadingExportReport}
        >
          Xuất báo cáo
        </Button>
      </div>

      <div className='grid grid-cols-12'>
        <div className='col-span-full col-start-6 mt-2 flex w-full flex-1 items-center justify-end gap-2'>
          <Select items={TYPE_READER_OPTIONS} onChange={handleChangeTypeReader} className='flex-grow' />

          <DatePicker
            picker={type}
            locale={locale}
            format={formatDatepicker}
            clearIcon={false}
            className='w-1/2 py-3'
            defaultValue={dayjs()}
            onChange={handleChangeDate}
          />
        </div>
      </div>

      <div className='mt-2 grid grid-cols-12 gap-4'>
        <div className='col-span-8 bg-white px-3 pb-3 shadow-lg'>
          <h3 className='font-bold text-primary-10 underline '>Thống kê dữ liệu đặt mượn</h3>

          <table cellPadding={3} cellSpacing={1} className='shadow-lg' width={'100%'}>
            <thead className='bg-primary-10'>
              <tr className='h-7 text-center font-semibold text-white'>
                <td>Tổng số lượt mượn</td>
                <td>Đang đặt mượn</td>
                <td>Hết hiệu lực</td>
                <td>Đặt mượn thành công</td>
                <td>Hủy</td>
              </tr>
            </thead>

            <tbody className='text-center'>
              <tr>
                <td>{resultByType.total.reduce((total, item) => (total += item), 0) || 0}</td>
                <td>{resultByType.working.reduce((total, item) => (total += item), 0) || 0}</td>
                <td>{resultByType.expired.reduce((total, item) => (total += item), 0) || 0}</td>
                <td>{resultByType.success.reduce((total, item) => (total += item), 0) || 0}</td>
                <td>{resultByType.cancel.reduce((total, item) => (total += item), 0) || 0}</td>
              </tr>
            </tbody>
          </table>

          <h3 className='font-bold text-primary-10 underline'>Đánh giá tổng quan</h3>

          <div className='mb-3 flex items-center gap-1'>
            <svg width='20' height='20' viewBox='0 0 20 20' fill='none' xmlns='http://www.w3.org/2000/svg'>
              <path
                d='M14.59 5.58L8 12.17L4.41 8.59L3 10L8 15L16 7L14.59 5.58ZM10 0C4.48 0 0 4.48 0 10C0 15.52 4.48 20 10 20C15.52 20 20 15.52 20 10C20 4.48 15.52 0 10 0ZM10 18C5.58 18 2 14.42 2 10C2 5.58 5.58 2 10 2C14.42 2 18 5.58 18 10C18 14.42 14.42 18 10 18Z'
                fill='#119757'
              />
            </svg>
            {OverallReview(resultByType.total, 2, type) === '' ? (
              <p className='text-[#AB02D6]'>
                <span className='text-normal text-black'> Không có</span> tổng số lượt đặt mượn{' '}
                <span className='text-normal text-black'> trong {label}</span>
              </p>
            ) : (
              <p className='text-[#AB02D6]'>
                Tổng số lượt đặt mượn{' '}
                <span className='text-normal text-black'> {OverallReview(resultByType.total, 2, type)}</span>{' '}
              </p>
            )}
          </div>

          <div className='mb-3 flex items-center gap-1'>
            <svg width='20' height='20' viewBox='0 0 20 20' fill='none' xmlns='http://www.w3.org/2000/svg'>
              <path
                d='M14.59 5.58L8 12.17L4.41 8.59L3 10L8 15L16 7L14.59 5.58ZM10 0C4.48 0 0 4.48 0 10C0 15.52 4.48 20 10 20C15.52 20 20 15.52 20 10C20 4.48 15.52 0 10 0ZM10 18C5.58 18 2 14.42 2 10C2 5.58 5.58 2 10 2C14.42 2 18 5.58 18 10C18 14.42 14.42 18 10 18Z'
                fill='#119757'
              />
            </svg>
            {OverallReview(resultByType.working, 2, type) === '' ? (
              <p className='text-[#3472A2]'>
                <span className='text-normal text-black'> Không có</span> số đang đặt mượn{' '}
                <span className='text-normal text-black'> trong {label}</span>
              </p>
            ) : (
              <p className='text-[#3472A2]'>
                Tổng số đang đặt mượn{' '}
                <span className='text-normal text-black'> {OverallReview(resultByType.working, 2, type)}</span>{' '}
              </p>
            )}
          </div>

          <div className='mb-3 flex items-center gap-1'>
            <svg width='20' height='20' viewBox='0 0 20 20' fill='none' xmlns='http://www.w3.org/2000/svg'>
              <path
                d='M14.59 5.58L8 12.17L4.41 8.59L3 10L8 15L16 7L14.59 5.58ZM10 0C4.48 0 0 4.48 0 10C0 15.52 4.48 20 10 20C15.52 20 20 15.52 20 10C20 4.48 15.52 0 10 0ZM10 18C5.58 18 2 14.42 2 10C2 5.58 5.58 2 10 2C14.42 2 18 5.58 18 10C18 14.42 14.42 18 10 18Z'
                fill='#119757'
              />
            </svg>
            {OverallReview(resultByType.expired, 2, type) === '' ? (
              <p className='text-[#A7A7A7]'>
                <span className='text-normal text-black'> Không có</span> số lượt đặt mượn hết hiệu lực{' '}
                <span className='text-normal text-black'> trong {label}</span>
              </p>
            ) : (
              <p className='text-[#A7A7A7]'>
                Số lượt đặt mượn hết hiệu lực{' '}
                <span className='text-normal text-black'> {OverallReview(resultByType.expired, 2, type)}</span>{' '}
              </p>
            )}
          </div>

          <div className='mb-3 flex items-center gap-1'>
            <svg width='20' height='20' viewBox='0 0 20 20' fill='none' xmlns='http://www.w3.org/2000/svg'>
              <path
                d='M14.59 5.58L8 12.17L4.41 8.59L3 10L8 15L16 7L14.59 5.58ZM10 0C4.48 0 0 4.48 0 10C0 15.52 4.48 20 10 20C15.52 20 20 15.52 20 10C20 4.48 15.52 0 10 0ZM10 18C5.58 18 2 14.42 2 10C2 5.58 5.58 2 10 2C14.42 2 18 5.58 18 10C18 14.42 14.42 18 10 18Z'
                fill='#119757'
              />
            </svg>
            {OverallReview(resultByType.success, 2, type) === '' ? (
              <p className='text-[#119757]'>
                <span className='text-normal text-black'> Không có</span> số lượt đặt mượn thành công{' '}
                <span className='text-normal text-black'> trong {label}</span>
              </p>
            ) : (
              <p className='text-[#119757]'>
                Số lượt đặt mượn thành công{' '}
                <span className='text-normal text-black'> {OverallReview(resultByType.success, 2, type)}</span>{' '}
              </p>
            )}
          </div>

          <div className='mb-3 flex items-center gap-1'>
            <svg width='20' height='20' viewBox='0 0 20 20' fill='none' xmlns='http://www.w3.org/2000/svg'>
              <path
                d='M14.59 5.58L8 12.17L4.41 8.59L3 10L8 15L16 7L14.59 5.58ZM10 0C4.48 0 0 4.48 0 10C0 15.52 4.48 20 10 20C15.52 20 20 15.52 20 10C20 4.48 15.52 0 10 0ZM10 18C5.58 18 2 14.42 2 10C2 5.58 5.58 2 10 2C14.42 2 18 5.58 18 10C18 14.42 14.42 18 10 18Z'
                fill='#119757'
              />
            </svg>
            {OverallReview(resultByType.cancel, 2, type) === '' ? (
              <p className='text-[#FF1616]'>
                <span className='text-normal text-black'> Không có</span> số lượt hủy đặt mượn{' '}
                <span className='text-normal text-black'> trong {label}</span>
              </p>
            ) : (
              <p className='text-[#FF1616]'>
                Số lượt hủy đặt mượn{' '}
                <span className='text-normal text-black'> {OverallReview(resultByType.cancel, 2, type)}</span>{' '}
              </p>
            )}
          </div>

          {!!resultByType.totalPercent && (
            <div className='flex items-center gap-1'>
              <svg width='14' height='14' viewBox='0 0 14 14' fill='none' xmlns='http://www.w3.org/2000/svg'>
                <path
                  d='M0 6.125H10.6488L5.7575 1.23375L7 0L14 7L7 14L5.76625 12.7663L10.6488 7.875H0V6.125Z'
                  fill='#3472A2'
                />
              </svg>

              <p className='font-bold uppercase text-primary-10'>{OverallReview(resultByType.total, 2, type)}</p>
            </div>
          )}
        </div>

        <div className='col-span-4 bg-white px-2 shadow-lg'>
          <h3 className='my-2 font-bold text-primary-10 underline'>Tỉ lệ đặt mượn theo trạng thái</h3>

          <div className='grid max-h-[250px] place-items-center'>
            {(loadingByWeek && type === 'week') || (loadingByMonthYear && (type === 'month' || type === 'year')) ? (
              <div>Đang tải dữ liệu</div>
            ) : !resultByType.totalPercent ? (
              <div className='grid h-[250px] place-items-center'>Không có dữ liệu</div>
            ) : (
              <Pie
                data={data}
                options={{
                  responsive: true,
                  plugins: {
                    datalabels: {
                      display: '%',
                      formatter: function (value: number, context) {
                        return value > 10 ? `${value}%` : null;
                      },
                      color: 'black',
                      anchor: 'center',
                      align: 'center',
                      clamp: false,
                      font: {
                        size: 16
                      }
                    },
                    legend: {
                      display: false
                    },
                    title: {
                      display: false
                    }
                  }
                }}
              />
            )}
          </div>
        </div>
      </div>

      <h3 className='font-bold text-primary-10 underline'>Biểu đồ thể hiện tỉ lệ đặt mượn online</h3>

      {resultByType.totalPercent === 0 ? (
        <div className='text-center'>Không có dữ liệu</div>
      ) : (
        <div style={{ width: '100%', height: '450px' }}>
          <Line
            data={data1}
            options={{
              scales: {
                x: { offset: true },
                y: {
                  beginAtZero: true
                }
              },
              responsive: true,
              maintainAspectRatio: false,
              plugins: {
                legend: {
                  title: { padding: 100 },
                  position: 'top' as const
                },
                title: {
                  display: false
                },
                datalabels: {
                  display: true,
                  align: 'center',
                  anchor: 'center',
                  formatter: function (value: number, context) {
                    switch (type) {
                      case 'month':
                        return null;

                      case 'year':
                      case 'week':
                        return value;

                      default:
                        break;
                    }
                  }
                }
              }
            }}
          />
        </div>
      )}
    </div>
  );
};

export default Chart;
