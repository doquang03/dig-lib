import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { day19Apis } from 'apis';
import { Button, Input, Loading, ModalDelete, Title, TitleDelete } from 'components';
import { path } from 'constants/path';
import { useEffect, useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { day19Schema } from 'utils/rules';

const initalFormValues: Omit<Day19, 'Id'> = {
  MaDay19: '',
  Ten: ''
};

const AddDay19Page = () => {
  const { id } = useParams();

  const [visible, setVisible] = useState<boolean>(false);
  const [selectedDay19, setSelectedDay19] = useState<Day19>();

  const navigate = useNavigate();

  const queryClient = useQueryClient();

  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors }
  } = useForm<typeof initalFormValues>({
    defaultValues: initalFormValues,
    resolver: yupResolver(day19Schema)
  });

  const { data: day19Data, refetch } = useQuery({
    queryKey: ['day19', id],
    queryFn: () => day19Apis.getDay19(id + ''),
    enabled: !!id,
    onError: () => {
      navigate(path.trangThaiSach);
    }
  });

  const day19 = useMemo(() => {
    if (!day19Data?.data.Item) {
      return;
    }

    return day19Data.data.Item;
  }, [day19Data?.data.Item]);

  const { mutate: createDay19, isLoading: loadingCreateDay19 } = useMutation({
    mutationFn: day19Apis.createDay19,
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ['day19s'] });
      toast.success('Tạo phân loại 19 dãy thành công');
      navigate(path.day19);
    }
  });

  const { mutate: updateDay19, isLoading: loadingUpdateDay19 } = useMutation({
    mutationFn: day19Apis.updateDay19,
    onSuccess: () => {
      refetch();
      toast.success('Cập nhật phân loại 19 dãy thành công');
    }
  });

  const { mutate: deleteDay19, isLoading: loadingDeleteDay19 } = useMutation({
    mutationFn: day19Apis.deleteDay19,
    onSuccess: () => {
      toast.success('Xóa phân loại 19 dãy thành công');
      navigate(path.day19);
    }
  });

  useEffect(() => {
    if (Boolean(id) && day19) {
      day19.Ten && setValue('Ten', day19.Ten);
      day19.MaDay19 && setValue('MaDay19', day19.MaDay19);
    }
  }, [id, setValue, day19]);

  const onSubmit = handleSubmit((data) => {
    let input = data.Ten.replace(/\s+/g, ' ');
    let code = data?.MaDay19?.replace(/\s+/g, ' ');

    if (!Boolean(id)) {
      !loadingCreateDay19 && createDay19({ ...data, Ten: input, MaDay19: code });
    } else {
      !loadingUpdateDay19 && updateDay19({ ...data, Id: id + '', Ten: input, MaDay19: code });
    }
  });

  const handleVisibleModal = () => setSelectedDay19(day19);

  const handleDelete = () => {
    deleteDay19(id + '');
  };

  const handleBack = () => navigate(path.day19);

  return (
    <div className='p-5'>
      <Title title={Boolean(id) ? 'Cập nhật 19 Dãy' : 'Thêm mới 19 Dãy'} />

      <form className='w-1/2 px-3' onSubmit={onSubmit}>
        <label className='mt-3 font-bold'>
          Tên 19 dãy <span className='text-danger-10'>*</span>
        </label>
        <Input
          placeholder='Nhập tên 19 dãy'
          name='Ten'
          register={register}
          errorMessage={errors?.Ten?.message}
          maxLength={256}
        />

        <label className='mt-3 font-bold'>
          Mã 19 dãy <span className='text-danger-10'>*</span>
        </label>
        <Input
          placeholder='Nhập mã 19 dãy'
          name='MaDay19'
          register={register}
          errorMessage={errors?.MaDay19?.message}
          maxLength={256}
        />

        <div className='mt-4 flex items-center justify-end gap-2'>
          <Button variant='secondary' onClick={handleBack} type='button'>
            Quay về
          </Button>

          {Boolean(id) ? (
            <>
              <Button variant='danger' onClick={handleVisibleModal} type='button'>
                Xóa
              </Button>

              <Button variant='default' type='submit'>
                Cập nhật
              </Button>
            </>
          ) : (
            <Button variant='default' type='submit'>
              Thêm mới
            </Button>
          )}
        </div>
      </form>

      <ModalDelete
        open={Boolean(selectedDay19)}
        closable={false}
        title={<TitleDelete firstText='Bạn có chắc chắn muốn xóa phân loại 19 dãy' secondText={selectedDay19?.Ten} />}
        handleCancel={() => setSelectedDay19(undefined)}
        handleOk={() => {
          handleDelete();
        }}
      />

      <Loading open={loadingCreateDay19 || loadingUpdateDay19 || loadingDeleteDay19} />
    </div>
  );
};

export default AddDay19Page;
