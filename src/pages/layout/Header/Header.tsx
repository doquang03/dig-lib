import {
  BarChartOutlined,
  BookOutlined,
  CaretDownFilled,
  CheckCircleFilled,
  ClockCircleFilled,
  ClockCircleOutlined,
  ContactsOutlined,
  HomeFilled,
  LeftCircleFilled,
  LogoutOutlined,
  PlusCircleFilled,
  RightCircleFilled,
  SettingFilled,
  TeamOutlined,
  UnorderedListOutlined,
  WarningFilled
} from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Divider, Dropdown, Layout, Menu, theme } from 'antd';
import { Footer } from 'antd/es/layout/layout';
import { statistic } from 'apis';
import { CoverBookIcon, DigitalIcon } from 'assets';
import { IDENTITY_CONFIG, SizeCollapse } from 'constants/config';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import 'css/LayoutApp.css';
import { MenuProps } from 'rc-menu';
import React, { useState } from 'react';
import { useCookies } from 'react-cookie';
import { useAuth } from 'react-oidc-context';
import { Link, useNavigate } from 'react-router-dom';
import { LocalStorageEventTartget } from 'utils/auth';
import { getUnitFromSS } from 'utils/utils';
const { Header: HeaderAntd } = Layout;

const Header = () => {
  const navigate = useNavigate();

  const { profile, userType, isAllowedAdjustment, reset } = useUser();

  const { mutateAsync: setDbName, isLoading: loadingSetDbName } = useMutation({
    mutationFn: statistic.setDatabaseName
  });

  const { data: dataCount, isLoading: isLoadingDataCount } = useQuery({
    queryFn: statistic.GetDataCount,
    queryKey: ['GetDataCount'],
    enabled: userType === 3000,
    cacheTime: Infinity
  });

  const auth = useAuth();

  const unit = getUnitFromSS(Number(userType));

  const [tabsCookies, setTabsCookies, removeCookie] = useCookies();

  const removeUnit = async () => {
    const unit = getUnitFromSS(Number(userType));
    try {
      await setDbName({
        idUser: '',
        DatabaseName: '',
        EnableEdit: false,
        tenDonVi: unit?.Ten || ''
      });

      const clearLSEvent = new Event('clearSS');
      LocalStorageEventTartget.dispatchEvent(clearLSEvent);

      sessionStorage.removeItem(`${userType}`);
    } catch (error) {
      console.warn('error', error);
    }
  };

  const handleSignout = async () => {
    auth.stopSilentRenew();

    removeCookie(profile?.Id || '');
    removeCookie('activeKey');

    const clearLSEvent = new Event('clearSS');
    LocalStorageEventTartget.dispatchEvent(clearLSEvent);

    sessionStorage.removeItem(`${userType}`);
    reset?.();

    await auth.clearStaleState();
    await auth.signoutSilent({ post_logout_redirect_uri: IDENTITY_CONFIG.post_logout_redirect_uri });
    await auth.removeUser();
  };

  const itemUsers: MenuProps['items'] = [
    {
      key: 'profile',
      label: (
        <a target='_blank' rel='noopener noreferrer' href={process.env.REACT_APP_ACCOUNT_INFO}>
          Thông tin cá nhân
        </a>
      )
    },
    {
      key: 'help',
      label: <Link to='/HoTro'>Trợ giúp</Link>
    },
    {
      key: 'divider-account',
      label: <Divider className='my-1' />
    },
    {
      key: 'signout',
      label: (
        <div className='w-full' onClick={handleSignout}>
          Đăng xuất
        </div>
      )
    }
  ];
  return (
    <HeaderAntd>
      <div className='flex items-center justify-between'>
        <div>
          {userType === 2001 && Boolean(unit) && (
            <button className='flex w-full items-center gap-1 font-bold text-white' onClick={removeUnit}>
              <LogoutOutlined />
              {unit?.Ten}
            </button>
          )}

          {userType === 3000 && (
            <div className='flex items-center gap-1'>
              <a className='headerShortcut' href={path.thongkedanhsachdatmuon} title='Yêu cầu mượn Online'>
                <PlusCircleFilled className='icon' />
                {isLoadingDataCount ? '--' : dataCount?.data?.Item.DangDatMuon || 0}
              </a>
              <button
                className='headerShortcut'
                onClick={() => {
                  navigate(path.thongkemuonsach, {
                    state: { TinhTrang: 1 }
                  });
                }}
                title='Danh sách chưa trả'
              >
                <CheckCircleFilled className='icon' />
                {isLoadingDataCount ? '--' : dataCount?.data?.Item.DangMuon || 0}
              </button>
              <button
                className='headerShortcut'
                onClick={() => {
                  navigate(path.thongkemuonsach, {
                    state: { TinhTrang: 2 }
                  });
                }}
                title='Danh sách gần trả'
              >
                <ClockCircleFilled className='icon' />
                {isLoadingDataCount ? '--' : dataCount?.data?.Item.DangMuonDenHanTra || 0}
              </button>
              <button
                className='headerShortcut'
                onClick={() => {
                  navigate(path.thongkemuonsach, {
                    state: { TinhTrang: 3 }
                  });
                }}
                title='Danh sách trả trễ'
              >
                <WarningFilled className='icon' />
                {isLoadingDataCount ? '--' : dataCount?.data?.Item.DangMuonTreHan || 0}
              </button>
            </div>
          )}
        </div>

        {!!profile?.Id && (
          <div className='flex cursor-pointer items-center gap-2'>
            <Dropdown menu={{ items: itemUsers }} trigger={['click']}>
              <div className='flex items-center gap-1'>
                <img src='/content/icon_user.jpg' alt='avatar' className='h-[40px] w-[40px]' />
                <span className='UserNameTitle'>{profile?.UserName}</span>

                <CaretDownFilled style={{ color: 'white' }} />
              </div>
            </Dropdown>
          </div>
        )}
      </div>
    </HeaderAntd>
  );
};

export default Header;
