import classNames from 'classnames';
import { ModalDelete } from 'components';
import { InfoCircleOutlined } from '@ant-design/icons';

type Props = {
  array?: NewspaperMagazine[];
  onCancel: () => void;
  onOk: () => void;
  loading: boolean;
  visible: boolean;
};

const DeleteMulti = (props: Props) => {
  const { array, onCancel, onOk, loading, visible } = props;

  return (
    <ModalDelete
      open={visible}
      closable={false}
      title={
        <>
          <h3 className='text-bold'>Bạn có muốn chắc chắn xóa những báo - tạp chí này không?</h3>
          <p className='text-[16px] font-extralight leading-normal'>
            Bạn sẽ không thể khôi phục sau khi xóa, những báo - tạp chí đang được đăng ký sẽ không thể xóa.
          </p>
        </>
      }
      handleCancel={onCancel}
      handleOk={onOk}
      loading={loading}
    >
      <div className='max-h-[300px] overflow-x-auto'>
        {array?.map(({ MaBaoTapChi, Ten, IsUsed }, index) => {
          const inactive = IsUsed;

          return (
            <div key={MaBaoTapChi} className='grid grid-cols-12 items-center border-b-2 px-2 py-2 font-bold'>
              <div className='col-span-1'>
                <p className={inactive ? 'text-danger-10' : ''}>{index + 1} </p>
              </div>
              <div className='col-span-5'>
                <p
                  className={classNames('truncate text-left', {
                    'text-danger-10': inactive
                  })}
                >
                  {Ten}
                </p>
              </div>
              <div className='col-span-6 grid grid-cols-12'>
                <div className='col-span-6'>
                  <p
                    className={classNames('mr-4', {
                      'text-danger-10': inactive
                    })}
                  >
                    {MaBaoTapChi}
                  </p>
                </div>
                <div className='col-span-6'>
                  {inactive && (
                    <p className='flex items-center gap-1 font-light text-danger-10'>
                      <InfoCircleOutlined style={{ color: 'red' }} />

                      <span className='text-[12px]'>Đang đăng ký</span>
                    </p>
                  )}
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </ModalDelete>
  );
};

export default DeleteMulti;
