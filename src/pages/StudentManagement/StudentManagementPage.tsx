import { path } from 'constants/path';
import { Outlet, useMatch } from 'react-router-dom';
import StudentManagermentContent from './StudentManagementContent/StudentManagermentContent';

const StudentManagementPage = () => {
  const match = useMatch(path.hocsinh);

  return <>{Boolean(match) ? <StudentManagermentContent /> : <Outlet />}</>;
};

export default StudentManagementPage;
