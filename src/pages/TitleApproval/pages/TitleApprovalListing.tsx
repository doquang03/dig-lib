import { useQuery } from '@tanstack/react-query';
import { TabsProps } from 'antd';
import { approvalApis, connectionApis } from 'apis';
import { Tabs, Title } from 'components';
import { useUser } from 'contexts/user.context';
import { NotFoundPage } from 'pages';
import { useState } from 'react';
import { ApprovedListing, WaitForApproval } from '../components';

const TitleApprovalListing = () => {
  const [key, setKey] = useState<'waitForApproval' | 'approvedListing'>('waitForApproval');

  const { userType, permission } = useUser();

  const { data: listOfNotApproved } = useQuery({
    queryKey: ['getCountApprovals'],
    queryFn: () => approvalApis.getListOfApprovals({ page: 0, pageSize: 0, IsComplete: false }),
    refetchOnWindowFocus: true
  });

  const { data: listOFApprovedsData } = useQuery({
    queryKey: ['getCountAppred'],
    queryFn: () => approvalApis.getListOfApprovals({ page: 0, pageSize: 0, IsComplete: true }),
    refetchOnWindowFocus: true
  });

  if (userType === 3000 && !permission?.Enable) {
    return <NotFoundPage />;
  }

  const items: TabsProps['items'] = [
    {
      key: 'waitForApproval',
      label: `Chờ duyệt (${listOfNotApproved?.data.Item.Count || 0})`,
      children: <WaitForApproval />,
      forceRender: true
    },
    {
      key: 'approvedListing',
      label: `Đã duyệt (${listOFApprovedsData?.data.Item.Count || 0})`,
      children: <ApprovedListing />,
      forceRender: true
    }
  ];

  return (
    <div className='p-5'>
      <Title title='QUẢN LÝ BIỂU GHI BIÊN MỤC' />

      <Tabs
        items={items}
        activeKey={key}
        onChange={(key) => setKey(key as 'waitForApproval' | 'approvedListing')}
        className='mt-3'
      />
    </div>
  );
};

export default TitleApprovalListing;
