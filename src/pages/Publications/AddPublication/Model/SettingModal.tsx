import { Checkbox, Modal, ModalProps } from 'antd';
import { Button, Title } from 'components';
import { useState } from 'react';

type Props = {
  arrAvaliable: Array<boolean>;
  setVisibleModal: Function;
  setArrAvaliable: Function;
  setCookie: Function;
} & ModalProps;

const SettingModal = (props: Props) => {
  const { arrAvaliable, setArrAvaliable, setVisibleModal, setCookie, open } = props;
  const [clone, setClone] = useState({ ...arrAvaliable });
  return (
    <Modal
      className='model-choses-library-monitor'
      open={open}
      closable={false}
      footer={[
        <Button
          className='gray-button'
          onClick={() => {
            setVisibleModal(false);
          }}
        >
          Quay về
        </Button>,
        <Button
          className='btn btn-primary btn-sm blue-button'
          style={{ marginLeft: '10px' }}
          onClick={() => {
            setCookie('settings_add_book', clone, {
              path: '/'
            });
            setArrAvaliable(clone);
            setVisibleModal(false);
          }}
        >
          Cập nhật
        </Button>
      ]}
    >
      <Title title={'CẤU HÌNH HIỂN THỊ'} />
      <div className='mt-8 grid grid-cols-1 md:grid-cols-12'>
        <div className='custom-row col-span-6 flex flex-col px-2'>
          <Checkbox
            className='checkbox-modal'
            checked={clone[0]}
            onChange={() => {
              let temp = { ...clone };
              temp[0] = !temp[0];
              setClone(temp);
            }}
          >
            Tên song ngữ
          </Checkbox>
          <Checkbox
            className='checkbox-modal'
            checked={clone[1]}
            onChange={() => {
              let temp = { ...clone };
              temp[1] = !temp[1];
              setClone(temp);
            }}
          >
            Tác giả phụ
          </Checkbox>
          <Checkbox
            className='checkbox-modal'
            checked={clone[2]}
            onChange={() => {
              let temp = { ...clone };
              temp[2] = !temp[2];
              setClone(temp);
            }}
          >
            Môn học
          </Checkbox>
          <Checkbox
            className='checkbox-modal'
            checked={clone[3]}
            onChange={() => {
              let temp = { ...clone };
              temp[3] = !temp[3];
              setClone(temp);
            }}
          >
            Khối lớp
          </Checkbox>
          <Checkbox
            className='checkbox-modal'
            checked={clone[4]}
            onChange={() => {
              let temp = { ...clone };
              temp[4] = !temp[4];
              setClone(temp);
            }}
          >
            Chủ điểm
          </Checkbox>
          <Checkbox
            className='checkbox-modal'
            checked={clone[5]}
            onChange={() => {
              let temp = { ...clone };
              temp[5] = !temp[5];
              setClone(temp);
            }}
          >
            Kệ sách
          </Checkbox>
          <Checkbox
            className='checkbox-modal'
            checked={clone[6]}
            onChange={() => {
              let temp = { ...clone };
              temp[6] = !temp[6];
              setClone(temp);
            }}
          >
            Người biên dịch
          </Checkbox>
          <Checkbox
            className='checkbox-modal'
            checked={clone[7]}
            onChange={() => {
              let temp = { ...clone };
              temp[7] = !temp[7];
              setClone(temp);
            }}
          >
            Lần xuất bản
          </Checkbox>
          <Checkbox
            className='checkbox-modal'
            checked={clone[8]}
            onChange={() => {
              let temp = { ...clone };
              temp[8] = !temp[8];
              setClone(temp);
            }}
          >
            Phí mượn
          </Checkbox>
          <Checkbox
            className='checkbox-modal'
            checked={clone[9]}
            onChange={() => {
              let temp = { ...clone };
              temp[9] = !temp[9];
              setClone(temp);
            }}
          >
            Minh họa
          </Checkbox>
        </div>
        <div className='custom-row col-span-6 flex flex-col px-2'>
          <Checkbox
            className='checkbox-modal'
            checked={clone[10]}
            onChange={() => {
              let temp = { ...clone };
              temp[10] = !temp[10];
              setClone(temp);
            }}
          >
            Mô tả vật lý
          </Checkbox>
          <Checkbox
            className='checkbox-modal'
            checked={clone[11]}
            onChange={() => {
              let temp = { ...clone };
              temp[11] = !temp[11];
              setClone(temp);
            }}
          >
            Khổ sách
          </Checkbox>
          <Checkbox
            className='checkbox-modal'
            checked={clone[12]}
            onChange={() => {
              let temp = { ...clone };
              temp[12] = !temp[12];
              setClone(temp);
            }}
          >
            Tài liệu kèm theo
          </Checkbox>
          <Checkbox
            className='checkbox-modal'
            checked={clone[13]}
            onChange={() => {
              let temp = { ...clone };
              temp[13] = !temp[13];
              setClone(temp);
            }}
          >
            ISBN/ISSN
          </Checkbox>
          <Checkbox
            className='checkbox-modal'
            checked={clone[14]}
            onChange={() => {
              let temp = { ...clone };
              temp[14] = !temp[14];
              setClone(temp);
            }}
          >
            SKU
          </Checkbox>
          <Checkbox
            className='checkbox-modal'
            checked={clone[15]}
            onChange={() => {
              let temp = { ...clone };
              temp[15] = !temp[15];
              setClone(temp);
            }}
          >
            DDC/Dãy 19
          </Checkbox>
          <Checkbox
            className='checkbox-modal'
            checked={clone[16]}
            onChange={() => {
              let temp = { ...clone };
              temp[16] = !temp[16];
              setClone(temp);
            }}
          >
            LLC
          </Checkbox>
          <Checkbox
            className='checkbox-modal'
            checked={clone[17]}
            onChange={() => {
              let temp = { ...clone };
              temp[17] = !temp[17];
              setClone(temp);
            }}
          >
            Mã màu
          </Checkbox>
          <Checkbox
            className='checkbox-modal'
            checked={clone[18]}
            onChange={() => {
              let temp = { ...clone };
              temp[18] = !temp[18];
              setClone(temp);
            }}
          >
            Tóm tắt
          </Checkbox>
          <Checkbox
            className='checkbox-modal'
            checked={clone[19]}
            onChange={() => {
              let temp = { ...clone };
              temp[19] = !temp[19];
              setClone(temp);
            }}
          >
            Tùng thư
          </Checkbox>
        </div>
      </div>
    </Modal>
  );
};

export default SettingModal;
