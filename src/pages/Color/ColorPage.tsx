import { Outlet } from 'react-router-dom';

const ColorPage = () => {
  return <Outlet />;
};

export default ColorPage;
