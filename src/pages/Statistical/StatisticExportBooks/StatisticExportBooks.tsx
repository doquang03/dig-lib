import { useMutation, useQuery } from '@tanstack/react-query';
import { DatePicker, Table, Tooltip } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { bookStoreApis, settingApis, statistic } from 'apis';
import classNames from 'classnames';
import { Button, Empty, Select, SizeChanger, Title } from 'components';
import { useUser } from 'contexts/user.context';
import dayjs from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';
import { uniqueId } from 'lodash';
import { ChangeEvent, useMemo, useState } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getSerialNumber } from 'utils/utils';

dayjs.extend(customParseFormat);

const { RangePicker } = DatePicker;

type RangeValue = Parameters<NonNullable<React.ComponentProps<typeof DatePicker.RangePicker>['onChange']>>[0];

const StatisticExportBooks = () => {
  const [pageSize, setPageSize] = useState<number>(30);
  const [page, setPage] = useState<number>(1);
  const [searchParams, setSearchParams] = useState<{
    idKho?: string;
    TuNgay: string;
    DenNgay: string;
    sort: string;
    page: number;
    pageSize: number;
  }>({
    TuNgay: '',
    DenNgay: dayjs().endOf('day').toISOString(),
    page: 1,
    pageSize: 30,
    sort: ''
  });

  const { isAllowedAdjustment } = useUser();

  const { isLoading: loadingStartDateData } = useQuery({
    queryKey: ['startDate'],
    queryFn: settingApis.getStartDate,
    onSuccess: (data) => {
      const date = data.data.Item.NgayBatDauNamHoc.split('/');

      const dateResult = `${date[1]}/${date[0]}/${new Date().getFullYear()}`;

      const isAfterToday = dayjs(dateResult).isAfter(dayjs());

      if (isAfterToday) {
        setSearchParams((prevState) => {
          return {
            ...prevState,
            TuNgay: dayjs(dateResult).subtract(1, 'year').toISOString()
          };
        });
      } else {
        setSearchParams((prevState) => {
          return {
            ...prevState,
            TuNgay: dayjs(dateResult).toISOString()
          };
        });
      }
    }
  });

  const { data: bookstoreData, isLoading: loadingBookstore } = useQuery({
    queryKey: ['bookstoreOptions'],
    queryFn: () => bookStoreApis.getBookstoreOptions()
  });

  const { data: thongKeThanhLyData, isLoading: loadingThongKeThanhLy } = useQuery({
    queryKey: ['thongKeThanhly', searchParams, page, pageSize],
    queryFn: () => statistic.getThongKeThanhLy(searchParams),
    enabled: Boolean(searchParams.TuNgay)
  });

  const { mutate: downloadExcel, isLoading: loadingDownloadExcel } = useMutation({
    mutationFn: () =>
      statistic.downloadExcelThongKeThanhLy({
        TuNgay: searchParams.TuNgay,
        Denngay: searchParams.DenNgay,
        idKho: searchParams.idKho || '',
        sort: searchParams.sort || ''
      }),
    onSuccess: (data) => {
      toast.success('Tải thống kê thanh lý thành công');
      const blob = new Blob([data.data, { type: 'application/xls' }]);
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = 'ThongKeXuatSach.xls';
      link.click();
    }
  });

  const thongKeThanhLy = useMemo(() => {
    if (!thongKeThanhLyData?.data.Item) return;

    return thongKeThanhLyData?.data.Item;
  }, [thongKeThanhLyData?.data.Item]);

  const columns: ColumnsType<StatisticEpxort> = useMemo(() => {
    return [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        className: 'min-content',
        render: (value, record, index) => getSerialNumber(page, pageSize, index),
        width: 80
      },
      {
        title: 'Mã kiểm soát',
        dataIndex: 'MaKiemSoat',
        width: 100
      },
      {
        title: 'Số đăng ký cá biệt',
        dataIndex: 'MaCB',
        key: 'MaCB',
        onCell: (record) => ({
          className:
            record.MaCB?.toLocaleLowerCase().includes('đã xóa') ||
            record.MaCB?.toLocaleLowerCase().includes('đã thanh lý')
              ? 'text-danger-10'
              : ''
        }),
        width: 200
      },
      {
        title: 'Tên sách',
        dataIndex: 'TenSach',
        sorter: true,
        key: 'TenSach',
        render: (value, { TenSach }) => (
          <Tooltip title={TenSach} arrow={true} className='truncate text-left' placement='topLeft'>
            <p>{TenSach}</p>
          </Tooltip>
        ),
        ellipsis: true,
        onCell: (record) => ({
          className: 'text-left'
        }),
        width: 280
      },
      {
        title: 'Kho sách',
        dataIndex: 'IdKhoSach',
        key: 'IdKhoSach',

        render: (value, record) => (
          <Tooltip
            title={bookstoreData?.data.Item.find(({ value }) => value === record?.IdKhoSach + '')?.label}
            arrow={true}
            className='truncate text-left'
            placement='topLeft'
          >
            {bookstoreData?.data.Item.find(({ value }) => value === record?.IdKhoSach + '')?.label || '--'}
          </Tooltip>
        ),
        ellipsis: true,
        onCell: (record) => ({
          className: 'text-left'
        })
      },
      {
        title: 'Tình trạng',
        dataIndex: 'TenTinhTrang',
        key: 'TenTinhTrang',
        width: 100
      },
      {
        title: 'Giá tiền',
        dataIndex: 'GiaTien',
        sorter: true,
        key: 'GiaTien',
        width: 100,
        render: (value, record) => (
          <>
            {record.GiaTien
              ? new Intl.NumberFormat('vi-VN', {
                  style: 'currency',
                  currency: 'VND',
                  maximumFractionDigits: 9
                }).format(Number(record.GiaTien))
              : ''}
          </>
        )
      },
      {
        title: 'Ngày thanh lý',
        dataIndex: 'NgayThanhLy',
        sorter: true,
        key: 'NgayThanhLy'
      },
      {
        title: 'Mã phiếu xuất kho',
        dataIndex: 'MaXuatKho',
        key: 'MaXuatKho',
        render: (value, record) => {
          return (
            <Link to={`/QuanLyXuatKho/ChiTietPhieuXuat/${record.IdPhieuXuat}`} className='text-primary-50 underline'>
              {record.MaXuatKho}
            </Link>
          );
        }
      }
    ];
  }, [bookstoreData?.data.Item, page, pageSize]);

  const handleChangeSelect = (e: ChangeEvent<HTMLSelectElement>) => {
    if (!e) return;

    const { value, name } = e.target;
    setSearchParams((prevState) => {
      return {
        ...prevState,
        [name]: value
      };
    });
  };

  const handleChangeRangePicker = (values: RangeValue) => {
    if (!values?.length) return;

    // @ts-ignore
    setSearchParams((prevState) => {
      return {
        ...prevState,
        TuNgay: values?.[0]?.startOf('day').toISOString(),
        DenNgay: values?.[1]?.endOf('day').toISOString()
      };
    });
  };

  const handleDownloadExcel = () => {
    downloadExcel();
  };

  const handleRefresh = () => {
    setSearchParams({
      TuNgay: dayjs().startOf('year').startOf('day').toISOString(),
      DenNgay: dayjs().endOf('day').toISOString(),
      page: 1,
      pageSize: 30,
      sort: ''
    });
  };

  const handleTableChange = (pagination: any, filters: any, sorter: any) => {
    let value = '';
    if (sorter.columnKey === 'TenSach') {
      if (sorter.order === 'ascend') value = '11';
      if (sorter.order === 'descend') value = '1';
    }
    if (sorter.columnKey === 'GiaTien') {
      if (sorter.order === 'ascend') value = '33';
      if (sorter.order === 'descend') value = '3';
    }
    if (sorter.columnKey === 'NgayThanhLy') {
      if (sorter.order === 'ascend') value = '22';
      if (sorter.order === 'descend') value = '2';
    }
    setSearchParams((prevState) => {
      return {
        ...prevState,
        sort: value
      };
    });
  };

  return (
    <div className='p-5'>
      <Title title='Thống kê thanh lý' />

      <form className='mt-3 flex flex-1 items-center gap-1 bg-primary-50/30 p-2.5'>
        <Select
          items={[{ label: 'Chọn kho', value: '' }, ...(bookstoreData?.data.Item || [])]}
          className='w-1/4'
          name='idKho'
          disabled={!bookstoreData?.data.Item.length || loadingBookstore}
          onChange={handleChangeSelect}
        />

        <RangePicker
          className='w-1/4 border border-gray-300 py-[0.8rem] px-2 outline-none'
          format={['DD/MM/YYYY', 'DD-MM-YYYY']}
          value={[dayjs(searchParams.TuNgay), dayjs(searchParams.DenNgay)]}
          placeholder={['Ngày bắt đầu', 'Ngày kết thúc']}
          disabledDate={(current) => {
            return current && current > dayjs().endOf('day');
          }}
          onChange={handleChangeRangePicker}
          disabled={loadingStartDateData}
        />

        <Button onClick={handleRefresh} type='button'>
          Làm mới
        </Button>
      </form>

      <div className='my-3 flex items-center justify-between'>
        {isAllowedAdjustment && (
          <Button onClick={handleDownloadExcel} loading={loadingDownloadExcel} disabled={loadingDownloadExcel}>
            Tải file Excel
          </Button>
        )}

        <p className='font-bold text-danger-40'>
          Tổng tiền:{' '}
          {thongKeThanhLy?.TongTienThanhLy
            ? new Intl.NumberFormat('vi-VN', {
                style: 'currency',
                currency: 'VND',
                maximumFractionDigits: 9
              }).format(Number(thongKeThanhLy?.TongTienThanhLy))
            : 0}
        </p>
      </div>

      <Table
        columns={columns}
        dataSource={thongKeThanhLy?.List_ThongKePXS || []}
        pagination={{
          current: searchParams.page,
          pageSize: searchParams.pageSize,
          total: thongKeThanhLy?.TongSoLuong,
          onChange: (page: number, pageSize: number) => {
            setSearchParams((prevState) => {
              return {
                ...prevState,
                page,
                pageSize
              };
            });
          },
          showSizeChanger: false,
          showQuickJumper: true,
          locale: { jump_to: '', page: '' }
        }}
        scroll={{ x: 980 }}
        rowKey={(record) => uniqueId()}
        className='custom-table'
        indentSize={40}
        bordered
        locale={{
          emptyText: () => <Empty />
        }}
        loading={loadingThongKeThanhLy}
        onChange={handleTableChange}
        showSorterTooltip={{
          title:
            searchParams.sort === ''
              ? 'Chọn để sắp xếp giảm dần'
              : ['11', '22', '33'].includes(searchParams.sort)
              ? 'Chọn để sắp xếp tăng dần'
              : 'Chọn để hủy sắp xếp'
        }}
      />

      <div
        className={classNames('relative', {
          'mt-[0px]': searchParams.pageSize >= (thongKeThanhLy?.TongSoLuong || 0)
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger
            visible={!!thongKeThanhLy?.TongSoLuong}
            value={searchParams.pageSize + ''}
            currentPage={searchParams.page.toString()}
            total={thongKeThanhLy?.TongSoLuong + ''}
            onChange={(pageSize) => {
              // @ts-ignore
              setSearchParams((prevState) => {
                return {
                  ...prevState,
                  page: 1,
                  pageSize
                };
              });
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default StatisticExportBooks;
