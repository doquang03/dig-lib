import { EditOutlined } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { DatePicker, Tooltip } from 'antd';
import { newspaperMagazineApis } from 'apis';
import classNames from 'classnames';
import { Button, Loading } from 'components';
import { FORMAT_DATE_PICKER } from 'constants/config';
import dayjs, { Dayjs } from 'dayjs';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import PrintNoteBook from './PrintNoteBook';
import RegisterModal from './RegisterModal';

const day: Record<number, string> = {
  0: 'CN',
  1: 'T2',
  2: 'T3',
  3: 'T4',
  4: 'T5',
  5: 'T6',
  6: 'T7'
};

const StatisticByMonth = () => {
  const [isPrint, setIsPrint] = useState<boolean>(false);
  const [isRegisterModalVisible, setIsRegisterModalVisible] = useState<boolean>(false);
  const [rangeDate, setRangeDate] = useState<{ Min: string; Max: string }>({
    Min: dayjs().startOf('month').toISOString(),
    Max: dayjs().endOf('month').toISOString()
  });
  const [idNewspaperMagazine, setIdNewspaperMagazine] = useState<string>('');
  const [dateValue, setDateValue] = useState<Dayjs[]>([]);
  const [row, setRow] = useState<{
    [key: string]: { [key: string]: { Id: string; IdBaoTapChi: string; Ngay: string; SoLuong: number } };
  }>({});

  const navigate = useNavigate();

  const handleBack = () => navigate(-1);

  const tooglePrintModal = () => setIsPrint((prevState) => !prevState);

  const { data: newspaperMagazineData } = useQuery({
    queryKey: ['getNewspaperMagazine'],
    queryFn: () => {
      return newspaperMagazineApis.getData({
        page: 1,
        pageSize: 0
      });
    }
  });

  const { data: newspaperMagazineRegisterationsData, refetch: refetchNewspaperMagazineRegisterationsData } = useQuery({
    queryKey: ['getNewspaperMagazineRegisterationsData', rangeDate],
    queryFn: () => {
      return newspaperMagazineApis.getNewspaperMagazineRegisteration({
        Min: rangeDate.Min,
        Max: rangeDate.Max
      });
    }
  });

  const { mutate: editCell, isLoading: loadingEditCell } = useMutation({
    mutationFn: newspaperMagazineApis.modifyCell,
    onSuccess: () => {
      refetchNewspaperMagazineRegisterationsData();
    }
  });

  useEffect(() => {
    if (
      !newspaperMagazineData?.data.Item.ListBaoTapChi.length &&
      !newspaperMagazineRegisterationsData?.data?.Item?.ListDangKyBaoTapChi.length
    ) {
      setRow({});

      return;
    }

    let cell: {
      [key: string]: { [key: string]: { Id: string; IdBaoTapChi: string; Ngay: string; SoLuong: number } };
    } = {};

    const newspaperMagazineRegiseration = newspaperMagazineRegisterationsData?.data.Item.ListDangKyBaoTapChi || [];

    // cột trước dòng sau
    newspaperMagazineRegiseration.forEach((item) => {
      // init dòng
      if (!cell[new Date(item.Ngay).toISOString()]) {
        cell[new Date(item.Ngay).toISOString()] = {};
      }

      // khởi tạo giá trị ô
      cell[new Date(item.Ngay).toISOString()][item?.IdBaoTapChi] = item;
    });

    setRow(cell);
  }, [newspaperMagazineData, newspaperMagazineRegisterationsData]);

  useEffect(() => {
    let array = [];
    const daysInMonth = dayjs().daysInMonth();

    for (let index = 0; index < daysInMonth; index++) {
      const elm = dayjs().startOf('month').add(index, 'day');
      array.push(elm);
    }

    setDateValue(array);
  }, []);

  const getCell = (idNewspaperMagazine: string, date: string) => {
    let cellValue = {
      Id: '',
      IdBaoTapChi: idNewspaperMagazine,
      Ngay: date,
      SoLuong: 0
    };

    // console.log(row[date][idNewspaperMagazine]);
    // tìm kiếm trong dữ liệu
    if (!row?.[date] || !row[date][idNewspaperMagazine]) return cellValue;

    return row[date][idNewspaperMagazine];
  };

  const toogleRegisterModal = (id: string) => () => {
    setIsRegisterModalVisible((prevState) => !prevState);
    setIdNewspaperMagazine(id);
  };

  const handleInvisibleRegisterModal = () => setIsRegisterModalVisible((prevState) => !prevState);

  const handleResiterSuccess = () => {
    refetchNewspaperMagazineRegisterationsData();
  };

  return (
    <div>
      <div className='flex items-center justify-between'>
        <div className='mt-6 flex items-center gap-2'>
          <Button className='hidden'>Thêm từ file Excel</Button>

          <Button onClick={tooglePrintModal}>Tải sổ đăng ký báo</Button>
        </div>

        <DatePicker
          picker='month'
          placeholder='Chọn tháng'
          defaultValue={dayjs()}
          format={FORMAT_DATE_PICKER[3]}
          onChange={(value) => {
            if (value) {
              let array = [];
              const daysInMonth = dayjs(value).daysInMonth();

              for (let index = 0; index < daysInMonth; index++) {
                const elm = dayjs(value).startOf('month').add(index, 'day');
                array.push(elm);
              }

              setRangeDate({
                Min: dayjs(value).startOf('month').toISOString(),
                Max: dayjs(value).endOf('month').toISOString()
              });

              setDateValue(array);
            }
          }}
        />
      </div>

      <div className='mt-3 max-h-[40rem] overflow-x-auto overflow-y-auto'>
        <table className='table-striped table-bordered table whitespace-normal' cellPadding='1' width={'100%'}>
          <thead className='sticky top-0 bg-[rgb(235,244,250)] text-center font-bold text-primary-10'>
            <tr>
              <td className='sticky left-0 bg-[rgb(235,244,250)] text-left font-semibold text-primary-10'>
                Tên báo, tạp chí
              </td>

              {dateValue.map((item) => {
                return (
                  <td key={item.toISOString()} className='z-0 bg-[rgb(235,244,250)]'>
                    <p className='border-b-[0.5px] border-b-primary-10'>{day[item.day()]}</p>
                    <p>{item.format('DD')}</p>
                  </td>
                );
              })}
            </tr>
          </thead>

          {Boolean(newspaperMagazineData?.data.Item.TotalCount) && (
            <>
              <tbody className='text-center font-semibold text-primary-10'>
                {newspaperMagazineData?.data.Item.ListBaoTapChi.map(({ Id, Ten }, index) => {
                  return (
                    <tr key={Id}>
                      <td
                        onClick={toogleRegisterModal(Id)}
                        className={classNames(
                          'sticky left-0 flex w-full cursor-pointer items-center justify-between gap-1 truncate whitespace-nowrap',
                          {
                            'bg-[#f5f5f5f5]': index % 2 !== 0,
                            'bg-[#f9f9f9f9]': index % 2 === 0
                          }
                        )}
                      >
                        {Ten.length > 25 ? (
                          <Tooltip title={Ten} arrow={true} className='flex w-full items-center justify-between gap-1'>
                            <p className='text-center'>{Ten.substring(0, 25).concat('...')}</p>
                            <EditOutlined />
                          </Tooltip>
                        ) : (
                          <>
                            <p className='text-center'>{Ten}</p>
                            <EditOutlined />
                          </>
                        )}
                      </td>

                      {dateValue.map((itemDate) => {
                        const resultDates = itemDate.format('DD/MM/YYYY').split('/');

                        const cell = getCell(
                          Id,
                          new Date(resultDates[1] + '/' + resultDates[0] + '/' + resultDates[2]).toISOString()
                        );

                        return (
                          <td key={itemDate.toISOString()}>
                            <input
                              type='text'
                              placeholder='--'
                              value={cell?.SoLuong || ''}
                              className='w-11 bg-transparent text-center focus:outline-primary-10'
                              onChange={(e) => {
                                let clone = { ...cell };

                                clone.SoLuong = +e.target.value;
                                if (
                                  !row[
                                    new Date(resultDates[1] + '/' + resultDates[0] + '/' + resultDates[2]).toISOString()
                                  ]
                                ) {
                                  row[
                                    new Date(resultDates[1] + '/' + resultDates[0] + '/' + resultDates[2]).toISOString()
                                  ] = {};
                                }

                                row[
                                  new Date(resultDates[1] + '/' + resultDates[0] + '/' + resultDates[2]).toISOString()
                                ][Id] = clone;

                                setRow({ ...row });
                              }}
                              onBlur={(e) => {
                                let clone = { ...cell };

                                clone.SoLuong = +e.target.value;

                                if (
                                  !row[
                                    new Date(resultDates[1] + '/' + resultDates[0] + '/' + resultDates[2]).toISOString()
                                  ]
                                ) {
                                  row[
                                    new Date(resultDates[1] + '/' + resultDates[0] + '/' + resultDates[2]).toISOString()
                                  ] = {};
                                }

                                row[
                                  new Date(resultDates[1] + '/' + resultDates[0] + '/' + resultDates[2]).toISOString()
                                ][Id] = clone;

                                editCell({ Id: cell.Id, IdBaoTapChi: Id, Ngay: cell.Ngay, SoLuong: cell.SoLuong });
                              }}
                              onKeyDown={(e) => {
                                if (e.key === 'Enter') {
                                  // @ts-ignore

                                  let clone = { ...cell };
                                  // @ts-ignore
                                  clone.SoLuong = +e.target.value;

                                  if (
                                    !row[
                                      new Date(
                                        resultDates[1] + '/' + resultDates[0] + '/' + resultDates[2]
                                      ).toISOString()
                                    ]
                                  ) {
                                    row[
                                      new Date(
                                        resultDates[1] + '/' + resultDates[0] + '/' + resultDates[2]
                                      ).toISOString()
                                    ] = {};
                                  }

                                  row[
                                    new Date(resultDates[1] + '/' + resultDates[0] + '/' + resultDates[2]).toISOString()
                                  ][Id] = clone;

                                  editCell({
                                    Id: cell.Id,
                                    IdBaoTapChi: Id,
                                    Ngay: cell.Ngay,
                                    SoLuong: cell.SoLuong
                                  });
                                }
                              }}
                              disabled={loadingEditCell}
                              min={0}
                              maxLength={5}
                            />
                          </td>
                        );
                      })}
                    </tr>
                  );
                })}
              </tbody>

              <tfoot className='sticky bottom-0 bg-[rgb(235,244,250)] text-center font-bold text-primary-10'>
                <tr>
                  <td className='sticky left-0 bg-[rgb(235,244,250)] text-left'>Tổng số báo, tạp chí</td>

                  {dateValue.map((itemDate) => {
                    let sum = 0;
                    const resultDates = itemDate.format('DD/MM/YYYY').split('/');

                    for (const key in row) {
                      if (
                        key === new Date(resultDates[1] + '/' + resultDates[0] + '/' + resultDates[2]).toISOString()
                      ) {
                        for (const _key in row[key]) {
                          sum += row[key][_key].SoLuong;
                        }
                        break;
                      }
                    }

                    return <td key={itemDate.toISOString()}>{sum}</td>;
                  })}
                </tr>
              </tfoot>
            </>
          )}
        </table>
      </div>

      <div className='mt-3 flex justify-end gap-2'>
        <Button variant='secondary' onClick={handleBack}>
          Quay về
        </Button>

        <Button>Cập nhật</Button>
      </div>

      <PrintNoteBook
        open={isPrint}
        onInvisile={tooglePrintModal}
        newspaperMagazines={newspaperMagazineData?.data.Item.ListBaoTapChi || []}
        onClose={() => setIsPrint(false)}
      />

      <RegisterModal
        open={isRegisterModalVisible}
        onInvisile={handleInvisibleRegisterModal}
        id={idNewspaperMagazine}
        onSuccess={handleResiterSuccess}
      />

      <Loading open={loadingEditCell} />
    </div>
  );
};

export default StatisticByMonth;
