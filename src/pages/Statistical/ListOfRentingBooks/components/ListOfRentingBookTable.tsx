import { useMutation, useQuery } from '@tanstack/react-query';
import { Table } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { rentBack, statistic } from 'apis';
import classNames from 'classnames';
import { Button, Empty, ModalDelete, SizeChanger } from 'components';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import { Dayjs } from 'dayjs';
import { PickerMode } from 'rc-picker/lib/interface';
import { useEffect, useMemo, useState, type ChangeEvent } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { convertDate, getSerialNumber } from 'utils/utils';
import NotReturnedBookModal from './NotReturnedBookModal';
import RentingBookSearchForm from './RentingBookSearchForm';

type Props = {
  picker: PickerMode;
  statusBook: 0 | 1 | 2 | 3 | 4 | 5;
};

const ListOfRentingBookTable = (props: Props) => {
  const [searchParams, setSearchParams] = useState<{
    day?: string;
    month?: string;
    year?: string;
    TinhTrang: 0 | 1 | 2 | 3 | 4 | 5;
    IsGlobal?: string | boolean;
    styleUser: '' | 'hs' | 'gv';
    page: number;
    pageSize: number;
    SortNgayMuon: boolean;
  }>({
    month: (new Date().getMonth() + 1).toString(),
    year: new Date().getFullYear().toString(),
    TinhTrang: 0,
    styleUser: '',
    page: 1,
    pageSize: 30,
    SortNgayMuon: false
  });
  const [visibleNotRented, setvisivleNotRented] = useState<boolean>(false);
  const [lostBookId, setLostBookId] = useState<string>('');

  const navigate = useNavigate();

  const { isAllowedAdjustment } = useUser();

  const {
    data: danhMucMuonSachData,
    isFetching: loadingDanhMucMuonSach,
    refetch: reftchListOFRetingBooks
  } = useQuery({
    queryKey: ['danhMucMuonSach', searchParams],
    queryFn: () => {
      if (props.picker === 'month') delete searchParams.day;
      return statistic.getDanhMucMuonSach(searchParams);
    }
  });

  const danhMucSach = useMemo(() => {
    if (!danhMucMuonSachData?.data.Item) return;

    return danhMucMuonSachData.data.Item;
  }, [danhMucMuonSachData?.data.Item]);

  useEffect(() => {
    if (!(props.picker === 'month')) {
      if (searchParams.day === undefined) searchParams.day = new Date().getDate().toString();
    } else {
      delete searchParams.day;
    }
  });

  const { mutate: lostBook, isLoading: loadingLostBook } = useMutation({
    mutationFn: rentBack.lostBook,
    onSuccess: () => {
      reftchListOFRetingBooks();
      toast.success('Đã cập nhật tình trạng sách');
      setLostBookId('');
    }
  });

  const { mutate: downloadExcel, isLoading: loadingDownloadExcel } = useMutation({
    mutationFn: () => statistic.downExcelThongKeMuonSach(searchParams),
    onSuccess: (data) => {
      toast.success('Tải danh mục mượn sách thành công');
      const blob = new Blob([data.data, { type: 'application/xls' }]);
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = 'DanhMucMuonSach.xls';
      link.click();
    }
  });

  const columns: ColumnsType<StaticRentingBook> = useMemo(() => {
    return [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        render: (value, record, index) => {
          return getSerialNumber(searchParams.page, searchParams.pageSize, index);
        },
        width: 80
      },
      {
        title: 'Mã cá biệt',
        dataIndex: 'MaCaBiet',
        key: 'MaCaBiet',
        onCell: (record) => ({
          className:
            record?.MaCaBiet?.toLocaleLowerCase().includes('xóa') ||
            record?.MaCaBiet?.toLocaleLowerCase().includes('thanh lý')
              ? 'text-danger-10'
              : ''
        }),
        width: 120
      },
      {
        title: 'Tên sách',
        dataIndex: 'TenSach',
        key: 'TenSach',
        onCell: (record) => ({
          className:
            record.TenSach.toLocaleLowerCase().includes('đã bị xóa') ||
            record.TenSach.toLocaleLowerCase().includes('đã bị thanh lý')
              ? 'text-danger-10 text-left'
              : 'text-left'
        }),
        render: (value, { TenSach, idSach }) => {
          return !TenSach.includes('"sách đã bị xóa"') ? (
            <Link
              to={`/Sach/CapNhat/${idSach}`}
              className='cursor-pointer truncate text-left text-primary-50 underline'
            >
              {TenSach}
            </Link>
          ) : (
            <p className='text-left'>{TenSach}</p>
          );
        },
        ellipsis: true,
        width: 250
      },
      {
        title: 'Mã kiểm soát',
        dataIndex: 'MaKiemSoat',
        key: 'MaKiemSoat',
        width: 120
      },
      {
        title: 'Người mượn',
        dataIndex: 'TenThanhVien',
        key: 'TenThanhVien',
        onCell: (record) => ({
          className: record.TenThanhVien?.toLocaleLowerCase().includes('đã xóa')
            ? 'text-danger-10 text-left'
            : 'text-left'
        }),
        render: (value, { TenThanhVien, LoaiTK, idUser, IsGlobal }) => {
          return !TenThanhVien?.toLocaleLowerCase().includes('đã xóa') && !IsGlobal ? (
            <Link
              to={LoaiTK === 'gv' ? `/GiaoVien/CapNhat/${idUser}` : `/HocSinh/CapNhat/${idUser}`}
              className='truncate text-primary-30 underline'
            >
              {TenThanhVien}
            </Link>
          ) : (
            TenThanhVien
          );
        },
        ellipsis: true
      },
      {
        title: 'Ngày mượn',
        dataIndex: 'NgayMuonTemp',
        key: 'NgayMuonTemp'
      },
      {
        title: 'Ngày phải trả',
        dataIndex: 'NgayTraTemp',
        key: 'NgayTraTemp'
      },
      {
        title: 'Ngày trả',
        dataIndex: 'NgayTraThucTeTemp',
        key: 'NgayTraThucTeTemp',
        render: (value, record) => {
          return (
            <>
              {' '}
              {record.DaTra
                ? `${new Date(record.NgayTraThucTe).getHours()}:${(
                    '0' + new Date(record.NgayTraThucTe).getMinutes()
                  ).slice(-2)} - ${convertDate(record.NgayTraThucTe)} `
                : record.NgayTraThucTeTemp}
            </>
          );
        }
      },
      {
        title: 'Tình trạng',
        dataIndex: 'TrangThaiSach',
        key: 'TrangThaiSach'
      },
      {
        title: 'Trạng thái',
        dataIndex: 'TenTrangThai',
        key: 'TenTrangThai',
        onCell: (record) => {
          switch (record.TrangThaiString) {
            case 'ChuaTra':
              return { className: 'bg-red-300' };

            case 'TraTre':
              return { className: 'bg-tertiary-60/50' };

            case 'TraDungHen':
              return { className: 'bg-tertiary-30/50' };

            case 'GanTra':
              return { className: 'bg-primary-60/50' };

            case 'MatSach':
              return { className: 'bg-secondary-30/50' };

            default:
              return { className: 'none' };
          }
        }
      },
      {
        title: '',
        dataIndex: 'action',
        key: 'action',
        render: (value, record) => {
          const handleLostBook = () => setLostBookId(record.Id);

          return (
            !!['GanTra', 'ChuaTra'].includes(record.TrangThaiString) && (
              <div className='flex items-end justify-center'>
                <Button variant='danger' onClick={handleLostBook}>
                  Mất sách
                </Button>
              </div>
            )
          );
        }
      }
    ];
  }, [searchParams.page, searchParams.pageSize]);

  const handleChangeSearchParams = (e: ChangeEvent<HTMLSelectElement | HTMLInputElement> | 0 | 1 | 2 | 3 | 4 | 5) => {
    if (typeof e === 'number') {
      setSearchParams((prevState) => {
        return {
          ...prevState,
          TinhTrang: e,
          page: 1
        };
      });
    } else {
      const { name, value } = e.target;
      if (name === 'IsGlobal') {
        setSearchParams((prevState) => {
          return {
            ...prevState,
            IsGlobal: value === '1' ? true : value === '0' ? false : undefined,
            page: 1
          };
        });
      } else
        setSearchParams((prevState) => {
          return {
            ...prevState,
            [name]: name === 'TinhTrang' ? Number(value) : value,
            page: 1
          };
        });
    }
  };

  const handleChangeDatePicker = (value: Dayjs | null) => {
    if (value) {
      props.picker === 'month'
        ? setSearchParams((prevState) => {
            return {
              ...prevState,
              month: (value?.month() + 1).toString(),
              year: value?.year().toString()
            };
          })
        : setSearchParams((prevState) => {
            return {
              ...prevState,
              day: value?.date().toString(),
              month: (value?.month() + 1).toString(),
              year: value?.year().toString()
            };
          });
    }
  };

  const handlePrintNotRentedBooks = () => navigate(path.traSachDanhMucSach);

  return (
    <div className='mt-4'>
      <RentingBookSearchForm
        picker={props.picker}
        statusBook={props.statusBook}
        onChangeSearchParams={handleChangeSearchParams}
        onChangeDatePicker={handleChangeDatePicker}
      />

      <div className={isAllowedAdjustment ? ' mt-3 flex items-center gap-2' : 'hidden'}>
        <Button onClick={handlePrintNotRentedBooks}> Trả sách</Button>

        <Button
          onClick={() => downloadExcel()}
          loading={loadingDownloadExcel}
          disabled={loadingDownloadExcel}
          type='button'
        >
          Tải file Excel
        </Button>

        <Button onClick={() => setvisivleNotRented(true)}>In danh sách chưa trả</Button>
      </div>

      <p className='mt-3 text-danger-10'>
        Tổng số sách được mượn: <span className='font-bold'>{danhMucSach?.SoSachDuocMuon || '--'} quyển</span>
      </p>

      <p className='mb-3 text-danger-10'>
        Tổng số người mượn sách: <span className='font-bold'>{danhMucSach?.SoNguoiMuonSach || '--'} người</span>
      </p>

      <Table
        bordered
        rowKey={(row) => row.Id}
        className='custom-table'
        columns={columns}
        dataSource={danhMucSach?.ListRutGon || []}
        locale={{
          emptyText: () => <Empty />
        }}
        loading={loadingDanhMucMuonSach}
        pagination={{
          onChange(current, pageSize) {
            setSearchParams((prevState) => {
              return {
                ...prevState,
                pageSize: pageSize || 30,
                page: current
              };
            });
          },
          pageSize: searchParams.pageSize,
          hideOnSinglePage: false,
          showSizeChanger: false,
          total: danhMucSach?.count,
          showQuickJumper: true,
          locale: { jump_to: '', page: '' },
          current: searchParams.page
        }}
      />

      <div
        className={classNames('relative', {
          'mt-[0px]': searchParams.pageSize >= Number(danhMucSach?.count || 0)
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger
            visible={!!danhMucSach?.ListRutGon.length}
            value={searchParams.pageSize + ''}
            currentPage={searchParams.page.toString()}
            total={danhMucSach?.count + ''}
            onChange={(pageSize) => {
              setSearchParams((prevState) => {
                return {
                  ...prevState,
                  pageSize: +pageSize,
                  page: 1
                };
              });
            }}
          />
        </div>
      </div>

      <ModalDelete
        open={!!lostBookId}
        closable={false}
        title={
          <h1 className='text-center'>
            CẢNH BÁO
            <p className='text-lg'>Sách được báo mất thì không thể phục hồi</p>
          </h1>
        }
        handleCancel={() => setLostBookId('')}
        handleOk={() => {
          lostBookId && lostBook(lostBookId);
        }}
        loading={loadingLostBook}
        labelOK='Xác nhận'
      />

      <NotReturnedBookModal
        open={visibleNotRented}
        onBack={() => setvisivleNotRented(false)}
        onSubmit={() => setvisivleNotRented(false)}
      />
    </div>
  );
};

export default ListOfRentingBookTable;
