import { useMutation } from '@tanstack/react-query';
import { Checkbox, DatePicker, Modal, ModalProps, Radio, RadioChangeEvent } from 'antd';
import { CheckboxChangeEvent } from 'antd/es/checkbox';
import locale from 'antd/es/date-picker/locale/vi_VN';
import statisticApis from 'apis/statistic.apis';
import { Button, Title } from 'components';
import { FORMAT_DATE_PICKER } from 'constants/config';
import dayjs, { Dayjs } from 'dayjs';
import { useState } from 'react';
import { toast } from 'react-toastify';

type Props = {
  onBack: VoidFunction;
  onSubmit: VoidFunction;
} & ModalProps;

const NotReturnedBookModal = (props: Props) => {
  const { open, onSubmit, onBack } = props;
  const [stateLoading, setStateLoading] = useState(false);
  const [formValues, setFormValues] = useState<{ date: string; typeAccount: '' | 'gv' | 'hs'; optionSplit: boolean }>({
    date: dayjs().format(FORMAT_DATE_PICKER[0]),
    typeAccount: '',
    optionSplit: true
  });

  const handleOnChangeDatePicker = (value: Dayjs | null) => {
    setFormValues((prevState) => {
      return {
        ...prevState,
        date: dayjs(value).format(FORMAT_DATE_PICKER[0])
      };
    });
  };

  const handleOnChange = (e: RadioChangeEvent | CheckboxChangeEvent) => {
    let { value, name = '' } = e.target;
    if (name === 'optionSplit') value = e.target.checked;
    setFormValues((prevState) => {
      return {
        ...prevState,
        [name]: value
      };
    });
  };

  const { mutateAsync: handleDownloadNotReturn, isLoading: isLoadingDownLoad } = useMutation({
    mutationFn: (
      payload: Partial<{
        day: string;
        typeAccount: string;
        optionSplit: boolean;
      }>
    ) => statisticApis.downloadExcelNotReturn(payload),
    mutationKey: ['DownloadNotReturn'],
    onSuccess: (data, req) => {
      toast.success('Tải danh sách chưa trả thành công');
      const blob = new Blob([data.data, { type: 'application/xls' }]);
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download =
        'Thống kê danh sách chưa trả của ' + (req.typeAccount === 'hs' ? 'học sinh' : 'giáo viên') + '.xls';
      link.click();
    }
  });

  const handlePrint = () => {
    if (formValues.typeAccount === '') {
      setStateLoading(true);
      let array: Array<Promise<any>> = [];
      array.push(handleDownloadNotReturn({ ...formValues, typeAccount: 'hs' }));
      array.push(handleDownloadNotReturn({ ...formValues, typeAccount: 'gv' }));
      Promise.all(array).then(() => {
        setStateLoading(false);
        onSubmit();
      });
    } else
      handleDownloadNotReturn(formValues).then(() => {
        onSubmit();
      });
  };

  return (
    <Modal open={open} footer={null} closable={false}>
      <Title title='In danh sách chưa trả' />

      <div className='mt-3 flex flex-col gap-3'>
        <div>
          <label className='font-semibold'>Tính đến ngày</label>
          <DatePicker
            name='rentDate'
            onChange={handleOnChangeDatePicker}
            className={'w-full border border-gray-300 py-[0.8rem] px-2 outline-none'}
            format={FORMAT_DATE_PICKER[0]}
            disabledDate={(current) => current >= dayjs().endOf('day')}
            locale={locale}
            defaultValue={dayjs()}
          />
        </div>

        <div>
          <label className='font-semibold'>Đối tượng bạn đọc:</label>
          <div className='flex items-center'>
            <Radio.Group onChange={handleOnChange} value={formValues.typeAccount} name='typeAccount'>
              <Radio value={''}>Tất cả</Radio>
              <Radio value={'gv'}>Giáo viên</Radio>
              <Radio value={'hs'}>Học sinh</Radio>
            </Radio.Group>
          </div>
        </div>

        <div className='flex flex-col'>
          <label className='font-semibold'>Tùy chọn:</label>
          <Checkbox onChange={handleOnChange} name='optionSplit' defaultChecked={formValues.optionSplit}>
            In mỗi lớp/tổ một danh sách
          </Checkbox>
        </div>
      </div>

      <div className='flex justify-end gap-2'>
        <Button variant='secondary' onClick={onBack} loading={isLoadingDownLoad || stateLoading}>
          Quay về
        </Button>
        <Button variant='default' onClick={handlePrint} loading={isLoadingDownLoad || stateLoading}>
          In danh sách
        </Button>
      </div>
    </Modal>
  );
};

export default NotReturnedBookModal;
