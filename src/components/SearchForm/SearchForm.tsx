import { Select } from 'components';
import { Button } from 'components/Button';
import { Input } from 'components/Input';
import { usePaginationNavigate } from 'hooks';
import { isEmpty, omitBy } from 'lodash';
import { useForm } from 'react-hook-form';

type Props = {} & Partial<HTMLInputElement>;

const SearchForm = (props: Props) => {
  const { placeholder = 'Nhập ...' } = props;

  const navigate = usePaginationNavigate();

  const { register, reset, handleSubmit } = useForm<{
    TextForSearch: string;
    Type: '' | 'image' | 'document' | 'audio' | 'video' | 'paper';
  }>({
    defaultValues: {
      TextForSearch: ''
    }
  });

  const handleRefresh = () => {
    reset();
    navigate({ page: '1' });
  };

  const onSubmit = handleSubmit((data) => {
    const { TextForSearch, Type } = data;

    navigate(omitBy({ TextForSearch, Type }, isEmpty));
  });

  return (
    <form className='my-5 w-full bg-primary-50/20 p-3.5' onSubmit={onSubmit}>
      <div className='flex w-2/3 flex-1 flex-col items-center gap-1 md:flex-row'>
        <Input placeholder={placeholder} containerClassName='flex-grow' name='TextForSearch' register={register} />
        <Select
          register={register}
          name='Type'
          className='flex-grow'
          items={[
            { value: '', label: 'Chọn loại tài liệu' },
            { value: 'paper', label: 'Sách in' },
            { value: 'document', label: 'Sách điện tử' },
            { value: 'image', label: 'Album ảnh' },
            { value: 'audio', label: 'Sách nói' },
            { value: 'video', label: 'Video' }
          ]}
        />
        <Button variant='secondary' type='button' className='w-full md:w-auto' onClick={handleRefresh}>
          Làm mới
        </Button>

        <Button variant='default' type='submit' className='w-full md:w-auto'>
          Tìm kiếm
        </Button>
      </div>
    </form>
  );
};

export default SearchForm;
