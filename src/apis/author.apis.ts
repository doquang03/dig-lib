import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/TacGia`;

const teacherApis = {
  getAuthors: (queryString: string) => {
    return httpThuVien.post(`/${endPoint}/IndexByModel` + queryString);
  },
  EditByID: (id: string) => {
    return httpThuVien.post(`/${endPoint}/EditByID?id=${id}`);
  },
  Delete: (id: string) => {
    return httpThuVien.post(`/${endPoint}/Delete?Id=${id}`);
  },
  ImportSave: (payload: object) => {
    return httpThuVien.post(`/${endPoint}/ImportSave`, payload);
  },
  PreviewImport: (payload: object) => {
    return httpThuVien({
      method: 'post',
      url: `/${endPoint}/PreviewImport`,
      data: payload,
      headers: { 'Content-Type': 'multipart/form-data' }
    });
  },
  Edit: (payload: object) => {
    return httpThuVien.post(`/${endPoint}/Edit`, payload);
  },
  Create: (payload: object) => {
    return httpThuVien.post(`/${endPoint}/Create`, payload);
  }
};

export default teacherApis;
