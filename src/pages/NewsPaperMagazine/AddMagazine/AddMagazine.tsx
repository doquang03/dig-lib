import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery } from '@tanstack/react-query';
import { newspaperMagazineApis } from 'apis';
import { AxiosError } from 'axios';
import { Button, Input, Loading, Select, Title } from 'components';
import { path } from 'constants/path';
import { useLanguages } from 'hooks';
import { useMemo } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { newspaperMagazineSchema } from 'utils/rules';

const exceptThisSymbols = ['e', 'E', '+', '-', '.'];

const AddMagazine = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue,
    setError
  } = useForm<{
    code: string;
    name: string;
    bilingualName: string;
    releaseTerm: string;
    language: string;
    editorialAgency: string;
    issuingAgency: string;
    paperSize: string;
    pageNumber: string;
    price: string;
    ISSN: string;
  }>({
    resolver: yupResolver(newspaperMagazineSchema('magazine'))
  });

  const { id } = useParams();

  const { data: languages } = useLanguages();

  const navigate = useNavigate();

  const { mutate: mutateAddNewspaper, isLoading: loadingAddNewspaper } = useMutation({
    mutationFn: newspaperMagazineApis.createNewspapgerMagazine,
    onSuccess: () => {
      toast.success('Thêm mới tạp chí thành công');
      navigate(path.newspaperMagazineListing);
    },
    onError: (error: AxiosError<ResponseApi<NewspaperMagazine>>) => {
      error?.response?.data.Item.MaBaoTapChi &&
        setError('code', { message: error?.response?.data.Message, type: 'validate' });

      error?.response?.data.Item.Ten && setError('name', { message: error?.response?.data.Message, type: 'validate' });

      error?.response?.data.Item.TenSongNgu &&
        setError('bilingualName', { message: error?.response?.data.Message, type: 'validate' });
    }
  });

  const { mutate: updateMagazine, isLoading: loadingUpdateMagazine } = useMutation({
    mutationFn: newspaperMagazineApis.updateNewspapgerMagazine,
    onSuccess: () => {
      toast.success('Cập nhật tạp chí thành công');
    },
    onError: (error: AxiosError<ResponseApi<NewspaperMagazine>>) => {
      error?.response?.data.Item.MaBaoTapChi &&
        setError('code', { message: error?.response?.data.Message, type: 'validate' });

      error?.response?.data.Item.Ten && setError('name', { message: error?.response?.data.Message, type: 'validate' });
      error?.response?.data.Item.TenSongNgu &&
        setError('bilingualName', { message: error?.response?.data.Message, type: 'validate' });
    }
  });

  const { isLoading: loadingMagazineData } = useQuery({
    queryKey: ['getOneData', id],
    queryFn: () => newspaperMagazineApis.getOneData(id + ''),
    enabled: Boolean(id),
    onSuccess: (data) => {
      const { Ten, MaBaoTapChi, CQBienTap, CQPhatHanh, CoGiay, GiaBia, IdNgonNgu, SoTrang, KyHan, TenSongNgu, ISSN } =
        data.data.Item;

      setValue('code', MaBaoTapChi);
      setValue('name', Ten);
      setValue('bilingualName', TenSongNgu);
      setValue('releaseTerm', KyHan);
      setValue('language', IdNgonNgu);
      setValue('editorialAgency', CQBienTap);
      setValue('issuingAgency', CQPhatHanh);
      setValue('paperSize', CoGiay);
      setValue('pageNumber', SoTrang.toString());
      setValue('price', GiaBia.toString());
      setValue('ISSN', ISSN);
    }
  });

  const languageOptions = useMemo(() => {
    if (!languages.length) return [];

    return languages.map(({ Id, Ten }) => {
      return {
        value: Id,
        label: Ten
      };
    });
  }, [languages]);

  const onSubmit = handleSubmit((data) => {
    let { code, name, bilingualName, editorialAgency, issuingAgency, ISSN } = data;

    code = code.replace(/ +(?= )/g, '');
    name = name.replace(/ +(?= )/g, '');
    bilingualName = bilingualName.replace(/ +(?= )/g, '');
    editorialAgency = editorialAgency.replace(/ +(?= )/g, '');
    issuingAgency = issuingAgency.replace(/ +(?= )/g, '');
    ISSN = ISSN.replace(/ +(?= )/g, '');

    setValue('code', code);
    setValue('name', name);
    setValue('bilingualName', bilingualName);
    setValue('editorialAgency', editorialAgency);
    setValue('issuingAgency', issuingAgency);
    setValue('ISSN', ISSN);

    Boolean(id)
      ? updateMagazine({
          Id: id + '',
          MaBaoTapChi: code,
          Ten: name,
          TenSongNgu: bilingualName,
          KyHan: data.releaseTerm,
          CQBienTap: editorialAgency,
          CQPhatHanh: issuingAgency,
          CoGiay: data.paperSize,
          SoTrang: +data.pageNumber,
          GiaBia: +data.price || 0,
          ISSN: ISSN,
          IdNgonNgu: data.language,
          isBao: false
        })
      : mutateAddNewspaper({
          MaBaoTapChi: data.code,
          Ten: data.name,
          TenSongNgu: data.bilingualName,
          KyHan: data.releaseTerm,
          CQBienTap: data.editorialAgency,
          CQPhatHanh: data.issuingAgency,
          CoGiay: data.paperSize,
          SoTrang: +data.pageNumber,
          GiaBia: +data.price,
          ISSN: ISSN,
          IdNgonNgu: data.language,
          isBao: false
        });
  });

  const handleBack = () => navigate(-1);

  return (
    <div>
      <Title title={Boolean(id) ? 'Cập nhật tạp chí' : 'Thêm tạp chí mới'} />

      <form action='' className='mt-3 grid grid-cols-12 gap-10'>
        <div className='col-span-4 flex flex-col gap-4'>
          <div>
            <label className='mb-1 font-semibold'>
              Mã tạp chí <span className='text-danger'>*</span>
            </label>

            <Input
              placeholder='Nhập mã tạp chí'
              register={register}
              name='code'
              errorMessage={errors.code?.message}
              maxLength={50}
            />
          </div>

          <div>
            <label className='mb-1 font-semibold'>
              Tên tạp chí <span className='text-danger'>*</span>
            </label>

            <Input
              placeholder='Nhập tên tạp chí'
              register={register}
              name='name'
              errorMessage={errors.name?.message}
              maxLength={256}
            />
          </div>

          <div>
            <label className='mb-1 font-semibold'>ISSN</label>

            <Input placeholder='Nhập ISSN' register={register} name='ISSN' />
          </div>

          <div>
            <label className='mb-1 font-semibold'>Tên tạp chí song ngữ</label>

            <Input
              placeholder='Nhập tên tạp chí song ngữ'
              register={register}
              name='bilingualName'
              maxLength={256}
              errorMessage={errors.bilingualName?.message}
            />
          </div>

          <div className='flex flex-grow items-center gap-2'>
            <div className='w-full'>
              <label className='mb-1 font-semibold'>Kỳ hạn phát hành</label>

              <Select
                register={register}
                name='releaseTerm'
                items={[
                  { value: 'Hàng tuần', label: 'Hàng tuần' },
                  { value: 'Hàng tháng', label: 'Hàng tháng' }
                ]}
                className='w-full'
              />
            </div>
            <div className='w-full'>
              <label className='mb-1 font-semibold'>Ngôn ngữ</label>

              <Select
                items={[{ label: 'Chọn ngôn ngữ', value: '' }, ...languageOptions]}
                className='w-full'
                register={register}
                name='language'
              />
            </div>
          </div>
        </div>

        <div className='col-span-4 flex flex-col gap-4'>
          <div>
            <label className='mb-1 font-semibold'>Đơn vị chủ quản</label>

            <Input placeholder='Nhập đơn vị chủ quản' register={register} name='editorialAgency' />
          </div>

          <div>
            <label className='mb-1 font-semibold'>Địa chỉ (Chủ quản phát hành)</label>

            <Input placeholder='Nhập địa chỉ (Chủ quản phát hành)' register={register} name='issuingAgency' />
          </div>

          <div className='flex items-center gap-2'>
            <div className='w-full'>
              <label className='mb-1 font-semibold'>Khổ giấy</label>

              <Input
                placeholder='Nhập khổ giấy'
                type='number'
                containerClassName='pl-2 bg-white w-full border rounded-lg flex mb-1'
                className='mr-2 w-full outline-none'
                right={
                  <div className='rounded-br-md rounded-tr-md bg-secondary-20/30 py-3 px-3'>
                    <span className='text-xs'>cm</span>
                  </div>
                }
                register={register}
                name='paperSize'
              />
            </div>

            <div className='w-full'>
              <label className='mb-1 font-semibold'>Số trang</label>

              <Input
                type='text'
                placeholder='Nhập số trang'
                containerClassName='pl-2 bg-white w-full border rounded-lg flex mb-1'
                className='mr-2 w-full outline-none'
                right={
                  <div className='rounded-br-md rounded-tr-md bg-secondary-20/30 py-3 px-3'>
                    <span className='text-xs'>trang</span>
                  </div>
                }
                register={register}
                name='pageNumber'
                onKeyDown={(e) => exceptThisSymbols.includes(e.key) && e.preventDefault()}
                onKeyPress={(event) => {
                  if (!/[0-9]/.test(event.key)) {
                    event.preventDefault();
                  }
                }}
                maxLength={3}
              />
            </div>
          </div>

          <div>
            <label className='mb-1 font-semibold'>Giá bìa</label>

            <Input
              type='number'
              placeholder='Nhập giá bìa'
              containerClassName='pl-2 bg-white w-full border rounded-lg flex mb-1'
              className='mr-2 w-full outline-none'
              right={
                <div className='rounded-br-md rounded-tr-md bg-secondary-20/30 py-3 px-3'>
                  <span className='text-xs'>đồng</span>
                </div>
              }
              register={register}
              name='price'
              onKeyDown={(e) => exceptThisSymbols.includes(e.key) && e.preventDefault()}
            />
          </div>
        </div>
      </form>

      <div className='mt-3 flex w-2/3 flex-1 items-center justify-end gap-1'>
        <Button variant='secondary' type='button' onClick={handleBack}>
          Quay về
        </Button>

        <Button type='button' onClick={onSubmit}>
          {Boolean(id) ? 'Cập nhật' : 'Thêm mới'}
        </Button>
      </div>

      <Loading open={loadingAddNewspaper || (loadingMagazineData && Boolean(id)) || loadingUpdateMagazine} />
    </div>
  );
};

export default AddMagazine;
