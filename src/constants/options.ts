export const TEACHER_OPTIONS = [
  { label: '--Chọn số lượng giáo viên', value: '' },
  { label: '30 giáo viên', value: 30 },
  { label: '50 giáo viên', value: 50 },
  { label: '100 giáo viên', value: 100 }
];

export const STUDENT_OPTIONS = [
  { label: '--Chọn số lượng học sinh', value: '' },
  { label: '30 học sinh', value: 30 },
  { label: '50 học sinh', value: 50 },
  { label: '100 học sinh', value: 100 }
];

export const GENDER_OPTIONS = [
  { label: 'Chọn giới tính', value: '' },
  { label: 'Nam', value: 'Nam' },
  { label: 'Nữ', value: 'Nữ' }
];

export const TYPE_READER_OPTIONS = [
  { label: 'Tất cả', value: '' },
  { label: 'Giáo viên', value: 'gv' },
  { label: 'Học sinh', value: 'hs' }
];

export const LOCATION_READER_OPTIONS = [
  { label: 'Tất cả bạn đọc', value: '' },
  { label: 'Thư viện nội bộ', value: '0' },
  { label: 'Thư viện liên thông', value: '1' }
];

export const DAY_OPTIONS = [
  { label: '01', value: '01' },
  { label: '02', value: '02' },
  { label: '03', value: '03' },
  { label: '04', value: '04' },
  { label: '05', value: '05' },
  { label: '06', value: '06' },
  { label: '07', value: '07' },
  { label: '08', value: '08' },
  { label: '09', value: '09' },
  { label: '10', value: '10' },
  { label: '11', value: '11' },
  { label: '12', value: '12' },
  { label: '13', value: '13' },
  { label: '14', value: '14' },
  { label: '15', value: '15' },
  { label: '16', value: '16' },
  { label: '17', value: '17' },
  { label: '18', value: '18' },
  { label: '19', value: '19' },
  { label: '20', value: '20' },
  { label: '21', value: '21' },
  { label: '22', value: '22' },
  { label: '23', value: '23' },
  { label: '24', value: '24' },
  { label: '25', value: '25' },
  { label: '26', value: '26' },
  { label: '27', value: '27' },
  { label: '28', value: '28' },
  { label: '29', value: '29' },
  { label: '30', value: '30' },
  { label: '31', value: '31' }
];

export const DAY30_OPTIONS = [
  { label: '01', value: '01' },
  { label: '02', value: '02' },
  { label: '03', value: '03' },
  { label: '04', value: '04' },
  { label: '05', value: '05' },
  { label: '06', value: '06' },
  { label: '07', value: '07' },
  { label: '08', value: '08' },
  { label: '09', value: '09' },
  { label: '10', value: '10' },
  { label: '11', value: '11' },
  { label: '12', value: '12' },
  { label: '13', value: '13' },
  { label: '14', value: '14' },
  { label: '15', value: '15' },
  { label: '16', value: '16' },
  { label: '17', value: '17' },
  { label: '18', value: '18' },
  { label: '19', value: '19' },
  { label: '20', value: '20' },
  { label: '21', value: '21' },
  { label: '22', value: '22' },
  { label: '23', value: '23' },
  { label: '24', value: '24' },
  { label: '25', value: '25' },
  { label: '26', value: '26' },
  { label: '27', value: '27' },
  { label: '28', value: '28' },
  { label: '29', value: '29' },
  { label: '30', value: '30' }
];

export const DAY28_OPTIONS = [
  { label: '01', value: '01' },
  { label: '02', value: '02' },
  { label: '03', value: '03' },
  { label: '04', value: '04' },
  { label: '05', value: '05' },
  { label: '06', value: '06' },
  { label: '07', value: '07' },
  { label: '08', value: '08' },
  { label: '09', value: '09' },
  { label: '10', value: '10' },
  { label: '11', value: '11' },
  { label: '12', value: '12' },
  { label: '13', value: '13' },
  { label: '14', value: '14' },
  { label: '15', value: '15' },
  { label: '16', value: '16' },
  { label: '17', value: '17' },
  { label: '18', value: '18' },
  { label: '19', value: '19' },
  { label: '20', value: '20' },
  { label: '21', value: '21' },
  { label: '22', value: '22' },
  { label: '23', value: '23' },
  { label: '24', value: '24' },
  { label: '25', value: '25' },
  { label: '26', value: '26' },
  { label: '27', value: '27' },
  { label: '28', value: '28' }
];

export const MONTH_OPTIONS = [
  { label: '01', value: '01' },
  { label: '02', value: '02' },
  { label: '03', value: '03' },
  { label: '04', value: '04' },
  { label: '05', value: '05' },
  { label: '06', value: '06' },
  { label: '07', value: '07' },
  { label: '08', value: '08' },
  { label: '09', value: '09' },
  { label: '10', value: '10' },
  { label: '11', value: '11' },
  { label: '12', value: '12' }
];

export const MONTH31_OPTIONS = [
  { label: '01', value: '01' },
  { label: '03', value: '03' },
  { label: '05', value: '05' },
  { label: '07', value: '07' },
  { label: '08', value: '08' },
  { label: '10', value: '10' },
  { label: '12', value: '12' }
];

export const MONTH30_OPTIONS = [
  { label: '01', value: '01' },
  { label: '03', value: '03' },
  { label: '04', value: '04' },
  { label: '05', value: '05' },
  { label: '06', value: '06' },
  { label: '07', value: '07' },
  { label: '08', value: '08' },
  { label: '09', value: '09' },
  { label: '10', value: '10' },
  { label: '11', value: '11' },
  { label: '12', value: '12' }
];

export const SCHOOL_YEAR_OPTIONS = [
  { label: '2023 - 2024', value: '2023 - 2024' },
  { label: '2022 - 2023', value: '2022 - 2023' },
  { label: '2021 - 2022', value: '2021 - 2022' },
  { label: '2020 - 2021', value: '2020 - 2021' },
  { label: '2019 - 2020', value: '2019 - 2020' },
  { label: '2018 - 2019', value: '2018 - 2019' },
  { label: '2017 - 2018', value: '2017 - 2018' },
  { label: '2016 - 2017', value: '2016 - 2017' },
  { label: '2015 - 2016', value: '2015 - 2016' },
  { label: '2014 - 2015', value: '2014 - 2015' },
  { label: '2013 - 2014', value: '2013 - 2014' }
];

export const SCHOOL_YEAR_OPTIONS_1 = [
  { label: '2023 - 2024', value: '2023 - 2024' },
  { label: '2022 - 2023', value: '2022 - 2023' },
  { label: '2021 - 2022', value: '2021 - 2022' },
  { label: '2020 - 2021', value: '2020 - 2021' },
  { label: '2019 - 2020', value: '2019 - 2020' },
  { label: '2018 - 2019', value: '2018 - 2019' },
  { label: '2017 - 2018', value: '2017 - 2018' },
  { label: '2016 - 2017', value: '2016 - 2017' },
  { label: '2015 - 2016', value: '2015 - 2016' },
  { label: '2014 - 2015', value: '2014 - 2015' },
  { label: '2013 - 2014', value: '2013 - 2014' }
];

export const CLASS_OPTIONS = [
  { label: 'Chọn lớp', value: '' },
  { label: '12A1', value: '12A1' },
  { label: '12A2', value: '22A2' },
  { label: '12A3', value: '12A3' },
  { label: '12A4', value: '12A4' },
  { label: '12A5', value: '12A5' },
  { label: '12A6', value: '12A6' },
  { label: '12A7', value: '12A7' },
  { label: '12A8', value: '12A8' },
  { label: '12A9', value: '12A9' },
  { label: '12A10', value: '12A10' }
];

export const PAGE_SIZE_OPTIONS = [
  { label: '30', value: 30 },
  { label: '50', value: 50 },
  { label: '100', value: 100 }
];

export const CLASSIFY_OPTIONS = [
  {
    label: 'Chọn phân loại',
    value: ''
  },
  {
    label: 'Kiêm nhiệm',
    value: 'Kiêm nhiệm'
  },
  {
    label: 'Chuyên trách',
    value: 'Chuyên trách'
  },
  {
    label: 'Cộng tác viên',
    value: 'Cộng tác viên'
  }
];

export const WORK_YEAR_OPTIONS = [
  { label: 'Chọn năm công tác', value: '' },
  { label: '2023', value: '2023' },
  { label: '2022', value: '2022' },
  { label: '2021', value: '2021' },
  { label: '2020', value: '2020' },
  { label: '2019', value: '2019' },
  { label: '2018', value: '2018' },
  { label: '2017', value: '2017' },
  { label: '2016', value: '2016' },
  { label: '2015', value: '2015' },
  { label: '2014', value: '2014' },
  { label: '2013', value: '2013' },
  { label: '2012', value: '2012' },
  { label: '2011', value: '2011' },
  { label: '2010', value: '2010' }
];

export const YEARS_OPTIONS = [
  { label: '2023', value: '2023' },
  { label: '2022', value: '2022' },
  { label: '2021', value: '2021' },
  { label: '2020', value: '2020' },
  { label: '2019', value: '2019' },
  { label: '2018', value: '2018' },
  { label: '2017', value: '2017' },
  { label: '2016', value: '2016' },
  { label: '2015', value: '2015' },
  { label: '2014', value: '2014' },
  { label: '2013', value: '2013' },
  { label: '2012', value: '2012' },
  { label: '2011', value: '2011' },
  { label: '2010', value: '2010' }
];

export const EUser = {
  Active: 0,
  DeActive: 1, //khoa the
  Deleted: 2
};

export const STATUS_OPTIONS = [
  {
    label: 'Chọn trạng thái',
    value: ''
  },
  {
    label: 'Đang kích hoạt',
    value: EUser.Active
  },
  {
    label: 'Đang bị khóa',
    value: EUser.DeActive
  }
];

export const GRADE_OPTIONS = [
  {
    label: 'Khối 1',
    value: '1'
  },
  {
    label: 'Khối 2',
    value: '2'
  },
  {
    label: 'Khối 3',
    value: '3'
  },
  {
    label: 'Khối 4',
    value: '4'
  },
  {
    label: 'Khối 5',
    value: '5'
  },
  {
    label: 'Khối 6',
    value: '6'
  },
  {
    label: 'Khối 7',
    value: '7'
  },
  {
    label: 'Khối 8',
    value: '8'
  },
  {
    label: 'Khối 9',
    value: '9'
  }
];

export const GRADE_SEARCH_OPTIONS = [
  {
    label: 'Chọn khối lớp',
    value: ''
  },
  {
    label: 'Khối 1',
    value: '1'
  },
  {
    label: 'Khối 2',
    value: '2'
  },
  {
    label: 'Khối 3',
    value: '3'
  },
  {
    label: 'Khối 4',
    value: '4'
  },
  {
    label: 'Khối 5',
    value: '5'
  },
  {
    label: 'Khối 6',
    value: '6'
  },
  {
    label: 'Khối 7',
    value: '7'
  },
  {
    label: 'Khối 8',
    value: '8'
  },
  {
    label: 'Khối 9',
    value: '9'
  }
];

export const OPTIONS_BY_TYPESCHOOL: { [key: string]: { label: string; value: string }[] } = {
  '0_MamnonMaugiao': [] as { label: string; value: string }[],
  '1_TruongCap1': [
    {
      label: 'Khối 1',
      value: '1'
    },
    {
      label: 'Khối 2',
      value: '2'
    },
    {
      label: 'Khối 3',
      value: '3'
    },
    {
      label: 'Khối 4',
      value: '4'
    },
    {
      label: 'Khối 5',
      value: '5'
    }
  ],
  '2_TruongCap2': [
    {
      label: 'Khối 6',
      value: '6'
    },
    {
      label: 'Khối 7',
      value: '7'
    },
    {
      label: 'Khối 8',
      value: '8'
    },
    {
      label: 'Khối 9',
      value: '9'
    }
  ],
  '3_TruongCap3': [
    {
      label: 'Khối 10',
      value: '10'
    },
    {
      label: 'Khối 11',
      value: '11'
    },
    {
      label: 'Khối 12',
      value: '12'
    }
  ]
};

export const VALIDATE_BY_TYPESCHOOL: { [key: string]: { max: number; min: number } } = {
  '0_MamnonMaugiao': { min: Number.MAX_VALUE, max: Number.MIN_VALUE },
  '1_TruongCap1': { min: 1, max: 5 },
  '2_TruongCap2': { min: 6, max: 9 },
  '3_TruongCap3': { min: 10, max: 12 }
};

export const OPERATORS = [
  {
    label: 'Chứa đựng',
    value: 'contains'
  },
  {
    label: 'Bằng',
    value: 'equals'
  },
  {
    label: 'Bắt đầu với',
    value: 'startWiths'
  },
  {
    label: 'Kết thúc với',
    value: 'endWiths'
  },
  {
    label: 'Là rỗng',
    value: 'isEmpty'
  },
  {
    label: 'Là không rỗng',
    value: 'isNotEmpty'
  }
];

export const RENTING_BOOK_STATUSES_OPTIONS = [
  {
    value: 0,
    label: 'Chọn tất cả'
  },
  {
    value: 1,
    label: 'Chưa trả'
  },
  {
    value: 2,
    label: 'Gần trả'
  },
  {
    value: 3,
    label: 'Trả trễ'
  },
  {
    value: 4,
    label: 'Trả đúng hẹn'
  },
  {
    value: 5,
    label: 'Mất sách'
  }
];

export const ACCOUNT_TYPE_OPTIONS = [
  {
    value: '',
    label: 'Chọn loại tài khoản'
  },
  {
    value: 'gv',
    label: 'Giáo viên'
  },
  {
    value: 'hs',
    label: 'Học sinh'
  }
];
export const ACCOUNT_ONLINE_TYPE_OPTIONS = [
  {
    value: 0,
    label: 'Đọc tại chỗ'
  },
  {
    value: 1,
    label: 'Đọc trực tuyến'
  }
];

export const RETURN_TYPE_OPTIONS = [
  {
    value: '',
    label: 'Tất cả'
  },
  {
    value: 0,
    label: 'Chưa trả'
  },
  {
    value: 1,
    label: 'Đã trả'
  }
];

export const KEY_VALIDATION_STATUS_OPTIONS = [
  { value: '', label: 'Chọn trạng thái kết nối' },
  { value: 'Ngắt kết nối', label: 'Ngắt kết nối' },
  { value: 'Đang kết nối', label: 'Đang kết nối' },
  { value: 'Chưa kết nối', label: 'Chưa kết nối' }
];

export const KEY_STATUS_OPTION = [
  { value: '', label: 'Chọn trạng thái' },
  { value: 'active', label: 'Đang hoạt động' },
  { value: 'inactive', label: 'Ngưng hoạt động' }
];
