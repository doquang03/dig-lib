import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { Modal, type ModalProps } from 'antd';
import { bookshelfApis } from 'apis';
import { AxiosError } from 'axios';
import { Button, Input } from 'components';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import { bookshelfSchema } from 'utils/rules';

const initalFormValues: Omit<Bookshelf, 'Id'> = {
  CreateDateTime: '',
  GhiChu: '',
  TenKe: '',
  ViTri: ''
};

type Props = {
  onBack: VoidFunction;
  onSuccess: (bookshelfValue: string) => void;
} & ModalProps;

const BookshelfModal = (props: Props) => {
  const { title, onBack, onSuccess, ...rest } = props;

  const queryClient = useQueryClient();

  const {
    register,
    handleSubmit,
    reset,
    setError,
    setValue,
    formState: { errors }
  } = useForm<typeof initalFormValues>({
    defaultValues: initalFormValues,
    resolver: yupResolver(bookshelfSchema)
  });
  const { mutate: createBookshelf, isLoading: loadingCreateBookshelf } = useMutation({
    mutationFn: bookshelfApis.createBookshelf,
    onSuccess: (data) => {
      queryClient.invalidateQueries(['GetInitCreateForm']);
      toast.success('Tạo kệ sách thành công');
      onSuccess(data.data.Item.id);
      onBack();
      reset();
    },
    onError: (error: AxiosError<ResponseApi<{}>>) => {
      setError('TenKe', { type: 'validate', message: error.response?.data?.Message }, { shouldFocus: true });
    }
  });

  const handleClose = () => {
    reset();
    onBack();
  };

  const onSubmit = handleSubmit((data) => {
    let { TenKe, ViTri } = data;

    TenKe = TenKe.replace(/ +(?= )/g, '');
    ViTri = ViTri.replace(/ +(?= )/g, '');

    setValue('TenKe', TenKe);
    setValue('ViTri', ViTri);

    createBookshelf({
      TenKe,
      ViTri,
      GhiChu: data.GhiChu,
      CreateDateTime: new Date().toISOString()
    });
  });

  return (
    <Modal
      title={<h3 className='font-bold uppercase text-primary-10'>{title}</h3>}
      {...rest}
      footer={null}
      closable={false}
    >
      <form onSubmit={onSubmit}>
        <label className='mt-3 mb-1 font-bold'>
          Tên kệ sách <span className='text-danger-10'>*</span>
        </label>
        <Input
          placeholder='Nhập tên kệ sách'
          name='TenKe'
          register={register}
          errorMessage={errors?.TenKe?.message}
          maxLength={256}
        />

        <label className='mt-4 mb-1 font-bold'>
          Vị trí kệ sách <span className='text-danger-10'>*</span>
        </label>
        <Input
          placeholder='Nhập vị trí kệ sách'
          name='ViTri'
          register={register}
          errorMessage={errors?.ViTri?.message}
          maxLength={256}
        />

        <label className='mt-3 mb-1 font-bold'>Mô tả</label>
        <Input
          component={'textarea'}
          placeholder='Nhập nội dung mô tả'
          name='GhiChu'
          register={register}
          errorMessage={errors?.GhiChu?.message}
        />

        <div className='mt-3 flex items-center justify-end gap-2'>
          <Button variant='secondary' type='button' onClick={handleClose} disabled={loadingCreateBookshelf}>
            Quay về
          </Button>

          <Button variant='default' disabled={loadingCreateBookshelf}>
            Thêm mới
          </Button>
        </div>
      </form>
    </Modal>
  );
};

export default BookshelfModal;
