import { yupResolver } from '@hookform/resolvers/yup';
import { useQuery } from '@tanstack/react-query';
import { Breadcrumb } from 'antd';
import { typeBookApis } from 'apis';
import { Button, Input, Title } from 'components';
import { path } from 'constants/path';
import 'css/ComponentAntD.css';
import { useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import { NavLink, useNavigate, useParams, useSearchParams } from 'react-router-dom';
import http from 'utils/httpThuVien';
import { typeBookSchema } from 'utils/rules';
import * as yup from 'yup';

type TypeBookForm = yup.InferType<typeof typeBookSchema>;
// TODO: Delete as soon as api implement
const initialTypeBookValue = {
  TenTheLoai: '',
  MaTheLoai: '',
  MoTa: ''
};

const AddTypeBookPage = () => {
  const { typeBookId } = useParams();
  const [searchParams] = useSearchParams();
  const parentId = searchParams.get('idParent');
  const [currentParent, setCurrentParent] = useState();
  const [routes, setRoutes] = useState<any>([
    {
      path: path.themtheloaisach,
      breadcrumbName: 'Thể loại sách'
    }
  ]);

  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors }
  } = useForm<TypeBookForm>({
    defaultValues: initialTypeBookValue,
    resolver: yupResolver(typeBookSchema)
  });

  const { isFetching } = useQuery({
    queryKey: ['Sua', typeBookId, parentId],
    queryFn: () => {
      let idSelect;
      if (typeBookId === undefined && parentId === null) {
        setRoutes([
          {
            path: path.themtheloaisach,
            breadcrumbName: 'Thể loại sách'
          }
        ]);
        return null;
      }
      if (typeBookId) idSelect = typeBookId;
      if (parentId) idSelect = parentId;
      let request = typeBookApis.Sua(idSelect as string);
      request.then((res) => {
        let listParent = res?.data?.Item?.ListParent;
        let TLS = res?.data?.Item?.TLS;
        let tempRoutes = [];
        tempRoutes.push({
          path: `/`,
          breadcrumbName: 'Thể loại sách'
        });
        for (let i = 0; i < listParent.length; i++) {
          tempRoutes.push({
            path: `?idParent=${listParent[i].Id}`,
            breadcrumbName: listParent[i].TenTheLoai
          });
        }
        if (parentId) {
          tempRoutes.push({
            path: `?idParent=${parentId}`,
            breadcrumbName: TLS.TenTheLoai
          });
        }

        setRoutes(tempRoutes);

        if (typeBookId) {
          setValue('TenTheLoai', TLS.TenTheLoai);
          setValue('MaTheLoai', TLS.MaTheLoai);
          setValue('MoTa', TLS.MoTa);
          setCurrentParent(TLS.IdParent);
        }
      });
      return request;
    }
    // teacherId === true => excute query function
  });

  const navigate = useNavigate();

  const isDisable = useMemo(() => !!typeBookId && isFetching, [typeBookId, isFetching]);

  const onSubmit = handleSubmit((data) => {
    if (!typeBookId) {
      http
        .post(`/TheLoaiSach/ThemByModel`, {
          IdParent: parentId,
          TenTheLoai: data.TenTheLoai,
          MaTheLoai: data.MaTheLoai,
          MoTa: data.MoTa
        })
        .then(function (response) {
          navigate(path.theloaisach);
        });
    } else {
      if (isFetching) return;
      http
        .post(`/TheLoaiSach/SuaByModel`, {
          Id: typeBookId,
          IdParent: currentParent,
          TenTheLoai: data.TenTheLoai,
          MaTheLoai: data.MaTheLoai,
          MoTa: data.MoTa
        })
        .then(function () {
          navigate(path.theloaisach);
        });
    }
  });

  const handleBack = () => {
    navigate(path.theloaisach);
  };

  function itemRender(route: any, params: any, routes: any, paths: any) {
    const last = routes.indexOf(route) === routes.length - 1;
    return last || typeBookId ? (
      <span>{route.breadcrumbName}</span>
    ) : (
      <NavLink to={paths[paths.length - 1]}>{route.breadcrumbName}</NavLink>
    );
  }

  return (
    <div className='p-5'>
      <Title title={!!typeBookId ? 'CẬP NHẬT THỂ LOẠI SÁCH' : 'THÊM MỚI THỂ LOẠI SÁCH'} />
      <Breadcrumb className='site-page-header my-2 p-2' separator='>' routes={routes} itemRender={itemRender} />
      <form className='ml-0' onSubmit={onSubmit}>
        <div className='mt-1.5 grid grid-cols-1 md:grid-cols-12'>
          <div className='custom-row col-span-5 px-2'>
            <div className='my-2'>
              <label className='mb-3 font-bold'>
                Tên thể loại <span className='text-danger-10'>*</span>
              </label>

              <Input
                placeholder='Nhập tên thể loại'
                name='TenTheLoai'
                register={register}
                errorMessage={errors?.TenTheLoai?.message}
                disabled={isDisable}
              />
            </div>

            <div className='my-2'>
              <label className='mb-3 font-bold'>
                Mã thể loại <span className='text-danger-10'>*</span>
              </label>

              <Input
                placeholder='Nhập mã thể loại'
                name='MaTheLoai'
                register={register}
                errorMessage={errors?.MaTheLoai?.message}
                disabled={isDisable}
              />
            </div>

            <div className='my-2'>
              <label className='mb-3 font-bold'>Mô tả</label>

              <Input placeholder='Nhập nội dung mô tả' name='MoTa' register={register} disabled={isDisable} />
            </div>
          </div>
        </div>
        <div className='mt-5 flex justify-end'>
          <Button type='button' variant='secondary' className='font-semibold' onClick={handleBack}>
            Quay về
          </Button>

          {!!typeBookId ? (
            <>
              <Button variant='default' type='submit' className='ml-2 font-semibold'>
                Cập nhật
              </Button>
            </>
          ) : (
            <Button variant='default' type='submit' className='ml-2 font-semibold'>
              Thêm mới
            </Button>
          )}
        </div>
      </form>
    </div>
  );
};

export default AddTypeBookPage;
