import { useQuery } from '@tanstack/react-query';
import { DatePicker, Table } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { errorSyncDocuments } from 'apis';
import classNames from 'classnames';
import { Button, Input, Select, SizeChanger, Title } from 'components';
import { FORMAT_DATE_PICKER } from 'constants/config';
import { path } from 'constants/path';
import dayjs, { Dayjs } from 'dayjs';
import { useStateAsync } from 'hooks';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import { getSerialNumber } from 'utils/utils';

type Params = {
  page: number;
  pageSize: number;
  searchValue?: string;
  submitedTime?: string;
};

function ErrorSyncDocument() {
  const [params, setParams] = useStateAsync<Params>({
    page: 1,
    pageSize: 30,
    searchValue: '',
    submitedTime: ''
  });

  const { data: errorSyncDocumentsData, refetch } = useQuery({
    queryKey: ['getListOfErrorSyncDocuments', params.page, params.pageSize],
    queryFn: () => {
      const requestBody: { page: number; pageSize: number; TextForSearch?: string; TimeSubmit?: string } = {
        page: 1,
        pageSize: 30
      };

      const { searchValue, submitedTime } = params;

      if (searchValue) {
        requestBody.TextForSearch = searchValue;
      }

      if (submitedTime) {
        requestBody.TimeSubmit = submitedTime;
      }

      return errorSyncDocuments.getData(requestBody);
    }
  });

  const { Count, ListModel } = errorSyncDocumentsData?.data?.Item || { Count: 0, ListModel: [] };

  function handleChangeDate(value: Dayjs | null) {
    setParams((prevState) => ({ ...prevState, submitedTime: value?.toISOString() }));
  }

  function handleSearchSyncer(value: string) {
    setParams((prevState) => ({ ...prevState, searchValue: value }));
  }

  const columns: ColumnsType<ErrorSyncDocument> = [
    {
      key: 'STT',
      title: 'STT',
      dataIndex: 'STT',
      render: (value, record, index) => getSerialNumber(params.page, params.pageSize, index),
      width: 100
    },
    {
      key: 'Số tài liệu đồng bộ',
      title: 'Số tài liệu đồng bộ',
      dataIndex: 'Ten',
      onCell: (record) => ({
        className: 'text-left'
      }),
      ellipsis: true,
      render: (value, { CountDigital, CountPaper }) =>
        `${CountDigital + CountPaper} tài liệu (${CountPaper} tài liệu giấy & ${CountDigital} tài liệu số)`
    },
    {
      key: 'Số tài liệu đồng bộ lỗi',
      title: 'Số tài liệu đồng bộ lỗi',
      dataIndex: 'SoTaiLieuDongBongBiLoi',
      onCell: (record) => ({
        className: 'text-left text-primary-20 hover:under-line'
      }),
      //   render: (value, record) => convertDate(record.ThoiGianTao),
      render: (value, { Id, CountErrorDigital, CountErrorPaper }) => (
        <Link to={`${path.errorSyncDocument}/${Id}`}>
          {CountErrorDigital + CountErrorPaper} tài liệu ({CountErrorPaper} tài liệu giấy & {CountErrorDigital} tài liệu
          số)
        </Link>
      )
    },
    {
      key: 'Người đồng bộ',
      title: 'Người đồng bộ',
      dataIndex: 'UserName'
      //   render: (value, record) => convertDate(record.ThoiGianTao)
    },
    {
      key: 'Thời gian đồng bộ',
      title: 'Thời gian đồng bộ',
      dataIndex: 'CreateDateTime',
      //   render: (value, record) => convertDate(record.ThoiGianTao),
      render: (value, record) => dayjs(record.CreateDateTime).format('HH:mm DD/MM/YYYY')
    }
  ];

  function handleSearch() {
    refetch();
  }

  async function handleRefresh() {
    await setParams({
      page: 1,
      pageSize: 30,
      searchValue: '',
      submitedTime: ''
    });

    await refetch();
  }

  return (
    <main className='mt-3'>
      <Title title='DANH SÁCH LỖI ĐỒNG BỘ TÀI LIỆU' />

      <div className='my-3 flex items-center gap-2 bg-[#3D9BE326] p-2'>
        <Input
          placeholder='Nhập tên người đồng bộ'
          containerClassName='w-full'
          value={params.searchValue || ''}
          onChange={(e) => handleSearchSyncer(e.target.value)}
        />

        <DatePicker
          format={FORMAT_DATE_PICKER}
          className={'w-full border border-gray-300 py-[0.8rem] px-2 outline-none'}
          placeholder='Chọn ngày gửi bản ghi'
          onChange={handleChangeDate}
          value={params?.submitedTime ? dayjs(params.submitedTime) : null}
        />

        <div className='flex w-full items-center gap-1'>
          <Button variant='secondary' onClick={handleRefresh}>
            Làm mới
          </Button>

          <Button onClick={handleSearch}>Tìm kiếm</Button>
        </div>
      </div>

      <Table
        columns={columns}
        className='custom-table mt-4'
        bordered
        dataSource={ListModel}
        rowKey={(row) => row.Id}
        // loading={loadingExpenses}
        pagination={{
          current: params.page || 1,
          pageSize: params.pageSize || 30,
          total: Count,
          onChange: (page: number, pageSize: number) => {
            setParams((prevState) => {
              return {
                ...prevState,
                page,
                pageSize
              };
            });
          },
          hideOnSinglePage: true,
          showSizeChanger: false,
          showQuickJumper: true,
          locale: { jump_to: '', page: '' }
        }}
      />

      {!!Count && (
        <div
          className={classNames('relative', {
            'mt-[86px]': params.pageSize >= Count
          })}
        >
          <div className='absolute bottom-2'>
            <SizeChanger visible={!!Count} value={ListModel.length + ''} total={Count + ''} />
          </div>
        </div>
      )}
    </main>
  );
}

export default ErrorSyncDocument;
