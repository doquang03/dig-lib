import { DownloadOutlined } from '@ant-design/icons';
import { Button, ProgressBar, Title } from 'components';
import { useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';

const AddNewspaperExcel = () => {
  const [step, setStep] = useState<number>(1);
  const [newspaperListing, setNewspaperListing] = useState<Array<NewspaperMagazine>>([]);

  const navigate = useNavigate();

  const fileInutRef = useRef<HTMLInputElement | null>(null);

  const handleOnFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files?.[0];
    // PreviewImport(fileFromLocal as File);
    // setStep(2);
  };

  const handleAccessFileInputRef = () => {
    fileInutRef.current?.click();
  };

  const handleBack = () => {
    if (step > 1) {
      setStep((step) => step--);
      setNewspaperListing([]);
      return;
    }
    navigate(-1);
  };

  return (
    <div>
      <Title title='Thêm mới tạp chí từ file Excel' />

      <div className='mt-10'>
        <ProgressBar activeStep={step} type='excel' />
      </div>

      {step === 1 && (
        <div className='mt-3 flex items-center justify-center rounded-md bg-tertiary-10 py-4'>
          <a href={`/Tempalates/MauGV.xls`} download='MauGV'>
            <Button className='bg-tertiary-20 hover:bg-tertiary-20/30'>
              <DownloadOutlined />
              File excel mẫu
            </Button>
          </a>

          <input
            type='file'
            accept='.csv,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel'
            className='hidden'
            ref={fileInutRef}
            onChange={handleOnFileChange}
          />

          <Button variant='default' className='ml-2' onClick={handleAccessFileInputRef}>
            Chọn tệp
          </Button>
        </div>
      )}

      <div className='mr-10 mt-6 flex items-center justify-end'>
        {step === 1 && (
          <>
            <Button variant='secondary' onClick={handleBack}>
              Quay về
            </Button>
          </>
        )}

        <>
          {step === 2 && (
            <>
              <Button variant='secondary' onClick={handleBack}>
                Quay về
              </Button>
            </>
          )}

          {step === 3 && (
            <Button variant='default' className='ml-2' onClick={handleBack}>
              Hoàn tất
            </Button>
          )}
        </>
      </div>
    </div>
  );
};

export default AddNewspaperExcel;
