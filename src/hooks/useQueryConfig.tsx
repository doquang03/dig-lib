import { isEmpty } from 'lodash';
import omitBy from 'lodash/omitBy';
import useQueryParams from './useQueryParams';
import { string } from 'yup';

export type QueryConfig = {
  [key in keyof SearchQueryParams]: string;
};

const useQueryConfig = (pageSize: string = '30') => {
  const queryParams: QueryConfig = useQueryParams();

  const queryConfig: QueryConfig = omitBy(
    {
      page: queryParams.page || '1',
      pageSize: queryParams.pageSize || pageSize,
      TextForSearch: queryParams.TextForSearch,
      gender: queryParams.gender,
      schoolYear: queryParams.schoolYear,
      status: queryParams.status,
      GioiTinh: queryParams.GioiTinh,
      TrangThai: queryParams.TrangThai,
      PhanLoai: queryParams.PhanLoai,
      OrderBy: queryParams.OrderBy,
      ThuMucSach: queryParams.ThuMucSach,
      KhoiLop: queryParams.KhoiLop,
      TacGia: queryParams.TacGia,
      BST: queryParams.BST,
      MonHoc: queryParams.MonHoc,
      ChuDiem: queryParams.ChuDiem,
      NXB: queryParams.NXB,
      KeSach: queryParams.KeSach,
      TaiLieuGiay: queryParams.TaiLieuGiay,
      IdKho: queryParams.IdKho,
      IsPublish: queryParams.IsPublish,
      Type: queryParams.Type
    },
    isEmpty
  );

  return queryConfig;
};

export default useQueryConfig;

// setValue(key as keyof FormInput, queryConfig[key]);
