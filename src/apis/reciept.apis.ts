import httpThuVien from 'utils/httpThuVien';

const endPointQuanLyNhapKho = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/QuanLyNhapKho`;

export type GetSoNhapKhoVars = {
  page?: number;
  pageSize?: number;
  MaPhieuNhap?: SearchInputField;
  SoLuong?: SearchInputField;
  NgayVaoSo?: SearchRangeField;
  NguoiTao?: SearchInputField;
  ThoiGianTao?: SearchRangeField;
};

const receiptApis = {
  createReceipt: (payload: {
    IdUserAdmin: string;
    UserName: string;
    GhiChuPhieuNhap?: string;
    DanhSachSach: {
      IdSach: string;
      IdKho: string;
      SoLuong: number;
      IdTrangThai: string;
      IdNCC: string;
      SCT?: string;
      SVS?: string;
      NVS: string;
    }[];
  }) =>
    httpThuVien.post(
      `${endPointQuanLyNhapKho}/TaoPhieuNhapSach?IdUserAdmin=${payload.IdUserAdmin}&UserName=${payload.UserName}&GhiChuPhieuNhap=${payload.GhiChuPhieuNhap}`,
      payload.DanhSachSach
    ),
  getReceipts: (payload: GetSoNhapKhoVars) => {
    return httpThuVien.post<ResponseApi<{ List_SoNhapKho: Receipt[]; TotalCount: number }>>(
      `${endPointQuanLyNhapKho}/Get_SoNhapKho`,
      payload
    );
  },
  getReceipt: (id: string, page: string, pageSize: string) =>
    httpThuVien.get<
      ResponseApi<{
        List_NhapKho: ReceiptById[];
        GhiChu: number;
        TotalCount: number;
        List_SoNhapKho: [];
        List_ChiTietNhapKho: [];
        ListKhoSach: [];
      }>
    >(`${endPointQuanLyNhapKho}/ChiTietNhapKho/${id}?page=${page}&pageSize=${pageSize}`),
  cancelReceipt: (id: string) => httpThuVien.get<ResponseApi>(`${endPointQuanLyNhapKho}/DeletePhieu/${id}`),
  downloadFile: (id: string) =>
    httpThuVien.post(
      `${endPointQuanLyNhapKho}/DownloadExcel?id=${id}`,
      {},
      {
        responseType: 'blob'
      }
    ),
  saveSoNhapKho: (payload: {
    ListSoNhapXuatKho: SachCaBiet[];
    IdKho: string;
    IdUserAdmin: string;
    UserName: string;
    GhiChu: string;
  }) =>
    httpThuVien.post(
      `${endPointQuanLyNhapKho}/Save_SoNhapKho?IdUserAdmin=${payload.IdUserAdmin}&UserName=${payload.UserName}&GhiChu=${payload.GhiChu}&IdKho=${payload.IdKho}`,
      payload.ListSoNhapXuatKho
    ),
  PrintSo: (id: string, Mau: string) =>
    httpThuVien.post(
      `${endPointQuanLyNhapKho}/PrintSo?id=${id}&Mau=${Mau}`,
      {},
      {
        responseType: 'blob'
      }
    )
};

export default receiptApis;
