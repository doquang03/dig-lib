import dayjs, { Dayjs } from 'dayjs';
import { Dispatch, ReactNode, createContext, useState } from 'react';
import { convertDate } from 'utils/utils';

type ContextValue = {
  auditName: string;
  storeId: string;
  createDateReport: Dayjs;
  createDateFinalize: Dayjs;
  ghiChu?: string;
  selectedBooks: BookAudit[];
  setSelectedBooks: Dispatch<React.SetStateAction<BookAudit[]>>;
  setAudit: Dispatch<
    React.SetStateAction<{
      auditName: string;
      storeId: string;
      createDateReport: Dayjs;
      createDateFinalize: Dayjs;
      ghiChu?: string | undefined;
    }>
  >;
};

export const AuditContext = createContext<ContextValue>({} as ContextValue);

export const SelectedBooksAuditProvider = ({ children }: { children: ReactNode }) => {
  const [audit, setAudit] = useState<{
    auditName: string;
    storeId: string;
    createDateReport: Dayjs;
    createDateFinalize: Dayjs;
    ghiChu?: string;
  }>({
    auditName: 'Phiếu kiểm kê tài sản - Ngày ' + convertDate(new Date()),
    storeId: '',
    createDateReport: dayjs(),
    createDateFinalize: dayjs()
  });

  const [selectedBooks, setSelectedBooks] = useState<BookAudit[]>([]);

  return (
    <AuditContext.Provider
      value={{
        auditName: audit.auditName,
        storeId: audit.storeId,
        createDateFinalize: audit.createDateFinalize,
        createDateReport: audit.createDateReport,
        ghiChu: audit.ghiChu,
        setAudit,
        selectedBooks,
        setSelectedBooks
      }}
    >
      {children}
    </AuditContext.Provider>
  );
};
