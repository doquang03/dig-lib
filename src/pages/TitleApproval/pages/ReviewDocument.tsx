import { useMutation, useQuery } from '@tanstack/react-query';
import { Modal, Tooltip } from 'antd';
import { approvalApis } from 'apis';
import { ExtensionFile } from 'apis/approval.api';
import classNames from 'classnames';
import { Button, Title } from 'components';
import { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { RejectionModal } from '../components';
import { Revert, RightArrow } from '../icons';

const labels = [
  '020 # # a ISBN',
  '020 # # c Giá bìa',
  '041 0 # a Tên ngôn ngữ',
  '044 # # a Xuất xứ',
  '082 0 4 a Mã DDC',
  '084 0 4 a Mã Dãy 19',
  '100 1 # a Tác giả',
  '245 1 0 a Tên sách',
  '250 # # a Tái bản',
  '260 # # b Nhà xuất bản',
  '260 # # c Năm xuất bản',
  '300 # # a Số trang',
  '490 0 # a Tùng thư',
  '500 # # a Phụ chú',
  '520 # # a Tóm tắt',
  '740 0 # a Tiêu đề bổ sung',
  '856 # # u Liên kết (url)'
];

const ReviewDocument = () => {
  const {
    state: { schoolName = '', selectedRecordId = '', bookId = '' }
  } = useLocation() || { state: {} };

  const navigate = useNavigate();

  const [dataToMerge, setDataToMerge] = useState<string[]>([]);
  const [listOfFilesToMerge, setListOfFilesToMerge] = useState<ExtensionFile[]>([]);
  const [bookCoverToMerge, setBookCoverToMerge] = useState<ExtensionFile>();

  const [dataFromHost, setDataFromHost] = useState<string[]>([
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    ''
  ]);
  const [listOfFilesFromHost, setListOfFilesFromHost] = useState<ExtensionFile[]>([]);
  const [bookCoverFromHost, setBookCoverFromHost] = useState<ExtensionFile>();

  const [editedIndex, setEditedIndex] = useState<number[]>([]);
  const [visibleModal, setVisibleModal] = useState<'rejection' | 'approval'>();
  const [activeTab, setActiveTab] = useState<'info' | 'marc21'>('info');
  const [fileMerging, setFileMerging] = useState<Array<'bookCover' | 'file'>>([]);

  const { data: dataToMergeData, isLoading: loadingBookDataToMerge } = useQuery({
    queryKey: ['getBookDataToMerge', selectedRecordId],
    queryFn: () => approvalApis.getBookDataToMerge(selectedRecordId),
    enabled: !!selectedRecordId,
    onSuccess: (data) => {
      const { ListParam, LinkBiaSach, ListMedia } = data.data.Item;

      setDataToMerge(ListParam);
      setListOfFilesToMerge(ListMedia);
      setBookCoverToMerge(LinkBiaSach);
    }
  });

  const { data: existedDataAtHost, isLoading: loadingExistedDataAtHost } = useQuery({
    queryKey: ['getDataExistedAtHost', bookId],
    queryFn: () => approvalApis.getExistedBookDataAtHost(bookId),
    enabled: !!bookId,
    onSuccess: (data) => {
      const { LinkBiaSach, ListMedia, ListParam } = data.data.Item;
      setDataFromHost(ListParam);
      setListOfFilesFromHost(ListMedia);
      setBookCoverFromHost(LinkBiaSach);
    }
  });

  const schoolData = dataToMergeData?.data.Item.ListParam || [];
  const hostData = existedDataAtHost?.data.Item.ListParam || [];

  const { mutate, isLoading } = useMutation({
    mutationFn: () =>
      approvalApis.approveDocument({
        IdBook: bookId,
        IdSubmit: selectedRecordId,
        ListParam: dataFromHost,
        LinkBiaSach: bookCoverFromHost?.LinkFile || '',
        ListMedia: listOfFilesFromHost.map((x) => x.LinkFile)
      }),
    onSuccess: () => {
      toast.success(`Phê duyệt thành công biểu ghi`);
      setVisibleModal(undefined);
      navigate(-1);
    }
  });

  useEffect(() => {
    if (!schoolName || !selectedRecordId) navigate(-1);
  }, [schoolName, selectedRecordId]);

  const handleMerge = (index: number) => () => {
    setDataFromHost(
      dataFromHost.map((ahaha, _index) => {
        if (_index === index) {
          return (ahaha = dataToMerge[index]);
        }

        return ahaha;
      })
    );

    setEditedIndex((prevState) => [...prevState, index]);
  };

  const fileSizeToMerge = listOfFilesToMerge.reduce((total, file) => (total += file.FileSize), 0);
  const fileSizeFromHost = listOfFilesFromHost.reduce((total, file) => (total += file.FileSize), 0);

  const handleMergeAll = () => {
    let tempArray: number[] = [];

    dataToMerge.forEach((item, index) => {
      if (dataToMerge[index] !== dataFromHost[index] || schoolData[index] !== hostData[index]) {
        tempArray.push(index);
      }
    });

    setEditedIndex(tempArray);

    setDataFromHost(dataToMerge);

    setBookCoverFromHost(bookCoverToMerge);

    setListOfFilesFromHost(listOfFilesToMerge || []);

    fileSizeToMerge !== fileSizeFromHost &&
      listOfFilesToMerge.length &&
      setFileMerging((prevState) => [...prevState, 'file']);

    !!bookCoverToMerge && setFileMerging((prevState) => [...prevState, 'bookCover']);
  };

  const handleUndo = (index: number) => () => {
    setDataFromHost(
      dataFromHost.map((x, _index) => {
        if (_index === index) {
          return (x = existedDataAtHost?.data?.Item?.ListParam?.[index] || '');
        }

        return x;
      })
    );

    setEditedIndex(editedIndex.filter((a) => a !== index));
  };

  return (
    <div>
      <Title title='PHÊ DUYỆT BẢN GHI' />

      <div className='mt-4 grid grid-cols-12'>
        <p className='col-span-6 text-right text-[20px] text-primary-10'>{schoolName}</p>

        <Tooltip title='Chuyển đổi tất cả' className='col-span-1 grid h-auto place-items-center'>
          <button
            onClick={handleMergeAll}
            disabled={(loadingBookDataToMerge && !!selectedRecordId) || (loadingExistedDataAtHost && !!bookId)}
          >
            <RightArrow />
          </button>
        </Tooltip>

        <p className='col-span-5 text-[20px] text-primary-10'>Dữ liệu thuộc Mục lục liên hợp</p>
      </div>

      {labels.map((lable, index) => {
        return (
          <div className='grid grid-cols-12 gap-2 space-y-3' key={index}>
            <div className='col-span-2 grid h-auto place-items-center'>
              <p className='w-[25rem] shrink whitespace-pre-wrap'>{lable}</p>
            </div>

            <div className='col-span-4'>
              <div
                className={`broder flex min-h-[40px] w-full items-center rounded-md border-[1px]  px-2 ${
                  dataToMerge[index] === dataFromHost[index] || schoolData[index] === hostData[index]
                    ? 'border-tertiary-30 bg-[#E5F4ED]'
                    : 'border-danger-10 bg-[#FCEEEE]'
                } `}
              >
                {dataToMerge[index]}
              </div>
            </div>

            <div className='col-span-1 flex items-center justify-center'>
              <button
                onClick={handleMerge(index)}
                disabled={(loadingBookDataToMerge && selectedRecordId) || (loadingExistedDataAtHost && bookId)}
              >
                {dataFromHost[index] !== dataToMerge[index] && <RightArrow />}
              </button>
            </div>

            <div className='broder  col-span-4 flex min-h-[40px] w-full items-center rounded-md border-[1px] border-[#4C514959] bg-[#F5F5F5] px-2'>
              <p className='flex-grow'>{dataFromHost[index]}</p>

              {/* trường và host 1 trong hai phải có data thì mới hiện nút revert */}
              {(!!schoolData[index] || !!hostData[index]) && (
                <>
                  {dataFromHost[index] === dataToMerge[index] && editedIndex.includes(index) && (
                    <button onClick={handleUndo(index)}>
                      <Revert />
                    </button>
                  )}
                </>
              )}
            </div>
          </div>
        );
      })}

      {/* cover image  */}
      <div className='grid grid-cols-12 gap-2 space-y-3'>
        <div className='col-span-2 grid h-auto place-items-center'>
          <p className='w-[25rem] shrink whitespace-pre-wrap'>Ảnh bìa</p>
        </div>

        <div className='col-span-4'>
          <a
            className={`broder flex min-h-[40px] w-full items-center rounded-md border-[1px] px-2  text-primary-10  ${
              bookCoverToMerge?.FileSize === bookCoverFromHost?.FileSize
                ? 'border-tertiary-30 bg-[#E5F4ED] hover:underline'
                : 'border-danger-10 bg-[#FCEEEE]'
            } `}
            target='_blank'
            rel='noreferrer'
            href={bookCoverToMerge?.LinkFile}
          >
            {bookCoverToMerge?.FileName || '--'}
          </a>
        </div>

        <div className='col-span-1 flex items-center justify-center'>
          {!!bookCoverToMerge && bookCoverToMerge.FileSize !== bookCoverFromHost?.FileSize && (
            <button
              onClick={() => {
                setFileMerging((prevState) => [...prevState, 'bookCover']);
                setBookCoverFromHost(bookCoverToMerge);
              }}
              disabled={(loadingBookDataToMerge && !!selectedRecordId) || (loadingExistedDataAtHost && !!bookId)}
            >
              <RightArrow />
            </button>
          )}
        </div>

        <div className='broder col-span-4 flex min-h-[40px] w-full items-center rounded-md border-[1px] px-2  '>
          <a
            className={`flex-grow ${bookCoverFromHost?.LinkFile ? 'text-primary-10 hover:underline' : ''} `}
            target='_blank'
            rel='noreferrer'
            href={bookCoverFromHost?.LinkFile}
          >
            {bookCoverFromHost?.FileName || '--'}
          </a>

          {fileMerging.includes('bookCover') && bookCoverToMerge?.FileSize === bookCoverFromHost?.FileSize && (
            <button
              onClick={() => {
                setFileMerging(fileMerging.filter((x) => x !== 'bookCover'));
                setBookCoverFromHost(existedDataAtHost?.data.Item.LinkBiaSach);
              }}
            >
              <Revert />
            </button>
          )}
        </div>
      </div>

      {/* files  */}
      <div className='grid grid-cols-12 gap-2 space-y-3'>
        <div className='col-span-2 grid h-auto place-items-center'>
          <p className='w-[25rem] shrink whitespace-pre-wrap'>File</p>
        </div>

        <div
          className={`broder col-span-4 min-h-[40px] w-full items-center rounded-md border-[1px] px-2  text-primary-10 ${
            fileSizeToMerge === fileSizeFromHost ? 'border-tertiary-30 bg-[#E5F4ED]' : 'border-danger-10 bg-[#FCEEEE]'
          } `}
        >
          <div className='flex-grow'>
            {listOfFilesToMerge.length
              ? listOfFilesToMerge.map((file, index) => (
                  <a
                    href={file.LinkFile}
                    target='_blank'
                    rel='noreferrer'
                    className='items-center hover:underline'
                    key={file.PageNumber}
                  >
                    {file.FileName}
                    {listOfFilesToMerge.length - 1 === index ? '.' : ', '}
                  </a>
                ))
              : '--'}
          </div>
        </div>

        <div className='col-span-1 flex items-center justify-center'>
          {fileSizeToMerge !== fileSizeFromHost && (
            <button
              onClick={() => {
                setFileMerging((prevState) => [...prevState, 'file']);
                setListOfFilesFromHost(listOfFilesToMerge);
              }}
              disabled={(loadingBookDataToMerge && !!selectedRecordId) || (loadingExistedDataAtHost && !!bookId)}
            >
              <RightArrow />
            </button>
          )}
        </div>

        <div className='broder col-span-4 flex min-h-[40px] w-full items-center gap-1 rounded-md border-[1px] px-2  text-primary-10'>
          <div className='flex-grow'>
            {listOfFilesFromHost.length
              ? listOfFilesFromHost.map((file, index) => (
                  <a
                    href={file.LinkFile}
                    target='_blank'
                    rel='noreferrer'
                    className=' flex-grow whitespace-pre-wrap hover:underline'
                    key={index}
                  >
                    {file.FileName}
                    {listOfFilesFromHost.length - 1 === index ? '.' : ', '}
                  </a>
                ))
              : '--'}
          </div>

          {fileMerging.includes('file') && fileSizeToMerge === fileSizeFromHost && (
            <button
              className='flex items-end justify-end'
              onClick={() => {
                setFileMerging(fileMerging.filter((x) => x !== 'file'));
                setListOfFilesFromHost(existedDataAtHost?.data.Item.ListMedia || []);
              }}
            >
              <Revert />
            </button>
          )}
        </div>
      </div>

      <div className='my-3 flex items-center justify-end gap-2'>
        <Button variant='secondary' onClick={() => navigate(-1)}>
          Quay về
        </Button>

        {editedIndex.length || fileMerging.length ? (
          <Button
            onClick={() => {
              if (!dataFromHost[7]) {
                return toast.error('Hãy chuyển Tên sách');
              }

              setVisibleModal('approval');
            }}
            variant={!dataFromHost[7] ? 'disabled' : 'default'}
            disabled={!dataFromHost[7]}
          >
            Phê duyệt
          </Button>
        ) : (
          <Button variant='danger' onClick={() => setVisibleModal('rejection')}>
            Từ chối
          </Button>
        )}
      </div>

      {/* approval modal */}
      <Modal open={visibleModal === 'approval'} centered closable={false} footer={null} width={800}>
        <Title title='Xem trước bản ghi' />

        <div className='grid grid-cols-12 gap-2'>
          <div className='relative col-span-4 h-[100px] w-full pt-[100%]'>
            <img
              src={bookCoverFromHost?.LinkFile || '/content/Book.png'}
              alt={bookCoverFromHost?.FileName}
              className='absolute top-0 left-0 h-full w-full bg-white object-cover p-2'
            />
          </div>

          <div className='col-span-8'>
            <h2 className='font-bold text-primary-10'>{dataFromHost[7]}</h2>
            <div className='mt-3 flex items-center gap-3'>
              <button
                className={classNames(
                  `grid w-[120px] place-items-center rounded-md py-3 shadow-md ${
                    activeTab === 'info' ? 'bg-[#EFF4F8] font-bold text-primary-10' : 'bg-white text-[#A7A7A7]'
                  }`
                )}
                onClick={() => setActiveTab('info')}
              >
                Thông tin sách
              </button>

              <button
                className={classNames(
                  `grid w-[120px] shrink-0 place-items-center whitespace-nowrap rounded-md py-3 shadow-md ${
                    activeTab === 'marc21' ? 'bg-[#EFF4F8] font-bold text-primary-10 ' : 'bg-white text-[#A7A7A7]'
                  }`
                )}
                onClick={() => setActiveTab('marc21')}
              >
                Biểu ghi Marc21
              </button>
            </div>

            {activeTab === 'info' && (
              <div className='my-3 grid grid-cols-12'>
                <div className='col-span-6 grid grid-cols-12 space-y-1'>
                  <label className='col-span-6 font-bold'>Tác giả: </label>
                  <p className='col-span-6 truncate text-left text-primary-10'>{dataFromHost[6] || '--'}</p>

                  <label className='col-span-6 font-bold'>ISBN: </label>
                  <p className='col-span-6 truncate text-left text-primary-10'>{dataFromHost[0] || '--'}</p>

                  <label className='col-span-6 font-bold'>Ngôn ngữ: </label>
                  <p className='col-span-6 truncate text-left text-primary-10'>{dataFromHost[2] || '--'}</p>

                  <label className='col-span-6 font-bold'>Năm xuất bản: </label>
                  <p className='col-span-6 truncate text-left text-primary-10'>{dataFromHost[10] || '--'}</p>

                  <label className='col-span-6 font-bold'>Nhà xuất bản: </label>
                  <p className='col-span-6 truncate text-left text-primary-10'>{dataFromHost[9] || '--'}</p>
                </div>

                <div className='col-span-6 grid grid-cols-12 space-y-1'>
                  <label className='col-span-6 font-bold'>Lần xuất bản: </label>
                  <p className='col-span-6 truncate text-left text-primary-10'>{dataFromHost[8] || '--'}</p>

                  <label className='col-span-6 font-bold'>Nơi xuất bản: </label>
                  <p className='col-span-6 truncate text-left text-primary-10'>{'--'}</p>

                  <label className='col-span-6 font-bold'>Giá bìa: </label>
                  <p className='col-span-6 truncate text-left text-primary-10'>
                    {dataFromHost[1] ? dataFromHost[1]?.replace(/\B(?=(\d{3})+(?!\d))/g, ',') + ' đồng' : '--'}
                  </p>

                  <label className='col-span-6 font-bold'>Mô tả vật lý: </label>
                  <p className='col-span-6 truncate text-left text-primary-10'>{'--'}</p>

                  <label className='col-span-6 font-bold'>Khổ mẫu: </label>
                  <p className='col-span-6 truncate text-left text-primary-10'>{'--'}</p>
                </div>
              </div>
            )}

            {activeTab === 'marc21' && (
              <p className='mt-2 whitespace-pre-wrap'>
                {existedDataAtHost?.data?.Item?.Marc21 || 'Không có biểu ghi 21'}
              </p>
            )}
          </div>
        </div>

        <div className='flex items-center justify-end gap-2'>
          <Button variant='secondary' onClick={() => setVisibleModal(undefined)} loading={isLoading}>
            Quay về
          </Button>

          <Button onClick={() => mutate()} loading={isLoading}>
            Xác nhận
          </Button>
        </div>
      </Modal>

      {/* rejection modal */}
      <RejectionModal
        open={visibleModal === 'rejection'}
        onCancel={() => setVisibleModal(undefined)}
        params={{
          IdBook: bookId,
          IdSubmit: selectedRecordId,
          ListParam: dataFromHost,
          LinkBiaSach: bookCoverFromHost?.LinkFile || '',
          ListMedia: listOfFilesFromHost.map((x) => x.LinkFile)
        }}
      />
    </div>
  );
};

export default ReviewDocument;
