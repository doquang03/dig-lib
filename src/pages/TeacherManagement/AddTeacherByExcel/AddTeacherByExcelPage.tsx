import { DeleteFilled, DownloadOutlined } from '@ant-design/icons';
import { useMutation } from '@tanstack/react-query';
import { Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { teacherApis } from 'apis';
import { Button, Loading, ProgressBar, Title } from 'components';
import { path } from 'constants/path';
import 'css/AddTeacherByExcelPage.css';
import { useMemo, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { saveByteArray } from 'utils/utils';

type GiaoVien = {
  ChucVu: string;
  DiaChi: string;
  GioiTinh: string;
  MaSoThanhVien: string;
  NgaySinh: string;
  SDT: string;
  Ten: string;
  STT: number;
  actions: string;
};

const AddTeacherByExcelPage = () => {
  const [step, setStep] = useState<number>(1);
  // Mock state.
  const [listGiaoVienExcel, setListGiaoVienExcel] = useState<Array<GiaoVien>>([]);
  const [listSuccess, setListSuccess] = useState([]);
  const [listFail, setlistFail] = useState([]);
  const [tempFile, setTempFile] = useState<File>();
  const [cellError, setCellError] = useState<Array<string>>([]);
  const [page, setPage] = useState(1);
  const [paginationSize, setPaginationSize] = useState(10);

  const fileInutRef = useRef<HTMLInputElement | null>(null);
  const ref = useRef<HTMLDivElement | null>(null);

  const navigate = useNavigate();

  const { mutate: PreviewImport, isLoading: isLoadingPreviewImport } = useMutation({
    mutationFn: (file: File) => {
      let bodyFormData = new FormData();
      bodyFormData.append('file', file as Blob);
      return teacherApis.PreviewImport(bodyFormData);
    },
    onSuccess: (res) => {
      let tempData = res?.data?.Item?.RawDataList;
      let result: Array<GiaoVien> = [];
      for (let i = 0; i < tempData.length; i++) {
        result.push({
          STT: i,
          Ten: tempData[i][1],
          MaSoThanhVien: tempData[i][2] || '',
          GioiTinh: tempData[i][3] || '',
          NgaySinh: tempData[i][4] || '',
          ChucVu: tempData[i][5] || '',
          DiaChi: tempData[i][6] || '',
          SDT: tempData[i][7] || '',
          actions: ''
        });
      }
      setListGiaoVienExcel(result);
    }
  });

  const { mutate: ImportSave, isLoading: isLoadingImportSave } = useMutation({
    mutationFn: (payload: Array<string>) => teacherApis.ImportSave(payload),
    onSuccess: (res) => {
      let tempData = res?.data?.Item;
      setListSuccess(tempData?.ListSuccess);
      setlistFail(tempData?.ListFail);
      setTempFile(tempData?.MemoryStream);
      setCellError(tempData?.CellError);
      let listShow = tempData.ListShow;
      let resultFinal: Array<GiaoVien> = [];
      let tempFor: GiaoVien;
      for (let i = 0; i < listShow.length; i++) {
        tempFor = {
          STT: listShow[i][0] - 1,
          Ten: listShow[i][1],
          MaSoThanhVien: listShow[i][2],
          GioiTinh: listShow[i][3],
          NgaySinh: listShow[i][4],
          ChucVu: listShow[i][5],
          DiaChi: listShow[i][6],
          SDT: listShow[i][7],
          actions: listShow[i][8]
        };
        resultFinal.push(tempFor);
      }
      setListGiaoVienExcel(resultFinal);
    }
  });
  const handleSubmit = () => {
    // TODO: Integrate api to submit data
    let tempData;
    let result = [];
    for (let i = 0; i < listGiaoVienExcel.length; i++) {
      tempData = [];
      tempData[0] = i;
      for (let param in listGiaoVienExcel[i]) {
        switch (param) {
          case 'Ten':
            tempData[1] = listGiaoVienExcel[i].Ten;
            break;
          case 'MaSoThanhVien':
            tempData[2] = listGiaoVienExcel[i].MaSoThanhVien;
            break;
          case 'GioiTinh':
            tempData[3] = listGiaoVienExcel[i].GioiTinh;
            break;
          case 'NgaySinh':
            tempData[4] = listGiaoVienExcel[i].NgaySinh;
            break;
          case 'ChucVu':
            tempData[5] = listGiaoVienExcel[i].ChucVu;
            break;
          case 'DiaChi':
            tempData[6] = listGiaoVienExcel[i].DiaChi;
            break;
          case 'SDT':
            tempData[7] = listGiaoVienExcel[i].SDT;
            break;
        }
      }
      result.push(JSON.stringify(tempData));
    }
    ImportSave(result);
    ref.current?.scrollIntoView({ behavior: 'smooth' });
    setStep(3);
  };

  const columns: ColumnsType<GiaoVien> = useMemo(() => {
    const handleDeleteRow = (STT: number) => {
      let result: Array<GiaoVien> = [];
      for (let i = 0; i < listGiaoVienExcel.length; i++) {
        if (STT === listGiaoVienExcel[i].STT) {
          continue;
        }
        result.push(listGiaoVienExcel[i]);
      }

      if (paginationSize * (page - 1) >= result.length) {
        setPage(page - 1);
      }

      setListGiaoVienExcel(result);
    };
    return [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        render: (value: any, record: any, j: number) => (page - 1) * paginationSize + j + 1
      },
      {
        title: 'Tên giáo viên',
        dataIndex: 'Ten',
        key: 'Ten',
        onCell: (record) => ({
          className:
            step === 3 && cellError[record.STT] && cellError[record.STT].indexOf('1') !== -1
              ? 'min-h-38 errorBackGround py-2'
              : ''
        })
      },
      {
        title: 'Mã giáo viên',
        dataIndex: 'MaSoThanhVien',
        key: 'MaSoThanhVien',
        onCell: (record) => ({
          className:
            step === 3 && cellError[record.STT] && cellError[record.STT].indexOf('2') !== -1
              ? 'min-h-38 errorBackGround py-2'
              : ''
        })
      },
      {
        title: 'Tổ',
        dataIndex: 'ChucVu',
        key: 'ChucVu',
        onCell: (record) => ({
          className:
            step === 3 && cellError[record.STT] && cellError[record.STT].indexOf('5') !== -1
              ? 'min-h-38 errorBackGround py-2'
              : ''
        })
      },
      {
        title: 'Ngày sinh',
        dataIndex: 'NgaySinh',
        key: 'NgaySinh',
        onCell: (record) => ({
          className:
            step === 3 && cellError[record.STT] && cellError[record.STT].indexOf('4') !== -1
              ? 'min-h-38 errorBackGround py-2'
              : ''
        })
      },
      {
        title: 'Giới tính',
        dataIndex: 'GioiTinh',
        key: 'GioiTinh',
        onCell: (record) => ({
          className:
            step === 3 && cellError[record.STT] && cellError[record.STT].indexOf('3') !== -1
              ? 'min-h-38 errorBackGround py-2'
              : ''
        })
      },
      {
        title: 'Số điện thoại',
        dataIndex: 'SDT',
        key: 'SDT',
        onCell: (record) => ({
          className:
            step === 3 && cellError[record.STT] && cellError[record.STT].indexOf('7') !== -1
              ? 'min-h-38 errorBackGround py-2'
              : ''
        })
      },
      {
        title: (step === 2 && 'Hành động') || (step === 3 && 'Nội dung lỗi'),
        dataIndex: 'actions',
        key: 'actions',
        render: (value, record) => {
          return (
            <>
              {step === 2 && (
                <button
                  className='mx-2'
                  onClick={(e) => {
                    e.stopPropagation();
                    handleDeleteRow(record.STT);
                  }}
                >
                  <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
                </button>
              )}
              {step === 3 && <div style={{ color: 'red' }}>{record.actions}</div>}
            </>
          );
        }
      }
    ];
  }, [cellError, listGiaoVienExcel, page, paginationSize, step]);

  const handleOnFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files?.[0];
    PreviewImport(fileFromLocal as File);
    setStep(2);
  };

  const handleAccessFileInputRef = () => {
    fileInutRef.current?.click();
  };

  const handleBack = () => {
    navigate(path.giaovien);
  };

  const handleBackCurrent = () => {
    setListGiaoVienExcel([]);
    setStep(1);
  };

  return (
    <div className='p-5' ref={ref}>
      <Title title='Thêm mới giáo viên từ file Excel' />

      <div className='my-10'>
        <ProgressBar activeStep={step} type='excel' />
      </div>

      {/* TODO: Condition are step > 1 and data is not undefined*/}
      {step === 1 && (
        <div className='flex items-center justify-center rounded-md bg-tertiary-10 py-4'>
          <a href={`/Tempalates/MauGV.xls`} download='MauGV'>
            <Button className='bg-tertiary-20 hover:bg-tertiary-20/30'>
              <DownloadOutlined />
              File excel mẫu
            </Button>
          </a>

          <input
            type='file'
            accept='.csv,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel'
            className='hidden'
            ref={fileInutRef}
            onChange={handleOnFileChange}
          />

          <Button variant='default' className='ml-2' onClick={handleAccessFileInputRef}>
            Chọn tệp
          </Button>
        </div>
      )}

      {step > 1 && (
        <div>
          {step === 2 && (
            <>
              <p>
                Tổng số giáo viên: <span className='font-bold'>{listGiaoVienExcel?.length}</span>
              </p>
              {listGiaoVienExcel?.length > 0 && <h3 className='text-primary-10'>DANH SÁCH GIÁO VIÊN</h3>}
            </>
          )}

          {step === 3 && (
            <>
              <p className='text-tertiary-30'>
                Lưu thành công: <span className='font-bold'> {' ' + listSuccess.length} giáo viên</span>
              </p>

              <p className='text-danger-10'>
                Lưu thất bại: <span className='font-bold'> {' ' + listFail.length} giáo viên</span>{' '}
                {!!listFail.length && (
                  <Button
                    variant='danger'
                    className='ml-2'
                    style={{ display: 'inline-block' }}
                    onClick={() => {
                      saveByteArray(tempFile, 'DsGiaoVienBiLoi.xls', 'application/xls');
                    }}
                  >
                    <DownloadOutlined />
                    Tải file lỗi
                  </Button>
                )}
              </p>

              {!!listFail.length && <h3 className='uppercase text-primary-10'>Danh sách giáo viên bị lỗi</h3>}
            </>
          )}

          {((step === 2 && !!listGiaoVienExcel?.length) || (step === 3 && listFail.length > 0)) && (
            <>
              <div className='mt-6'>
                <Table
                  loading={isLoadingPreviewImport && isLoadingImportSave}
                  columns={columns}
                  pagination={{
                    onChange(current, pageSize) {
                      setPage(current);
                      setPaginationSize(pageSize);
                    },
                    defaultPageSize: 10,
                    hideOnSinglePage: true,
                    showSizeChanger: false
                  }}
                  dataSource={listGiaoVienExcel}
                  scroll={{ x: 980 }}
                  rowKey={(record) => record.STT}
                  className='custom-table'
                  bordered
                />
              </div>
            </>
          )}
        </div>
      )}

      <div className='mr-10 mt-6 flex items-center justify-end'>
        {step === 1 && (
          <>
            <Button variant='secondary' onClick={handleBack}>
              Quay về
            </Button>
          </>
        )}

        <>
          {step === 2 && !isLoadingPreviewImport && (
            <>
              <Button variant='secondary' onClick={handleBackCurrent}>
                Quay về
              </Button>
              {listGiaoVienExcel?.length > 0 && (
                <Button variant='default' className='ml-2' onClick={handleSubmit}>
                  Tiếp tục
                </Button>
              )}
            </>
          )}

          {step === 3 && !isLoadingImportSave && (
            <Button variant='default' className='ml-2' onClick={handleBack}>
              Hoàn tất
            </Button>
          )}
        </>
      </div>
      <Loading open={isLoadingPreviewImport || isLoadingImportSave} />
    </div>
  );
};

export default AddTeacherByExcelPage;
