import { useQuery } from '@tanstack/react-query';
import { day19Apis } from 'apis';
import classNames from 'classnames';
import { Button, Loading, SelectSearchForm, Title } from 'components';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import { usePaginationNavigate, useQueryConfig } from 'hooks';
import { useMemo } from 'react';
import { Link, useNavigate } from 'react-router-dom';

const Day19ManagementPage = () => {
  const navigate = useNavigate();

  const queryConfig = useQueryConfig();

  const refresh = usePaginationNavigate();

  const { isAllowedAdjustment } = useUser();

  const { data: day19Data, isLoading } = useQuery({
    queryKey: ['day19s', queryConfig.OrderBy, queryConfig.TextForSearch],
    queryFn: () =>
      day19Apis.getDay19List({
        Keyword: queryConfig.TextForSearch,
        Sort: Number(queryConfig?.OrderBy) || 0
      })
  });

  const ddc = useMemo(() => {
    if (!day19Data?.data.Item) {
      return;
    }

    return day19Data?.data?.Item;
  }, [day19Data?.data?.Item]);

  const handleAddDDC = () => navigate(path.themDay19);
  const handleSearchByName = () => navigate(path.bangTraCuuTen);
  const handleRefresh = () => refresh();

  return (
    <div className='p-5'>
      <Title title='Danh sách phân loại 19 dãy' />

      <div className='mt-2'>
        <SelectSearchForm
          placeholder='Nhập tên, mã phân loại 19 dãy'
          items={[
            { label: 'Sắp xếp theo', value: '' },
            { label: 'Mã 19 dãy tăng dần', value: 1 },
            { label: 'Mã 19 dãy giảm dần', value: 2 },
            { label: 'Tên 19 dãy tăng dần', value: 3 },
            { label: 'Tên 19 dãy giảm dần', value: 4 }
          ]}
        />
      </div>

      <div
        className={classNames('', {
          'my-2 flex items-center gap-2': isAllowedAdjustment,
          hidden: !isAllowedAdjustment
        })}
      >
        <Button variant='default' onClick={handleAddDDC}>
          Thêm phân loại 19 dãy
        </Button>

        <Button variant='default' onClick={handleSearchByName}>
          Bảng trả cứu theo tên
        </Button>
      </div>

      <div className='mt-4 grid grid-cols-12 px-3'>
        <div className='col-span-6'>
          <ul className='list-inside list-disc'>
            {ddc?.slice(0, 500)?.map(({ Ten, MaDay19, Id }) => {
              return (
                <li key={Id}>
                  <Link to={isAllowedAdjustment ? `CapNhat/${Id}` : ''}>
                    {MaDay19} - {Ten}
                  </Link>
                </li>
              );
            })}
          </ul>
        </div>

        <div className='col-span-6'>
          <ul className='list-inside list-disc'>
            {ddc?.slice(500, ddc?.length)?.map(({ Ten, MaDay19, Id }) => {
              return (
                <li key={Id}>
                  <Link to={`CapNhat/${Id}`}>
                    {MaDay19} - {Ten}
                  </Link>
                </li>
              );
            })}
          </ul>
        </div>
      </div>

      {!Boolean(ddc?.length) && (
        <div className='text-center'>
          <p>
            Không tìm thấy kết quả phù hợp. <button onClick={handleRefresh}>Làm mới</button>
          </p>
        </div>
      )}

      <Loading open={isLoading} />
    </div>
  );
};

export default Day19ManagementPage;
