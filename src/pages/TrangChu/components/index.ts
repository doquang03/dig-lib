export { default as EducationHome } from './EducationHome';
export { default as EducationHomeClassic } from './EducationHomeClassic';
export { default as SchoolHome } from './SchoolHome';
