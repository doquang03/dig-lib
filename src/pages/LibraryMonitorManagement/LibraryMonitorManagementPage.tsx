import { path } from 'constants/path';
import { Outlet, useMatch } from 'react-router-dom';
import LibraryMonitorManagermentContent from './LibraryMonitorManagermentContent/LibraryMonitorManagermentContent';

const LibraryMonitorManagementPage = () => {
  const match = useMatch(path.canbothuvien);

  return <>{Boolean(match) ? <LibraryMonitorManagermentContent /> : <Outlet />}</>;
};

export default LibraryMonitorManagementPage;
