import httpThuVien from 'utils/httpThuVien';

const controllerName = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/LoiDongBo`;

const errorSyncDocuments = {
  getData: (params: { page: number; pageSize: number; TextForSearch?: string; TimeSubmit?: string }) =>
    httpThuVien.post<ResponseApi<ListResponse<ErrorSyncDocument>>>(`${controllerName}/LoadData`, params),
  getListOfErrorSyncDocById: (params: { page: number; pageSize: number; IdLuotDongBo: string; IsPaper: boolean }) =>
    httpThuVien.post<ResponseApi<ListResponse<ErrorSyncDocumentById>>>(`${controllerName}/Detail`, params),
  getListOfDetailDocumentErrorById: (id: string) =>
    httpThuVien.post<ResponseApi<ListResponse<ErrorSyncDocumentById>>>(`${controllerName}/DetailError/${id}`),
  getOldDetailBookData: (id: string) =>
    httpThuVien.post<ResponseApi<{ Model: Book | DetailDigitalDocument }>>(`${controllerName}/DetailErrorUnit/${id}`)
};

export default errorSyncDocuments;
