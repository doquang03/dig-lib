import { DeleteFilled, DownloadOutlined, SettingFilled } from '@ant-design/icons';
import { useMutation } from '@tanstack/react-query';
import { Table, Tooltip } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { bookApis } from 'apis';
import { Button, Loading, ProgressBar, Title } from 'components';
import { path } from 'constants/path';
import { JSXElementConstructor, ReactElement, ReactFragment, useMemo, useRef, useState } from 'react';
import { useNavigate } from 'react-router';
import { download, saveByteArray } from 'utils/utils';
import { useCookies } from 'react-cookie';
import { MacDinhThuTu, ViTriCotThemDauSach } from 'constants/book';
import SettingModal from '../AddPublication/Model/SettingModal';
import { keys } from 'lodash';
import { RenderFunction } from 'antd/es/tooltip';

type BookColumsType = Book & { STT: number; ListError?: Array<string>; cellError?: Array<number> };

const AddBookExcelPage = () => {
  const [currentStep, setCurrentStep] = useState<number>(1);
  const [listBooksExcel, setListBooksExcel] = useState<Array<BookColumsType>>([]);
  const [rawDataList, setRawDataList] = useState([]);
  const [page, setPage] = useState(1);
  const [paginationSize, setPaginationSize] = useState(10);
  const [link, setLink] = useState<string>('');
  const [listSuccess, setListSuccess] = useState(0);
  const [listFail, setlistFail] = useState([]);

  const [arrAvaliable, setArrAvaliable] = useState<Array<boolean>>([]);

  const fileInutRef = useRef<HTMLInputElement | null>(null);

  const [cookies, setCookie] = useCookies(['settings_add_book']);
  const [visibleModal, setVisibleModal] = useState(false);

  const [isDay19, setIsDay19] = useState<boolean>();

  const navigate = useNavigate();

  const [isCreate] = useState(window.location.pathname.indexOf('Them') !== -1);
  useMemo(() => {
    if (arrAvaliable?.length === 0) {
      if (cookies.settings_add_book) {
        setArrAvaliable(cookies.settings_add_book);
      } else {
        setArrAvaliable([
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true
        ]);
      }
    }
  }, [arrAvaliable?.length, cookies.settings_add_book]);
  const { mutate: PreviewImport, isLoading: isLoadingPreviewImport } = useMutation({
    mutationFn: (file: File) => {
      let bodyFormData = new FormData();
      bodyFormData.append('file', file as Blob);
      bodyFormData.append(
        'arrCauHinhHienThi',
        JSON.stringify(Object.keys(arrAvaliable).map((key: string) => arrAvaliable[Number.parseInt(key)]))
      );
      if (isCreate) bodyFormData.append('fileName', 'MauSach_Create.xls');
      else bodyFormData.append('fileName', 'MauSach_Update.xls');
      return bookApis.PreviewImport(bodyFormData);
    },
    onSuccess: (res) => {
      let tempData = res?.data?.Item?.RawDataList;
      setRawDataList(tempData);
      setIsDay19(res?.data?.Item.isDay19);
      let UnitData: BookColumsType;
      let result: Array<BookColumsType> = [];
      let count: number;
      for (let i = 0; i < tempData.length; i++) {
        count = 0;
        UnitData = { STT: i };
        // eslint-disable-next-line no-loop-func
        tempData[i].forEach((element: string) => {
          while (
            count < MacDinhThuTu.length && //Kiểm tra số đếm ít hơn số cột
            ((isCreate && count === 1) || // Kiểm tra cột mã kiểm soát khi tạo file
              (isDay19 && count === 7) || // Kiểm tra IsDay19 disable cột DDC
              (!isDay19 && count === 6) || // Kiểm tra IsDay19 disable cột Day19
              (count === 7 && arrAvaliable[ViTriCotThemDauSach.indexOf(6)] === false) || // Kiểm tra DDC trong cấu hình hiển thị
              (ViTriCotThemDauSach.indexOf(count) !== -1 && arrAvaliable[ViTriCotThemDauSach.indexOf(count)] === false)) //Kiểm tra trong cấu hình hiển thị
          ) {
            count++;
          }
          // @ts-ignore
          UnitData[MacDinhThuTu[count]] = element;
          count++;
        });
        UnitData.STT = i;
        result.push(UnitData);
      }

      setListBooksExcel(result);
      setCurrentStep(2);
    }
  });

  const { mutate: ImportSave_Create, isLoading: isLoadingImportSave } = useMutation({
    mutationFn: (payload: { data: Array<object>; arrCauHinh: Array<Boolean> }) =>
      bookApis.ImportSave_Create(rawDataList, payload.arrCauHinh),
    onSuccess: (res) => {
      let tempData = res?.data?.Item;

      setListSuccess(tempData?.UpdateSuccess);
      setlistFail(tempData?.ListFail);
      setLink(tempData?.MemoryStream);
      let listShow = tempData?.ListFail;
      let resultFinal: Array<BookColumsType> = [];
      let tempFor: BookColumsType;
      for (let i = 0; i < listShow.length; i++) {
        tempFor = listShow[i];
        tempFor.STT = i;
        resultFinal.push(tempFor);
      }
      setListBooksExcel(resultFinal);
      setCurrentStep(3);
    }
  });

  const { mutate: DownloadTemplate, isLoading: isLoadingTemplate } = useMutation({
    mutationFn: (fileName: string) => bookApis.TemplateExcel(arrAvaliable, fileName),
    onSuccess: (res, fileName) => {
      download(fileName, res.data);
    }
  });

  const handleSubmit = () => {
    ImportSave_Create({ data: listBooksExcel, arrCauHinh: arrAvaliable });
  };

  const columns: ColumnsType<BookColumsType> = useMemo(() => {
    const handleDeleteRow = (STT: number) => {
      let result: Array<BookColumsType> = [];
      for (let i = 0; i < listBooksExcel.length; i++) {
        if (STT === listBooksExcel[i].STT) {
          continue;
        }
        result.push(listBooksExcel[i]);
      }
      setListBooksExcel(result);
    };
    return [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        render: (value: any, record: any, j: number) => record.STT + 1,
        onCell: (record: BookColumsType) => ({ className: '' })
      },
      {
        title: 'Tên sách',
        width: 150,
        dataIndex: 'TenSach',
        key: 'TenSach',
        render: (value: any, record: any) =>
          record?.TenSach?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.TenSach} arrow={true}>
              <p className='text-center'>{record.TenSach.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.TenSach}</p>
          )
      },
      {
        width: 150,
        title: 'Tên song ngữ',
        dataIndex: 'TenSongNgu',
        key: 'TenSongNgu',
        render: (value: any, record: any) =>
          record?.TenSongNgu?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.TenSongNgu} arrow={true}>
              <p className='text-center'>{record.TenSongNgu.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.TenSongNgu}</p>
          )
      },
      {
        width: 150,
        title: 'Mã ISBN',
        dataIndex: 'ISBN',
        key: 'ISBN',
        render: (value: any, record: any) =>
          record?.ISBN?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.ISBN} arrow={true}>
              <p className='text-center'>{record.ISBN.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.ISBN}</p>
          )
      },
      {
        width: 150,
        title: 'Bộ sưu tập',
        dataIndex: 'IdBoSuuTap',
        key: 'IdBoSuuTap',
        render: (value: any, record: any) =>
          record?.IdBoSuuTap?.length > 10 ? (
            <Tooltip placement='topLeft' title={record.IdBoSuuTap} arrow={true}>
              <p className='text-center'>{record.IdBoSuuTap.substring(0, 10).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.IdBoSuuTap}</p>
          )
      },
      {
        width: 150,
        title: 'Day19',
        dataIndex: 'Day19',
        key: 'Day19',
        render: (value: any, record: any) =>
          record?.Day19?.length > 4 ? (
            <Tooltip placement='topLeft' title={record.Day19} arrow={true}>
              <p className='text-center'>{record.Day19.substring(0, 4).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.Day19}</p>
          )
      },
      {
        width: 150,
        title: 'DDC',
        dataIndex: 'DDC',
        key: 'DDC',
        render: (value: any, record: any) =>
          record?.DDC?.length > 4 ? (
            <Tooltip placement='topLeft' title={record.DDC} arrow={true}>
              <p className='text-center'>{record.DDC.substring(0, 4).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.DDC}</p>
          )
      },
      {
        width: 150,
        title: 'Tác giả chính',
        dataIndex: 'TacGia',
        key: 'TacGia',
        render: (value: any, record: any) =>
          record?.TacGia?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.TacGia} arrow={true}>
              <p className='text-center'>{record.TacGia.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.TacGia}</p>
          )
      },
      {
        width: 150,
        title: 'Tác giả phụ',
        dataIndex: 'TacGiaPhu',
        key: 'TacGiaPhu',
        render: (value: any, record: any) =>
          record?.TacGiaPhu?.length > 20 ? (
            <Tooltip placement='topLeft' title={record.TacGiaPhu} arrow={true}>
              <p className='text-center'>{record.TacGiaPhu.substring(0, 20).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.TacGiaPhu}</p>
          )
      },
      {
        width: 150,
        title: 'Nhà xuất bản',
        dataIndex: 'IdNhaXuatBan',
        key: 'IdNhaXuatBan',
        render: (value: any, record: any) =>
          record?.IdNhaXuatBan?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.IdNhaXuatBan} arrow={true}>
              <p className='text-center'>{record.IdNhaXuatBan.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.IdNhaXuatBan}</p>
          )
      },
      {
        width: 150,
        title: 'Nơi xuất bản',
        dataIndex: 'IdNoiXuatBan',
        key: 'IdNoiXuatBan',
        render: (value: any, record: any) =>
          record?.IdNoiXuatBan?.length > 10 ? (
            <Tooltip placement='topLeft' title={record.IdNoiXuatBan} arrow={true}>
              <p className='text-center'>{record.IdNoiXuatBan.substring(0, 10).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.IdNoiXuatBan}</p>
          )
      },
      {
        title: 'Năm xuất bản',
        dataIndex: 'NamXuatBan',
        key: 'NamXuatBan'
      },
      {
        width: 150,
        title: 'Môn học',
        dataIndex: 'IdMonHoc',
        key: 'IdMonHoc',
        render: (value: any, record: any) =>
          record?.IdMonHoc?.length > 10 ? (
            <Tooltip placement='topLeft' title={record.IdMonHoc} arrow={true}>
              <p className='text-center'>{record.IdMonHoc.substring(0, 10).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.IdMonHoc}</p>
          )
      },
      { width: 150, title: 'Khối lớp', dataIndex: 'KhoiLopString', key: 'KhoiLopString' },
      {
        width: 150,
        title: 'Chủ điểm',
        dataIndex: 'IdChuDiem',
        key: 'IdChuDiem',
        render: (value: any, record: any) =>
          record?.IdChuDiem?.length > 10 ? (
            <Tooltip placement='topLeft' title={record.IdChuDiem} arrow={true}>
              <p className='text-center'>{record.IdChuDiem.substring(0, 10).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.IdChuDiem}</p>
          )
      },
      {
        width: 150,
        title: 'Kệ sách',
        dataIndex: 'IdKeSach',
        key: 'IdKeSach',
        render: (value: any, record: any) =>
          record?.IdKeSach?.length > 10 ? (
            <Tooltip placement='topLeft' title={record.IdKeSach} arrow={true}>
              <p className='text-center'>{record.IdKeSach.substring(0, 10).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.IdKeSach}</p>
          )
      },
      {
        width: 150,
        title: 'Thư mục tài liệu',
        dataIndex: 'IdThuMucSach',
        key: 'IdThuMucSach',
        render: (value: any, record: any) =>
          record?.IdThuMucSach?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.IdThuMucSach} arrow={true}>
              <p className='text-center'>{record.IdThuMucSach.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.IdThuMucSach}</p>
          )
      },
      {
        title: 'Số trang',
        dataIndex: 'SoTrang',
        key: 'SoTrang'
      },
      {
        width: 150,
        title: 'Ngôn ngữ',
        dataIndex: 'IdNgonNgu',
        key: 'IdNgonNgu',
        render: (value: any, record: any) =>
          record?.IdNgonNgu?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.IdNgonNgu} arrow={true}>
              <p className='text-center'>{record.IdNgonNgu.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.IdNgonNgu}</p>
          )
      },
      {
        width: 150,
        title: 'Nước xuất xứ',
        dataIndex: 'XuatXu',
        key: 'XuatXu',
        render: (value: any, record: any) =>
          record?.XuatXu?.length > 10 ? (
            <Tooltip placement='topLeft' title={record.XuatXu} arrow={true}>
              <p className='text-center'>{record.XuatXu.substring(0, 10).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.XuatXu}</p>
          )
      },
      {
        width: 150,
        title: 'Người biên dịch',
        dataIndex: 'NguoiBienDich',
        key: 'NguoiBienDich',
        render: (value: any, record: any) =>
          record?.NguoiBienDich?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.NguoiBienDich} arrow={true}>
              <p className='text-center'>{record.NguoiBienDich.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.NguoiBienDich}</p>
          )
      },
      {
        title: 'Lần xuất bản',
        dataIndex: 'TaiBan',
        key: 'TaiBan'
      },
      {
        width: 150,
        title: 'SKU',
        dataIndex: 'SKU',
        key: 'SKU',
        render: (value: any, record: any) =>
          record?.SKU?.length > 10 ? (
            <Tooltip placement='topLeft' title={record.SKU} arrow={true}>
              <p className='text-center'>{record.SKU.substring(0, 10).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.SKU}</p>
          )
      },
      {
        width: 150,
        title: 'LLC',
        dataIndex: 'LLC',
        key: 'LLC',
        render: (value: any, record: any) =>
          record?.LLC?.length > 10 ? (
            <Tooltip placement='topLeft' title={record.LLC} arrow={true}>
              <p className='text-center'>{record.LLC.substring(0, 10).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.LLC}</p>
          )
      },
      {
        width: 150,
        title: 'Mã màu',
        dataIndex: 'IdMaMau',
        key: 'IdMaMau',
        render: (value: any, record: any) =>
          record?.IdMaMau?.length > 10 ? (
            <Tooltip placement='topLeft' title={record.IdMaMau} arrow={true}>
              <p className='text-center'>{record.IdMaMau.substring(0, 10).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.IdMaMau}</p>
          )
      },
      {
        width: 150,
        title: 'Phí mượn sách',
        dataIndex: 'PhiMuonSach',
        key: 'PhiMuonSach',
        render: (value: any, record: any) =>
          record?.PhiMuonSach?.length > 6 ? (
            <Tooltip placement='topLeft' title={record.PhiMuonSach} arrow={true}>
              <p className='text-center'>{record.PhiMuonSach.substring(0, 6).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.PhiMuonSach}</p>
          )
      },
      {
        width: 150,
        title: 'Giá bìa',
        dataIndex: 'GiaBia',
        key: 'GiaBia',
        render: (value: any, record: any) =>
          record?.GiaBia?.length > 7 ? (
            <Tooltip placement='topLeft' title={record.GiaBia} arrow={true}>
              <p className='text-center'>{record.GiaBia.substring(0, 7).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.GiaBia}</p>
          )
      },
      {
        width: 150,
        title: 'Mô tả vật lý',
        dataIndex: 'MoTaVatLy',
        key: 'MoTaVatLy',
        render: (value: any, record: any) =>
          record?.MoTaVatLy?.length > 20 ? (
            <Tooltip placement='topLeft' title={record.MoTaVatLy} arrow={true}>
              <p className='text-center'>{record.MoTaVatLy.substring(0, 20).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.MoTaVatLy}</p>
          )
      },
      {
        width: 150,
        title: 'Khổ sách',
        dataIndex: 'CoSach',
        key: 'CoSach',
        render: (value: any, record: any) =>
          record?.CoSach?.length > 5 ? (
            <Tooltip placement='topLeft' title={record.CoSach} arrow={true}>
              <p className='text-center'>{record.CoSach.substring(0, 5).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.CoSach}</p>
          )
      },
      {
        width: 150,
        title: 'Tài liệu kèm theo',
        dataIndex: 'TaiLieuDinhKem',
        key: 'TaiLieuDinhKem',
        render: (value: any, record: any) =>
          record?.TaiLieuDinhKem?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.TaiLieuDinhKem} arrow={true}>
              <p className='text-center'>{record.TaiLieuDinhKem.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.TaiLieuDinhKem}</p>
          )
      },
      {
        width: 150,
        title: 'Minh họa',
        dataIndex: 'MinhHoa',
        key: 'MinhHoa',
        render: (value: any, record: any) =>
          record?.MinhHoa?.length > 10 ? (
            <Tooltip placement='topLeft' title={record.MinhHoa} arrow={true}>
              <p className='text-center'>{record.MinhHoa.substring(0, 10).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.MinhHoa}</p>
          )
      },
      {
        width: 150,
        title: 'Tùng thư',
        dataIndex: 'TungThu',
        key: 'TungThu',
        render: (value: any, record: any) =>
          record?.TungThu?.length > 10 ? (
            <Tooltip placement='topLeft' title={record.TungThu} arrow={true}>
              <p className='text-center'>{record.TungThu.substring(0, 10).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.TungThu}</p>
          )
      },
      {
        width: 150,
        title: 'Phụ chú',
        dataIndex: 'PhuChu',
        key: 'PhuChu',
        render: (value: any, record: any) =>
          record?.PhuChu?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.PhuChu} arrow={true}>
              <p className='text-center'>{record.PhuChu.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.PhuChu}</p>
          )
      },
      {
        width: 150,
        title: 'Tóm tắt',
        dataIndex: 'TomTat',
        key: 'TomTat',
        render: (value: any, record: any) =>
          record?.TomTat?.length > 20 ? (
            <Tooltip placement='topLeft' title={record.TomTat} arrow={true}>
              <p className='text-center'>{record.TomTat.substring(0, 20).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.TomTat}</p>
          )
      },
      {
        title: (currentStep === 2 && 'Hành động') || (currentStep === 3 && 'Nội dung lỗi'),
        dataIndex: 'actions',
        key: 'actions',
        render: (value: string, record: BookColumsType) => {
          return (
            <>
              {currentStep === 2 && (
                <button
                  className='mx-2'
                  onClick={(e) => {
                    e.stopPropagation();
                    handleDeleteRow(record.STT);
                  }}
                >
                  <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
                </button>
              )}

              {currentStep === 3 && <p className='text-danger-10'>{record.ListError?.join(', ')}</p>}
            </>
          );
        }
      }
    ].filter((item) => {
      if (item.dataIndex === 'MaKiemSoat' && isCreate) return false;
      let index = MacDinhThuTu.indexOf(item.dataIndex);
      if (index !== -1) {
        if (isDay19 === false && index === 6) return false;
        if (isDay19 === true && index === 7) return false;
        let indexOfMacDinh = ViTriCotThemDauSach.indexOf(index);
        if (index !== -1 && arrAvaliable[indexOfMacDinh] === false) return false;
        if (index === 7 && arrAvaliable[ViTriCotThemDauSach.indexOf(6)] === false) return false;
      }
      item.onCell = (record: BookColumsType) => ({
        className:
          currentStep === 3 && record?.cellError && record?.cellError.indexOf(index) !== -1
            ? 'min-h-38 errorBackGround py-2'
            : ''
      });
      return true;
    });
  }, [currentStep, listBooksExcel, page, paginationSize, isCreate, isDay19, arrAvaliable]);
  const handleOnFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files?.[0];

    PreviewImport(fileFromLocal as File);
  };

  const handleAccessFileInputRef = () => {
    fileInutRef.current?.click();
  };

  const handleBack = () => {
    if (listFail?.length > 0) {
      handleBackCurrent();
    } else navigate(path.sach);
  };

  const handleBackCurrent = () => {
    setListBooksExcel([]);
    setCurrentStep(1);
  };

  return (
    <div className='p-5'>
      <Title title={isCreate ? 'Thêm sách mới từ excel' : 'Cập nhật sách từ excel'} />

      <div className='my-10'>
        <ProgressBar activeStep={currentStep} type='excel' />
      </div>

      {currentStep === 1 && (
        <div className='flex items-center justify-center rounded-md bg-tertiary-10 py-4'>
          <Button
            className='bg-tertiary-20 hover:bg-tertiary-20/30'
            onClick={() => {
              if (isCreate) DownloadTemplate('MauSach_Create.xls');
              else DownloadTemplate('MauSach_Update.xls');
            }}
          >
            <DownloadOutlined />
            File excel mẫu
          </Button>

          <input
            type='file'
            accept='.csv,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel'
            className='hidden'
            ref={fileInutRef}
            onChange={handleOnFileChange}
          />

          <Button variant='default' className='ml-2' onClick={handleAccessFileInputRef}>
            Chọn tệp
          </Button>
          <Button
            variant='default'
            type='button'
            className='setting-button ml-2 font-semibold'
            onClick={() => {
              setVisibleModal(true);
            }}
          >
            <SettingFilled style={{ color: 'white' }}></SettingFilled> Cấu hình hiển thị
          </Button>
        </div>
      )}

      {currentStep === 2 && (
        <>
          <p>
            Tổng số sách: <span className='font-bold'>{listBooksExcel?.length}</span>
          </p>
          {listBooksExcel?.length > 0 && <h3 className='text-primary-10'>DANH SÁCH ẤN PHẨM</h3>}
        </>
      )}

      {currentStep === 3 && (
        <>
          <p className='text-tertiary-30'>
            Lưu thành công: <span className='font-bold'> {' ' + listSuccess} sách</span>
          </p>

          {!!listFail.length && (
            <>
              <p className='text-danger-10'>
                Lưu thất bại: <span className='font-bold'> {' ' + listFail.length} sách</span>{' '}
                {
                  <Button
                    variant='danger'
                    className='ml-2'
                    style={{ display: 'inline-block' }}
                    onClick={() => {
                      saveByteArray(link, 'DSSachLoi.xls', 'application/xls');
                    }}
                  >
                    <DownloadOutlined />
                    Tải file lỗi
                  </Button>
                }
              </p>

              {!!listFail.length && <h3 className='uppercase text-primary-10'>Danh sách ấn phẩm bị lỗi</h3>}
            </>
          )}
        </>
      )}

      {((currentStep === 2 && !!listBooksExcel?.length) || (currentStep === 3 && listFail.length > 0)) && (
        <>
          <div className='mt-6'>
            <Table
              loading={isLoadingPreviewImport && isLoadingImportSave}
              columns={columns}
              pagination={{
                onChange(current, pageSize) {
                  setPage(current);
                  setPaginationSize(pageSize);
                },
                defaultPageSize: 10,
                hideOnSinglePage: true,
                showSizeChanger: false
              }}
              dataSource={listBooksExcel}
              scroll={{ x: 3000 }}
              rowKey={(record) => record.STT}
              className='custom-table'
              bordered
            />
          </div>
        </>
      )}

      <div className='mr-10 mt-6 flex items-center justify-end'>
        {currentStep === 1 && (
          <>
            <Button variant='secondary' onClick={handleBack}>
              Quay về
            </Button>
          </>
        )}

        <>
          {currentStep === 2 && !isLoadingPreviewImport && (
            <>
              <Button variant='secondary' onClick={handleBackCurrent}>
                Quay về
              </Button>
              {listBooksExcel?.length > 0 && (
                <Button variant='default' className='ml-2' onClick={handleSubmit}>
                  Tiếp tục
                </Button>
              )}
            </>
          )}

          {currentStep === 3 && !isLoadingImportSave && (
            <Button variant='default' className='ml-2' onClick={handleBack}>
              Hoàn tất
            </Button>
          )}
        </>
      </div>
      <Loading open={isLoadingTemplate || isLoadingPreviewImport || isLoadingImportSave} />
      <SettingModal
        arrAvaliable={arrAvaliable}
        open={visibleModal}
        setVisibleModal={setVisibleModal}
        setArrAvaliable={setArrAvaliable}
        setCookie={setCookie}
      />
    </div>
  );
};

export default AddBookExcelPage;
