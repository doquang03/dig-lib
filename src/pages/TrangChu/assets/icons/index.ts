export { default as LibraryIcon } from './LibraryIcon';
export { default as NetworkingIcon } from './NetworkingIcon';
export { default as NoteIcon } from './NoteIcon';
export { default as ReadingBookIcon } from './ReadingBookIcon';
export { default as RentingBookIcon } from './RentingBooksIcon';
export { default as StudentIcon } from './StudentIcon';
export { default as TeacherIcon } from './TeacherIcon';
