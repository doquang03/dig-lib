type VisitingReader = {
  Id: string;
  Ten: string;
  MaSoThanhVien: string;
  LoaiTK: string;
  TenTruong: string;
  LopHoc: string;
  ChucVu: string;
  NgaySinh: string;
  GioiTinh: string;
  DaMuon: number;
  DangMuon: number;
};
