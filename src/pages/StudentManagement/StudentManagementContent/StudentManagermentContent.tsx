import { DeleteFilled, EditOutlined, LockOutlined, IdcardOutlined } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Tooltip } from 'antd';
import Table, { type ColumnsType } from 'antd/es/table';
import { studentApis } from 'apis';
import classNames from 'classnames';
import { Button, Empty, Input, ModalDelete, Select, SizeChanger, Title, TitleDelete } from 'components';
import type { ButtonCustomProps } from 'components/Button/Button';
import { EUser, VALIDATE_BY_TYPESCHOOL } from 'constants/options';
import { path } from 'constants/path';
import { useLazyQuery, useOptionsFilterData, usePaginationNavigate, useQueryConfig } from 'hooks';
import { isEmpty, omitBy, trim } from 'lodash';
import { Key, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { convertDate, getSerialNumber } from 'utils/utils';
import { ActiveModal, DeleteStudentsModal, UpdateSchoolYear } from './components';
import { useUser } from 'contexts/user.context';

type FormInput = {
  TextForSearch: string;
  gender: string;
  schoolYear: string;
  class: string;
  status: string;
};

const initialFormValue = {
  TextForSearch: '',
  gender: '',
  class: '',
  schoolYear: '',
  status: ''
};

type TrangThai = 0 | 1;
type TrangThaiContent = 'Đang kích hoạt' | 'Đang bị khóa';

const convertStatus: Record<TrangThai, TrangThaiContent> = {
  0: 'Đang kích hoạt',
  1: 'Đang bị khóa'
};

const StudentManagermentContent = () => {
  const { TypeSchool } = useUser();

  const grade_Validate = useMemo(() => {
    const result = {
      max: Number.MIN_VALUE,
      min: Number.MAX_VALUE
    };
    if (TypeSchool) {
      for (let param in VALIDATE_BY_TYPESCHOOL) {
        if (TypeSchool.includes(param)) {
          if (result.max < VALIDATE_BY_TYPESCHOOL[param].max) {
            result.max = VALIDATE_BY_TYPESCHOOL[param].max;
          }

          if (result.min > VALIDATE_BY_TYPESCHOOL[param].min) {
            result.min = VALIDATE_BY_TYPESCHOOL[param].min;
          }
        }
      }
    }
    return result;
  }, [TypeSchool]);

  const [selectedStudents, setSelectedStudents] = useState<Student[]>([]);
  const [selectedStudent, setSelectedStudent] = useState<{
    student: Student | undefined;
    type: 'lock' | 'delete' | 'unlock';
  }>();
  const [visibleModal, setVisibleModal] = useState<boolean>(false);
  const [visibleModalUpdate, setVisibleModalUpdate] = useState<boolean>(false);

  const [fail, setFail] = useState<Student[]>([]);

  const { register, handleSubmit, reset, setValue } = useForm<FormInput>({
    defaultValues: initialFormValue
  });

  const navigate = useNavigate();

  const { isAllowedAdjustment } = useUser();

  const handleNavigation = usePaginationNavigate();

  const { schoolYears, genders, status, isLoading: loadingOptionFilter } = useOptionsFilterData();

  const queryConfig = useQueryConfig();

  const {
    data: studentData,
    isLoading: loadingStudentData,
    isRefetching: refetchingStudentData,
    refetch
  } = useQuery({
    queryKey: ['students', queryConfig],
    queryFn: () => studentApis.getStudents(queryConfig),
    onSuccess: () => {
      setSelectedStudent(undefined);
      setVisibleModal(false);
      setVisibleModalUpdate(false);
    }
  });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [_, { refetch: refetchGetListOfStudents }] = useLazyQuery(
    ['downloadFileStudents'],
    studentApis.downloadFileStudents,
    {
      onSuccess: (data: any) => {
        const blob = new Blob([data.data, { type: 'application/xls' }]);
        const link = document.createElement('a');
        link.href = URL.createObjectURL(blob);
        link.download = 'DanhSachHocSinh.xls';
        link.click();
      }
    }
  );

  const { mutate: deleteStudents, isLoading: loadingDeleteStudent } = useMutation({
    mutationFn: (students: string[]) => studentApis.deleteStudent(students),
    onSuccess: () => {
      refetch();
      toast.success('Xóa học sinh thành công');
      if (Boolean(selectedStudents.length)) {
        queryConfig.page = '1';
      }

      handleNavigation({ ...queryConfig });

      setSelectedStudents([]);
    }
  });

  const { mutate: activeStudent, isLoading: loadingActiveStudent } = useMutation({
    mutationFn: (payload: { student: Student }) => studentApis.activeStudent(payload.student.Id),
    onSuccess: (_, payload) => {
      payload.student.TrangThai = payload.student.TrangThai === EUser.Active ? EUser.DeActive : EUser.Active;
      refetch();
      toast.success('Kích hoạt học sinh thành công');
      setSelectedStudent(undefined);
    }
  });

  const { mutate: deactiveStudent, isLoading: loadingDeactiveStudent } = useMutation({
    mutationFn: (payload: { student: Student }) => studentApis.deactiveStudent(payload.student.Id),
    onSuccess: (_, payload) => {
      payload.student.TrangThai = payload.student.TrangThai === EUser.DeActive ? EUser.Active : EUser.DeActive;
      refetch();
      toast.success('Khóa học sinh thành công');
      setSelectedStudent(undefined);
    }
  });

  const students = useMemo(() => {
    if (!studentData?.data.Item) return;

    const { ListThanhVien, PageNumber, PageSize, TotalRecord } = studentData?.data.Item;

    return { ListThanhVien, PageNumber, PageSize, TotalRecord };
  }, [studentData?.data.Item]);

  const columns: ColumnsType<Student> = useMemo(() => {
    return [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',

        render: (value, record, index) => {
          return (
            queryConfig.page && queryConfig.pageSize && getSerialNumber(+queryConfig.page, +queryConfig.pageSize, index)
          );
        }
      },
      {
        title: 'Tên học sinh',
        dataIndex: 'Ten',
        key: 'Ten',
        render: (value, { Ten }) =>
          Ten.length > 25 ? (
            <Tooltip placement='topLeft' title={Ten} arrow={true} className='text-left'>
              <p className='text-left'>{Ten.substring(0, 25).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-left'>{Ten}</p>
          ),
        onCell: (record) => ({
          className: 'text-left w-[280px]'
        })
      },
      {
        title: 'Mã học sinh',
        dataIndex: 'MaSoThanhVien',
        key: 'MaSoThanhVien'
      },
      {
        title: 'Lớp',
        dataIndex: 'LopHoc',
        key: 'LopHoc',
        render: (value, { LopHoc }) => <p className='text-left'>{LopHoc}</p>,

        onCell: (record) => ({
          className: 'text-left'
        })
      },
      {
        title: 'Ngày sinh',
        dataIndex: 'NgaySinh',
        key: 'NgaySinh',
        render: (value, { NgaySinh }) => <p>{convertDate(NgaySinh)}</p>
      },
      {
        title: 'Giới tính',
        dataIndex: 'GioiTinh',
        key: 'GioiTinh'
      },
      {
        title: 'Năm học',
        dataIndex: 'NienKhoa',
        key: 'NienKhoa'
      },
      {
        title: 'Tình trạng',
        dataIndex: 'TrangThai',
        key: 'TrangThai',
        render: (value, { TrangThai }) => <p>{convertStatus[TrangThai as TrangThai]}</p>
      },
      {
        title: 'Hành động',
        dataIndex: 'actions',
        key: 'actions',

        render: (value, record) => {
          return (
            <>
              <Tooltip title='Cập nhật'>
                <button
                  className='mx-2'
                  onClick={(e) => {
                    e.stopPropagation();
                    navigate(`CapNhat/${record.Id}`, { preventScrollReset: true });
                  }}
                >
                  <EditOutlined style={{ fontSize: '20px', color: '#08c' }} />
                </button>
              </Tooltip>

              <Tooltip title='Xuất thẻ thư viện'>
                <button
                  className={classNames('', {
                    'mx-2': isAllowedAdjustment,
                    hidden: !isAllowedAdjustment
                  })}
                  onClick={(e) => {
                    e.stopPropagation();
                    navigate(path.xuatthethuvien, {
                      state: { selectedStudents: [record.Id + ''] },
                      preventScrollReset: true
                    });
                  }}
                >
                  <IdcardOutlined style={{ fontSize: '20px', color: '#08c' }} />
                </button>
              </Tooltip>

              <Tooltip title={record.TrangThai === 0 ? 'Khóa' : 'Kích hoạt'}>
                <button
                  className={classNames('', {
                    'mx-2': isAllowedAdjustment,
                    hidden: !isAllowedAdjustment
                  })}
                  onClick={(e) => {
                    e.stopPropagation();
                    setSelectedStudent((preveState) => {
                      return { ...preveState, student: record, type: record.TrangThai === 0 ? 'lock' : 'unlock' };
                    });
                  }}
                >
                  <LockOutlined style={{ fontSize: '20px', color: record.TrangThai === 0 ? '#08c' : 'red' }} />
                </button>
              </Tooltip>

              <Tooltip title={'Xóa'}>
                <button
                  className={classNames('', {
                    'mx-2': isAllowedAdjustment,
                    hidden: !isAllowedAdjustment
                  })}
                  onClick={(e) => {
                    e.stopPropagation();
                    setSelectedStudent((preveState) => {
                      return { ...preveState, student: record, type: 'delete' };
                    });
                  }}
                >
                  <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
                </button>
              </Tooltip>
            </>
          );
        },
        onCell: (record) => ({
          className: 'text-left w-[220px]'
        })
      }
    ];
  }, [navigate, queryConfig.page, queryConfig.pageSize]);

  const onPaginate = useCallback(
    (page: number, pageSize: number) => {
      handleNavigation({ ...queryConfig, page: page + '', pageSize: pageSize + '' });
    },
    [handleNavigation, queryConfig]
  );

  const pagination = useMemo(() => {
    let size = 30;
    let currentPage = 1;

    if (queryConfig.pageSize) {
      size = +queryConfig.pageSize;
    }
    if (queryConfig.page) {
      currentPage = +queryConfig.page;
    }

    return {
      current: currentPage,
      pageSize: size,
      total: students?.TotalRecord,
      onChange: onPaginate,
      hideOnSinglePage: true,
      showSizeChanger: false,
      showQuickJumper: true,
      locale: { jump_to: '', page: '' }
    };
  }, [onPaginate, queryConfig.page, queryConfig.pageSize, students?.TotalRecord]);

  useEffect(() => {
    Object.keys(queryConfig).forEach((key) => {
      if (!key) {
        return;
      }
      // @ts-ignore
      setValue(key, queryConfig[key]);
    });
  }, [queryConfig, setValue]);

  const onSubmit = handleSubmit((data) => {
    const searchResult = trim(data.TextForSearch, ' ').replace(/\s\s+/g, ' ');

    handleNavigation({ ...omitBy({ ...data, TextForSearch: searchResult }, isEmpty), page: '1' });
  });

  const handleResetField = () => {
    reset();
    setSelectedStudents([]);
    handleNavigation({ page: '1', pageSize: '30' });
  };

  const handleDownloadListOfStudent = () => {
    refetchGetListOfStudents();
  };

  const handleUploadAvatar = () => {
    navigate(path.themhinhdaidienhocsinh);
  };

  const handleAddStudentByExcelFile = () => {
    navigate(path.themhocsinhexcel);
  };

  const handleAddStudent = () => {
    navigate(path.themhocsinh);
  };

  const handleExportCard = () => {
    if (!selectedStudents.length) {
      return;
    }

    navigate(path.xuatthethuvien, {
      state: { selectedStudents: selectedStudents.map(({ Id }) => Id) }
    });
  };

  const handleDeleteStudent = () => {
    !!selectedStudents.length && setVisibleModal(true);
  };

  const listId = useMemo(() => {
    const selectId: string[] = [];
    let countFail: Student[] = [];
    selectedStudents.forEach((element) => {
      const inputString: string = 'lop' + element.LopHoc;
      const regexPattern: RegExp = /\D+/;
      const classGroup: string[] = inputString.split(regexPattern);
      let success;
      if (element.NienKhoa.includes('-')) {
        const yyyy: string = element.NienKhoa.split('-')[1].trim();
        const yy: number = parseInt(yyyy, 10);
        if (!isNaN(yy)) {
          if (yy <= new Date().getFullYear()) success = true;
          else success = false;
        } else {
          success = true;
        }
      } else {
        const yy: number = parseInt(element.NienKhoa.trim(), 10);
        if (!isNaN(yy)) {
          if (yy <= new Date().getFullYear()) success = true;
          else success = false;
        } else {
          success = true;
        }
      }
      if (success) {
        if (classGroup.length >= 3) {
          if (parseInt(classGroup[1]) < grade_Validate.max) {
            selectId.push(element.Id);
          } else {
            countFail.push(element);
          }
        } else {
          selectId.push(element.Id);
        }
      } else {
        countFail.push(element);
      }
    });
    setFail(countFail);
    return selectId;
  }, [grade_Validate.max, grade_Validate.min, selectedStudents]);

  const handleUpdateSchoolYear = () => {
    if (listId.length > 0) setVisibleModalUpdate(true);
  };

  return (
    <div className='p-5'>
      <Title title='Danh sách học sinh' />

      <form className='my-5 flex flex-col flex-wrap gap-2 bg-secondary-10 py-3 px-3.5 md:flex-row' onSubmit={onSubmit}>
        <Input
          placeholder='Nhập mã, tên học sinh, lớp'
          containerClassName='w-1/5'
          name='TextForSearch'
          register={register}
        />

        <Select
          items={genders || []}
          className='w-full md:w-1/5'
          name='gender'
          register={register}
          disabled={loadingOptionFilter || !Boolean(genders?.length)}
        />

        <Select
          items={schoolYears || []}
          className='w-full md:w-1/5'
          name='schoolYear'
          register={register}
          disabled={loadingOptionFilter || !Boolean(schoolYears?.length)}
        />

        <Select
          items={status || []}
          className='w-full md:w-1/5'
          name='status'
          register={register}
          disabled={loadingOptionFilter || !Boolean(status?.length)}
        />

        <Button type='button' variant='secondary' className='font-semibold' onClick={handleResetField}>
          Làm mới
        </Button>

        <Button type='submit' variant='default' className='font-semibold'>
          Tìm kiếm
        </Button>
      </form>

      <div
        className={classNames('my-4 gap-1', {
          hidden: !isAllowedAdjustment,
          'flex-col flex-wrap gap-1 md:flex md:flex-row': isAllowedAdjustment
        })}
      >
        {[
          { label: 'Thêm học sinh', variant: 'default', onClick: handleAddStudent },
          {
            label: ' Thêm từ Excel',
            variant: 'default',
            onClick: handleAddStudentByExcelFile
          },
          {
            label: 'Xuất thẻ thư viện',
            variant: 'default',
            onClick: handleExportCard,
            disabled: !selectedStudents.length
          },
          {
            label: 'Cập nhật ảnh đại diện',
            variant: 'default',
            onClick: handleUploadAvatar
          },
          {
            label: 'Cập nhật năm học',
            variant: 'default',
            onClick: handleUpdateSchoolYear,
            disabled: !listId.length
          },
          {
            label: 'Tải danh sách học sinh',
            variant: 'default',
            onClick: handleDownloadListOfStudent
          },
          {
            label: 'Xóa học sinh',
            variant: 'danger',
            onClick: handleDeleteStudent,
            disabled: !selectedStudents.length
          }
        ].map(({ label, variant, disabled, onClick }) => {
          const variantResult = disabled ? 'disabled' : variant;
          return (
            <Button
              variant={variantResult as ButtonCustomProps['variant']}
              className={classNames('mt-2 w-[100%] md:w-auto', {
                'bg-gray-400': disabled,
                'cursor-not-allowed': disabled,
                'hover:bg-gray-400': disabled
              })}
              key={label}
              onClick={onClick}
              disabled={disabled}
            >
              {label}
            </Button>
          );
        })}
      </div>

      <div className='mt-6'>
        {Number.isInteger(queryConfig.page && +queryConfig.page) ? (
          <Table
            loading={loadingStudentData && !students?.ListThanhVien.length}
            rowSelection={{
              selectedRowKeys: selectedStudents.map((item) => item.Id),
              preserveSelectedRowKeys: true,
              onChange: (_: Key[], selectedRowValue: Student[]) => {
                setSelectedStudents(selectedRowValue);
              }
            }}
            columns={columns}
            dataSource={studentData?.data?.Item?.ListThanhVien || []}
            pagination={pagination}
            scroll={{ x: 1500 }}
            rowKey={(record) => record.Id}
            className='custom-table'
            bordered
            locale={{
              emptyText: () => <Empty />
            }}
          />
        ) : (
          <div className='rounded-md bg-white p-2 shadow-md'>
            <p className=' text-center'>
              Đã có lỗi xảy ra.
              <button onClick={handleResetField}>Vui lòng làm mới tìm kiếm.</button>
            </p>
          </div>
        )}
      </div>

      <div
        className={classNames('relative', {
          // @ts-ignore
          'mt-[64px]': (queryConfig.page && +queryConfig.pageSize) >= students?.TotalRecord
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger
            visible={!!students?.ListThanhVien?.length}
            value={students?.ListThanhVien?.length + ''}
            total={students?.TotalRecord + ''}
          />
        </div>
      </div>

      {/* Lock/unlock Modal for one student */}
      <ActiveModal
        type={selectedStudent?.type}
        selectedStudent={selectedStudent?.student}
        onCancel={() => setSelectedStudent(undefined)}
        onOk={() => {
          selectedStudent?.type === 'unlock'
            ? activeStudent({ student: selectedStudent?.student as Student })
            : deactiveStudent({ student: selectedStudent?.student as Student });
        }}
        loading={loadingActiveStudent || loadingDeactiveStudent}
      />

      {/* Deleted Modal for one student */}
      <ModalDelete
        open={Boolean(selectedStudent?.student) && selectedStudent?.type === 'delete'}
        closable={false}
        title={
          <TitleDelete firstText={'Bạn có chắn chắn muốn xóa học sinh'} secondText={selectedStudent?.student?.Ten} />
        }
        handleCancel={() => setSelectedStudent(undefined)}
        handleOk={() => {
          if (!selectedStudent?.student?.DaTra || selectedStudent?.student?.TrangThai === 1) {
            setSelectedStudent(undefined);
            !selectedStudent?.student?.DaTra && toast.error(`Học sinh ${selectedStudent?.student?.Ten} chưa trả sách`);
            selectedStudent?.student?.TrangThai === 1 &&
              toast.error(`Học sinh ${selectedStudent?.student?.Ten} đang bị khóa`);
            return;
          }

          deleteStudents([selectedStudent?.student?.Id + '']);
        }}
        loading={loadingDeleteStudent || refetchingStudentData}
      />

      {/* Deleted Modal for many students */}
      <DeleteStudentsModal
        visible={visibleModal}
        students={selectedStudents || []}
        onCancel={() => {
          setVisibleModal(false);
        }}
        onOk={() => {
          // filter item have Trang Thai = 1 (Khóa) hoặc TrangThai = 2 (Đã xóa) hoặc Datra = false (chưa trả sách)
          const studentsResult = selectedStudents
            .filter((item) => {
              if (item.TrangThai === 1 || item.TrangThai === 2 || !item.DaTra) {
                // eslint-disable-next-line array-callback-return
                return;
              }

              return item;
            })
            .map(({ Id }) => Id);

          deleteStudents(studentsResult);
        }}
        loading={loadingDeleteStudent || refetchingStudentData}
      />

      {/* Update schoolyear Modal for many students */}
      <UpdateSchoolYear
        open={visibleModalUpdate}
        onClose={() => {
          setVisibleModalUpdate(false);
        }}
        onSuccess={() => {
          setSelectedStudents([]);
          refetch();
        }}
        listId={listId || []}
        countFail={fail}
        loading={refetchingStudentData}
      />
    </div>
  );
};

export default StudentManagermentContent;
