import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/NhaXuatBan`;

const publishingCompanyApis = {
  Index: (queryString: string) => {
    return httpThuVien.post(`/${endPoint}/IndexByModel` + queryString);
  },
  Delete: (id: string) => {
    return httpThuVien.post(`/${endPoint}/Xoa?id=` + id);
  },
  EditByID: (id: string) => {
    return httpThuVien.post(`/${endPoint}/SuaByID?id=` + id);
  },
  Create: (payload: object) => {
    return httpThuVien.post<ResponseApi<{ Id: string }>>(`/${endPoint}/Them`, payload);
  },
  EditModel: (payload: object) => {
    return httpThuVien.post(`/${endPoint}/Sua`, payload);
  }
};

export default publishingCompanyApis;
