import { DatePicker } from 'antd';
import locale from 'antd/es/date-picker/locale/vi_VN';
import { Input, Select } from 'components';
import { ACCOUNT_TYPE_OPTIONS, LOCATION_READER_OPTIONS, RENTING_BOOK_STATUSES_OPTIONS } from 'constants/options';
import dayjs, { type Dayjs } from 'dayjs';
import { debounce } from 'lodash';
import type { PickerMode } from 'rc-picker/lib/interface';
import { ChangeEvent, useEffect, useState } from 'react';

type Props = {
  picker: PickerMode;
  statusBook: 0 | 1 | 2 | 3 | 4 | 5;
  onChangeSearchParams: (e: ChangeEvent<HTMLSelectElement | HTMLInputElement> | 0 | 1 | 2 | 3 | 4 | 5) => void;
  onChangeDatePicker: (value: Dayjs | null) => void;
};

const RentingBookSearchForm = (props: Props) => {
  const { picker, statusBook, onChangeSearchParams, onChangeDatePicker } = props;

  const [valueStatus, setValueStatus] = useState<0 | 1 | 2 | 3 | 4 | 5>(0);

  const handleOnChange = debounce((e: ChangeEvent<HTMLSelectElement | HTMLInputElement> | 0 | 1 | 2 | 3 | 4 | 5) => {
    onChangeSearchParams(e);
  }, 300);

  const handleOnChangeDatePicker = (value: Dayjs | null) => {
    onChangeDatePicker(value || null);
  };

  const handleOnChangeStatus = (e: ChangeEvent<HTMLSelectElement | HTMLInputElement>) => {
    setValueStatus(+e.target.value as 0 | 1 | 2 | 3 | 4 | 5);
    handleOnChange(e);
  };

  useEffect(() => {
    if (statusBook !== undefined && [0, 1, 2, 3, 4, 5].includes(statusBook)) {
      setValueStatus(statusBook);
      handleOnChange(statusBook as 0 | 1 | 2 | 3 | 4 | 5);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [statusBook]);

  return (
    <form className='flex flex-col gap-2 bg-primary-60/20 px-2 py-3 md:flex-row'>
      <Input
        placeholder='Nhập tên bạn đọc, mã, lớp/tổ'
        name='textForSearch'
        containerClassName='w-full md:w-1/3'
        onChange={handleOnChange}
      />

      <Select items={ACCOUNT_TYPE_OPTIONS} name='styleUser' className='w-full md:w-1/6' onChange={handleOnChange} />

      <Select
        items={RENTING_BOOK_STATUSES_OPTIONS}
        name='TinhTrang'
        className='w-full md:w-1/6'
        onChange={(e) => handleOnChangeStatus(e)}
        value={valueStatus}
      />

      <DatePicker
        placeholder={picker === 'month' ? 'Chọn tháng' : 'Chọn ngày'}
        name='rentDate'
        picker={picker}
        onChange={handleOnChangeDatePicker}
        className={'w-full border border-gray-300 py-[0.8rem] px-2 outline-none md:w-1/6'}
        format={picker === 'month' ? 'MM/YYYY' : 'DD/MM/YYYY'}
        disabledDate={(current) => current >= dayjs().endOf('day')}
        locale={locale}
        defaultValue={dayjs()}
      />

      <Select items={LOCATION_READER_OPTIONS} className='w-full md:w-1/6' name='IsGlobal' onChange={handleOnChange} />
    </form>
  );
};

export default RentingBookSearchForm;
