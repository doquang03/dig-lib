import { DeleteFilled, EditOutlined, InfoCircleOutlined } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Spin, Table, Tooltip } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { expenseApis, settingApis } from 'apis';
import classNames from 'classnames';
import { Button, Loading, ModalDelete, SizeChanger, Title, TitleDelete } from 'components';
import { SCHOOL_YEAR_OPTIONS_1 } from 'constants/options';
import dayjs from 'dayjs';
import { ChangeEvent, useMemo, useRef, useState } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { convertDate, download, getSerialNumber } from 'utils/utils';

type Expense = {
  Id: string;
  ThoiGianTao: string;
  TrangThai: string;
  NoiDung: string;
  GiaTien: number;
};

const ExpenseList = () => {
  const [selectedExpense, setSelectedExpense] = useState<Expense>();
  const [yearValue, setYearValue] = useState<string>();
  const [searchForm, setSearchForm] = useState<{
    page: number;
    pageSize: number;
    Ngay: {
      Min: string;
      Max: string;
    };
  }>({
    page: 1,
    pageSize: 30,
    Ngay: {
      Min: '',
      Max: ''
    }
  });

  const selectRef = useRef<HTMLSelectElement | null>(null);
  const ref = useRef(null);

  const { data: startDateData, isLoading: loadingStartDateData } = useQuery({
    queryKey: ['startDate'],
    queryFn: settingApis.getStartDate,
    onSuccess: (data) => {
      const date = data.data.Item.NgayBatDauNamHoc.split('/');
      const dateResult = `${date[1]}/${date[0]}/${new Date().getFullYear()}`;

      let currentYear1 = '';
      let currentYear2 = '';

      // nếu ngày bắt đầu năm học > ngày hiện tại thì lấy năm hiện tại - năm hiện tại + 1
      // nếu ngày bắt đàu năm học < ngày hiện tại thì lấy năm hiện tại - 1 - năm hiện tại
      if (dayjs(dateResult).startOf('day').isAfter(dayjs())) {
        currentYear1 = dayjs(dateResult).subtract(1, 'day').startOf('day').toISOString();
        currentYear2 = dayjs(dateResult).subtract(1, 'day').startOf('day').add(1, 'year').toISOString();
      } else {
        currentYear1 = dayjs(dateResult).subtract(1, 'day').startOf('day').toISOString();
        currentYear2 = dayjs(dateResult).subtract(1, 'day').startOf('day').add(1, 'year').toISOString();
      }
      setYearValue(`${currentYear1} - ${currentYear2}`);

      setSearchForm((prevState) => {
        return {
          ...prevState,
          Ngay: {
            Min: currentYear1,
            Max: currentYear2
          }
        };
      });
    }
  });

  // useEffect(() => {
  //   if (startDateData === undefined) return;
  //   const date = startDateData?.data.Item.NgayBatDauNamHoc.split('/');
  //   const dateResult = `${date[1]}/${date[0]}/${new Date().getFullYear()}`;
  //   const dateTime = new Date(dateResult);
  //   if (dateTime < new Date()) {
  //     setYearValue(new Date().getFullYear() - 1 + ' - ' + new Date().getFullYear());
  //     // return new Date().getFullYear() - 1 + ' - ' + new Date().getFullYear();
  //   } else {
  //     setYearValue(new Date().getFullYear() + ' - ' + (new Date().getFullYear() + 1));
  //     // return new Date().getFullYear() + ' - ' + (new Date().getFullYear() + 1);
  //   }
  // }, [startDateData?.data?.Item?.NgayBatDauNamHoc]);

  const {
    data: expensesData,
    isLoading: loadingExpenses,
    refetch
  } = useQuery({
    queryKey: ['getExpenses', searchForm.Ngay.Max, searchForm.Ngay.Min, searchForm.page, searchForm.pageSize],
    queryFn: () => expenseApis.getMany(searchForm.Ngay, searchForm.page, searchForm.pageSize),
    enabled: Boolean(searchForm.Ngay.Min) && Boolean(searchForm.Ngay.Max)
  });

  const { mutate: downloadExpense, isLoading: loadingDownloadFileExcel } = useMutation({
    mutationKey: ['downloadExpense', searchForm.Ngay, searchForm.page, searchForm.pageSize],
    mutationFn: () => expenseApis.downloadExcel(searchForm.Ngay, searchForm.page, searchForm.pageSize),
    onSuccess: (data) => {
      download('SoTheoDoiKinhPhi.xls', data.data);
    }
  });

  const { mutate: downloadExpenseWord, isLoading: loadingDownloadFileExcelWord } = useMutation({
    mutationKey: ['downloadExpenseWord', searchForm.Ngay, searchForm.page, searchForm.pageSize],
    mutationFn: () => expenseApis.downloadExpenseWord(searchForm.Ngay, searchForm.page, searchForm.pageSize),
    onSuccess: (data) => {
      download('SoTheoDoiKinhPhi.doc', data.data);
    }
  });

  const { mutate: deleteExpense, isLoading: loadingDeleteExpense } = useMutation({
    mutationFn: expenseApis.delete,
    onSuccess: () => {
      toast.success('Xóa sổ theo dõi kinh phí thành công');

      setSelectedExpense(undefined);
      refetch();
    }
  });

  const isAllowed = useMemo(() => {
    if (!startDateData?.data.Item.NgayBatDauNamHoc) return;
    const currentDate = dayjs();

    const min = dayjs(searchForm.Ngay.Min).add(-1, 'day');
    const max = dayjs(searchForm.Ngay.Max).add(1, 'day');
    return currentDate.isAfter(min) && currentDate.isBefore(max);
  }, [searchForm.Ngay.Max, searchForm.Ngay.Min, startDateData]);

  const onClick = () => {
    selectRef && selectRef?.current?.focus();
  };

  const columns: ColumnsType<Expense> = [
    {
      key: 'STT',
      title: 'STT',
      dataIndex: 'STT',
      render: (value, record, index) => getSerialNumber(searchForm.page, searchForm.pageSize, index),
      width: 100
    },
    {
      key: 'Ngày',
      title: 'Ngày',
      dataIndex: 'ThoiGianTao',
      render: (value, record) => convertDate(record.ThoiGianTao),
      width: 170
    },
    {
      key: 'Trạng thái',
      title: 'Trạng thái',
      dataIndex: 'TrangThai',
      width: 170
    },
    {
      key: 'Nội dung',
      title: 'Nội dung',
      dataIndex: 'NoiDung',
      onCell: (record) => ({
        className: 'text-left'
      }),
      ellipsis: true,
      render: (value, { NoiDung }) => (
        <Tooltip title={NoiDung} arrow={true} className='truncate text-left' placement='topLeft'>
          <p>{NoiDung}</p>
        </Tooltip>
      )
    },
    {
      key: 'Tổng tiền',
      title: 'Tổng tiền',
      dataIndex: 'GiaTien',
      width: 130,
      render: (value, record) =>
        new Intl.NumberFormat('vi-VN', {
          style: 'currency',
          currency: 'VND',
          maximumFractionDigits: 9
        }).format(Number(record.GiaTien || 0))
    },
    {
      key: 'Hành động',
      title: 'Hành động',
      dataIndex: 'Hành động',
      render: (value, record) => {
        return (
          <>
            <Tooltip title='Cập nhật'>
              <Link to={`CapNhat/${record.Id}`} className='mx-2'>
                <EditOutlined style={{ fontSize: '20px', color: '#08c' }} />
              </Link>
            </Tooltip>

            <Tooltip title='Xóa'>
              <button
                className='mx-2'
                onClick={(e) => {
                  e.stopPropagation();
                  setSelectedExpense(record);
                }}
              >
                <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
              </button>
            </Tooltip>
          </>
        );
      },
      width: 130
    }
  ];

  const handleSelectSchoolYear = (e: ChangeEvent<HTMLSelectElement>) => {
    if (!e) return;

    const dateStart = startDateData?.data.Item.NgayBatDauNamHoc;

    if (!dateStart) return;

    const { value } = e.target;

    setYearValue(value);

    const years = value.split('-');

    const dateConvert = dateStart?.split('/');

    const currentYear = dayjs(`${dateConvert[1]}/${dateConvert[0]}/${years[1].trimEnd()}`)
      .subtract(1, 'day')
      .startOf('day')
      .toISOString();

    const preYear = dayjs(`${dateConvert[1]}/${dateConvert[0]}/${years[0].trimStart()}`).endOf('day').toISOString();

    setSearchForm((prevState) => {
      return {
        ...prevState,
        Ngay: {
          Min: preYear,
          Max: currentYear
        },
        page: 1,
        pageSize: 30
      };
    });
  };

  const handleDelete = () => {
    if (!selectedExpense?.Id) return;

    deleteExpense(selectedExpense?.Id);
  };

  const handleDownloadExcel = () => {
    downloadExpense();
  };

  const handleDownloadWord = () => {
    downloadExpenseWord();
  };
  return (
    <div className='p-5'>
      <Title title='Sổ theo dõi kinh phí' />

      <div className='mt-3 flex items-center justify-between'>
        <div>
          <div className='relative h-[2.25rem] w-[200px]' onClick={onClick}>
            <select
              className='date-picker top-2 z-20 h-10 w-[200px] rounded-md border border-primary-10  bg-transparent px-1 outline-none'
              ref={selectRef}
              onChange={handleSelectSchoolYear}
              value={yearValue}
            >
              {SCHOOL_YEAR_OPTIONS_1.map((schoolYear, index) => (
                <option
                  key={index}
                  value={schoolYear.value}
                  style={{
                    marginLeft: '10px'
                  }}
                >
                  {schoolYear.label}
                </option>
              ))}
            </select>

            <div className='absolute right-1 top-2 z-10'>{loadingStartDateData ? <Spin spinning /> : <></>}</div>
          </div>

          <div className='mt-6 flex gap-2'>
            {isAllowed && (
              <Link to={'ThemMoi'}>
                <Button>Thêm mới</Button>
              </Link>
            )}

            <Button onClick={handleDownloadExcel}>Tải file excel</Button>

            <Button onClick={handleDownloadWord}>In sổ</Button>

            <Link to='BieuDoTheoDoiKinhPhi'>
              <Button>Xem biểu đồ qua các năm</Button>
            </Link>
          </div>
        </div>

        <div className='flex flex-col gap-2'>
          <div className='flex items-center gap-1'>
            <InfoCircleOutlined
              style={{ color: expensesData?.data.Item && expensesData?.data.Item.Sum_Ton <= 0 ? 'red' : '#119757' }}
            />
            <p
              className={classNames('font-bold', {
                'text-danger-10': expensesData?.data.Item && expensesData?.data.Item.Sum_Ton <= 0,
                'text-tertiary-30': expensesData?.data.Item && expensesData?.data.Item.Sum_Ton > 0
              })}
            >
              Tổng tồn còn lại qua các năm:{' '}
              {new Intl.NumberFormat('vi-VN', {
                style: 'currency',
                currency: 'VND',
                maximumFractionDigits: 9
              }).format(Number(expensesData?.data.Item.Sum_Ton))}
            </p>
          </div>

          <div className='flex items-center gap-2'>
            <div className='w-[300px] rounded-md border border-tertiary-30/80 bg-tertiary-30/10 p-2'>
              <p className='font-bold text-tertiary-30'>
                Tổng thu:{' '}
                {new Intl.NumberFormat('vi-VN', {
                  style: 'currency',
                  currency: 'VND',
                  maximumFractionDigits: 9
                }).format(Number(expensesData?.data.Item.Sum_Thu))}
              </p>
            </div>

            <div className='w-[300px] rounded-md border border-danger-10/80 bg-danger-10/10 p-2'>
              <p className='font-bold text-danger-10'>
                Tổng chi:{' '}
                {new Intl.NumberFormat('vi-VN', {
                  style: 'currency',
                  currency: 'VND',
                  maximumFractionDigits: 9
                }).format(Number(expensesData?.data.Item.Sum_Chi))}
              </p>
            </div>
          </div>

          <div className='flex items-center gap-2'>
            <div className='w-[300px] rounded-md border border-primary-10/80 bg-primary-10/10 p-2'>
              <p className='font-bold text-primary-10'>
                Quỹ ngân sách:{' '}
                {new Intl.NumberFormat('vi-VN', {
                  style: 'currency',
                  currency: 'VND',
                  maximumFractionDigits: 9
                }).format(Number(expensesData?.data.Item.Sum_QuyNganSach))}
              </p>
            </div>

            <div className='w-[300px] rounded-md border border-[#D9C510]/80 bg-[#D9C510]/10 p-2'>
              <p className='font-bold text-[#D9C510]'>
                Tổng tồn:{' '}
                {new Intl.NumberFormat('vi-VN', {
                  style: 'currency',
                  currency: 'VND',
                  maximumFractionDigits: 9
                }).format(
                  Number(
                    expensesData?.data.Item &&
                      expensesData?.data.Item?.Sum_QuyNganSach +
                        expensesData?.data.Item?.Sum_Thu +
                        expensesData?.data.Item?.Sum_Ton -
                        expensesData?.data.Item?.Sum_Chi
                  )
                )}
              </p>
            </div>
          </div>
        </div>
      </div>

      <Table
        ref={ref}
        columns={columns}
        className='custom-table mt-4'
        bordered
        dataSource={expensesData?.data.Item.ListModel}
        rowKey={(row) => row.Id}
        loading={loadingExpenses}
        pagination={{
          current: searchForm.page || 1,
          pageSize: searchForm.pageSize || 30,
          total: expensesData?.data.Item?.Total,
          onChange: (page: number, pageSize: number) => {
            setSearchForm((prevState) => {
              return {
                ...prevState,
                page,
                pageSize
              };
            });
          },
          hideOnSinglePage: true,
          showSizeChanger: false,
          showQuickJumper: true,
          locale: { jump_to: '', page: '' }
        }}
      />

      <div
        className={classNames('relative', {
          'mt-[64px]': Number(searchForm.pageSize) >= (expensesData?.data?.Item?.Total || 0)
        })}
      >
        <div className='absolute bottom-2'>
          <SizeChanger
            visible={!!expensesData?.data.Item?.Total}
            currentPage={searchForm.page + ''}
            value={searchForm.pageSize + ''}
            total={expensesData?.data.Item?.Total + ''}
          />
        </div>
      </div>

      <ModalDelete
        open={Boolean(selectedExpense)}
        title={
          <TitleDelete
            firstText='Bạn có muốn xóa sổ theo dõi kinh phí'
            secondText={convertDate(selectedExpense?.ThoiGianTao)}
          />
        }
        handleCancel={() => {
          setSelectedExpense(undefined);
        }}
        handleOk={handleDelete}
        loading={loadingDeleteExpense}
      />

      <Loading open={loadingDownloadFileExcel || loadingDownloadFileExcelWord} />
    </div>
  );
};

export default ExpenseList;
