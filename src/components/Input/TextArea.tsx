import type { InputHTMLAttributes, KeyboardEvent } from 'react';
import type { RegisterOptions, UseFormRegister } from 'react-hook-form/dist/types';
import classNames from 'classnames';

type Props = {
  containerClassName?: string;
  register?: UseFormRegister<any>;
  rules?: RegisterOptions;
  errorMessage?: string;
  classNameError?: string;
} & InputHTMLAttributes<HTMLTextAreaElement>;

const TextArea = (props: Props) => {
  const {
    containerClassName = '',
    register,
    name,
    rules,
    errorMessage,
    classNameError = 'text-[14px] text-red-500 mt-1',
    ...rest
  } = props;

  const registerResult = register && name ? register(name, rules) : null;

  const onKeyDown = (e: KeyboardEvent<HTMLTextAreaElement>) => {
    if (rest.type === 'number' && e.code === 'KeyE') {
      e.preventDefault();
    }
  };

  return (
    <div className={classNames(`max-md:w-full ${containerClassName}`)}>
      <textarea
        onKeyDown={onKeyDown}
        className={classNames(
          'w-full rounded-md border border-gray-300 py-3.5 px-4 outline-none focus:border-primary-30 focus:shadow-sm' +
            rest.className,
          {
            'border-red-500 focus:border-red-500': !!errorMessage
          }
        )}
        {...rest}
        {...registerResult}
      />
      {!!errorMessage && <p className={classNameError}>{errorMessage}</p>}
    </div>
  );
};

export default TextArea;
