import { createSearchParams, useNavigate } from 'react-router-dom';
import useQueryConfig, { QueryConfig } from './useQueryConfig';

type Props =
  | ({
      page?: string;
      pageSize?: string;
    } & QueryConfig)
  | undefined;

const usePaginationNavigate = () => {
  const navigate = useNavigate();

  const { pageSize } = useQueryConfig();

  const pathname = window.location.pathname;

  const handleNavigation = (props?: Props) => {
    return navigate({
      pathname: pathname,
      search: createSearchParams({
        ...props,
        page: props?.page || '1',
        pageSize: props?.pageSize || pageSize + ''
      }).toString()
    });
  };

  return handleNavigation;
};

export default usePaginationNavigate;
