import httpThuVien from 'utils/httpThuVien';

const endpoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/ThanhVien`;

const memberApis = {
  getDataBySearch: (value: string) =>
    httpThuVien.post<
      ResponseApi<{ Id: string; MaThanhVien: string; Ten: string; LoaiTK: string; ChucVu: string; LopHoc: string }[]>
    >(`${endpoint}/GetDataFromText?TextForSearch=${value}`),
  getOneData: (id: string) => httpThuVien.post<ResponseApi<Student>>(`${endpoint}/EditById?Id=${id}`)
};

export default memberApis;
