import { Modal, type ModalProps } from 'antd';
import { Button, Select, TreeSelect } from 'components';
import type { ReactNode } from 'react';
import type { Control, FieldPath, FieldValues, UseFormRegister } from 'react-hook-form';

type Props<TFieldValues extends FieldValues> = {
  title: ReactNode;
  label: string;
  name?: FieldPath<any>;
  control?: Control<any>;
  register?: UseFormRegister<any>;
  items: Selections;
  children?: ReactNode;
  onBack: VoidFunction;
  onUpdate: VoidFunction;
  loading: boolean;
  error?: string;
  viewModel?: 'select' | 'treeview';
} & ModalProps;

export default function UpdateModal<TFieldValues extends FieldValues>(props: Props<TFieldValues>) {
  const {
    title,
    label,
    register,
    control,
    open,
    items,
    children,
    onBack,
    onUpdate,
    name,
    loading,
    error,
    viewModel = 'select',
    ...rest
  } = props;

  return (
    <Modal
      closable={false}
      open={open}
      title={<h3 className='uppercase text-primary-10'>{title}</h3>}
      centered
      footer={
        <div className='flex items-center justify-end gap-2'>
          <Button variant='secondary' onClick={onBack} disabled={loading} loading={loading}>
            Quay về
          </Button>

          <Button variant='default' onClick={onUpdate} type='submit' disabled={loading} loading={loading}>
            Cập nhật
          </Button>
        </div>
      }
      {...rest}
    >
      <label className='mb-3 font-bold'>
        {label} <span className='text-danger-10'>*</span>
      </label>

      {viewModel === 'select' && (
        <Select
          items={items}
          className='w-full'
          name={name}
          register={register}
          errorMessage={error}
          disabled={loading}
        />
      )}
      {viewModel === 'treeview' && (
        <TreeSelect
          items={items}
          className='w-full'
          name={name || ''}
          control={control}
          errorMessage={error}
          disabled={loading}
        />
      )}
      {children}
    </Modal>
  );
}
