import { DeleteFilled } from '@ant-design/icons';
import { useMutation } from '@tanstack/react-query';
import { Table, Tooltip } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { studentApis } from 'apis';
import { Button, Empty, Loading, ProgressBar, Title } from 'components';
import { path } from 'constants/path';
import { useMemo, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';

let locale = {
  emptyText: 'Không có dữ liệu'
};

const AddStudentAvatarPage = () => {
  const [step, setStep] = useState<number>(1);
  const [rowErrorData, setRowErrorData] = useState<UploadFileData['ErrorName']>([]);
  const [rowSuccessData, setRowSuccessData] = useState<UploadFileData['RawDataList']>([[]]);
  const [rowSavedData, setRowSavedData] = useState<UploadFileData['ListSuccess']>([]);
  const [page, setPage] = useState(1);
  const [paginationSize, setPaginationSize] = useState(10);

  const fileInutRef = useRef<HTMLInputElement | null>(null);
  const ref = useRef<HTMLDivElement | null>(null);

  const navigate = useNavigate();

  const { mutate: uploadFile, isLoading: uploadingFile } = useMutation({
    mutationFn: studentApis.uploadAvatarFileZip,
    onSuccess: (data) => {
      setRowErrorData(data.data.Item.ErrorName);
      setRowSuccessData(data.data.Item.RawDataList);
      setStep(2);
    }
  });

  const { mutate: saveFile, isLoading: savingFile } = useMutation({
    mutationFn: studentApis.saveAvatar,
    onSuccess: (data) => {
      ref.current?.scrollIntoView({ behavior: 'smooth' });
      setRowSavedData(data.data.Item.ListSuccess);
      setStep(3);
    }
  });

  const columnsStudentData: ColumnsType<any> | undefined = useMemo(() => {
    for (let index = 0; index < rowSuccessData.length; index++) {
      let columns = [
        {
          title: 'STT',
          dataIndex: 'stt',
          key: 'stt',
          render: (value: any, record: any, j: number) => (page - 1) * paginationSize + j + 1
        },
        {
          title: 'Tên học sinh',
          dataIndex: 'name',
          key: 'name',
          render: (value: any, record: any) => {
            return record[0].length > 25 ? (
              <Tooltip placement='topLeft' title={record[0]} arrow={true}>
                <p className='text-center'>{record[0].substring(0, 25).concat('...')}</p>
              </Tooltip>
            ) : (
              <p className='text-center'>{record[0]}</p>
            );
          }
        },
        {
          title: 'Mã học sinh',
          dataIndex: 'gender',
          key: 'gender',
          render: (value: any, record: any[]) => record[1]
        }
      ];
      if (step === 2) {
        columns.push({
          title: 'Ảnh hiện tại',
          dataIndex: 'avatar',
          key: 'avatar',
          // @ts-ignore
          render: (value, record, j) => {
            return (
              <div className='flex items-center justify-center'>
                {step === 2 && (
                  <img
                    src={!record[3] ? '/content/default-avatar.jpeg' : record[3]}
                    alt={record[3]}
                    className='h-[80px] w-[80px]'
                  />
                )}
              </div>
            );
          }
        });
      }

      columns.push({
        title: 'Ảnh đại diện',
        dataIndex: 'actions',
        key: 'actions',
        render: (value: any, record: (string | undefined)[]) => {
          return (
            <>
              <div className='flex items-center justify-center'>
                <img src={record[2]} alt={record[2]} className='h-[80px] w-[80px]' />
              </div>
            </>
          );
        }
      });

      if (step === 2) {
        columns.push({
          title: 'Hành động',
          dataIndex: 'actions',
          key: 'actions',
          render: (value: any, record: string[]) => {
            return (
              <>
                <button
                  className='mx-2'
                  onClick={(e) => {
                    e.stopPropagation();
                    for (let index = 0; index < rowSuccessData.length; index++) {
                      setRowSuccessData(rowSuccessData.filter((item) => item[1] !== record[1]));
                    }
                  }}
                >
                  <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
                </button>
              </>
            );
          }
        });
      }

      return columns;
    }
  }, [page, paginationSize, rowSuccessData, step]);

  const columnsNotMatchTable: ColumnsType<[]> = [
    {
      title: 'STT',
      dataIndex: 'stt',
      key: 'stt',
      render: (value, record, index) => index + 1,
      width: '100px'
    },
    {
      title: 'Tên file',
      dataIndex: 'fileName',
      key: 'fileName',
      render: (value, record, index) => record
    }
  ];

  const handleOnFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files?.[0];

    const form = new FormData();
    if (fileFromLocal) {
      form.append('file', fileFromLocal);

      uploadFile(form);
    }
  };

  const handleAccessFileInputRef = () => {
    fileInutRef.current?.click();
  };

  const handleBack = () => {
    step === 2 ? setStep(1) : navigate(path.hocsinh);
  };

  const handleSubmit = () => {
    saveFile(rowSuccessData);
  };

  return (
    <div className='p-5' ref={ref}>
      <Title title='Cập nhật ảnh đại diện cho học sinh' />

      <div className='my-10'>
        <ProgressBar activeStep={step} type='avatar' />
      </div>

      {/* TODO: Condition are step > 1 and data is not undefined*/}
      {step === 1 && (
        <div className='flex items-center justify-center rounded-md bg-tertiary-10 py-4'>
          <input
            type='file'
            accept='.zip,.rar,.7zip'
            className='hidden'
            ref={fileInutRef}
            onChange={handleOnFileChange}
          />

          <Button variant='default' className='ml-2' onClick={handleAccessFileInputRef}>
            Chọn tệp
          </Button>
        </div>
      )}

      {step > 1 && (
        <div>
          {step === 2 && !!rowErrorData?.length && (
            <>
              <div>
                <div className='w-[350px] rounded-md bg-tertiary-40 px-3 py-4'>
                  <span className='p-0'>
                    Tổng số file không trùng khớp với mã học sinh:{' '}
                    <span className='font-bold'>{rowErrorData.length}</span>
                  </span>
                </div>

                <Table
                  rootClassName='w-[350px]'
                  loading={uploadingFile || savingFile}
                  columns={columnsNotMatchTable}
                  pagination={false}
                  dataSource={rowErrorData}
                  rowKey={(record) => record + ''}
                  className='custom-table'
                  scroll={{ y: 165 }}
                  bordered
                />
              </div>
            </>
          )}

          <p className='mt-4'>
            Tổng số hình ảnh được nhận diện:
            <span className='font-bold'> {rowSuccessData?.length}</span>
          </p>

          {rowSuccessData?.length > 0 && step === 2 && (
            <h3 className='text-primary-10'>DANH SÁCH HỌC SINH ĐƯỢC CẬP NHẬT ẢNH ĐẠI DIỆN</h3>
          )}

          {step === 3 && (
            <>
              <p className='text-tertiary-30'>
                Cập nhật thành công: <span className='font-bold'>{' ' + rowSavedData?.length} học sinh</span>
              </p>

              <p className='text-danger-10'>
                Cập nhật thất bại: <span className='font-bold'>{' ' + rowErrorData?.length} học sinh</span>
              </p>

              <h3 className='uppercase text-primary-10'>DANH SÁCH HỌC SINH CẬP NHẬT ẢNH ĐẠI DIỆN THÀNH CÔNG</h3>
            </>
          )}

          <div className='my-6'>
            {rowSuccessData?.length > 0 && (
              <Table
                loading={uploadingFile || savingFile}
                columns={columnsStudentData}
                dataSource={rowSuccessData || []}
                scroll={{ x: 1200 }}
                rowKey={(record) => record[1]}
                className='custom-table'
                pagination={{
                  onChange(current, pageSize) {
                    setPage(current);
                    setPaginationSize(pageSize);
                  },
                  defaultPageSize: 10,
                  hideOnSinglePage: true,
                  showSizeChanger: false
                }}
                locale={{
                  emptyText: () => <Empty label={locale.emptyText} />
                }}
                bordered
              />
            )}
          </div>
        </div>
      )}

      <div className='mr-10 mt-2 flex items-center justify-end'>
        <>
          {step < 3 && (
            <>
              <Button variant='secondary' onClick={handleBack}>
                Quay về
              </Button>

              {!!rowSuccessData?.[0]?.length && (
                <Button variant='default' className='ml-2' onClick={handleSubmit}>
                  Tiếp tục
                </Button>
              )}
            </>
          )}

          {step === 3 && (
            <Button variant='default' className='ml-2' onClick={handleBack}>
              Hoàn tất
            </Button>
          )}
        </>
      </div>

      <Loading open={uploadingFile || savingFile} />
    </div>
  );
};

export default AddStudentAvatarPage;
