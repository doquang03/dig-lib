import { DatePicker } from 'antd';
import { Input, Select, Title } from 'components';
import { FORMAT_DATE_PICKER } from 'constants/config';
import locale from 'antd/es/date-picker/locale/vi_VN';

const DOCUMENT_TYPE_OPTIONS = [
  {
    label: 'Chọn loại tài liệu',
    value: ''
  },
  {
    label: 'Hình ảnh',
    value: 'image'
  },
  {
    label: 'Tài liệu',
    value: 'document'
  },
  {
    label: 'Âm thanh',
    value: 'audio'
  },
  {
    label: 'Video',
    value: 'video'
  }
];

function HostDocumentPage() {
  return (
    <div>
      <Title title='Danh sách tài liệu mục lục liên hợp' />

      <form action='' className='flex items-center bg-secondary-10 p-2'>
        <Input containerClassName='w-full' placeholder='Nhập tên tài liệu, ISB, tác giả' />

        <Select items={DOCUMENT_TYPE_OPTIONS} className='w-full' />

        {/* <DatePicker
          format={FORMAT_DATE_PICKER}
          className='w-full py-2'
          placeholder='Chọn ngày'
          clearIcon={false}
          locale={locale}
          value={dayjs(form.ThoiGianTao)}
          disabledDate={(current) => {
            return (
              (current && current > dayjs().endOf('day')) || current < dayjs(date).endOf('day').subtract(1, 'year')
            );
          }}
          onChange={handleChangedate}
          disabled={(Boolean(id) && loadingExpense) || loadingCreate || loadingUpdate}
        /> */}
      </form>
    </div>
  );
}

export default HostDocumentPage;
