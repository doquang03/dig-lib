import httpThuVien from 'utils/httpThuVien';

const endpoint = `${process.env.REACT_APP_SERVICE_MLLH}/api/Books`;
const unitEndpoint = `${process.env.REACT_APP_SERVICE_MLLH}/api/HostLibrary`;

const pullRequestApis = {
  getBookInPullRequest: (params: {
    page: number;
    pageSize: number;
    SortBookName?: boolean;
    SortPublishDate?: boolean;
    SortTime?: boolean;
    IsDocument?: boolean;
    ListQuery?: { Operator: string; SearchField: string; Value: string }[];
  }) =>
    httpThuVien.post<ResponseApi<{ ListModel: PullRequestBook[]; count: number }>>(`${endpoint}/SearchOpac`, params),

  getPullRequestDetail: (id: string) =>
    httpThuVien.get<
      ResponseApi<{
        Marc21: string;
        ListParam: string[];
        ListMedia: FileContent[];
        LinkBiaSach: FileContent;
      }>
    >(`${endpoint}/DetailBooks/${id}`),
  getListOfUnits: () => httpThuVien.get<ResponseApi<{ DonVi: Option[] }>>(`${unitEndpoint}/ListModelUnit`)
};

export default pullRequestApis;
