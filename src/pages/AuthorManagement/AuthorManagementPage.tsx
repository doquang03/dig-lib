import { path } from 'constants/path';
import { Outlet, useMatch } from 'react-router-dom';
import AuthorManagermentContent from './AuthorManagermentContent/AuthorManagermentContent';

const AuthorManagementPage = () => {
  const match = useMatch(path.tacgia);

  return <>{Boolean(match) ? <AuthorManagermentContent /> : <Outlet />}</>;
};

export default AuthorManagementPage;
