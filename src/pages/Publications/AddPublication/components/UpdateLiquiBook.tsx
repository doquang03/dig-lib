import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation } from '@tanstack/react-query';
import { Modal, ModalProps } from 'antd';
import { bookApis } from 'apis';
import { UpdateSachCaBietVars } from 'apis/book.apis';
import { Button, DatePicker, Input, Select } from 'components';
import dayjs from 'dayjs';
import { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import { updateSachCaBietSchema } from 'utils/rules';
import { convertDate } from 'utils/utils';

type Props = {
  onHide: VoidFunction;
  suppliers: NguonCungCap[];
  bookStatus: StatusBook[];
  selectedSachCaBiets: TrangThaiSachVM[];
  onSuccess: VoidFunction;
  selectedSachCaBiet?: TrangThaiSachVM;
} & ModalProps;

const UpdateLiquiBook = (props: Props) => {
  const { open, suppliers, bookStatus, selectedSachCaBiets, selectedSachCaBiet, onHide, onSuccess } = props;

  const {
    register,
    handleSubmit,
    control,
    reset,
    setValue,
    formState: { errors }
  } = useForm<UpdateSachCaBietVars>({
    defaultValues: {
      IdTrangThai: '',
      SoChungTu: '',
      SoVaoSoTongQuat: '',
      IdNguonCungCap: ''
    },
    resolver: yupResolver(updateSachCaBietSchema)
  });

  const { mutate, isLoading } = useMutation({
    mutationFn: bookApis.updateSachCaBiet,
    onSuccess: (res, data) => {
      reset();
      toast.success('Cập nhật thành công');
      onHide();
      onSuccess();
      const listFail = res.data.Item.ListFail;
      for (let i = 0; i < data.length; i++) {
        const element = data[i];
        let value = -1;
        listFail.forEach((item, i) => {
          if (item.Id === element.Id) {
            value = i;
            return;
          }
        });
        if (value === -1) {
          Object.assign(selectedSachCaBiets[i], data[i]);
        }
      }
    }
  });

  useEffect(() => {
    if (!selectedSachCaBiet) return;

    setValue('IdNguonCungCap', selectedSachCaBiet.IdNguonCungCap);
    setValue('IdTrangThai', selectedSachCaBiet.IdTrangThai);
    setValue('SoChungTu', selectedSachCaBiet.SoChungTu as string);
    setValue('SoVaoSoTongQuat', selectedSachCaBiet.SoVaoSoTongQuat as string);
    setValue('NgayVaoSo_String', selectedSachCaBiet.NgayVaoSo_String as string);
  }, [selectedSachCaBiet, setValue]);

  useEffect(() => {
    if (!selectedSachCaBiets.length) return;

    if (selectedSachCaBiets.length > 1) {
      setValue('IdNguonCungCap', '');
      setValue('IdTrangThai', '');
      setValue('SoChungTu', '');
      setValue('SoVaoSoTongQuat', '');
      setValue('NgayVaoSo_String', '');
      return;
    }

    setValue('IdNguonCungCap', selectedSachCaBiets[0].IdNguonCungCap);
    setValue('IdTrangThai', selectedSachCaBiets[0].IdTrangThai as string);
    setValue('SoChungTu', selectedSachCaBiets[0].SoChungTu as string);
    setValue('SoVaoSoTongQuat', selectedSachCaBiets[0].SoVaoSoTongQuat as string);
    // @ts-ignore
    setValue('NgayVaoSo_String', selectedSachCaBiets[0].NgayVaoSo_Str || selectedSachCaBiets[0].NgayVaoSo_String);
  }, [selectedSachCaBiets, setValue]);

  const handleOnHide = () => {
    onHide();
  };

  const suppliersSelections =
    suppliers?.map(({ Id, TenNguonCungCap }) => {
      return {
        value: Id,
        label: TenNguonCungCap
      };
    }) || [];

  const bookStatusSelections =
    bookStatus?.map(({ Id, TenTT }) => {
      return {
        value: Id,
        label: TenTT
      };
    }) || [];

  const onSubmit = handleSubmit((data) => {
    const { IdNguonCungCap, IdTrangThai, SoChungTu, SoVaoSoTongQuat, NgayVaoSo_String } = data;

    const result = selectedSachCaBiets.map((x) => {
      return {
        Id: x.Id || '',
        IdSach: x.IdSach || '',
        IdTrangThai,
        IdNguonCungCap,
        SoChungTu,
        SoVaoSoTongQuat,
        NgayVaoSo_String: convertDate(NgayVaoSo_String)
      };
    });

    selectedSachCaBiet
      ? mutate([
          {
            Id: selectedSachCaBiet.Id,
            IdSach: selectedSachCaBiet.IdSach,
            IdTrangThai,
            IdNguonCungCap,
            SoChungTu,
            SoVaoSoTongQuat,
            NgayVaoSo_String: convertDate(NgayVaoSo_String)
          }
        ])
      : mutate(result);
  });

  return (
    <Modal
      open={open}
      title={<h3 className='border-b-2 pb-2 font-bold uppercase text-primary-10'>Cập nhật sách cá biệt</h3>}
      closable={false}
      footer={null}
    >
      <form onSubmit={onSubmit}>
        <div className='grid grid-cols-12'>
          <label className='col-span-4 self-center font-bold'>
            Tình trạng sách <span className='text-danger-10'>*</span>
          </label>
          <div className='col-span-8 w-full'>
            <Select
              items={[{ value: '', label: 'Chọn tình trạng' }, ...bookStatusSelections]}
              className='w-full'
              register={register}
              name='IdTrangThai'
              errorMessage={errors.IdTrangThai?.message}
            />
          </div>
        </div>

        <div className='my-3 flex items-center justify-between'>
          <label className='font-bold'>Số chứng từ</label>
          <Input placeholder='CT1/1' containerClassName='w-2/3' register={register} name='SoChungTu' maxLength={100} />
        </div>

        <div className='my-3 flex items-center justify-between'>
          <label className='font-bold'>Số vào sổ tổng quát</label>
          <Input
            placeholder='12/34'
            containerClassName='w-2/3'
            register={register}
            name='SoVaoSoTongQuat'
            maxLength={100}
          />
        </div>

        <div className='my-3 flex items-center justify-between'>
          <label className='font-bold'>
            Ngày vào sổ <span className='text-danger-10'>*</span>
          </label>
          <div className='w-2/3'>
            <DatePicker
              placeholder='01/01/2023'
              control={control}
              name={'NgayVaoSo_String'}
              disabled={false}
              disabledDate={(current) => {
                return current && current > dayjs().endOf('day');
              }}
              errorMessage={errors?.NgayVaoSo_String?.message}
            />
          </div>
        </div>

        <div className='grid grid-cols-12'>
          <label className='col-span-4 self-center font-bold'>
            Nguồn cung cấp <span className='text-danger-10'>*</span>
          </label>

          <div className='col-span-8 w-full'>
            <Select
              items={[{ value: '', label: 'Chọn nguồn cung cấp' }, ...suppliersSelections]}
              className='w-full'
              placeholder='Chọn nguồn cung cấp'
              register={register}
              name='IdNguonCungCap'
              errorMessage={errors?.IdNguonCungCap?.message}
            />
          </div>
        </div>

        <div className='mt-4 flex items-center justify-end gap-2'>
          <Button variant='secondary' onClick={handleOnHide} type='button' loading={isLoading} disabled={isLoading}>
            Quay về
          </Button>

          <Button loading={isLoading} disabled={isLoading}>
            Cập nhật
          </Button>
        </div>
      </form>
    </Modal>
  );
};

export default UpdateLiquiBook;
