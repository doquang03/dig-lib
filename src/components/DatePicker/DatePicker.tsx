import { DatePicker, type DatePickerProps } from 'antd';
import classNames from 'classnames';
import dayjs from 'dayjs';
import { PickerMode } from 'rc-picker/lib/interface';
import { Control, Controller, FieldValues, Path } from 'react-hook-form';

type RHFDatePickerFieldProps<FormValues extends FieldValues> = {
  control: Control<FormValues>;
  name: Path<FormValues>;
  className?: string;
  style?: React.CSSProperties;
  placeholder?: string;
  errorMessage?: string;
  disabled?: boolean;
  disabledDate?: (current: dayjs.Dayjs) => boolean;
  onChangeOut?: Function;
  picker?: Exclude<PickerMode, 'date' | 'time'>;
} & DatePickerProps;

const dateFormatList = ['DD/MM/YYYY', 'DD-MM-YYYY'];
const yearFormatList = ['YYYY'];
const monthFormatList = ['MM/YYYY', 'MM-YYYY'];
function RHFDatePickerField<FormValues extends FieldValues>(props: RHFDatePickerFieldProps<FormValues>) {
  const {
    control,
    name,
    placeholder = 'Chọn ngày',
    errorMessage,
    disabledDate,
    disabled,
    picker,
    onChangeOut,
    className,
    style,
    ...rest
  } = props;
  return (
    <>
      <Controller
        control={control}
        name={name}
        render={({ field: { onChange, value } }) => {
          return (
            <DatePicker
              value={
                value
                  ? picker === 'year'
                    ? dayjs(value, yearFormatList)
                    : picker === 'month'
                    ? dayjs(value, monthFormatList[0])
                    : dayjs(value, dateFormatList[0])
                  : null
              }
              placeholder={placeholder}
              onChange={(date) => {
                if (onChangeOut) onChangeOut(date);
                onChange(date);
              }}
              format={picker === 'year' ? yearFormatList : picker === 'month' ? monthFormatList : dateFormatList}
              picker={picker}
              className={classNames(className, 'w-full border border-gray-300 py-[0.8rem] px-2 outline-none', {
                'border border-red-500': !!errorMessage
              })}
              style={style}
              disabledDate={disabledDate}
              disabled={disabled}
              {...rest}
            />
          );
        }}
      />
      {!!errorMessage && <p className='mt-1 text-[14px] text-red-500'>{errorMessage}</p>}
    </>
  );
}

export default RHFDatePickerField;
