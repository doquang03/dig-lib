import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation } from '@tanstack/react-query';
import { Modal, ModalProps } from 'antd';
import { bookStoreApis } from 'apis';
import { AxiosError } from 'axios';
import { Button, Input, InputNumber } from 'components';
import { useMemo } from 'react';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import { MaSachCaBiet } from 'utils/rules';

type Props = {
  khoSelect: BookStoreType;
  onHide: VoidFunction;
  onSuccess: VoidFunction;
} & ModalProps;

type FormInput = {
  MaCBMoi: string;
};

const SettingDKCB = (props: Props) => {
  const {
    control,
    handleSubmit,
    reset,
    formState: { errors }
  } = useForm<FormInput>({
    defaultValues: {
      MaCBMoi: ''
    },
    resolver: yupResolver(MaSachCaBiet)
  });
  const { khoSelect, open, onHide, onSuccess } = props;

  const { mutate: settingMaDKCB, isLoading } = useMutation({
    mutationFn: (maCaBietMoi: number) => bookStoreApis.SuaSoDKCBKho(maCaBietMoi, khoSelect.Id),
    onSuccess: (res, data) => {
      toast.success('Thay đổi số đếm đăng kí cá biệt thành công');
      khoSelect.MaCBCount = data;
      onHide();
      onSuccess();
      reset();
    }
  });

  const handleComplete = handleSubmit((data: any) => {
    settingMaDKCB(+data.MaCBMoi || 0);
  });

  return (
    <Modal
      open={open}
      title={<h3 className='border-b-2 pb-2 font-bold uppercase text-primary-10'>Vị trí đặt lại mã cá biệt</h3>}
      closable={false}
      footer={null}
    >
      <form onSubmit={handleComplete}>
        <div className='mb-4 flex flex-col'>
          <label className='col-span-3 mb-4 text-base'>
            Sửa lại số đếm đkcb của kho có thể làm sai dữ liệu, bạn chấp nhận rủi ro và tự chịu trách nhiệm khi thay đổi
            số đếm đkcb.
          </label>
        </div>
        <div className='mb-4 flex flex-col'>
          <div className='col-span-9 flex items-center justify-between gap-1'>
            <InputNumber
              rootClassName='grow'
              name='MaCBMoi'
              placeholder='Nhập số đếm đăng kí cá biệt'
              control={control}
              maxLength={4}
            />
          </div>
          {!!errors && <p className='mt-1 text-[14px] text-red-500'>{errors.MaCBMoi?.message}</p>}
        </div>

        <div className='mt-3 flex items-center justify-end gap-2'>
          <Button type='button' variant='secondary' onClick={onHide} loading={isLoading} disabled={isLoading}>
            Quay về
          </Button>

          <Button loading={isLoading} disabled={isLoading}>
            Cập nhật
          </Button>
        </div>
      </form>
    </Modal>
  );
};

export default SettingDKCB;
