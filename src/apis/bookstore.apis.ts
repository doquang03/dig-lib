import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/KhoSach`;

export type GetListSachCaBietByIdKho = {
  page?: number;
  pageSize?: number;
  idKho?: string;
  TenSach?: SearchInputField;
  MaKiemSoat?: SearchInputField;
  MaKiemSoatCaBiet?: SearchInputField;
  SoCT?: SearchInputField;
  SoVSTQ?: SearchInputField;
  NgayVaoSo?: SearchRangeField;
  NguonCungCap?: SearchInputField;
  TrangThai?: SearchInputField;
  TextForSearch?: string;
  DaTra?: boolean;
  isSortTenSach?: boolean;
  isSortMaDKCB?: boolean;
  isSortNgayVaoSo?: boolean;
};

const bookStoreApis = {
  getBookstoreOptions: (id: string = '') => httpThuVien.get<ResponseApi<Selections>>(`${endPoint}/GetOptions?id=${id}`),
  XoaBoSCB: (listSCB: Array<SachCaBiet>, isResetMaCbCount: Boolean = false) => {
    return httpThuVien.post<ResponseApi<Selections>>(
      `${endPoint}/XoaBoSCB?isResetMaCbCount=${isResetMaCbCount}`,
      listSCB
    );
  },
  ChuyenKhoSach: (listIdSCB: string[], idKhoSachMoi: string, idKhoSachCu: string, isResetMaCB: boolean = false) =>
    httpThuVien.post(
      `/${endPoint}/ChuyenKhoSach?idKhoSachMoi=${idKhoSachMoi}&idKhoSachCu=${idKhoSachCu}&isResetMaCB=${isResetMaCB}`,
      listIdSCB
    ),
  ThayDoiDauSach: (listIdSCB: string[], idSach: string) =>
    httpThuVien.post(`/${endPoint}/ThayDoiDauSach?idSach=${idSach}`, listIdSCB),
  GetListSCB: (payload: GetListSachCaBietByIdKho, isColapse: boolean = false, showBookDeleted = false) => {
    return httpThuVien.post<
      ResponseApi<{
        ListKhoSach: BookStoreType[];
        List_NguonCungCap: NguonCungCap[];
        List_Sach: Book[];
        List_SachCB: Array<SachCaBiet & { LinkBiaSach?: string }>;
        List_TrangThai: StatusBook[];
        List_ViTriSach: number;
        Position_Kho: number;
        TotalCount: number;
      }>
    >(`${endPoint}/GetListSCB?idKho=${payload.idKho}&isColapse=${isColapse}&ShowDeleted=${showBookDeleted}`, payload);
  },
  XuatMauPhichSach_ActionResult: (listIdSachCaBiet: string[], tuychon: string, IdKho: string) =>
    httpThuVien.post(`/${endPoint}/XuatMauPhichSach_ActionResult?tuychon=${tuychon}&IdKho=${IdKho}`, listIdSachCaBiet, {
      responseType: 'blob'
    }),
  UpdateSCB: (idSCB: string, maCaBietMoi: string) =>
    httpThuVien.post(`/${endPoint}/SuaMaCaBiet?idSCB=${idSCB}&maCaBietMoi=${maCaBietMoi}`),
  ResetSachCaBietSelect: (data: object, idKho: string, Position_Kho: number) =>
    httpThuVien.post(`/${endPoint}/ResetSachCaBiet?idKho=${idKho}&Position_Kho=${Position_Kho}`, data),
  ResetSachCaBietAll: (idKho: string) => httpThuVien.post(`/${endPoint}/ResetSachCaBietAll?idKho=${idKho}`),
  SuaSoDKCBKho: (soDemDKCB: number, idKho: string) =>
    httpThuVien.post(`/${endPoint}/SuaSoDKCBKho?soDemDKCB=${soDemDKCB}&idKho=${idKho}`),
  PreviewExcel_Thuong: (payload: object) => {
    return httpThuVien({
      method: 'post',
      url: `/${endPoint}/PreviewExcel_Thuong`,
      data: payload,
      headers: { 'Content-Type': 'multipart/form-data' }
    });
  },
  TemplateExcel_Thuong: () => {
    return httpThuVien.post(`/${endPoint}/TemplateExcel`, {}, { responseType: 'arraybuffer' });
  },
  ImportSave_Thuong: (
    data: Array<Array<string>>,
    idKhoSach: string,
    IdUserAdmin: string,
    UserName: string,
    isThemKhoSach: boolean,
    fileName: string
  ) => {
    return httpThuVien.post(
      `/${endPoint}/SaveExcel_Thuong?codeBST=sach&idKhoSach=${idKhoSach}&IdUserAdmin=${IdUserAdmin}&UserName=${UserName}&fileName=${fileName}&isThemKhoSach=${isThemKhoSach}`,
      data
    );
  },
  PreviewExcel_SCB: (payload: object, IdTrangThai: string) => {
    return httpThuVien({
      method: 'post',
      url: `/${endPoint}/PreviewExcel_SCB?IdTrangThai=${IdTrangThai}`,
      data: payload,
      headers: { 'Content-Type': 'multipart/form-data' }
    });
  },
  TemplateExcel_SCB: () => {
    return httpThuVien.post(`/${endPoint}/TemplateExcel_SCB`, {}, { responseType: 'arraybuffer' });
  },
  ImportSave_SCB: (
    data: Array<Array<string>>,
    idKhoSach: string,
    IdUserAdmin: string,
    UserName: string,
    isThemKhoSach: boolean,
    fileName: string,
    IdTrangThai: string
  ) => {
    return httpThuVien.post(
      `/${endPoint}/SaveExcel_SCB?codeBST=sach&idKhoSach=${idKhoSach}&IdUserAdmin=${IdUserAdmin}&UserName=${UserName}&fileName=${fileName}&isThemKhoSach=${isThemKhoSach}&IdTrangThai=${IdTrangThai}`,
      data
    );
  },
  PreviewExcel_SCB_SGK: (payload: object, IdTrangThai: string) => {
    return httpThuVien({
      method: 'post',
      url: `/${endPoint}/PreviewExcel_SCB_SGK?IdTrangThai=${IdTrangThai}`,
      data: payload,
      headers: { 'Content-Type': 'multipart/form-data' }
    });
  },
  TemplateExcel_SCB_SGK: () => {
    return httpThuVien.post(`/${endPoint}/TemplateExcel_SCB_SGK`, {}, { responseType: 'arraybuffer' });
  },
  ImportSave_SCB_SGK: (
    data: {
      [key: string]: Array<Array<string>>;
    },
    idKhoSach: string,
    IdUserAdmin: string,
    UserName: string,
    isThemKhoSach: boolean,
    fileName: string,
    IdTrangThai: string
  ) => {
    return httpThuVien.post(
      `/${endPoint}/SaveExcel_SCB_SGK?codeBST=sach&idKhoSach=${idKhoSach}&IdUserAdmin=${IdUserAdmin}&UserName=${UserName}&fileName=${fileName}&isThemKhoSach=${isThemKhoSach}&IdTrangThai=${IdTrangThai}`,
      data
    );
  },
  danhSachTrongKho: (
    payload: Partial<{
      MaDKCB: SearchInputField;
      TenSach: SearchInputField;
      IdKhoSach: string;
      TacGia: SearchInputField;
      NamXuatBan: SearchInputField;
      ISBN: SearchInputField;
      IdTinhTrang: string;
      SortDKCB: boolean;
      SortTenSach: boolean;
      SortKhoSach: boolean;
      SortTacGia: boolean;
      SortNamXuatBan: boolean;
      SortISBN: boolean;
      page: number;
      pageSize: number;
      currentId: string[];
    }>
  ) =>
    httpThuVien.post<ResponseApi<{ ListSCB: BooksInStoreAudit[]; count: number }>>(
      `/${endPoint}/DanhSachTrongKho`,
      payload
    ),
  EditBySCB: (code: string) => {
    return httpThuVien.post(`/${endPoint}/EditBySCB?code=${code}`);
  }
};

export default bookStoreApis;
