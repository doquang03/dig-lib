import { Table, Tooltip } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { Empty, Title } from 'components';
import { useMemo, useState } from 'react';
import { getSerialNumber } from 'utils/utils';

type Props = {
  documentType: 'import' | 'export';
  data: any;
};

const TotalDocuments = (props: Props) => {
  const { documentType, data } = props;
  const [page, setPage] = useState(1);

  const PhanI = useMemo(() => {
    return data?.data?.Item.PhanI;
  }, [data?.data?.Item.PhanI]);

  const PhanII = useMemo(() => {
    return data?.data?.Item.PhanII;
  }, [data?.data?.Item.PhanII]);

  const List_KhoSach = useMemo(() => {
    if (!data?.data?.Item.List_KhoSach) return [];
    return data?.data?.Item.List_KhoSach;
  }, [data?.data?.Item.List_KhoSach]);
  const ListBoSuuTap = useMemo(() => {
    if (!data?.data?.Item.ListBoSuuTap) return [];
    return data?.data?.Item.ListBoSuuTap;
  }, [data?.data?.Item.ListBoSuuTap]);

  const columns: ColumnsType<any> = [
    { key: 'NgayVaoSo', dataIndex: 'NgayVaoSo', title: 'Ngày vào sổ' },
    {
      key: 'stt',
      dataIndex: 'STT',
      render: (value, record, index) => getSerialNumber(page, 10, index),
      title: 'Số thứ tự'
    },
    {
      key: 'ncc',
      dataIndex: 'NguonCungCap',
      title: 'Nguồn cung cấp',
      render: (value: any, record: any) =>
        record?.NguonCungCap?.length > 30 ? (
          <Tooltip placement='topLeft' title={record.NguonCungCap} arrow={true}>
            <p className='text-center'>{record.NguonCungCap.substring(0, 30).concat('...')}</p>
          </Tooltip>
        ) : (
          <p className='text-center'>{record.NguonCungCap}</p>
        )
    },
    {
      key: 'soCT',
      dataIndex: 'SoChungTu',
      title: 'Số CT',
      render: (value: any, record: any) =>
        record?.SoChungTu?.length > 30 ? (
          <Tooltip placement='topLeft' title={record.SoChungTu} arrow={true}>
            <p className='text-center'>{record.SoChungTu.substring(0, 30).concat('...')}</p>
          </Tooltip>
        ) : (
          <p className='text-center'>{record.SoChungTu}</p>
        )
    },
    {
      key: 'TongSo',
      dataIndex: 'TongSo',
      title: 'Tổng số',
      children: [
        ...(ListBoSuuTap.map((store: Collection, index: number) => {
          return {
            key: store.Id,
            dataIndex: store.Id,
            title: store.Name,
            render: (value: any, record: any) => {
              if (record && record.List_BoSuuTap && record.List_BoSuuTap.length > 0) {
                const list = record.List_BoSuuTap.filter((_: any) => _.MaTL === store.Id);
                if (list.length > 0) return list[0].SoLuong;
                else return 0;
              } else return 0;
            }
          };
        }) || []),
        { key: 'GiaTien', dataIndex: 'GiaTien', title: 'Giá tiền' }
      ]
    },
    {
      key: 'PhanLoaiTaiLieu',
      dataIndex: 'PhanLoaiTaiLieu',
      title: 'Phân loại tài liệu',
      children: [
        {
          key: 'noiDung',
          dataIndex: 'noiDung',
          title: 'a) Nội Dung',
          children:
            List_KhoSach.map((store: BookStoreType, index: number) => {
              return {
                key: store.Id,
                dataIndex: store.Id,
                title: store.Ten,
                render: (value: any, record: any) => {
                  if (record && record.List_SLTLS && record.List_SLTLS.length > 0) {
                    const list = record.List_SLTLS.filter((_: any) => _.MaTL === store.Id);
                    if (list.length > 0) return list[0].SoLuong;
                    else return 0;
                  } else return 0;
                }
              };
            }) || []
        },
        {
          key: 'ngonNgu',
          dataIndex: 'ngonNgu',
          title: 'b) Ngôn ngữ',
          children: [
            { key: 'VN', dataIndex: 'VN', title: 'Tiếng Việt' },
            { key: 'EN', dataIndex: 'EN', title: 'Tiếng Anh' },
            { key: 'NgonNguKhac', dataIndex: 'NgonNguKhac', title: 'Khác' }
          ]
        }
      ]
    },
    {
      key: 'GhiChu',
      dataIndex: 'GhiChu',
      title: 'Ghi chú',
      render: (value: any, record: any) =>
        record?.GhiChu?.length > 30 ? (
          <Tooltip placement='topLeft' title={record.GhiChu} arrow={true}>
            <p className='text-center'>{record.GhiChu.substring(0, 30).concat('...')}</p>
          </Tooltip>
        ) : (
          <p className='text-center'>{record.GhiChu}</p>
        )
    }
  ];

  return (
    <div>
      <Title
        title={documentType === 'import' ? 'Phần I. Tổng số tài liệu nhập kho' : 'Phần II. Tổng số tài liệu xuất kho'}
      />

      <div className='mt-3'>
        <Table
          locale={{
            emptyText: () => <Empty />
          }}
          columns={columns}
          className='custom-table'
          bordered
          dataSource={documentType === 'import' ? PhanI : PhanII}
          scroll={{ x: true }}
          pagination={{
            onChange: (page: number, pageSize: number) => {
              setPage(page);
            },
            defaultPageSize: 10,
            hideOnSinglePage: true,
            showSizeChanger: false,
            showQuickJumper: true,
            locale: { jump_to: '', page: '' }
          }}
        />
      </div>
    </div>
  );
};

export default TotalDocuments;
