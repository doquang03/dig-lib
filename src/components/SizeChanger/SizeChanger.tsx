import classNames from 'classnames';
import { usePaginationNavigate, useQueryConfig } from 'hooks';
import { ChangeEvent, useMemo } from 'react';

type Props = {
  visible: boolean;
  value?: string;
  currentPage?: string;
  total: string;
  pageSizeDefault?: string;
  onChange?: (pageSize: string) => void;
};

const SizeChanger = (props: Props) => {
  const { visible, total, onChange, value, currentPage, pageSizeDefault } = props;

  const queryConfig = useQueryConfig(pageSizeDefault);
  let page: string | undefined;
  let pageSize: string | undefined;
  if (!currentPage) {
    page = queryConfig.page;
    pageSize = queryConfig.pageSize;
  } else {
    page = currentPage;
    pageSize = value;
  }

  const handleNavigation = usePaginationNavigate();

  const handleSelectQuantity = (event: ChangeEvent<HTMLSelectElement>) => {
    const { value: pageSize } = event.target;

    if (onChange) {
      return onChange?.(pageSize);
    }

    return handleNavigation({ ...queryConfig, page: '1', pageSize });
  };

  const resultTotal = useMemo(() => {
    let result = 0;

    if (page && pageSize) {
      let temp = +page * +pageSize;

      if (temp - +total >= 0) {
        result = +total;
      } else if (temp - +total < 0) {
        result = +page * +pageSize;
      }
    }

    return result;
  }, [page, pageSize, total]);

  const currentPageSize = useMemo(() => {
    if (page && pageSize) {
      return +page * +pageSize - +pageSize + 1;
    }
  }, [page, pageSize]);

  const PAGE_SIZE_OPTIONS = [
    { label: pageSizeDefault || 30, value: pageSizeDefault || 30 },
    { label: (+(pageSizeDefault || 30) / 3) * 5, value: (+(pageSizeDefault || 30) / 3) * 5 },
    { label: (+(pageSizeDefault || 30) / 3) * 10, value: (+(pageSizeDefault || 30) / 3) * 10 }
  ];

  return (
    <>
      {visible && !!total && (
        <div className={classNames('flex items-center')}>
          <select
            className={`rounded-md border border-gray-300 py-[0.85rem] px-1 outline-none focus:border-primary-30 focus:shadow-sm `}
            onChange={handleSelectQuantity}
            value={value}
          >
            {PAGE_SIZE_OPTIONS.map(({ label, value }) => (
              <option value={value} key={value}>
                {label}
              </option>
            ))}
          </select>

          <p className='ml-2 mt-3 text-primary-10'>
            Hiển thị {currentPageSize} - {resultTotal} của {total}
          </p>
        </div>
      )}
    </>
  );
};

export default SizeChanger;
