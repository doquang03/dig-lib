import { DeleteFilled, DownloadOutlined } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Radio, Table, Tooltip } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { bookStoreApis } from 'apis';
import bookStoreTypeApis from 'apis/bookStoreType.apis';
import { Button, Loading, ProgressBar, Title } from 'components';
import { MacDinhThuTuKhoSach } from 'constants/bookstore';
import { path } from 'constants/path';
import { UserConText } from 'contexts/user.context';
import 'css/AddBookNormalByExcel.css';
import { useContext, useMemo, useRef, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { download, saveByteArray } from 'utils/utils';

type BookColumsType = Book & {
  STT: number;
  ListError?: Array<string>;
  cellError?: Array<number>;
  SoLuong?: number;
  TrangThai?: string;
  MaNCC?: string;
  SoChungTu?: string;
  SoVaoSoTongQuat?: string;
};

const AddBookNormalByExcel = () => {
  const { storeId } = useParams();
  const [currentStep, setCurrentStep] = useState<number>(1);
  const [isDay19, setIsDay19] = useState<boolean>();
  const [rawDataList, setRawDataList] = useState([]);
  // Mock state.
  const [listBookExcel, setListBookExcel] = useState<Array<BookColumsType>>([]);
  const [saveError, setSaveError] = useState(0);
  const [saveSuccess, setSaveSuccess] = useState(0);
  const [SaveSuccessSCB, setSaveSuccessSCB] = useState(0);
  const [listFail, setListFail] = useState([]);
  const [link, setLink] = useState<string>('');
  const [isCreate, setIsCreate] = useState<boolean>(true);

  const [fileName, setFileName] = useState('');

  const fileInutRef = useRef<HTMLInputElement | null>(null);
  const ref = useRef<HTMLDivElement | null>(null);

  const [page, setPage] = useState(1);
  const [paginationSize, setPaginationSize] = useState(10);

  const user = useContext(UserConText);

  const navigate = useNavigate();

  const { data, isLoading } = useQuery({
    queryFn: () => bookStoreTypeApis.EditByID(storeId as string),
    queryKey: ['bookStoreTypeEditByID'],
    enabled: !!storeId
  });

  const TenKho = useMemo(() => {
    return data?.data.Item.Ten;
  }, [data]);

  const { mutate: PreviewImport, isLoading: isLoadingPreviewImport } = useMutation({
    mutationFn: (file: File) => {
      let bodyFormData = new FormData();
      bodyFormData.append('file', file as Blob);
      return bookStoreApis.PreviewExcel_Thuong(bodyFormData);
    },
    onSuccess: (res) => {
      let tempData = res?.data?.Item?.RawDataList;
      tempData.forEach((element: any, index: number) => {
        element[0] = index.toString();
      });
      setRawDataList(tempData);
      setIsDay19(res?.data?.Item.isDay19);
      let UnitData: BookColumsType;
      let result: Array<BookColumsType> = [];
      let count: number;
      for (let i = 0; i < tempData.length; i++) {
        count = 0;
        UnitData = { STT: i };
        // eslint-disable-next-line no-loop-func
        tempData[i].forEach((element: string) => {
          while (
            count < MacDinhThuTuKhoSach.length && //Kiểm tra số đếm ít hơn số cột
            ((res?.data?.Item.isDay19 && count === 11) || // Kiểm tra IsDay19 disable cột DDC
              (!res?.data?.Item.isDay19 && count === 10)) // Kiểm tra IsDay19 disable cột Day19
          ) {
            count++;
          }
          // @ts-ignore
          UnitData[MacDinhThuTuKhoSach[count++]] = element;
        });
        UnitData.STT = i;
        result.push(UnitData);
      }
      setListBookExcel(result);
      setCurrentStep(2);
    }
  });

  const { mutate: ImportSave, isLoading: isLoadingImportSave } = useMutation({
    mutationFn: (payload: Array<Array<string>>) =>
      bookStoreApis.ImportSave_Thuong(
        payload,
        storeId as string,
        user.profile?.Id as string,
        user.profile?.UserName as string,
        isCreate,
        fileName
      ),
    onSuccess: (res) => {
      let tempData = res?.data?.Item;
      setSaveError(tempData?.SaveError);
      setSaveSuccess(tempData?.SaveSuccess);
      setSaveSuccessSCB(tempData?.SaveSuccess_SachCaBiet);
      setListFail(tempData?.ListFail);
      setLink(tempData?.MemoryStream);
      let listShow = tempData?.ListFail;
      let resultFinal: Array<BookColumsType> = [];
      let tempFor: BookColumsType;
      for (let i = 0; i < listShow.length; i++) {
        tempFor = listShow[i];
        tempFor.STT = i;
        resultFinal.push(tempFor);
      }
      setListBookExcel(resultFinal);
      setCurrentStep(3);
    }
  });

  const { mutate: DownloadTemplate, isLoading: isLoadingTemplate } = useMutation({
    mutationFn: () => bookStoreApis.TemplateExcel_Thuong(),
    onSuccess: (res, fileName) => {
      download('MauKhoSach.xls', res.data);
    }
  });

  const handleSubmit = () => {
    // TODO: Integrate api to submit data
    ImportSave(rawDataList);
    ref.current?.scrollIntoView({ behavior: 'smooth' });
    setCurrentStep(3);
  };

  const columns: ColumnsType<BookColumsType> = useMemo(() => {
    const handleDeleteRow = (STT: number) => {
      let result: Array<BookColumsType> = [];
      for (let i = 0; i < listBookExcel.length; i++) {
        if (STT === listBookExcel[i].STT) {
          continue;
        }
        result.push(listBookExcel[i]);
      }
      for (var i = 0; i < rawDataList.length; i++) {
        if (+rawDataList[i][0] === STT) {
          rawDataList.splice(i, 1);
          break;
        }
      }
      setListBookExcel(result);
    };
    return [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        render: (value: any, record: any, j: number) => record.STT + 1,
        onCell: (record: BookColumsType) => ({ className: '' })
      },
      {
        title: 'Tên sách',
        dataIndex: 'TenSach',
        key: 'TenSach',
        render: (value: any, record: any) =>
          record?.TenSach?.length > 36 ? (
            <Tooltip placement='topLeft' title={record.TenSach} arrow={true}>
              <p className='text-center'>{record.TenSach.substring(0, 33).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.TenSach}</p>
          ),
        onHeaderCell: () => ({ className: 'min-w-[150px]' })
      },
      {
        title: 'Tên song ngữ',
        dataIndex: 'TenSongNgu',
        key: 'TenSongNgu',
        render: (value: any, record: any) =>
          record?.TenSongNgu?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.TenSongNgu} arrow={true}>
              <p className='text-center'>{record.TenSongNgu.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.TenSongNgu}</p>
          )
      },
      {
        title: 'Mã ISBN',
        dataIndex: 'ISBN',
        key: 'ISBN',
        render: (value: any, record: any) =>
          record?.ISBN?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.ISBN} arrow={true}>
              <p className='text-center'>{record.ISBN.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.ISBN}</p>
          )
      },
      {
        title: 'Số lượng',
        dataIndex: 'SoLuong',
        key: 'SoLuong'
      },
      {
        title: 'Tình trạng',
        dataIndex: 'IdTrangThai',
        key: 'IdTrangThai',
        render: (value: any, record: any) =>
          record?.IdTrangThai?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.IdTrangThai} arrow={true}>
              <p className='text-center'>{record.IdTrangThai.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.IdTrangThai}</p>
          )
      },
      {
        title: 'Mã nguồn cung cấp',
        dataIndex: 'IdNguonCungCap',
        key: 'IdNguonCungCap',
        render: (value: any, record: any) =>
          record?.IdNguonCungCap?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.IdNguonCungCap} arrow={true}>
              <p className='text-center'>{record.IdNguonCungCap.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.IdNguonCungCap}</p>
          )
      },
      {
        title: 'Số chừng từ',
        dataIndex: 'SoChungTu',
        key: 'SoChungTu',
        render: (value: any, record: any) =>
          record?.SoChungTu?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.SoChungTu} arrow={true}>
              <p className='text-center'>{record.SoChungTu.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.SoChungTu}</p>
          )
      },
      {
        title: 'Số vào sổ tổng quát',
        dataIndex: 'SoVaoSoTongQuat',
        key: 'SoVaoSoTongQuat',
        render: (value: any, record: any) =>
          record?.SoVaoSoTongQuat?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.SoVaoSoTongQuat} arrow={true}>
              <p className='text-center'>{record.SoVaoSoTongQuat.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.SoVaoSoTongQuat}</p>
          )
      },
      {
        title: 'Ngày vào sổ',
        dataIndex: 'NgayVaoSo_String',
        key: 'NgayVaoSo_String',
        render: (value: any, record: any) =>
          record?.NgayVaoSo_String?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.NgayVaoSo_String} arrow={true}>
              <p className='text-center'>{record.NgayVaoSo_String.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.NgayVaoSo_String}</p>
          )
      },
      {
        title: 'Day19',
        dataIndex: 'Day19',
        key: 'Day19',
        render: (value: any, record: any) =>
          record?.Day19?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.Day19} arrow={true}>
              <p className='text-center'>{record.Day19.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.Day19}</p>
          )
      },
      {
        title: 'DDC',
        dataIndex: 'DDC',
        key: 'DDC',
        render: (value: any, record: any) =>
          record?.DDC?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.DDC} arrow={true}>
              <p className='text-center'>{record.DDC.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.DDC}</p>
          )
      },
      {
        title: 'Tác giả chính',
        dataIndex: 'TacGia',
        key: 'TacGia',
        render: (value: any, record: any) =>
          record?.TacGia?.length > 36 ? (
            <Tooltip placement='topLeft' title={record.TacGia} arrow={true}>
              <p className='text-center'>{record.TacGia.substring(0, 33).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.TacGia}</p>
          ),
        onHeaderCell: () => ({ className: 'min-w-[150px]' })
      },
      {
        title: 'Tác giả phụ',
        dataIndex: 'TacGiaPhu',
        key: 'TacGiaPhu',
        render: (value: any, record: any) =>
          record?.TacGiaPhu?.length > 36 ? (
            <Tooltip placement='topLeft' title={record.TacGiaPhu} arrow={true}>
              <p className='text-center'>{record.TacGiaPhu.substring(0, 33).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.TacGiaPhu}</p>
          ),
        onHeaderCell: () => ({ className: 'min-w-[150px]' })
      },
      {
        title: 'Nhà xuất bản',
        dataIndex: 'IdNhaXuatBan',
        key: 'IdNhaXuatBan',
        render: (value: any, record: any) =>
          record?.IdNhaXuatBan?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.IdNhaXuatBan} arrow={true}>
              <p className='text-center'>{record.IdNhaXuatBan.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.IdNhaXuatBan}</p>
          )
      },
      {
        title: 'Nơi xuất bản',
        dataIndex: 'IdNoiXuatBan',
        key: 'IdNoiXuatBan',
        render: (value: any, record: any) =>
          record?.IdNoiXuatBan?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.IdNoiXuatBan} arrow={true}>
              <p className='text-center'>{record.IdNoiXuatBan.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.IdNoiXuatBan}</p>
          )
      },
      {
        title: 'Năm xuất bản',
        dataIndex: 'NamXuatBan',
        key: 'NamXuatBan',
        render: (value: any, record: any) =>
          record?.NamXuatBan?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.NamXuatBan} arrow={true}>
              <p className='text-center'>{record.NamXuatBan.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.NamXuatBan}</p>
          )
      },
      {
        title: 'Môn học',
        dataIndex: 'IdMonHoc',
        key: 'IdMonHoc',
        render: (value: any, record: any) =>
          record?.IdMonHoc?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.IdMonHoc} arrow={true}>
              <p className='text-center'>{record.IdMonHoc.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.IdMonHoc}</p>
          )
      },
      { width: 150, title: 'Khối lớp', dataIndex: 'KhoiLopString', key: 'KhoiLopString' },
      {
        title: 'Chủ điểm',
        dataIndex: 'IdChuDiem',
        key: 'IdChuDiem',
        render: (value: any, record: any) =>
          record?.IdChuDiem?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.IdChuDiem} arrow={true}>
              <p className='text-center'>{record.IdChuDiem.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.IdChuDiem}</p>
          )
      },
      {
        title: 'Kệ sách',
        dataIndex: 'IdKeSach',
        key: 'IdKeSach',
        render: (value: any, record: any) =>
          record?.IdKeSach?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.IdKeSach} arrow={true}>
              <p className='text-center'>{record.IdKeSach.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.IdKeSach}</p>
          )
      },
      {
        title: 'Thư mục tài liệu',
        dataIndex: 'IdThuMucSach',
        key: 'IdThuMucSach',
        render: (value: any, record: any) =>
          record?.IdThuMucSach?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.IdThuMucSach} arrow={true}>
              <p className='text-center'>{record.IdThuMucSach.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.IdThuMucSach}</p>
          )
      },
      {
        title: 'Số trang',
        dataIndex: 'SoTrang',
        key: 'SoTrang'
      },
      {
        title: 'Ngôn ngữ',
        dataIndex: 'IdNgonNgu',
        key: 'IdNgonNgu',
        render: (value: any, record: any) =>
          record?.IdNgonNgu?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.IdNgonNgu} arrow={true}>
              <p className='text-center'>{record.IdNgonNgu.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.IdNgonNgu}</p>
          )
      },
      {
        title: 'Nước xuất xứ',
        dataIndex: 'XuatXu',
        key: 'XuatXu',
        render: (value: any, record: any) =>
          record?.XuatXu?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.XuatXu} arrow={true}>
              <p className='text-center'>{record.XuatXu.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.XuatXu}</p>
          )
      },
      {
        title: 'Người biên dịch',
        dataIndex: 'NguoiBienDich',
        key: 'NguoiBienDich',
        render: (value: any, record: any) =>
          record?.NguoiBienDich?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.NguoiBienDich} arrow={true}>
              <p className='text-center'>{record.NguoiBienDich.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.NguoiBienDich}</p>
          )
      },
      {
        title: 'Lần xuất bản',
        dataIndex: 'TaiBan',
        key: 'TaiBan'
      },
      {
        title: 'SKU',
        dataIndex: 'SKU',
        key: 'SKU',
        render: (value: any, record: any) =>
          record?.SKU?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.SKU} arrow={true}>
              <p className='text-center'>{record.SKU.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.SKU}</p>
          )
      },
      {
        title: 'LLC',
        dataIndex: 'LLC',
        key: 'LLC',
        render: (value: any, record: any) =>
          record?.LLC?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.LLC} arrow={true}>
              <p className='text-center'>{record.LLC.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.LLC}</p>
          )
      },
      {
        title: 'Mã màu',
        dataIndex: 'IdMaMau',
        key: 'IdMaMau',
        render: (value: any, record: any) =>
          record?.IdMaMau?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.IdMaMau} arrow={true}>
              <p className='text-center'>{record.IdMaMau.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.IdMaMau}</p>
          )
      },
      {
        title: 'Phí mượn sách',
        dataIndex: 'PhiMuonSach',
        key: 'PhiMuonSach',
        render: (value: any, record: any) =>
          record?.PhiMuonSach?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.PhiMuonSach} arrow={true}>
              <p className='text-center'>{record.PhiMuonSach.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.PhiMuonSach}</p>
          )
      },
      {
        title: 'Giá bìa',
        dataIndex: 'GiaBia',
        key: 'GiaBia',
        render: (value: any, record: any) =>
          record?.GiaBia?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.GiaBia} arrow={true}>
              <p className='text-center'>{record.GiaBia.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.GiaBia}</p>
          )
      },
      {
        title: 'Mô tả vật lý',
        dataIndex: 'MoTaVatLy',
        key: 'MoTaVatLy',
        render: (value: any, record: any) =>
          record?.MoTaVatLy?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.MoTaVatLy} arrow={true}>
              <p className='text-center'>{record.MoTaVatLy.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.MoTaVatLy}</p>
          )
      },
      {
        title: 'Khổ sách',
        dataIndex: 'CoSach',
        key: 'CoSach',
        render: (value: any, record: any) =>
          record?.CoSach?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.CoSach} arrow={true}>
              <p className='text-center'>{record.CoSach.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.CoSach}</p>
          )
      },
      {
        title: 'Tài liệu kèm theo',
        dataIndex: 'TaiLieuDinhKem',
        key: 'TaiLieuDinhKem',
        render: (value: any, record: any) =>
          record?.TaiLieuDinhKem?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.TaiLieuDinhKem} arrow={true}>
              <p className='text-center'>{record.TaiLieuDinhKem.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.TaiLieuDinhKem}</p>
          )
      },
      {
        title: 'Minh họa',
        dataIndex: 'MinhHoa',
        key: 'MinhHoa',
        render: (value: any, record: any) =>
          record?.MinhHoa?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.MinhHoa} arrow={true}>
              <p className='text-center'>{record.MinhHoa.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.MinhHoa}</p>
          )
      },
      {
        width: 150,
        title: 'Tùng thư',
        dataIndex: 'TungThu',
        key: 'TungThu',
        render: (value: any, record: any) =>
          record?.TungThu?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.TungThu} arrow={true}>
              <p className='text-center'>{record.TungThu.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.TungThu}</p>
          )
      },
      {
        width: 150,
        title: 'Phụ chú',
        dataIndex: 'PhuChu',
        key: 'PhuChu',
        render: (value: any, record: any) =>
          record?.PhuChu?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.PhuChu} arrow={true}>
              <p className='text-center'>{record.PhuChu.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.PhuChu}</p>
          )
      },
      {
        width: 150,
        title: 'Tóm tắt',
        dataIndex: 'TomTat',
        key: 'TomTat',
        render: (value: any, record: any) =>
          record?.TomTat?.length > 15 ? (
            <Tooltip placement='topLeft' title={record.TomTat} arrow={true}>
              <p className='text-center'>{record.TomTat.substring(0, 15).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-center'>{record.TomTat}</p>
          )
      },
      {
        title: (currentStep === 2 && 'Hành động') || (currentStep === 3 && 'Nội dung lỗi'),
        dataIndex: 'actions',
        key: 'actions',
        width: '70%',
        render: (value: string, record: BookColumsType) => {
          return (
            <>
              {currentStep === 2 && (
                <button
                  className='mx-2'
                  onClick={(e) => {
                    e.stopPropagation();
                    handleDeleteRow(record.STT);
                  }}
                >
                  <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
                </button>
              )}

              {currentStep === 3 && <p className='text-danger-10'>{record.ListError?.join(', ')}</p>}
            </>
          );
        }
      }
    ].filter((item, index) => {
      if (isDay19 === false && item.key === 'Day19') return false;
      if (isDay19 === true && item.key === 'DDC') return false;
      item.onCell = (record: BookColumsType) => ({
        className:
          currentStep === 3 && record?.cellError && record?.cellError.indexOf(index) !== -1
            ? 'min-h-38 errorBackGround py-2'
            : ''
      });
      return true;
    });
  }, [currentStep, rawDataList, listBookExcel, isDay19]);

  const handleOnFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files?.[0];
    if (!fileFromLocal) return;

    setFileName(fileFromLocal?.name as string);
    PreviewImport(fileFromLocal as File);
  };

  const handleAccessFileInputRef = () => {
    fileInutRef.current?.click();
  };

  const handleBack = () => {
    if (saveError > 0) {
      handleBackCurrent();
    } else navigate(path.khosach);
  };

  const handleBackCurrent = () => {
    setListBookExcel([]);
    setCurrentStep(1);
  };

  return (
    <div className='excel-normal my-5' ref={ref}>
      <Title title={'Thêm sách từ file Excel - ' + (TenKho === undefined ? '' : TenKho)} />
      {currentStep === 1 && (
        <div className='thongtin-group col-span-5 mt-5 w-[320px]'>
          <h5 className='title-box'>
            Thêm vào sổ nhập kho
            <div className='background-fake'></div>
          </h5>

          <Radio.Group
            className='form-group form-group-sm row'
            style={{ paddingLeft: '15px', paddingRight: '15px' }}
            onChange={(e: any) => {
              setIsCreate(e?.target?.value);
            }}
            value={isCreate}
          >
            <Radio value={true} className='text-inbox grow justify-center'>
              Thêm
            </Radio>
            <Radio value={false} className='text-inbox grow justify-center'>
              Không thêm
            </Radio>
          </Radio.Group>
        </div>
      )}
      <div className='my-10'>
        <ProgressBar activeStep={currentStep} type='excel' />
      </div>

      {/* TODO: Condition are currentStep > 1 and data is not undefined*/}
      {currentStep === 1 && (
        <div className='flex items-center justify-center rounded-md bg-tertiary-10 py-4'>
          <Button
            className='bg-tertiary-20 hover:bg-tertiary-20/30'
            onClick={() => {
              DownloadTemplate();
            }}
          >
            <DownloadOutlined />
            File excel mẫu
          </Button>

          <input
            type='file'
            accept='.csv,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel'
            className='hidden'
            ref={fileInutRef}
            onChange={handleOnFileChange}
          />

          <Button variant='default' className='ml-2' onClick={handleAccessFileInputRef}>
            Chọn tệp
          </Button>
        </div>
      )}

      {currentStep > 1 && (
        <div>
          {currentStep === 2 && (
            <>
              <p>
                Tổng số đầu sách: <span className='font-bold'>{listBookExcel?.length}</span>
              </p>

              {!!listBookExcel?.length && <h3 className='text-primary-10'>DANH SÁCH ĐÃ NHẬN DIỆN</h3>}
            </>
          )}

          {currentStep === 3 && (
            <>
              <p className='text-tertiary-30'>
                Lưu thành công:{' '}
                <span className='font-bold'>
                  {saveSuccess} đầu sách - {SaveSuccessSCB} sách cá biệt
                </span>
              </p>

              <p className='flex items-center gap-1 text-danger-10'>
                Lưu thất bại: <span className='font-bold'> {' ' + saveError} đầu sách</span>{' '}
                {listFail.length > 0 && (
                  <Button
                    variant='danger'
                    className='ml-2'
                    onClick={() => {
                      saveByteArray(link, 'DSSachLoi.xls', 'application/xls');
                    }}
                  >
                    <DownloadOutlined />
                    Tải file lỗi
                  </Button>
                )}
              </p>
              {listFail.length > 0 && <h3 className='uppercase text-primary-10'>Danh sách sách bị lỗi</h3>}
            </>
          )}

          {((currentStep === 2 && !!listBookExcel?.length) || (currentStep === 3 && listFail.length > 0)) && (
            <div className='relative mt-6'>
              <Table
                loading={isLoadingPreviewImport && isLoadingImportSave}
                columns={columns}
                pagination={{
                  onChange(current, pageSize) {
                    setPage(current);
                    setPaginationSize(pageSize);
                  },
                  defaultPageSize: 10,
                  hideOnSinglePage: true,
                  showSizeChanger: false
                }}
                dataSource={listBookExcel}
                scroll={{ x: 980 }}
                rowKey={(record) => record.STT}
                className='custom-table'
                bordered
              />
            </div>
          )}
        </div>
      )}

      <div className='mr-10 mt-6 flex items-center justify-end'>
        {currentStep === 1 && (
          <>
            <Button
              variant='secondary'
              onClick={() => {
                navigate(path.khosach);
              }}
            >
              Quay về
            </Button>
          </>
        )}

        <>
          {currentStep === 2 && !isLoadingPreviewImport && (
            <>
              <Button variant='secondary' onClick={handleBackCurrent}>
                Quay về
              </Button>
              <Button variant='default' className='ml-2' onClick={handleSubmit}>
                Tiếp tục
              </Button>
            </>
          )}

          {currentStep === 3 && !isLoadingImportSave && (
            <Button variant='default' className='ml-2' onClick={handleBack}>
              Hoàn tất
            </Button>
          )}
        </>
      </div>
      <Loading open={isLoading || isLoadingTemplate || isLoadingPreviewImport || isLoadingImportSave} />
    </div>
  );
};

export default AddBookNormalByExcel;
