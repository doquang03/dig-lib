import { DeleteFilled, EditFilled, SearchOutlined } from '@ant-design/icons';
import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Spin, Tooltip } from 'antd';
import { bookApis, receiptApis } from 'apis';
import classNames from 'classnames';
import { Button, DatePicker, Input, InputNumber, Loading, Select, Title } from 'components';
import { AutoComplete } from 'components/AutoComplete';
import { path } from 'constants/path';
import { UserConText } from 'contexts/user.context';
import dayjs from 'dayjs';
import { useOutsideClick } from 'hooks';
import { debounce } from 'lodash';
import { RefObject, useContext, useMemo, useState } from 'react';
import { useFieldArray, useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { addReceiptSchema } from 'utils/rules';
import { convertDate } from 'utils/utils';
import * as yup from 'yup';

type bookForm = yup.InferType<typeof addReceiptSchema>;

const AddReceipt = () => {
  const [visibleSearchResult, setVisibleSearchResult] = useState<boolean>(true);
  const [searchBookValue, setSearchBookValue] = useState<string>('');
  const [booksResult, setBooksResult] = useState<Book[]>([]);
  const [selectedBooks, setSelectedBooks] = useState<Book[]>([]);
  const [errorSachCaBiet, setErrorSachCaBiet] = useState<boolean>(false);

  const user = useContext(UserConText);

  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    reset,
    setValue,
    getValues,
    control,
    formState: { errors }
  } = useForm<bookForm>({
    resolver: yupResolver(addReceiptSchema)
  });

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'sachCaBiet'
  });

  const { data: optionsData, isLoading: isLoadingForm } = useQuery({
    queryKey: ['GetInitCreateForm'],
    queryFn: bookApis.GetInitCreateForm
  });

  const { isLoading: isLoadingSearchBooks, refetch } = useQuery({
    queryKey: ['books', searchBookValue],
    queryFn: () => bookApis.GetBooksList({ TextForSearch: searchBookValue }),
    onSuccess: (data) => {
      setBooksResult(data.data.Item.ListBook);
    },
    enabled: Boolean(searchBookValue)
  });

  const { mutate: createReceipt, isLoading: isLoadingCreateReceipt } = useMutation({
    mutationFn: receiptApis.createReceipt,
    onSuccess: () => {
      reset();
      toast.success('Tạo phiếu nhập kho thành công');
      navigate(path.quanlynhapkho);
    }
  });

  const handleSearch = debounce((e: React.ChangeEvent<HTMLInputElement>) => {
    setVisibleSearchResult(true);
    setSearchBookValue(e.target.value);
  }, 700);

  const options = useMemo(() => {
    if (!optionsData?.data.Item) return;

    const { Options_KhoSach, Options_TrangThaiSach, Options_NguonCungCap } = optionsData?.data.Item;

    const khoSach = [{ label: 'Chọn kho sách', value: '' }, ...Options_KhoSach];
    const trangThai = [{ label: 'Chọn tình trạng', value: '' }, ...Options_TrangThaiSach];
    const nguonCungCap = [{ label: 'Chọn nguồn cung cấp', value: '' }, ...Options_NguonCungCap];

    return { khoSach, trangThai, nguonCungCap };
  }, [optionsData?.data.Item]);

  const handleAdd = (book: Book) => () => {
    const index = selectedBooks.findIndex((_book) => _book.MaKiemSoat === book.MaKiemSoat);

    if (index !== -1) {
      const currentQuantity = Number(getValues(`sachCaBiet.${index}.SoLuong`));

      setValue(`sachCaBiet.${index}.SoLuong`, currentQuantity + 1 + '');
      setVisibleSearchResult(false);
    } else {
      setSelectedBooks((prevState) => [...prevState, book]);
      append({ KhoSach: '', SoLuong: '1', TrangThai: '', NgayNhap: '' });
      setVisibleSearchResult(false);
    }
  };

  const handleDelete = (index: number) => () => {
    const array = [...selectedBooks];
    array.splice(index, 1);
    setSelectedBooks(array);
    remove(index);
  };

  const handleClickOutside = () => setVisibleSearchResult(false);

  const handleBack = () => navigate(path.quanlynhapkho);

  const ref = useOutsideClick<HTMLDivElement>(handleClickOutside);

  const onSubmit = handleSubmit((data) => {
    if (!data.sachCaBiet?.length) {
      setErrorSachCaBiet(true);
      return;
    }

    createReceipt({
      IdUserAdmin: user?.profile?.Id + '',
      UserName: user?.profile?.UserName + '',
      GhiChuPhieuNhap: data.GhiChu + '',
      DanhSachSach: data.sachCaBiet.map((book, index) => {
        return {
          IdKho: book.KhoSach + '',
          IdNCC: book.NguonCungCap + '',
          NVS: convertDate(book.NgayNhap + ''),
          SCT: book.SoCT + '',
          SVS: book.VSTQ + '',
          IdSach: selectedBooks[index].Id + '',
          IdTrangThai: book.TrangThai + '',
          SoLuong: Number(book.SoLuong)
        };
      })
    });
  });

  return (
    <div className='p-5'>
      <Title title='Tạo phiếu nhập kho' />

      <div className='relative mt-3' ref={ref as RefObject<HTMLDivElement>}>
        <Input
          placeholder='Mã cá biệt, tên sách'
          className='w-full bg-transparent p-3 outline-none'
          containerClassName={classNames('border  w-1/2 bg-white rounded-full flex items-center', {
            'border-danger-10': !Boolean(selectedBooks.length) && errorSachCaBiet,
            'border-gray-300': Boolean(selectedBooks.length) && !errorSachCaBiet
          })}
          right={
            <button
              onClick={() => {
                if (searchBookValue) {
                  refetch();
                  setVisibleSearchResult(true);
                }
              }}
            >
              <SearchOutlined style={{ color: 'black', opacity: 0.3, marginRight: 10 }} />
            </button>
          }
          onChange={handleSearch}
        />
        {!Boolean(selectedBooks.length) && errorSachCaBiet && (
          <span className='text-danger-10'>Hãy thêm sách cần tạo phiếu nhập kho!</span>
        )}

        {visibleSearchResult && Boolean(searchBookValue) && (
          <div className='absolute z-30 max-h-[150px] w-1/2 overflow-y-auto rounded-md bg-white px-3 shadow-md'>
            {!!booksResult.length ? (
              booksResult.map((book) => (
                <button className='flex w-full items-center border-b-2 py-3' onClick={handleAdd(book)} key={book.Id}>
                  {book.MaKiemSoat} - {book.TenSach}
                </button>
              ))
            ) : (
              <>
                {isLoadingSearchBooks && !!searchBookValue ? (
                  <div className='py-2 text-center'>
                    Đang tìm kiếm sách <Spin />{' '}
                  </div>
                ) : (
                  <div className='py-2 text-center'>
                    Không tìm thấy dữ liệu với từ khóa <span className='font-bold'>{searchBookValue}</span>
                  </div>
                )}
              </>
            )}
          </div>
        )}

        {!!selectedBooks.length && (
          <>
            <table className='mt-5 table overflow-scroll'>
              <thead className='bg-primary-10 text-white'>
                <tr>
                  <th className='text-center'>STT</th>
                  <th className='text-center'>Mã kiểm soát</th>
                  <th className='text-center'>Tên sách</th>
                  <th className='text-center'>Kho</th>
                  <th className='text-center'>Số lượng</th>
                  <th className='text-center'>Tình trạng</th>
                  <th className='text-center'>Nguồn cung cấp</th>
                  <th className='text-center'>Số chứng từ</th>
                  <th className='text-center'>Số VSTQ</th>
                  <th className='text-center'>Ngày vào sổ</th>
                  <th className='text-center'>Giá tiền</th>
                  <th className='text-center'></th>
                </tr>
              </thead>

              <tbody className='bg-white'>
                {fields.map((row, index) => {
                  const { MaKiemSoat, TenSach, GiaBia } = selectedBooks[index];

                  return (
                    <tr key={row.id} className='text-center'>
                      <td>{index + 1}</td>
                      <td>{MaKiemSoat}</td>
                      <td>
                        {TenSach && TenSach.length > 20 ? (
                          <Tooltip placement='bottomRight' title={TenSach} arrow={true}>
                            {TenSach.substring(0, 20).concat('...')}
                          </Tooltip>
                        ) : (
                          TenSach
                        )}
                      </td>
                      <td>
                        <AutoComplete
                          className='w-full grow'
                          name={`sachCaBiet.${index}.KhoSach` as const}
                          placeholder='Nhập kho'
                          control={control}
                          items={options?.khoSach || []}
                          errorMessage={errors?.sachCaBiet?.[index]?.KhoSach?.message}
                          showSearch
                        />
                      </td>
                      <td>
                        <InputNumber
                          control={control}
                          name={`sachCaBiet.${index}.SoLuong` as const}
                          placeholder='Nhập số lượng'
                          errorMessage={errors?.sachCaBiet?.[index]?.SoLuong?.message}
                        />
                      </td>
                      <td>
                        <Select
                          items={options?.trangThai || []}
                          name={`sachCaBiet.${index}.TrangThai` as const}
                          register={register}
                          errorMessage={errors?.sachCaBiet?.[index]?.TrangThai?.message}
                        />
                      </td>
                      <td>
                        <Select
                          items={options?.nguonCungCap || []}
                          name={`sachCaBiet.${index}.NguonCungCap` as const}
                          className='w-full'
                          register={register}
                          errorMessage={errors?.sachCaBiet?.[index]?.NguonCungCap?.message}
                        />
                      </td>
                      <td>
                        <Input
                          placeholder='CT12/2'
                          name={`sachCaBiet.${index}.SoCT` as const}
                          register={register}
                          errorMessage={errors?.sachCaBiet?.[index]?.SoCT?.message}
                          maxLength={100}
                        />
                      </td>
                      <td>
                        <Input
                          placeholder='Số vào sổ tổng quát'
                          name={`sachCaBiet.${index}.VSTQ` as const}
                          register={register}
                          errorMessage={errors?.sachCaBiet?.[index]?.VSTQ?.message}
                          maxLength={100}
                        />
                      </td>
                      <td>
                        <DatePicker
                          control={control}
                          name={`sachCaBiet.${index}.NgayNhap` as const}
                          errorMessage={errors?.sachCaBiet?.[index]?.NgayNhap?.message}
                          disabledDate={(current) => {
                            return current && current > dayjs().endOf('day');
                          }}
                        />
                      </td>
                      <td>
                        {GiaBia
                          ? new Intl.NumberFormat('vi-VN', {
                              style: 'currency',
                              currency: 'VND',
                              maximumFractionDigits: 9
                            }).format(Number(GiaBia))
                          : ''}
                      </td>

                      <td>
                        <button className='mx-2' onClick={handleDelete(index)}>
                          <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
                        </button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>

            <Input
              name='GhiChu'
              register={register}
              left={<EditFilled style={{ color: 'black', opacity: 0.3 }} />}
              placeholder='Nhập nội dung ghi chú'
              containerClassName='border-b-2 my-4 flex items-center gap-3'
              className='placeholder-italic w-full bg-transparent outline-none'
              maxLength={255}
            />
          </>
        )}
      </div>

      <div className='flex items-center justify-end gap-3'>
        <Button onClick={handleBack} variant='secondary'>
          Quay về
        </Button>

        <Button onClick={onSubmit}>Thêm mới</Button>
      </div>

      <Loading open={isLoadingForm || isLoadingCreateReceipt} />
    </div>
  );
};

export default AddReceipt;
