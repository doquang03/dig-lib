import { path } from 'constants/path';
import { Outlet, useMatch } from 'react-router-dom';
import CollectionManagermentContent from './CollectionManagermentContent/CollectionManagermentContent';

const CollectionManagementPage = () => {
  const match = useMatch(path.bosuutap);

  return <>{Boolean(match) ? <CollectionManagermentContent /> : <Outlet />}</>;
};

export default CollectionManagementPage;
