function HomeIcon() {
  return (
    <svg width={20} height={19} viewBox='0 0 20 19' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path
        d='M16.3024 6.20218H12.7347V1.48417C12.7347 1.21676 12.9515 1 13.2189 1H15.8182C16.0856 1 16.3024 1.21679 16.3024 1.48417V6.20218H16.3024Z'
        fill='url(#paint0_linear_9783_4992)'
      />
      <path
        d='M16.3024 6.20218H12.7347V1.48417C12.7347 1.21676 12.9515 1 13.2189 1H15.8182C16.0856 1 16.3024 1.21679 16.3024 1.48417V6.20218H16.3024Z'
        fill='url(#paint1_linear_9783_4992)'
      />
      <path
        d='M16.3025 1.4843V6.20216H14.1049V1.00024H15.8185C16.0855 1.00024 16.3025 1.21681 16.3025 1.4843Z'
        fill='url(#paint2_linear_9783_4992)'
      />
      <path
        d='M16.4052 7.34136V16.5606C16.4052 17.0625 15.9983 17.4694 15.4964 17.4694H3.50382C3.00188 17.4694 2.59497 17.0625 2.59497 16.5606V7.34136L7.56971 3.65918H11.4305L16.4052 7.34136Z'
        fill='url(#paint3_linear_9783_4992)'
      />
      <path
        d='M2.59485 11.3928V16.5605C2.59485 17.0625 3.00175 17.4694 3.50369 17.4694H15.4963C15.9982 17.4694 16.4051 17.0625 16.4051 16.5605V11.3928H2.59485Z'
        fill='url(#paint4_linear_9783_4992)'
      />
      <path
        d='M16.4053 7.34119V11.3926H2.59473V7.34119L7.5698 3.65894H11.4302L16.4053 7.34119Z'
        fill='url(#paint5_linear_9783_4992)'
      />
      <path
        d='M2.59473 7.34119V11.3926H16.4053V7.34119L11.4302 3.65894H7.5698L2.59473 7.34119Z'
        fill='url(#paint6_linear_9783_4992)'
      />
      <path
        d='M9.5 3.65918V17.4694H15.4963C15.9982 17.4694 16.4051 17.0625 16.4051 16.5606V7.34136L11.4304 3.65918H9.5Z'
        fill='url(#paint7_linear_9783_4992)'
      />
      <path
        d='M9.87879 3.95751L17.0144 9.2391C17.5978 9.6709 18.4253 9.51412 18.8103 8.89888C19.1551 8.34795 19.018 7.62454 18.4957 7.23786L10.5247 1.33799C9.9159 0.887336 9.08413 0.887336 8.47527 1.33799L0.504364 7.23786C-0.018025 7.62451 -0.15507 8.34795 0.189713 8.89888C0.574723 9.51412 1.40226 9.6709 1.98562 9.2391L9.12124 3.95751C9.34627 3.79092 9.65376 3.79092 9.87879 3.95751Z'
        fill='url(#paint8_linear_9783_4992)'
      />
      <path
        d='M9.87879 3.95751L17.0144 9.2391C17.5978 9.6709 18.4253 9.51412 18.8103 8.89888C19.1551 8.34795 19.018 7.62454 18.4957 7.23786L10.5247 1.33799C9.9159 0.887336 9.08413 0.887336 8.47527 1.33799L0.504364 7.23786C-0.018025 7.62451 -0.15507 8.34795 0.189713 8.89888C0.574723 9.51412 1.40226 9.6709 1.98562 9.2391L9.12124 3.95751C9.34627 3.79092 9.65376 3.79092 9.87879 3.95751Z'
        fill='url(#paint9_linear_9783_4992)'
      />
      <path
        d='M18.8054 8.90627L10.5247 2.77717C9.91591 2.32655 9.08414 2.32655 8.47529 2.77717L0.194702 8.90627C0.581753 9.51516 1.40473 9.66898 1.98564 9.23899L9.12125 3.9574C9.34628 3.79082 9.65377 3.79082 9.8788 3.9574L17.0144 9.23899C17.5954 9.66898 18.4183 9.51512 18.8054 8.90627Z'
        fill='url(#paint10_linear_9783_4992)'
      />
      <path
        d='M12.7346 6.07136L17.0143 9.23909C17.5977 9.6709 18.4252 9.51411 18.8102 8.89887C19.155 8.34795 19.0179 7.62454 18.4956 7.23786L14.5146 4.29126L12.7346 6.07136Z'
        fill='url(#paint11_linear_9783_4992)'
      />
      <path
        d='M6.26535 6.07132L1.9856 9.23909C1.40224 9.6709 0.574697 9.51411 0.189687 8.89887C-0.155059 8.34795 -0.0180139 7.62454 0.504338 7.23786L4.48525 4.29126L6.26535 6.07132Z'
        fill='url(#paint12_linear_9783_4992)'
      />
      <path
        d='M8.62005 10.9605H6.68149C6.45167 10.9605 6.26538 10.7742 6.26538 10.5444V8.6058C6.26538 8.37599 6.45167 8.1897 6.68149 8.1897H8.62005C8.84986 8.1897 9.03615 8.37599 9.03615 8.6058V10.5444C9.03615 10.7741 8.84986 10.9605 8.62005 10.9605Z'
        fill='url(#paint13_linear_9783_4992)'
      />
      <path
        d='M8.62005 10.9605H6.68149C6.45167 10.9605 6.26538 10.7742 6.26538 10.5444V8.6058C6.26538 8.37599 6.45167 8.1897 6.68149 8.1897H8.62005C8.84986 8.1897 9.03615 8.37599 9.03615 8.6058V10.5444C9.03615 10.7741 8.84986 10.9605 8.62005 10.9605Z'
        fill='url(#paint14_linear_9783_4992)'
      />
      <path
        d='M8.62005 10.9605H6.68149C6.45167 10.9605 6.26538 10.7742 6.26538 10.5444V8.6058C6.26538 8.37599 6.45167 8.1897 6.68149 8.1897H8.62005C8.84986 8.1897 9.03615 8.37599 9.03615 8.6058V10.5444C9.03615 10.7741 8.84986 10.9605 8.62005 10.9605Z'
        fill='url(#paint15_linear_9783_4992)'
      />
      <path
        d='M12.3185 10.9605H10.38C10.1502 10.9605 9.96387 10.7742 9.96387 10.5444V8.6058C9.96387 8.37599 10.1502 8.1897 10.38 8.1897H12.3185C12.5484 8.1897 12.7346 8.37599 12.7346 8.6058V10.5444C12.7346 10.7741 12.5484 10.9605 12.3185 10.9605Z'
        fill='url(#paint16_linear_9783_4992)'
      />
      <path
        d='M12.3185 10.9605H10.38C10.1502 10.9605 9.96387 10.7742 9.96387 10.5444V8.6058C9.96387 8.37599 10.1502 8.1897 10.38 8.1897H12.3185C12.5484 8.1897 12.7346 8.37599 12.7346 8.6058V10.5444C12.7346 10.7741 12.5484 10.9605 12.3185 10.9605Z'
        fill='url(#paint17_linear_9783_4992)'
      />
      <path
        d='M12.3185 10.9605H10.38C10.1502 10.9605 9.96387 10.7742 9.96387 10.5444V8.6058C9.96387 8.37599 10.1502 8.1897 10.38 8.1897H12.3185C12.5484 8.1897 12.7346 8.37599 12.7346 8.6058V10.5444C12.7346 10.7741 12.5484 10.9605 12.3185 10.9605Z'
        fill='url(#paint18_linear_9783_4992)'
      />
      <path
        d='M8.62005 14.5957H6.68149C6.45167 14.5957 6.26538 14.4094 6.26538 14.1796V12.2411C6.26538 12.0112 6.45167 11.825 6.68149 11.825H8.62005C8.84986 11.825 9.03615 12.0112 9.03615 12.2411V14.1796C9.03615 14.4094 8.84986 14.5957 8.62005 14.5957Z'
        fill='url(#paint19_linear_9783_4992)'
      />
      <path
        d='M8.62005 14.5957H6.68149C6.45167 14.5957 6.26538 14.4094 6.26538 14.1796V12.2411C6.26538 12.0112 6.45167 11.825 6.68149 11.825H8.62005C8.84986 11.825 9.03615 12.0112 9.03615 12.2411V14.1796C9.03615 14.4094 8.84986 14.5957 8.62005 14.5957Z'
        fill='url(#paint20_linear_9783_4992)'
      />
      <path
        d='M8.62005 14.5957H6.68149C6.45167 14.5957 6.26538 14.4094 6.26538 14.1796V12.2411C6.26538 12.0112 6.45167 11.825 6.68149 11.825H8.62005C8.84986 11.825 9.03615 12.0112 9.03615 12.2411V14.1796C9.03615 14.4094 8.84986 14.5957 8.62005 14.5957Z'
        fill='url(#paint21_linear_9783_4992)'
      />
      <path
        d='M12.3185 14.5957H10.38C10.1502 14.5957 9.96387 14.4094 9.96387 14.1796V12.2411C9.96387 12.0112 10.1502 11.825 10.38 11.825H12.3185C12.5484 11.825 12.7346 12.0112 12.7346 12.2411V14.1796C12.7346 14.4094 12.5484 14.5957 12.3185 14.5957Z'
        fill='url(#paint22_linear_9783_4992)'
      />
      <path
        d='M12.3185 14.5957H10.38C10.1502 14.5957 9.96387 14.4094 9.96387 14.1796V12.2411C9.96387 12.0112 10.1502 11.825 10.38 11.825H12.3185C12.5484 11.825 12.7346 12.0112 12.7346 12.2411V14.1796C12.7346 14.4094 12.5484 14.5957 12.3185 14.5957Z'
        fill='url(#paint23_linear_9783_4992)'
      />
      <path
        d='M12.3185 14.5957H10.38C10.1502 14.5957 9.96387 14.4094 9.96387 14.1796V12.2411C9.96387 12.0112 10.1502 11.825 10.38 11.825H12.3185C12.5484 11.825 12.7346 12.0112 12.7346 12.2411V14.1796C12.7346 14.4094 12.5484 14.5957 12.3185 14.5957Z'
        fill='url(#paint24_linear_9783_4992)'
      />
      <defs>
        <linearGradient
          id='paint0_linear_9783_4992'
          x1='13.1343'
          y1='3.60107'
          x2='16.3254'
          y2='3.60107'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#FFA1AE' />
          <stop offset={1} stopColor='#FF4565' />
        </linearGradient>
        <linearGradient
          id='paint1_linear_9783_4992'
          x1='15.2851'
          y1='2.64139'
          x2='14.2203'
          y2='4.20729'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#FE0364' stopOpacity={0} />
          <stop offset='0.2343' stopColor='#F90362' stopOpacity='0.234' />
          <stop offset='0.5173' stopColor='#EA035B' stopOpacity='0.517' />
          <stop offset='0.8243' stopColor='#D20250' stopOpacity='0.824' />
          <stop offset={1} stopColor='#C00148' />
        </linearGradient>
        <linearGradient
          id='paint2_linear_9783_4992'
          x1='15.1608'
          y1='3.6012'
          x2='16.3376'
          y2='3.6012'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#FE0364' stopOpacity={0} />
          <stop offset='0.2343' stopColor='#F90362' stopOpacity='0.234' />
          <stop offset='0.5173' stopColor='#EA035B' stopOpacity='0.517' />
          <stop offset='0.8243' stopColor='#D20250' stopOpacity='0.824' />
          <stop offset={1} stopColor='#C00148' />
        </linearGradient>
        <linearGradient
          id='paint3_linear_9783_4992'
          x1='4.08747'
          y1='6.7266'
          x2='12.3513'
          y2='14.9905'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#F5FBFF' />
          <stop offset={1} stopColor='#DBD5EF' />
        </linearGradient>
        <linearGradient
          id='paint4_linear_9783_4992'
          x1='9.49998'
          y1='14.1699'
          x2='9.49998'
          y2='17.6829'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#DBD5EF' stopOpacity={0} />
          <stop offset='0.2853' stopColor='#D9D2EE' stopOpacity='0.285' />
          <stop offset='0.4739' stopColor='#D4C9E9' stopOpacity='0.474' />
          <stop offset='0.6346' stopColor='#CBBAE2' stopOpacity='0.635' />
          <stop offset='0.7795' stopColor='#BFA5D7' stopOpacity='0.78' />
          <stop offset='0.9126' stopColor='#AF8ACA' stopOpacity='0.913' />
          <stop offset={1} stopColor='#A274BF' />
        </linearGradient>
        <linearGradient
          id='paint5_linear_9783_4992'
          x1='8.58826'
          y1='8.02207'
          x2='6.87578'
          y2='5.49615'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#DBD5EF' stopOpacity={0} />
          <stop offset='0.2853' stopColor='#D9D2EE' stopOpacity='0.285' />
          <stop offset='0.4739' stopColor='#D4C9E9' stopOpacity='0.474' />
          <stop offset='0.6346' stopColor='#CBBAE2' stopOpacity='0.635' />
          <stop offset='0.7795' stopColor='#BFA5D7' stopOpacity='0.78' />
          <stop offset='0.9126' stopColor='#AF8ACA' stopOpacity='0.913' />
          <stop offset={1} stopColor='#A274BF' />
        </linearGradient>
        <linearGradient
          id='paint6_linear_9783_4992'
          x1='10.4117'
          y1='8.02207'
          x2='12.1242'
          y2='5.49615'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#DBD5EF' stopOpacity={0} />
          <stop offset='0.2853' stopColor='#D9D2EE' stopOpacity='0.285' />
          <stop offset='0.4739' stopColor='#D4C9E9' stopOpacity='0.474' />
          <stop offset='0.6346' stopColor='#CBBAE2' stopOpacity='0.635' />
          <stop offset='0.7795' stopColor='#BFA5D7' stopOpacity='0.78' />
          <stop offset='0.9126' stopColor='#AF8ACA' stopOpacity='0.913' />
          <stop offset={1} stopColor='#A274BF' />
        </linearGradient>
        <linearGradient
          id='paint7_linear_9783_4992'
          x1='11.7219'
          y1='10.5643'
          x2='16.608'
          y2='10.5643'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#DBD5EF' stopOpacity={0} />
          <stop offset='0.2853' stopColor='#D9D2EE' stopOpacity='0.285' />
          <stop offset='0.4739' stopColor='#D4C9E9' stopOpacity='0.474' />
          <stop offset='0.6346' stopColor='#CBBAE2' stopOpacity='0.635' />
          <stop offset='0.7795' stopColor='#BFA5D7' stopOpacity='0.78' />
          <stop offset='0.9126' stopColor='#AF8ACA' stopOpacity='0.913' />
          <stop offset={1} stopColor='#A274BF' />
        </linearGradient>
        <linearGradient
          id='paint8_linear_9783_4992'
          x1='8.01293'
          y1='1.0622'
          x2='9.98229'
          y2='10.5666'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#B3DAFE' />
          <stop offset={1} stopColor='#0182FC' />
        </linearGradient>
        <linearGradient
          id='paint9_linear_9783_4992'
          x1='8.01293'
          y1='1.0622'
          x2='9.98229'
          y2='10.5666'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#B3DAFE' />
          <stop offset={1} stopColor='#0182FC' />
        </linearGradient>
        <linearGradient
          id='paint10_linear_9783_4992'
          x1='9.50003'
          y1='6.82035'
          x2='9.50003'
          y2='0.881403'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#314DC9' stopOpacity={0} />
          <stop offset='0.2761' stopColor='#304BC4' stopOpacity='0.276' />
          <stop offset='0.5628' stopColor='#2B45B8' stopOpacity='0.563' />
          <stop offset='0.8535' stopColor='#243BA3' stopOpacity='0.854' />
          <stop offset={1} stopColor='#1F3596' />
        </linearGradient>
        <linearGradient
          id='paint11_linear_9783_4992'
          x1='16.4463'
          y1='7.4761'
          x2='18.6042'
          y2='9.63397'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#314DC9' stopOpacity={0} />
          <stop offset='0.2761' stopColor='#304BC4' stopOpacity='0.276' />
          <stop offset='0.5628' stopColor='#2B45B8' stopOpacity='0.563' />
          <stop offset='0.8535' stopColor='#243BA3' stopOpacity='0.854' />
          <stop offset={1} stopColor='#1F3596' />
        </linearGradient>
        <linearGradient
          id='paint12_linear_9783_4992'
          x1='2.55363'
          y1='7.47606'
          x2='0.395756'
          y2='9.63393'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#314DC9' stopOpacity={0} />
          <stop offset='0.2761' stopColor='#304BC4' stopOpacity='0.276' />
          <stop offset='0.5628' stopColor='#2B45B8' stopOpacity='0.563' />
          <stop offset='0.8535' stopColor='#243BA3' stopOpacity='0.854' />
          <stop offset={1} stopColor='#1F3596' />
        </linearGradient>
        <linearGradient
          id='paint13_linear_9783_4992'
          x1='6.87412'
          y1='8.79844'
          x2='8.98999'
          y2='10.9143'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#B3DAFE' />
          <stop offset={1} stopColor='#0182FC' />
        </linearGradient>
        <linearGradient
          id='paint14_linear_9783_4992'
          x1='7.65075'
          y1='8.93118'
          x2='7.65075'
          y2='7.95609'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#314DC9' stopOpacity={0} />
          <stop offset='0.2761' stopColor='#304BC4' stopOpacity='0.276' />
          <stop offset='0.5628' stopColor='#2B45B8' stopOpacity='0.563' />
          <stop offset='0.8535' stopColor='#243BA3' stopOpacity='0.854' />
          <stop offset={1} stopColor='#1F3596' />
        </linearGradient>
        <linearGradient
          id='paint15_linear_9783_4992'
          x1='7.17426'
          y1='9.57506'
          x2='6.16563'
          y2='9.57506'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#314DC9' stopOpacity={0} />
          <stop offset='0.2761' stopColor='#304BC4' stopOpacity='0.276' />
          <stop offset='0.5628' stopColor='#2B45B8' stopOpacity='0.563' />
          <stop offset='0.8535' stopColor='#243BA3' stopOpacity='0.854' />
          <stop offset={1} stopColor='#1F3596' />
        </linearGradient>
        <linearGradient
          id='paint16_linear_9783_4992'
          x1='10.5726'
          y1='8.79844'
          x2='12.6885'
          y2='10.9143'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#B3DAFE' />
          <stop offset={1} stopColor='#0182FC' />
        </linearGradient>
        <linearGradient
          id='paint17_linear_9783_4992'
          x1='11.3493'
          y1='8.93118'
          x2='11.3493'
          y2='7.95609'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#314DC9' stopOpacity={0} />
          <stop offset='0.2761' stopColor='#304BC4' stopOpacity='0.276' />
          <stop offset='0.5628' stopColor='#2B45B8' stopOpacity='0.563' />
          <stop offset='0.8535' stopColor='#243BA3' stopOpacity='0.854' />
          <stop offset={1} stopColor='#1F3596' />
        </linearGradient>
        <linearGradient
          id='paint18_linear_9783_4992'
          x1='10.8728'
          y1='9.57506'
          x2='9.86412'
          y2='9.57506'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#314DC9' stopOpacity={0} />
          <stop offset='0.2761' stopColor='#304BC4' stopOpacity='0.276' />
          <stop offset='0.5628' stopColor='#2B45B8' stopOpacity='0.563' />
          <stop offset='0.8535' stopColor='#243BA3' stopOpacity='0.854' />
          <stop offset={1} stopColor='#1F3596' />
        </linearGradient>
        <linearGradient
          id='paint19_linear_9783_4992'
          x1='6.87412'
          y1='12.4337'
          x2='8.98999'
          y2='14.5496'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#B3DAFE' />
          <stop offset={1} stopColor='#0182FC' />
        </linearGradient>
        <linearGradient
          id='paint20_linear_9783_4992'
          x1='7.65075'
          y1='12.5665'
          x2='7.65075'
          y2='11.5914'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#314DC9' stopOpacity={0} />
          <stop offset='0.2761' stopColor='#304BC4' stopOpacity='0.276' />
          <stop offset='0.5628' stopColor='#2B45B8' stopOpacity='0.563' />
          <stop offset='0.8535' stopColor='#243BA3' stopOpacity='0.854' />
          <stop offset={1} stopColor='#1F3596' />
        </linearGradient>
        <linearGradient
          id='paint21_linear_9783_4992'
          x1='7.17426'
          y1='13.2104'
          x2='6.16563'
          y2='13.2104'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#314DC9' stopOpacity={0} />
          <stop offset='0.2761' stopColor='#304BC4' stopOpacity='0.276' />
          <stop offset='0.5628' stopColor='#2B45B8' stopOpacity='0.563' />
          <stop offset='0.8535' stopColor='#243BA3' stopOpacity='0.854' />
          <stop offset={1} stopColor='#1F3596' />
        </linearGradient>
        <linearGradient
          id='paint22_linear_9783_4992'
          x1='10.5726'
          y1='12.4337'
          x2='12.6885'
          y2='14.5496'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#B3DAFE' />
          <stop offset={1} stopColor='#0182FC' />
        </linearGradient>
        <linearGradient
          id='paint23_linear_9783_4992'
          x1='11.3493'
          y1='12.5665'
          x2='11.3493'
          y2='11.5914'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#314DC9' stopOpacity={0} />
          <stop offset='0.2761' stopColor='#304BC4' stopOpacity='0.276' />
          <stop offset='0.5628' stopColor='#2B45B8' stopOpacity='0.563' />
          <stop offset='0.8535' stopColor='#243BA3' stopOpacity='0.854' />
          <stop offset={1} stopColor='#1F3596' />
        </linearGradient>
        <linearGradient
          id='paint24_linear_9783_4992'
          x1='10.8728'
          y1='13.2104'
          x2='9.86412'
          y2='13.2104'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#314DC9' stopOpacity={0} />
          <stop offset='0.2761' stopColor='#304BC4' stopOpacity='0.276' />
          <stop offset='0.5628' stopColor='#2B45B8' stopOpacity='0.563' />
          <stop offset='0.8535' stopColor='#243BA3' stopOpacity='0.854' />
          <stop offset={1} stopColor='#1F3596' />
        </linearGradient>
      </defs>
    </svg>
  );
}

export default HomeIcon;
