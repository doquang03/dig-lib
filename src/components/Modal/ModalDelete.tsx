import { InfoCircleOutlined, LoadingOutlined } from '@ant-design/icons';
import { Button, Modal, Spin, type ModalProps } from 'antd';
import { ReactNode } from 'react';
import 'css/ComponentAntD.css';

type Props = {
  handleCancel?: () => void;
  handleOk?: () => void;
  children?: ReactNode;
  labelOK?: string;
  loading?: boolean;
} & ModalProps;

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

function ModalDelete(props: Props) {
  const { handleCancel, handleOk, title, children, labelOK = 'Xóa', loading, ...rest } = props;

  return (
    <Modal
      closable={false}
      className='model-delete'
      {...rest}
      centered
      footer={[
        <Button key='back' className='gray-button' onClick={handleCancel} disabled={loading}>
          {loading && <Spin indicator={antIcon} className='mr-2' />} Quay về
        </Button>,
        <Button key='ok' className='btn btn-primary btn-sm red-button ml-[10px]' onClick={handleOk} disabled={loading}>
          {loading && <Spin indicator={antIcon} className='mr-2' />} {labelOK}
        </Button>
      ]}
    >
      <InfoCircleOutlined className='warning-icon' />
      <div className='title-h2'>{title}</div>
      {children}
    </Modal>
  );
}

export default ModalDelete;
