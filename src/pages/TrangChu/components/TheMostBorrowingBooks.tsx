import { Select } from 'components';
import BarChart from './BarChart';
import { ChangeEvent } from 'react';
import { useQuery } from '@tanstack/react-query';
import { bookstoreTypeApis } from 'apis';

type Props = {
  data: {
    label: string;
    value: any;
  }[];
  onTopChange: (e: ChangeEvent<HTMLSelectElement>) => void;
  onWarehouseChange: (e: ChangeEvent<HTMLSelectElement>) => void;
};

function TheMostBorrowingBooks(props: Props) {
  const { data, onTopChange, onWarehouseChange } = props;

  const { data: warehouseData } = useQuery({
    queryKey: ['LoaiKhoSach'],
    queryFn: () => bookstoreTypeApis.IndexByTreeCountBook()
  });

  const warehouseOptions =
    warehouseData?.data?.Item?.ListKhoSach?.map((warehouse) => ({
      label: warehouse.Ten,
      value: warehouse.Id
    })) || [];

  return (
    <div className='my-3 rounded-xl bg-white p-2 shadow-md'>
      <div className='flex items-center'>
        <h3 className='text-xl font-bold text-primary-10'>SÁCH ĐƯỢC MƯỢN NHIỀU NHẤT</h3>

        <div className='ml-auto'>
          <Select
            className='mr-2'
            items={[...Array(10)]
              .map((item, index) => ({
                label: `Top ${index + 1}`,
                value: `${index + 1}`
              }))
              .reverse()}
            onChange={onTopChange}
          />

          <Select items={[{ label: 'Chọn kho sách', value: '' }, ...warehouseOptions]} onChange={onWarehouseChange} />
        </div>
      </div>

      <BarChart
        options={{
          labels: { yAxisLabel: 'Tên sách', xAxisLabel: 'Số lượt' }
        }}
        data={data}
      />
    </div>
  );
}

export default TheMostBorrowingBooks;
