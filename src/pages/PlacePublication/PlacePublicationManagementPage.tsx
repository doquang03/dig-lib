import { path } from 'constants/path';
import { Outlet, useMatch } from 'react-router-dom';
import PlacePublicationManagermentContent from './PlacePublicationManagermentContent/PlacePublicationManagermentContent';

const PlacePublicationManagementPage = () => {
  const match = useMatch(path.noixuatban);

  return <>{Boolean(match) ? <PlacePublicationManagermentContent /> : <Outlet />}</>;
};

export default PlacePublicationManagementPage;
