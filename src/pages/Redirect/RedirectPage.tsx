/* eslint-disable react-hooks/exhaustive-deps */
import { Spin } from 'antd';
import Modal from 'antd/es/modal/Modal';
import { IDENTITY_CONFIG } from 'constants/config';
import { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import { hasAuthParams, type AuthContextProps } from 'react-oidc-context';

const RedirectPage = (props: { auth: AuthContextProps }) => {
  const { auth } = props;

  const [isError, setIsError] = useState<boolean>(false);

  useEffect(() => {
    // token is expiring => signinSilent
    // the `return` is important - addAccessTokenExpiring() returns a cleanup function
    return auth.events.addAccessTokenExpiring(() => {
      auth.signinSilent();
    });
  }, [auth.events, auth.signinSilent]);

  useEffect(() => {
    const signinRedirect = async () => {
      try {
        if (
          !hasAuthParams() &&
          !auth.isAuthenticated &&
          !auth.activeNavigator &&
          !auth.isLoading &&
          // prevent from re-call server many times when error happens
          (!isError || !auth.error)
        ) {
          await auth.signinRedirect({ redirect_uri: IDENTITY_CONFIG.redirect_uri });
        }
      } catch (error) {
        setIsError(true);
        auth.clearStaleState();
        auth.removeUser();
        auth.stopSilentRenew();
        // auth.signoutSilent({ post_logout_redirect_uri: IDENTITY_CONFIG.post_logout_redirect_uri });
      }
    };

    if (!isError) {
      signinRedirect();
    }
  }, [auth.isAuthenticated, auth.activeNavigator, auth.isLoading, isError]);

  return (
    <div className='grid h-screen place-items-center'>
      <Modal title='Đã có lỗi trong quá trình xác thực!' open={isError} footer={null} closable={false}>
        {!!auth.error?.message ? (
          auth.error.message
        ) : (
          <>
            <p>Vui lòng liên hệ CSKH</p>
            <a href='tel:02822532586' className='block'>
              Điện thoại: (028) 22 532 586
            </a>
            <address>
              <a href='mailto:cskh.bitech@gmail.com'>Email: cskh.bitech@gmail.com</a>
            </address>
          </>
        )}
      </Modal>

      {auth.isLoading && !isError && (
        <>
          <Helmet>
            <meta charSet='utf-8' />
            <title>Loading... - Thư viện số</title>
            <link rel='Loading' href={window.location.href} />
          </Helmet>
          <div className='flex items-center justify-center gap-4'>
            <p> Đang chuyển hướng</p> <Spin size='large' spinning />
          </div>
        </>
      )}
    </div>
  );
};

export default RedirectPage;
