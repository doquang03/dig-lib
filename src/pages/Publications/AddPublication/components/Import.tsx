import { DeleteFilled, PlusOutlined } from '@ant-design/icons';
import { Button, DatePicker, Input, InputNumber, Select } from 'components';
import type {
  ArrayPath,
  Control,
  FieldArrayMethodProps,
  FieldArrayWithId,
  FieldError,
  FieldErrorsImpl,
  FieldValues,
  Merge,
  UseFieldArrayRemove,
  UseFormRegister
} from 'react-hook-form';
import { AutoComplete } from 'components/AutoComplete';
import dayjs from 'dayjs';
import { useUser } from 'contexts/user.context';

type KhoSachFormValues = {
  KhoSach: string;
  SoLuong: string;
  TrangThai: string;
  NguonCungCap: string;
  SoCT: string;
  VSTQ: string;
  NgayNhap: string;
  GhiChu: string;
};

type Props<T extends FieldValues> = {
  khoSachOptions: Selections;
  nguonCungCapOptions: Selections;
  trangThaiOptions: Selections;
  fields: FieldArrayWithId<T, ArrayPath<T>, 'id'>[];
  append: (
    value: Partial<KhoSachFormValues> | Partial<KhoSachFormValues>[],
    options?: FieldArrayMethodProps | undefined
  ) => void;
  remove: UseFieldArrayRemove;
  register: UseFormRegister<T>;
  errors: Merge<FieldError, (Merge<FieldError, FieldErrorsImpl<KhoSachFormValues>> | undefined)[]> | undefined;
  control: Control<T>;
};

const initialValues = {
  KhoSach: '',
  SoLuong: '1',
  TrangThai: '',
  NguonCungCap: '',
  SoCT: '',
  VSTQ: '',
  NgayNhap: '',
  GhiChu: ''
};

export default function Import<T extends FieldValues>({
  khoSachOptions,
  nguonCungCapOptions,
  trangThaiOptions,
  append,
  remove,
  fields,
  register,
  errors,
  control
}: Props<T>) {
  const handleAdd = () => {
    append(initialValues);
  };

  const handleDelete = (index: number) => () => remove(index);

  const { isAllowedAdjustment } = useUser();

  return (
    <div>
      <table className='table w-full overflow-x-auto shadow-md' width={'100%'}>
        <thead className='bg-primary-10  text-center text-white'>
          <tr>
            <td width={'10%'} className='text-center'>
              STT
            </td>
            <td width={'10%'} className='text-center'>
              Kho
            </td>
            <td width={'10%'} className='text-center'>
              Số lượng
            </td>
            <td width={'10%'} className='text-center'>
              Tình trạng
            </td>
            <td width={'10%'} className='text-center'>
              Nguồn cung cấp
            </td>
            <td width={'10%'} className='text-center'>
              Số CT
            </td>
            <td width={'10%'} className='text-center'>
              Số VSTQ
            </td>
            <td width={'10%'} className='text-center'>
              Ngày vào sổ
            </td>
            <td width={'7%'}></td>
          </tr>
        </thead>
        <tbody className='bg-white'>
          {fields.map((row, index) => {
            return (
              <tr key={row.id}>
                <td className='whitespace-nowrap text-center' width={'7%'}>
                  {index + 1}
                </td>
                <td width={'10%'} className='text-center'>
                  <AutoComplete
                    className='w-full grow'
                    name={`NhapKho.${index}.KhoSach` as const}
                    placeholder='Nhập kho'
                    control={control}
                    items={khoSachOptions}
                    errorMessage={errors?.[index]?.KhoSach?.message}
                    showSearch
                  />
                </td>
                <td width={'10%'} className='text-center'>
                  <InputNumber
                    control={control}
                    //  @ts-ignore
                    name={`NhapKho.${index}.SoLuong` as const}
                    placeholder='Nhập số lượng'
                    errorMessage={errors?.[index]?.SoLuong?.message}
                  />
                </td>
                <td className='text-center' width={'10%'}>
                  <Select
                    items={trangThaiOptions}
                    //  @ts-ignore
                    name={`NhapKho.${index}.TrangThai` as const}
                    register={register}
                    errorMessage={errors?.[index]?.TrangThai?.message}
                  />
                </td>
                <td className='text-center' width={'10%'}>
                  <Select
                    items={nguonCungCapOptions}
                    //  @ts-ignore
                    name={`NhapKho.${index}.NguonCungCap` as const}
                    register={register}
                    errorMessage={errors?.[index]?.NguonCungCap?.message}
                    className='w-full'
                  />
                </td>
                <td width={'10%'}>
                  <Input
                    placeholder='Số CT'
                    //  @ts-ignore
                    name={`NhapKho.${index}.SoCT` as const}
                    register={register}
                    errorMessage={errors?.[index]?.SoCT?.message}
                    maxLength={100}
                  />
                </td>
                <td width={'10%'}>
                  <Input
                    placeholder='Số VSTQ'
                    //  @ts-ignore
                    name={`NhapKho.${index}.VSTQ` as const}
                    register={register}
                    errorMessage={errors?.[index]?.VSTQ?.message}
                    maxLength={100}
                  />
                </td>
                <td width={'10%'}>
                  <DatePicker
                    control={control}
                    //  @ts-ignore
                    name={`NhapKho.${index}.NgayNhap` as const}
                    disabled={false}
                    errorMessage={errors?.[index]?.NgayNhap?.message}
                    disabledDate={(current) => {
                      return current && current > dayjs().endOf('day');
                    }}
                    className='w-full'
                  />
                </td>
                <td width={'7%'}>
                  <button className='mx-2' onClick={handleDelete(index)} type='button'>
                    <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>

      {!!fields.length && (
        <>
          <label className='font-bold'>Ghi chú:</label>

          {/* @ts-ignore */}
          <Input placeholder='Ghi chú' component='textarea' register={register} name='GhiChu' />
        </>
      )}

      <div className={isAllowedAdjustment ? 'mt-4 flex justify-end' : 'hidden'}>
        <Button onClick={handleAdd} type='button'>
          <PlusOutlined /> Thêm phiếu nhập kho
        </Button>
      </div>
    </div>
  );
}
