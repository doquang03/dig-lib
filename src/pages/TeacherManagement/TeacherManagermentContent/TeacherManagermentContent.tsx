import { DeleteFilled, EditOutlined, InfoCircleOutlined, LockOutlined, IdcardOutlined } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Tooltip } from 'antd';
import Table, { type ColumnsType } from 'antd/es/table';
import { teacherApis } from 'apis';
import classNames from 'classnames';
import {
  Button,
  Empty,
  Input,
  Loading,
  ModalAlert,
  ModalDelete,
  Select,
  SizeChanger,
  Title,
  TitleDelete
} from 'components';
import type { ButtonCustomProps } from 'components/Button/Button';
import { EUser, GENDER_OPTIONS, STATUS_OPTIONS } from 'constants/options';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import { usePaginationNavigate, useQueryConfig } from 'hooks';
import { isEmpty, omitBy, trim } from 'lodash';
import { Key, useCallback, useEffect, useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import { createSearchParams, Link, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { convertDate, download, getSerialNumber } from 'utils/utils';

type Mock = {
  Mail: string;
  GioiTinh: string;
  Id: string;
  Ten: string;
  TrangThai: number;
  NgaySinh: string;
  MaSoThanhVien: string;
  DaTra: boolean;
};

type FormInput = {
  TextForSearch: string;
  GioiTinh: string;
  TrangThai: string;
};

const TeacherManagermentContent = () => {
  const [selectedTeachers, setSelectedTeachers] = useState<Mock[]>([]);
  const [selectedTeacher, setSelectedTeacher] = useState<{
    Teacher: Mock | undefined;
    type: 'lock' | 'delete' | 'unlock';
  }>();
  const [visibleModal, setVisibleModal] = useState<boolean>(false);
  const [totalGiaoVien, setTotalGiaoVien] = useState(0);
  const [confirm, setConfirm] = useState(false);

  const { register, handleSubmit, reset, setValue } = useForm<FormInput>({
    defaultValues: {
      TextForSearch: '',
      GioiTinh: '',
      TrangThai: ''
    }
  });
  const navigate = useNavigate();

  const handleNavigation = usePaginationNavigate();

  const queryConfig = useQueryConfig();

  const { isAllowedAdjustment } = useUser();

  const {
    data: dataTeacher,
    refetch,
    isFetching
  } = useQuery({
    queryKey: ['teachers', queryConfig],
    queryFn: () => {
      let page = 1;
      if (typeof queryConfig.page === 'string') page = parseInt(queryConfig.page);
      let queryString = `?page=${page}&sizeNumber=${queryConfig.pageSize}`;
      if (queryConfig.TextForSearch !== undefined) queryString += `&TextForSearch=${queryConfig.TextForSearch}`;
      if (queryConfig.GioiTinh !== undefined) queryString += `&GioiTinh=${queryConfig.GioiTinh}`;
      if (queryConfig.TrangThai !== undefined) queryString += `&TrangThai=${parseInt(queryConfig.TrangThai)}`;
      let request = teacherApis.getTeachers(queryString);
      return request;
    },
    onSuccess: () => {
      setSelectedTeacher(undefined);
      setVisibleModal(false);
    }
  });

  const listGiaoVien = useMemo(() => {
    if (!dataTeacher?.data?.Item) return;
    const { ListThanhVien, count } = dataTeacher?.data?.Item;
    setTotalGiaoVien(count);
    return ListThanhVien;
  }, [dataTeacher?.data?.Item]);

  const { mutate: handleDownloadExcel, isLoading: isLoadingDownload } = useMutation({
    mutationFn: () => teacherApis.DownFileExcel({}),
    onSuccess: (res) => {
      download('DanhSachGiaoVien.xls', res.data);
    }
  });

  const { mutate: handleDeleteSingle, isLoading: isLoadingDeleteSingle } = useMutation({
    mutationFn: (Id: string) => teacherApis.DeleteSingle(Id),
    onSuccess: (res) => {
      if (listGiaoVien.length === 1) handleResetField();
      else refetch();
      setSelectedTeachers(selectedTeachers.filter((item) => item.Id !== selectedTeacher?.Teacher?.Id));
      toast.success('Xóa giáo viên thành công');
      setSelectedTeacher(undefined);
    }
  });

  const { mutate: handleDeleteMulti, isLoading: isLoadingDeleteMulti } = useMutation({
    mutationFn: (payload: Array<string>) => teacherApis.DeleteMulti(payload),
    onSuccess: (res, payload) => {
      handleNavigation({ page: '1' });
      toast.success('Xóa giáo viên thành công');
      refetch();
    }
  });

  const { mutate: handleChangeStatus, isLoading: isLoadingChangeStatus } = useMutation({
    mutationFn: (payload: { teacher: Mock; label: string }) => teacherApis.ChangeStatus(payload.teacher.Id),
    onSuccess: (res, payload) => {
      payload.teacher.TrangThai = payload.teacher.TrangThai === EUser.Active ? EUser.DeActive : EUser.Active;
      toast.success(payload.label + ' giáo viên thành công');
      setSelectedTeacher(undefined);
    }
  });

  const handleDeleteTeachers = () => {
    let tempArray = [];
    for (let i = 0; i < selectedTeachers.length; i++) {
      if (selectedTeachers[i].TrangThai === EUser.DeActive || selectedTeachers[i].DaTra === false) continue;
      tempArray.push(selectedTeachers[i].Id);
    }
    handleDeleteMulti(tempArray);
  };

  const columns: ColumnsType<Mock> = useMemo(() => {
    return [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        render: (value, record, index) => {
          return (
            queryConfig.page && queryConfig.pageSize && getSerialNumber(+queryConfig.page, +queryConfig.pageSize, index)
          );
        }
      },
      {
        title: 'Tên giáo viên',
        dataIndex: 'Ten',
        key: 'Ten',
        render: (value, { Ten }) =>
          Ten.length > 25 ? (
            <Tooltip placement='topLeft' title={Ten} arrow={true} className='text-left'>
              <p>{Ten.substring(0, 25).concat('...')}</p>
            </Tooltip>
          ) : (
            <p className='text-left'>{Ten}</p>
          ),
        onCell: (record) => ({
          className: 'text-left w-[280px]'
        })
      },
      {
        title: 'Mã giáo viên',
        dataIndex: 'MaSoThanhVien',
        key: 'MaSoThanhVien'
      },
      {
        title: 'Tổ',
        dataIndex: 'ChucVu',
        key: 'ChucVu',
        // @ts-ignore
        render: (value, { ChucVu }) => <p className='text-left'>{ChucVu}</p>
      },
      {
        title: 'Ngày sinh',
        dataIndex: 'NgaySinh',
        key: 'NgaySinh',
        render: (value, { NgaySinh }) => <p>{convertDate(NgaySinh)}</p>
      },
      {
        title: 'Giới tính',
        dataIndex: 'GioiTinh',
        key: 'GioiTinh'
      },
      {
        title: 'Tình trạng',
        dataIndex: 'TrangThai',
        key: 'TrangThai',
        render: (value, { TrangThai }) => <p>{TrangThai === EUser.Active ? 'Đang kích hoạt' : 'Đang bị khóa'}</p>
      },
      {
        title: 'Hành động',
        dataIndex: 'actions',
        key: 'actions',
        render: (value, record) => {
          return (
            <>
              <Tooltip title='Cập nhật'>
                <Link to={`CapNhat/${record.Id}`} className='mx-2'>
                  <EditOutlined style={{ fontSize: '20px', color: '#08c' }} />
                </Link>
              </Tooltip>

              <Tooltip title='Xuất thẻ thư viện'>
                <button
                  className={classNames('', {
                    'mx-2': isAllowedAdjustment,
                    hidden: !isAllowedAdjustment
                  })}
                  onClick={(e) => {
                    e.stopPropagation();
                    navigate(path.xuatthethuviengiaovien, { state: { selectedTeachers: [record.Id + ''] } });
                  }}
                >
                  <IdcardOutlined style={{ fontSize: '20px', color: '#08c' }} />
                </button>
              </Tooltip>

              <Tooltip title={record.TrangThai === 0 ? 'Khóa' : 'Kích hoạt'}>
                <button
                  className={classNames('', {
                    'mx-2': isAllowedAdjustment,
                    hidden: !isAllowedAdjustment
                  })}
                  onClick={(e) => {
                    e.stopPropagation();
                    setSelectedTeacher((preveState) => {
                      return {
                        ...preveState,
                        Teacher: record,
                        type: record.TrangThai === EUser.Active ? 'lock' : 'unlock'
                      };
                    });
                  }}
                >
                  <LockOutlined
                    style={{ fontSize: '20px', color: record.TrangThai === EUser.Active ? '#08c' : 'red' }}
                  />
                </button>
              </Tooltip>

              <Tooltip title={'Xóa'}>
                <button
                  className={classNames('', {
                    'mx-2': isAllowedAdjustment,
                    hidden: !isAllowedAdjustment
                  })}
                  onClick={(e) => {
                    e.stopPropagation();
                    setSelectedTeacher((preveState) => {
                      return { ...preveState, Teacher: record, type: 'delete' };
                    });
                  }}
                >
                  <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
                </button>
              </Tooltip>
            </>
          );
        },
        onCell: (record) => ({
          className: 'text-left w-[220px]'
        })
      }
    ];
  }, [navigate, queryConfig.page, queryConfig.pageSize]);

  const onSubmit = handleSubmit((data) => {
    handleNavigation({
      ...omitBy({ ...data, TextForSearch: trim(data.TextForSearch, ' ').replace(/\s\s+/g, ' ') }, isEmpty),
      page: '1'
    });
  });

  const onPaginate = useCallback(
    (page: number, pageSize: number) => {
      handleNavigation({ ...queryConfig, page: page + '', pageSize: pageSize + '' });
    },
    [handleNavigation, queryConfig]
  );

  const pagination = useMemo(() => {
    return {
      current: (!!queryConfig.page && +queryConfig.page) || 1,
      pageSize: (!!queryConfig.pageSize && +queryConfig.pageSize) || 30,
      total: totalGiaoVien,
      onChange: onPaginate,
      hideOnSinglePage: true,
      showSizeChanger: false,
      showQuickJumper: true,
      locale: { jump_to: '', page: '' }
    };
  }, [onPaginate, queryConfig.page, queryConfig.pageSize, totalGiaoVien]);

  const onSelectChange = (_: Key[], selectedRowValue: Mock[]) => {
    setSelectedTeachers(selectedRowValue);
  };

  const rowSelections = useMemo(() => {
    return {
      selectedRowKeys: selectedTeachers.map((item) => item.Id),
      preserveSelectedRowKeys: true,
      onChange: onSelectChange
    };
  }, [selectedTeachers]);

  useEffect(() => {
    Object.keys(queryConfig).forEach((key) => {
      if (!key) {
        return;
      }
      // @ts-ignore
      setValue(key, queryConfig[key]);
    });
  }, [queryConfig, setValue]);

  const handleResetField = () => {
    setSelectedTeachers([]);
    const { pathname } = window.location;
    navigate({
      pathname: pathname,
      search: createSearchParams({}).toString()
    });
    reset();
  };

  const handleSelectedTeachers = () => {
    navigate(path.themgiaovien);
  };

  const handleAddTeacherByExcel = () => {
    navigate(path.themgiaovienexcel);
  };

  const handleAddAvatarTeacher = () => {
    navigate(path.themhinhdaidiengiaovien);
  };

  const handleExportCard = () => {
    if (!selectedTeachers.length) {
      return;
    }
    let tempData = selectedTeachers?.map((record) => {
      return record.Id;
    });
    navigate(path.xuatthethuviengiaovien, { state: { selectedTeachers: tempData } });
  };

  let locale = {
    emptyText: 'Không tìm được kết quả phù hợp'
  };

  return (
    <div className='p-5'>
      <Title title='DANH SÁCH GIÁO VIÊN' />
      <form className='my-5 flex flex-col flex-wrap gap-2 bg-secondary-10 py-3 px-3.5 md:flex-row' onSubmit={onSubmit}>
        <Input
          placeholder='Nhập mã, tên giáo viên, tổ'
          containerClassName='w-1/4'
          name='TextForSearch'
          register={register}
        />

        <Select items={GENDER_OPTIONS} className='w-full md:w-1/4' name='GioiTinh' register={register} />

        <Select items={STATUS_OPTIONS} className='w-full md:w-1/4' name='TrangThai' register={register} />

        <Button type='button' variant='secondary' className='font-semibold' onClick={handleResetField}>
          Làm mới
        </Button>

        <Button type='submit' variant='default' className='font-semibold'>
          Tìm kiếm
        </Button>
      </form>

      <div
        className={classNames('my-4', {
          hidden: !isAllowedAdjustment,
          'flex-col flex-wrap gap-1 md:flex md:md:flex-row': isAllowedAdjustment
        })}
      >
        {[
          { label: 'Thêm giáo viên', variant: 'default', onClick: handleSelectedTeachers },
          { label: ' Thêm từ Excel', variant: 'default', onClick: handleAddTeacherByExcel },
          {
            label: 'Xuất thẻ thư viện',
            variant: 'default',
            onClick: handleExportCard,
            disabled: !selectedTeachers.length
          },
          { label: 'Cập nhật ảnh đại diện', variant: 'default', onClick: handleAddAvatarTeacher },
          {
            label: 'Tải danh sách giáo viên',
            variant: 'default',
            onClick: () => {
              handleDownloadExcel();
            }
          },
          // { label: 'Đồng bộ', variant: 'default' },
          {
            label: 'Xóa giáo viên',
            variant: 'danger',
            onClick: () => {
              if (!selectedTeachers.length) {
                return;
              }
              setVisibleModal(true);
            },
            disabled: !selectedTeachers.length
          }
        ].map(({ label, variant, disabled, onClick }) => {
          const variantResult = disabled ? 'disabled' : variant;
          return (
            <Button
              variant={variantResult as ButtonCustomProps['variant']}
              className={classNames('w-[100%] md:w-auto', {
                'bg-gray-400': disabled,
                'cursor-not-allowed': disabled,
                'hover:bg-gray-400': disabled
              })}
              key={label}
              onClick={onClick}
              disabled={disabled}
            >
              {label}
            </Button>
          );
        })}
      </div>

      <div className='mt-6'>
        {Number.isInteger(queryConfig.page && +queryConfig.page) ? (
          <Table
            loading={isFetching && !listGiaoVien?.length}
            rowSelection={rowSelections}
            columns={columns}
            dataSource={listGiaoVien || []}
            pagination={pagination}
            scroll={{ x: 980 }}
            rowKey={(record) => record.Id}
            className='custom-table'
            locale={{
              emptyText: () => <Empty label={locale.emptyText} />
            }}
            bordered
          />
        ) : (
          <div className='rounded-md bg-white p-2 shadow-md'>
            <p className=' text-center'>
              Đã có lỗi xảy ra.
              <button onClick={handleResetField}>Vui lòng làm mới tìm kiếm.</button>
            </p>
          </div>
        )}
      </div>

      <div
        className={classNames('relative', {
          // @ts-ignore
          'mt-[64px]': (queryConfig?.page && +queryConfig.pageSize) >= totalGiaoVien
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger
            visible={!!listGiaoVien?.length && Number.isInteger(queryConfig.page && +queryConfig.page)}
            value={listGiaoVien?.length + ''}
            total={totalGiaoVien.toString()}
          />
        </div>
      </div>

      {/* Lock Modal for one Teacher */}
      <ModalDelete
        open={Boolean(selectedTeacher?.Teacher) && selectedTeacher?.type === 'lock'}
        closable={false}
        loading={isLoadingChangeStatus}
        style={{ width: '512px' }}
        title={
          <>
            <TitleDelete
              firstText={'Bạn có chắn chắn muốn khóa giáo viên'}
              secondText={selectedTeacher?.Teacher?.Ten}
            ></TitleDelete>
          </>
        }
        labelOK={'Khóa'}
        handleCancel={() => {
          setSelectedTeacher(undefined);
        }}
        handleOk={() => {
          handleChangeStatus({ teacher: selectedTeacher?.Teacher as Mock, label: 'Khóa' });
        }}
      />

      {/* Unlock Modal for one Teacher */}
      <ModalDelete
        open={Boolean(selectedTeacher?.Teacher) && selectedTeacher?.type === 'unlock'}
        closable={false}
        title={
          <TitleDelete
            firstText={'Bạn có chắn chắn muốn kích hoạt giáo viên'}
            secondText={selectedTeacher?.Teacher?.Ten}
          ></TitleDelete>
        }
        labelOK={'Kích hoạt'}
        handleCancel={() => {
          setSelectedTeacher(undefined);
        }}
        handleOk={() => {
          handleChangeStatus({ teacher: selectedTeacher?.Teacher as Mock, label: 'Kích hoạt' });
        }}
        loading={isLoadingChangeStatus}
      />

      {/* Deleted Modal for one Teacher */}
      <ModalDelete
        open={Boolean(selectedTeacher?.Teacher) && selectedTeacher?.type === 'delete'}
        closable={false}
        loading={isLoadingDeleteSingle || isFetching}
        title={
          <TitleDelete
            firstText={'Bạn có chắn chắn muốn xóa giáo viên'}
            secondText={selectedTeacher?.Teacher?.Ten}
          ></TitleDelete>
        }
        handleCancel={() => setSelectedTeacher(undefined)}
        handleOk={() => {
          if (!selectedTeacher?.Teacher?.DaTra || selectedTeacher?.Teacher?.TrangThai === EUser.DeActive) {
            setSelectedTeacher(undefined);
            !selectedTeacher?.Teacher?.DaTra && toast.error(`Giáo viên ${selectedTeacher?.Teacher?.Ten} chưa trả sách`);
            selectedTeacher?.Teacher?.TrangThai === EUser.DeActive &&
              toast.error(`Giáo viên ${selectedTeacher?.Teacher?.Ten} đang bị khóa`);
            return;
          }
          handleDeleteSingle(selectedTeacher?.Teacher?.Id);
        }}
      />

      {/* Deleted Modal for many Teachers */}
      <ModalDelete
        open={!!selectedTeachers.length && visibleModal}
        closable={false}
        title={
          <>
            <h3 className='text-bold'>Bạn có muốn chắc chắn xóa những giáo viên này không?</h3>
            <p className='text-[16px] font-extralight leading-normal'>
              Bạn sẽ không thể khôi phục sau khi xóa, những giáo viên đang mượn sách sẽ không thể xóa.
            </p>
          </>
        }
        loading={isLoadingDeleteMulti || isFetching}
        handleCancel={() => {
          setVisibleModal(false);
        }}
        handleOk={() => {
          handleDeleteTeachers();
        }}
      >
        <div className='max-h-[300px] overflow-x-auto'>
          {selectedTeachers.map(({ MaSoThanhVien, Ten, TrangThai, DaTra }, index) => {
            const inactive = TrangThai === EUser.DeActive || DaTra === false;
            return (
              <div key={MaSoThanhVien} className='grid grid-cols-12 items-center border-b-2 px-2 py-2 font-bold'>
                <div className='col-span-1'>
                  <p className={inactive ? 'text-danger-10' : ''}>{index + 1} </p>
                </div>
                <div className='col-span-5'>
                  <p
                    className={classNames('truncate text-left', {
                      'text-danger-10': inactive
                    })}
                  >
                    {Ten}
                  </p>
                </div>
                <div className='col-span-6 grid grid-cols-12'>
                  <div className='col-span-6'>
                    <p
                      className={classNames('mr-4', {
                        'text-danger-10': inactive
                      })}
                    >
                      {MaSoThanhVien}
                    </p>
                  </div>
                  <div className='col-span-6 flex-row items-center' style={{ display: 'flex' }}>
                    {inactive && (
                      <>
                        <p className='flex items-center gap-1 font-light text-danger-10'>
                          <InfoCircleOutlined style={{ color: 'red' }} />

                          <span className='text-[12px]'>
                            {TrangThai === EUser.DeActive && 'Đang bị khóa'}
                            {'\n'}
                            {!DaTra && 'Chưa trả sách'}
                          </span>
                        </p>
                      </>
                    )}
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </ModalDelete>
      <ModalAlert
        title={'Đã hủy'}
        open={confirm}
        closable={false}
        handleOk={() => {
          setConfirm(false);
        }}
      ></ModalAlert>
      <Loading open={isLoadingDownload}></Loading>
    </div>
  );
};

export default TeacherManagermentContent;
