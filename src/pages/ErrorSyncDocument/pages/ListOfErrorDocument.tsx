import { Pagination, TabsProps } from 'antd';
import { Button, SizeChanger, Tabs, Title } from 'components';
import { path } from 'constants/path';
import { useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { DocumentTab } from './components';
import { useQuery } from '@tanstack/react-query';
import { errorSyncDocuments } from 'apis';

function ListOfErrorDocument() {
  const [params, setParams] = useState<{
    page: number;
    pageSize: number;
    key: 'paper' | 'digital';
  }>({
    page: 1,
    pageSize: 30,
    key: 'paper'
  });

  const navigate = useNavigate();

  const { id } = useParams();

  const { data: listOfErrorSyncPaperDocumentById } = useQuery({
    queryKey: ['getListOfErrorSyncDocumentById'],
    queryFn: () =>
      errorSyncDocuments.getListOfErrorSyncDocById({
        page: params.page,
        pageSize: params.pageSize,
        IdLuotDongBo: id + '',
        IsPaper: true
      }),
    enabled: !!id
  });

  const { data: listOfErrorSyncDigitalDocumentById } = useQuery({
    queryKey: ['listOfErrorSyncDigitalDocumentById'],
    queryFn: () =>
      errorSyncDocuments.getListOfErrorSyncDocById({
        page: params.page,
        pageSize: params.pageSize,
        IdLuotDongBo: id + '',
        IsPaper: false
      }),
    enabled: !!id
  });

  const { Count: numberOfDigitalDocuments, ListModel: listOfDigitalDocument } = listOfErrorSyncDigitalDocumentById?.data
    .Item || { Count: 0, ListModel: [] };

  const { Count: numberOfPaperDocuments, ListModel: listOfPaperDocument } = listOfErrorSyncPaperDocumentById?.data
    .Item || {
    Count: 0,
    ListModel: []
  };

  const tabItems: TabsProps['items'] = [
    {
      key: 'paper',
      label: `Tài liệu giấy (${numberOfPaperDocuments})`,
      children: <DocumentTab key='paper' documents={listOfPaperDocument} />
    },
    {
      key: 'digital',
      label: `Tài liệu số  (${numberOfDigitalDocuments})`,
      children: <DocumentTab key='digital' documents={listOfDigitalDocument} />
    }
  ];

  function handleChangeTab(key: 'paper' | 'digital') {
    setParams({
      page: 1,
      pageSize: 30,
      key
    });
  }

  return (
    <div className='my-3'>
      <Title title='DANH SÁCH TÀI LIỆU BỊ LỖI' />

      <div className='my-3'>
        <Tabs
          items={tabItems}
          activeKey={params.key}
          onChange={(_key) => handleChangeTab(_key as 'paper' | 'digital')}
        />
      </div>

      <div className='flex justify-end'>
        <Button variant='secondary' onClick={() => navigate(path.errorSyncDocument)}>
          Quay về
        </Button>
      </div>

      <div className='flex flex-col items-center justify-between gap-3 md:flex-row'>
        <SizeChanger
          visible={true}
          value={
            params.key === 'paper' ? listOfPaperDocument?.length.toString() : listOfDigitalDocument?.length.toString()
          }
          total={params.key === 'paper' ? numberOfPaperDocuments.toString() : numberOfDigitalDocuments.toString()}
          onChange={(pageSize) => setParams((prevState) => ({ ...prevState, pageSize: +pageSize }))}
        />

        <Pagination
          current={params.page}
          total={params.key === 'paper' ? numberOfPaperDocuments : numberOfDigitalDocuments}
          onChange={(page, pageSize) => setParams((prevState) => ({ ...prevState, pageSize }))}
          showSizeChanger={false}
          pageSize={params.pageSize}
          showQuickJumper={true}
          locale={{ jump_to: '', page: '' }}
        />
      </div>
    </div>
  );
}

export default ListOfErrorDocument;
