import { path } from 'constants/path';
import { Outlet, useMatch } from 'react-router-dom';
import StatisticalManagermentContent from './StatisticalManagermentContent/StatisticalManagermentContent';

const StatisticalManagementPage = () => {
  const match = useMatch(path.thongke);

  return <>{Boolean(match) ? <StatisticalManagermentContent /> : <Outlet />}</>;
};

export default StatisticalManagementPage;
