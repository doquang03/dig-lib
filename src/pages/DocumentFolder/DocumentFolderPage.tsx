import { DeleteFilled, PlusSquareFilled, PrinterFilled, EditOutlined } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Table, Tag, Tooltip } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { documentFolderApis } from 'apis';
import { Button, Loading, ModalDelete, SearchForm, Title, SizeChanger, Empty } from 'components';
import { usePaginationNavigate, useQueryConfig } from 'hooks';
import { Key, useCallback, useMemo, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { download } from 'utils/utils';
import DeleteMultiModal from './components/DeleteMultiModal';
import DocumentFolderModal from './components/Modal';
import classNames from 'classnames';
import { useUser } from 'contexts/user.context';
import { ItemType } from 'antd/es/breadcrumb/Breadcrumb';

const locale = { emptyText: 'Không có dữ liệu' };

const DocumentFolderPage = () => {
  const [selectedDocumentFolders, setSelectedDocumentFolders] = useState<DocumentFolder[]>([]);
  const [documentFolder, setDocumentFolder] = useState<DocumentFolder | undefined>(undefined);
  const [visibleInfoModal, setVisibleInfoModal] = useState<boolean>(false);
  const [visibleDeleModal, setVisibleDeleteModal] = useState<boolean>(false);
  const [visibleDelMultiModal, setVisibleMultiModal] = useState<boolean>(false);
  const [parent, setParent] = useState<DocumentFolder>();

  const queryConfig = useQueryConfig();

  const ref = useRef<HTMLDivElement | null>(null);

  const navigate = useNavigate();

  const { isAllowedAdjustment } = useUser();

  const handleNavigation = usePaginationNavigate();

  const [routes, setRoutes] = useState<ItemType[]>([
    {
      breadcrumbName: 'Thư mục sách chính',
      path: ''
    }
  ]);

  const { page, pageSize, TextForSearch, Type } = queryConfig;

  const {
    data: dataDocument,
    refetch,
    isLoading: isLoadingDocumentFolders
  } = useQuery({
    queryKey: ['documentFolders', page, pageSize, TextForSearch, Type],
    queryFn: () =>
      documentFolderApis.getDocuments({ Page: Number(page + ''), PageSize: Number(pageSize + ''), TextForSearch, Type })
  });

  const filterList = useMemo(() => {
    if (!dataDocument) return [];
    const data = dataDocument?.data?.Item?.ListThuMucSach;
    type dicnationary = {
      [key: string]: any;
    };

    let checkParent: dicnationary = {};
    let final: DocumentFolder[] = [];
    for (let i = 0; i < data.length; i++) {
      let tempData: DocumentFolder = {
        Id: data[i].Id,
        CreateDateTime: data[i].CreateDateTime,
        IdParent: data[i].IdParent,
        TenThuMuc: data[i].TenThuMuc,
        MaThuMuc: data[i].MaThuMuc,
        Type: data[i].Type,
        GhiChu: data[i].GhiChu,
        IsActive: data[i].IsActive,
        IsUsed: data[i].IsUsed
      };

      if (checkParent[data[i].Id] === undefined) checkParent[data[i].Id] = tempData;

      if (data[i].IdParent === '') {
        final.push(tempData);
      } else {
        if (checkParent[data[i].IdParent]?.children === undefined) checkParent[data[i].IdParent].children = [];
        checkParent[data[i].IdParent].children.push(tempData);
      }
    }

    return final;
  }, [dataDocument]);

  const { mutate: deleteDocumentFolder, isLoading: isLoadingDeleteFolder } = useMutation({
    mutationFn: () =>
      documentFolderApis.deleteDocuments(selectedDocumentFolders.map((documentFolder) => documentFolder.Id)),
    onSuccess: () => {
      toast.success('Xóa thư mục tài liệu thành công.');
      refetch();
      setVisibleDeleteModal(false);
      setDocumentFolder(undefined);
    }
  });

  const { mutate: printDocumentFolder, isLoading: isLoadingPrintDocumentFolders } = useMutation({
    mutationFn: documentFolderApis.printDocuments,
    onSuccess: (data) => {
      download(`Sach_ThuMuc.doc`, data.data);

      toast.success('In thư mục tài liệu thành công');
    }
  });

  const count = useMemo(() => {
    if (!dataDocument?.data?.Item) {
      return 0;
    }

    return dataDocument?.data?.Item.count;
  }, [dataDocument]);

  console.log(dataDocument?.data?.Item?.ListThuMucSach);

  const taoRoutes = (parentId: string) => {
    setRoutes([
      {
        breadcrumbName: 'Thư mục sách chính',
        path: ''
      }
    ]);
    console.log(parentId, dataDocument?.data?.Item?.ListThuMucSach);
    if (!dataDocument?.data?.Item?.ListThuMucSach) return;
    for (let index = 0; index < dataDocument?.data?.Item?.ListThuMucSach.length; index++) {
      const element = dataDocument?.data?.Item?.ListThuMucSach[index];
      if (parentId === element.Id) {
        if (element.IdParent) {
          taoRoutes(element.IdParent);
        }
        setRoutes((prevState) => [...prevState, { breadcrumbName: element.TenThuMuc, path: '' }]);
      }
    }
  };
  const getParent = useCallback(
    (parentId: string) => {
      if (!dataDocument?.data?.Item?.ListThuMucSach) return;
      for (let index = 0; index < dataDocument?.data?.Item?.ListThuMucSach.length; index++) {
        const element = dataDocument?.data?.Item?.ListThuMucSach[index];

        if (parentId === element.Id) {
          return element;
        }
      }
    },
    [dataDocument?.data?.Item?.ListThuMucSach]
  );

  const onPaginate = useCallback(
    (page: number, pageSize: number) => {
      ref.current?.scrollIntoView({ behavior: 'smooth' });
      handleNavigation({ ...queryConfig, page: page + '', pageSize: pageSize + '' });
    },
    [handleNavigation, queryConfig]
  );

  const pagination = useMemo(() => {
    let size = 30;
    let currentPage = 1;

    if (queryConfig.pageSize) {
      size = +queryConfig.pageSize;
    }
    if (queryConfig.page) {
      currentPage = +queryConfig.page;
    }

    return {
      current: currentPage,
      pageSize: size,
      total: count,
      onChange: onPaginate,
      showSizeChanger: false,
      showQuickJumper: true,
      locale: { jump_to: '', page: '' }
    };
  }, [count, onPaginate, queryConfig.page, queryConfig.pageSize]);

  const onSelectChange = (_: Key[], selectedRowValue: DocumentFolder[]) => {
    setSelectedDocumentFolders(selectedRowValue);
  };

  const rowSelections = useMemo(() => {
    return {
      selectedRowKeys: selectedDocumentFolders.map((item) => item.Id),
      preserveSelectedRowKeys: true,
      onChange: onSelectChange
    };
  }, [selectedDocumentFolders]);

  const getType = (type: string) => {
    switch (type) {
      case 'document':
        return 'Sách điện tử';
      case 'image':
        return 'Album ảnh';
      case 'audio':
        return 'Sách nói';
      case 'video':
        return 'Video';
      default:
        return 'Sách in';
    }
  };

  const columns: ColumnsType<DocumentFolder> = [
    {
      title: 'STT',
      dataIndex: 'stt',
      key: 'stt',
      render: (value, record, index) => index + 1,
      width: 100
    },
    {
      title: 'Mã thư mục',
      dataIndex: 'MaThuMuc',
      key: 'MaThuMuc',
      render: (value, record) => (
        <Tooltip placement='topLeft' title={record.MaThuMuc} arrow={true} className='truncate'>
          <p>{record.MaThuMuc}</p>
        </Tooltip>
      ),
      onCell: (record) => ({
        className: 'text-left'
      }),
      ellipsis: true
    },
    {
      title: 'Tên thư mục',
      dataIndex: 'TenThuMuc',
      key: 'TenThuMuc',
      render: (value, record) => (
        <Tooltip placement='topLeft' title={record.TenThuMuc} arrow={true} className='truncate'>
          <p>{record.TenThuMuc}</p>
        </Tooltip>
      ),
      onCell: (record) => ({
        className: 'text-left'
      }),
      ellipsis: true
    },
    {
      title: 'Loại tài liệu',
      dataIndex: 'Type',
      key: 'Type',
      render: (value, record) => (
        <Tooltip placement='topLeft' title={getType(record.Type)} arrow={true} className='truncate'>
          <p>{getType(record.Type)}</p>
        </Tooltip>
      ),
      onCell: (record) => ({
        className: 'text-left'
      }),
      ellipsis: true
    },
    {
      key: 'Trạng thái',
      title: 'Trạng thái',
      render: (value, { IsActive }) =>
        IsActive ? <Tag color='green'>Đang hoạt động</Tag> : <Tag color='red'>Ngưng hoạt động</Tag>
    },
    {
      title: 'Ghi chú',
      dataIndex: 'GhiChu',
      key: 'GhiChu',
      render: (value, record) => (
        <Tooltip placement='topLeft' title={record.GhiChu} arrow={true} className='truncate'>
          <p>{record.GhiChu}</p>
        </Tooltip>
      ),
      onCell: (record) => ({
        className: 'text-left'
      }),
      ellipsis: true
    }
  ];

  if (isAllowedAdjustment) {
    columns.push({
      title: 'Hành động',
      dataIndex: 'actions',
      key: 'actions',
      width: 200,
      render(value, record) {
        return (
          <div className='flex items-center justify-center gap-3 text-primary-10'>
            {/* <Tooltip title='Thêm sách với thư mục tài liệu'>
                <Link to={`/Sach`} preventScrollReset={true} state={{ folder: 'fromDocument' }}>
                  <PlusSquareFilled />
                </Link>
              </Tooltip>

              <Tooltip title='Tìm kiếm sách với thư mục'>
                <Link to={`/Sach?ThuMucSach=${record.Id}`}>
                  <ReconciliationFilled />
                </Link>
              </Tooltip> */}

            <Tooltip title='Thêm thư mục'>
              <button
                onClick={() => {
                  taoRoutes(record.Id);
                  setParent(record);
                  setVisibleInfoModal(true);
                }}
              >
                <PlusSquareFilled />
              </button>
            </Tooltip>

            <Tooltip title='Chỉnh sửa thư mục'>
              <button
                onClick={() => {
                  taoRoutes(record.IdParent);
                  setParent(getParent(record.IdParent));
                  handleEdit(record);
                }}
              >
                <EditOutlined />
              </button>
            </Tooltip>

            {/* <Tooltip title='Chỉnh sửa thư mục'>
                <button onClick={handleEdit(record)}>
                  <EditFilled />
                </button>
              </Tooltip> */}

            <Tooltip title='In sách có trong thư mục'>
              <button onClick={handlePrintDocument(record)}>
                <PrinterFilled />
              </button>
            </Tooltip>

            <Tooltip title='Xóa thư mục'>
              <button onClick={handleOpenDeleteModal(record)}>
                <DeleteFilled style={{ color: 'red' }} />
              </button>
            </Tooltip>
          </div>
        );
      }
    });
  }

  const handlePrintDocument = (documentFolder: DocumentFolder) => () =>
    !isLoadingPrintDocumentFolders && printDocumentFolder([documentFolder]);

  const handleEdit = (documentFolder: DocumentFolder) => {
    setDocumentFolder(documentFolder);
    setVisibleInfoModal(true);
  };

  const handleOpenDeleteModal = (documentFolder: DocumentFolder) => () => {
    setSelectedDocumentFolders([documentFolder]);
    setVisibleDeleteModal(true);
  };

  const handleCloseDeleteModal = () => {
    setSelectedDocumentFolders([]);
    setVisibleDeleteModal(false);
  };

  const handleDeleteDocumentFolder = () => deleteDocumentFolder();

  const handleOpenAddDocumentFolder = () => {
    taoRoutes('');
    setParent(undefined);
    setVisibleInfoModal(true);
  };

  const handleVisibleModal = () => {
    setDocumentFolder(undefined);
    setVisibleInfoModal(false);
  };

  const handleOpenDelMultiModal = () => {
    setVisibleMultiModal(true);
  };

  const handleCloseDeleteMultiModal = () => setVisibleMultiModal(false);

  const handleOnDeleteMultiSuccess = () => setSelectedDocumentFolders([]);

  const handlePrintDocumentFolders = () =>
    !isLoadingPrintDocumentFolders && printDocumentFolder(selectedDocumentFolders);
  return (
    <div className='p-5' ref={ref}>
      <Title title='Thư mục tài liệu' />

      <div className='my-5'>
        <SearchForm placeholder='Nhập tên thư mục tài liệu' />
      </div>

      <div
        className={classNames('', {
          hidden: !isAllowedAdjustment,
          'flex items-center gap-2': isAllowedAdjustment
        })}
      >
        <Button onClick={handleOpenAddDocumentFolder}>Thêm thư mục tài liệu</Button>

        <Button
          variant={!!selectedDocumentFolders.length ? 'default' : 'disabled'}
          disabled={!selectedDocumentFolders.length}
          onClick={handlePrintDocumentFolders}
        >
          In thư mục tài liệu
        </Button>

        <Button
          variant={!!selectedDocumentFolders.length ? 'danger' : 'disabled'}
          disabled={!selectedDocumentFolders.length}
          onClick={handleOpenDelMultiModal}
        >
          Xóa thư mục tài liệu
        </Button>
      </div>

      <div className='mt-4'>
        <Table
          columns={columns}
          rowSelection={rowSelections}
          dataSource={filterList || []}
          rowKey={(record) => record.Id}
          className='custom-table'
          pagination={pagination}
          locale={{
            emptyText: () => <Empty label={locale.emptyText} />
          }}
          loading={isLoadingDocumentFolders}
          bordered
        />
      </div>

      <div
        className={classNames('relative', {
          'mt-0': Number(queryConfig.pageSize) >= count
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger visible={!!filterList.length} value={filterList.length + ''} total={count + ''} />
        </div>
      </div>

      <DocumentFolderModal
        open={visibleInfoModal}
        onBack={handleVisibleModal}
        documentId={documentFolder?.Id}
        parent={parent}
        routes={routes}
      />

      <ModalDelete
        title='Bạn chắc chắn muốn xóa thư mục tài liệu này không?'
        handleOk={handleDeleteDocumentFolder}
        handleCancel={handleCloseDeleteModal}
        open={visibleDeleModal}
        loading={isLoadingDeleteFolder}
      />

      <DeleteMultiModal
        onSuccess={handleOnDeleteMultiSuccess}
        documentFolders={selectedDocumentFolders}
        visible={visibleDelMultiModal}
        onBack={handleCloseDeleteMultiModal}
      />

      <Loading open={isLoadingPrintDocumentFolders} />
    </div>
  );
};

export default DocumentFolderPage;
