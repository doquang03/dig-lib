import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery } from '@tanstack/react-query';
import { publishingCompanyApis } from 'apis';
import { AxiosError } from 'axios';
import { Button, Input, Loading, TextArea, Title } from 'components';
import { path } from 'constants/path';
import { regex } from 'constants/regex';
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { publishingCompanySchema } from 'utils/rules';
import * as yup from 'yup';
type publishingCompanyForm = yup.InferType<typeof publishingCompanySchema>;

const AddPublishingCompanyPage = () => {
  const { publishingCompanyId } = useParams();

  const navigate = useNavigate();

  const [currentSchema, setCurrentSchema] = useState(publishingCompanySchema);
  const [duplicatePublishingCompany, setDuplicatePublishingCompany] = useState<Array<string>>([]);
  const [isTrigger, setIsTrigger] = useState('idle');

  const {
    register,
    handleSubmit,
    trigger,
    setValue,
    setError,
    formState: { errors }
  } = useForm<publishingCompanyForm>({
    defaultValues: {
      name: ''
    },
    resolver: yupResolver(currentSchema)
  });

  const { isFetching } = useQuery({
    queryKey: ['EditByID', publishingCompanyId],
    queryFn: () => publishingCompanyApis.EditByID(publishingCompanyId as string),
    onSuccess: (res) => {
      setValue('name', res?.data?.Item?.Ten ?? '');
      setValue('note', res?.data?.Item?.GhiChu ?? '');
    },
    onError: () => {
      navigate(path.nhaxuatban);
    },
    enabled: !!publishingCompanyId
  });

  const { mutate: handleCreate, isLoading: isLoadingCreate } = useMutation({
    mutationFn: (payload: object) => publishingCompanyApis.Create(payload),
    onSuccess: () => {
      toast.success('Thêm nhà xuất bản thành công');
      navigate(path.nhaxuatban);
    },
    onError: (
      ex: AxiosError<
        ResponseApi<{
          Ten: string;
        }>
      >
    ) => {
      if (ex?.response?.data?.ExMessage === 'Ten') {
        let tempPublishingCompany = duplicatePublishingCompany.concat([ex?.response?.data?.Item?.Ten]);
        setDuplicatePublishingCompany(tempPublishingCompany);
        let publishingCompanySchemaClone = publishingCompanySchema.clone();
        let publishingCompanySchemaObject = yup.object({
          name: yup
            .string()
            .trim()
            .required('Tên nhà xuất bản không được để trống')
            .matches(regex.excludeNumber, 'Tên nhà xuất bản không được nhập chữ số')
            .max(255, 'Tên nhà xuất bản chứa tối đa 255 ký tự')
            .nonNullable()
            .notOneOf(tempPublishingCompany, ex?.response?.data?.Message)
        });
        publishingCompanySchemaClone = publishingCompanySchemaClone.concat(publishingCompanySchemaObject);
        setCurrentSchema(publishingCompanySchemaClone);
        setIsTrigger('name');
        setError('name', { type: 'validate', message: ex.response.data.Message }, { shouldFocus: true });
      }
    }
  });

  const { mutate: handleEdit, isLoading: isLoadingEdit } = useMutation({
    mutationFn: (payload: object) => publishingCompanyApis.EditModel(payload),
    onSuccess: () => {
      toast.success('Cập nhật nhà xuất bản thành công');
    },
    onError: (
      ex: AxiosError<
        ResponseApi<{
          Ten: string;
        }>
      >
    ) => {
      if (ex?.response?.data?.ExMessage === 'Ten') {
        let tempPublishingCompany = duplicatePublishingCompany.concat([ex?.response?.data?.Item?.Ten]);
        setDuplicatePublishingCompany(tempPublishingCompany);
        let publishingCompanySchemaClone = publishingCompanySchema.clone();
        let publishingCompanySchemaObject = yup.object({
          name: yup
            .string()
            .trim()
            .required('Tên nhà xuất bản không được để trống')
            .matches(regex.excludeNumber, 'Tên nhà xuất bản không được nhập chữ số')
            .max(255, 'Tên nhà xuất bản chứa tối đa 255 ký tự')
            .nonNullable()
            .notOneOf(tempPublishingCompany, ex?.response?.data?.Message)
        });
        publishingCompanySchemaClone = publishingCompanySchemaClone.concat(publishingCompanySchemaObject);
        setCurrentSchema(publishingCompanySchemaClone);
        setIsTrigger('name');
      }
    }
  });

  useEffect(() => {
    if (isTrigger === 'name') {
      trigger(isTrigger);
      setIsTrigger('idle');
    }
  }, [isTrigger, trigger]);

  const handleCompelete = handleSubmit((data) => {
    data.name = data.name.replace(/\s+/g, ' ');
    data.note = data?.note?.replace(/\s+/g, ' ');
    setValue('name', data.name);
    if (!publishingCompanyId) {
      handleCreate({ Ten: data.name, GhiChu: data?.note });
    } else {
      if (isFetching) return;
      handleEdit({ Id: publishingCompanyId, Ten: data.name ?? '', GhiChu: data?.note ?? '' });
    }
  });

  function backPublishingCompany() {
    navigate(path.nhaxuatban);
  }
  return (
    <div className='p-5'>
      <Title title={!!publishingCompanyId ? 'CẬP NHẬT NHÀ XUẤT BẢN' : 'THÊM MỚI NHÀ XUẤT BẢN'} />

      <div className='relative my-6'>
        <div>
          <div className='form-group row' style={{ marginBottom: '10px' }}>
            <label
              className='col-form-label col-sm-2 font-bold'
              style={{ marginBottom: '0', paddingTop: '5px', width: '100%' }}
            >
              Tên nhà xuất bản <span style={{ color: 'red' }}>*</span>
            </label>
          </div>
          <div className='form-group row'>
            <div className='component-form-group'>
              <Input
                placeholder='Nhập tên nhà xuất bản'
                className='form-control form-control-sm'
                register={register}
                name='name'
                errorMessage={errors?.name?.message}
                maxLength={256}
              ></Input>
            </div>
          </div>
          <div className='form-group row' style={{ marginBottom: '10px' }}>
            <label
              className='col-form-label col-sm-2 font-bold'
              style={{ marginBottom: '0', paddingTop: '5px', width: '100%' }}
            >
              Ghi chú
            </label>
          </div>
          <div className='form-group row'>
            <div className='component-form-group'>
              <TextArea
                placeholder='Nhập nội dung ghi chú'
                className='form-control form-control-sm'
                register={register}
                name='note'
                errorMessage={errors?.note?.message}
              ></TextArea>
            </div>
          </div>
          <div className='form-group row'>
            <div className='component-form-group flex' style={{ textAlign: 'end' }}>
              <Button className='gray-button' onClick={backPublishingCompany}>
                Quay về
              </Button>
              {!publishingCompanyId ? (
                <Button
                  className='btn btn-primary btn-sm blue-button'
                  onClick={handleCompelete}
                  style={{ marginLeft: '10px' }}
                >
                  Thêm mới
                </Button>
              ) : (
                <Button
                  className='btn btn-primary btn-sm blue-button'
                  onClick={handleCompelete}
                  style={{ marginLeft: '10px' }}
                >
                  Cập nhật
                </Button>
              )}
            </div>
          </div>
        </div>
      </div>
      <Loading open={isLoadingCreate || isLoadingEdit}></Loading>
    </div>
  );
};

export default AddPublishingCompanyPage;
