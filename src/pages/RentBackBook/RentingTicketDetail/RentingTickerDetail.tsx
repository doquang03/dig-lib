import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Table } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { rentBack } from 'apis';
import classNames from 'classnames';
import { Button, DatePicker, Empty, Select, Title } from 'components';
import dayjs, { Dayjs } from 'dayjs';
import { useBookStatuses } from 'hooks';
import { useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { returnExtendSchema, returnSchema } from 'utils/rules';
import { convertDate, getSerialNumber } from 'utils/utils';
import ActionModal from '../components/ActionModal';
import MemberCard from '../components/MemberCard';

const RentingTicketDetail = () => {
  const [{ page, pageSize }, setPaging] = useState<{ page: number; pageSize: number }>({
    page: 1,
    pageSize: 15
  });
  const [action, setAction] = useState<{
    book: StaticRentingBook & { BiaSach: string };
    action: 'return' | 'extend';
  }>();

  const navigate = useNavigate();

  const { id } = useParams();

  const { data: bookStatusData } = useBookStatuses();

  const {
    control: actionsControl,
    formState: { errors: actionsErrors },
    handleSubmit: actionHandleSubmit,
    reset: actionReset,
    setValue: actionSetValue
  } = useForm<{ returnExtendDate: Dayjs | null }>({
    resolver: yupResolver(returnExtendSchema)
  });

  const {
    register: returnRegister,
    formState: { errors: returnErrors },
    handleSubmit: returnHandleSubmit,
    reset: returnReset,
    setValue: setValueReturn
  } = useForm<{ status?: string }>({
    resolver: yupResolver(returnSchema)
  });

  const { data: rentingTicketAdminData } = useQuery({
    queryKey: ['getAdminRentingTicket', id],
    queryFn: () => rentBack.getAdminRentingTicket(id + ''),
    enabled: Boolean(id)
  });

  const {
    data: rentingBooksInTicketData,
    isLoading: loadingRentingBooksInTicket,
    refetch: refetchRentingBooksInTicket
  } = useQuery({
    queryKey: ['getRentingBooksInTicket', id],
    queryFn: () => rentBack.getRentingBooksInTicket(id + ''),
    enabled: Boolean(id)
  });

  const rentingTicketAdmin = rentingTicketAdminData?.data.Item;
  const rentingBooksInTicket = rentingBooksInTicketData?.data.Item;

  const { mutate: returnBook, isLoading: loadingReturnBook } = useMutation({
    mutationFn: rentBack.returnBook,
    onSuccess: () => {
      toast.success('Trả sách thành công');

      setValueReturn('status', '');
      refetchRentingBooksInTicket();
      returnReset();
      setAction(undefined);
    }
  });

  const { mutate: extendRenting, isLoading: loadingExtendRenting } = useMutation({
    mutationFn: rentBack.extendRengting,
    onSuccess: () => {
      toast.success('Gia hạn sách thành công');
      setAction(undefined);
      actionReset();
      refetchRentingBooksInTicket();
      actionSetValue('returnExtendDate', null);
    }
  });

  const statusTicket = useMemo(() => {
    if (!rentingBooksInTicket?.ListThongTinMuonSach.length) return;

    let statusText = '';

    const returnedList = rentingBooksInTicket?.ListThongTinMuonSach.filter(({ DaTra }) => DaTra) || [];

    let isReturnedAll = rentingBooksInTicket?.ListThongTinMuonSach.length - returnedList.length === 0;

    if (!returnedList.length && !isReturnedAll) {
      statusText = 'Chưa trả';
    } else if (!isReturnedAll && returnedList.length) {
      statusText = `Đã trả ${returnedList.length} quyển`;
    } else if (isReturnedAll && returnedList.length) {
      statusText = 'Đã trả';
    }

    return { isReturnedAll, returnedList, statusText };
  }, [rentingBooksInTicket?.ListThongTinMuonSach]);

  const columns: ColumnsType<StaticRentingBook & { BiaSach: string }> = [
    {
      title: 'STT',
      dataIndex: 'stt',
      key: 'stt',
      render: (value, record, index) => {
        return getSerialNumber(page, pageSize, index);
      }
    },
    {
      title: 'Mã cá biệt',
      dataIndex: 'MaCaBiet',
      key: 'MaCaBiet',
      onCell: (record) => ({
        className:
          record?.MaCaBiet?.toLocaleLowerCase().includes('đã xóa') ||
          record?.MaCaBiet?.toLocaleLowerCase().includes('đã thanh lý')
            ? 'text-danger-10'
            : ''
      })
    },
    {
      title: 'Tên sách',
      dataIndex: 'TenSach',
      key: 'TenSach',
      onCell: (record) => ({
        className:
          record.TenSach?.toLocaleLowerCase().includes('đã xóa') ||
          record.TenSach?.toLocaleLowerCase().includes('đã thanh lý')
            ? 'text-danger-10'
            : ''
      }),
      render: (value, { TenSach, idSach }) => {
        return !TenSach.includes('"sách đã bị xóa"') ? (
          <Link to={`/Sach/CapNhat/${idSach}`} className='cursor-pointer text-primary-50 underline'>
            {TenSach}
          </Link>
        ) : (
          TenSach
        );
      }
    },
    {
      title: 'Bìa sách',
      dataIndex: 'BiaSach',
      key: 'BiaSach',
      render: (value, { TenSach, idSach, BiaSach }) => (
        <center>
          <img src={BiaSach || '/content/book.png'} alt={TenSach} width={70} height={40} />
        </center>
      )
    },
    {
      title: 'Ngày mượn',
      dataIndex: 'NgayMuonTemp',
      key: 'NgayMuonTemp'
    },
    {
      title: 'Ngày trả',
      dataIndex: 'NgayTraThucTeTemp',
      key: 'NgayTraThucTeTemp',
      render: (value, record) => {
        return (
          <>
            {' '}
            {record.DaTra
              ? `${new Date(record.NgayTraThucTe).getHours()}:${(
                  '0' + new Date(record.NgayTraThucTe).getMinutes()
                ).slice(-2)} - ${convertDate(record.NgayTraThucTe)} `
              : record.NgayTraThucTeTemp}
          </>
        );
      }
    },
    {
      title: 'Tình trạng',
      dataIndex: 'TenTrangThai',
      key: 'TenTrangThai',
      onCell: (record) => {
        switch (record.TrangThaiString) {
          case 'ChuaTra':
            return { className: 'bg-red-300' };

          case 'TraTre':
            return { className: 'bg-tertiary-60/50' };

          case 'TraDungHen':
            return { className: 'bg-tertiary-30/50' };

          case 'GanTra':
            return { className: 'bg-primary-60/50' };

          case 'MatSach':
            return { className: 'bg-secondary-30/50' };

          default:
            return { className: 'none' };
        }
      }
    },
    {
      title: 'Hành động',
      dataIndex: 'acions',
      key: 'acions',
      render: (value, record) => (
        <div className='flex items-center justify-center gap-2'>
          {record.TrangThaiString !== 'MatSach' && record.TrangThaiString !== 'TraDungHen' && (
            <Button
              className='bg-tertiary-50 hover:bg-tertiary-50/75'
              onClick={() => setAction({ book: record, action: 'return' })}
            >
              <span className='text-tertiary-50/100'>Trả sách</span>
            </Button>
          )}

          <Button
            className={classNames('bg-tertiary-40 hover:bg-tertiary-40/75', {
              hidden:
                record.TrangThaiString === 'ChuaTra' ||
                record.TrangThaiString === 'MatSach' ||
                record.TrangThaiString === 'TraDungHen'
            })}
            onClick={() => {
              actionSetValue('returnExtendDate', dayjs(record.NgayPhaiTra).add(1, 'day'));
              setAction({ book: record, action: 'extend' });
            }}
          >
            <span className='text-tertiary-40/100'>Gia hạn</span>
          </Button>
        </div>
      )
    }
  ];

  const handleExtend = actionHandleSubmit((data) => {
    data.returnExtendDate &&
      extendRenting([{ Id: action?.book?.Id + '', NgayTraNew: dayjs(data.returnExtendDate).format('DD/MM/YYYY') }]);
  });

  const handleReturn = returnHandleSubmit((data) => {
    data.status &&
      returnBook([{ Id: action?.book?.Id + '', IdTrangThai: data.status, IdUser: rentingBooksInTicket?.Id + '' }]);
  });

  const handleBack = () => {
    setAction(undefined);
    actionReset();
    setValueReturn('status', '');
  };

  return (
    <div className='my-3'>
      <Title title='Chi tiết phiếu mượn sách' />

      <div className='mt-3 flex gap-3'>
        {rentingBooksInTicket && (
          <MemberCard
            //@ts-ignore
            member={{
              Id: rentingBooksInTicket.Id,
              ChucVu: rentingBooksInTicket?.ToLop,
              LopHoc: rentingBooksInTicket?.ToLop,
              Ten: rentingBooksInTicket?.TenThanhVien,
              LoaiTK: rentingBooksInTicket?.LoaiTK,
              MaSoThanhVien: rentingBooksInTicket?.MaSoThanhVien,
              NgaySinh: rentingBooksInTicket?.NgaySinh
            }}
            onCloseTab={() => {}}
            isLeft={false}
          />
        )}

        <div className='flex w-1/3 flex-col justify-around gap-3'>
          <h3 className='m-0 border-b-[1px] border-b-primary-10 pb-1 text-primary-10'>Thông tin phiếu mượn sách</h3>

          <div className='flex w-1/2 items-center justify-between'>
            <p>Mã phiếu mượn:</p>
            <span className='font-bold text-primary-10'>{rentingTicketAdmin?.MaPhieu}</span>{' '}
          </div>

          <div className='flex w-1/2 items-center justify-between'>
            <p>Thời gian mượn:</p>
            <span className='font-bold'>{dayjs(rentingTicketAdmin?.ThoiGian).format('HH:mm DD/MM/YYYY')}</span>{' '}
          </div>

          <div className='flex w-1/2 items-center justify-between'>
            <p>Số lượng sách:</p>
            <span className='font-bold'>{rentingBooksInTicket?.ListThongTinMuonSach.length}</span>{' '}
          </div>

          <div className='flex w-1/2 items-center justify-between'>
            <p>Người tạo:</p>
            <span className='font-bold'>{rentingTicketAdmin?.UserNameAdmin}</span>{' '}
          </div>

          <div className='flex w-1/2 items-center justify-between'>
            <p>Trạng thái: </p>
            <span
              className={classNames('rounded-full border p-2 shadow-md ', {
                'border-tertiary-70 bg-tertiary-70/20 text-tertiary-70 shadow-tertiary-70/40':
                  statusTicket?.statusText.includes('quyển'),
                'border-danger-10 bg-danger-10/20 text-danger-10 shadow-danger-10/40':
                  statusTicket?.statusText === 'Chưa trả',
                'border-tertiary-30 bg-tertiary-30/20 text-tertiary-30 shadow-tertiary-30/40':
                  statusTicket?.statusText === 'Đã trả'
              })}
            >
              {statusTicket?.statusText}
            </span>{' '}
          </div>
        </div>
      </div>

      <h3 className='text-primary-10'>Dach sách đang mượn</h3>

      <Table
        className='custom-table'
        columns={columns}
        bordered
        loading={loadingRentingBooksInTicket}
        dataSource={rentingBooksInTicket?.ListThongTinMuonSach}
        rowKey={(row) => row.Id}
        locale={{
          emptyText: () => <Empty />
        }}
        pagination={{
          onChange(current, pageSize) {
            setPaging({
              page: current,
              pageSize
            });
          },
          defaultPageSize: pageSize,
          hideOnSinglePage: true,
          showSizeChanger: false,
          total: rentingBooksInTicket?.ListThongTinMuonSach.length,
          showQuickJumper: true,
          locale: { jump_to: '', page: '' }
        }}
      />

      <div className='mt-3 flex justify-end'>
        <Button onClick={() => navigate(-1)}>Quay về</Button>
      </div>

      <ActionModal
        title='Chọn Tình trạng sách'
        open={action?.action === 'return'}
        onSubmit={handleReturn}
        onBack={handleBack}
        labelSubmit='Trả sách'
        loading={loadingReturnBook}
      >
        <Select
          items={[{ label: 'Chọn tình trạng sách', value: '' }, ...bookStatusData]}
          className='w-full'
          register={returnRegister}
          name='status'
          errorMessage={returnErrors.status?.message}
          disabled={loadingReturnBook}
        />
      </ActionModal>

      <ActionModal
        title='Chọn ngày gia hạn'
        open={action?.action === 'extend'}
        onSubmit={handleExtend}
        onBack={handleBack}
        loading={loadingExtendRenting}
      >
        <DatePicker
          control={actionsControl}
          name='returnExtendDate'
          errorMessage={actionsErrors?.returnExtendDate?.message}
          disabledDate={(current) => {
            return current && current <= dayjs().endOf('day');
          }}
          placeholder='Chọn ngày trả'
          disabled={loadingExtendRenting}
          defaultValue={dayjs(action?.book.NgayTraThucTe).add(1, 'day')}
        />
      </ActionModal>
    </div>
  );
};

export default RentingTicketDetail;
