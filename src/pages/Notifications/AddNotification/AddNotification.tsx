import { CloseSquareFilled } from '@ant-design/icons';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import { useMutation, useQuery } from '@tanstack/react-query';
import { DatePicker } from 'antd';
import locale from 'antd/es/date-picker/locale/vi_VN';
import notificationApis from 'apis/notification.api';
import CustomEditorBuild from 'ckeditor5-custom-build';
import { Button, Input, Loading, Title } from 'components';
import 'css/SettingPage.css';
import dayjs, { Dayjs } from 'dayjs';
import { ChangeEvent, useMemo, useRef, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';

function addOneHour(date = new Date()) {
  date.setHours(date.getHours() + 1);

  return date;
}

const AddNotification = () => {
  const [file, setFile] = useState<File | undefined>();
  const [formValues, setFormValues] = useState<{
    thumnail?: string;
    title: string;
    permission: string;
    content: string;
    publishMode: string;
    publishTime: string;
  }>({
    content: '',
    permission: 'all',
    title: '',
    publishMode: 'instance',
    publishTime: addOneHour().toISOString(),
    thumnail: ''
  });
  const [error, setError] = useState<boolean>(false);

  const fileInutRef = useRef<HTMLInputElement | null>(null);

  const { id } = useParams();

  const navigate = useNavigate();

  const { mutate, isLoading } = useMutation({
    mutationFn: id ? notificationApis.updateNewNotification : notificationApis.addNewNotification,
    onSuccess: () => {
      if (id) {
        return toast.success('Cập nhật thông báo thành công');
      }

      toast.success('Tạo thông báo thành công');

      navigate(-1);
    }
  });

  const { isLoading: loadingNotification } = useQuery({
    queryKey: ['getNotification', id],
    queryFn: () => notificationApis.getNotification(id || ''),
    enabled: !!id,
    onSuccess: (data) => {
      const { HinhNen, IsAll, NoiDung, ThoiGian, TieuDe } = data.data.Item;

      setFormValues({
        content: NoiDung,
        title: TieuDe,
        permission: IsAll ? 'all' : 'internal',
        publishMode: new Date(ThoiGian) < new Date() ? 'instance' : 'alarm',
        thumnail: HinhNen,
        publishTime: ThoiGian
      });

      setError(false);
    }
  });

  const previewImage = useMemo(() => (file ? URL.createObjectURL(file) : ''), [file]);

  const handleAccessFileInputRef = () => {
    fileInutRef.current?.click();
  };

  const handleOnFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files?.[0];

    if (!fileFromLocal) {
      return;
    }

    setError(false);

    setFile(fileFromLocal);
  };

  const handleRemovePreviewImage = () => () => {
    setFile(undefined);
  };

  const handleAddNoti = () => {
    if (!formValues.content || !formValues.title || (!file && !formValues.thumnail)) {
      setError(true);

      return;
    }

    if (dayjs(formValues.publishTime) < dayjs()) {
      toast.warning('Không được chọn thời điểm đăng trong quá khứ');

      return;
    }

    const formData = new FormData();

    file && formData.append('HinhNen', file);
    id && formData.append('Id', id);
    formData.append(`TieuDe`, formValues.title.replace(/\s\s+/g, ' '));
    formData.append(`IsAll`, (formValues.permission === 'all') + '');
    formData.append(`IsActive`, 'true');
    formData.append(`NoiDung`, formValues.content);
    formData.append(
      `ThoiGian`,
      formValues.publishMode === 'instance' ? new Date().toISOString() : formValues?.publishTime
    );

    mutate(formData);
  };

  const handleChangePermission = (e: ChangeEvent<HTMLInputElement>) => {
    if (!e) return;

    const { value } = e.target;

    setFormValues((prevState) => {
      return {
        ...prevState,
        permission: value
      };
    });
  };

  const handleChangePublishMode = (e: ChangeEvent<HTMLInputElement>) => {
    if (!e) return;

    const { value } = e.target;

    setFormValues((prevState) => {
      return {
        ...prevState,
        publishMode: value
      };
    });
  };

  const handleChangeTitle = (e: ChangeEvent<HTMLInputElement>) => {
    if (!e) return;

    const { value } = e.target;

    setFormValues((prevState) => {
      return {
        ...prevState,
        title: value.replace(/\s+/g, ' ').trimStart()
      };
    });
  };

  const handleChangeAlarm = (e: Dayjs | null) => {
    if (!e) return;

    if (dayjs(e) < dayjs()) {
      toast.warning('Không được chọn thời điểm đăng trong quá khứ');
      return;
    }

    setFormValues((prevState) => {
      return {
        ...prevState,
        publishTime: dayjs(e).toISOString()
      };
    });
  };

  return (
    <>
      <Title title={!!id ? 'Cập nhật THÔNG BÁO' : 'TẠO THÔNG BÁO MỚI'} />

      <div className='flex gap-5'>
        <div className='flex-1'>
          <p className='font-bold'>
            Hình nền thông báo <span className='text-danger-10'>*</span>{' '}
          </p>

          <input
            type='file'
            accept='image/jpeg,image/gif,image/jpg,image/png,image/bmp'
            className='hidden'
            ref={fileInutRef}
            onChange={handleOnFileChange}
            // @ts-ignore
            onClick={(e: MouseEvent<HTMLInputElement>) => (e.target.value = null)}
          />

          {error && (!file || (!formValues.thumnail && id)) && (
            <p className='text-danger-10'>Hình nền thông báo không được để trống</p>
          )}

          <Button variant='primary-outline' onClick={handleAccessFileInputRef} className='mt-2'>
            <svg width={15} height={15} viewBox='0 0 15 15' fill='none' xmlns='http://www.w3.org/2000/svg'>
              <path
                d='M14.5 9.70588H10.5628V15H4.65711V9.70588H0.719971L7.60997 3.52941L14.5 9.70588ZM0.719971 1.76471V0H14.5V1.76471H0.719971Z'
                fill='#17a2b8'
              />
            </svg>
            Chọn tệp
          </Button>

          {(Boolean(file) || formValues.thumnail) && (
            <div className='relative mt-2 h-[200px] w-[200px]'>
              <button className='absolute right-0 -top-1 shadow-lg' type='button' onClick={handleRemovePreviewImage()}>
                <CloseSquareFilled style={{ color: 'red' }} />
              </button>

              <a href={previewImage} target='_blank' rel='noreferrer'>
                <img
                  src={previewImage || formValues.thumnail}
                  alt='Hình ảnh nội dung'
                  className='h-full w-full bg-contain bg-center shadow-md'
                />
              </a>
            </div>
          )}

          <p className='mt-4 mb-1 font-bold'>
            Tiêu đề <span className='text-danger-10'>*</span>{' '}
          </p>

          <Input
            placeholder='Nhập tiêu đề thông báo'
            onChange={handleChangeTitle}
            errorMessage={error && !Boolean(formValues.title.trim()) ? 'Tiêu đề thông báo không được để trống' : ''}
            value={formValues.title}
            maxLength={100}
          />
        </div>

        <div className='flex flex-1 flex-col space-y-3'>
          <p className='font-bold'>
            Quyền xem <span className='text-danger-10'>*</span>{' '}
          </p>

          <label className='flex items-center gap-2 hover:cursor-pointer'>
            <input
              type='radio'
              value='all'
              onChange={handleChangePermission}
              checked={formValues.permission === 'all'}
            />
            Thông báo đến tất cả bạn đọc
          </label>

          <label className='flex items-center gap-2 hover:cursor-pointer'>
            <input
              type='radio'
              value='internal'
              onChange={handleChangePermission}
              checked={formValues.permission === 'internal'}
            />
            Chỉ thông báo đến bạn đọc nội bộ
          </label>
        </div>
      </div>

      <p className='mt-4 mb-2 font-bold'>
        Nội dung thông báo <span className='text-danger-10'>*</span>{' '}
      </p>

      <CKEditor
        editor={CustomEditorBuild}
        data={formValues.content}
        onChange={(_, editor) =>
          setFormValues((prevState) => {
            return {
              ...prevState,
              content: editor?.getData()
            };
          })
        }
      />

      {error && !formValues.content && <p className='text-danger-10'>Nội dung thông báo không được để trống</p>}

      <div className={`mb-3 flex items-center justify-between`}>
        <div className='mt-3 flex flex-1 flex-col space-y-3'>
          <p className='font-bold'>
            Thời gian thông báo <span className='text-danger-10'>*</span>{' '}
          </p>

          <label className='flex items-center gap-2 hover:cursor-pointer'>
            <input
              type='radio'
              value='instance'
              onChange={handleChangePublishMode}
              checked={formValues.publishMode === 'instance'}
            />
            Đăng ngay lập tức
          </label>

          <div className='flex items-center gap-2'>
            <label className='flex items-center gap-2 hover:cursor-pointer'>
              <input
                type='radio'
                value='alarm'
                onChange={handleChangePublishMode}
                checked={formValues.publishMode === 'alarm'}
              />
              Tùy chỉnh thời gian đăng
            </label>

            {formValues.publishMode === 'alarm' && (
              <DatePicker
                placeholder='Chọn thời gian đăng'
                locale={locale}
                showTime
                onChange={handleChangeAlarm}
                format={'HH:mm:ss DD/MM/YYYY'}
                value={dayjs(formValues.publishTime)}
                disabledDate={(current) => {
                  return current && current < dayjs().startOf('day');
                }}
              />
            )}
          </div>
        </div>

        <div className='flex items-center gap-1'>
          <Button variant='secondary' onClick={() => navigate(-1)}>
            Quay về
          </Button>

          <Button onClick={handleAddNoti}>{!!id ? 'Cập nhật' : 'Thêm mới'}</Button>
        </div>
      </div>

      <Loading open={isLoading || (loadingNotification && !!id)} />
    </>
  );
};

export default AddNotification;
