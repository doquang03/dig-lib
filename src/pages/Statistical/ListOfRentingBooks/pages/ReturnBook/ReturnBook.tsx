import { useMutation, useQuery } from '@tanstack/react-query';
import { DatePicker, Table } from 'antd';
import { ColumnsType, TableProps } from 'antd/es/table';
import { rentBack, statistic } from 'apis';
import classNames from 'classnames';
import { Button, Empty, Input, Loading, Select, SizeChanger, Title } from 'components';
import { path } from 'constants/path';
import dayjs from 'dayjs';
import { useBookStatuses, useStoresOptions } from 'hooks';
import { ChangeEvent, Key, useMemo, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getSerialNumber } from 'utils/utils';

const { RangePicker } = DatePicker;

type RangeValue = Parameters<NonNullable<React.ComponentProps<typeof DatePicker.RangePicker>['onChange']>>[0];

const initialOrderBy = {
  MaCaBiet: true,
  TenSach: true,
  NguoiMuon: true,
  NgayMuonTemp: true,
  NgayTraTemp: true
};

const ReturnBook = () => {
  const [selectedBooks, setSelectedBooks] = useState<Array<StaticRentingBook & { IdTrangThai?: string }>>([]);

  const [orderBy, setOrderBy] = useState<
    Partial<{
      MaCaBiet: boolean;
      TenSach: boolean;
      NguoiMuon: boolean;
      NgayMuonTemp: boolean;
      NgayTraTemp: boolean;
    }>
  >(initialOrderBy);

  const [searchForm, setSearchForm] = useState<
    { page: number; pageSize: number } & Partial<{
      NgayMuonMin: string;
      NgayMuonMax: string;
      NgayPhaiTraMin: string;
      NgayPhaiTraMax: string;
      IdTinhTrang: string;
      TextForReturn: string;
    }>
  >({ page: 1, pageSize: 30, TextForReturn: '' });

  const navigate = useNavigate();

  const { data: storeOptions, isLoading: loadingStoresOptions } = useStoresOptions();

  const { data: bookStatuses, isLoading: loadingBookStatuses } = useBookStatuses();

  const {
    data: danhMucMuonSachData,
    isLoading: loadingDanhMucMuonSach,
    refetch: reftchListOFRetingBooks
  } = useQuery({
    queryKey: ['danhMucMuonSach', orderBy, searchForm],
    queryFn: () => {
      let bodyReq: Partial<{
        page: number;
        pageSize: number;
        TextForReturn: string;
        NgayMuon: SearchRangeField;
        NgayPhaiTra: SearchRangeField;
        IdTinhTrang: SearchInputField;
      }> = {};

      if (searchForm.TextForReturn) {
        bodyReq.TextForReturn = searchForm.TextForReturn;
      }

      if (searchForm?.NgayMuonMin && searchForm?.NgayMuonMax) {
        bodyReq.NgayMuon = {
          Min: searchForm?.NgayMuonMin,
          Max: searchForm?.NgayMuonMax
        };
      }

      if (searchForm?.NgayPhaiTraMin && searchForm?.NgayPhaiTraMax) {
        bodyReq.NgayPhaiTra = {
          Min: searchForm?.NgayPhaiTraMin,
          Max: searchForm?.NgayPhaiTraMax
        };
      }

      if (searchForm?.IdTinhTrang) {
        bodyReq.IdTinhTrang = {
          truongSearch: searchForm?.IdTinhTrang,
          operatorSearch: 'equals'
        };
      }

      return statistic.getDanhMucMuonSach({
        page: searchForm.page,
        pageSize: searchForm.pageSize,
        ListTinhTrang: [1, 2],
        SortMaKSCB: orderBy.MaCaBiet,
        SortTenSach: orderBy.TenSach,
        SortNguoiMuon: orderBy.NguoiMuon,
        SortNgayMuon: orderBy.NgayMuonTemp,
        SortNgayPhaiTra: orderBy.NgayTraTemp,
        ...bodyReq
      });
    }
  });

  const danhMucMuonSach = useMemo(() => {
    if (!danhMucMuonSachData?.data.Item) return;

    const { SoSachDuocMuon = 0, SoNguoiMuonSach = 0, ListRutGon = [], count } = danhMucMuonSachData?.data.Item;

    return {
      total: count,
      danhMucSachArray: ListRutGon,
      soNguoiMuon: SoNguoiMuonSach
    };
  }, [danhMucMuonSachData?.data.Item]);

  const { mutate: returnBook, isLoading: loadingReturnBook } = useMutation({
    mutationFn: rentBack.returnBook,
    onSuccess: () => {
      toast.success('Trả sách thành công');
      reftchListOFRetingBooks();

      setSelectedBooks([]);
    }
  });

  const handleFilterByStatus = (e: ChangeEvent<HTMLSelectElement>) => {
    setSearchForm((prevState) => {
      return {
        ...prevState,
        IdTinhTrang: e.target.value,
        page: 1,
        pageSize: 30
      };
    });
  };

  const columns: ColumnsType<StaticRentingBook> = [
    {
      title: 'STT',
      dataIndex: 'stt',
      key: 'stt',
      render: (value, record, index) => {
        return getSerialNumber(searchForm.page, searchForm.pageSize, index);
      }
    },
    {
      title: 'Mã cá biệt',
      dataIndex: 'MaCaBiet',
      key: 'MaCaBiet',
      onCell: (record) => ({
        className:
          record.MaCaBiet?.toLocaleLowerCase().includes('đã xóa') ||
          record.MaCaBiet?.toLocaleLowerCase().includes('đã thanh lý')
            ? 'text-danger-10'
            : ''
      }),
      sorter: true,
      sortDirections: ['descend', 'ascend']
    },
    {
      title: 'Tên sách',
      dataIndex: 'TenSach',
      key: 'TenSach',
      sorter: true,
      sortDirections: ['descend', 'ascend']
    },
    {
      title: 'Người mượn',
      dataIndex: 'TenThanhVien',
      key: 'TenThanhVien',
      sorter: true,
      sortDirections: ['descend', 'ascend']
    },
    {
      title: 'Ngày mượn',
      dataIndex: 'NgayMuonTemp',
      key: 'NgayMuonTemp',
      sorter: true,
      sortDirections: ['descend', 'ascend']
    },
    {
      title: 'Ngày phải trả',
      dataIndex: 'NgayTraTemp',
      key: 'NgayTraTemp',
      sorter: true,
      sortDirections: ['descend', 'ascend']
    },
    {
      title: 'Tình trạng',
      dataIndex: 'TrangThaiSach',
      key: 'TrangThaiSach',
      sortDirections: ['descend', 'ascend']
    },
    {
      title: 'Tình trạng trả sách',
      dataIndex: 'actions',
      key: 'actions',
      render(value, record) {
        const isSelected = selectedBooks.findIndex((book) => book.Id === record.Id);

        const handleSetIdTrangThai = (value: string, id: string) => {
          setSelectedBooks(
            selectedBooks.map((book) => {
              if (book.Id === id) {
                return {
                  ...book,
                  IdTrangThai: value
                };
              }

              return book;
            })
          );
        };

        return (
          <div className='flex items-center justify-center gap-3 text-primary-10'>
            {isSelected !== -1 && (
              <Select items={bookStatuses} onChange={(e) => handleSetIdTrangThai(e.target.value, record.Id)} />
            )}
          </div>
        );
      }
    }
  ];

  const onChange: TableProps<StaticRentingBook>['onChange'] = (pagination, filters, sorter, extra) => {
    setOrderBy(
      // @ts-ignore
      { [sorter.field]: sorter.order === 'ascend' }
    );
  };

  const onSelectChange = (_: Key[], selectedRowValue: StaticRentingBook[]) => {
    setSelectedBooks(
      selectedRowValue.map((row) => {
        return {
          ...row,
          IdTrangThai: bookStatuses[0].value
        };
      })
    );
  };

  const rowSelections = {
    selectedRowKeys: selectedBooks.map((item) => item.Id),
    preserveSelectedRowKeys: true,
    onChange: onSelectChange
  };

  const onPaginate = (page: number, pageSize: number) => {
    setSearchForm((prevState) => {
      return {
        ...prevState,
        page,
        pageSize
      };
    });
  };

  const pagination = useMemo(() => {
    let size = 30;
    let currentPage = 1;

    if (searchForm.pageSize) {
      size = searchForm.pageSize;
    }
    if (currentPage) {
      currentPage = searchForm.page;
    }

    return {
      current: currentPage,
      pageSize: size,
      total: danhMucMuonSach?.total,
      onChange: onPaginate,
      showSizeChanger: false,
      showQuickJumper: true,
      locale: { jump_to: '', page: '' }
    };
  }, [searchForm.page, searchForm.pageSize, danhMucMuonSach?.total]);

  const handleRefresh = () => {
    setSelectedBooks([]);

    setSearchForm({ page: 1, pageSize: 30 });
  };

  const handleReturnSelectedBooks = () => {
    const returnBooksParams = selectedBooks.map((book) => {
      return {
        Id: book.Id,
        IdUser: book.idUser,
        IdTrangThai: book.IdTrangThai || ''
      };
    });

    returnBook(returnBooksParams);
  };

  const handleSearch = (e: ChangeEvent<HTMLInputElement>) => {
    if (!e) return;

    const { value } = e.target;

    setSearchForm((prevState) => {
      return {
        ...prevState,
        TextForReturn: value,
        page: 1,
        pageSize: 30
      };
    });
  };

  const handleChangeRentingDate = (values: RangeValue) => {
    setSearchForm((prevState) => {
      return {
        ...prevState,
        NgayMuonMin: values?.[0]?.startOf('day').toISOString() as string,
        NgayMuonMax: values?.[1]?.endOf('day').toISOString() as string,
        page: 1,
        pageSize: 30
      };
    });
  };

  const handleChangeReturningDate = (values: RangeValue) => {
    setSearchForm((prevState) => {
      return {
        ...prevState,
        NgayPhaiTraMin: values?.[0]?.startOf('day').toISOString() as string,
        NgayPhaiTraMax: values?.[1]?.endOf('day').toISOString() as string,
        page: 1,
        pageSize: 30
      };
    });
  };

  return (
    <div className='p-5'>
      <Title title='Chọn sách cần trả' />

      <form className='mt-3 flex flex-1 items-center gap-1 bg-primary-50/20 p-2.5'>
        <Input
          placeholder='Nhập mã KSCB, tên người mượn, tên sách'
          onChange={handleSearch}
          value={searchForm?.TextForReturn}
          containerClassName='w-1/4'
        />

        <RangePicker
          className='w-1/5 border border-gray-300 py-[0.8rem] px-2 outline-none'
          format={['DD/MM/YYYY', 'DD-MM-YYYY']}
          placeholder={['Ngày bắt đầu mượn', 'Ngày kết thúc mượn']}
          disabledDate={(current) => {
            return current && current > dayjs().endOf('day');
          }}
          onChange={handleChangeRentingDate}
          name='NgayMuon'
          value={[
            searchForm.NgayMuonMin ? dayjs(searchForm.NgayMuonMin) : null,
            searchForm.NgayMuonMax ? dayjs(searchForm.NgayMuonMax) : null
          ]}
        />

        <RangePicker
          className='w-1/5 border border-gray-300 py-[0.8rem] px-2 outline-none'
          format={['DD/MM/YYYY', 'DD-MM-YYYY']}
          placeholder={['Ngày bắt đầu trả', 'Ngày kết thúc trả']}
          onChange={handleChangeReturningDate}
          name='NgayPhaiTra'
          value={[
            searchForm.NgayPhaiTraMin ? dayjs(searchForm.NgayPhaiTraMin) : null,
            searchForm.NgayPhaiTraMax ? dayjs(searchForm.NgayPhaiTraMax) : null
          ]}
        />

        <Select
          name='IdTinhTrang'
          items={[{ label: 'Tất cả tình trạng', value: '' }, ...bookStatuses]}
          onChange={(e) => handleFilterByStatus(e)}
          disabled={loadingBookStatuses}
          value={searchForm?.IdTinhTrang}
        />

        <Button variant='secondary' onClick={handleRefresh}>
          Làm mới
        </Button>
      </form>
      <div className='mt-3 flex items-center justify-between'>
        <p className='font-semibold text-tertiary-30'>Tổng số sách đã chọn để trả: {selectedBooks.length}</p>
      </div>

      <div className='reltive mt-3'>
        <Table
          columns={columns}
          rowSelection={rowSelections}
          dataSource={danhMucMuonSach?.danhMucSachArray || []}
          rowKey={(record) => record.Id}
          className='custom-table'
          pagination={pagination}
          locale={{
            emptyText: () => <Empty />,
            triggerDesc: 'Click để sắp xếp tăng dần',
            triggerAsc: 'Click để sắp xếp giảm dần',
            cancelSort: 'Click để hủy sắp xếp'
          }}
          loading={loadingDanhMucMuonSach}
          bordered
          onChange={onChange}
        />
      </div>

      {!!danhMucMuonSach?.total && (
        <div
          className={classNames('relative', {
            'mt-[0px]': searchForm.pageSize >= danhMucMuonSach?.total
          })}
        >
          <div className='absolute bottom-1'>
            <SizeChanger
              visible={!!danhMucMuonSach?.total}
              value={searchForm.pageSize + ''}
              currentPage={searchForm.page.toString()}
              total={danhMucMuonSach?.total + ''}
              onChange={(pageSize) => {
                setSearchForm((prevState) => {
                  return {
                    ...prevState,
                    page: 1,
                    pageSize: +pageSize
                  };
                });
              }}
            />
          </div>
        </div>
      )}

      <div className='mt-3 flex items-center justify-end gap-2'>
        <Button
          variant='secondary'
          onClick={() => navigate(path.thongkemuonsach)}
          loading={loadingReturnBook}
          disabled={loadingReturnBook}
        >
          Quay về
        </Button>

        <Button
          variant='primary'
          disabled={!selectedBooks.length || loadingReturnBook}
          onClick={handleReturnSelectedBooks}
          loading={loadingReturnBook}
        >
          Trả sách
        </Button>
      </div>
      <Loading open={loadingBookStatuses} />
    </div>
  );
};

export default ReturnBook;
