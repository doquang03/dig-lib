import { InfoCircleOutlined } from '@ant-design/icons';
import classNames from 'classnames';
import { ModalDelete } from 'components';

type Props = {
  students?: Student[];
  onCancel: () => void;
  onOk: () => void;
  loading: boolean;
  visible: boolean;
};

const DeleteStudentsModal = (props: Props) => {
  const { students, onCancel, onOk, loading, visible } = props;

  return (
    <ModalDelete
      open={Boolean(students?.length) && visible}
      closable={false}
      title={
        <>
          <h3 className='text-bold'>Bạn có muốn chắc chắn xóa những học sinh này không?</h3>
          <p className='text-[16px] font-extralight leading-normal'>
            Bạn sẽ không thể khôi phục sau khi xóa, những học sinh đang mượn sách sẽ không thể xóa.
          </p>
        </>
      }
      handleCancel={onCancel}
      handleOk={onOk}
      loading={loading}
    >
      <div className='max-h-[300px] overflow-x-auto'>
        {students?.map(({ MaSoThanhVien, Ten, TrangThai, DaTra }, index) => {
          const inactive = TrangThai === 1 || !DaTra;

          return (
            <div key={MaSoThanhVien} className='grid grid-cols-12 items-center border-b-2 px-2 py-2 font-bold'>
              <div className='col-span-1'>
                <p className={inactive ? 'text-danger-10' : ''}>{index + 1} </p>
              </div>
              <div className='col-span-5'>
                <p
                  className={classNames('truncate text-left', {
                    'text-danger-10': inactive
                  })}
                >
                  {Ten}
                </p>
              </div>
              <div className='col-span-6 grid grid-cols-12'>
                <div className='col-span-6'>
                  <p
                    className={classNames('mr-4', {
                      'text-danger-10': inactive
                    })}
                  >
                    {MaSoThanhVien}
                  </p>
                </div>
                <div className='col-span-6'>
                  {inactive && (
                    <p className='flex items-center gap-1 font-light text-danger-10'>
                      <InfoCircleOutlined style={{ color: 'red' }} />

                      <span className='text-[12px]'>
                        {TrangThai === 1 && 'Đang bị khóa'}
                        {'\n'}
                        {!DaTra && 'Chưa trả sách'}
                      </span>
                    </p>
                  )}
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </ModalDelete>
  );
};

export default DeleteStudentsModal;
