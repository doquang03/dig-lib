import { Modal, ModalProps } from 'antd';
import { Button } from 'components';

type Props = {
  books: Book[];
} & ModalProps;

const FailureConvertationModal = (props: Props) => {
  const { books, onCancel, ...rest } = props;

  return (
    <Modal centered footer={null} closable={false} {...rest} width={700}>
      <h1 className='my-3 text-[20px] text-primary-10'>Danh sách tài liệu không thể chuyển đổi</h1>

      <div className='my-2 max-h-[40rem] w-full overflow-x-auto overflow-y-auto'>
        <table width={'100%'} className='table-striped table-bordered table w-full whitespace-normal'>
          <thead>
            <tr>
              <th>STT</th>
              <th>Sách</th>
              <th>Lý do</th>
            </tr>
          </thead>

          <tbody>
            {books.map((book, index) => (
              <tr key={book.Id}>
                <td>{index + 1}</td>
                <td>
                  {book.MaKiemSoat} - {book.TenSach}
                </td>
                <td>{book.IsDigitalResource ? 'Đã được chuyển đổi' : 'Nội dung sách không tồn tại'}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>

      <div className='grid place-items-center'>
        <Button onClick={onCancel} className=''>
          Đóng
        </Button>
      </div>
    </Modal>
  );
};

export default FailureConvertationModal;
