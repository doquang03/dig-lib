import { CSSProperties } from 'react';
import classnames from 'classnames';
type Props = {
  title: string;
  className?: string;
  style?: CSSProperties;
};

const Title = (props: Props) => {
  const { title, className, style } = props;

  return (
    <div
      className={classnames(
        'w-full truncate border-b-2 border-primary-10 text-[32px] font-bold uppercase text-primary-10',
        {
          className
        }
      )}
      style={style}
    >
      {title}
    </div>
  );
};

export default Title;
