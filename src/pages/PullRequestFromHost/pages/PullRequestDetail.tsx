import { useMutation, useQuery } from '@tanstack/react-query';
import { bookApis, pullRequestApis } from 'apis';
import classNames from 'classnames';
import { Button, Empty, Loading, Title } from 'components';
import { AUDIO_FILE_TYPE, VIDEO_FILE_TYPE } from 'constants/config';
import { useMemo, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getEmbed } from 'utils/utils';

const PullRequestDetail = () => {
  const { id } = useParams();
  const [activeTab, setActiveTab] = useState<'info' | 'marc21'>('info');

  const navigate = useNavigate();

  const { data } = useQuery({
    queryKey: ['getPullRequestDetail', id],
    queryFn: () => pullRequestApis.getPullRequestDetail(id + ''),
    onError: () => navigate(-1)
  });

  const { LinkBiaSach, ListMedia, ListParam, Marc21 } = data?.data.Item || {
    LinkBiaSach: undefined,
    ListMedia: [],
    ListParam: [],
    Marc21: ''
  };

  const { mutate: mergeSelectedBooks, isLoading: loadingPullBookIntoLib } = useMutation({
    mutationFn: () => bookApis.mergePullRequest([id + '']),
    onSuccess: () => {
      toast.success(`Tải thành công sách ${ListParam[7]} về thư viện`);
    }
  });

  const fileExtension = useMemo(() => {
    let _fileExtension: 'image' | 'pdf' | 'ppt' | 'audio' | 'video' | '' = '';

    if (!ListMedia.length) {
      return (_fileExtension = '');
    }

    if (ListMedia.length > 1) {
      _fileExtension = 'image';

      return _fileExtension;
    }

    const file = ListMedia[0].FileName.split('.');
    const _file = file[file.length - 1];

    if (_file.toLocaleLowerCase().includes('pdf')) {
      _fileExtension = 'pdf';

      return _fileExtension;
    }

    if (['pptx', 'ppt'].includes(_file)) {
      _fileExtension = 'ppt';

      return _fileExtension;
    }

    if (AUDIO_FILE_TYPE.includes(_file)) {
      _fileExtension = 'audio';

      return _fileExtension;
    }

    if (VIDEO_FILE_TYPE.includes(_file)) {
      _fileExtension = 'video';

      return _fileExtension;
    }
  }, [ListMedia, id]);

  return (
    <>
      <div className='flex h-screen flex-1 flex-col p-5'>
        <Title title='THÔNG TIN CHI TIẾT SÁCH' />

        {fileExtension === 'audio' ? (
          <>
            <div className='flex items-center gap-2'>
              <h2 className='font-bold text-primary-10'>{ListParam[7]}</h2>

              <button className='flex items-center gap-1 bg-[#EFF4F8] p-3 text-primary-10 shadow-md'>
                <svg width={13} height={16} viewBox='0 0 13 16' fill='none' xmlns='http://www.w3.org/2000/svg'>
                  <path
                    d='M12.8333 5.5H9.16667V0H3.66667V5.5H0L6.41667 11.9167L12.8333 5.5ZM0 13.75V15.5833H12.8333V13.75H0Z'
                    fill='#3472A2'
                  />
                </svg>
                Tải về
              </button>
            </div>

            <audio controls className='mt-3 w-[500px]'>
              <source src={ListMedia?.[0].LinkFile} />
            </audio>
          </>
        ) : fileExtension === 'video' ? (
          <>
            <video controls muted key={fileExtension} width={700} height={500} className='mt-3'>
              <source src={ListMedia?.[0]?.LinkFile} type='video/mp4' />
            </video>

            <h2 className='font-bold text-primary-10'>{ListParam[7]}</h2>
          </>
        ) : (
          ListParam[ListParam.length - 1] &&
          getEmbed(ListParam[ListParam.length - 1]) && (
            <>
              <iframe
                width={800}
                height={500}
                src={`//www.youtube.com/embed/${getEmbed(ListParam[ListParam.length - 1])}`}
                frameBorder='0'
                allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
                allowFullScreen
                title='Embedded youtube'
              />
            </>
          )
        )}

        <div className='my-3 grid grid-cols-12 gap-4'>
          {((fileExtension && !['video', 'audio'].includes(fileExtension)) || fileExtension === '') && (
            <div className='relative col-span-2 max-h-[100px] w-full pt-[100%]'>
              <img
                src={LinkBiaSach?.LinkFile || '/content/Book.png'}
                alt={ListParam[7]}
                className='absolute top-0 left-0 h-full w-full bg-white object-cover p-2'
              />
            </div>
          )}

          <div className='col-span-10'>
            {((fileExtension && !['video', 'audio'].includes(fileExtension)) || fileExtension === '') && (
              <h2 className='font-bold text-primary-10'>{ListParam[7]}</h2>
            )}

            <div className='mt-3 flex items-center gap-3'>
              <button
                className={classNames(
                  `grid w-[120px] place-items-center rounded-md py-3 shadow-md ${
                    activeTab === 'info' ? 'bg-[#EFF4F8] font-bold text-primary-10' : 'bg-white text-[#A7A7A7]'
                  }`
                )}
                onClick={() => setActiveTab('info')}
              >
                Thông tin sách
              </button>

              <button
                className={classNames(
                  `grid w-[120px] shrink-0 place-items-center whitespace-nowrap rounded-md py-3 shadow-md ${
                    activeTab === 'marc21' ? 'bg-[#EFF4F8] font-bold text-primary-10 ' : 'bg-white text-[#A7A7A7]'
                  }`
                )}
                onClick={() => setActiveTab('marc21')}
              >
                Biểu ghi Marc21
              </button>
            </div>

            {activeTab === 'info' && (
              <div className='my-3 grid grid-cols-12'>
                <div className='col-span-6 grid grid-cols-12 space-y-1'>
                  <label className='col-span-6 font-bold'>Tác giả: </label>
                  <p className='col-span-6 truncate text-left text-primary-10'>{ListParam[6] || '--'}</p>

                  <label className='col-span-6 font-bold'>ISBN: </label>
                  <p className='col-span-6 truncate text-left text-primary-10'>{ListParam[0] || '--'}</p>

                  <label className='col-span-6 font-bold'>Ngôn ngữ: </label>
                  <p className='col-span-6 truncate text-left text-primary-10'>{ListParam[2] || '--'}</p>

                  <label className='col-span-6 font-bold'>Năm xuất bản: </label>
                  <p className='col-span-6 truncate text-left text-primary-10'>{ListParam[10] || '--'}</p>

                  <label className='col-span-6 font-bold'>Nhà xuất bản: </label>
                  <p className='col-span-6 truncate text-left text-primary-10'>{ListParam[9] || '--'}</p>
                </div>

                <div className='col-span-6 grid grid-cols-12 space-y-1'>
                  <label className='col-span-6 font-bold'>Lần xuất bản: </label>
                  <p className='col-span-6 truncate text-left text-primary-10'>{ListParam[8] || '--'}</p>

                  <label className='col-span-6 font-bold'>Nơi xuất bản: </label>
                  <p className='col-span-6 truncate text-left text-primary-10'>{'--'}</p>

                  <label className='col-span-6 font-bold'>Giá bìa: </label>
                  <p className='col-span-6 truncate text-left text-primary-10'>
                    {ListParam[1] ? ListParam[1]?.replace(/\B(?=(\d{3})+(?!\d))/g, ',') + ' đồng' : '--'}
                  </p>

                  <label className='col-span-6 font-bold'>Mô tả vật lý: </label>
                  <p className='col-span-6 truncate text-left text-primary-10'>{'--'}</p>

                  <label className='col-span-6 font-bold'>Khổ mẫu: </label>
                  <p className='col-span-6 truncate text-left text-primary-10'>{'--'}</p>
                </div>
              </div>
            )}

            {activeTab === 'marc21' && <p className='mt-2 whitespace-pre-wrap'>{Marc21 || 'Không có biểu ghi 21'}</p>}
          </div>
        </div>

        {((fileExtension && !['video', 'audio'].includes(fileExtension)) || fileExtension === '') && (
          <>
            <h2 className='text-primary-10'>Thông tin nội dung</h2>

            <div className='mt-1 flex items-center gap-2'>
              {ListMedia.length ? (
                ListMedia.map((file, index) => (
                  <div key={index}>
                    {fileExtension === 'ppt' && (
                      <center>
                        <img src='/content/ppt.png' alt={file?.FileName} className='h-24 w-24 shadow' />
                      </center>
                    )}

                    {fileExtension === 'pdf' && (
                      <center>
                        <img src='/content/pdf.png' alt={file?.FileName} className='h-24 w-24 shadow' />
                      </center>
                    )}

                    {fileExtension === 'image' && (
                      <center>
                        <img
                          src={file.LinkFile || '/content/book.png'}
                          alt={file?.FileName}
                          className='h-24 w-24 shadow'
                        />
                      </center>
                    )}

                    <a
                      className='mt-2 block text-xs text-primary-30'
                      href={file?.LinkFile}
                      target='_blank'
                      rel='noreferrer'
                    >
                      {file?.FileName}
                    </a>
                  </div>
                ))
              ) : (
                <Empty />
              )}
            </div>
          </>
        )}
      </div>

      <div className='mb-3 flex w-full items-center justify-end gap-2'>
        <Button variant='secondary' onClick={() => navigate(-1)}>
          Quay về
        </Button>

        <Button onClick={() => mergeSelectedBooks()}>Đồng bộ về Thư viện</Button>
      </div>

      <Loading open={loadingPullBookIntoLib} />
    </>
  );
};

export default PullRequestDetail;
