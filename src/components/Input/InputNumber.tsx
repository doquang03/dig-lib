import { InputNumber, type InputNumberProps } from 'antd';
import classNames from 'classnames';
import { Control, Controller, FieldValues, Path } from 'react-hook-form';

type Props<T extends FieldValues> = {
  control: Control<T>;
  name: Path<T>;
  placeholder?: string;
  errorMessage?: string;
  disabled?: boolean;
} & InputNumberProps;

function RHFInputNumber<T extends FieldValues>(props: Props<T>) {
  const { control, name, placeholder, errorMessage, disabled = false, min = 1, max = 99999, ...rest } = props;

  return (
    <>
      <Controller
        control={control}
        name={name}
        render={({ field: { onChange, value } }) => {
          return (
            <InputNumber
              placeholder={placeholder}
              min={min}
              value={value}
              max={max}
              onChange={onChange}
              disabled={disabled}
              {...rest}
              className={classNames('w-full border border-gray-300 py-[0.5rem] px-2 outline-none', {
                'border border-red-500': !!errorMessage
              })}
              onKeyPress={(event) => {
                if (!/[0-9]/.test(event.key)) {
                  event.preventDefault();
                }
              }}
            />
          );
        }}
      />

      {!!errorMessage && <p className='text-[14px] text-red-500'>{errorMessage}</p>}
    </>
  );
}

export default RHFInputNumber;
