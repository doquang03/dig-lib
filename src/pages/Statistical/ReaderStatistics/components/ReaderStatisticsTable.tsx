import { useMutation, useQuery } from '@tanstack/react-query';
import { Table, Tag } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { statistic } from 'apis';
import classNames from 'classnames';
import { Button, Empty, SizeChanger } from 'components';
import { Dayjs } from 'dayjs';
import { PickerMode } from 'rc-picker/lib/interface';
import { useMemo, useState, type ChangeEvent, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getSerialNumber } from 'utils/utils';
import ReaderStatisticsSearchForm from './ReaderStatisticsSearchForm';
import { useUser } from 'contexts/user.context';

type Props = {
  picker: PickerMode;
};

const ReaderStatisticsTable = (props: Props) => {
  const [searchParams, setSearchParams] = useState<{
    day?: string;
    month?: string;
    year?: string;
    returnType: string | number;
    Online: 0 | 1;
    styleUser: '' | 'hs' | 'gv';
    page: number;
    pageSize: number;
    textForSearch: string;
  }>({
    month: (new Date().getMonth() + 1).toString(),
    year: new Date().getFullYear().toString(),
    Online: 0,
    returnType: '',
    styleUser: '',
    page: 1,
    pageSize: 30,
    textForSearch: ''
  });

  const { isAllowedAdjustment } = useUser();

  const { data: danhMucMuonSachData, isLoading: loadingDanhMucMuonSach } = useQuery({
    queryKey: [
      'danhMucMuonSach',
      searchParams.month,
      searchParams.styleUser,
      searchParams.year,
      searchParams.page,
      searchParams.pageSize,
      searchParams.textForSearch,
      searchParams.day,
      searchParams.returnType
    ],
    queryFn: () => {
      return statistic.getDanhMucMuonSach(searchParams, true);
    },
    enabled: !Boolean(searchParams.Online)
  });

  useEffect(() => {
    if (!(props.picker === 'month')) {
      if (searchParams.day === undefined) searchParams.day = new Date().getDate().toString();
    } else {
      delete searchParams.day;
    }
  });

  const danhMucSach = useMemo(() => {
    if (!Boolean(searchParams.Online)) {
      if (!danhMucMuonSachData?.data.Item) return;
      return danhMucMuonSachData.data.Item;
    } else {
      return;
    }
  }, [danhMucMuonSachData, searchParams.Online]);

  const { mutate: downloadExcel, isLoading: loadingDownloadExcel } = useMutation({
    mutationFn: () => statistic.downExcelThongKeMuonSach(searchParams, true),
    onSuccess: (data) => {
      toast.success('Tải danh mục mượn sách thành công');
      const blob = new Blob([data.data, { type: 'application/xls' }]);
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = 'ThongKeBanDoc.xls';
      link.click();
    }
  });

  const columns: ColumnsType<StaticRentingBook | OnlineRentingBook> = useMemo(() => {
    return [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        width: 100,
        render: (value, record, index) => {
          return getSerialNumber(searchParams.page, searchParams.pageSize, index);
        }
      },
      {
        title: 'Mã cá biệt',
        dataIndex: 'MaCaBiet',
        key: 'MaCaBiet',
        onCell: (record) => ({
          className:
            record?.MaCaBiet?.toLocaleLowerCase().includes('đã xóa') ||
            record?.MaCaBiet?.toLocaleLowerCase().includes('đã thanh lý')
              ? 'text-danger-10'
              : ''
        }),
        width: 150
      },
      {
        title: 'Tên sách',
        dataIndex: 'TenSach',
        key: 'TenSach',
        onCell: (record) => ({
          className:
            record.TenSach?.toLocaleLowerCase().includes('đã xóa') ||
            record.TenSach?.toLocaleLowerCase().includes('đã thanh lý')
              ? 'text-danger-10 text-left'
              : 'text-left'
        }),
        render: (value, { TenSach, idSach }) => {
          return !TenSach.includes('"sách đã bị xóa"') ? (
            <Link
              to={`/Sach/CapNhat/${idSach}`}
              className='cursor-pointer truncate text-left text-primary-50  underline'
            >
              {TenSach}
            </Link>
          ) : (
            <p className=' text-left'>{TenSach}</p>
          );
        },
        ellipsis: true
      },
      {
        title: 'Mã kiểm soát',
        dataIndex: 'MaKiemSoat',
        key: 'MaKiemSoat',
        width: 150
      },
      {
        title: 'Người mượn',
        dataIndex: 'TenThanhVien',
        key: 'TenThanhVien',
        onCell: (record) => ({
          className: record.TenThanhVien?.toLocaleLowerCase().includes('xóa') ? 'text-danger-10 text-left' : 'text-left'
        }),
        render: (value, { TenThanhVien, LoaiTK, idUser }) => {
          return !TenThanhVien?.toLocaleLowerCase().includes('xóa') ? (
            <Link
              to={LoaiTK === 'gv' ? `/GiaoVien/CapNhat/${idUser}` : `/HocSinh/CapNhat/${idUser}`}
              className='cursor-pointer truncate text-primary-50 underline'
            >
              {TenThanhVien}
            </Link>
          ) : (
            TenThanhVien
          );
        },
        ellipsis: true
      },
      {
        title: 'Ngày đọc',
        dataIndex: 'NgayMuonTemp',
        key: 'NgayMuonTemp',
        width: 180
      },
      {
        title: 'Tình trạng trả sách',
        dataIndex: 'TrangThaiTraSach',
        key: 'TrangThaiTraSach',
        render: (value, record) => {
          const statusLabel = record.DaTra ? 'Đã trả' : 'Chưa trả';
          return <Tag color={record.DaTra ? 'success' : 'warning'}>{statusLabel}</Tag>;
        },
        width: 180
      }
    ];
  }, [searchParams.page, searchParams.pageSize]);

  const handleChangeSearchParams = (e: ChangeEvent<HTMLSelectElement | HTMLInputElement>) => {
    const { name, value } = e.target;
    setSearchParams((prevState) => {
      return {
        ...prevState,
        [name]: name === 'Online' ? Number(value) : value,
        page: 1
      };
    });
  };

  const handleChangeDatePicker = (value: Dayjs | null) => {
    if (value) {
      props.picker === 'month'
        ? setSearchParams((prevState) => {
            return {
              ...prevState,
              month: (value?.month() + 1).toString(),
              year: value?.year().toString()
            };
          })
        : setSearchParams((prevState) => {
            return {
              ...prevState,
              day: value?.date().toString(),
              month: (value?.month() + 1).toString(),
              year: value?.year().toString()
            };
          });
    }
  };

  return (
    <div className='mt-4'>
      <ReaderStatisticsSearchForm
        picker={props.picker}
        onChangeSearchParams={handleChangeSearchParams}
        onChangeDatePicker={handleChangeDatePicker}
      />

      <Button
        onClick={() => {
          if (!Boolean(searchParams.Online)) downloadExcel();
        }}
        loading={loadingDownloadExcel}
        disabled={loadingDownloadExcel}
        type='button'
        className={isAllowedAdjustment ? 'mt-3' : 'hidden'}
      >
        Tải file Excel
      </Button>

      <p className='mt-3 inline-block text-danger-10'>
        Tổng số sách được mượn đọc là: <span className='font-bold'>{danhMucSach?.SoSachDuocMuon || '--'} quyển</span>
      </p>
      <p className='mx-2.5 mt-3 inline-block text-danger-10'>-</p>
      <p className='mb-3 inline-block text-danger-10'>
        Tổng số người mượn sách đọc là: <span className='font-bold'>{danhMucSach?.SoNguoiMuonSach || '--'} người</span>
      </p>

      <Table
        bordered
        rowKey={(row) => row.Id}
        className='custom-table'
        columns={columns}
        dataSource={danhMucSach?.ListRutGon || []}
        locale={{
          emptyText: () => <Empty />
        }}
        loading={loadingDanhMucMuonSach}
        pagination={{
          onChange(current, pageSize) {
            setSearchParams((prevState) => {
              return {
                ...prevState,
                pageSize: pageSize || 30,
                page: current
              };
            });
          },
          pageSize: searchParams.pageSize,
          hideOnSinglePage: true,
          showSizeChanger: false,
          total: danhMucSach?.count,
          showQuickJumper: true,
          locale: { jump_to: '', page: '' },
          current: searchParams.page
        }}
      />

      <div
        className={classNames('relative', {
          'mt-[64px]': searchParams.pageSize >= Number(danhMucSach?.count)
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger
            visible={!!danhMucSach?.ListRutGon.length}
            value={searchParams.pageSize + ''}
            currentPage={searchParams.page.toString()}
            total={danhMucSach?.count + ''}
            onChange={(pageSize) => {
              setSearchParams((prevState) => {
                return {
                  ...prevState,
                  pageSize: +pageSize,
                  page: 1
                };
              });
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default ReaderStatisticsTable;
