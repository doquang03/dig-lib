import { QueryConfig } from 'hooks/useQueryConfig';
import { isEmpty, omitBy } from 'lodash';
import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/Sach`;

const endPointMarc21 = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/Marc`;

type GetInitViewData = {
  Options_BoSuuTap: Selections;
  Options_ChuDiem: Selections;
  Options_MonHoc: Selections;
  Options_NXB: Selections;
  Options_TacGia: Selections;
  Options_ThuMucSach: Selections;
  Options_KeSach: Selections;
};

type GetUpdateBooksData = {
  TotalEnty: number;
  TotalFail: number;
  ListThatBai: [];
};

export type GetBookByIdData = {
  SachDTO: Book;
  SLDangDuocDat: number;
  SLDangDuocMuon: number;
  TongSoLanDuocMuon: number;
};

export type CreatePhieuNhapSachVars = {
  id: string;
  IdUserAdmin: string;
  UserName: string;
  GhiChuPhieuNhap: string;
  DanhSachPhieuNhap: {
    IdKho: string;
    SoLuong: number;
    IdTrangThai: string;
    IdNCC: string;
    SCT: string;
    SVS: string;
    NVS: string;
    GhiChu: string;
  }[];
};

export type UpdateSachCaBietVars = {
  Id: string;
  IdSach: string;
  IdTrangThai: string;
  IdNguonCungCap: string;
  SoChungTu: string;
  SoVaoSoTongQuat: string;
  NgayVaoSo_String: string;
};

const bookshelfApis = {
  GetInitView: () => {
    return httpThuVien.get<ResponseApi<GetInitViewData>>(`/${endPoint}/GetInitView`);
  },
  GetInitCreateForm: () => {
    return httpThuVien.get(`/${endPoint}/GetInitCreateForm`);
  },
  CreateByCode: (id: string) => {
    return httpThuVien.post(`/${endPoint}/CreateByCode?codeBST=` + id);
  },
  ///////////tạo sách payload, idUserAdmin, userName///////////
  Create: (payload: FormData) => {
    return httpThuVien.post<ResponseApi<{ Id: string }>>(`/${endPoint}/Create`, payload, {
      headers: { 'Content-Type': 'multipart/form-data', Accept: '*/*' }
    });
  },
  GetListSachPure: (page: number, sizeNumber: number, TextForSearch: string) => {
    return httpThuVien.post<ResponseApi<{ ListSach: Book[]; count: number }>>(
      `/${endPoint}/GetListSachPure?page=${page}&sizeNumber=${sizeNumber}&TextForSearch=${TextForSearch}`
    );
  },
  ///////////lấy thông tin sách theo id///////////
  EditByID: (id: string) => {
    return httpThuVien.post(`/${endPoint}/EditByID?id=` + id);
  },
  TemplateExcel: (arrCauHinh: Array<Boolean>, fileName: string) => {
    return httpThuVien.post(
      `/${endPoint}/TemplateExcel?fileName=${fileName}`,
      Object.keys(arrCauHinh).map((key: string) => arrCauHinh[Number.parseInt(key)]),
      { responseType: 'arraybuffer' }
    );
  },
  PreviewImport: (payload: object) => {
    return httpThuVien({
      method: 'post',
      url: `/${endPoint}/PreviewImport`,
      data: payload,
      headers: { 'Content-Type': 'multipart/form-data' }
    });
  },
  ImportSave_Create: (data: Array<object>, arrCauHinh: Array<Boolean>) => {
    return httpThuVien.post(`/${endPoint}/ImportSave_Create`, {
      data: data,
      arrCauHinh: JSON.stringify(Object.keys(arrCauHinh).map((key: string) => arrCauHinh[Number.parseInt(key)]))
    });
  },
  ImportSave_Update: (data: Array<object>, arrCauHinh: Array<Boolean>) => {
    return httpThuVien.post(`/${endPoint}/ImportSave_Update`, {
      data: data,
      arrCauHinh: JSON.stringify(Object.keys(arrCauHinh).map((key: string) => arrCauHinh[Number.parseInt(key)]))
    });
  },
  GetBooksList: (payload: QueryConfig) => {
    const {
      BST,
      ChuDiem,
      TextForSearch,
      KhoiLop,
      MonHoc,
      OrderBy,
      ThuMucSach,
      TacGia,
      NXB,
      KeSach,
      TaiLieuGiay,
      IdKho,
      IsPublish
    } = payload;

    let isDigitalResource: string | boolean = '';

    switch (TaiLieuGiay) {
      case '1':
        isDigitalResource = false;
        break;

      case '2':
        isDigitalResource = true;
        break;

      case '':
        isDigitalResource = '';
        break;

      default:
        break;
    }

    let result = omitBy(
      {
        ...payload,
        CurrentPage: payload.page,
        KhoSach_selected: IdKho,
        BoSuuTap_selected: BST,
        ChuDiem_selected: ChuDiem,
        Keyword: TextForSearch,
        KhoiLop_selected: KhoiLop,
        MonHoc_selected: MonHoc,
        NXB_selected: NXB,
        SapXepType_selected: OrderBy || '33',
        ThuMucSach_selected: ThuMucSach,
        tacGia_Selected: TacGia,
        KeSach: KeSach,
        IsDigitalResource: isDigitalResource,
        IsPublish: IsPublish === 'true' ? true : IsPublish === 'false' ? false : undefined
      },
      isEmpty
    );
    if (IsPublish) {
      result = { ...result, IsPublish: IsPublish === 'true' ? true : IsPublish === 'false' ? false : undefined };
    }
    return httpThuVien.post<ResponseApi<{ ListBook: Book[]; count: number }>>(
      `/${endPoint}/GetListSach_V2`,
      typeof isDigitalResource === 'boolean'
        ? {
            ...result,
            IsDigitalResource: isDigitalResource
          }
        : result
    );
  },
  getBookById: (id: string) =>
    httpThuVien.post<
      ResponseApi<{ SachDTO: Book; SLDangDuocDat: number; SLDangDuocMuon: number; TongSoLanDuocMuon: number }>
    >(`/${endPoint}/EditByID?id=${id}`),
  getArrayBookId: (arrayId: Array<string>) =>
    httpThuVien.post<ResponseApi<EditArraySach>>(`/${endPoint}/Init_EditArraySach`, arrayId),
  Init_SachSearch: (TextForSearch: string) =>
    httpThuVien.post<ResponseApi<EditArraySach>>(`/${endPoint}/Init_SachSearch?TextForSearch=${TextForSearch}`),
  editBookById: (payload: FormData) => {
    return httpThuVien.post<ResponseApi<{ Id: string }>>(`/${endPoint}/Edit`, payload, {
      headers: { 'Content-Type': 'multipart/form-data', Accept: '*/*' }
    });
  },
  exportExcel: (ids: string[]) =>
    httpThuVien.post(`/${endPoint}/ExportExcelByList`, ids, { responseType: 'arraybuffer' }),
  importMarc21: (file: FormData) =>
    httpThuVien.post<ResponseApi<GetImportMarc21Data>>(`/${endPointMarc21}/ImportMarc?CodeBSTForCreate=sach`, file, {
      headers: { 'Content-Type': 'multipart/form-data', Accept: '*/*' }
    }),
  exportMarc21: (payload: { Id: string }[]) =>
    httpThuVien.post<ResponseApi<GetImportMarc21Data>>(`/${endPointMarc21}/ExportMarcByList`, payload, {
      responseType: 'arraybuffer'
    }),
  updateBooksCollection: (payload: { bstId: string; ids: string[] }) =>
    httpThuVien.post<ResponseApi<GetUpdateBooksData>>(
      `/${endPoint}/CapNhatBoSuuTap?idBST=${payload.bstId}`,
      payload.ids
    ),
  updateBooksDocumentFolder: (payload: { documentId: string; ids: string[] }) =>
    httpThuVien.post<ResponseApi<GetUpdateBooksData>>(
      `/${endPoint}/CapNhatThuMucSach?idTMS=${payload.documentId}`,
      payload.ids
    ),
  updateBooksTopic: (payload: { idTopic: string; ids: string[] }) =>
    httpThuVien.post<ResponseApi<GetUpdateBooksData>>(
      `/${endPoint}/UpdateTopic?IdTopic=${payload.idTopic}`,
      payload.ids
    ),
  updateBooksSubject: (payload: { idSubject: string; ids: string[] }) =>
    httpThuVien.post<ResponseApi<GetUpdateBooksData>>(
      `/${endPoint}/UpdateSubject?IdSubject=${payload.idSubject}`,
      payload.ids
    ),
  deleteBook: (id: string) => httpThuVien.post<ResponseApi<GetUpdateBooksData>>(`/${endPoint}/Delete?id=${id}`),
  deleteBooks: (ids: string[]) => httpThuVien.post<ResponseApi<GetUpdateBooksData>>(`/${endPoint}/DeleteMulti`, ids),
  getWareHouseInfoById: (id: string) =>
    httpThuVien.post<ResponseApi<GetWareHouseData>>(`/${endPoint}/Init_EditSach?Id=${id}`),
  getBookContent: (id: string) =>
    httpThuVien.post<ResponseApi<GetBookContent>>(`/${endPoint}/Init_NoiDungSach?id=${id}`),
  uploadImagesFile: (payload: FormData) =>
    httpThuVien.post<ResponseApi<GetBookContent>>(`/${endPoint}/UploadImage`, payload, {
      headers: { 'Content-Type': 'multipart/form-data', Accept: '*/*' }
    }),
  uploadPDFFile: (payload: FormData) =>
    httpThuVien.post<ResponseApi<GetBookContent>>(`/${endPoint}/UploadPDF`, payload, {
      headers: { 'Content-Type': 'multipart/form-data', Accept: '*/*' }
    }),
  uploadAudiVideo: (payload: FormData) =>
    httpThuVien.post<ResponseApi<GetBookContent>>(`/${endPoint}/UploadMedia`, payload, {
      headers: { 'Content-Type': 'multipart/form-data', Accept: '*/*' }
    }),
  deleteFile: (payload: { id: string; fileName: string }) =>
    httpThuVien.post<ResponseApi<GetBookContent>>(
      `/${endPoint}/DeleteFile?id=${payload.id}&fileName=${payload.fileName}`
    ),
  createPhieuNhapSach: (payload: CreatePhieuNhapSachVars) => {
    const { id, DanhSachPhieuNhap, GhiChuPhieuNhap, IdUserAdmin, UserName } = payload;

    return httpThuVien.post<ResponseApi>(
      `/${endPoint}/CreatePhieuNhapSach?id=${id}&IdUserAdmin=${IdUserAdmin}&UserName=${UserName}&GhiChuPhieuNhap=${GhiChuPhieuNhap}`,
      DanhSachPhieuNhap
    );
  },
  updateSachCaBiet: (payload: UpdateSachCaBietVars[]) =>
    httpThuVien.post<ResponseApi<{ ListFail: UpdateSachCaBietVars[] }>>(`/${endPoint}/EditSaveChange`, payload),
  exportQRCode: (payload: {
    ListSachCaBiet: string[];
    tuychon: string[];
    tuychon_QR: string[];
    loaiQR: string;
    tuyChon_NangCao: string[];
    tuychon_InSGK: string;
  }) =>
    httpThuVien.post(`/${endPoint}/XuatQR_Sach_IActionResult`, payload, {
      responseType: 'blob'
    }),
  XuatDanhMucChuDiem: (idChuDiem: string) => {
    return httpThuVien.post(
      `/${endPoint}/XuatDanhMucChuDiem?idChuDiem=${idChuDiem}`,
      {},
      {
        responseType: 'arraybuffer'
      }
    );
  },
  XuatDanhMucMonHoc: (idMonHoc: string) => {
    return httpThuVien.post(
      `/${endPoint}/XuatDanhMucMonHoc?idMonHoc=${idMonHoc}`,
      {},
      {
        responseType: 'arraybuffer'
      }
    );
  },
  XuatDanhMucKhoiLop: (KhoiLop: string) => {
    return httpThuVien.post(
      `/${endPoint}/XuatDanhMucKhoiLop?KhoiLop=${KhoiLop}`,
      {},
      {
        responseType: 'arraybuffer'
      }
    );
  },
  digitizingBooks: (ids: string[]) =>
    httpThuVien.post<ResponseApi<{ ListSuccess: string[]; ListFail: string[] }>>(`${endPoint}/DigitizingBook`, ids),
  publishToHost: (ids: string[]) =>
    httpThuVien.post<ResponseApi<{ ListSuccess: FailureMessage[]; ListFail: FailureMessage[] }>>(
      `${endPoint}/UploadMarc21`,
      ids
    ),
  mergePullRequest: (ids: string[]) => httpThuVien.post<ResponseApi>(`${endPoint}/DownloadMarc21`, ids)
};

export default bookshelfApis;
