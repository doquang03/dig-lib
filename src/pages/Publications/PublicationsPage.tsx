import { Outlet } from 'react-router-dom';

const PublicationsPage = () => {
  return <Outlet />;
};

export default PublicationsPage;
