import { TabsProps } from 'antd';
import { Tabs, Title } from 'components';
import { useState } from 'react';
import { Chart } from './components';

const items: TabsProps['items'] = [
  {
    key: 'week',
    label: `Thống kê theo tuần`,
    children: <Chart type='week' />,
    forceRender: false
  },
  {
    key: 'month',
    label: `Thống kê theo tháng`,
    children: <Chart type='month' />,
    forceRender: false
  },
  {
    key: 'year',
    label: `Thống kê theo năm`,
    children: <Chart type='year' />,
    forceRender: false
  }
];

const StatisticalReservingBooksChart = () => {
  const [activeTab, setActiveTab] = useState<string>('week');

  const handleChangeTab = (activeTab: string) => setActiveTab(activeTab);
  return (
    <div>
      <Title title='THỐNG KÊ LỊCH SỬ ĐẶT MƯỢN ONLINE' />

      <Tabs
        items={items}
        activeKey={activeTab}
        onChange={handleChangeTab}
        destroyInactiveTabPane={true}
        className='mt-3'
      />
    </div>
  );
};

export default StatisticalReservingBooksChart;
