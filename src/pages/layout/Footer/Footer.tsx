import { Layout } from 'antd';
import 'css/LayoutApp.css';
const { Footer: FooterAntd } = Layout;

const Footer = () => {
  return (
    <FooterAntd id='footer'>
      <span>Copyright 2023 - Thiết kế và phát triển bởi Công ty TNHH Đầu tư Phát triển BiTech</span>
      <span> - </span>
      <span>
        <a href='tel:02822532586'>Điện thoại: (028) 22 532 586</a>
      </span>
      <span> - </span>
      <span>
        <a href='http://zalo.me/0902585262'>Zalo: (09) 02 585 262</a>
      </span>
      <span> - </span>
      <span>
        <a href='mailto:bitech.info123@gmail.com'>Email: bitech.info123@gmail.com</a>
      </span>
      <span> - </span>
      <span>
        <a href='mailto:cskh.bitech@gmail.com'>CSKH: cskh.bitech@gmail.com</a>
      </span>
    </FooterAntd>
  );
};

export default Footer;
