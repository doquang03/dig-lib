import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { ddcApis } from 'apis';
import { Button, Input, Loading, ModalDelete, Title, TitleDelete } from 'components';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import { useEffect, useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { ddcSchema } from 'utils/rules';

const initalFormValues: Omit<DDC, 'Id' | 'CreateDatetTime'> = {
  MaDDC: '',
  Ten: ''
};

const AddDDCPage = () => {
  const { id } = useParams();

  const [visible, setVisible] = useState<boolean>(false);
  const [selectedDDC, setSelectedDDC] = useState<DDC>();

  const navigate = useNavigate();

  const { isAllowedAdjustment } = useUser();

  const queryClient = useQueryClient();

  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors }
  } = useForm<typeof initalFormValues>({
    defaultValues: initalFormValues,
    resolver: yupResolver(ddcSchema)
  });

  const { data: ddcData, refetch } = useQuery({
    queryKey: ['ddc', id],
    queryFn: () => ddcApis.getDDC(id + ''),
    enabled: !!id,
    onError: () => {
      navigate(path.ddc);
    }
  });

  const ddc = useMemo(() => {
    if (!ddcData?.data.Item) {
      return;
    }

    return ddcData.data.Item;
  }, [ddcData?.data.Item]);

  const { mutate: createDDC, isLoading: loadingCreateDDC } = useMutation({
    mutationFn: ddcApis.createDDC,
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ['ddcs'] });
      toast.success('Tạo DDC thành công');
      navigate(path.ddc);
    }
  });

  const { mutate: updateDDC, isLoading: loadingUpdateDDC } = useMutation({
    mutationFn: ddcApis.updateDDC,
    onSuccess: () => {
      refetch();
      toast.success('Cập nhật DDC thành công');
    }
  });

  const { mutate: deleteDDC, isLoading: loadingDeleteUpdate } = useMutation({
    mutationFn: ddcApis.deleteDCC,
    onSuccess: () => {
      toast.success('Xóa DDC thành công');
      navigate(path.ddc);
    }
  });

  useEffect(() => {
    if (Boolean(id) && ddc) {
      setValue('Ten', ddc.Ten);
      setValue('MaDDC', ddc.MaDDC);
    }
  }, [id, setValue, ddc]);

  const onSubmit = handleSubmit((data) => {
    let input = data.Ten.replace(/\s+/g, ' ');
    let code = data?.MaDDC?.replace(/\s+/g, ' ');
    if (!Boolean(id)) {
      !loadingCreateDDC && createDDC({ ...data, Ten: input, MaDDC: code });
    } else {
      !loadingUpdateDDC && updateDDC({ ...data, Ten: input, MaDDC: code, Id: id + '' });
    }
  });

  const handleDelete = () => {
    deleteDDC(id + '');
  };

  const handleBack = () => navigate(path.ddc);

  return (
    <div className='p-5'>
      <Title title={Boolean(id) ? 'Cập nhật DDC' : 'Thêm mới DDC'} />

      <form className='w-1/2 px-3' onSubmit={onSubmit}>
        <label className='mt-3 font-bold'>
          Tên DDC <span className='text-danger-10'>*</span>
        </label>
        <Input
          placeholder='Nhập tên DDC'
          name='Ten'
          register={register}
          errorMessage={errors?.Ten?.message}
          maxLength={256}
        />

        <label className='mt-3 font-bold'>
          Mã DDC <span className='text-danger-10'>*</span>
        </label>
        <Input
          placeholder='Nhập mã DDC'
          name='MaDDC'
          register={register}
          errorMessage={errors?.MaDDC?.message}
          maxLength={256}
        />

        <div className='mt-4 flex items-center justify-end gap-2'>
          <Button variant='secondary' onClick={handleBack} type='button'>
            Quay về
          </Button>

          {Boolean(id) ? (
            <>
              <Button
                type='button'
                variant='danger'
                onClick={() => setSelectedDDC(ddc)}
                className={isAllowedAdjustment ? '' : 'hidden'}
              >
                Xóa
              </Button>

              <Button variant='default' type='submit' className={isAllowedAdjustment ? '' : 'hidden'}>
                Cập nhật
              </Button>
            </>
          ) : (
            <Button variant='default' type='submit' className={isAllowedAdjustment ? '' : 'hidden'}>
              Thêm mới
            </Button>
          )}
        </div>
      </form>

      <ModalDelete
        open={Boolean(selectedDDC)}
        closable={false}
        title={<TitleDelete firstText='Bạn có chắn chắn muốn xóa DDC này' secondText={selectedDDC?.Ten} />}
        handleCancel={() => {
          setSelectedDDC(undefined);
        }}
        handleOk={() => {
          setSelectedDDC(undefined);
          handleDelete();
        }}
      />

      <Loading open={loadingCreateDDC || loadingUpdateDDC || loadingDeleteUpdate} />
    </div>
  );
};

export default AddDDCPage;
