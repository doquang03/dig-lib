import { DeleteFilled, EditOutlined } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Tooltip } from 'antd';
import Table, { type ColumnsType } from 'antd/es/table';
import { publishingCompanyApis } from 'apis';
import classNames from 'classnames';
import { Button, Empty, Input, Loading, ModalDelete, SizeChanger, Title, TitleDelete } from 'components';
import type { ButtonCustomProps } from 'components/Button/Button';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import { usePaginationNavigate, useQueryConfig } from 'hooks';
import { isEmpty, omitBy, trim } from 'lodash';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import { createSearchParams, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getSerialNumber } from 'utils/utils';

// TODO: Delete as soon as api implement
type Mock = {
  Id: string;
  Ten: string;
  GhiChu: string;
};

type FormInput = {
  TextForSearch: string;
};

const PublishingCompanyManagermentContent = () => {
  const { register, handleSubmit, reset, setValue } = useForm<FormInput>({
    defaultValues: {
      TextForSearch: ''
    }
  });

  const navigate = useNavigate();

  const handleNavigation = usePaginationNavigate();

  const queryConfig = useQueryConfig();
  const [totalListNhaXuanBan, setTotalListNhaXuanBan] = useState(0);
  const [selectedDelete, setSelectedDelete] = useState<Mock>();
  const [isVisiable, setIsVisiable] = useState(false);

  const { isAllowedAdjustment } = useUser();

  const {
    data: dataNhaXuatBan,
    refetch,
    isFetching
  } = useQuery({
    queryKey: ['NhaXuanBan', queryConfig.page, queryConfig.pageSize, queryConfig.TextForSearch],
    queryFn: () => {
      let page = 1;
      if (typeof queryConfig.page === 'string') page = parseInt(queryConfig.page);
      let queryString = `?page=${page}&sizeNumber=${queryConfig.pageSize}`;
      if (queryConfig.TextForSearch !== undefined) queryString += `&TextForSearch=${queryConfig.TextForSearch}`;
      let request = publishingCompanyApis.Index(queryString);
      return request;
    }
  });

  const listNhaXuanBan = useMemo(() => {
    if (!dataNhaXuatBan?.data?.Item) return;
    const { ListNhaXuatBan, count } = dataNhaXuatBan?.data?.Item;
    setTotalListNhaXuanBan(count);
    return ListNhaXuatBan;
  }, [dataNhaXuatBan?.data?.Item]);

  const { mutate: DeleteSingle, isLoading: isLoadingDeleteSingle } = useMutation({
    mutationFn: (Id: string) => publishingCompanyApis.Delete(Id),
    onSuccess: () => {
      if (listNhaXuanBan?.length === 1) handleResetField();
      else refetch();
      toast('Xóa nhà xuất bản thành công');
    }
  });

  const onPaginate = useCallback(
    (page: number, pageSize: number) => {
      handleNavigation({ ...queryConfig, page: page + '', pageSize: pageSize + '' });
    },
    [handleNavigation, queryConfig]
  );

  const pagination = useMemo(() => {
    return {
      current: (!!queryConfig.page && +queryConfig.page) || 1,
      pageSize: (!!queryConfig.pageSize && +queryConfig.pageSize) || 30,
      total: totalListNhaXuanBan,
      onChange: onPaginate,
      hideOnSinglePage: true,
      showSizeChanger: false,
      showQuickJumper: true,
      locale: { jump_to: '', page: '' }
    };
  }, [onPaginate, queryConfig.page, queryConfig.pageSize, totalListNhaXuanBan]);

  const columns = useMemo(() => {
    const _columns: ColumnsType<Mock> = [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        className: 'min-content',
        render: (value, record, index) => {
          return (
            queryConfig.page && queryConfig.pageSize && getSerialNumber(+queryConfig.page, +queryConfig.pageSize, index)
          );
        },
        width: 100
      },
      {
        title: 'Tên nhà xuất bản',
        dataIndex: 'Ten',
        key: 'Ten',
        className: 'min-content',
        render: (value, { Ten }) => (
          <Tooltip placement='topLeft' title={Ten} arrow={true} className='truncate'>
            <p>{Ten}</p>
          </Tooltip>
        ),
        onCell: (record) => ({
          className: 'text-left'
        }),
        ellipsis: true
      },
      {
        title: 'Ghi chú',
        dataIndex: 'GhiChu',
        key: 'GhiChu',
        render: (value, { GhiChu }) => (
          <Tooltip placement='topLeft' title={GhiChu} arrow={true} className='truncate'>
            <p>{GhiChu}</p>
          </Tooltip>
        ),
        onCell: (record) => ({
          className: 'text-left'
        })
      }
    ];

    if (isAllowedAdjustment) {
      _columns.push({
        title: 'Hành động',
        dataIndex: 'actions',
        key: 'actions',
        className: 'min-content',
        render: (value, record) => {
          return (
            <>
              <Tooltip title='Cập nhật'>
                <button
                  className='mx-2'
                  onClick={(e) => {
                    e.stopPropagation();
                    navigate(`CapNhat/${record.Id}`);
                  }}
                >
                  <EditOutlined style={{ fontSize: '20px', color: '#08c' }} />
                </button>
              </Tooltip>

              <Tooltip title='Xóa'>
                <button
                  className='mx-2'
                  onClick={(e) => {
                    e.stopPropagation();
                    setSelectedDelete(() => {
                      return record;
                    });
                    setIsVisiable(true);
                  }}
                >
                  <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
                </button>
              </Tooltip>
            </>
          );
        },
        width: 200
      });
    }

    return _columns;
  }, [isAllowedAdjustment, navigate, queryConfig.page, queryConfig.pageSize]);

  useEffect(() => {
    Object.keys(queryConfig).forEach((key) => {
      if (!key) {
        return;
      }
      // @ts-ignore
      setValue(key, queryConfig[key]);
    });
  }, [queryConfig, setValue]);

  const onSubmit = handleSubmit((data) => {
    handleNavigation({
      ...omitBy({ ...data, TextForSearch: trim(data.TextForSearch, ' ').replace(/\s\s+/g, ' ') }, isEmpty),
      page: '1'
    });
  });

  const handleResetField = () => {
    const { pathname } = window.location;

    navigate({
      pathname: pathname,
      search: createSearchParams({}).toString()
    });

    reset();
  };

  // TODO: Do something with selected PublishingCompanys
  const handleSelectedPublishingCompanys = () => {
    navigate(path.themnhaxuatban);
  };

  let locale = {
    emptyText: 'Không tìm được kết quả phù hợp'
  };

  return (
    <div className='p-5'>
      <Title title='DANH SÁCH NHÀ XUẤT BẢN' />

      <form className='my-5 flex flex-col gap-2 bg-secondary-10 py-3 px-3.5 md:flex-row' onSubmit={onSubmit}>
        <div className='flex w-full flex-col gap-2 md:w-1/2 md:flex-row'>
          <Input
            placeholder='Nhập tên nhà xuất bản'
            containerClassName='w-[100%]'
            name='TextForSearch'
            register={register}
          />

          <Button type='button' variant='secondary' className='shrink-0 font-semibold' onClick={handleResetField}>
            Làm mới
          </Button>

          <Button type='submit' variant='default' className='shrink-0 font-semibold'>
            Tìm kiếm
          </Button>
        </div>
      </form>

      <Button
        className={classNames('w-[100%] md:w-auto', {
          hidden: !isAllowedAdjustment,
          'w-[100%] md:w-auto': isAllowedAdjustment
        })}
        onClick={handleSelectedPublishingCompanys}
      >
        Thêm nhà xuất bản
      </Button>

      <div className='mt-6'>
        {Number.isInteger(queryConfig.page && +queryConfig.page) ? (
          <Table
            loading={isFetching && !listNhaXuanBan?.length}
            columns={columns}
            dataSource={listNhaXuanBan || []}
            pagination={pagination}
            scroll={{ x: 980 }}
            rowKey={(record) => record.Id}
            className='custom-table'
            locale={{
              emptyText: () => <Empty label={locale.emptyText} />
            }}
            bordered
          />
        ) : (
          <div className='rounded-md bg-white p-2 shadow-md'>
            <p className=' text-center'>
              Đã có lỗi xảy ra.
              <button onClick={handleResetField}>Vui lòng làm mới tìm kiếm.</button>
            </p>
          </div>
        )}
      </div>

      <div
        className={classNames('relative', {
          // @ts-ignore
          'mt-[64px]': (queryConfig.page && +queryConfig.pageSize) >= totalListNhaXuanBan
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger
            visible={!!listNhaXuanBan?.length && Number.isInteger(queryConfig.page && +queryConfig.page)}
            value={queryConfig.pageSize}
            total={totalListNhaXuanBan.toString()}
          />
        </div>
      </div>

      <ModalDelete
        open={isVisiable}
        closable={false}
        title={
          <TitleDelete
            firstText='Bạn có chắn chắn muốn xóa nhà xuất bản'
            secondText={selectedDelete?.Ten}
          ></TitleDelete>
        }
        // TODO: Remove inline funtion, integrate api
        handleCancel={() => {
          setSelectedDelete(undefined);
          setIsVisiable(false);
        }}
        handleOk={() => {
          setIsVisiable(false);
          DeleteSingle(selectedDelete?.Id as string);
          setSelectedDelete(undefined);
        }}
      />
      <Loading open={isLoadingDeleteSingle}></Loading>
    </div>
  );
};

export default PublishingCompanyManagermentContent;
