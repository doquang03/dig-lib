import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { statusBooksApis } from 'apis';
import statusBookApis from 'apis/statusBook.apis';
import { AxiosError } from 'axios';
import { Button, Input, Title } from 'components';
import { path } from 'constants/path';
import { useEffect, useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { statusBookSchema } from 'utils/rules';

const initalFormValues: Omit<StatusBook, 'Id' | 'DenBu'> = {
  TenTT: '',
  TrangThai: true
};

const AddStatusBookPage = () => {
  const { id } = useParams();

  const [type, setType] = useState<boolean>(true);

  const navigate = useNavigate();

  const queryClient = useQueryClient();

  const {
    register,
    handleSubmit,
    setValue,
    setError,
    formState: { errors }
  } = useForm<typeof initalFormValues>({
    defaultValues: initalFormValues,
    resolver: yupResolver(statusBookSchema)
  });

  const { data: statusBookData, refetch } = useQuery({
    queryKey: ['statusBook', id],
    queryFn: () => statusBooksApis.getStatusBook(id + ''),
    enabled: !!id,
    onError: () => {
      navigate(path.trangThaiSach);
    }
  });

  const statusBook = useMemo(() => {
    if (!statusBookData?.data.Item) {
      return;
    }

    return statusBookData.data.Item;
  }, [statusBookData?.data.Item]);

  const { mutate: createstatusBook, isLoading: loadingCreatestatusBook } = useMutation({
    mutationFn: statusBookApis.createStatusBook,
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ['statusBooks'] });
      toast.success('Tạo tình trạng sách thành công');

      navigate(path.trangThaiSach);
    },
    onError: (error: AxiosError<ResponseApi<{}>>) => {
      setError('TenTT', { type: 'validate', message: error?.response?.data?.Message });
    }
  });

  const { mutate: updatestatusBook, isLoading: loadingUpdatestatusBook } = useMutation({
    mutationFn: statusBookApis.updateStatusBook,
    onSuccess: () => {
      refetch();
      toast.success('Cập nhật tình trạng sách thành công');
    }
  });

  useEffect(() => {
    if (Boolean(id) && statusBook) {
      setValue('TenTT', statusBook?.TenTT + '');
      setType(statusBook?.TrangThai);
    }
  }, [statusBook?.TenTT, statusBook?.TrangThai, id, setValue, statusBook]);

  const onSubmit = handleSubmit((data) => {
    let name = data.TenTT.replace(/\s+/g, ' ');

    if (!Boolean(id)) {
      !loadingCreatestatusBook && createstatusBook({ ...data, TenTT: name, TrangThai: type });
    } else {
      !loadingUpdatestatusBook &&
        updatestatusBook({
          ...data,
          TenTT: name,
          TrangThai: statusBook?.DenBu ? false : type,
          Id: id + ''
        });
    }
  });

  const handleOnChange = (type: boolean) => () => {
    setType(type);
  };

  const handleBack = () => navigate(path.trangThaiSach);

  return (
    <div className='p-5'>
      <Title title={Boolean(id) ? 'Cập nhật tình trạng sách' : 'Thêm mới tình trạng sách'} />

      <form className='w-1/2 px-3' onSubmit={onSubmit}>
        <label className='mt-3 font-bold'>
          Tên tình trạng sách <span className='text-danger-10'>*</span>
        </label>

        <Input
          placeholder='Nhập tên tình trạng sách'
          name='TenTT'
          register={register}
          errorMessage={errors?.TenTT?.message}
          maxLength={256}
        />

        {!statusBook?.DenBu ? (
          <div>
            <label className='mt-4 font-bold'>Phân loại</label>
            <button className='flex gap-2' onClick={handleOnChange(true)} type='button'>
              <Input type={'radio'} checked={type} onChange={() => handleOnChange(true)} />

              <label>Cho phép mượn sách</label>
            </button>
            <button className='flex gap-2' onClick={handleOnChange(false)} type='button'>
              <Input type={'radio'} checked={!type} onChange={() => handleOnChange(false)} />

              <label>Không cho phép mượn sách</label>
            </button>
          </div>
        ) : (
          <div className='mt-1'>
            <i className='text-primary-30'>
              Lưu ý: tình trạng "Mất" đang được đánh dầu là tình trạng đền bù. Tình trạng này sẽ không được phép chỉnh
              sửa thông tin Phân loại!
            </i>
          </div>
        )}

        <div className='mt-3 flex items-center justify-end gap-2'>
          <Button variant='secondary' onClick={handleBack} type='button'>
            Quay về
          </Button>

          <Button variant='default' type='submit' disabled={loadingCreatestatusBook || loadingUpdatestatusBook}>
            {Boolean(id) ? 'Cập nhật' : 'Thêm mới'}
          </Button>
        </div>
      </form>
    </div>
  );
};

export default AddStatusBookPage;
