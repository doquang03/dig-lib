import { useQuery } from '@tanstack/react-query';
import { studentApis } from 'apis';
import { useMemo } from 'react';
import { toast } from 'react-toastify';

const useOptionsFilterData = () => {
  const { data, isLoading, error } = useQuery({
    queryKey: ['filterOptions'],
    queryFn: studentApis.getOptionsFilterData,
    staleTime: Infinity,
    onError: (err: Error) => {
      toast.error(err.message);
    }
  });

  const status = useMemo(() => {
    if (!data?.data.Item.ListTrangThai) return;

    return data?.data.Item.ListTrangThai.map(({ label, value }) => {
      return {
        label: label,
        value: value === -1 ? '' : value
      };
    });
  }, [data?.data.Item.ListTrangThai]);

  return { schoolYears: data?.data.Item.ListNienKhoa, status, genders: data?.data.Item.ListGioiTinh, isLoading, error };
};

export default useOptionsFilterData;
