import { RedoOutlined } from '@ant-design/icons';
import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation } from '@tanstack/react-query';
import { Modal, type ModalProps } from 'antd';
import { authorApis } from 'apis';
import type { AxiosError } from 'axios';
import { Button, Input } from 'components';
import { memo, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import { authorSchema } from 'utils/rules';
import { GenerateVietnameseAuthorCode } from 'utils/utils';
import * as yup from 'yup';

type AuthorForm = yup.InferType<typeof authorSchema>;

const initialAuthorValue = {
  TenTacGia: '',
  MaTacGia: '',
  QuocTich: '',
  MoTa: ''
};

type Props = {
  onBack: VoidFunction;
  onSuccess: (data: string, id: string) => void;
} & ModalProps;

const AuthorModal = (props: Props) => {
  const { title, onBack, onSuccess, ...rest } = props;

  const [currentSchema, setCurrentSchema] = useState(authorSchema);
  const [duplicateAuthor, setDuplicateAuthor] = useState<Array<string>>([]);
  const [isTrigger, setIsTrigger] = useState('idle');

  const {
    register,
    handleSubmit,
    setValue,
    getValues,
    reset,
    trigger,
    formState: { errors }
  } = useForm<AuthorForm>({
    defaultValues: initialAuthorValue,
    resolver: yupResolver(currentSchema)
  });

  const { mutate: handleCreate, isLoading: isLoadingCreate } = useMutation({
    mutationFn: (payload: object) => authorApis.Create(payload),
    onSuccess: (data) => {
      toast.success('Thêm tác giả thành công');

      onSuccess(getValues('TenTacGia'), data.data.Item.id);
      reset();
      onBack();
    },
    onError: (
      ex: AxiosError<
        ResponseApi<{
          MaTacGia: string;
        }>
      >
    ) => {
      if (ex?.response?.data?.ExMessage === 'MaTacGia') {
        let tempAuthor = duplicateAuthor.concat([ex?.response?.data?.Item?.MaTacGia]);
        setDuplicateAuthor(tempAuthor);
        let authorSchemaClone = authorSchema.clone();
        let authorSchemaObject = yup.object({
          MaTacGia: yup
            .string()
            .trim()
            .notOneOf(tempAuthor, ex?.response?.data?.Message)
            .required('Mã tác giả không được để trống')
        });
        authorSchemaClone = authorSchemaClone.concat(authorSchemaObject);
        setCurrentSchema(authorSchemaClone);
        setIsTrigger('MaTacGia');
      }
    }
  });

  useEffect(() => {
    if (isTrigger === 'MaTacGia') {
      trigger(isTrigger);
      setIsTrigger('idle');
    }
  }, [isTrigger, trigger]);

  const onSubmit = handleSubmit((data) => {
    data.MaTacGia = data.MaTacGia.replace(/ +(?= )/g, '');
    data.MaTacGia = data.MaTacGia.toLocaleUpperCase();
    data.TenTacGia.replace(/\s+/g, ' ');
    setValue('MaTacGia', data.MaTacGia);

    handleCreate(data);
  });

  const handleGetCode = () => {
    setValue('MaTacGia', GenerateVietnameseAuthorCode(getValues('TenTacGia')));
  };

  const handleClose = () => {
    reset();
    onBack();
  };

  return (
    <Modal
      title={<h3 className='font-bold uppercase text-primary-10'>{title}</h3>}
      {...rest}
      footer={null}
      closable={false}
    >
      <form onSubmit={onSubmit}>
        <label className='font-bold'>
          Tên tác giả <span className='text-danger-10'>*</span>
        </label>
        <Input
          placeholder='Nhập tên tác giả'
          containerClassName='mb-3'
          name='TenTacGia'
          register={register}
          onKeyUp={handleGetCode}
          errorMessage={errors?.TenTacGia?.message}
          maxLength={256}
        />

        <label className='font-bold'>
          Mã tác giả <span className='text-danger-10'>*</span>
        </label>
        <div className='container-refesh'>
          <Input
            containerClassName='container-refesh-input mb-3'
            placeholder='Nhập mã tác giả'
            name='MaTacGia'
            register={register}
            errorMessage={errors?.MaTacGia?.message}
            maxLength={51}
          />
          <Button type='button' className='container-refesh-button' onClick={handleGetCode}>
            <RedoOutlined rotate={180} />
          </Button>
        </div>

        <label className='font-bold'>Quốc tịch</label>
        <Input
          placeholder='Nhập quốc tịch'
          name='QuocTich'
          register={register}
          errorMessage={errors?.QuocTich?.message}
          maxLength={101}
          containerClassName='mb-3'
        />

        <label className='font-bold'>Mô tả</label>
        <Input
          placeholder='Nhập nội dung mô tả'
          name='MoTa'
          register={register}
          errorMessage={errors?.MoTa?.message}
          maxLength={256}
          containerClassName='mb-3'
        />

        <div className='flex items-center justify-end gap-2'>
          <Button variant='secondary' type='button' onClick={handleClose} disabled={isLoadingCreate}>
            Quay về
          </Button>

          <Button variant='default' disabled={isLoadingCreate}>
            Thêm mới
          </Button>
        </div>
      </form>
    </Modal>
  );
};

export default memo(AuthorModal);
