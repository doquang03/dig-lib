import { VerticalBarChart } from 'components';
import { useMemo } from 'react';

type Props = {
  khoSach?: { [key: string]: string };
  sachCaBietTheoKho?: { [key: string]: number };
};

function NumberOfSUniqueBooks(props: Props) {
  const { khoSach, sachCaBietTheoKho = {} } = props;

  const { labels, data } = useMemo(() => {
    const labels = [];
    const data = [];

    const warehouseIds = [];

    for (const key in khoSach) {
      const element = khoSach[key];

      warehouseIds.push(key);

      labels.push(element);

      data.push({
        label: element,
        value: sachCaBietTheoKho[key] || 0
      });
    }

    return { labels, data };
  }, [khoSach, sachCaBietTheoKho]);

  return (
    <div className='my-3 h-[292px] rounded-xl bg-white p-2 shadow-md'>
      <div className='flex items-center'>
        <h3 className='text-xl font-bold text-primary-10'>THỐNG KÊ SỐ LƯỢNG SÁCH CÁ BIỆT</h3>
      </div>

      <VerticalBarChart
        options={{
          labels: {
            xAxisLabel: 'Kho sách',
            yAxisLabel: 'Số lượng sách cá biệt'
          }
        }}
        labels={labels}
        datasets={data}
      />
    </div>
  );
}

export default NumberOfSUniqueBooks;
