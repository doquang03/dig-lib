import { Outlet } from 'react-router-dom';

const RentBackBookManagementPage = () => {
  return <Outlet />;
};

export default RentBackBookManagementPage;
