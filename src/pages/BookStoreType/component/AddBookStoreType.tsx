import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation } from '@tanstack/react-query';
import { Breadcrumb, Modal, ModalProps } from 'antd';
import type { ItemType } from 'antd/es/breadcrumb/Breadcrumb';
import { bookstoreTypeApis } from 'apis';
import { AxiosError } from 'axios';
import { Button, Input, Select } from 'components';
import { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { NavLink } from 'react-router-dom';
import { toast } from 'react-toastify';
import { bookstoreTypeSchema } from 'utils/rules';
import * as yup from 'yup';

type Props = {
  bookStoreType?: BookStoreType;
  onSuccess: VoidFunction;
  onHide: VoidFunction;
  IdParent?: string;
  routes: ItemType[];
  isAdd: boolean;
} & ModalProps;

type TypeBookStoreTypeForm = yup.InferType<typeof bookstoreTypeSchema>;

const AddBookStoreType = (props: Props) => {
  const { open, bookStoreType, IdParent, routes, isAdd, onSuccess, onHide } = props;

  const {
    register,
    setValue,
    handleSubmit,
    setError,
    reset,
    formState: { errors }
  } = useForm<TypeBookStoreTypeForm>({
    defaultValues: {
      Ten: '',
      MaKho: '',
      CachTangMaCB: ''
    },
    resolver: yupResolver(bookstoreTypeSchema)
  });

  const { mutate: createBookStore, isLoading: isLoadingCreateBookStore } = useMutation({
    mutationFn: bookstoreTypeApis.createBookStoreSType,
    onSuccess: (data) => {
      reset();
      onSuccess();
      if (!bookStoreType) toast.success('Thêm kho sách thành công');
      else toast.success('Cập nhật kho sách thành công');
    },
    onError: (error: AxiosError<ResponseApi>) => {
      setError('MaKho', { type: 'validate', message: error.response?.data.Message }, { shouldFocus: true });
    }
  });

  useEffect(() => {
    if (!bookStoreType) return;

    if (bookStoreType && isAdd) {
      return setValue('CachTangMaCB', bookStoreType?.CachTangMaCB + '' || '0');
    }

    setValue('CachTangMaCB', bookStoreType?.CachTangMaCB + '' || '0');
    setValue('MaKho', bookStoreType.MaKho);
    setValue('Ten', bookStoreType.Ten);
  }, [bookStoreType, isAdd, setValue]);

  function itemRender(route: ItemType, params: any, routes: ItemType[], paths: string[]) {
    const last = routes.indexOf(route) === routes.length - 1;
    return last ? (
      <span>{route.breadcrumbName}</span>
    ) : (
      <NavLink to={paths[paths.length - 1]}>{route.breadcrumbName}</NavLink>
    );
  }

  const onSubmit = handleSubmit((data) => {
    data.Ten = data.Ten.trim().replace(/\s\s+/g, ' ');
    data.MaKho = data.MaKho.trim().replace(/\s\s+/g, ' ');

    if (isAdd) {
      createBookStore({
        Ten: data.Ten.replace(/\s\s+/g, ' '),
        MaKho: data.MaKho.replace(/\s\s+/g, ' '),
        CachTangMaCB: data.CachTangMaCB || '0',
        IdParent: IdParent || ''
      });
    } else if (bookStoreType) {
      createBookStore({
        Id: bookStoreType?.Id,
        Ten: data.Ten.replace(/\s\s+/g, ' '),
        MaKho: data.MaKho.replace(/\s\s+/g, ' '),
        CachTangMaCB: data.CachTangMaCB || '0',
        IdParent: bookStoreType?.IdParent,
        IsEdit: true
      });
    }
  });

  const handleBack = () => {
    onHide();
    reset();
  };

  return (
    <Modal
      open={open}
      title={
        <h3 className='font-semibold uppercase text-primary-10'>
          {Boolean(bookStoreType) && !isAdd ? 'Cập nhật kho sách' : 'Thêm kho sách'}
        </h3>
      }
      footer={null}
      closable={false}
    >
      <Breadcrumb
        className='rounded-sm bg-secondary-30 p-2 font-semibold text-secondary-20'
        separator='>'
        items={routes}
        itemRender={itemRender}
      />
      <div className='mt-3'>
        <label className='font-bold'>
          Tên kho sách <span className='text-danger-10'>*</span>{' '}
        </label>

        <Input placeholder='Nhập tên kho sách' register={register} name='Ten' errorMessage={errors.Ten?.message} />
      </div>
      <div className='my-3'>
        <label className='font-bold'>
          Mã kho sách <span className='text-danger-10'>*</span>{' '}
        </label>

        <Input placeholder='Nhập mã kho sách' register={register} name='MaKho' errorMessage={errors.MaKho?.message} />
      </div>{' '}
      <div>
        <label className='font-bold'>
          Cách tăng mã cá biệt <span className='text-danger-10'>*</span>{' '}
        </label>

        <Select
          register={register}
          name='CachTangMaCB'
          className='w-full'
          errorMessage={errors.CachTangMaCB?.message}
          items={[
            { value: '', label: 'Chọn cách tăng mã cá biệt' },
            { value: 3, label: 'Theo mã kho sách' },
            { value: 2, label: 'Theo nhan đề sách' },
            { value: 1, label: 'Theo tổng số sách' }
          ]}
          disabled={Boolean(IdParent)}
        />
      </div>
      <div className='mt-3 flex items-center justify-end gap-2'>
        <Button
          variant='secondary'
          disabled={isLoadingCreateBookStore}
          loading={isLoadingCreateBookStore}
          onClick={handleBack}
        >
          Quay về
        </Button>

        <Button onClick={onSubmit} disabled={isLoadingCreateBookStore} loading={isLoadingCreateBookStore}>
          {Boolean(bookStoreType) && !isAdd ? 'Cập nhật' : 'Thêm mới'}
        </Button>
      </div>
    </Modal>
  );
};

export default AddBookStoreType;
