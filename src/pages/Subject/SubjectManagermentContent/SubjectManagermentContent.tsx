import { DeleteFilled, EditOutlined } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Tooltip } from 'antd';
import Table, { type ColumnsType } from 'antd/es/table';
import { subjectApis } from 'apis';
import classNames from 'classnames';
import { Button, Empty, Input, Loading, ModalDelete, Select, SizeChanger, Title, TitleDelete } from 'components';
import { OPTIONS_BY_TYPESCHOOL } from 'constants/options';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import { usePaginationNavigate, useQueryConfig } from 'hooks';
import { isEmpty, omitBy, trim } from 'lodash';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import { createSearchParams, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getSerialNumber } from 'utils/utils';

// TODO: Delete as soon as api implement
type Mock = {
  Id: string;
  Ten: string;
  MaMonHoc: string;
  KhoiLop: Array<string>;
};

type FormInput = {
  TextForSearch: string;
  KhoiLop: string;
};

const SubjectManagermentContent = () => {
  const { register, handleSubmit, reset, setValue } = useForm<FormInput>({
    defaultValues: {
      TextForSearch: '',
      KhoiLop: ''
    }
  });

  const navigate = useNavigate();

  const handleNavigation = usePaginationNavigate();

  const { TypeSchool, isAllowedAdjustment } = useUser();

  const grade_options = useMemo(() => {
    const array: { label: string; value: string }[] = [
      {
        label: 'Chọn khối lớp',
        value: ''
      }
    ];
    if (TypeSchool) {
      for (let param in OPTIONS_BY_TYPESCHOOL) {
        if (TypeSchool.includes(param)) {
          OPTIONS_BY_TYPESCHOOL[param].forEach((element) => {
            array.push(element);
          });
        }
      }
    }
    return array;
  }, [TypeSchool]);

  const queryConfig = useQueryConfig();
  const [totalListSubject, setTotalListSubject] = useState(0);
  const [selectedDelete, setSelectedDelete] = useState<Mock>();
  const [isVisiable, setIsVisiable] = useState(false);

  const {
    data: dataSubject,
    refetch,
    isFetching
  } = useQuery({
    queryKey: ['Subject', queryConfig],
    queryFn: () => {
      let page = 1;
      if (typeof queryConfig.page === 'string') page = parseInt(queryConfig.page);
      let queryString = `?page=${page}&sizeNumber=${queryConfig.pageSize}`;
      if (queryConfig.TextForSearch !== undefined) queryString += `&TextForSearch=${queryConfig.TextForSearch}`;
      if (queryConfig.KhoiLop !== undefined) queryString += `&KhoiLop=${queryConfig.KhoiLop}`;
      let request = subjectApis.Index(queryString);
      return request;
    }
  });

  const listSubject = useMemo(() => {
    if (!dataSubject?.data?.Item) return;
    const { ListMonHoc, count } = dataSubject?.data?.Item;
    setTotalListSubject(count);
    return ListMonHoc;
  }, [dataSubject?.data?.Item]);

  const { mutate: handleDelete, isLoading: isLoadingDelete } = useMutation({
    mutationFn: (Id: string) => subjectApis.Delete(Id),
    onSuccess: () => {
      refetch();
      toast.success('Xóa môn học thành công');
    }
  });

  const onPaginate = useCallback(
    (page: number, pageSize: number) => {
      handleNavigation({ ...queryConfig, page: page + '', pageSize: pageSize + '' });
    },
    [handleNavigation, queryConfig]
  );

  const pagination = useMemo(() => {
    return {
      current: (!!queryConfig.page && +queryConfig.page) || 1,
      pageSize: (!!queryConfig.pageSize && +queryConfig.pageSize) || 30,
      total: totalListSubject,
      onChange: onPaginate,
      hideOnSinglePage: true,
      showSizeChanger: false,
      showQuickJumper: true,
      locale: { jump_to: '', page: '' }
    };
  }, [onPaginate, queryConfig.page, queryConfig.pageSize, totalListSubject]);

  const columns = useMemo(() => {
    const _columns: ColumnsType<Mock> = [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        className: 'min-content',
        render: (value, record, index) => {
          return (
            queryConfig.page && queryConfig.pageSize && getSerialNumber(+queryConfig.page, +queryConfig.pageSize, index)
          );
        },
        width: 100
      },
      {
        title: 'Tên môn học',
        dataIndex: 'Ten',
        key: 'Ten',
        render: (value, { Ten }) => (
          <Tooltip title={Ten} arrow={true} className='truncate'>
            <p>{Ten}</p>
          </Tooltip>
        ),
        onCell: (record) => ({
          className: 'text-left'
        }),
        ellipsis: true
      },
      {
        title: 'Mã môn học',
        dataIndex: 'MaMonHoc',
        key: 'MaMonHoc',
        width: 200
      },
      {
        title: 'Khối lớp',
        dataIndex: 'KhoiLop',
        key: 'KhoiLop',
        render: (value, record) => {
          const final = 'Khối ' + record?.KhoiLop?.join('-Khối ');
          return final;
        }
      }
    ];

    if (isAllowedAdjustment) {
      _columns.push({
        title: 'Hành động',
        dataIndex: 'actions',
        key: 'actions',
        className: 'min-content',
        width: 150,
        render: (value, record) => {
          return (
            <>
              <Tooltip title='Chỉnh sửa'>
                <button
                  className='mx-2'
                  onClick={(e) => {
                    e.stopPropagation();
                    navigate(`CapNhat/${record.Id}`);
                  }}
                >
                  <EditOutlined style={{ fontSize: '20px', color: '#08c' }} />
                </button>
              </Tooltip>

              <Tooltip title='Xóa'>
                <button
                  className='mx-2'
                  onClick={(e) => {
                    e.stopPropagation();
                    setSelectedDelete(() => {
                      return record;
                    });
                    setIsVisiable(true);
                  }}
                >
                  <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
                </button>
              </Tooltip>
            </>
          );
        }
      });
    }

    return _columns;
  }, [isAllowedAdjustment, navigate, queryConfig.page, queryConfig.pageSize]);

  useEffect(() => {
    Object.keys(queryConfig).forEach((key) => {
      if (!key) {
        return;
      }
      // @ts-ignore
      setValue(key, queryConfig[key]);
    });
  }, [queryConfig, setValue]);

  const onSubmit = handleSubmit((data) => {
    handleNavigation({
      ...omitBy({ ...data, TextForSearch: trim(data.TextForSearch, ' ').replace(/\s\s+/g, ' ') }, isEmpty),
      page: '1'
    });
  });

  const handleResetField = () => {
    const { pathname } = window.location;

    navigate({
      pathname: pathname,
      search: createSearchParams({}).toString()
    });

    reset();
  };

  // TODO: Do something with selected Subjects
  const handleSelectedSubjects = () => {
    navigate(path.themmonhoc);
  };

  let locale = {
    emptyText: 'Không tìm được kết quả phù hợp'
  };

  return (
    <div className='p-5'>
      <Title title='DANH SÁCH môn học' />

      <form className='my-5 flex flex-col flex-wrap gap-2 bg-secondary-10 py-3 px-3.5 md:flex-row' onSubmit={onSubmit}>
        <Input
          placeholder='Nhập môn học'
          containerClassName='w-[100%] md:w-[460px] 2xl:w-[520px] xl:w-[360px]'
          name='TextForSearch'
          register={register}
        />

        <Select items={grade_options} className='w-[100%] md:w-[460px]' name='KhoiLop' register={register} />

        <Button type='button' variant='secondary' className='font-semibold' onClick={handleResetField}>
          Làm mới
        </Button>

        <Button type='submit' variant='default' className='font-semibold'>
          Tìm kiếm
        </Button>
      </form>

      <Button
        className={classNames('w-[100%] md:w-auto', {
          hidden: !isAllowedAdjustment,
          'w-[100%] md:w-auto': isAllowedAdjustment
        })}
        onClick={handleSelectedSubjects}
      >
        Thêm môn học
      </Button>

      <div className='mt-6'>
        <Table
          loading={isFetching && !listSubject?.length}
          columns={columns}
          dataSource={listSubject || []}
          pagination={pagination}
          scroll={{ x: 980 }}
          rowKey={(record) => record.Id}
          className='custom-table'
          locale={{
            emptyText: () => <Empty label={locale.emptyText} />
          }}
          bordered
        />
      </div>

      <div
        className={classNames('relative', {
          // @ts-ignore
          'mt-[64px]': (queryConfig.page && +queryConfig.pageSize) >= totalListSubject
        })}
      >
        <div className='absolute bottom-1'>
          <SizeChanger
            visible={!!listSubject?.length}
            value={queryConfig.pageSize}
            total={totalListSubject.toString()}
          />
        </div>
      </div>

      <ModalDelete
        open={isVisiable}
        closable={false}
        title={
          <TitleDelete firstText='Bạn có chắn chắn muốn xóa môn học' secondText={selectedDelete?.Ten}></TitleDelete>
        }
        // TODO: Remove inline funtion, integrate api
        handleCancel={() => {
          setSelectedDelete(undefined);
          setIsVisiable(false);
        }}
        handleOk={() => {
          setIsVisiable(false);
          handleDelete(selectedDelete?.Id as string);
          setSelectedDelete(undefined);
        }}
      />
      <Loading open={isLoadingDelete}></Loading>
    </div>
  );
};

export default SubjectManagermentContent;
