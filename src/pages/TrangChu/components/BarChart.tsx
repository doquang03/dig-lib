import { Tooltip } from 'antd';
import { Empty } from 'components';
import { useEffect, useMemo, useRef, useState } from 'react';

type Props = {
  title?: string;
  data: { label: string; value: any }[];
  options?: {
    labels: {
      xAxisLabel?: string;
      yAxisLabel?: string;
    };
  };
};

const BarChart = (props: Props) => {
  const { title, data, options } = props;
  const { labels } = options || {};

  const [labelWidth, setLabelWidth] = useState<number>(0);
  const [parentWidth, setParentWidth] = useState<number>(0);
  let ref = useRef<HTMLDivElement>(null);
  const refParent = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (!ref.current) return;

    setLabelWidth(ref.current.offsetWidth);
  }, [ref?.current?.offsetWidth, data]);

  useEffect(() => {
    if (!refParent.current) return;

    setParentWidth(refParent.current.offsetWidth);
  }, [refParent.current?.offsetWidth]);

  // 12 * 2: gap of 2 bar chart
  // 16: padding of box
  const res = parentWidth - labelWidth - 12 * 2 - 16;

  const { rangeX = 1, temp } = useMemo(() => {
    const maxValue = data.length ? Math.max(...data?.map(({ value }) => value)) : 0;

    let temp = '1';

    [...Array(maxValue.toString().length - 1)].forEach((_) => (temp += '0'));
    const rangeX = Math.ceil(maxValue / Number(temp)) + 1 || 0;

    return { rangeX, maxValue, temp };
  }, [data]);

  return (
    <div className='rounded-md bg-white p-4 pb-7 shadow-sm' ref={refParent}>
      {title ? <h3 className='mb-3 font-bold uppercase text-primary-10'>{title}</h3> : null}

      {!data.length ? (
        <div className='grid h-40 place-items-center' ref={ref}>
          <Empty />
        </div>
      ) : (
        <div className='relative grid grid-cols-12'>
          <div className='col-span-3 flex flex-col gap-y-5' ref={ref}>
            {labels?.yAxisLabel ? (
              <span className='absolute top-1/2 -left-[1.65rem] -rotate-90 text-xs font-bold'>{labels.yAxisLabel}</span>
            ) : null}

            {data.map(({ label }) => (
              <p key={label} className={`truncate whitespace-nowrap ${!!labels?.yAxisLabel ? 'ml-4' : 'mr-0'}`}>
                {label}
              </p>
            ))}
          </div>

          <div className='absolute left-2 col-span-9 col-start-4'>
            {data.map(({ label, value }, index) => {
              return (
                <Tooltip title={`${label} - ${value}`} key={label}>
                  <div
                    key={label}
                    className='h-[21px] rounded bg-gradient-to-r from-[#005FA8] to-blue-500/80 shadow-md'
                    style={{
                      width: (value * (res / rangeX)) / Number(temp),
                      top: index * (20 + 21),
                      position: 'absolute',
                      zIndex: 10
                    }}
                  />
                </Tooltip>
              );
            })}
          </div>

          <div className={`absolute bottom-0 col-span-9 col-start-4 w-auto`}>
            {[...Array(rangeX)]?.map((_, index) => (
              <div
                key={index}
                style={{
                  left: index * (res / rangeX),
                  position: 'absolute',
                  fontSize: 12
                }}
              >
                <div
                  className={`absolute bottom-6 left-2 border-l-[1px]`}
                  style={{
                    height: data.length * 21 + data.length * 20 - 20
                  }}
                ></div>

                {index * Number(temp)}
              </div>
            ))}
          </div>

          {labels?.xAxisLabel ? (
            <span className='absolute right-0 -bottom-5 whitespace-nowrap text-xs font-bold'>{labels.xAxisLabel}</span>
          ) : null}
        </div>
      )}
    </div>
  );
};

export default BarChart;
