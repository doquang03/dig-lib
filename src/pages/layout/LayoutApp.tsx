import {
  BarChartOutlined,
  BookOutlined,
  ClockCircleOutlined,
  ContactsOutlined,
  HomeFilled,
  LeftCircleFilled,
  RightCircleFilled,
  SettingFilled,
  TeamOutlined,
  UnorderedListOutlined
} from '@ant-design/icons';
import { Divider, Layout, Menu, theme } from 'antd';
import { CoverBookIcon, DigitalIcon } from 'assets';
import { SizeCollapse } from 'constants/config';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import 'css/LayoutApp.css';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { getUnitFromSS } from 'utils/utils';
import { Footer } from './Footer';
import { Header } from './Header';
import { ItemType, MenuItemType } from 'antd/es/menu/hooks/useItems';

const LayoutApp = (props: { children: React.ReactNode }) => {
  const {
    token: { colorBgContainer }
  } = theme.useToken();

  const { userType, isAllowedAdjustment, permission } = useUser();

  const unit = getUnitFromSS(Number(userType));

  const menuMembers = [
    {
      key: 'hocsinh',
      label: <Link to={path.hocsinh}>Học sinh</Link>
    },
    {
      key: 'giaovien',
      label: <Link to={path.giaovien}>Giáo viên</Link>
    },
    {
      key: 'canbothuvien',
      label: <Link to={path.canbothuvien}>Cán bộ thư viện</Link>
    }
  ];

  if (permission?.EnableDigital) {
    menuMembers.push({
      key: 'bandocltv',
      label: <Link to={path.bandocltv}>Bạn đọc LTV</Link>
    });
  }

  const schoolMenuItems: ItemType<MenuItemType>[] = [
    {
      key: 'library',
      icon: <HomeFilled />,
      label: 'Thư viện',
      children: [
        {
          key: 'home',
          label: <Link to={path.home}>Trang chủ</Link>
        },
        {
          key: 'noiquythuvien',
          label: <Link to={path.noiquythuvien}>Nội quy thư viện</Link>
        }
      ]
    },
    {
      key: 'manager',
      icon: <TeamOutlined />,
      label: 'Quản lý thành viên',
      children: menuMembers
    },
    {
      key: 'managerbook',
      icon: <UnorderedListOutlined />,
      label: 'Quản lý thông tin',
      children: [
        {
          key: 'monhoc',
          label: <Link to={path.monhoc}>Môn học</Link>
        },
        {
          key: 'chudiem',
          label: <Link to={path.chudiem}>Chủ điểm</Link>
        },
        {
          key: 'divider-menu',
          className: 'divider-menu',
          label: <Divider className='my-1' />
        },
        {
          key: 'bosuutap',
          label: <Link to={path.bosuutap}>Bộ sưu tập</Link>
        },
        {
          key: 'tacgia',
          label: <Link to={path.tacgia}>Tác giả</Link>
        },
        {
          key: 'nhaxuatban',
          label: <Link to={path.nhaxuatban}>Nhà xuất bản</Link>
        },
        {
          key: 'noixuatban',
          label: <Link to={path.noixuatban}>Nơi xuất bản</Link>
        },
        {
          key: 'ngonngu',
          label: <Link to={path.ngonngu}>Ngôn ngữ</Link>
        },
        {
          key: 'kesach',
          label: <Link to={path.kesach}>Kệ sách</Link>
        },
        {
          key: 'trangThaiSach',
          label: <Link to={path.trangThaiSach}>Tình trạng sách</Link>
        },
        {
          key: 'nguoncungcap',
          label: <Link to={path.nguoncungcap}>Nguồn cung cấp</Link>
        },
        {
          key: 'ddc',
          label: <Link to={path.ddc}>DDC</Link>
        },
        {
          key: 'day19',
          label: <Link to={path.day19}>Phân loại 19 Dãy</Link>
        },
        {
          key: 'mamau',
          label: <Link to={path.mamau}>Mã màu</Link>
        }
      ]
    },
    {
      key: 'information',
      label: 'Thông tin sách',
      icon: React.createElement(BookOutlined),
      children: [
        {
          key: 'sach',
          label: <Link to={`${path.sach}?page=1&pageSize=24`}>Sách</Link>
        },
        {
          key: 'thuMucTaiLieu',
          label: <Link to={path.thuMucTaiLieu}>Thư mục tài liệu</Link>
        }
      ]
    },
    {
      key: 'bookstore',
      label: 'Kho sách',
      icon: <CoverBookIcon />,
      children: [
        {
          key: 'khosach',
          label: <Link to={path.khosach}>Kho sách</Link>
        },
        {
          key: 'loaikho',
          label: <Link to={path.loaikho}>Loại kho sách</Link>
        },
        {
          key: 'quanlynhapkho',
          label: <Link to={path.quanlynhapkho}>Quản lý nhập kho</Link>
        },
        {
          key: 'quanlyxuatkho',
          label: <Link to={path.quanlyxuatkho}>Quản lý xuất kho</Link>
        }
      ]
    },
    {
      key: 'newsPaperMagazine',
      label: 'Báo, tạp chí',
      icon: <CoverBookIcon />,
      children: [
        {
          key: 'newspaperMagazineListing',
          label: <Link to={path.newspaperMagazineListing}>Báo, tạp chí</Link>
        },
        {
          key: 'registerNewspaperMagazine',
          label: <Link to={path.registerNewspaperMagazine}>Đăng ký báo, tạp chí</Link>
        }
      ]
    },
    {
      key: 'rentback',
      label: 'Lưu thông',
      icon: React.createElement(ContactsOutlined),
      children: [
        { key: 'muonTraSach', label: <Link to={path.muonTraSach}>Mượn trả sách</Link> },
        { key: 'muonTrongNgay', label: <Link to={path.muonTrongNgay}>Đọc sách tại chỗ</Link> },
        {
          key: 'danhSachPhieuMuon',
          label: <Link to={path.danhSachPhieuMuon}>Danh sách phiếu mượn</Link>
        },
        { key: 'danhSachDatMuon', label: <Link to={path.danhSachDatMuon}>Danh sách đặt mượn</Link> }
      ]
    },
    {
      key: 'analysis',
      label: 'Thống kê',
      icon: React.createElement(BarChartOutlined),
      children: [
        {
          key: 'thongke',
          label: <Link to={path.thongke}>Biểu đồ thống kê</Link>
        },
        {
          key: 'thongkemuonsach',
          label: <Link to={path.thongkemuonsach}>Danh mục mượn</Link>
        },
        {
          key: 'thongkedanhsachdatmuon',
          label: <Link to={path.thongkedanhsachdatmuon}>Danh mục đặt mượn</Link>
        },
        {
          key: 'thongkebandoc',
          label: <Link to={path.thongkebandoc}>Thống kê bạn đọc</Link>
        },
        {
          key: 'thongkesoluonganpham',
          label: <Link to={path.thongkesoluonganpham}>Số lượng ấn phẩm</Link>
        },
        {
          key: 'thongkekiemketaisan',
          label: <Link to={path.thongkekiemketaisan}>Kiểm kê tài sản</Link>
        },
        {
          key: 'sotheodoikinhphi',
          label: <Link to={path.sotheodoikinhphi}>Sổ theo dõi kinh phí</Link>
        },
        {
          key: 'thongkexuatbaocao',
          label: <Link to={path.thongkexuatbaocao}>Xuất báo cáo</Link>
        },
        {
          key: 'thongkexuatsach',
          label: <Link to={path.thongkexuatsach}>Thống kê thanh lý</Link>
        }
      ]
    },
    {
      key: 'setting',
      label: 'Cài đặt',
      icon: <SettingFilled />,
      children: [
        {
          key: 'quanlythuvien',
          label: <Link to={path.quanlythuvien}>Tổng quan</Link>
        },
        {
          key: 'quanlythuviencsdl',
          label: <Link to={path.quanlythuviencsdl}>CSDL</Link>
        },
        {
          key: 'hoTro',
          label: <Link to={path.hoTro}>Hỗ trợ</Link>
        }
      ]
    }
  ];

  if (permission?.EnableDigital) {
    schoolMenuItems.push({
      key: 'connectionManagementPage',
      label: 'Thư viện số',
      icon: <DigitalIcon />,
      children: permission.Enable
        ? [
            {
              key: 'ebook',
              label: (
                <Link to={path.ebook} state={{ hello: 'hello' }}>
                  Sách điện tử
                </Link>
              )
            },
            {
              key: 'imagesAlbum',
              label: (
                <Link to={path.imagesAlbum} state={{ hello: 'hi' }}>
                  Album ảnh
                </Link>
              )
            },
            {
              key: 'audio',
              label: <Link to={path.audio}>Sách nói</Link>
            },
            {
              key: 'video',
              label: <Link to={path.video}>Video</Link>
            },
            {
              key: 'divider-menu',
              className: 'divider-menu',
              label: <Divider className='my-1' />
            },
            {
              key: 'connectionManagement',
              label: <Link to={path.connectionManagement}>Quản lý kết nối</Link>
            },
            { key: 'titleApproval', label: <Link to={path.titleApproval}>Phê duyệt bản ghi</Link> },
            {
              key: 'connection',
              label: <Link to={path.connection}>Kết nối</Link>
            },
            {
              key: 'pullRequest',
              label: <Link to={path.pullRequest}>Đồng bộ dữ liệu MLLH</Link>
            },
            { key: 'errorSyncDocument', label: <Link to={path.errorSyncDocument}>Lỗi đồng bộ thư viện</Link> }
          ]
        : [
            {
              key: 'ebook',
              label: <Link to={path.ebook}>Sách điện tử</Link>
            },
            {
              key: 'imagesAlbum',
              label: <Link to={path.imagesAlbum}>Album ảnh</Link>
            },
            {
              key: 'audio',
              label: <Link to={path.audio}>Sách nói</Link>
            },
            {
              key: 'video',
              label: <Link to={path.video}>Video</Link>
            },
            // {
            //   key: 'digitalDocument',
            //   label: <Link to={path.digitalDocument}>Tài nguyên số</Link>
            // },
            // {
            //   key: 'media',
            //   label: <Link to={path.media}>Tài nguyên đa phương tiện</Link>
            // },
            {
              key: 'divider-menu',
              className: 'divider-menu',
              label: <Divider className='my-1' />
            },
            {
              key: 'connection',
              label: <Link to={path.connection}>Kết nối</Link>
            },
            {
              key: 'pullRequest',
              label: <Link to={path.pullRequest}>Đồng bộ dữ liệu MLLH</Link>
            },
            { key: 'errorSyncDocument', label: <Link to={path.errorSyncDocument}>Lỗi đồng bộ thư viện</Link> }
          ]
    });
  }

  schoolMenuItems.push({
    key: 'lichsutruycap',
    label: <Link to={path.lichsutruycap}>Lịch sử truy cập</Link>,
    icon: <ClockCircleOutlined />
  });

  schoolMenuItems.push({
    key: 'digitalLibrary',
    label: 'Trang bạn đọc',
    icon: <DigitalIcon />,
    children: [
      {
        key: 'gioiThieu',
        label: <Link to={path.gioiThieu}>Giới thiệu</Link>
      },
      {
        key: 'thongBao',
        label: <Link to={path.thongBao}>Thông báo</Link>
      },
      {
        key: 'huongDanSuDung',
        label: <Link to={path.huongDanSuDung}>Hướng dẫn sử dụng</Link>
      }
    ]
  });

  const menuData = [];
  if (userType === 3000 || permission?.EnableDigital) {
    menuData.push({
      key: 'home',
      label: <Link to={path.home}>Trang chủ</Link>
    });
  }

  menuData.push({
    key: 'listOfUnits',
    label: <Link to={path.listOfUnits}>Đơn vị con</Link>
  });

  const educationDepartmentMenuItems = [
    {
      key: 'library',
      icon: <HomeFilled />,
      label: 'Thư viện',
      children: menuData
    },
    {
      key: 'lichsutruycap',
      icon: <ClockCircleOutlined />,
      label: <Link to={path.lichsutruycap}>Lịch sử</Link>
    }
  ];

  if (permission?.EnableDigital) {
    educationDepartmentMenuItems.push({
      key: 'digitalLibrary',
      label: 'Thư viện số',
      icon: <DigitalIcon />,
      children: [
        {
          key: 'connectionManagement',
          label: <Link to={path.connectionManagement}>Quản lý kết nối</Link>
        },
        { key: 'hostLibraryManagement', label: <Link to={path.hostLibraryManagement}>Cài đặt</Link> },
        { key: 'titleApproval', label: <Link to={path.titleApproval}>Phê duyệt bản ghi</Link> },
        { key: 'taiLieuMLLHL', label: <Link to={path.hostDocument}>Tài liệu MLLH</Link> },
        { key: 'folderManagement', label: <Link to={path.folderManagement}>Quản lý thư mục</Link> }
      ]
    });
  }

  let currentTag: string = 'home';
  let start: number = 0;
  let end: number = window.location.pathname.length;
  let tempData = window.location.pathname.slice(start, end);
  if (tempData.lastIndexOf('/') !== -1) end = tempData.lastIndexOf('/');
  if (tempData !== '/QuanLyThuVien/CSDL') tempData = window.location.pathname.slice(start, end);

  for (let param in path) {
    if (tempData.indexOf(path[param as keyof typeof path]) !== -1) {
      currentTag = param;
    }
  }

  tempData = window.location.pathname.slice(start, window.location.pathname.length);
  if (currentTag === 'home') {
    for (let param in path) {
      if (tempData.indexOf(path[param as keyof typeof path]) !== -1) currentTag = param;
    }
  }

  let icon = React.createElement(LeftCircleFilled);

  let isCheckCollape = false;
  if (window.innerWidth <= SizeCollapse) {
    icon = React.createElement(RightCircleFilled);
    isCheckCollape = true;
  }
  const [stateCollape, setCollape] = useState({ collapsed: isCheckCollape });

  const [stateIconCollapse, setIconCollape] = useState({
    icon: icon
  });

  const [stateMenuSelect, setMenuSelect] = useState(currentTag);

  const menuItems =
    userType === 2001 && Boolean(unit) && !isAllowedAdjustment
      ? schoolMenuItems.filter((item) => item?.key !== 'rentback' && item?.key !== 'setting')
      : (userType === 2001 && Boolean(unit)) || userType === 3000
      ? schoolMenuItems
      : educationDepartmentMenuItems;

  const items = [
    ...menuItems,
    {
      key: 'collapsemenu',
      label: 'Thu gọn trình đơn',
      icon: stateIconCollapse.icon
    }
  ];

  const getSubMenuKey = (props: string) => {
    let result;

    for (let i = 0; i < items.length; i++) {
      result = items[i]?.key;
      // @ts-ignore
      let tempArr = items[i]?.children;
      let length = 0;
      if (tempArr?.length) length = tempArr.length;
      for (let j = 0; j < length; j++) {
        if (tempArr?.[j].key === props) {
          return result;
        }
      }
    }
    return '';
  };

  const [stateSubMenuSelect, setSubMenuSelect] = useState([getSubMenuKey(currentTag)]);
  if (stateMenuSelect !== currentTag) {
    setMenuSelect(currentTag);
    setSubMenuSelect([getSubMenuKey(currentTag)]);
  }
  const choseComandFunction = (props: any) => {
    if (props?.key !== 'collapsemenu') setMenuSelect(props?.key);
    switch (props?.key) {
      case 'collapsemenu': {
        props.domEvent.preventDefault();
        collapseMenu(props);
        break;
      }
    }
  };

  const changeStatusHandle = (prop: any) => {
    setSubMenuSelect([prop[1]]);
  };

  React.useEffect(() => {
    function handleResize() {
      if (isCheckCollape === false && window.innerWidth <= SizeCollapse) changeCollapse();
    }
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  });

  const changeCollapse = () => {
    isCheckCollape = true;
    let columnmain = document.getElementById('ColumnMain');
    columnmain?.classList.add('small-menu');
    setIconCollape({ icon: React.createElement(RightCircleFilled) });
    setCollape({
      collapsed: true
    });
  };
  const changeUnCollapse = () => {
    isCheckCollape = false;
    let columnmain = document.getElementById('ColumnMain');
    columnmain?.classList.remove('small-menu');
    setIconCollape({ icon: React.createElement(LeftCircleFilled) });
    setCollape({
      collapsed: false
    });
  };

  const collapseMenu = (props: any) => {
    if (stateCollape.collapsed === false) {
      changeCollapse();
    } else {
      changeUnCollapse();
    }
  };

  return (
    <Layout className='MainLayout'>
      <Header />

      <Layout className={stateCollape.collapsed ? 'ColumnMainLayout small-menu' : 'ColumnMainLayout'} id='ColumnMain'>
        <div style={{ background: colorBgContainer }} id='MainSilder'>
          <div className='logo print-logo'>
            <a
              href='/'
              title='Trang chủ'
              style={{
                backgroundImage: `${
                  permission?.EnableDigital ? 'url(~/content/logo.png)' : 'url(~/content/logo-classic.png)'
                }`
              }}
            >
              <img
                src={permission?.EnableDigital ? '/content/logo.png' : '/content/logo-classic.png'}
                alt='logo'
                style={{ width: '100%', height: '100%' }}
              ></img>
            </a>
          </div>

          <Menu
            id='MainMenu'
            mode='inline'
            style={{ borderRight: 0, background: '#23282d', color: '#eee' }}
            items={items}
            onOpenChange={changeStatusHandle}
            inlineCollapsed={stateCollape.collapsed}
            onClick={choseComandFunction}
            selectedKeys={[stateMenuSelect]}
            openKeys={stateSubMenuSelect as string[]}
          />
        </div>

        <Layout id='ContentApp'>{props.children}</Layout>
      </Layout>

      <Footer />
    </Layout>
  );
};

export default LayoutApp;
