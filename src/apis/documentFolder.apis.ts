import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/ThuMucSach`;

type GetDocumentsVars = {
  PageSize: number;
  Page: number;
  TextForSearch?: string;
  Type?: string;
};

const documentFolderApis = {
  getDocuments: (payload: GetDocumentsVars) => {
    return httpThuVien.post<
      ResponseApi<{
        ListThuMucSach: DocumentFolder[];
        count: number;
      }>
    >(`/${endPoint}/GetThuMucSach`, payload);
  },
  getDocument: (id: string) => {
    return httpThuVien.get<
      ResponseApi<{
        ThuMucSach: DocumentFolder;
      }>
    >(`/${endPoint}/EditById?id=${id}`);
  },
  // deleteDocument: (payload: DocumentFolder) => httpThuVien.post(`/${endPoint}/DeleteOnClick`, payload),
  deleteDocuments: (ids: string[]) => httpThuVien.post(`/${endPoint}/DeleteMulti`, ids),
  updateDocument: (payload: DocumentFolder) => httpThuVien.post(`/${endPoint}/InsertAndEdit`, payload),
  printDocuments: (payload: DocumentFolder[]) =>
    httpThuVien.post(`/${endPoint}/InSach_ThuMucSach`, payload, { responseType: 'blob' })
};

export default documentFolderApis;
