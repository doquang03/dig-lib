import { path } from 'constants/path';
import { Outlet, useMatch } from 'react-router-dom';
import TeacherManagermentContent from './TeacherManagermentContent/TeacherManagermentContent';

const TeacherManagementPage = () => {
  const match = useMatch(path.giaovien);

  return <>{Boolean(match) ? <TeacherManagermentContent /> : <Outlet />}</>;
};

export default TeacherManagementPage;
