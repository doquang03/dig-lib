import { DeleteFilled, DownloadOutlined } from '@ant-design/icons';
import { useMutation } from '@tanstack/react-query';
import { Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { ddcApis } from 'apis';
import classNames from 'classnames';
import { Button, Empty, Loading, ProgressBar, Title } from 'components';
import { path } from 'constants/path';
import { uniqueId } from 'lodash';
import { useMemo, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';

let locale = {
  emptyText: 'Không có dữ liệu!'
};

const AddDDCByExcelPage = () => {
  const [step, setStep] = useState<number>(1);
  const [link, setLink] = useState<string>('');
  const [rowPreviewData, setPreviewRowData] = useState<UploadFileData['RawDataList']>([[]]);
  const [rowSavedData, setRowSavedData] = useState<Pick<UploadFileData<DDC>, 'ListFail' | 'ListSuccess' | 'ListShow'>>({
    ListFail: [],
    ListSuccess: [],
    ListShow: []
  });
  const [page, setPage] = useState(1);
  const [paginationSize, setPaginationSize] = useState(10);

  const fileInutRef = useRef<HTMLInputElement | null>(null);
  const ref = useRef<HTMLDivElement | null>(null);

  const navigate = useNavigate();

  const { mutate: UploadFileData, isLoading: uploadingExcelFile } = useMutation({
    mutationFn: ddcApis.uploadFileData,
    onSuccess: (data) => {
      setPreviewRowData(data.data.RawDataList);
      setStep(2);
    }
  });

  const { mutate: saveExcelFile, isLoading: savingExcelFile } = useMutation({
    mutationFn: ddcApis.saveExcelFile,
    onSuccess: (data) => {
      ref.current?.scrollIntoView({ behavior: 'smooth' });

      setStep(3);

      const byteArray = new Uint8Array(data.data.Item.FileErrorDownload.MemoryStream);

      const blob = new Blob([byteArray], { type: 'application/xls' });

      const link = URL.createObjectURL(blob);

      setLink(link);

      setPreviewRowData([]);

      setRowSavedData((prevState) => {
        return {
          ...prevState,
          ListFail: data.data.Item.ListFail,
          ListSuccess: data.data.Item.ListSuccess,
          ListShow: data.data.Item.ListShow
        };
      });
    }
  });

  const disable = useMemo(() => savingExcelFile || uploadingExcelFile, [savingExcelFile, uploadingExcelFile]);

  const rowData = useMemo(() => {
    if (step === 2 && !!rowPreviewData.length) {
      return rowPreviewData;
    } else if (step === 3 && !!rowSavedData.ListShow.length) {
      return rowSavedData.ListShow;
    }
  }, [rowPreviewData, rowSavedData.ListShow, step]);

  const columns: ColumnsType<any> | undefined = useMemo(() => {
    const tableResult = step === 2 ? rowPreviewData : rowSavedData.ListShow;

    for (let index = 0; index < tableResult.length; index++) {
      return [
        {
          title: 'STT',
          dataIndex: 'stt',
          key: 'stt',
          render: (value, record, j: number) => (page - 1) * paginationSize + j + 1
        },
        {
          title: 'Tên DDC',
          dataIndex: 'TenDDC',
          key: 'TenDDC',
          render: (value, record) => {
            return (
              <>
                {step === 2 && record[1]}

                {step === 3 && (
                  <p
                    className={classNames('', {
                      'bg-danger-50 py-3': record[1].Color
                    })}
                  >
                    {record[1].Value}
                  </p>
                )}
              </>
            );
          }
        },
        {
          title: 'Mã DDC',
          dataIndex: 'MaDDC',
          key: 'MaDDC',
          render: (value, record) => {
            return (
              <>
                {step === 2 && record[2]}

                {step === 3 && (
                  <p
                    className={classNames('', {
                      'bg-danger-50 py-3': record[2].Color
                    })}
                  >
                    {record[2].Value}
                  </p>
                )}
              </>
            );
          }
        },
        {
          title: (step === 2 && 'Hành động') || (step === 3 && 'Nội dung lỗi'),
          dataIndex: 'actions',
          key: 'actions',
          render: (value, record) => {
            return (
              <div>
                {step === 2 && (
                  <button
                    className='mx-2'
                    onClick={(e) => {
                      for (let index = 0; index < rowPreviewData.length; index++) {
                        setPreviewRowData(rowPreviewData.filter((item) => item[1] !== record[1]));
                      }
                    }}
                  >
                    <DeleteFilled style={{ fontSize: '20px', color: 'red' }} />
                  </button>
                )}

                {step === 3 && <p className=' text-danger-10'>{record[3].Value}</p>}
              </div>
            );
          }
        }
      ];
    }
  }, [page, paginationSize, rowPreviewData, rowSavedData.ListShow, step]);

  const handleOnFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files?.[0];

    const form = new FormData();

    if (fileFromLocal) {
      form.append('file', fileFromLocal);
      UploadFileData(form);
    }
  };

  const handleAccessFileInputRef = () => {
    fileInutRef.current?.click();
  };

  const handleBack = () => {
    if (step === 2) {
      setStep(1);
      setPreviewRowData([]);
    } else {
      navigate(path.ddc);
    }
  };

  const handleSubmit = () => {
    saveExcelFile(rowPreviewData);
  };

  return (
    <div className='p-5' ref={ref}>
      <Title title='Thêm DDC từ file Excel' />

      <div className='my-10'>
        <ProgressBar activeStep={step} type='excel' />
      </div>

      {step === 1 && (
        <div className='flex items-center justify-center rounded-md bg-tertiary-10 py-4'>
          <a href={`/Tempalates/MauDDC.xls`} download='MauDDC.xls'>
            <Button className='bg-tertiary-20 hover:bg-tertiary-20/30' disabled={disable}>
              <DownloadOutlined />
              File excel mẫu
            </Button>
          </a>

          <input
            type='file'
            accept='.csv,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel'
            className='hidden'
            ref={fileInutRef}
            onChange={handleOnFileChange}
          />

          <Button variant='default' className='ml-2' onClick={handleAccessFileInputRef} disabled={disable}>
            Chọn tệp
          </Button>
        </div>
      )}

      {step > 1 && (
        <div>
          {step === 2 && (
            <>
              <p>
                {!rowPreviewData?.length ? (
                  <>
                    <p className='font-bold'>Không có DDC nào được nhận diện. Hãy chọn file khác!</p>

                    <input
                      type='file'
                      accept='.csv,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel'
                      className='hidden'
                      ref={fileInutRef}
                      onChange={handleOnFileChange}
                    />

                    <Button variant='default' className='ml-2' onClick={handleAccessFileInputRef} disabled={disable}>
                      Chọn tệp
                    </Button>
                  </>
                ) : (
                  <>
                    Tổng số DDC: <span className='font-bold'>{rowPreviewData?.length}</span>
                  </>
                )}
              </p>

              <h3 className='text-primary-10'>DANH SÁCH DCC</h3>
            </>
          )}

          {step === 3 && (
            <>
              <p className='text-tertiary-30'>
                Lưu thành công: <span className='font-bold'>{rowSavedData.ListSuccess.length} DDC</span>
              </p>

              {!!rowSavedData.ListShow.length && (
                <>
                  <p className='flex items-center gap-1 text-danger-10'>
                    Lưu thất bại: <span className='font-bold'>{rowSavedData.ListFail.length} DDC</span>
                    <a href={link} download='DanhSachDDCLoi.xls'>
                      <Button variant='danger' className='ml-2'>
                        <DownloadOutlined />
                        Tải file lỗi
                      </Button>
                    </a>
                  </p>
                  <h3 className='uppercase text-primary-10'>Danh DDC bị lỗi</h3>
                </>
              )}
            </>
          )}

          <div className='my-6'>
            <Table
              pagination={{
                onChange(current, pageSize) {
                  setPage(current);
                  setPaginationSize(pageSize);
                },
                defaultPageSize: 10,
                hideOnSinglePage: true,
                showSizeChanger: false
              }}
              loading={disable}
              columns={columns}
              dataSource={rowData || []}
              scroll={{ x: 980 }}
              rowKey={() => uniqueId()}
              className='custom-table'
              locale={{
                emptyText: () => <Empty label={locale.emptyText} />
              }}
              bordered
            />
          </div>
        </div>
      )}

      <div className='mr-10 mt-2 flex items-center justify-end'>
        <Button variant='secondary' onClick={handleBack} disabled={disable}>
          Quay về
        </Button>

        {!!rowPreviewData?.length && (
          <>
            {step === 2 && (
              <>
                <Button variant='default' className='ml-2' onClick={handleSubmit} disabled={disable}>
                  Tiếp tục
                </Button>
              </>
            )}

            {step === 3 && (
              <Button variant='default' className='ml-2' onClick={handleBack}>
                Hoàn tất
              </Button>
            )}
          </>
        )}
      </div>

      <Loading open={disable} />
    </div>
  );
};

export default AddDDCByExcelPage;
