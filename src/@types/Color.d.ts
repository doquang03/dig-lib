type Color = {
  Id: string;
  TenMau: string;
  ListColor: {
    Name: string;
  }[];
  IdLienKet: string;
  KieuLienKet: number;
  TenLienKet: string;
  TenKieuLienKet: string;
};
