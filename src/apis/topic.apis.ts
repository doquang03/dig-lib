import httpThuVien from 'utils/httpThuVien';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/ChuDiem`;

const topicApis = {
  GetAll: () => httpThuVien.post<ResponseApi<{ ListChuDiem: Topic[] }>>(`/${endPoint}/Index`),
  Index: (queryString: string) => {
    return httpThuVien.post(`/${endPoint}/IndexByModel` + queryString);
  },
  Delete: (id: string) => {
    return httpThuVien.post(`/${endPoint}/Delete?id=` + id);
  },
  EditByID: (id: string) => {
    return httpThuVien.post(`/${endPoint}/EditByID?id=` + id);
  },
  Create: (payload: object) => {
    return httpThuVien.post(`/${endPoint}/Create`, payload);
  },
  EditModel: (payload: object) => {
    return httpThuVien.post(`/${endPoint}/Edit`, payload);
  }
};

export default topicApis;
