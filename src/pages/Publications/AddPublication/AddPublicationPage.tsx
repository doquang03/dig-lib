/* eslint-disable react-hooks/exhaustive-deps */
import { CameraFilled, PlusOutlined, SettingFilled } from '@ant-design/icons';
import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { Tabs } from 'antd';
import { bookApis, errorSyncDocuments } from 'apis';
import { CreatePhieuNhapSachVars, GetBookByIdData } from 'apis/book.apis';
import { AxiosError } from 'axios';
import { Button, DatePicker, Input, Loading, ModalDelete, Select, Title, TitleDelete, TreeSelect } from 'components';
import { AutoComplete } from 'components/AutoComplete';
import { AUDIO_FILE_TYPE, VIDEO_FILE_TYPE } from 'constants/config';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import 'css/AddPublicationPage.css';
import dayjs from 'dayjs';
import { ChangeEvent, useEffect, useMemo, useRef, useState } from 'react';
import { useCookies } from 'react-cookie';
import { useFieldArray, useForm } from 'react-hook-form';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { bookSchema } from 'utils/rules';
import { convertDate, removeVietnameseTones } from 'utils/utils';
import * as yup from 'yup';
import SettingModal from './Model/SettingModal';
import AuthorModal from './components/AuthorModal';
import BookContent from './components/BookContent';
import BookshelfModal from './components/BookshelfModal';
import ColorModal from './components/ColorModal';
import Import from './components/Import';
import LanguageModal from './components/LanguageModal';
import PermissionTab from './components/PermissionTab';
import PlacePublicationModal from './components/PlacePublicationModal';
import PublishingCompanyModal from './components/PublishingCompanyModal';
import WarehouseInfor from './components/WarehouseInfor';
import { Revert } from 'pages/TitleApproval/icons';
import { isNaN } from 'lodash';

type BookForm = yup.InferType<typeof bookSchema>;

type MonHoc = {
  Id: string;
  MonHoc: string;
  Ten: string;
  KhoiLop: string[];
};

const exceptThisSymbols = ['e', 'E', '+', '-', '.'];

const initialBookValue = {
  IdBoSuuTap: ''
};

function MergingButton(props: { onClick: VoidFunction; visible: boolean }) {
  return (
    <>
      {props.visible ? (
        <button onClick={props.onClick} type='button'>
          <Revert />
        </button>
      ) : null}
    </>
  );
}

const AddPublicationPage = () => {
  const { bookId } = useParams();

  const [cookies, setCookie] = useCookies(['settings_add_book']);

  const navigate = useNavigate();

  const { profile, isAllowedAdjustment, permission } = useUser();

  const { state } = useLocation();

  const queryClient = useQueryClient();

  const [file, setFile] = useState<File>();
  const fileInutRef = useRef<HTMLInputElement | null>(null);
  const [currentImage, setCurrentImage] = useState();
  const [activeKey, setActiveKey] = useState<'general' | 'input' | 'content'>('general');

  const [isVisiable, setIsVisiable] = useState<boolean>(false);

  const [boSuuTapItem, setBoSuuTapItem] = useState<Selections>([]);
  const [nhaXuatBanItem, setNhaXuatBanItem] = useState<Selections>([]);
  const [noiXuatBanItem, setNoiXuatBanItem] = useState<Selections>([]);

  const [tacGiaItem, setTacGia] = useState<Selections>([]);

  const [chuDiemItem, setChuDiemItem] = useState<Selections>([]);
  const [monHocItem, setMonHocItem] = useState<MonHoc[]>([]);

  const [khoiLop, setKhoiLop] = useState<Selections>([]);

  const [keSachItem, setKeSachItem] = useState<Selections>([]);
  const [ngonNguItem, setNgonNguItem] = useState<Selections>([]);
  const [thuMucTaiLieuItem, setThuMucTaiLieuItem] = useState<Selections>([]);
  const [DDC_Day19, setDDC_Day19] = useState<Selections>([]);
  const [isDay19, setIsDay19] = useState(false);
  const [maMauItem, setMaMauItem] = useState<Selections>([]);
  const [khoSachItem, setKhoSachItem] = useState<Selections>([]);
  const [trangThaiItem, setTrangThaiItem] = useState<Selections>([]);
  const [nguonCungCapItem, setNguonCungCapItem] = useState<Selections>([]);
  const [arrAvaliable, setArrAvaliable] = useState<Array<boolean>>([]);
  const [visibleAuthorModal, setVisibleAuthorModal] = useState<{
    authorType: 'ListTacGiaJson' | 'ListTacGiaPhuJson' | undefined;
    visible: boolean;
  }>({
    authorType: undefined,
    visible: false
  });
  const [visiblePublishingModal, setVisiblePublishingModal] = useState<boolean>(false);
  const [visiblePlacePublicationModal, setVisiblePlacePublicationModal] = useState<boolean>(false);
  const [visibleBookshelfModal, setVisibleBookshelfModal] = useState<boolean>(false);
  const [visibleLanguageModal, setVisibleLanguageModal] = useState<boolean>(false);
  const [visibleColorModal, setVisibleColorModal] = useState<boolean>(false);
  const [pdfFile, setpdfFile] = useState<File | undefined>(undefined);
  const [imagesFile, setImagesFile] = useState<File[]>([]);
  const [contentType, setContentType] = useState<'image' | 'PDF' | 'audio' | 'video' | ''>('');
  const [imagesFromData, setImagesFromData] = useState<string[]>([]);

  // permission state
  const [permissionFormValue, setPermissionFormValues] = useState<{
    allowReading: boolean;
    libraryAllowReading: number;
    readingFee: string;
    allowDownloading: boolean;
    libraryAllowDownloading: number;
    downloadPercent: string;
    allowBorrowing: boolean;
    libraryAllowBorrowing: number;
    borrowingFee: string;
  }>({
    allowReading: true,
    libraryAllowReading: 1,
    readingFee: '0',
    allowDownloading: true,
    libraryAllowDownloading: 1,
    downloadPercent: '100',
    allowBorrowing: true,
    libraryAllowBorrowing: 1,
    borrowingFee: '0'
  });

  const handleChangeAllowPermission = (e: ChangeEvent<HTMLInputElement>) => {
    if (!e) return;

    const { name, checked } = e.target;

    setPermissionFormValues((prevState) => {
      return { ...prevState, [name]: checked };
    });
  };

  const handleChangePermissionOfLib = (e: ChangeEvent<HTMLInputElement>) => {
    if (!e) return;

    const { value, name } = e.target;

    setPermissionFormValues((prevState) => {
      return { ...prevState, [name]: +value };
    });
  };

  const handleChangeFee = (e: ChangeEvent<HTMLInputElement>) => {
    if (!e) return;

    const { value, name } = e.target;

    if (name === 'downloadPercent') {
      setPermissionFormValues((prevState) => {
        return { ...prevState, downloadPercent: +value > 100 ? '100' : +value === 0 ? '' : value };
      });
    } else {
      setPermissionFormValues((prevState) => {
        return { ...prevState, [name]: value.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',') };
      });
    }
  };

  const {
    register,
    handleSubmit,
    setValue,
    setError,
    getValues,
    reset,
    control,
    watch,
    formState: { errors }
  } = useForm<BookForm>({
    defaultValues: initialBookValue,
    resolver: yupResolver(bookSchema)
  });

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'NhapKho'
  });

  const {
    data: formOptionsData,
    isLoading: isLoadingForm,
    isFetching: fetchingLoadingForm
  } = useQuery({
    queryKey: ['GetInitCreateForm'],
    queryFn: bookApis.GetInitCreateForm,
    onSuccess: (res) => {
      let Item = res.data.Item;
      setBoSuuTapItem([{ label: 'Chọn bộ sưu tập', value: '' }, ...Item.Options_BoSuuTap]);
      setNhaXuatBanItem([{ label: 'Chọn nhà xuất bản', value: '' }, ...Item.Options_NXB]);
      setNoiXuatBanItem([{ label: 'Chọn nơi xuất bản', value: '' }, ...Item.Options_NoiXuatBan]);

      setMonHocItem(res.data.Item.Options_MonHoc);

      setChuDiemItem([{ label: 'Chọn chủ điểm', value: '' }, ...Item.Options_ChuDiem]);

      setKeSachItem([{ label: 'Chọn kệ sách', value: '' }, ...Item.Options_KeSach]);
      setNgonNguItem([{ label: 'Chọn ngôn ngữ', value: '' }, ...Item.Options_Language]);
      setThuMucTaiLieuItem([{ label: 'Chọn thư mục tài liệu', value: '' }, ...Item.Options_ThuMucSach]);

      setTacGia(Item.Options_TacGia);
      setIsDay19(Item.IsDay19);
      setDDC_Day19([{ label: `Chọn ${Item.IsDay19 ? 'Dãy 19' : 'DDC'}`, value: '' }, ...Item.Options_DDC_Day19]);
      setMaMauItem([{ label: 'Chọn mã màu', value: '' }, ...Item.Options_MaMau]);
      setKhoSachItem([{ label: 'Chọn kho sách', value: '' }, ...Item.Options_KhoSach]);
      setTrangThaiItem([{ label: 'Chọn tình trạng sách', value: '' }, ...Item.Options_TrangThaiSach]);
      setNguonCungCapItem([{ label: 'Chọn nguồn cung cấp', value: '' }, ...Item.Options_NguonCungCap]);
    },
    cacheTime: 0
  });

  const { mutate: deleteBook, isLoading: isLoadingDelete } = useMutation({
    mutationFn: bookApis.deleteBook,
    onSuccess: () => {
      setIsVisiable(false);
      toast.success('Xóa đầu sách thành công');
      handleBack();
    }
  });

  const options = useMemo(() => {
    if (!formOptionsData?.data.Item) return;

    const { Item } = formOptionsData.data;
    setMonHocItem(Item.Options_MonHoc);

    const optionsCollection = [{ label: 'Chọn bộ sưu tập', value: '' }, ...Item.Options_BoSuuTap];
    const optionsPublishingCompany = [{ label: 'Chọn nhà xuất bản', value: '' }, ...Item.Options_NXB];
    const optionsPublishingPlace = [{ label: 'Chọn nơi xuất bản', value: '' }, ...Item.Options_NoiXuatBan];
    const subjectsOptions = [
      { label: 'Chọn môn học', value: '' },
      ...Item.Options_MonHoc.map(({ Ten, Id }: { Ten: string; Id: string }) => {
        return {
          label: Ten,
          value: Id
        };
      })
    ];
    const topicOptions = [{ label: 'Chọn chủ điểm', value: '' }, ...Item.Options_ChuDiem];
    const bookshelfOptions = [{ label: 'Chọn kệ sách', value: '' }, ...Item.Options_KeSach];
    const languageOptions = [{ label: 'Chọn ngôn ngữ', value: '' }, ...Item.Options_Language];
    const documentFolderOptions = [{ label: 'Chọn thư mục tài liệu', value: '' }, ...Item.Options_ThuMucSach];
    const authorOptions = Item.Options_TacGia;
    const day19Options = Item.IsDay19;
    const DDCOptions = [{ label: `Chọn ${Item.IsDay19 ? 'Dãy 19' : 'DDC'}`, value: '' }, ...Item.Options_DDC_Day19];
    const colorOptions = [{ label: 'Chọn mã màu', value: '' }, ...Item.Options_MaMau];
    const storeOptions = [{ label: 'Chọn kho sách', value: '' }, ...Item.Options_KhoSach];
    const statusOptions = [{ label: 'Chọn tình trạng sách', value: '' }, ...Item.Options_TrangThaiSach];
    const supplierOptions = [{ label: 'Chọn nguồn cung cấp', value: '' }, ...Item.Options_NguonCungCap];

    return {
      optionsCollection,
      optionsPublishingCompany,
      optionsPublishingPlace,
      subjectsOptions,
      topicOptions,
      bookshelfOptions,
      languageOptions,
      documentFolderOptions,
      authorOptions,
      day19Options,
      DDCOptions,
      colorOptions,
      storeOptions,
      statusOptions,
      supplierOptions
    };
  }, [formOptionsData?.data.Item]);

  const {
    data: bookData,
    isLoading: isLoadingGetBookById,
    refetch
  } = useQuery({
    queryKey: ['book', bookId, Boolean(formOptionsData?.data.Item)],
    queryFn: () => bookApis.getBookById(bookId + ''),
    enabled: Boolean(bookId) && Boolean(formOptionsData?.data.Item),
    cacheTime: 0
  });

  // call api able to get book's old data
  const { data: oldBookData } = useQuery({
    queryKey: ['getBookOldData', bookId, state?.isFromErrorSyncedDocument, state?.errorSyncedDocumentId],
    queryFn: () => errorSyncDocuments.getOldDetailBookData(state?.errorSyncedDocumentId + ''),
    enabled: !!state && state?.isFromErrorSyncedDocument && !!state?.errorSyncedDocumentId && !!bookId
  });

  const oldBookValue = oldBookData?.data?.Item?.Model || {};

  useEffect(() => {
    if (!bookData?.data.Item && !Boolean(bookId)) return;

    const book = bookData?.data?.Item?.SachDTO;
    if (!book) return;

    setValue('IdBoSuuTap', book?.IdBoSuuTap + '');
    setValue('TenSach', book?.TenSach + '');
    setValue('TenSongNgu', book.TenSongNgu);
    book.listIdTacGia && setValue('ListTacGiaJson', book.listIdTacGia || []);
    book.listIdTacGiaPhu && setValue('ListTacGiaPhuJson', book.listIdTacGiaPhu || []);
    setValue('IdNhaXuatBan', book.IdNhaXuatBan);
    setValue('IdNoiXuatBan', book.IdNoiXuatBan);
    setValue('XuatXu', book.XuatXu);
    setValue('SoTrang', book?.SoTrang);
    // @ts-ignore
    setValue('NamXuatBan', isNaN(Number(book?.NamXuatBan)) ? null : book.NamXuatBan);
    setValue('IdThuMucSach', book.IdThuMucSach || '');
    setValue('IdMonHoc', book.IdMonHoc);

    onChangeSuject(book.IdMonHoc as string);

    book.KhoiLop && setValue('IdKhoiLop', book.KhoiLop || []);
    setValue('IdChuDiem', book.IdChuDiem);
    setValue('IdKeSach', book.IdKeSach);
    setValue('IdNgonNgu', book.IdNgonNgu);
    setValue('GiaBia', book.GiaBia);
    setValue('ISBN', book.ISBN);
    book.Day19 && setValue('Day19', book.Day19);
    book.DDC && setValue('DDC', book.DDC);

    setValue('SKU', book.SKU);
    setValue('LLC', book.LLC);
    setValue('TomTat', book.TomTat);
    setValue('MinhHoa', book.MinhHoa);
    setValue('TungThu', book.TungThu);
    setValue('PhuChu', book.PhuChu);
    // setValue('PhiMuonSach', book.PhiMuonSach);
    setValue('TaiBan', book.TaiBan);
    setValue('TaiLieuDinhKem', book.TaiLieuDinhKem);
    setValue('MoTaVatLy', book.MoTaVatLy);
    setValue('IdMaMau', book.IdMaMau);
    setValue('CoSach', book.CoSach);
    setValue('NguoiBienDich', book.NguoiBienDich);

    // permission
    setPermissionFormValues({
      allowReading: book.QuyenDoc !== 0 ? true : false,
      libraryAllowReading: book.QuyenDoc || 1,
      readingFee: book?.PhiDocSach?.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',') || '',
      allowDownloading: book.QuyenTai !== 0 ? true : false,
      libraryAllowBorrowing: book.QuyenTai || 1,
      downloadPercent: book?.TiLeTaiXuong && +book?.TiLeTaiXuong ? book?.TiLeTaiXuong : '1',
      allowBorrowing: book.QuyenMuon !== 0 ? true : false,
      libraryAllowDownloading: book.QuyenMuon || 1,
      borrowingFee: book?.PhiMuonSach?.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',') || ''
    });
  }, [setValue, bookData?.data.Item, Boolean(bookId), bookData?.data.Item.SachDTO.IdMonHoc]);

  const {
    data: bookContentData,
    refetch: refetchBookContent,
    isLoading: isLoadingBookContent
  } = useQuery({
    queryKey: ['bookContent', bookId],
    queryFn: () => bookApis.getBookContent(bookId + ''),
    enabled: !!bookId,
    onSuccess: (data) => {
      const resultContent = data.data.Item;

      if (resultContent.ListImage.length && resultContent.isImage) {
        setContentType('image');
        setImagesFromData(data.data.Item.ListImage);
        return;
      }

      if (!!resultContent.ListFile.length) {
        const file = resultContent.ListFile?.[0];

        const fileNameSplited = file.FileName.split('.');

        AUDIO_FILE_TYPE.includes(fileNameSplited[fileNameSplited.length - 1]) && setContentType('audio');

        VIDEO_FILE_TYPE.includes(fileNameSplited[fileNameSplited.length - 1]) && setContentType('video');

        file.FileName.includes('.pdf') && setContentType('PDF');

        setImagesFromData(data.data.Item.ListImage);
      }
    }
  });

  const { mutateAsync: createBookAsync, isLoading: isLoadingCreateBook } = useMutation({
    mutationFn: (payLoad: FormData) => bookApis.Create(payLoad),
    onError: (err: AxiosError<ResponseApi>) => {
      let bookNameField = err.response?.data.Message.includes('Sách');
      let ISBNField = err.response?.data.Message.includes('ISBN');

      ISBNField && setError('ISBN', { type: 'validate', message: err.response?.data.Message }, { shouldFocus: true });
      bookNameField &&
        setError('TenSach', { type: 'validate', message: err.response?.data.Message }, { shouldFocus: true });

      handleChangeTab('general');
    }
  });

  const { mutateAsync: editeBookAsync, isLoading: isLoadingEditBook } = useMutation({
    mutationFn: (payLoad: FormData) => bookApis.editBookById(payLoad),
    onError: (err: AxiosError<ResponseApi>) => {
      let bookNameField = err.response?.data.Message.includes('Sách');
      let ISBNField = err.response?.data.Message.includes('ISBN');

      ISBNField && setError('ISBN', { type: 'validate', message: err.response?.data.Message }, { shouldFocus: true });
      bookNameField &&
        setError('TenSach', { type: 'validate', message: err.response?.data.Message }, { shouldFocus: true });

      handleChangeTab('general');
    }
  });

  const { mutateAsync: createPhieuNhapKhoAsync, isLoading: isLoadingCreatePhieuNhapKho } = useMutation({
    mutationFn: (payLoad: CreatePhieuNhapSachVars) => bookApis.createPhieuNhapSach(payLoad),
    onError: () => {
      handleChangeTab('input');
    }
  });

  const { mutateAsync: uploadPDFAsync, isLoading: isLoadingUploadPDF } = useMutation({
    mutationFn: bookApis.uploadPDFFile,
    onSuccess: () => {
      queryClient.invalidateQueries(['bookContent', bookId]);
    }
  });

  const { mutateAsync: uploadImagesAsycn, isLoading: isLoadingUploadImages } = useMutation({
    mutationFn: bookApis.uploadImagesFile
  });

  const { mutateAsync: uploadAudioVideoAsync, isLoading: isLoadingUpdateMedia } = useMutation({
    mutationFn: bookApis.uploadAudiVideo,
    onSuccess: () => {
      queryClient.invalidateQueries(['bookContent', bookId]);
    }
  });

  useMemo(() => {
    if (arrAvaliable?.length === 0) {
      if (cookies.settings_add_book) {
        setArrAvaliable(cookies.settings_add_book);
      } else {
        setArrAvaliable([
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true
        ]);
      }
    }
  }, [arrAvaliable?.length, cookies.settings_add_book]);

  const [visibleModal, setVisibleModal] = useState(false);

  const previewImage = useMemo(() => {
    if (file) return URL.createObjectURL(file);
    if (currentImage) return currentImage;
    return '';
  }, [currentImage, file]);

  const resultPreview = useMemo(() => {
    return previewImage ? previewImage : bookData?.data.Item.SachDTO.LinkBiaSach;
  }, [bookData?.data.Item.SachDTO.LinkBiaSach, previewImage]);

  useEffect(() => {
    if (!state?.documentId || thuMucTaiLieuItem.length < 1) return;
    setValue('IdThuMucSach', state?.documentId);
  }, [state?.documentId, thuMucTaiLieuItem]);

  const IdNoiXuatBan = watch('IdNoiXuatBan');

  useEffect(() => {
    if (!IdNoiXuatBan || IdNoiXuatBan.length < 1) return;

    setValue('IdNoiXuatBan', IdNoiXuatBan);
  }, [IdNoiXuatBan]);

  const IdMonHoc = watch('IdMonHoc');
  const IdKhoiLop = watch('IdKhoiLop');
  useEffect(() => {
    if (!IdMonHoc || IdMonHoc.length < 1) return;

    onChangeSuject(IdMonHoc);
    setValue('IdKhoiLop', IdKhoiLop);
  }, [IdMonHoc]);

  const handleBack = () => navigate(-1);

  const handleAccessFileInputRef = () => {
    fileInutRef.current?.click();
  };

  const handleOnFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files?.[0];
    if (!fileFromLocal) {
      return;
    }

    if (fileFromLocal && !fileFromLocal.type.startsWith('image')) {
      return;
    }
    setFile(fileFromLocal);
  };

  const isDisable = (isLoadingGetBookById && Boolean(bookId)) || isLoadingCreatePhieuNhapKho || isLoadingForm;

  const onSubmit = handleSubmit(
    async (data) => {
      data.TenSach = data.TenSach.replace(/\s\s+/g, ' ');
      data.TenSongNgu = data.TenSongNgu && data.TenSongNgu.replace(/\s\s+/g, ' ');

      if (
        data.TenSongNgu &&
        removeVietnameseTones(data.TenSach).toLocaleLowerCase().trim() ===
          removeVietnameseTones(data.TenSongNgu).toLocaleLowerCase().trim()
      ) {
        setError('TenSongNgu', { message: 'Tên song ngữ không thể trùng với tên đầu sách' });
        handleChangeTab('general');
        return;
      }

      const formData = new FormData();

      Boolean(bookId) && formData.append('id', bookId + '');
      file && formData.append('FileImageCover', file);
      formData.append('TenSach', data.TenSach);
      data.TenSongNgu && formData.append('TenSongNgu', data?.TenSongNgu.replace(/\s\s+/g, ' '));
      formData.append('IdBoSuuTap', data.IdBoSuuTap);
      data.IdKeSach && formData.append('IdKeSach', data.IdKeSach);
      data.IdNhaXuatBan && formData.append('IdNhaXuatBan', data.IdNhaXuatBan);
      data.IdNgonNgu && formData.append('IdNgonNgu', data.IdNgonNgu);
      data.IdNoiXuatBan && formData.append('IdNoiXuatBan', data.IdNoiXuatBan);
      data.IdThuMucSach && formData.append('IdThuMucSach', data.IdThuMucSach);
      data.CoSach && formData.append('CoSach', data.CoSach);
      data.ISBN && formData.append('ISBN', data.ISBN);
      data.DDC && formData.append('DDC', data.DDC);
      data.LLC && formData.append('LLC', data.LLC);
      data.Day19 && formData.append('Day19', data.Day19);
      data.IdMaMau && formData.append('IdMaMau', data.IdMaMau);
      data.SKU && formData.append('SKU', data.SKU);
      data.IdMonHoc && formData.append('IdMonHoc', data.IdMonHoc);
      !!data.IdKhoiLop?.length && data.IdKhoiLop.forEach((khoiLop) => formData.append('KhoiLop', khoiLop));
      data.IdChuDiem && formData.append('IdChuDiem', data.IdChuDiem);
      data.MinhHoa && formData.append('MinhHoa', data.MinhHoa);
      data.TungThu && formData.append('TungThu', data.TungThu);
      data.PhuChu && formData.append('PhuChu', data.PhuChu);
      data.NamXuatBan && formData.append('NamXuatBan', dayjs(data.NamXuatBan).year() + '');
      data.TaiBan && formData.append('TaiBan', data.TaiBan);
      data.XuatXu && formData.append('XuatXu', data.XuatXu);
      data?.SoTrang && formData.append('SoTrang', data?.SoTrang);
      data.MoTaVatLy && formData.append('MoTaVatLy', data.MoTaVatLy);
      data.NguoiBienDich && formData.append('NguoiBienDich', data.NguoiBienDich);
      data.TomTat && formData.append('TomTat', data.TomTat);
      data.TaiLieuDinhKem && formData.append('TaiLieuDinhKem', data.TaiLieuDinhKem);
      data.GiaBia && formData.append('GiaBia', data.GiaBia);
      data.PhiMuonSach && formData.append('PhiMuonSach', data.PhiMuonSach);

      !!data.ListTacGiaJson?.length &&
        data.ListTacGiaJson.forEach((author) => {
          if (typeof author !== 'string') {
            formData.append('listIdTacGia', author.value);
          }

          if (typeof author === 'string') {
            formData.append('listIdTacGia', author);
          }
        });

      !!data.ListTacGiaPhuJson?.length &&
        data.ListTacGiaPhuJson.forEach((author) => {
          if (typeof author !== 'string') {
            formData.append('listIdTacGiaPhu', author.value);
          }

          if (typeof author === 'string') {
            formData.append('listIdTacGiaPhu', author);
          }
        });

      formData.append('idUserAdmin', profile?.Id + '');
      formData.append('userName', profile?.UserName + '');

      setValue('TenSach', data.TenSach.replace(/\s\s+/g, ' '));
      // data?.TenSongNgu && setValue('TenSongNgu', data?.TenSongNgu.replace(/\s\s+/g, ' '));

      // permission
      formData.append(
        'QuyenDoc',
        permissionFormValue.allowReading ? permissionFormValue.libraryAllowReading + '' : '0'
      );

      formData.append(
        'PhiDocSach',
        permissionFormValue.allowReading ? permissionFormValue.readingFee.replaceAll(',', '') : '0'
      );

      formData.append(
        'QuyenTai',
        permissionFormValue.allowDownloading ? permissionFormValue.libraryAllowDownloading + '' : '0'
      );

      formData.append('TiLeTaiXuong', permissionFormValue.allowDownloading ? permissionFormValue.downloadPercent : '1');

      formData.append(
        'QuyenMuon',
        permissionFormValue.allowBorrowing ? permissionFormValue.libraryAllowBorrowing + '' : '0'
      );

      formData.append('PhiMuonSach', permissionFormValue.allowBorrowing ? permissionFormValue.borrowingFee : '0');

      try {
        let bookIdData = '';
        if (!Boolean(bookId)) {
          // Tạo thông tin chung
          const bookData = await createBookAsync(formData);
          bookIdData = bookData.data.Item.Id;
          // Tạo phiếu nhập kho nếu có
          !!data.NhapKho?.length &&
            (await createPhieuNhapKhoAsync({
              id: bookData.data.Item.Id,
              IdUserAdmin: profile?.Id + '',
              UserName: profile?.UserName + '',
              GhiChuPhieuNhap: data.GhiChu + '',
              DanhSachPhieuNhap: data.NhapKho.map((x) => {
                return {
                  IdKho: x.KhoSach,
                  SoLuong: Number(x.SoLuong),
                  IdTrangThai: x.TrangThai,
                  IdNCC: x.NguonCungCap,
                  SCT: x.SoCT + '',
                  SVS: x.VSTQ + '',
                  NVS: convertDate(x.NgayNhap + '')
                };
              }) as CreatePhieuNhapSachVars['DanhSachPhieuNhap']
            }));
          // cập nhật
        } else {
          // cập nhật đầu sách
          await editeBookAsync(formData);
        }
        // Tạo nội dung pdf sách nếu có
        if (pdfFile && contentType === 'PDF') {
          const formFileData = new FormData();
          formFileData.append('id', bookIdData || bookId + '');
          formFileData.append('filePDF', pdfFile);

          await uploadPDFAsync(formFileData);
        }

        // Tạo nội dung hình ảnh sách nếu có
        if ((!!imagesFile.length || !!imagesFromData.length) && contentType === 'image') {
          const formImageData = new FormData();
          formImageData.append('id', bookIdData || bookId + '');
          // Thêm hình ảnh
          imagesFile.forEach((imageFile) => formImageData.append('listFile', imageFile));
          // update hình ảnh
          imagesFromData.forEach((image, index) => {
            formImageData.append('listLink', image);
            formImageData.append('stt', index + '');
          });
          await uploadImagesAsycn(formImageData);
        }

        // Tạo nội dung video || audio nếu có
        if (pdfFile && (contentType === 'video' || contentType === 'audio')) {
          const formFileData = new FormData();
          formFileData.append('id', bookIdData || bookId + '');
          formFileData.append('fileMedia', pdfFile);

          await uploadAudioVideoAsync(formFileData);
          await refetchBookContent();
        }
        if (!Boolean(bookId)) {
          toast.success('Tạo đầu sách thành công');
          reset();
          navigate(path.sach);
        } else {
          toast.success('Cập nhật đầu sách thành công');
          refetch();
        }
      } catch (error) {
        console.warn('error', error);
      }
    },
    (errors) => {
      if (errors.TenSongNgu || errors.IdBoSuuTap || errors.TenSach || errors.NamXuatBan) {
        return handleChangeTab('general');
      } else if (errors.NhapKho) {
        return handleChangeTab('input');
      }
    }
  );

  const authorValues = (visibleAuthorModal?.authorType && getValues(visibleAuthorModal?.authorType)) || [];

  const handleAddAuthor = (authorType: 'ListTacGiaJson' | 'ListTacGiaPhuJson') => () =>
    setVisibleAuthorModal({ authorType, visible: true });

  const handleCloseAuthorModal = () => setVisibleAuthorModal({ authorType: undefined, visible: false });

  const handleAddAuthorSuccess = (data: string, id: string) => {
    if (!visibleAuthorModal?.authorType) return;

    setTacGia((prevState) => [{ label: data.replace(/ +(?= )/g, ''), value: id }, ...prevState]);

    setValue(visibleAuthorModal?.authorType, [{ label: data.replace(/ +(?= )/g, ''), value: id }, ...authorValues]);
  };

  const handleOpenPublishingModal = () => setVisiblePublishingModal(true);

  const handleClosePublishingModal = () => setVisiblePublishingModal(false);

  const handleOpenPlacePublicationModal = () => setVisiblePlacePublicationModal(true);

  const handleClosePlacePublicationModal = () => setVisiblePlacePublicationModal(false);

  const handleCloseBookshelfModal = () => setVisibleBookshelfModal(false);

  const handleOpenBookshelfModal = () => setVisibleBookshelfModal(true);

  const handleOpenLanguageModal = () => setVisibleLanguageModal(true);

  const handleCloseLanguageModal = () => setVisibleLanguageModal(false);

  const handleOpenColorModal = () => setVisibleColorModal(true);

  const handleCloseColorModal = () => setVisibleColorModal(false);

  const handleChangeTab = (key: string) => {
    setActiveKey(key as 'general' | 'input' | 'content');
  };

  const handleOnSuccess = () => {
    refetch();
  };

  const handleCreateBookshelfSuccess = (bookshelfValue: string) => {
    setValue('IdKeSach', bookshelfValue);
  };

  const handleCreateLanguageSuccess = (languageValue: string) => setValue('IdNgonNgu', languageValue);

  const handleCreatePlacePulicationSuccess = (placePulicationValue: string) =>
    setValue('IdNoiXuatBan', placePulicationValue);

  const handleGetFile = (file: File | undefined) => {
    setpdfFile(file);
  };

  const handleGetImageFile = (files: File[]) => {
    setImagesFile(files);
  };

  const handleGetContentType = (type: 'image' | 'PDF' | 'audio' | 'video' | '') => setContentType(type);

  const handleGetImagesFromData = (images: string[]) => setImagesFromData(images);

  const onChangeSuject = (value: string) => {
    khoiLop.splice(0, khoiLop.length);
    monHocItem
      ?.find(({ Id }) => Id === value)
      ?.KhoiLop.forEach((lop) => {
        khoiLop.push({
          value: lop,
          label: `Khối ${lop}`
        });
      });
    setKhoiLop(khoiLop);

    setValue('IdKhoiLop', []);
  };

  function handleRevertToOldValue(field: keyof BookForm) {
    // @ts-ignore
    setValue(field, isNaN(field) ? null : oldBookValue[field]);
  }

  const dataRevertToOldValue = (field: keyof BookForm) => {
    // @ts-ignore
    return JSON.stringify(getValues(field) || '') !== JSON.stringify(oldBookValue[field] || '');
  };

  const detailTab = useMemo(() => {
    return {
      label: `Thông tin chung`,
      key: `general`,
      children: (
        <>
          <div className='thongtin-group'>
            <Button
              variant='default'
              type='button'
              className={isAllowedAdjustment ? 'setting-button ml-2 font-semibold' : 'hidden'}
              onClick={() => {
                setVisibleModal(true);
              }}
            >
              <SettingFilled style={{ color: 'white' }}></SettingFilled> Cấu hình hiển thị
            </Button>
            <h5 className='title-box'>
              Thông tin chung
              <div className='background-fake'></div>
            </h5>
            <div className='container-maintain grid grid-cols-1 md:grid-cols-12'>
              <div className='col-span-6 md:mr-1'>
                <div className='form-group form-group-sm row'>
                  <label className='text-inbox col-sm-4 font-weight'>
                    Bộ sưu tập<span className='text-danger-10'>*</span>
                  </label>
                  <div className='col-sm-8 flex gap-1 max-md:w-full'>
                    <Select
                      items={options?.optionsCollection || []}
                      className='w-full flex-grow'
                      name='IdBoSuuTap'
                      register={register}
                      errorMessage={errors?.IdBoSuuTap?.message}
                      disabled={isDisable || !isAllowedAdjustment}
                    />

                    {dataRevertToOldValue('IdBoSuuTap') && (
                      <MergingButton
                        onClick={() => handleRevertToOldValue('IdBoSuuTap')}
                        visible={state?.isFromErrorSyncedDocument || false}
                      />
                    )}
                  </div>
                </div>
                <div className='form-group form-group-sm row'>
                  <label className='text-inbox col-sm-4 font-weight'>
                    Tên đầu sách<span className='text-danger-10'>*</span>
                  </label>
                  <div className='col-sm-8 flex gap-1 max-md:w-full'>
                    <Input
                      containerClassName='w-full'
                      placeholder='Nhập tên đầu sách'
                      name='TenSach'
                      register={register}
                      errorMessage={errors?.TenSach?.message}
                      disabled={isDisable || !isAllowedAdjustment}
                      maxLength={256}
                    />

                    {dataRevertToOldValue('TenSach') && (
                      <MergingButton
                        onClick={() => handleRevertToOldValue('TenSach')}
                        visible={state?.isFromErrorSyncedDocument || false}
                      />
                    )}
                  </div>
                </div>
                {arrAvaliable[0] && (
                  <div className='form-group form-group-sm row'>
                    <label className='text-inbox col-sm-4 font-weight'>Tên song ngữ</label>
                    <div className='col-sm-8 flex gap-1 max-md:w-full'>
                      <Input
                        containerClassName='w-full'
                        placeholder='Nhập tên song ngữ'
                        name='TenSongNgu'
                        register={register}
                        errorMessage={errors?.TenSongNgu?.message}
                        disabled={isDisable || !isAllowedAdjustment}
                        maxLength={256}
                      />

                      {dataRevertToOldValue('TenSongNgu') && (
                        <MergingButton
                          onClick={() => handleRevertToOldValue('TenSongNgu')}
                          visible={state?.isFromErrorSyncedDocument || false}
                        />
                      )}
                    </div>
                  </div>
                )}
                <div className='form-group form-group-sm row'>
                  <label className='text-inbox col-sm-4 font-weight'>Tác giả chính</label>
                  <div className='col-sm-8 flex gap-1 max-md:w-full'>
                    <AutoComplete
                      className='grow'
                      name='ListTacGiaJson'
                      control={control}
                      mode='tags'
                      placeholder='Chọn tác giả chính'
                      items={tacGiaItem || []}
                      errorMessage={errors.ListTacGiaJson?.message}
                      disabled={!isAllowedAdjustment}
                    />

                    <Button
                      type='button'
                      className='container-refesh-button shrink-0'
                      onClick={handleAddAuthor('ListTacGiaJson')}
                      disabled={!isAllowedAdjustment}
                    >
                      <PlusOutlined />
                    </Button>

                    {dataRevertToOldValue('TenSongNgu') && (
                      <MergingButton
                        onClick={() => handleRevertToOldValue('ListTacGiaJson')}
                        visible={state?.isFromErrorSyncedDocument || false}
                      />
                    )}
                  </div>
                </div>
                {arrAvaliable[1] && (
                  <div className='form-group form-group-sm row'>
                    <label className='text-inbox col-sm-4 font-weight'>Tác giả phụ</label>
                    <div className='col-sm-8 flex gap-1 max-md:w-full'>
                      <AutoComplete
                        className='grow'
                        name='ListTacGiaPhuJson'
                        control={control}
                        mode='tags'
                        placeholder='Chọn tác giả phụ'
                        items={tacGiaItem || []}
                        errorMessage={errors.ListTacGiaPhuJson?.message}
                        disabled={!isAllowedAdjustment}
                      />

                      <Button
                        type='button'
                        className='container-refesh-button shrink-0'
                        onClick={handleAddAuthor('ListTacGiaPhuJson')}
                        disabled={!isAllowedAdjustment}
                      >
                        <PlusOutlined />
                      </Button>

                      {dataRevertToOldValue('ListTacGiaPhuJson') && (
                        <MergingButton
                          onClick={() => handleRevertToOldValue('ListTacGiaPhuJson')}
                          visible={state?.isFromErrorSyncedDocument || false}
                        />
                      )}
                    </div>
                  </div>
                )}
                <div className='form-group form-group-sm row'>
                  <label className='text-inbox col-sm-4 font-weight'>Nhà xuất bản</label>
                  <div className='col-sm-8 flex gap-1 max-md:w-full'>
                    <Select
                      items={options?.optionsPublishingCompany || []}
                      className='w-full grow'
                      name='IdNhaXuatBan'
                      register={register}
                      errorMessage={errors?.IdNhaXuatBan?.message}
                      disabled={isDisable || !isAllowedAdjustment}
                    />

                    <Button
                      type='button'
                      className='container-refesh-button shrink-0'
                      onClick={handleOpenPublishingModal}
                      disabled={!isAllowedAdjustment}
                    >
                      <PlusOutlined />
                    </Button>

                    {dataRevertToOldValue('IdNhaXuatBan') && (
                      <MergingButton
                        onClick={() => handleRevertToOldValue('IdNhaXuatBan')}
                        visible={state?.isFromErrorSyncedDocument || false}
                      />
                    )}
                  </div>
                </div>
                <div className='form-group form-group-sm row'>
                  <label className='text-inbox col-sm-4 font-weight'>Nơi xuất bản</label>

                  <div className='col-sm-8 flex gap-1 max-md:w-full'>
                    <Select
                      items={options?.optionsPublishingPlace || []}
                      className='w-full grow'
                      name='IdNoiXuatBan'
                      register={register}
                      errorMessage={errors?.IdNoiXuatBan?.message}
                      disabled={isDisable || !isAllowedAdjustment}
                    />

                    <Button
                      type='button'
                      className='container-refesh-button shrink-0'
                      onClick={handleOpenPlacePublicationModal}
                      disabled={!isAllowedAdjustment}
                    >
                      <PlusOutlined />
                    </Button>

                    {dataRevertToOldValue('IdNoiXuatBan') && (
                      <MergingButton
                        onClick={() => handleRevertToOldValue('IdNoiXuatBan')}
                        visible={state?.isFromErrorSyncedDocument || false}
                      />
                    )}
                  </div>
                </div>
                <div className='form-group form-group-sm row'>
                  <label className='text-inbox col-sm-4 font-weight'>
                    Năm xuất bản <span className='text-danger-10'>*</span>
                  </label>

                  <div className='col-sm-8 flex gap-1 max-md:w-full'>
                    <DatePicker
                      name='NamXuatBan'
                      picker='year'
                      placeholder='Nhập năm xuất bản'
                      control={control}
                      disabledDate={(current) => {
                        return current && current > dayjs().endOf('day');
                      }}
                      errorMessage={errors?.NamXuatBan?.message}
                      disabled={isDisable || !isAllowedAdjustment}
                    />

                    {dataRevertToOldValue('NamXuatBan') && (
                      <MergingButton
                        onClick={() => handleRevertToOldValue('NamXuatBan')}
                        visible={state?.isFromErrorSyncedDocument || false}
                      />
                    )}
                  </div>
                </div>
              </div>

              <div className='col-span-6 md:ml-1'>
                {arrAvaliable[7] && (
                  <div className='form-group form-group-sm row'>
                    <label className='text-inbox col-sm-4 font-weight'>Lần xuất bản</label>
                    <div className='col-sm-8 flex gap-1 max-md:w-full'>
                      <Input
                        containerClassName='grow'
                        placeholder='Nhập lần xuất bản'
                        name='TaiBan'
                        onKeyPress={(event) => {
                          if (!/[0-9]/.test(event.key)) {
                            event.preventDefault();
                          }
                        }}
                        register={register}
                        errorMessage={errors?.TaiBan?.message}
                        disabled={isDisable || !isAllowedAdjustment}
                        maxLength={3}
                      />

                      {dataRevertToOldValue('TaiBan') && (
                        <MergingButton
                          onClick={() => handleRevertToOldValue('TaiBan')}
                          visible={state?.isFromErrorSyncedDocument || false}
                        />
                      )}
                    </div>
                  </div>
                )}
                {arrAvaliable[2] && (
                  <div className='form-group form-group-sm row'>
                    <label className='text-inbox col-sm-4 font-weight'>Môn học</label>
                    <div className='col-sm-8 flex gap-1 max-md:w-full'>
                      <Select
                        onChange={(e) => {
                          onChangeSuject(e.target.value);
                        }}
                        items={options?.subjectsOptions || []}
                        className='w-full grow'
                        name='IdMonHoc'
                        register={register}
                        errorMessage={errors?.IdMonHoc?.message}
                        disabled={isDisable || !isAllowedAdjustment}
                      />

                      {dataRevertToOldValue('IdMonHoc') && (
                        <MergingButton
                          onClick={() => handleRevertToOldValue('IdMonHoc')}
                          visible={state?.isFromErrorSyncedDocument || false}
                        />
                      )}
                    </div>
                  </div>
                )}

                {arrAvaliable[3] && (
                  <div className='form-group form-group-sm row'>
                    <label className='text-inbox col-sm-4 font-weight'>Khối lớp</label>

                    <div className='col-sm-8 flex gap-1 max-md:w-full'>
                      <AutoComplete
                        items={khoiLop || []}
                        className='w-full grow'
                        name='IdKhoiLop'
                        control={control}
                        errorMessage={errors?.IdKhoiLop?.message}
                        mode='multiple'
                        placeholder='Chọn khối lớp'
                        disabled={!isAllowedAdjustment}
                      />

                      {dataRevertToOldValue('IdKhoiLop') && (
                        <MergingButton
                          onClick={() => handleRevertToOldValue('IdKhoiLop')}
                          visible={state?.isFromErrorSyncedDocument || false}
                        />
                      )}
                    </div>
                  </div>
                )}

                {arrAvaliable[4] && (
                  <div className='form-group form-group-sm row'>
                    <label className='text-inbox col-sm-4 font-weight'>Chủ điểm</label>
                    <div className='col-sm-8 flex gap-1 max-md:w-full'>
                      <Select
                        items={options?.topicOptions || []}
                        className='w-full grow'
                        name='IdChuDiem'
                        register={register}
                        errorMessage={errors?.IdChuDiem?.message}
                        disabled={isDisable || !isAllowedAdjustment}
                      />

                      {dataRevertToOldValue('IdChuDiem') && (
                        <MergingButton
                          onClick={() => handleRevertToOldValue('IdChuDiem')}
                          visible={state?.isFromErrorSyncedDocument || false}
                        />
                      )}
                    </div>
                  </div>
                )}

                {arrAvaliable[5] && (
                  <div className='form-group form-group-sm row'>
                    <label className='text-inbox col-sm-4 font-weight'>Kệ sách</label>
                    <div className='col-sm-8 flex gap-1 max-md:w-full'>
                      <AutoComplete
                        className='grow'
                        name='IdKeSach'
                        control={control}
                        placeholder='Nhập kệ sách'
                        items={options?.bookshelfOptions || []}
                        errorMessage={errors?.IdKeSach?.message}
                        showSearch
                        disabled={!isAllowedAdjustment}
                      />

                      <Button
                        type='button'
                        className='container-refesh-button shrink-0'
                        onClick={handleOpenBookshelfModal}
                        disabled={!isAllowedAdjustment}
                      >
                        <PlusOutlined />
                      </Button>

                      {dataRevertToOldValue('IdKeSach') && (
                        <MergingButton
                          onClick={() => handleRevertToOldValue('IdKeSach')}
                          visible={state?.isFromErrorSyncedDocument || false}
                        />
                      )}
                    </div>
                  </div>
                )}
                <div className='form-group form-group-sm row'>
                  <label className='text-inbox col-sm-4 font-weight'>Ngôn ngữ</label>
                  <div className='col-sm-8 flex gap-1 max-md:w-full'>
                    <AutoComplete
                      items={options?.languageOptions || []}
                      className='w-full grow'
                      name='IdNgonNgu'
                      placeholder='Nhập ngôn ngữ'
                      control={control}
                      errorMessage={errors?.IdNgonNgu?.message}
                      showSearch
                      disabled={!isAllowedAdjustment}
                    />

                    <Button
                      type='button'
                      className='container-refesh-button shrink-0'
                      onClick={handleOpenLanguageModal}
                      disabled={!isAllowedAdjustment}
                    >
                      <PlusOutlined />
                    </Button>

                    {dataRevertToOldValue('IdNgonNgu') && (
                      <MergingButton
                        onClick={() => handleRevertToOldValue('IdNgonNgu')}
                        visible={state?.isFromErrorSyncedDocument || false}
                      />
                    )}
                  </div>
                </div>
                <div className='form-group form-group-sm row'>
                  <label className='text-inbox col-sm-4 font-weight'>Giá tiền</label>
                  <div className='col-sm-8 flex gap-1 max-md:w-full'>
                    <Input
                      containerClassName='grow'
                      placeholder='Nhập giá tiền'
                      name='GiaBia'
                      onKeyPress={(event) => {
                        if (!/[0-9]/.test(event.key)) {
                          event.preventDefault();
                        }
                      }}
                      register={register}
                      errorMessage={errors?.GiaBia?.message}
                      disabled={isDisable || !isAllowedAdjustment}
                    />

                    {dataRevertToOldValue('GiaBia') && (
                      <MergingButton
                        onClick={() => handleRevertToOldValue('GiaBia')}
                        visible={state?.isFromErrorSyncedDocument || false}
                      />
                    )}
                  </div>
                </div>
                <div className='form-group form-group-sm row'>
                  <label className='text-inbox col-sm-4 font-weight'>Thư mục tài liệu</label>
                  <div className='col-sm-8 flex gap-1 max-md:w-full'>
                    <TreeSelect
                      items={options?.documentFolderOptions || []}
                      className='w-full grow'
                      name='IdThuMucSach'
                      control={control}
                      errorMessage={errors?.IdThuMucSach?.message}
                      disabled={isDisable || !isAllowedAdjustment}
                    />

                    {dataRevertToOldValue('IdThuMucSach') && (
                      <MergingButton
                        onClick={() => handleRevertToOldValue('IdThuMucSach')}
                        visible={state?.isFromErrorSyncedDocument || false}
                      />
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className='thongtin-group'>
            <h5 className='title-box'>
              Thông tin chi tiết
              <div className='background-fake'></div>
            </h5>
            <div className='container-maintain grid grid-cols-1 md:grid-cols-12'>
              <div className='col-span-6 md:mr-1'>
                <div className='form-group form-group-sm row'>
                  <label className='text-inbox col-sm-4 font-weight'>Nơi xuất xứ</label>
                  <div className='col-sm-8 flex gap-1 max-md:w-full'>
                    <Input
                      containerClassName='flex-grow'
                      placeholder='Nhập nơi xuất xứ'
                      name='XuatXu'
                      register={register}
                      errorMessage={errors?.XuatXu?.message}
                      disabled={isDisable || !isAllowedAdjustment}
                      maxLength={256}
                    />

                    {dataRevertToOldValue('XuatXu') && (
                      <MergingButton
                        onClick={() => handleRevertToOldValue('XuatXu')}
                        visible={state?.isFromErrorSyncedDocument || false}
                      />
                    )}
                  </div>
                </div>

                {arrAvaliable[6] && (
                  <div className='form-group form-group-sm row'>
                    <label className='text-inbox col-sm-4 font-weight'>Người biên dịch</label>
                    <div className='col-sm-8 flex gap-1 max-md:w-full'>
                      <Input
                        containerClassName='flex-grow'
                        placeholder='Nhập người biên dịch'
                        name='NguoiBienDich'
                        register={register}
                        errorMessage={errors?.NguoiBienDich?.message}
                        disabled={isDisable || !isAllowedAdjustment}
                        maxLength={256}
                      />

                      {dataRevertToOldValue('NguoiBienDich') && (
                        <MergingButton
                          onClick={() => handleRevertToOldValue('NguoiBienDich')}
                          visible={state?.isFromErrorSyncedDocument || false}
                        />
                      )}
                    </div>
                  </div>
                )}
                {/* {arrAvaliable[8] && (
                  <div className='form-group form-group-sm row'>
                    <label className='text-inbox col-sm-4 font-weight'>Phí mượn</label>
                    <div className='col-sm-8 max-md:w-full'>
                      <Input
                        placeholder='Nhập phí mượn'
                        name='PhiMuonSach'
                        register={register}
                        errorMessage={errors?.PhiMuonSach?.message}
                        disabled={isDisable || !isAllowedAdjustment}
                        maxLength={256}
                      />
                    </div>
                  </div>
                )} */}
                <div className='form-group form-group-sm row'>
                  <label className='text-inbox col-sm-4 font-weight'>Số trang</label>
                  <div className='col-sm-8 flex gap-1 max-md:w-full'>
                    <Input
                      containerClassName='flex-grow'
                      placeholder='Nhập số trang'
                      name='SoTrang'
                      register={register}
                      errorMessage={errors?.SoTrang?.message}
                      disabled={isDisable || !isAllowedAdjustment}
                      maxLength={4}
                      // @ts-ignore
                      onWheel={(e) => e.target.blur()}
                      onKeyDown={(e) => {
                        if (e.key === 'ArrowDown' || e.key === 'ArrowUp' || exceptThisSymbols.includes(e.key)) {
                          e.preventDefault();
                        }
                      }}
                      onKeyPress={(event) => {
                        if (!/^[0-9]*\.?[0-9]*$/.test(event.key)) {
                          event.preventDefault();
                        }
                      }}
                    />

                    {dataRevertToOldValue('SoTrang') && (
                      <MergingButton
                        onClick={() => handleRevertToOldValue('SoTrang')}
                        visible={state?.isFromErrorSyncedDocument || false}
                      />
                    )}
                  </div>
                </div>
                {arrAvaliable[10] && (
                  <div className='form-group form-group-sm row'>
                    <label className='text-inbox col-sm-4 font-weight'>Mô tả vật lý</label>
                    <div className='col-sm-8 flex gap-1 max-md:w-full'>
                      <Input
                        containerClassName='flex-grow'
                        placeholder='Nhập mô tả vật lý'
                        name='MoTaVatLy'
                        register={register}
                        errorMessage={errors?.MoTaVatLy?.message}
                        disabled={isDisable || !isAllowedAdjustment}
                        maxLength={256}
                      />

                      {dataRevertToOldValue('MoTaVatLy') && (
                        <MergingButton
                          onClick={() => handleRevertToOldValue('MoTaVatLy')}
                          visible={state?.isFromErrorSyncedDocument || false}
                        />
                      )}
                    </div>
                  </div>
                )}
                {arrAvaliable[11] && (
                  <div className='form-group form-group-sm row'>
                    <label className='text-inbox col-sm-4 font-weight'>Khổ mẫu</label>
                    <div className='col-sm-8 flex gap-1 max-md:w-full'>
                      <Input
                        containerClassName='grow'
                        placeholder='Nhập khổ mẫu'
                        name='CoSach'
                        register={register}
                        errorMessage={errors?.CoSach?.message}
                        disabled={isDisable || !isAllowedAdjustment}
                        maxLength={10}
                      />

                      {dataRevertToOldValue('CoSach') && (
                        <MergingButton
                          onClick={() => handleRevertToOldValue('CoSach')}
                          visible={state?.isFromErrorSyncedDocument || false}
                        />
                      )}
                    </div>
                  </div>
                )}

                {arrAvaliable[12] && (
                  <div className='form-group form-group-sm row'>
                    <label className='text-inbox col-sm-4 font-weight'>Tài liệu kèm theo</label>
                    <div className='col-sm-8 flex gap-1 max-md:w-full'>
                      <Input
                        containerClassName='grow'
                        placeholder='Nhập tài liệu kèm theo'
                        name='TaiLieuDinhKem'
                        register={register}
                        errorMessage={errors?.TaiLieuDinhKem?.message}
                        disabled={isDisable || !isAllowedAdjustment}
                        maxLength={256}
                      />

                      {dataRevertToOldValue('TaiLieuDinhKem') && (
                        <MergingButton
                          onClick={() => handleRevertToOldValue('TaiLieuDinhKem')}
                          visible={state?.isFromErrorSyncedDocument || false}
                        />
                      )}
                    </div>
                  </div>
                )}

                {arrAvaliable[17] && (
                  <div className='form-group form-group-sm row'>
                    <label className='text-inbox col-sm-4 font-weight'>Mã màu</label>
                    <div className='col-sm-8 flex gap-1 max-md:w-full'>
                      <Select
                        items={options?.colorOptions || []}
                        className='w-full grow'
                        name='IdMaMau'
                        register={register}
                        errorMessage={errors?.IdMaMau?.message}
                        disabled={isDisable || !isAllowedAdjustment}
                      />

                      <Button
                        type='button'
                        className='container-refesh-button shrink-0'
                        onClick={handleOpenColorModal}
                        disabled={!isAllowedAdjustment}
                      >
                        <PlusOutlined></PlusOutlined>
                      </Button>

                      {dataRevertToOldValue('IdMaMau') && (
                        <MergingButton
                          onClick={() => handleRevertToOldValue('IdMaMau')}
                          visible={state?.isFromErrorSyncedDocument || false}
                        />
                      )}
                    </div>
                  </div>
                )}
              </div>

              <div className='col-span-6 md:ml-1'>
                {arrAvaliable[13] && (
                  <div className='form-group form-group-sm row'>
                    <label className='text-inbox col-sm-4 font-weight'>ISBN/ISSN</label>
                    <div className='col-sm-8 flex gap-1 max-md:w-full'>
                      <Input
                        containerClassName='grow'
                        placeholder='Nhập ISBN/ISSN'
                        name='ISBN'
                        register={register}
                        errorMessage={errors?.ISBN?.message}
                        disabled={isDisable || !isAllowedAdjustment}
                        maxLength={256}
                      />

                      {dataRevertToOldValue('ISBN') && (
                        <MergingButton
                          onClick={() => handleRevertToOldValue('ISBN')}
                          visible={state?.isFromErrorSyncedDocument || false}
                        />
                      )}
                    </div>
                  </div>
                )}

                {arrAvaliable[14] && (
                  <div className='form-group form-group-sm row'>
                    <label className='text-inbox col-sm-4 font-weight'>SKU</label>
                    <div className='col-sm-8 flex gap-1 max-md:w-full'>
                      <Input
                        containerClassName='grow'
                        placeholder='Nhập SKU'
                        name='SKU'
                        register={register}
                        errorMessage={errors?.SKU?.message}
                        disabled={isDisable || !isAllowedAdjustment}
                        maxLength={256}
                      />

                      {dataRevertToOldValue('SKU') && (
                        <MergingButton
                          onClick={() => handleRevertToOldValue('SKU')}
                          visible={state?.isFromErrorSyncedDocument || false}
                        />
                      )}
                    </div>
                  </div>
                )}

                {arrAvaliable[15] && (
                  <div className='form-group form-group-sm row'>
                    <label className='text-inbox col-sm-4 font-weight'>Khung phân loại</label>
                    <div className='col-sm-8 flex max-md:w-full'>
                      <Select
                        items={options?.DDCOptions || []}
                        className='w-full grow'
                        name={isDay19 ? 'Day19' : 'DDC'}
                        register={register}
                        errorMessage={isDay19 ? errors?.Day19?.message : errors?.DDC?.message}
                        disabled={isDisable || !isAllowedAdjustment}
                      />

                      {dataRevertToOldValue(isDay19 ? 'Day19' : 'DDC') && (
                        <MergingButton
                          onClick={() => handleRevertToOldValue(isDay19 ? 'Day19' : 'DDC')}
                          visible={state?.isFromErrorSyncedDocument || false}
                        />
                      )}
                    </div>
                  </div>
                )}

                {arrAvaliable[16] && (
                  <div className='form-group form-group-sm row'>
                    <label className='text-inbox col-sm-4 font-weight'>LLC</label>
                    <div className='col-sm-8 flex gap-1 max-md:w-full'>
                      <Input
                        containerClassName='grow'
                        placeholder='Nhập LLC'
                        name='LLC'
                        register={register}
                        errorMessage={errors?.LLC?.message}
                        disabled={isDisable || !isAllowedAdjustment}
                        maxLength={256}
                      />

                      {dataRevertToOldValue('LLC') && (
                        <MergingButton
                          onClick={() => handleRevertToOldValue('LLC')}
                          visible={state?.isFromErrorSyncedDocument || false}
                        />
                      )}
                    </div>
                  </div>
                )}
                {arrAvaliable[18] && (
                  <div className='form-group form-group-sm row'>
                    <label className='text-inbox col-sm-4 font-weight'>Tóm tắt</label>
                    <div className='col-sm-8 flex gap-1 max-md:w-full'>
                      <Input
                        containerClassName='grow'
                        placeholder='Nhập tóm tắt'
                        name='TomTat'
                        register={register}
                        errorMessage={errors?.TomTat?.message}
                        disabled={isDisable || !isAllowedAdjustment}
                        maxLength={256}
                      />

                      {dataRevertToOldValue('TomTat') && (
                        <MergingButton
                          onClick={() => handleRevertToOldValue('TomTat')}
                          visible={state?.isFromErrorSyncedDocument || false}
                        />
                      )}
                    </div>
                  </div>
                )}

                {arrAvaliable[9] && (
                  <div className='form-group form-group-sm row'>
                    <label className='text-inbox col-sm-4 font-weight'>Minh họa</label>
                    <div className='col-sm-8 flex gap-1 max-md:w-full'>
                      <Input
                        containerClassName='grow'
                        placeholder='Nhập minh họa'
                        name='MinhHoa'
                        register={register}
                        errorMessage={errors?.MinhHoa?.message}
                        disabled={isDisable || !isAllowedAdjustment}
                        maxLength={256}
                      />

                      {dataRevertToOldValue('MinhHoa') && (
                        <MergingButton
                          onClick={() => handleRevertToOldValue('MinhHoa')}
                          visible={state?.isFromErrorSyncedDocument || false}
                        />
                      )}
                    </div>
                  </div>
                )}

                {arrAvaliable[19] && (
                  <div className='form-group form-group-sm row'>
                    <label className='text-inbox col-sm-4 font-weight'>Tên tùng thư</label>
                    <div className='col-sm-8 flex gap-1 max-md:w-full'>
                      <Input
                        containerClassName='grow'
                        placeholder='Nhập tên tùng thư'
                        name='TungThu'
                        register={register}
                        errorMessage={errors?.TungThu?.message}
                        disabled={isDisable || !isAllowedAdjustment}
                        maxLength={256}
                      />

                      {dataRevertToOldValue('TungThu') && (
                        <MergingButton
                          onClick={() => handleRevertToOldValue('TungThu')}
                          visible={state?.isFromErrorSyncedDocument || false}
                        />
                      )}
                    </div>
                  </div>
                )}

                {arrAvaliable[19] && (
                  <div className='form-group form-group-sm row'>
                    <label className='text-inbox col-sm-4 font-weight'>Phụ chú</label>
                    <div className='col-sm-8 flex gap-1 max-md:w-full'>
                      <Input
                        containerClassName='grow'
                        placeholder='Nhập tên phụ chú'
                        name='PhuChu'
                        register={register}
                        errorMessage={errors?.PhuChu?.message}
                        disabled={isDisable || !isAllowedAdjustment}
                        maxLength={256}
                      />

                      {dataRevertToOldValue('PhuChu') && (
                        <MergingButton
                          onClick={() => handleRevertToOldValue('PhuChu')}
                          visible={state?.isFromErrorSyncedDocument || false}
                        />
                      )}
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </>
      )
    };
  }, [
    boSuuTapItem,
    register,
    errors?.IdBoSuuTap?.message,
    errors?.TenSach?.message,
    errors?.TenSongNgu?.message,
    errors.ListTacGiaJson?.message,
    errors.ListTacGiaPhuJson?.message,
    errors?.IdNhaXuatBan?.message,
    errors?.IdNoiXuatBan?.message,
    errors?.NamXuatBan?.message,
    errors?.TaiBan?.message,
    errors?.IdMonHoc?.message,
    errors?.IdKhoiLop?.message,
    errors?.IdChuDiem?.message,
    errors?.IdKeSach?.message,
    errors?.IdNgonNgu?.message,
    errors?.GiaBia?.message,
    errors?.IdThuMucSach?.message,
    errors?.XuatXu?.message,
    errors?.NguoiBienDich?.message,
    errors?.PhiMuonSach?.message,
    errors?.SoTrang?.message,
    errors?.MoTaVatLy?.message,
    errors?.CoSach?.message,
    errors?.TaiLieuDinhKem?.message,
    errors?.IdMaMau?.message,
    errors?.ISBN?.message,
    errors?.SKU?.message,
    errors?.Day19?.message,
    errors?.DDC?.message,
    errors?.LLC?.message,
    errors?.TomTat?.message,
    errors?.MinhHoa?.message,
    errors?.TungThu?.message,
    errors?.PhuChu?.message,
    isDisable,
    arrAvaliable,
    control,
    tacGiaItem,
    nhaXuatBanItem,
    noiXuatBanItem,
    monHocItem,
    chuDiemItem,
    keSachItem,
    ngonNguItem,
    thuMucTaiLieuItem,
    maMauItem,
    isDay19,
    DDC_Day19
  ]);

  const inputTab = {
    label: `Nhập kho`,
    key: `input`,
    children: (
      <>
        {!Boolean(bookId) ? (
          <Import
            khoSachOptions={khoSachItem}
            nguonCungCapOptions={nguonCungCapItem}
            trangThaiOptions={trangThaiItem}
            fields={fields}
            append={append}
            remove={remove}
            register={register}
            errors={errors.NhapKho}
            control={control}
          />
        ) : (
          <WarehouseInfor {...(bookData?.data?.Item as GetBookByIdData)} onSuccess={handleOnSuccess} />
        )}
      </>
    )
  };

  const marc21 = {
    label: `Marc21`,
    key: `marc21`,
    children: <div className='whitespace-pre-wrap'>{bookData?.data.Item?.SachDTO?.MARC21}</div>
  };

  const contentTab = {
    label: `Nội dung sách`,
    key: `content`,
    children: (
      <BookContent
        type={contentType}
        bookId={bookId}
        bookContentData={bookContentData?.data.Item}
        getFile={handleGetFile}
        getImagesFile={handleGetImageFile}
        getContentType={handleGetContentType}
        getImageFromData={handleGetImagesFromData}
      />
    )
  };

  const permissionTab = {
    label: 'Phân quyền',
    key: `permission`,
    children: (
      <PermissionTab
        formValues={permissionFormValue}
        onChangeAllowPermission={handleChangeAllowPermission}
        onChangPermissionOfLib={handleChangePermissionOfLib}
        onChangeAllowFee={handleChangeFee}
      />
    )
  };

  const tabMenu = Boolean(bookId) ? [detailTab, marc21, inputTab, contentTab] : [detailTab, inputTab, contentTab];
  if (permission?.EnableDigital) {
    tabMenu.push(permissionTab);
  }

  return (
    <div className='publication-page p-5'>
      <Title title={Boolean(bookId) ? 'Cập nhật' : 'Thêm sách'} />
      <form className='mt-10 grid w-full grid-cols-1 md:grid-cols-12'>
        <div className='relative col-span-2 mb-10 flex h-full w-full justify-center overflow-hidden'>
          <input
            type='file'
            accept='image/jpeg,image/gif,image/jpg,image/png,image/bmp'
            className='hidden'
            ref={fileInutRef}
            onChange={handleOnFileChange}
            disabled={isDisable}
          />

          <div className='group relative h-44 w-44'>
            <div className='container-image-book my-5 h-44 w-44'>
              <img
                src={resultPreview ? resultPreview : '/content/Book.png'}
                alt='Hình đại diện'
                className='book-image h-full w-full object-cover shadow-md transition-all duration-200 ease-in-out group-hover:blur-[2px]'
              />
            </div>

            <button
              type='button'
              onClick={handleAccessFileInputRef}
              className='absolute inset-0 my-5 flex  h-44 w-44 scale-110 items-center justify-center p-5 opacity-0 transition-all duration-200 ease-out group-hover:scale-100 group-hover:bg-black/40 group-hover:opacity-100'
            >
              <span className='mr-2 text-white'>Chọn hình ảnh</span> <CameraFilled style={{ color: 'white' }} />
            </button>
          </div>
        </div>
        <div className='custom-row col-span-10 px-2'>
          <div className='thongtin-group-tab relative'>
            <Tabs type='card' size='large' items={tabMenu} activeKey={activeKey} onChange={handleChangeTab} />
          </div>

          <div className='mt-5 flex justify-end px-2'>
            <Button type='button' variant='secondary' className='font-semibold' onClick={handleBack}>
              Quay về
            </Button>
            {state?.isFromErrorSyncedDocument && bookId && (
              <Button
                type='button'
                className='ml-2 font-semibold'
                variant={isDisable ? 'disabled' : 'danger'}
                disabled={isDisable}
                onClick={() => {
                  setIsVisiable(true);
                }}
              >
                Xóa sách
              </Button>
            )}
            {!!bookId ? (
              <Button
                variant='default'
                type='button'
                className={isAllowedAdjustment ? 'ml-2 font-semibold' : 'hidden'}
                onClick={onSubmit}
                disabled={isDisable}
              >
                Cập nhật
              </Button>
            ) : (
              <Button
                variant='default'
                type='button'
                className={isAllowedAdjustment ? 'ml-2 font-semibold' : 'hidden'}
                onClick={onSubmit}
                disabled={isDisable}
              >
                Thêm mới
              </Button>
            )}
          </div>
        </div>
      </form>

      <SettingModal
        arrAvaliable={arrAvaliable}
        open={visibleModal}
        setVisibleModal={setVisibleModal}
        setArrAvaliable={setArrAvaliable}
        setCookie={setCookie}
      />

      <ModalDelete
        open={isVisiable}
        closable={false}
        title={
          <TitleDelete firstText={'Bạn có chắn chắn muốn xóa đầu sách'} secondText={getValues('TenSach')}></TitleDelete>
        }
        handleCancel={() => {
          setIsVisiable(false);
        }}
        handleOk={() => {
          deleteBook(bookId || '');
        }}
        loading={isLoadingDelete}
      />

      <AuthorModal
        open={visibleAuthorModal.visible}
        title='Thêm tác giả'
        onBack={handleCloseAuthorModal}
        onSuccess={handleAddAuthorSuccess}
      />

      <PublishingCompanyModal
        open={visiblePublishingModal}
        title='Thêm nhà xuất bản'
        onBack={handleClosePublishingModal}
      />

      <PlacePublicationModal
        open={visiblePlacePublicationModal}
        title='Thêm nơi xuất bản'
        onBack={handleClosePlacePublicationModal}
        onSuccess={handleCreatePlacePulicationSuccess}
      />

      <BookshelfModal
        open={visibleBookshelfModal}
        title='Thêm kệ sách'
        onBack={handleCloseBookshelfModal}
        onSuccess={handleCreateBookshelfSuccess}
      />

      <LanguageModal
        open={visibleLanguageModal}
        title='Thêm ngôn ngữ'
        onBack={handleCloseLanguageModal}
        onCreateSuccess={handleCreateLanguageSuccess}
      />

      <ColorModal open={visibleColorModal} title='' onBack={handleCloseColorModal} />

      <Loading
        open={
          isLoadingCreateBook ||
          isLoadingCreatePhieuNhapKho ||
          fetchingLoadingForm ||
          (isLoadingGetBookById && Boolean(bookId)) ||
          isLoadingUploadPDF ||
          isLoadingEditBook ||
          isLoadingUploadImages ||
          (isLoadingBookContent && Boolean(bookId)) ||
          isLoadingUpdateMedia
        }
      />
    </div>
  );
};

export default AddPublicationPage;
