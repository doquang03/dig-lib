type Audit = {
  Id: string;
  CreateDateTime: string;
  MaKiemKe: string;
  IdUserAdmin: string;
  UserName: string;
  TenPhieu: string;
  IdKho: string;
  NgayVaoSo: string;
  NgayChotSo: string;
  GhiChu: string;
  CountSach: number;
  CountGia: number;
};
