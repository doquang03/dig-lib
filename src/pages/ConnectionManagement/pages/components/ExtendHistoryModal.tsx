import { useMutation, useQuery } from '@tanstack/react-query';
import { connectionApis } from 'apis';
import { BaseModal, ModalDelete, TitleDelete } from 'components';
import dayjs from 'dayjs';
import { useState } from 'react';
import { toast } from 'react-toastify';

type Props = {
  selectedRecord?: UnitKey;
  onCancel: VoidFunction;
  onSuccess?: VoidFunction;
};

const ExtendHistoryModal = (props: Props) => {
  const { selectedRecord, onCancel, onSuccess } = props;

  const [cancelPackage, setCancelPackage] = useState<{
    index: number;
    ExtensionTime: number;
    ExpiredTime: any;
    Id: string;
  }>();

  const { data, refetch, isRefetching } = useQuery({
    queryKey: ['getExtensionTimePackages', selectedRecord],
    queryFn: () => connectionApis.getListExtensionTimePackages(selectedRecord?.IdKey + ''),
    enabled: !!selectedRecord?.IdKey
  });

  const { CreateDateTime, ListModel, Value } = data?.data.Item || { Value: '', CreateDateTime: '', ListModel: [] };

  const { mutate, isLoading } = useMutation({
    mutationFn: () => connectionApis.cancelExtensionTimePackage(cancelPackage?.Id + ''),
    onSuccess: () => {
      refetch();
      toast.success('Hủy gói gia hạn thành công');
      setCancelPackage(undefined);
      if (onSuccess) onSuccess();
    }
  });

  const hasUnending = ListModel.findIndex((_package) => _package.ExtensionTime === null);

  return (
    <>
      <BaseModal
        title='LỊCH SỬ GIA HẠN KEY'
        onInvisile={onCancel}
        onOk={() => {}}
        open={!!selectedRecord}
        width={750}
        hasUpdate={false}
      >
        <div className='mt-3 grid grid-cols-12 items-center'>
          <p className='col-span-2 h-auto align-middle font-bold'>Key</p>

          <p className='col-span-10 border-[1px] bg-[#F2F2F2] p-2'>{Value || '--'}</p>
        </div>

        <div className='mt-3 grid grid-cols-12 items-center'>
          <div className='col-span-2 h-auto font-bold'>
            <p>Thời gian tạo</p>
          </div>

          <p className='col-span-8'>{dayjs(CreateDateTime).format('HH:mm DD/MM/YYYY')}</p>
        </div>

        {hasUnending !== -1 ? (
          <>
            {ListModel.map((_package, index) => (
              <div className='mt-3 grid grid-cols-12 items-center gap-2' key={_package.Id}>
                <div className='col-span-2 h-auto font-bold'>
                  <p>Gói gia hạn {index + 1}</p>
                </div>

                <div className='col-span-3'>
                  <p className='whitespace-nowrap'>
                    {' '}
                    {!_package.ExtensionTime
                      ? 'Vĩnh viễn'
                      : `Hết hạn sau ${_package.ExtensionTime / (24 * 60 * 60)} ngày`}
                  </p>
                </div>

                <div className='col-span-1'>
                  <svg width={18} height={12} viewBox='0 0 18 12' fill='none' xmlns='http://www.w3.org/2000/svg'>
                    <path
                      d='M4.37114e-07 7L14.17 7L10.59 10.59L12 12L18 6L12 -5.24537e-07L10.59 1.41L14.17 5L6.11959e-07 5L4.37114e-07 7Z'
                      fill='black'
                    />
                  </svg>
                </div>

                <div className='col-span-3'>
                  <p className='whitespace-nowrap text-tertiary-30'>
                    Ngày hết hạn:{' '}
                    {!_package.ExpiredTime ? '--' : dayjs(_package.ExpiredTime).format('HH:mm DD/MM/YYYY')}
                  </p>
                </div>

                {hasUnending === index && (
                  <div className='col-span-3 flex w-full items-center justify-end'>
                    <button
                      className='boder flex items-center gap-1 rounded-md border border-danger-30 p-2 text-danger-30'
                      onClick={() => setCancelPackage({ ..._package, index })}
                    >
                      <svg width={13} height={17} viewBox='0 0 13 17' fill='none' xmlns='http://www.w3.org/2000/svg'>
                        <path
                          d='M0.928571 15.1111C0.928571 16.15 1.76429 17 2.78571 17H10.2143C11.2357 17 12.0714 16.15 12.0714 15.1111V3.77778H0.928571V15.1111ZM3.21286 8.38667L4.52214 7.055L6.5 9.05722L8.46857 7.055L9.77786 8.38667L7.80929 10.3889L9.77786 12.3911L8.46857 13.7228L6.5 11.7206L4.53143 13.7228L3.22214 12.3911L5.19071 10.3889L3.21286 8.38667ZM9.75 0.944444L8.82143 0H4.17857L3.25 0.944444H0V2.83333H13V0.944444H9.75Z'
                          fill='#A23434'
                        />
                      </svg>
                      Hủy gia hạn
                    </button>
                  </div>
                )}
              </div>
            ))}
          </>
        ) : (
          <>
            {ListModel.map((_package, index) => (
              <div className='mt-3 grid grid-cols-12 items-center gap-2' key={_package.Id}>
                <div className='col-span-2 h-auto font-bold'>
                  <p>Gói gia hạn {index + 1}</p>
                </div>

                <div className='col-span-3'>
                  <p className='whitespace-nowrap'>
                    {' '}
                    {!_package.ExtensionTime
                      ? 'Vĩnh viễn'
                      : `Hết hạn sau ${_package.ExtensionTime / (24 * 60 * 60)} ngày`}
                  </p>
                </div>

                <div className='col-span-1'>
                  <svg width={18} height={12} viewBox='0 0 18 12' fill='none' xmlns='http://www.w3.org/2000/svg'>
                    <path
                      d='M4.37114e-07 7L14.17 7L10.59 10.59L12 12L18 6L12 -5.24537e-07L10.59 1.41L14.17 5L6.11959e-07 5L4.37114e-07 7Z'
                      fill='black'
                    />
                  </svg>
                </div>

                <div className='col-span-3'>
                  <p
                    className={`whitespace-nowrap ${
                      dayjs(_package.ExpiredTime).isAfter(dayjs()) ? 'text-tertiary-30' : 'text-danger-10'
                    }`}
                  >
                    Ngày hết hạn:{' '}
                    {!_package.ExpiredTime ? '--' : dayjs(_package.ExpiredTime).format('HH:mm DD/MM/YYYY')}
                  </p>
                </div>

                <div className='col-span-3 flex w-full items-center justify-end'>
                  {dayjs(_package.ExpiredTime).isAfter(dayjs()) && (
                    <button
                      className='boder flex items-center gap-1 rounded-md border border-danger-30 p-2 text-danger-30'
                      onClick={() => setCancelPackage({ ..._package, index })}
                    >
                      <svg width={13} height={17} viewBox='0 0 13 17' fill='none' xmlns='http://www.w3.org/2000/svg'>
                        <path
                          d='M0.928571 15.1111C0.928571 16.15 1.76429 17 2.78571 17H10.2143C11.2357 17 12.0714 16.15 12.0714 15.1111V3.77778H0.928571V15.1111ZM3.21286 8.38667L4.52214 7.055L6.5 9.05722L8.46857 7.055L9.77786 8.38667L7.80929 10.3889L9.77786 12.3911L8.46857 13.7228L6.5 11.7206L4.53143 13.7228L3.22214 12.3911L5.19071 10.3889L3.21286 8.38667ZM9.75 0.944444L8.82143 0H4.17857L3.25 0.944444H0V2.83333H13V0.944444H9.75Z'
                          fill='#A23434'
                        />
                      </svg>
                      Hủy gia hạn
                    </button>
                  )}
                </div>
              </div>
            ))}
          </>
        )}
      </BaseModal>

      <ModalDelete
        open={!!cancelPackage}
        title={
          <TitleDelete
            firstText='Bạn có chắc chắn muốn hủy gia hạn'
            secondText={`Gói gia hạn ${(cancelPackage?.index || 0) + 1} ${
              cancelPackage?.ExtensionTime
                ? `(Hết hạn sau ${(cancelPackage?.ExtensionTime || 0) / (24 * 60 * 60)} ngày)`
                : `(Vĩnh viễn)`
            } `}
          />
        }
        handleOk={() => mutate()}
        loading={isLoading || isRefetching}
        labelOK='Xác nhận'
        handleCancel={() => setCancelPackage(undefined)}
      />
    </>
  );
};

export default ExtendHistoryModal;
