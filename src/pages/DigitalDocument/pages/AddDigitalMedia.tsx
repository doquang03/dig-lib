import { CameraFilled, CloseSquareFilled } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Checkbox, DatePicker, Radio, RadioChangeEvent } from 'antd';
import { CheckboxChangeEvent } from 'antd/es/checkbox';
import locale from 'antd/es/date-picker/locale/vi_VN';
import { digitalDocumentApis, errorSyncDocuments } from 'apis';
import { Button, Loading, Title } from 'components';
import { AUDIO_FILE_TYPE, FORMAT_DATE_PICKER, VIDEO_FILE_TYPE } from 'constants/config';
import dayjs, { Dayjs } from 'dayjs';
import AuthorModal from 'pages/Publications/AddPublication/components/AuthorModal';
import LanguageModal from 'pages/Publications/AddPublication/components/LanguageModal';
import PublishingCompanyModal from 'pages/Publications/AddPublication/components/PublishingCompanyModal';
import { ChangeEvent, useMemo, useRef, useState } from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { InputWithLable, SelectWithLabel, TagSelectWithLabel } from '../components';
import { getEmbed } from 'utils/utils';
import { Revert } from 'pages/TitleApproval/icons';

const exceptThisSymbols = ['e', 'E', '+', '-', '.'];

const AddDigitalMedia = () => {
  const [formValues, setFormValues] = useState<{
    title: string;
    readingFee?: string;
    mainAuthors: string[];
    viceAuthors: string[];
    allowedReading: boolean;
    readingPermission?: number;
    allowedDownload: boolean;
    downloadPermission?: number;
    documentPercent: string;
    publisher?: string;
    language?: string;
    publishDate?: string;
    numberOfCopies?: number;
    source: string;
    size: string;
    reference: string;
    price: string;
    summary: string;
    isbn: string;
    bookCover?: string;
    ddc_19day?: string;
  }>({
    title: '',
    readingFee: '',
    mainAuthors: [],
    viceAuthors: [],
    allowedReading: false,
    allowedDownload: false,
    documentPercent: '',
    source: '',
    size: '',
    reference: '',
    price: '',
    summary: '',
    isbn: '',
    readingPermission: 1,
    downloadPermission: 1
  });
  const [addableModal, setAddableModal] = useState<'mainAuthor' | 'viceAuthor' | 'nxb' | 'language' | 'setting'>();
  const [file, setFile] = useState<File>();
  const [bookCoverPreview, setBookCoverPreview] = useState<File>();
  const [authors, setAuthors] = useState<Option[]>([]);
  const [listOfFiles, setListOfFiles] = useState<DetailDigitalDocument['ListMedia']>();

  const navigate = useNavigate();

  const { id } = useParams();

  const { state } = useLocation();

  const {
    data: optionData,
    isLoading: loadingOptionsData,
    isRefetching,
    refetch: refetchGetOptions,
    status
  } = useQuery({
    queryKey: ['getOptionsCreationDigitalDocument'],
    queryFn: () => digitalDocumentApis.getOptions(),
    onSuccess: (data) => {
      setAuthors(data.data.Item.Options_TacGia);
    }
  });

  const { isLoading: loadingDigitalDocument, refetch: refetchDocument } = useQuery({
    queryKey: ['getDetailDocument', id, status === 'success'],
    queryFn: () => digitalDocumentApis.getDetailDocument(id + ''),
    enabled: !!id && status === 'success',
    onSuccess: (data) => {
      const doc = data.data.Item;

      setFormValues({
        bookCover: doc.LinkBiaSach,
        title: doc.TenSach,
        mainAuthors: doc.listIdTacGia,
        viceAuthors: doc.listIdTacGiaPhu,
        publisher: doc.IdNhaXuatBan,
        language: doc.IdNgonNgu,
        publishDate: doc.NgayXuatBan,
        numberOfCopies: doc.SoBanSao,
        allowedReading: doc.QuyenDoc !== 0 ? true : false,
        readingPermission: doc.QuyenDoc || 1,
        allowedDownload: doc.QuyenTai !== 0 ? true : false,
        downloadPermission: doc.QuyenTai || 1,
        ddc_19day: IsDay19 ? doc.Day19 : doc.DDC,
        isbn: doc.ISBN,
        source: doc.NguonTaiLieu,
        size: '',
        reference: doc.LienKet,
        price: !!doc.GiaBia
          ? doc.GiaBia.trim()
              .replace(/\D/g, '')
              .replace(/\B(?=(\d{3})+(?!\d))/g, ',')
          : '',
        summary: doc.TomTat,
        documentPercent: doc.TiLeTaiXuong,
        readingFee: doc.PhiDocSach
          ? doc.PhiDocSach.trim()
              .replace(/\D/g, '')
              .replace(/\B(?=(\d{3})+(?!\d))/g, ',')
          : ''
      });

      setListOfFiles(doc.ListMedia);
    }
  });

  // call api able to get book's old data
  const { data: oldBookData } = useQuery({
    queryKey: ['getBookOldData', id, state?.isFromErrorSyncedDocument, state?.errorSyncedDocumentId],
    queryFn: () => errorSyncDocuments.getOldDetailBookData(state?.errorSyncedDocumentId + ''),
    enabled: !!state && state?.isFromErrorSyncedDocument && !!state?.errorSyncedDocumentId && !!id
  });

  const oldBookValue = oldBookData?.data?.Item?.Model;

  const { mutate: update, isLoading: loadingUpDate } = useMutation({
    mutationFn: digitalDocumentApis.updateDigitalDocument,
    onSuccess: () => {
      toast.success('Cập nhật tài liệu thành công');
      refetchDocument();
      refetchGetOptions();
      setListOfFiles(undefined);
      setFile(undefined);
    }
  });

  const { mutate: createDigitalDocument, isLoading: loadingCreateDigitalDocument } = useMutation({
    mutationFn: digitalDocumentApis.createDigitalDocument,
    onSuccess: () => {
      setFormValues({
        title: '',
        readingFee: '',
        mainAuthors: [],
        viceAuthors: [],
        allowedReading: false,
        allowedDownload: false,
        documentPercent: '',
        source: '',
        size: '',
        reference: '',
        price: '',
        summary: '',
        isbn: ''
      });

      setFile(undefined);

      toast.success('Tạo tài liêu thành công');

      navigate(-1);
    }
  });

  const {
    Options_DDC_Day19 = [],
    Options_Language,
    Options_NXB,
    IsDay19
  } = optionData?.data.Item || {
    IsDay19: false,
    Options_DDC_Day19: [],
    Options_Language: [],
    Options_NXB: []
  };

  const fileInutRef = useRef<HTMLInputElement | null>(null);
  const bookCoverInputRef = useRef<HTMLInputElement | null>(null);

  const previewImage = useMemo(() => {
    if (bookCoverPreview) return URL.createObjectURL(bookCoverPreview);

    return '';
  }, [bookCoverPreview]);

  const src = useMemo(() => file && URL.createObjectURL(file), [file]);

  const resultPreview = useMemo(() => {
    return previewImage ? previewImage : formValues?.bookCover;
  }, [formValues?.bookCover, previewImage]);

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { value, name } = e.target;

    setFormValues((prevState) => {
      return { ...prevState, [name]: value };
    });
  };

  const handleReadingFee = (e: ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;

    setFormValues((prevState) => {
      return {
        ...prevState,
        readingFee: value
          .trim()
          .replace(/\D/g, '')
          .replace(/\B(?=(\d{3})+(?!\d))/g, ',')
      };
    });
  };

  const handleDocunemtPercent = (e: ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;

    setFormValues((prevState) => {
      return {
        ...prevState,
        documentPercent: +value > 100 ? '100' : value
      };
    });
  };

  const handleAddableModal = (value: 'mainAuthor' | 'viceAuthor' | 'nxb' | 'language') => setAddableModal(value);

  const handleCloseModal = () => setAddableModal(undefined);

  const handleAccessFileInputRef = () => fileInutRef.current?.click();

  const handleAddAuthorSuccess = (data: string, id: string) => {
    refetchGetOptions();

    addableModal === 'mainAuthor' &&
      setFormValues((prevState) => {
        return { ...prevState, mainAuthors: [id, ...formValues.mainAuthors] };
      });

    addableModal === 'viceAuthor' &&
      setFormValues((prevState) => {
        return { ...prevState, viceAuthors: [id, ...formValues.viceAuthors] };
      });
  };

  const hanldeAddSuccessfully = async (value: string, name: string) => {
    await refetchGetOptions();

    setFormValues((prevState) => {
      return { ...prevState, [name]: value };
    });
  };

  const handleChangeAuthor = (value: string[], name: string) => {
    setFormValues((prevState) => {
      return { ...prevState, [name]: value };
    });
  };

  const handleOnFileChange = (e: ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files?.[0];

    if (!fileFromLocal) {
      return;
    }

    const fileSizeInMb = Number((fileFromLocal?.size / (1024 * 1024)).toFixed(2));

    // if (bookContentValue === 'audio') {
    if (fileFromLocal.type.includes('audio')) {
      if (fileSizeInMb > 200) {
        return toast.warning('Dung lượng file audio phải nhỏ hơn 200Mb');
      }
    }

    if (fileFromLocal.type.includes('video')) {
      if (fileSizeInMb > 500) {
        return toast.warning('Dung lượng file video phải nhỏ hơn 500Mb');
      }
    }

    setListOfFiles(undefined);
    setFile(fileFromLocal);
  };

  const handleAccessBookCoverFileInputRef = () => bookCoverInputRef.current?.click();

  const handleBookCoverChange = (e: ChangeEvent<HTMLInputElement>) => {
    const fileFromLocal = e.target?.files?.[0];
    if (!fileFromLocal) {
      return;
    }

    if (fileFromLocal && !fileFromLocal.type.startsWith('image')) return;

    setBookCoverPreview(fileFromLocal);
  };

  const handleChangePermission = (e: CheckboxChangeEvent) => {
    const { value, name } = e.target;

    if (!name) return;

    setFormValues((prevState) => {
      return { ...prevState, [name]: !value };
    });
  };

  const handleChangeReadingPermission = (e: RadioChangeEvent) => {
    if (!e) return;

    setFormValues((prevState) => {
      return { ...prevState, readingPermission: +e.target.value };
    });
  };

  const handleChangeSelect = (e: ChangeEvent<HTMLSelectElement>) => {
    const { value, name } = e.target;

    setFormValues((prevState) => {
      return { ...prevState, [name]: value };
    });
  };

  const handleChangeDownloadPermission = (e: RadioChangeEvent) => {
    if (!e) return;

    setFormValues((prevState) => {
      return { ...prevState, downloadPermission: +e.target.value };
    });
  };

  const handleChangePublishDate = (value: Dayjs | null) => {
    setFormValues((prevState) => {
      return { ...prevState, publishDate: value?.toISOString() };
    });
  };

  const handleSubmit = () => {
    const formData = new FormData();

    !!id && formData.append('Id', id);
    bookCoverPreview && formData.append('LinkBiaSach', bookCoverPreview);
    formData.append('TenSach', formValues.title.replace(/\s\s+/g, ' '));
    formValues.mainAuthors.length && formValues.mainAuthors.forEach((value) => formData.append('ListIdTacGia', value));
    formValues.viceAuthors.length &&
      formValues.viceAuthors.forEach((value) => formData.append('ListIdTacGiaPhu', value));
    formValues.publisher && formData.append('IdNhaXuatBan', formValues.publisher);
    formValues.language && formData.append('IdNgonNgu', formValues.language);
    formValues.isbn && formData.append('ISBN', formValues.isbn);
    formValues.publishDate && formData.append('NgayXuatBan', dayjs(formValues.publishDate).toISOString());
    formValues.summary && formData.append('TomTat', formValues.summary);
    formValues.reference && formData.append('LienKet', formValues.reference);
    formValues.source && formData.append('NguonTaiLieu', formValues.source);
    formValues.numberOfCopies && formData.append('SoBanSao', formValues.numberOfCopies + '');
    formValues.allowedReading
      ? formData.append('QuyenDoc', formValues.readingPermission + '')
      : formData.append('QuyenDoc', 0 + '');
    formValues.allowedReading &&
      formValues.readingFee &&
      formData.append('PhiDocSach', formValues.readingFee.replaceAll(',', '').trim());

    formValues.allowedDownload
      ? formData.append('QuyenTai', formValues.downloadPermission + '')
      : formData.append('QuyenTai', 0 + '');
    formValues.allowedDownload &&
      formValues.documentPercent &&
      formData.append('TiLeTaiXuong', formValues.documentPercent);

    formValues.price && formData.append('GiaBia', formValues.price);
    file && formData.append('ListMedia', file);

    !!id ? update(formData) : createDigitalDocument(formData);
  };

  function handleMergeOldValue(name: keyof typeof formValues) {
    if (!oldBookValue) return;

    const field: Record<Partial<keyof typeof formValues>, keyof DetailDigitalDocument> = {
      title: 'TenSach',
      mainAuthors: 'listIdTacGia',
      viceAuthors: 'listIdTacGiaPhu',
      publisher: 'IdNhaXuatBan',
      language: 'IdNgonNgu',
      publishDate: 'NgayXuatBan',
      numberOfCopies: 'SoBanSao',
      ddc_19day: 'Day19',
      isbn: 'ISBN',
      source: 'NguonTaiLieu',
      size: 'KhoMau',
      reference: 'LienKet',
      price: 'GiaBia',
      summary: 'TomTat',
      readingFee: 'Id',
      allowedReading: 'Id',
      allowedDownload: 'Id',
      documentPercent: 'Id',
      readingPermission: 'Id',
      downloadPermission: 'Id',
      bookCover: 'Id'
    };

    if (name === 'price') {
      return setFormValues((prevState) => {
        return {
          ...prevState,

          price:
            // @ts-ignore
            oldBookValue?.[field[name]]
              ?.trim()
              ?.replace(/\D/g, '')
              ?.replace(/\B(?=(\d{3})+(?!\d))/g, ',') || ''
        };
      });
    }

    setFormValues((prevState) => {
      // @ts-ignore
      return { ...prevState, [name]: oldBookValue[field[name]] };
    });
  }

  const fileNames = listOfFiles?.ListFile?.[0]?.FileName?.split('.') || [];

  return (
    <div>
      <Title title={`${!!id ? 'Cập nhật' : 'Thêm mới'} tài liệu số - đa phương tiện`} />

      <div className='mt-4 grid grid-cols-12 gap-8'>
        <div className='col-span-2 h-full'>
          <div className='relative mb-10 flex h-full w-full justify-center overflow-hidden'>
            <input
              type='file'
              accept='image/jpeg,image/gif,image/jpg,image/png,image/bmp'
              className='hidden'
              ref={bookCoverInputRef}
              onChange={handleBookCoverChange}
            />

            <div className='group relative h-44 w-44'>
              <div className='my-5 h-44 w-44'>
                <img
                  src={resultPreview || '/content/Book.png'}
                  alt='Hình bìa tài liệu'
                  className='h-44 w-44 object-cover shadow-md transition-all duration-200 ease-in-out group-hover:blur-[2px]'
                />
              </div>

              <button
                type='button'
                onClick={handleAccessBookCoverFileInputRef}
                className='absolute inset-0 my-5 flex  h-44 w-44 scale-110 items-center justify-center p-5 opacity-0 transition-all duration-200 ease-out group-hover:scale-100 group-hover:bg-black/40 group-hover:opacity-100'
              >
                <span className='mr-2 text-white'>Chọn hình ảnh</span> <CameraFilled style={{ color: 'white' }} />
              </button>
            </div>
          </div>
        </div>

        <div className='col-span-4 flex flex-col gap-y-6'>
          <InputWithLable
            label='Nhan đề'
            placeholder='Nhập tên nhan đề'
            required
            maxLength={255}
            name='title'
            onChange={handleChange}
            value={formValues.title || ''}
            hasMerge={!!state?.isFromErrorSyncedDocument || false}
            onMerge={() => handleMergeOldValue('title')}
          />

          <TagSelectWithLabel
            label='Tác giả chính'
            placeholder='Chọn tác giả chính'
            items={authors}
            hasAddableButton
            onClickAddableButton={() => handleAddableModal('mainAuthor')}
            value={formValues.mainAuthors}
            onChange={(value: string[]) => handleChangeAuthor(value, 'mainAuthors')}
            disabled={loadingOptionsData}
            hasMerge={!!state?.isFromErrorSyncedDocument || false}
            onMerge={() => handleMergeOldValue('mainAuthors')}
          />

          <TagSelectWithLabel
            label='Tác giả phụ'
            placeholder='Chọn tác giả phụ'
            items={authors}
            hasAddableButton
            value={formValues.viceAuthors}
            onClickAddableButton={() => handleAddableModal('viceAuthor')}
            onChange={(value: string[]) => handleChangeAuthor(value, 'viceAuthors')}
            disabled={loadingOptionsData}
            hasMerge={!!state?.isFromErrorSyncedDocument || false}
            onMerge={() => handleMergeOldValue('viceAuthors')}
          />

          <SelectWithLabel
            label='Nhà xuất bản'
            items={[{ label: 'Chọn nhà xuất bản', value: '' }, ...Options_NXB]}
            hasAddableButton
            onClickAddableButton={() => handleAddableModal('nxb')}
            disabled={loadingOptionsData || isRefetching}
            value={formValues.publisher || ''}
            name='publisher'
            onChange={handleChangeSelect}
            hasMerge={!!state?.isFromErrorSyncedDocument || false}
            onMerge={() => handleMergeOldValue('publisher')}
          />

          <SelectWithLabel
            label='Ngôn ngữ'
            items={[{ label: 'Chọn ngôn ngữ', value: '' }, ...Options_Language]}
            hasAddableButton
            onClickAddableButton={() => handleAddableModal('language')}
            disabled={loadingOptionsData || isRefetching}
            value={formValues.language || ''}
            name='language'
            onChange={handleChangeSelect}
            hasMerge={!!state?.isFromErrorSyncedDocument || false}
            onMerge={() => handleMergeOldValue('language')}
          />

          <div className='grid grid-cols-12'>
            <label className='col-span-3 flex items-center gap-1 font-bold'>Ngày ban hành</label>

            <div className='col-span-9 flex items-center gap-1'>
              <DatePicker
                className='w-full border border-gray-300 py-[0.8rem] px-2 outline-none'
                placeholder='Chọn ngày ban hành'
                locale={locale}
                format={FORMAT_DATE_PICKER}
                disabledDate={(current) => {
                  return current && current > dayjs().endOf('day');
                }}
                onChange={handleChangePublishDate}
                value={
                  formValues.publishDate && formValues.publishDate !== '0001-01-01T00:00:00Z'
                    ? dayjs(formValues.publishDate)
                    : null
                }
              />

              {state?.isFromErrorSyncedDocument ? (
                <button onClick={() => handleMergeOldValue('publishDate')}>
                  <Revert />
                </button>
              ) : null}
            </div>
          </div>

          <InputWithLable
            label='Số bản sao'
            placeholder='Nhập số bản sao'
            name='numberOfCopies'
            onChange={handleChange}
            value={formValues.numberOfCopies || ''}
            onKeyDown={(e) => exceptThisSymbols.includes(e.key) && e.preventDefault()}
            maxLength={4}
            // @ts-ignore
            onWheel={(e) => e.target.blur()}
            onKeyPress={(event) => {
              if (!/[0-9]/.test(event.key)) {
                event.preventDefault();
              }
            }}
            hasMerge={!!state?.isFromErrorSyncedDocument || false}
            onMerge={() => handleMergeOldValue('numberOfCopies')}
          />
        </div>

        <div className='col-span-4 flex flex-col gap-y-6'>
          <SelectWithLabel
            name='ddc_19day'
            label='Khung phân loại'
            items={[{ label: IsDay19 ? `Chọn 19 Dãy` : 'Chọn DDC', value: '' }, ...Options_DDC_Day19]}
            hasMerge={!!state?.isFromErrorSyncedDocument || false}
            onMerge={() => handleMergeOldValue('ddc_19day')}
            value={formValues.ddc_19day || ''}
            onChange={handleChangeSelect}
          />

          <InputWithLable
            label='ISBN/ISSN'
            placeholder='Nhập ISBN/ISSN'
            name='isbn'
            onChange={handleChange}
            value={formValues.isbn || ''}
            maxLength={50}
            hasMerge={!!state?.isFromErrorSyncedDocument || false}
            onMerge={() => handleMergeOldValue('isbn')}
          />

          <InputWithLable
            label='Nguồn tài liệu'
            placeholder='Nhập nguồn tài liệu'
            name='source'
            value={formValues.source || ''}
            onChange={handleChange}
            hasMerge={!!state?.isFromErrorSyncedDocument || false}
            onMerge={() => handleMergeOldValue('source')}
          />

          <InputWithLable
            label='Liên kết'
            placeholder='Nhập liên kết'
            name='reference'
            onChange={handleChange}
            value={formValues.reference || ''}
            maxLength={100}
            hasMerge={!!state?.isFromErrorSyncedDocument || false}
            onMerge={() => handleMergeOldValue('reference')}
          />

          <InputWithLable
            label='Giá tiền'
            placeholder='Nhập giá tiền'
            name='price'
            onChange={(e: ChangeEvent<HTMLInputElement>) => {
              const { value } = e.target;

              setFormValues((prevState) => {
                return {
                  ...prevState,
                  price: value
                    .trim()
                    .replace(/\D/g, '')
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                };
              });
            }}
            value={formValues.price}
            maxLength={11}
            onKeyDown={(e) => exceptThisSymbols.includes(e.key) && e.preventDefault()}
            hasMerge={!!state?.isFromErrorSyncedDocument || false}
            onMerge={() => handleMergeOldValue('price')}
          />

          <InputWithLable
            label='Tóm tắt'
            placeholder='Nhập tóm tắt'
            name='summary'
            onChange={handleChange}
            value={formValues.summary || ''}
            hasMerge={!!state?.isFromErrorSyncedDocument || false}
            onMerge={() => handleMergeOldValue('summary')}
          />
        </div>
      </div>

      <div className='mt-4 grid w-full grid-cols-12 gap-2'>
        <label className='col-span-1 col-start-3 gap-1 font-bold'>Phân quyền</label>

        <div className='col-span-2 rounded-md bg-white p-2 shadow-md'>
          <Checkbox
            checked={formValues.allowedReading}
            onChange={handleChangePermission}
            name='allowedReading'
            value={formValues.allowedReading}
            className='font-bold'
          >
            Cho phép xem
          </Checkbox>

          <div className='my-1 ml-6'>
            <Radio
              disabled={!formValues.allowedReading}
              onChange={handleChangeReadingPermission}
              checked={formValues.readingPermission === 1}
              value={1}
            >
              Thư viện nội bộ
            </Radio>

            <Radio
              disabled={!formValues.allowedReading}
              checked={formValues.readingPermission === 2}
              onChange={handleChangeReadingPermission}
              value={2}
            >
              Tất cả thư viện liên thông
            </Radio>
          </div>

          <p className='mb-1 font-bold'>Phí xem</p>

          <div className='broder-[1px] relative ml-2 rounded-md border border-primary-10 py-2 px-1'>
            <input
              className='font-bold outline-none'
              onKeyDown={(e) => exceptThisSymbols.includes(e.key) && e.preventDefault()}
              onChange={handleReadingFee}
              value={formValues.readingFee || ''}
              disabled={!formValues.allowedReading}
              type='text'
              maxLength={11}
            />

            <p className='absolute top-0 right-0 bg-[#D9D9D9]/50 py-2 px-1'>đồng</p>
          </div>
        </div>

        <div className='col-span-2 rounded-md bg-white p-2 shadow-md'>
          <Checkbox
            checked={formValues.allowedDownload}
            onChange={handleChangePermission}
            name='allowedDownload'
            value={formValues.allowedDownload}
            className='font-bold'
          >
            Cho phép tải về
          </Checkbox>

          <div className='my-1 ml-6'>
            <Radio
              disabled={!formValues.allowedDownload}
              onChange={handleChangeDownloadPermission}
              value={1}
              checked={formValues.downloadPermission === 1}
            >
              Thư viện nội bộ
            </Radio>

            <Radio
              disabled={!formValues.allowedDownload}
              onChange={handleChangeDownloadPermission}
              value={2}
              checked={formValues.downloadPermission === 2}
            >
              Tất cả thư viện liên thông
            </Radio>
          </div>

          <div className='broder-[1px] relative ml-2 rounded-md border border-primary-10 py-2 px-1'>
            <input
              className='font-bold outline-none'
              onKeyDown={(e) => exceptThisSymbols.includes(e.key) && e.preventDefault()}
              onChange={handleDocunemtPercent}
              value={formValues.documentPercent || ''}
              maxLength={3}
              max={100}
              type='number'
              disabled={!formValues.allowedDownload}
            />

            <p className='absolute top-0 right-0 bg-[#D9D9D9]/50 py-2 px-1'>% tác phẩm</p>
          </div>
        </div>
      </div>

      <div className='mt-4 grid grid-cols-12'>
        <label className='col-span-1 col-start-3 flex items-center gap-1 font-bold'>
          File tài liệu <span className='text-danger-10'>*</span>
        </label>

        <div className='col-span-9 flex items-center gap-1'>
          <Button onClick={handleAccessFileInputRef}>Chọn tệp</Button>

          <input
            type='file'
            accept='video/mp4,video/x-m4v,video/*,.mp3,audio/*'
            className='hidden'
            ref={fileInutRef}
            onChange={handleOnFileChange}
            multiple
            // @ts-ignore
            onClick={(e: React.MouseEvent<HTMLInputElement, MouseEvent>) => (e.target.value = null)}
          />
        </div>
      </div>

      {/* file from db */}
      {!!listOfFiles?.ListFile.length ? (
        <div className='mt-2 mb-3 grid w-full grid-cols-12 gap-3'>
          <div className='relative col-span-3 col-start-4'>
            <button
              className='absolute right-0 -top-1 z-10 shadow-lg'
              type='button'
              onClick={() => {
                setListOfFiles(undefined);
              }}
            >
              <CloseSquareFilled style={{ color: 'red', fontSize: '24px' }} />
            </button>
            {VIDEO_FILE_TYPE.includes(fileNames[fileNames.length - 1]) && (
              <video width={'100%'} height={700} controls muted key={src}>
                <source src={listOfFiles?.ListFile?.[0].LinkFile} type='video/mp4' />
              </video>
            )}

            {AUDIO_FILE_TYPE.includes(fileNames[fileNames.length - 1]) && (
              <audio controls className='w-full'>
                <source src={listOfFiles?.ListFile?.[0].LinkFile} />
              </audio>
            )}
          </div>
        </div>
      ) : !!file ? (
        <div className='mt-2 mb-3 grid w-full grid-cols-12 gap-3'>
          <div className='relative col-span-3 col-start-4'>
            <button
              className='absolute right-0 -top-1 z-10 shadow-lg'
              type='button'
              onClick={() => {
                setFile(undefined);
              }}
            >
              <CloseSquareFilled style={{ color: 'red', fontSize: '24px' }} />
            </button>
            {file.type.includes('video') && (
              <video width={'100%'} height={700} controls autoPlay key={src}>
                <source src={src} type='video/mp4' />
              </video>
            )}

            {file.type.includes('audio') && (
              <audio controls key={src} className='w-full'>
                <source src={src} type={file.type} />
              </audio>
            )}
          </div>
        </div>
      ) : (
        formValues.reference &&
        getEmbed(formValues.reference) && (
          <div className='mt-2 mb-3 grid w-full grid-cols-12 gap-3'>
            <div className='col-span-3 col-start-4'>
              <iframe
                width={'100%'}
                height={250}
                src={`//www.youtube.com/embed/${getEmbed(formValues.reference)}`}
                frameBorder='0'
                allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
                allowFullScreen
                title='Embedded youtube'
              />
            </div>
          </div>
        )
      )}

      <AuthorModal
        open={addableModal === 'mainAuthor'}
        title='Thêm tác giả'
        onBack={handleCloseModal}
        onSuccess={handleAddAuthorSuccess}
      />

      <AuthorModal
        open={addableModal === 'viceAuthor'}
        title='Thêm tác giả'
        onBack={handleCloseModal}
        onSuccess={handleAddAuthorSuccess}
      />

      <PublishingCompanyModal
        open={addableModal === 'nxb'}
        title='Thêm nhà xuất bản'
        onBack={handleCloseModal}
        onSuccess={(value) => hanldeAddSuccessfully(value, 'publisher')}
      />

      <LanguageModal
        open={addableModal === 'language'}
        title='Thêm ngôn ngữ'
        onBack={handleCloseModal}
        onCreateSuccess={(value) => hanldeAddSuccessfully(value, 'language')}
      />

      <div className='my-3 flex items-center justify-end gap-2'>
        <Button variant='secondary' onClick={() => navigate(-1)}>
          Quay về
        </Button>

        <Button
          disabled={
            !formValues.title.trim() &&
            !file &&
            !listOfFiles?.ListFile.length &&
            !(formValues.reference && getEmbed(formValues.reference))
          }
          variant={
            !formValues.title.trim() &&
            !file &&
            !listOfFiles?.ListFile.length &&
            !(formValues.reference && getEmbed(formValues.reference))
              ? 'disabled'
              : 'default'
          }
          onClick={handleSubmit}
        >
          {!!id ? 'Cập nhật' : 'Thêm mới'}
        </Button>
      </div>

      <Loading
        open={loadingCreateDigitalDocument || (!!id && loadingDigitalDocument) || loadingUpDate || isRefetching}
      />
    </div>
  );
};

export default AddDigitalMedia;
