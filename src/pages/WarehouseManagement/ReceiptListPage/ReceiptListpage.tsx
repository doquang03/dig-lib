import { CloseCircleOutlined, DownloadOutlined, EyeFilled, PrinterFilled, QrcodeOutlined } from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Table, Tooltip } from 'antd';
import { ColumnsType, TableProps } from 'antd/es/table';
import { receiptApis } from 'apis';
import { GetSoNhapKhoVars } from 'apis/reciept.apis';
import { SettingIcon } from 'assets';
import classNames from 'classnames';
import { Button, Empty, Loading, ModalDelete, SizeChanger, Title } from 'components';
import { path } from 'constants/path';
import dayjs from 'dayjs';
import { useColumnFilterTableProps, usePaginationNavigate, useQueryConfig } from 'hooks';
import { useCallback, useMemo, useState } from 'react';
import { useCookies } from 'react-cookie';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { convertDate, getSerialNumber } from 'utils/utils';
import SettingModalImport from '../components/SettingModalImport';
import { useUser } from 'contexts/user.context';
import CreateReceipt from 'pages/Publications/AddPublication/components/CreateReceipt';

const ReceiptListpage = () => {
  const navigate = useNavigate();

  const [visibleModal, setVisibleModal] = useState<boolean>(false);
  const [selectedId, setSelectedId] = useState<Receipt>();
  const [arrAvaliable, setArrAvaliable] = useState<Array<boolean>>([]);
  const [visibleModalImport, setVisibleModalImport] = useState<boolean>(false);
  const [visibleReceiptModal, setVisibleReceiptModal] = useState<boolean>(false);
  const [searchParams, setSearchParams] = useState<
    Partial<{
      MaNhapKho: [string];
      Count: [string];
      UserName: [string];
      CreateDateTime: [string, string];
      NgayVaoSo: [string, string];
    }>
  >();

  const [col, setCol] = useState<keyof Receipt | undefined>();

  const { isAllowedAdjustment } = useUser();

  const [cookies, setCookie] = useCookies(['settings_receipts']);

  const queryConfig = useQueryConfig();

  const { getColumnSearchProps, getColumnFilterDateProps } = useColumnFilterTableProps<Receipt>((key) => setCol(key));

  const handleNavigation = usePaginationNavigate();

  const onPaginate = useCallback(
    (page: number, pageSize: number) => {
      handleNavigation({ ...queryConfig, page: page + '', pageSize: pageSize + '' });
    },
    [handleNavigation, queryConfig]
  );

  const {
    data: receiptData,
    isLoading: isLoadingReceiptData,
    refetch,
    isRefetching
  } = useQuery({
    queryKey: ['receipts', queryConfig, searchParams],
    queryFn: () => {
      let bodyReq: GetSoNhapKhoVars = {};

      if (!!searchParams?.MaNhapKho?.[0]) {
        bodyReq.MaPhieuNhap = {
          truongSearch: searchParams?.MaNhapKho?.[0],
          operatorSearch: 'contains'
        };
      }

      if (!!searchParams?.Count?.[0]) {
        bodyReq.SoLuong = {
          truongSearch: searchParams?.Count?.[0],
          operatorSearch: 'equals'
        };
      }

      if (!!searchParams?.UserName?.[0]) {
        bodyReq.NguoiTao = {
          truongSearch: searchParams?.UserName?.[0],
          operatorSearch: 'contains'
        };
      }

      if (!!searchParams?.NgayVaoSo?.length) {
        bodyReq.NgayVaoSo = {
          Min: dayjs(searchParams.NgayVaoSo[0]).startOf('day').toISOString(),
          Max: dayjs(searchParams.NgayVaoSo[1]).endOf('day').toISOString()
        };
      }

      if (!!searchParams?.CreateDateTime?.length) {
        bodyReq.ThoiGianTao = {
          Min: dayjs(searchParams.CreateDateTime?.[0]).startOf('day').toISOString(),
          Max: dayjs(searchParams.CreateDateTime?.[1]).endOf('day').toISOString()
        };
      }

      return receiptApis.getReceipts({
        pageSize: Number(queryConfig.pageSize),
        page: Number(queryConfig.page),
        ...bodyReq
      });
    },
    onSuccess: () => {
      setVisibleModal(false);
    }
  });

  const { mutate: cancelReceipt, isLoading: isLoadingCancelReceipt } = useMutation({
    mutationFn: receiptApis.cancelReceipt,
    onSuccess: () => {
      setSelectedId(undefined);
      refetch();
      toast.success('Hủy phiếu nhập thành công');
    }
  });

  const { mutate: downloadFile, isLoading: isLoadingDownload } = useMutation({
    mutationFn: receiptApis.downloadFile,
    onSuccess: (data) => {
      toast.success('Tải phiếu nhập kho thành công');
      const blob = new Blob([data.data, { type: 'application/xls' }]);
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = `PhieuNhapKho.xls`;
      link.click();
      setSelectedId(undefined);
    }
  });

  const { List_SoNhapKho = [], TotalCount = 0 } = useMemo(() => {
    if (!receiptData?.data.Item) {
      return {} as {
        List_SoNhapKho: Receipt[];
        TotalCount: number;
      };
    }

    return receiptData?.data.Item;
  }, [receiptData?.data.Item]);

  const pagination = useMemo(() => {
    return {
      current: (!!queryConfig.page && +queryConfig.page) || 1,
      pageSize: (!!queryConfig.pageSize && +queryConfig.pageSize) || 30,
      total: TotalCount,
      onChange: onPaginate,
      hideOnSinglePage: true,
      showSizeChanger: false,
      showQuickJumper: true,
      locale: { jump_to: '', page: '' }
    };
  }, [TotalCount, onPaginate, queryConfig.page, queryConfig.pageSize]);

  useMemo(() => {
    if (arrAvaliable?.length === 0) {
      if (cookies.settings_receipts) {
        setArrAvaliable(cookies.settings_receipts);
      } else {
        setArrAvaliable([true, true, true, true, true]);
      }
    }
  }, [arrAvaliable?.length, cookies.settings_receipts]);

  const handleShowColumns = () => {
    setVisibleModalImport(true);
  };

  const handleSelecteReceipt = (record: Receipt) => () => {
    setVisibleModal(true);
    setSelectedId(record);
  };

  const handlePrintReceipt = (receiptId: string) => () => navigate(path.inPhieuNhap, { state: receiptId });

  const handleDownloadFile = (record: Receipt) => () => {
    setSelectedId(record);
    downloadFile(record.Id);
  };

  const handleExportQR = (record: Receipt) => () => {
    setSelectedId(record);
    setVisibleReceiptModal(true);
  };

  const onChange: TableProps<Receipt>['onChange'] = (pagination, filters, sorter, extra) => {
    setSearchParams((prevState) => {
      return { ...prevState, ...filters };
    });
  };

  const columns: ColumnsType<Receipt> = useMemo(() => {
    let cols: ColumnsType<Receipt> = [
      {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        width: 100,
        render: (value, record, index) => {
          return (
            queryConfig.page && queryConfig.pageSize && getSerialNumber(+queryConfig.page, +queryConfig.pageSize, index)
          );
        }
      }
    ];

    arrAvaliable?.[0] &&
      cols.push({
        title: 'Mã phiếu nhập kho',
        dataIndex: 'MaNhapKho',
        key: 'MaNhapKho',
        ...getColumnSearchProps('Mã nhập kho', 'MaNhapKho', col === 'MaNhapKho')
      });

    arrAvaliable?.[1] &&
      cols.push({
        title: 'Số lượng',
        dataIndex: 'Count',
        key: 'Count',
        ...getColumnSearchProps('Số lượng', 'Count', col === 'Count')
      });

    arrAvaliable?.[3] &&
      cols.push({
        title: 'Người tạo',
        dataIndex: 'UserName',
        key: 'UserName',
        ...getColumnSearchProps('Người tạo', 'UserName', col === 'UserName')
      });

    arrAvaliable?.[2] &&
      cols.push({
        title: 'Ngày vào sổ',
        dataIndex: 'NgayVaoSo',
        key: 'NgayVaoSo',
        render: (value, { NgayVaoSo, Count }) => {
          const itemRender = NgayVaoSo.map((item) => convertDate(item));
          const first = itemRender.slice(0, 2);
          if (itemRender.length > 2) {
            return (
              <Tooltip placement='topLeft' title={itemRender.join(', ')} arrow={true} className='truncate text-left'>
                <p className='text-left'>{first.join(', ') + ' ...'}</p>
              </Tooltip>
            );
          } else {
            return <p className='text-left'>{itemRender.join(', ')}</p>;
          }
        },
        ...getColumnFilterDateProps('NgayVaoSo', col === 'NgayVaoSo')
      });

    arrAvaliable?.[4] &&
      cols.push({
        title: 'Ngày tạo',
        dataIndex: 'CreateDateTime',
        key: 'CreateDateTime',
        render: (value, record, index) => dayjs(record.CreateDateTime).format('HH:mm DD/MM/YYYY'),
        ...getColumnFilterDateProps('CreateDateTime', col === 'CreateDateTime')
      });

    return [
      ...cols,
      {
        title: 'Hành động',
        dataIndex: 'actions',
        key: 'actions',
        render: (value, record) => {
          return (
            <div className='flex items-center justify-center gap-3'>
              <Tooltip title='Chi tiết'>
                <button onClick={() => navigate(`ChiTietPhieuNhap/${record.Id}`)}>
                  <EyeFilled style={{ fontSize: '20px', color: '#08c' }} />
                </button>
              </Tooltip>

              <Tooltip title={'Xuất mã QR'} className={!isAllowedAdjustment ? 'hidden' : ''}>
                <button onClick={handleExportQR(record)}>
                  <QrcodeOutlined style={{ fontSize: '20px', color: '#08c' }} />
                </button>
              </Tooltip>

              <Tooltip title='In phiếu nhập' className={!isAllowedAdjustment ? 'hidden' : ''}>
                <button onClick={handlePrintReceipt(record.Id)}>
                  <PrinterFilled style={{ fontSize: '20px', color: '#08c' }} />
                </button>
              </Tooltip>

              <Tooltip title='Tải phiếu nhập' className={!isAllowedAdjustment ? 'hidden' : ''}>
                <button onClick={handleDownloadFile(record)}>
                  <DownloadOutlined style={{ fontSize: '20px', color: '#08c' }} />
                </button>
              </Tooltip>

              <Tooltip title='Hủy phiếu' className={!isAllowedAdjustment ? 'hidden' : ''}>
                <button onClick={handleSelecteReceipt(record)}>
                  <CloseCircleOutlined style={{ fontSize: '20px', color: 'red' }} />
                </button>
              </Tooltip>
            </div>
          );
        }
      }
    ];
  }, [
    arrAvaliable,
    getColumnFilterDateProps,
    getColumnSearchProps,
    isAllowedAdjustment,
    queryConfig.page,
    queryConfig.pageSize
  ]);

  const handleAddReceipt = () => navigate(path.taoPhieuNhapKho);

  console.log('selectedId', selectedId);

  return (
    <div className='p-5'>
      <Title title='Danh sách phiếu nhập kho' />

      <div className={isAllowedAdjustment ? 'my-3 flex items-center justify-between gap-2' : 'hidden'}>
        <div className='flex items-center gap-2'>
          <Button onClick={handleAddReceipt}>Thêm mới</Button>
        </div>

        <Button onClick={handleShowColumns}>
          <SettingIcon /> Cấu hình hiển thị
        </Button>
      </div>

      <div className='mt-1'>
        <Table
          loading={isLoadingReceiptData}
          columns={columns}
          dataSource={List_SoNhapKho}
          pagination={pagination}
          scroll={{ x: 980 }}
          className='custom-table relative'
          locale={{
            emptyText: () => <Empty />
          }}
          bordered
          rowKey={(record) => record.Id}
          onChange={onChange}
        />

        {!!List_SoNhapKho.length && (
          <div
            className={classNames('relative', {
              'mt-[86px]': Number(queryConfig.pageSize) >= TotalCount
            })}
          >
            <div className='absolute bottom-2'>
              <SizeChanger visible={TotalCount > 0} value={List_SoNhapKho.length + ''} total={TotalCount + ''} />
            </div>
          </div>
        )}
      </div>

      <ModalDelete
        open={visibleModal}
        closable={false}
        title={'Bạn có chắc chắn muốn hủy phiếu nhập kho này không?'}
        handleCancel={() => {
          setVisibleModal(false);
        }}
        handleOk={() => {
          Boolean(selectedId?.Id) && cancelReceipt(selectedId?.Id + '');
        }}
        loading={isLoadingCancelReceipt || isRefetching}
        labelOK='Hủy phiếu'
      />

      <SettingModalImport
        arrAvaliable={arrAvaliable}
        open={visibleModalImport}
        setVisibleModal={setVisibleModalImport}
        setArrAvaliable={setArrAvaliable}
        setCookie={setCookie}
      />

      {selectedId?.ListIdSachCaBiet.length && (
        <CreateReceipt
          ids={selectedId?.ListIdSachCaBiet.map((item) => item)}
          open={visibleReceiptModal}
          onHide={() => setVisibleReceiptModal(false)}
          onSuccess={() => {
            setSelectedId(undefined);
            setVisibleReceiptModal(false);
          }}
        />
      )}

      <Loading open={isLoadingDownload} />
    </div>
  );
};

export default ReceiptListpage;
