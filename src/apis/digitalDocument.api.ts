import httpThuVien from 'utils/httpThuVien';

const endpoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/TaiLieuSo`;

type GetUpdateBooksData = {
  TotalEnty: number;
  TotalFail: number;
  ListThatBai: [];
};

const digitalDocumentApis = {
  getOptions: (type?: string) =>
    httpThuVien.get<
      ResponseApi<{
        IsDay19: boolean;
        Options_TacGia: Option[];
        Options_DDC_Day19: Option[];
        Options_NXB: Option[];
        Options_Language: Option[];
        Options_ThuMucSach: Option[];
      }>
    >(`${endpoint}/GetInitCreateFormOptions?type=${type}`),
  createDigitalDocument: (params: FormData) =>
    httpThuVien.post<ResponseApi<{ Id: string }>>(`${endpoint}/Create`, params, {
      headers: { 'Content-Type': 'multipart/form-data', Accept: '*/*' }
    }),
  updateDigitalDocument: (params: FormData) =>
    httpThuVien.post<ResponseApi<{ Id: string }>>(`${endpoint}/Edit`, params, {
      headers: { 'Content-Type': 'multipart/form-data', Accept: '*/*' }
    }),
  updateBooksDocumentFolder: (payload: { documentId: string; ids: string[] }) =>
    httpThuVien.post<ResponseApi<GetUpdateBooksData>>(
      `/${endpoint}/CapNhatThuMucSach?idTMS=${payload.documentId}`,
      payload.ids
    ),
  getDigitalDocuments: (params: {
    page: number;
    pageSize: number;
    TextForSearch?: string;
    IdAuthor?: string;
    IdPublishingCompany?: string;
    PublicationDate?: string;
    IdLanguage?: string;
    IsPublic?: boolean;
    SortPublicationDate?: boolean;
    Type?: 'image' | 'document' | 'audio' | 'video';
  }) => httpThuVien.post<ResponseApi<{ ListModel: DigitalDocument[]; Count: number }>>(`${endpoint}/Index`, params),
  getOptionsForSearching: (type?: string) =>
    httpThuVien.get<
      ResponseApi<{
        Options_TacGia: Option[];
        Options_NXB: Option[];
        Options_NgonNgu: Option[];
        Options_ThuMucSach: Option[];
      }>
    >(`${endpoint}/GetInitViewOptions?type=${type}`),
  deleteMulti: (ids: string[]) => httpThuVien.delete<ResponseApi>(`${endpoint}/DeleteMulti`, { data: ids }),
  deleteSingle: (id: string) => httpThuVien.delete<ResponseApi>(`${endpoint}/Delete/${id}`),
  deleteSingleAll: (id: string) => httpThuVien.delete<ResponseApi>(`${endpoint}/DeleteAll/${id}`),
  getDetailDocument: (id: string) =>
    httpThuVien.get<ResponseApi<DetailDigitalDocument>>(`${endpoint}/ChiTietTaiLieuSo/${id}`),
  ImportSave: (payload: Array<Array<string>>, type: string) => {
    return httpThuVien.post(`/${endpoint}/ImportSave?type=${type}`, payload);
  },
  PreviewImport: (payload: object) => {
    return httpThuVien({
      method: 'post',
      url: `/${endpoint}/PreviewImport`,
      data: payload,
      headers: { 'Content-Type': 'multipart/form-data' }
    });
  },
  publishToHost: (ids: string[]) =>
    httpThuVien.post<ResponseApi<{ ListSuccess: FailureMessage[]; ListFail: FailureMessage[] }>>(
      `${endpoint}/UploadMarc21`,
      ids
    )
};

export default digitalDocumentApis;
