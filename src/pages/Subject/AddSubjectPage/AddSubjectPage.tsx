import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQuery } from '@tanstack/react-query';
import { subjectApis } from 'apis';
import { Button, Input, Loading, Title } from 'components';
import { OPTIONS_BY_TYPESCHOOL } from 'constants/options';
import { path } from 'constants/path';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { subjectSchema } from 'utils/rules';
import * as yup from 'yup';
import 'css/AddSubjectPage.css';
import { AxiosError } from 'axios';
import { useMemo } from 'react';
import { AutoComplete } from 'components/AutoComplete';
import { useUser } from 'contexts/user.context';
type subjectForm = yup.InferType<typeof subjectSchema>;

const AddSubjectPage = () => {
  const {
    register,
    handleSubmit,
    setValue,
    control,
    setError,
    formState: { errors }
  } = useForm<subjectForm>({
    defaultValues: {
      Ten: '',
      MaMonHoc: '',
      KhoiLop: []
    },
    resolver: yupResolver(subjectSchema)
  });

  const { subjectId } = useParams();
  const navigate = useNavigate();

  const { TypeSchool } = useUser();

  const grade_options = useMemo(() => {
    const array: { label: string; value: string }[] = [];
    if (TypeSchool) {
      for (let param in OPTIONS_BY_TYPESCHOOL) {
        if (TypeSchool.includes(param)) {
          OPTIONS_BY_TYPESCHOOL[param].forEach((element) => {
            array.push(element);
          });
        }
      }
    }
    return array;
  }, [TypeSchool]);

  const { isFetching } = useQuery({
    queryKey: ['EditByID', subjectId],
    queryFn: () => subjectApis.EditByID(subjectId as string),
    onSuccess: (res) => {
      setValue('Ten', res?.data?.Item?.Ten);
      setValue('MaMonHoc', res?.data?.Item?.MaMonHoc);
      setValue('KhoiLop', res?.data?.Item?.KhoiLop);
    },
    onError: () => {
      navigate(path.monhoc);
    },
    enabled: !!subjectId
  });

  const { mutate: handleCreate, isLoading: isLoadingCreate } = useMutation({
    mutationFn: (payload: object) => subjectApis.Create(payload),
    onSuccess: () => {
      toast.success('Thêm môn học thành công');
      navigate(path.monhoc);
    },
    onError: (ex: AxiosError<ResponseApi<{ Ten: string; MaMonHoc: string }>>) => {
      checkValidate(ex);
    }
  });

  const { mutate: handleEdit, isLoading: isLoadingEdit } = useMutation({
    mutationFn: (payload: object) => subjectApis.EditModel(payload),
    onSuccess: () => {
      toast.success('Cập nhật môn học thành công');
    },
    onError: (ex: AxiosError<ResponseApi<{ Ten: string; MaMonHoc: string }>>) => {
      checkValidate(ex);
    }
  });

  const checkValidate = (ex: AxiosError<ResponseApi<{ Ten: string; MaMonHoc: string }>>) => {
    if (ex?.response?.data?.ExMessage === 'Ten') {
      setError('Ten', { type: 'duplicate', message: ex.response?.data.Message });
    }
    if (ex?.response?.data?.ExMessage === 'MaMonHoc') {
      setError('MaMonHoc', { type: 'duplicate', message: ex.response?.data.Message });
    }
  };

  const handleCompelete = handleSubmit((data) => {
    data.Ten = data.Ten.replace(/ +(?= )/g, '');
    setValue('Ten', data.Ten);
    if (!subjectId) {
      handleCreate(data);
    } else {
      let tempData = {
        Id: subjectId,
        Ten: data.Ten,
        MaMonHoc: data.MaMonHoc,
        KhoiLop: data.KhoiLop?.sort((a, b) => +a - +b)
      };
      handleEdit(tempData);
    }
  });

  function backSubject() {
    navigate(path.monhoc);
  }

  return (
    <div className='p-5'>
      <Title title={!!subjectId ? 'CẬP NHẬT MÔN HỌC' : 'THÊM MỚI MÔN HỌC'} />

      <div className='subject-page relative my-6'>
        <div>
          <div className='form-group row' style={{ marginBottom: '10px' }}>
            <label
              className='col-form-label col-sm-2 font-bold'
              style={{ marginBottom: '0', paddingTop: '5px', width: '100%' }}
            >
              Khối lớp <span style={{ color: 'red' }}>*</span>
            </label>
          </div>
          <div className='form-group row' style={{ paddingLeft: '15px' }}>
            <AutoComplete
              items={grade_options || []}
              className='w-full grow'
              name='KhoiLop'
              control={control}
              errorMessage={errors?.KhoiLop?.message}
              mode='multiple'
              placeholder='Chọn khối lớp'
              listHeight={Number.MAX_SAFE_INTEGER}
            />
          </div>
          <div className='form-group row' style={{ marginBottom: '10px' }}>
            <label
              className='col-form-label col-sm-2 font-bold'
              style={{ marginBottom: '0', paddingTop: '5px', width: '100%' }}
            >
              Tên môn học <span style={{ color: 'red' }}>*</span>
            </label>
          </div>
          <div className='form-group row'>
            <div className='component-form-group'>
              <Input
                placeholder='Nhập tên môn học'
                className='form-control form-control-sm'
                register={register}
                name='Ten'
                errorMessage={errors?.Ten?.message}
                disabled={!!subjectId && isFetching}
                maxLength={256}
              ></Input>
            </div>
          </div>
          <div className='form-group row' style={{ marginBottom: '10px' }}>
            <label
              className='col-form-label col-sm-2 font-bold'
              style={{ marginBottom: '0', paddingTop: '5px', width: '100%' }}
            >
              Mã môn học <span style={{ color: 'red' }}>*</span>
            </label>
          </div>
          <div className='form-group row'>
            <div className='component-form-group'>
              <Input
                placeholder='Nhập mã môn học'
                className='form-control form-control-sm'
                register={register}
                name='MaMonHoc'
                errorMessage={errors?.MaMonHoc?.message}
                disabled={!!subjectId && isFetching}
                maxLength={101}
              ></Input>
            </div>
          </div>
          <div className='form-group row'>
            <div className='component-form-group flex' style={{ textAlign: 'end' }}>
              <Button className='gray-button' onClick={backSubject}>
                Quay về
              </Button>
              {!subjectId ? (
                <Button
                  className='btn btn-primary btn-sm blue-button'
                  onClick={handleCompelete}
                  style={{ marginLeft: '10px' }}
                >
                  Thêm mới
                </Button>
              ) : (
                <Button
                  className='btn btn-primary btn-sm blue-button'
                  onClick={handleCompelete}
                  style={{ marginLeft: '10px' }}
                >
                  Cập nhật
                </Button>
              )}
            </div>
          </div>
        </div>
      </div>
      <Loading open={isLoadingCreate || isLoadingEdit}></Loading>
    </div>
  );
};

export default AddSubjectPage;
