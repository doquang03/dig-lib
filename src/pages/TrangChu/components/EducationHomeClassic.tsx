import {
  CheckCircleFilled,
  ClockCircleFilled,
  PlusCircleFilled,
  RightCircleOutlined,
  WarningFilled
} from '@ant-design/icons';
import 'css/bootstrap.min.css';
import 'css/HomePage.css';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { path } from 'constants/path';
import { statistic } from 'apis';
import { useQuery } from '@tanstack/react-query';
import { useUser } from 'contexts/user.context';

const EducationHomeClassic = () => {
  const { userType } = useUser();
  const { data: dataCount, isLoading: isLoadingDataCount } = useQuery({
    queryFn: statistic.GetDataCount,
    queryKey: ['GetDataCount'],
    enabled: userType === 3000,
    cacheTime: Infinity
  });

  const hehe = useLocation();

  console.log('hehe', hehe);

  return (
    <div className='right'>
      <div className='content'>
        <div className='clearfix layout-content'>
          <div className='home-welcome'>
            <h2 style={{ textAlign: 'center' }}>Chào mừng thầy cô đến với phần mềm Quản lý Thư viện B.Lib</h2>
            <h3 style={{ textAlign: 'center' }}>Thư viện</h3>
          </div>

          <div className='mb-3'>
            <div style={{ width: '100%', display: 'flex' }}>
              <div
                className='col-md-12'
                style={{ marginLeft: 'auto', marginRight: 'auto', maxWidth: '95%', width: '95%' }}
              >
                <div className='col-md-3'>
                  {/* yêu cầu mượn */}
                  <div className='rounded-sm border-[1px] border-[#18bad0] shadow-sm'>
                    <div className='border-[1px] border-b-[#18bad0] bg-[#D9EDF7]'>
                      <div className='flex items-center justify-between border-b-[1px] p-2'>
                        <svg
                          viewBox='64 64 896 896'
                          focusable='false'
                          data-icon='plus-circle'
                          width='3em'
                          height='3em'
                          fill='#31708F'
                          aria-hidden='true'
                        >
                          <path d='M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448 448-200.6 448-448S759.4 64 512 64zm192 472c0 4.4-3.6 8-8 8H544v152c0 4.4-3.6 8-8 8h-48c-4.4 0-8-3.6-8-8V544H328c-4.4 0-8-3.6-8-8v-48c0-4.4 3.6-8 8-8h152V328c0-4.4 3.6-8 8-8h48c4.4 0 8 3.6 8 8v152h152c4.4 0 8 3.6 8 8v48z' />
                        </svg>

                        <div className='flex flex-col items-center '>
                          <span className='text-[40px] font-semibold leading-none text-[#31708F]'>
                            {' '}
                            {isLoadingDataCount ? '--' : dataCount?.data?.Item.DangDatMuon || 0}
                          </span>
                          <span className='text-[#31708F]'>Yêu cầu mượn</span>
                        </div>
                      </div>
                    </div>

                    <Link
                      to='/ThongKe/DanhSachDatMuon'
                      className='flex items-center justify-between bg-[#F5F5F5] px-2 py-5'
                    >
                      <span>Xem yêu cầu mượn</span>

                      <RightCircleOutlined />
                    </Link>
                  </div>
                </div>

                {/* Số đang mượn */}
                <div className='col-md-3'>
                  <div className='rounded-sm border-[1px] border-[#58cc30] shadow-sm'>
                    <div className='border-[1px] border-b-[#58cc30] bg-[#DFF0D8]'>
                      <div className='flex items-center justify-between border-b-[1px] p-2'>
                        <svg
                          viewBox='64 64 896 896'
                          focusable='false'
                          data-icon='check-circle'
                          width='3em'
                          height='3em'
                          fill='#3C763D'
                          aria-hidden='true'
                        >
                          <path d='M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448 448-200.6 448-448S759.4 64 512 64zm193.5 301.7l-210.6 292a31.8 31.8 0 01-51.7 0L318.5 484.9c-3.8-5.3 0-12.7 6.5-12.7h46.9c10.2 0 19.9 4.9 25.9 13.3l71.2 98.8 157.2-218c6-8.3 15.6-13.3 25.9-13.3H699c6.5 0 10.3 7.4 6.5 12.7z'></path>
                        </svg>

                        <div className='flex flex-col items-center'>
                          <span className='text-[40px] font-semibold leading-none text-[#3C763D]'>
                            {isLoadingDataCount ? '--' : dataCount?.data?.Item.DangMuon || 0}
                          </span>
                          <span className='text-[#3C763D]'>Số đang mượn</span>
                        </div>
                      </div>
                    </div>

                    <Link
                      to={path.thongkemuonsach}
                      state={{
                        TinhTrang: 1
                      }}
                      className='flex w-full items-center justify-between bg-[#F5F5F5] px-2 py-5'
                    >
                      <span>Xem số đang mượn</span>

                      <RightCircleOutlined />
                    </Link>
                  </div>
                </div>

                {/* đến hạn trả */}
                <div className='col-md-3'>
                  <div className='rounded-sm border-[1px] border-[#d8ca30] shadow-sm'>
                    <div className='border-[1px] border-b-[#d8ca30] bg-[#FCF8E3]'>
                      <div className='flex items-center justify-between border-b-[1px] p-2'>
                        <svg
                          viewBox='64 64 896 896'
                          focusable='false'
                          data-icon='clock-circle'
                          width='3em'
                          height='3em'
                          fill='#8A6D3B'
                          aria-hidden='true'
                        >
                          <path d='M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448 448-200.6 448-448S759.4 64 512 64zm176.5 585.7l-28.6 39a7.99 7.99 0 01-11.2 1.7L483.3 569.8a7.92 7.92 0 01-3.3-6.5V288c0-4.4 3.6-8 8-8h48.1c4.4 0 8 3.6 8 8v247.5l142.6 103.1c3.6 2.5 4.4 7.5 1.8 11.1z' />
                        </svg>

                        <div className='flex flex-col items-center'>
                          <span className='text-[40px] font-semibold leading-none text-[#8A6D3B]'>
                            {isLoadingDataCount ? '--' : dataCount?.data?.Item.DangMuonDenHanTra || 0}
                          </span>

                          <span className='text-[#8A6D3B]'>Đến hạn trả</span>
                        </div>
                      </div>
                    </div>

                    <Link
                      className='flex w-full items-center justify-between bg-[#F5F5F5] px-2 py-5'
                      to={path.thongkemuonsach}
                      state={{
                        TinhTrang: 2
                      }}
                    >
                      <span>Xem đến hạn trả</span>

                      <RightCircleOutlined />
                    </Link>
                  </div>
                </div>

                {/* quá hạn trả */}
                <div className='col-md-3'>
                  <div className='rounded-sm border-[1px] border-[#e65b42] shadow-sm'>
                    <div className='border-[1px] border-b-[#e65b42] bg-[#F2DEDE]'>
                      <div className='flex items-center justify-between border-b-[1px] p-2'>
                        <svg
                          viewBox='64 64 896 896'
                          focusable='false'
                          data-icon='warning'
                          width='3em'
                          height='3em'
                          fill='#A94442'
                          aria-hidden='true'
                        >
                          <path d='M955.7 856l-416-720c-6.2-10.7-16.9-16-27.7-16s-21.6 5.3-27.7 16l-416 720C56 877.4 71.4 904 96 904h832c24.6 0 40-26.6 27.7-48zM480 416c0-4.4 3.6-8 8-8h48c4.4 0 8 3.6 8 8v184c0 4.4-3.6 8-8 8h-48c-4.4 0-8-3.6-8-8V416zm32 352a48.01 48.01 0 010-96 48.01 48.01 0 010 96z'></path>
                        </svg>

                        <div className='flex flex-col items-center'>
                          <span className='text-[40px] font-semibold leading-none text-[#A94442]'>
                            {isLoadingDataCount ? '--' : dataCount?.data?.Item.DangMuonDenHanTra || 0}
                          </span>

                          <span className='text-[#A94442]'>Quá hạn trả</span>
                        </div>
                      </div>
                    </div>

                    <Link
                      className='flex w-full items-center justify-between bg-[#F5F5F5] px-2 py-5 hover:text-red-500'
                      to={path.thongkemuonsach}
                      state={{
                        TinhTrang: 3
                      }}
                    >
                      <span>Xem quá hạn trả</span>

                      <RightCircleOutlined />
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='home-menu'>
            <div className='home-menu-group' style={{ textAlign: 'center' }}>
              <div className='title'>MƯỢN TRẢ SÁCH</div>
              <div className='btntag-box'>
                <Link to={path.muonTraSach} className='btntag btntag-success'>
                  <img src='/content/checkin.png' alt=''></img>
                  Mượn trả sách
                </Link>
              </div>
            </div>
            <div className='home-menu-group'>
              <div className='title'>QUẢN LÝ THÀNH VIÊN</div>
              <div className='btntag-box'>
                <Link to={path.hocsinh} className='btntag btntag-success'>
                  <img src='/content/student.png' alt=''></img>
                  Học sinh
                </Link>
                <Link to={path.giaovien} className='btntag btntag-success'>
                  <img src='/content/teacher.png' alt=''></img>
                  Giáo viên
                </Link>
              </div>
            </div>
            <div className='home-menu-group'>
              <div className='title'>QUẢN LÝ THÔNG TIN SÁCH</div>
              <div className='btntag-box'>
                <Link to={path.sach} className='btntag btntag-success'>
                  <img src='/content/book2.png' alt=''></img>
                  Sách
                </Link>
                <Link to={path.trangThaiSach} className='btntag btntag-success'>
                  <img src='/content/checklist2.png' alt=''></img>
                  Trạng thái
                </Link>
              </div>
            </div>
            <div className='home-menu-group'>
              <div className='title'>NHẬP XUẤT KHO</div>
              <div className='btntag-box'>
                <Link to={path.quanlynhapkho} className='btntag btntag-success'>
                  <img src='/content/import.png' alt=''></img>
                  Nhập kho
                </Link>
                <Link to={path.quanlyxuatkho} className='btntag btntag-success'>
                  <img src='/content/export.png' alt=''></img>
                  Xuất kho
                </Link>
              </div>
            </div>
            <div className='home-menu-group'>
              <div className='title'>THỐNG KÊ</div>
              <div className='btntag-box'>
                <Link to={path.thongke} className='btntag btntag-success'>
                  <img src='/content/chart2.png' alt=''></img>
                  Biểu đồ
                </Link>
                <div className='btntag-group'>
                  <Link to={path.thongkemuonsach} className='btntag btntag-success'>
                    TK sách mượn
                  </Link>
                  <Link to={path.traSachDanhMucSach} className='btntag btntag-success'>
                    TK chưa trả
                  </Link>
                  <Link to={path.thongkexuatbaocao} className='btntag btntag-success'>
                    Xuất báo cáo
                  </Link>
                </div>
              </div>
            </div>
            <div className='home-menu-group'>
              <div className='title'>CÀI ĐẶT</div>
              <div className='btntag-box'>
                <Link to={path.quanlythuvien} className='btntag btntag-success'>
                  <img src='/content/tools.png' alt=''></img>
                  Tổng quan
                </Link>
                <Link to={path.quanlythuviencsdl} className='btntag btntag-success'>
                  <img src='/content/database.png' alt=''></img>
                  CSDL
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EducationHomeClassic;
