import { CalendarOutlined, CloseCircleOutlined, CloseOutlined, SearchOutlined } from '@ant-design/icons';
import { Button, DatePicker, Input, InputRef } from 'antd';
import locale from 'antd/es/date-picker/locale/vi_VN';
import { ColumnType } from 'antd/es/table';
import dayjs from 'dayjs';
import 'dayjs/plugin/updateLocale';
import { Key, useRef } from 'react';

import updateLocale from 'dayjs/plugin/updateLocale';
import useOutsideClick from './useOutsideClick';

dayjs.extend(updateLocale);
dayjs.updateLocale('en', {
  weekStart: 1
});

const { RangePicker } = DatePicker;

export default function useColumnFilterTableProps<T>(cb?: (key?: keyof T) => void) {
  let searchInput = useRef<InputRef>(null);
  let searchInputTo = useRef<InputRef>(null);
  let arrClearFunction: Array<VoidFunction | undefined> = [];
  const ref = useOutsideClick<HTMLDivElement>(() => cb?.(undefined));
  // filter by text
  const getColumnSearchProps = (placeholder?: string, _key?: keyof T, visible?: boolean): ColumnType<T> => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters, close }) => {
      arrClearFunction.push(clearFilters);
      arrClearFunction.push(confirm);
      return (
        // @ts-ignore
        <div className='p-2' onKeyDown={(e) => e.stopPropagation()} ref={ref}>
          <Input
            placeholder={placeholder || 'Nhập ...'}
            value={selectedKeys[0]}
            onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
            onPressEnter={() => confirm()}
            autoFocus
            suffix={
              <button
                className='mb-1'
                onClick={() => {
                  cb?.(undefined);
                  clearFilters?.();
                  confirm();
                }}
              >
                <CloseCircleOutlined style={{ opacity: 0.3 }} />
              </button>
            }
          />

          <button
            onClick={() => {
              close();

              cb?.(undefined);
            }}
            className='absolute -right-2 -top-3 rounded-full bg-slate-500/75 px-1'
          >
            <CloseOutlined style={{ fontSize: 14, color: 'white' }} />
          </button>
        </div>
      );
    },
    filterIcon: (filtered: boolean) => {
      return <SearchOutlined style={{ color: filtered ? '#1890ff' : 'white', fontSize: 'bold' }} />;
    },
    onFilterDropdownOpenChange: (visible) => {
      _key && cb?.(_key);

      if (visible) {
        setTimeout(() => {
          searchInput.current?.focus();
          searchInput.current?.select();
        }, 100);
      }
    },
    filterDropdownOpen: visible
  });

  // filter by range number
  const getColumnRangeProps = (_key?: keyof T, visible?: boolean): ColumnType<T> => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters, close }) => {
      arrClearFunction.push(clearFilters);
      arrClearFunction.push(confirm);
      return (
        <div onKeyDown={(e) => e.stopPropagation()} className='p-2'>
          <div className='relative flex items-center gap-1'>
            <Input
              ref={searchInput}
              placeholder={'Từ'}
              value={selectedKeys[0]}
              onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
              onPressEnter={() => confirm()}
              type='number'
            />
            -
            <Input
              ref={searchInputTo}
              placeholder={'Đến'}
              value={selectedKeys[1]}
              onChange={(e) => setSelectedKeys(e.target.value ? [selectedKeys[0], e.target.value] : [])}
              onPressEnter={() => confirm()}
              type='number'
            />
            <button
              onClick={() => {
                close();
                cb?.(undefined);
              }}
              className='absolute -right-2 -top-3 rounded-full bg-slate-500/75 px-1'
            >
              <CloseOutlined style={{ fontSize: 14, color: 'white' }} />
            </button>
          </div>

          <Button
            className='mt-1'
            onClick={() => {
              clearFilters?.();
              confirm();
            }}
          >
            Làm mới
          </Button>
        </div>
      );
    },
    filterIcon: (filtered: boolean) => (
      <SearchOutlined style={{ color: filtered ? '#1890ff' : 'white', fontSize: 'bold' }} />
    ),
    onFilterDropdownOpenChange: (visible) => {
      _key && cb?.(_key);

      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      } else {
        cb?.(undefined);
      }
    },
    filterDropdownOpen: visible
  });

  // filter date
  const getColumnFilterDateProps = (_key?: keyof T, visible?: boolean): ColumnType<T> => ({
    filterDropdown: ({ setSelectedKeys, confirm, clearFilters, close }) => {
      arrClearFunction.push(clearFilters);
      arrClearFunction.push(confirm);
      return (
        <form className='relative flex items-center gap-1 p-2' onKeyDown={(e) => e.stopPropagation()}>
          <RangePicker
            className='border border-gray-300 py-[0.8rem] px-2 outline-none'
            format={['DD/MM/YYYY', 'DD-MM-YYYY']}
            placeholder={['Ngày bắt đầu', 'Ngày kết thúc']}
            disabledDate={(current) => {
              return current && current > dayjs().endOf('day');
            }}
            onChange={(value) => {
              setSelectedKeys((value as unknown as Key[]) ? (value as unknown as Key[]) : []);
              confirm();
            }}
            locale={locale}
          />

          <button
            onClick={() => {
              close();
            }}
            className='absolute -right-2 -top-3 rounded-full bg-slate-500/75 px-1'
            type='button'
          >
            <CloseOutlined style={{ fontSize: 14, color: 'white' }} />
          </button>
        </form>
      );
    },
    filterIcon: (filtered: boolean) => (
      <CalendarOutlined style={{ color: filtered ? '#1890ff' : 'white', fontSize: 'bold' }} />
    ),
    onFilterDropdownOpenChange: (visible) => {
      _key && cb?.(_key);
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    filterDropdownOpen: visible
  });

  const clearAll = () => {
    arrClearFunction.forEach((element) => {
      element && element();
    });
  };

  return { getColumnSearchProps, getColumnFilterDateProps, getColumnRangeProps, clearAll };
}
