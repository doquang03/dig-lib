export const data = [
  {
    key: 1,
    name: 'Á kim',
    code: '',
    children: [{ key: 1_1, name: '- Hóa học', code: '540' }]
  },
  {
    key: 2,
    name: 'Ắc quy điện',
    code: '6C2'
  },
  {
    key: 3,
    name: 'Ắc quy khí',
    code: '6C2'
  },
  {
    key: 4,
    name: 'Ắc quy vô tuyến điện',
    code: '6T1'
  },
  {
    key: 5,
    name: 'Album',
    code: '002'
  },
  {
    key: 6,
    name: 'Amiđan (viêm)',
    code: '617'
  },
  {
    key: 7,
    name: 'Amin thơm',
    code: '547'
  },
  {
    key: 8,
    name: 'Ampe kế',
    code: '6C2'
  },
  {
    key: 9,
    name: 'An ninh chính trị và trật tự an toàn xã hội',
    code: '32(V)'
  },
  {
    key: 10,
    name: 'An ninh lương thực',
    code: '33'
  },
  {
    key: 11,
    name: 'Anh đào- cây',
    code: '634'
  },
  {
    key: 12,
    name: 'Anh hùng cách mạng (chủ nghĩa)',
    code: '',
    children: [{ key: 12_1, name: '- Giáo dục', code: '371' }]
  },
  {
    key: 13,
    name: 'Anh hùng lao động',
    code: '331'
  },
  {
    key: 14,
    name: 'Anh hùng quân đội',
    code: '',
    children: [{ key: 14_1, name: '- Tiểu sử', code: '(35V)(092)' }]
  },
  {
    key: 15,
    name: 'Ánh sáng',
    code: '331',
    children: [
      { key: 15_1, name: '- Cách đo', code: '551' },
      { key: 15_2, name: '- Điều trị', code: '615' },
      { key: 15_3, name: '- Kỹ thuật chiếu sáng', code: '6C2' },
      { key: 15_4, name: '- Trong xây dựng', code: '6X' },
      { key: 15_5, name: '- Vật lý học', code: '535A' }
    ]
  },
  {
    key: 16,
    name: 'Ánh sáng (lý thuyết)',
    code: '535'
  },
  {
    key: 17,
    name: 'Áp điện học',
    code: '537'
  },
  {
    key: 18,
    name: 'Ảo ảnh',
    code: '551'
  },
  {
    key: 19,
    name: 'Ảo giác tâm lý học',
    code: '158'
  },
  {
    key: 20,
    name: 'Ảo thuật (nghệ thuật)',
    code: '70'
  },
  {
    key: 21,
    name: 'Áp điện',
    code: '537'
  },
  {
    key: 22,
    name: 'Áp suất (vật lý)',
    code: '530'
  },
  {
    key: 23,
    name: 'Áp xe phổi',
    code: '617'
  },
  {
    key: 24,
    name: 'Ăn mòn và chống ăn mòn',
    code: '',
    children: [
      { key: 24_1, name: '- Kim loại', code: '6C4' },
      { key: 24_2, name: '- Vật liệu nói chung', code: '604' }
    ]
  },
  {
    key: 25,
    name: 'Ăn uống',
    code: '',
    children: [
      { key: 25_1, name: '- Của trẻ mẫu giáo', code: '618N' },
      { key: 25_2, name: '- Của thiếu niên', code: '613' },
      { key: 25_2, name: '- Nhiễm độc', code: '616V' },
      { key: 25_2, name: '- Vệ sinh', code: '613' }
    ]
  },
  {
    key: 26,
    name: 'Ăn uống công cộng',
    code: '',
    children: [
      { key: 26_1, name: '- Kỹ thuật nấu nướng', code: '6C8' },
      { key: 26_2, name: '- Kiểm tra vệ sinh', code: '613' },
      { key: 26_2, name: '- Kinh tế', code: '338' }
    ]
  },
  {
    key: 27,
    name: 'Ăngten',
    code: '6T2'
  },
  {
    key: 28,
    name: 'Ăngghen, F',
    code: '3K1'
  },
  {
    key: 29,
    name: 'Âm dương ngũ hành',
    code: '158',
    children: [
      { key: 26_1, name: '- thuyết (triết học)', code: '1T' },
      { key: 26_2, name: '- ứng dụng trong khoa học thần bí', code: '29' }
    ]
  },
  {
    key: 30,
    name: 'Âm đạo - bệnh',
    code: '618P'
  },
  {
    key: 31,
    name: 'Âm học',
    code: '534',
    children: [
      { key: 31_1, name: '- điện thanh học', code: '6T2' },
      { key: 31_2, name: '- động vật', code: '59' },
      { key: 31_2, name: '- người', code: '5A2' },
      { key: 31_2, name: '- ứng dụng', code: '606' },
      { key: 31_2, name: '- trong xây dựng', code: '6X' }
    ]
  },
  {
    key: 32,
    name: 'Âm nhạc',
    code: '78'
  },
  {
    key: 33,
    name: 'Âm nhạc dân gian Việt Nam',
    code: '78(V)'
  },
  {
    key: 34,
    name: 'Âm thanh',
    code: '534'
  },
  {
    key: 35,
    name: 'Ấn phẩm định kì (xuất bản, biên soạn …)',
    code: '002'
  },
  {
    key: 36,
    name: 'Ấp trứng',
    code: '636'
  },
  {
    key: 37,
    name: 'Ba đảm đang - phong trào',
    code: '32(V)'
  },
  {
    key: 38,
    name: 'Ba sẵn sàng - phong trào',
    code: '3KTV'
  },
  {
    key: 39,
    name: 'Bà mẹ và trẻ em - bảo vệ',
    code: '610'
  },
  {
    key: 40,
    name: 'Bà mẹ Việt Nam anh hùng - chính sách',
    code: '36(V)'
  },
  {
    key: 41,
    name: 'Bạc',
    code: '',
    children: [
      { key: 41_1, name: '- kim loại', code: '6C3' },
      { key: 41_2, name: '- mỏ', code: '59' },
      { key: 41_2, name: '- nguyên tố hóa học', code: '540' },
      { key: 41_2, name: '- sản xuất', code: '6C3' }
    ]
  },
  {
    key: 42,
    name: 'Bạc hà - cây',
    code: '',
    children: [
      { key: 41_1, name: '- sản xuất tinh dầu', code: '6C7' },
      { key: 41_2, name: '- sử dụng làm thuốc', code: '615' },
      { key: 41_2, name: '- trồng trọt', code: '633' }
    ]
  },
  {
    key: 43,
    name: 'Bách - cây',
    code: '634'
  },
  {
    key: 44,
    name: 'Bách khoa toàn thư',
    code: '636',
    children: [{ key: 41_1, name: '- tổng hợp', code: '03' }]
  },
  {
    key: 45,
    name: 'Bách khoa toàn thư - thiếu nhi',
    code: 'ĐV21'
  },
  {
    key: 46,
    name: 'Bách khoa từ điển',
    code: '03'
  },
  {
    key: 47,
    name: 'Bạch cầu (bệnh)',
    code: '616H'
  },
  {
    key: 48,
    name: 'Bạch dương (cây)',
    code: '634'
  },
  {
    key: 49,
    name: 'Bạch Đằng - chiến thắng',
    code: '9(V)1'
  },
  {
    key: 50,
    name: 'Bạch hạch (viêm)',
    code: '616H',
    children: [{ key: 50_1, name: '- của trẻ em', code: '618N' }]
  },
  {
    key: 51,
    name: 'Bạch hầu (bệnh)',
    code: '616N',
    children: [{ key: 51_1, name: '- của trẻ em', code: '618N' }]
  },
  {
    key: 52,
    name: 'Bạch huyết (hệ)',
    code: '',
    children: [
      { key: 52_1, name: '- bệnh học', code: '616N' },
      { key: 52_1, name: '- giải phẫu học', code: '5A2' },
      { key: 52_1, name: '- sinh lí học', code: '5A2' }
    ]
  },
  {
    key: 53,
    name: 'Bạch kim',
    code: '',
    children: [
      { key: 53_1, name: '- khai thác', code: '6C1' },
      { key: 53_1, name: '- luyện kim', code: '6C3' },
      { key: 53_1, name: '- mỏ', code: '553' }
    ]
  },
  {
    key: 54,
    name: 'Bài học - cơ cấu',
    code: '371'
  },
  {
    key: 55,
    name: 'Bài tiết',
    code: '615'
  },
  {
    key: 56,
    name: 'Bài toán Côsi',
    code: '615'
  },
  {
    key: 57,
    name: 'Bài trí mỹ thuật',
    code: '74',
    children: [{ key: 57_1, name: '- của trẻ em tuổi mầm non', code: '372' }]
  },
  {
    key: 58,
    name: 'Bãi cỏ vườn hoa',
    code: '635'
  },
  {
    key: 59,
    name: 'Bãi tắm',
    code: '397'
  },
  {
    key: 60,
    name: 'Bại liệt (bệnh và điều trị)',
    code: '616V',
    children: [{ key: 60_1, name: '- ở trẻ em', code: '618N' }]
  },
  {
    key: 61,
    name: 'Ban nhạc',
    code: '78'
  },
  {
    key: 62,
    name: 'Bàn chải (sản xuất)',
    code: '6C9'
  },
  {
    key: 63,
    name: 'Bàn ghế',
    code: '',
    children: [
      { key: 63_1, name: '- gỗ', code: '618N' },
      { key: 63_2, name: '- kim loại', code: '6C4' }
    ]
  },
  {
    key: 64,
    name: 'Bàn phím máy tính',
    code: ''
  },
  {
    key: 65,
    name: 'Bàn tính',
    code: ''
  },
  {
    key: 66,
    name: 'Bán buôn',
    code: '339'
  },
  {
    key: 67,
    name: 'Bán dẫn',
    code: '636',
    children: [
      { key: 66_1, name: '- máy thu vô tuyến bán dẫn', code: '6T0' },
      { key: 66_2, name: '- ống bán dẫn', code: '6C3' },
      { key: 66_2, name: '- vật lý học', code: '531' }
    ]
  },
  {
    key: 68,
    name: 'Bản chất và hiện tượng',
    code: '1D'
  },
  {
    key: 69,
    name: 'Bản đồ',
    code: '912Đ'
  },
  {
    key: 70,
    name: 'Bản kẽm',
    code: '6C9'
  },
  {
    key: 71,
    name: 'Bản nhạc - xuất bản, phát hành',
    code: '002'
  },
  {
    key: 72,
    name: 'Bản tin điện tử',
    code: ''
  },
  {
    key: 73,
    name: 'Bản vẽ kĩ thuật',
    code: '6C7'
  },
  {
    key: 74,
    name: 'Bảng tính',
    code: '519'
  },
  {
    key: 75,
    name: 'Bánh, mứt, kẹo - sản xuất',
    code: '6C8'
  },
  {
    key: 76,
    name: 'Bánh mì - sản xuất',
    code: '636'
  },
  {
    key: 77,
    name: 'Bào thai học',
    code: '6C6'
  },
  {
    key: 78,
    name: 'Báo chí',
    code: '05',
    children: [
      { key: 78_1, name: '- sự nghiệp', code: '6T0' },
      { key: 78_2, name: '- lịch sử báo chí của Đảng cộng sản Việt Nam', code: '3KV4' }
    ]
  },
  {
    key: 79,
    name: 'Bảo hiểm học đường',
    code: '370'
  },
  {
    key: 80,
    name: 'Bảo hiểm nhà nước',
    code: '36'
  },
  {
    key: 81,
    name: 'Bảo hiểm xã hội',
    code: '36',
    children: [{ key: 81_1, name: '- công nhân viên chức', code: '331' }]
  },
  {
    key: 82,
    name: 'Bảo hộ lao động',
    code: '331'
  },
  {
    key: 83,
    name: 'Bảo tàng',
    code: '392',
    children: [
      { key: 83_1, name: '- địa chỉ', code: '379' },
      { key: 83_2, name: '- địa chỉ', code: '373' }
    ]
  },
  {
    key: 84,
    name: 'Bảo tàng cách mạng Việt Nam',
    code: '9(V)(069)'
  },
  {
    key: 85,
    name: 'Bảo tàng động vật',
    code: '59(069)'
  },
  {
    key: 86,
    name: 'Bảo tàng - Hồ Chí Minh',
    code: '3K5H(069)'
  },
  {
    key: 87,
    name: 'Bảo tàng lịch sử Việt Nam',
    code: '9(V)(069)'
  },
  {
    key: 88,
    name: 'Bảo tàng quân đội Việt Nam',
    code: '35(V)(069)'
  },
  {
    key: 89,
    name: 'Bảo tàng sinh học',
    code: '57(069)'
  },
  {
    key: 90,
    name: 'Bảo toàn năng lượng (định luật)',
    code: '530'
  },
  {
    key: 91,
    name: 'Bảo tồn văn hóa dân tộc',
    code: '384'
  },
  {
    key: 92,
    name: 'Bảo vệ an ninh chính trị và trật tự an toàn xã hội',
    code: '34(V)'
  },
  {
    key: 93,
    name: 'Bảo vệ bà mẹ và trẻ em',
    code: '610',
    children: [{ key: 93_1, name: '- luật pháp', code: '34(V)' }]
  },
  {
    key: 94,
    name: 'Bảo vệ cây trồng',
    code: '632'
  },
  {
    key: 95,
    name: 'Bảo vệ giới sinh vật',
    code: '57(069)',
    children: [
      { key: 95_1, name: '- động vật', code: '59(069)' },
      { key: 95_2, name: '- thực vật', code: '58(069)' }
    ]
  },
  {
    key: 96,
    name: 'Bảo vệ thiên nhiên',
    code: '50(069)'
  },
  {
    key: 97,
    name: 'Bảo vệ hòa bình',
    code: '327'
  },
  {
    key: 98,
    name: 'Bảo vệ môi sinh',
    code: '636',
    children: [
      { key: 98_1, name: '- xã hội học', code: '301' },
      { key: 98_2, name: '- y học', code: '613' }
    ]
  },
  {
    key: 99,
    name: 'Bảo vệ môi trường',
    code: '50',
    children: [{ key: 99_1, name: '- kinh tế', code: '338' }]
  },
  {
    key: 100,
    name: 'Bảo vệ rừng',
    code: '634'
  },
  {
    key: 101,
    name: 'Bảo vệ sức khỏe thiếu niên nhi đồng',
    code: '610'
  },
  {
    key: 102,
    name: 'Bão - phòng chống bão cho tàu thuyền',
    code: '6V'
  },
  {
    key: 103,
    name: 'Bão từ',
    code: '636'
  },
  {
    key: 104,
    name: '',
    code: ''
  },
  {
    key: 105,
    name: 'Bát đĩa',
    code: '6C'
  },
  {
    key: 105,
    name: 'Bắn súng',
    code: '',
    children: [
      { key: 105_1, name: '- Quân sự', code: '355' },
      { key: 105_2, name: '- thể thao', code: '7A7' }
    ]
  },
  {
    key: 106,
    name: 'Bắp cải',
    code: '635'
  },
  {
    key: 107,
    name: 'Bắp thịt (sinh lý học)',
    code: '',
    children: [
      { key: 107_1, name: '- bệnh và điều trị', code: '616N' },
      { key: 107_2, name: '- động vật', code: '591' },
      { key: 107_2, name: '- sinh lý', code: '5A2' }
    ]
  },
  {
    key: 108,
    name: 'Bắt cóc con tin',
    code: '327'
  },
  {
    key: 109,
    name: 'Bắt thú biển',
    code: '639'
  },
  {
    key: 110,
    name: 'Bất động sản',
    code: '333'
  },
  {
    key: 111,
    name: 'Bầu - cây',
    code: '635',
    children: [
      { key: 111_1, name: '- ở Việt Nam', code: '34(V)0' },
      { key: 111_2, name: '- ở nước ngoài', code: '34(N)' }
    ]
  },
  {
    key: 112,
    name: 'Bầu cử (chế độ)',
    code: '635',
    children: [
      { key: 111_1, name: '- ở Việt Nam', code: '34(V)0' },
      { key: 111_2, name: '- ở nước ngoài', code: '34(N)' }
    ]
  },
  {
    key: 113,
    name: 'Bèo',
    code: '',
    children: [
      { key: 113_1, name: '- làm phân bón', code: '631' },
      { key: 113_2, name: '- làm thức ăn gia súc', code: '636' },
      { key: 113_3, name: '- ươm thả', code: '633' }
    ]
  },
  {
    key: 114,
    name: 'Béo (bệnh)',
    code: '616N'
  },
  {
    key: 115,
    name: 'Bể bơi',
    code: '7A5'
  },
  {
    key: 116,
    name: 'Bể nuôi cá',
    code: '59(077)'
  },
  {
    key: 117,
    name: 'Bể thận - viêm',
    code: '616N'
  },
  {
    key: 118,
    name: 'Bến cảng',
    code: '6V4'
  },
  {
    key: 119,
    name: 'Bệnh',
    code: '',
    children: [
      { key: 113_1, name: '- phụ nữ', code: '618P' },
      { key: 113_2, name: '- trẻ em', code: '618N' }
    ]
  },
  {
    key: 120,
    name: 'Bệnh học thực vật',
    code: '632'
  },
  {
    key: 121,
    name: 'Bệnh lí học đại cương',
    code: '615'
  },
  {
    key: 122,
    name: 'Bệnh kí sinh trùng',
    code: '616V'
  },
  {
    key: 123,
    name: 'Bệnh nhiễm trùng',
    code: '616V'
  },
  {
    key: 124,
    name: 'Bệnh nội khoa',
    code: '616N'
  },
  {
    key: 125,
    name: 'Bệnh viện',
    code: '610',
    children: [{ key: 125_1, name: '- xây dựng', code: '6X4' }]
  },
  {
    key: 126,
    name: 'Bếp đun',
    code: '6X9',
    children: [{ key: 126_1, name: '- sản xuất', code: '6C4' }]
  },
  {
    key: 127,
    name: 'Bếp ga',
    code: '6C9'
  },
  {
    key: 128,
    name: 'Bếp núc (công việc)',
    code: '6C8'
  },
  {
    key: 129,
    name: 'Bi kịch',
    code: '792'
  },
  {
    key: 130,
    name: 'Bí - trồng trọt',
    code: '635'
  },
  {
    key: 131,
    name: 'Bí tử (thực vật học)',
    code: '586'
  },
  {
    key: 132,
    name: 'Bia đá - khoa nghiên cứu',
    code: '90'
  },
  {
    key: 133,
    name: 'Bia (sản xuất)',
    code: '6C8'
  },
  {
    key: 135,
    name: 'Bia kỉ niệm',
    code: '72'
  },
  {
    key: 136,
    name: 'Bìa cứng',
    code: '6C7'
  },
  {
    key: 137,
    name: 'Biên tập (xuất bản)',
    code: '002'
  },
  {
    key: 138,
    name: 'Biến dị',
    code: '',
    children: [
      { key: 138_1, name: '- động vật', code: '59' },
      { key: 138_2, name: '- người', code: '5A1' },
      { key: 138_3, name: '- thực vật', code: '581' },
      { key: 138_4, name: '- vi sinh vật', code: '57A' },
      { key: 138_5, name: '- yếu tố tiến hóa biển', code: '57' },
      { key: 138_6, name: '- miêu tả từng biển', code: '91' },
      { key: 138_7, name: '- thủy văn học', code: '551' }
    ]
  },
  {
    key: 139,
    name: 'Biện chứng duy vật - quy luật và phạm trù',
    code: '1D'
  },
  {
    key: 140,
    name: 'Biểu diễn thời trang',
    code: '387'
  },
  {
    key: 141,
    name: 'Biểu đồ kĩ thuật',
    code: '607'
  },
  {
    key: 142,
    name: 'Binh chủng (các)',
    code: '355'
  },
  {
    key: 143,
    name: 'Binh pháp',
    code: '355'
  },
  {
    key: 144,
    name: 'Bò - chăn nuôi',
    code: '636'
  },
  {
    key: 145,
    name: 'Bò sát (loài) (động vật học)',
    code: '636'
  },
  {
    key: 145,
    name: 'Bọ chét (côn trùng)',
    code: '592'
  },
  {
    key: 146,
    name: 'Bọ ngựa',
    code: '592'
  },
  {
    key: 147,
    name: 'Bọ nhảy',
    code: '636'
  },
  {
    key: 148,
    name: 'Bói toán',
    code: '29'
  },
  {
    key: 149,
    name: 'Bom',
    code: '355'
  },
  {
    key: 150,
    name: 'Bón phân',
    code: '631'
  },
  {
    key: 151,
    name: 'Bóng (cây)',
    code: '633'
  },
  {
    key: 152,
    name: 'Bóng bàn',
    code: '7A8'
  },
  {
    key: 153,
    name: 'Bóng bầu dục',
    code: '7A8'
  },
  {
    key: 154,
    name: 'Bóng chuyền',
    code: '7A8'
  },
  {
    key: 155,
    name: 'Bóng đá',
    code: '7A8'
  },
  {
    key: 156,
    name: 'Bóng ném',
    code: '7A8'
  },
  {
    key: 157,
    name: 'Bóng nước',
    code: '7A8'
  },
  {
    key: 158,
    name: 'Bóng rổ',
    code: '7A8'
  },
  {
    key: 159,
    name: 'Bóng (trong hội họa)',
    code: '75'
  },
  {
    key: 160,
    name: 'Bóng điện',
    code: '6C2'
  },
  {
    key: 161,
    name: 'Bóng rạ',
    code: '617'
  },
  {
    key: 162,
    name: '',
    code: '',
    children: [
      {
        key: 162_1,
        name: '- của trẻ em',
        code: '618N'
      }
    ]
  },
  {
    key: 163,
    name: 'Bowling',
    code: '7A8'
  },
  {
    key: 164,
    name: 'Bồ câu (chăn nuôi)',
    code: '636'
  },
  {
    key: 165,
    name: 'Bồ đề',
    code: '633'
  },
  {
    key: 166,
    name: 'Bổ sung (sách thư viện)',
    code: '02'
  },
  {
    key: 167,
    name: 'Bổ túc văn hóa',
    code: '02'
  },
  {
    key: 168,
    name: 'Bộ đội',
    code: '',
    children: [
      { key: 168_1, name: '- biên phòng', code: '35' },
      { key: 168_2, name: '- đường sắt', code: '35' },
      { key: 168_3, name: '- nhảy dù', code: '35' },
      { key: 168_4, name: '- ô tô', code: '35' },
      { key: 168_5, name: '- tên lửa', code: '35' },
      { key: 168_6, name: '- thiếu giáp', code: '35' }
    ]
  },
  {
    key: 169,
    name: 'Bộ đổi điện',
    code: '6C2',
    children: [{ key: 169_1, name: '- trong vô tuyến điện', code: '6T2' }]
  },
  {
    key: 170,
    name: 'Bộ máy nhà nước - tổ chức',
    code: '34(V)'
  },
  {
    key: 171,
    name: 'Bộ nhớ (máy tính)',
    code: '6T'
  },
  {
    key: 172,
    name: 'Bộ pin',
    code: '6C2'
  },
  {
    key: 173,
    name: 'Bộ pin',
    code: '6C2'
  },
  {
    key: 174,
    name: 'Bộ xử lý trung tâm',
    code: '6T'
  },
  {
    key: 175,
    name: 'Bông - cây',
    code: '6T'
  },
  {
    key: 176,
    name: 'Bột ăn (Sản xuất)',
    code: '6C8'
  },
  {
    key: 177,
    name: 'Bột kim loại (luyện)',
    code: '6C3'
  },
  {
    key: 178,
    name: 'Bột nặn - chất dẻo',
    code: '6C7'
  },
  {
    key: 179,
    name: 'Bột tẩm (sản xuất)',
    code: '6C8'
  },
  {
    key: 180,
    name: 'Bơi lội',
    code: '7A5'
  },
  {
    key: 181,
    name: 'Bơi thuyền',
    code: '7A5'
  },
  {
    key: 182,
    name: 'Bơm (máy)',
    code: '6C2'
  },
  {
    key: 183,
    name: 'Bụi - vệ sinh',
    code: '613'
  },
  {
    key: 184,
    name: 'Bụi (chống)',
    code: '',
    children: [
      { key: 184_1, name: '- ở mỏ', code: '6C1' },
      { key: 184_2, name: '- trong thành phố', code: '6X9' }
    ]
  },
  {
    key: 185,
    name: 'Bùn khoáng',
    code: '552'
  },
  {
    key: 186,
    name: 'Bún - sản xuất',
    code: '6C8'
  },
  {
    key: 187,
    name: 'Buôn lậu',
    code: '',
    children: [
      { key: 187_1, name: '- kinh tế', code: '339' },
      { key: 187_2, name: '- luật', code: '34(V)' }
    ]
  },
  {
    key: 188,
    name: 'Bút kí',
    code: '',
    children: [{ key: 188_1, name: '- lí luận sáng tác', code: '801' }]
  },
  {
    key: 189,
    name: 'Bút toán',
    code: '317'
  },
  {
    key: 190,
    name: 'Bừa',
    code: '631'
  },
  {
    key: 191,
    name: 'Bức xạ',
    code: '',
    children: [
      { key: 191_1, name: '- di truyền học', code: '57' },
      { key: 191_2, name: '- hóa học', code: '541' },
      { key: 191_3, name: '- vật lý khí quyển', code: '551' }
    ]
  },
  {
    key: 192,
    name: 'Bưởi',
    code: '634'
  },
  {
    key: 193,
    name: 'Bướm - sinh vật học',
    code: '592'
  },
  {
    key: 194,
    name: 'Bưu chính , bưu điện',
    code: '6T1',
    children: [{ key: 194_1, name: '- kinh tế', code: '338' }]
  },
  {
    key: 195,
    name: 'Bưu thiếp (sưu tầm)',
    code: '392'
  },
  {
    key: 196,
    name: 'Ca cao (trồng trọt)',
    code: '633'
  },
  {
    key: 197,
    name: 'Ca hát',
    code: '78'
  },
  {
    key: 198,
    name: 'Ca khúc',
    code: '78'
  },
  {
    key: 199,
    name: 'Ca kịch (nghệ thuật)',
    code: '79'
  },
  {
    key: 200,
    name: 'Ca nhạc kịch',
    code: '78'
  },
  {
    key: 201,
    name: 'Ca nô (chế tạo)',
    code: '6V4'
  },
  {
    key: 202,
    name: 'Cà - trồng trọt',
    code: '635'
  },
  {
    key: 203,
    name: 'Cà phê',
    code: '',
    children: [
      { key: 203_1, name: '- chế biến', code: '6C8' },
      { key: 203_2, name: '- kỹ thuật trồng trọt', code: '633' }
    ]
  },
  {
    key: 204,
    name: 'Cà rốt',
    code: '635'
  },
  {
    key: 205,
    name: 'Cá',
    code: '',
    children: [
      { key: 205_1, name: '- bệnh và vật sát hại', code: '639' },
      { key: 205_2, name: '- công nghiệp chế biến', code: '6C8' },
      { key: 205_2, name: '- đánh bắt', code: '639' },
      { key: 205_2, name: '- động vật học', code: '596' },
      { key: 205_2, name: '- nuôi thả', code: '633' },
      { key: 205_2, name: '- sản xuất nước mắm', code: '633' }
    ]
  },
  {
    key: 206,
    name: 'Cá hộp',
    code: '6C8'
  },
  {
    key: 207,
    name: 'Cá nhân',
    code: '',
    children: [
      { key: 207_1, name: '- chống chủ nghĩa cá nhân', code: '1DL' },
      { key: 207_2, name: '- vai trò cá nhân trong lịch sử', code: '1DL' }
    ]
  },
  {
    key: 208,
    name: 'Cá nhân (thuộc tính tâm lý)',
    code: '15'
  },
  {
    key: 209,
    name: 'Cách âm',
    code: '6X'
  },
  {
    key: 210,
    name: 'Cách điện',
    code: '6c2'
  },
  {
    key: 215,
    name: 'Cách mạng',
    code: '',
    children: [
      { key: 211_1, name: '- lịch sử trên thế giới', code: '9(T)' },
      { key: 211_2, name: '- trong từng nước', code: '32(N)' },
      { key: 211_3, name: '- phong trào hiện đại, trên thế giới', code: '32' },
      { key: 211_4, name: '- ở Việt Nam (lịch sử)', code: '9(V)' }
    ]
  },
  {
    key: 212,
    name: 'Cách mạng khoa học kỹ thuật',
    code: '601'
  },
  {
    key: 213,
    name: 'Cách mạng văn hóa',
    code: '38'
  },
  {
    key: 214,
    name: 'Cách mạng Nga 1905',
    code: 'Cách mạng Pháp 1789'
  },
  {
    key: 215,
    name: 'Cách mangh tháng 8 năm 1945 ở Việt Nam',
    code: '',
    children: [
      { key: 215_1, name: '- lịch sử', code: '9(V)16' },
      { key: 215_2, name: '- vai trò lãnh đạo của Đảng', code: '3KV1' }
    ]
  },
  {
    key: 220,
    name: 'Cách mạng Việt Nam',
    code: '',
    children: [
      { key: 216_1, name: '- đường lối hiện nay', code: '32(V)' },
      { key: 216_2, name: '- lịch sử', code: '9(V)' },
      { key: 216_3, name: '- tiểu sử các nhà hoạt động cách mạng', code: '3KV1(902)' },
      { key: 216_4, name: '- vai trò lãnh đạo của Đảng Cộng sản Việt Nam', code: '3KV5' },
      { key: 216_5, name: '- viện bảo tàng', code: '9(V) (069)' }
    ]
  },
  {
    key: 217,
    name: 'Cách mạng xanh',
    code: '631'
  },
  {
    key: 218,
    name: 'Cách mạng xã hội chủ nghĩa (lí luận)',
    code: '1DL'
  },
  {
    key: 219,
    name: 'Cải bắp',
    code: '635'
  },
  {
    key: 220,
    name: 'Cải cách giáo dục',
    code: '370',
    children: [
      { key: 220_1, name: '- đại học', code: '378' },
      { key: 220_2, name: '- phổ thông', code: '373' }
    ]
  },
  {
    key: 221,
    name: 'Cải cách hành chính',
    code: '',
    children: [
      { key: 221_1, name: '- nước ngoài', code: '34(N)' },
      { key: 221_2, name: '- Việt Nam', code: '(34V)' }
    ]
  },
  {
    key: 222,
    name: 'Cải cách ruộng đất ở Việt Nam',
    code: '33(V)',
    children: [{ key: 222_1, name: '- lịch sử', code: '9(V)2' }]
  },
  {
    key: 223,
    name: 'Cải củ',
    code: '635'
  },
  {
    key: 224,
    name: 'Cải lương',
    code: '',
    children: [
      { key: 223_1, name: '- lí luận sáng tác', code: '8(V)' },
      { key: 223_2, name: '- nghệ thuật', code: '79' },
      { key: 223_3, name: '- tác phẩn dân gian', code: 'KV3' },
      { key: 223_4, name: '- tác phẩn hiện đại', code: 'V22' }
    ]
  },
  {
    key: 225,
    name: 'Cải tạo đất nông nghiệp',
    code: '631',
    children: [
      { key: 225_1, name: '- động vật', code: '636' },
      { key: 225_2, name: '- thực vật', code: '631' }
    ]
  },
  {
    key: 226,
    name: 'Cải tiến kĩ thuật',
    code: '601'
  },
  {
    key: 227,
    name: 'Cam - cây',
    code: '634'
  },
  {
    key: 228,
    name: 'Cam thảo',
    code: '633'
  },
  {
    key: 229,
    name: 'Cảm giác (tâm lí học)',
    code: '15'
  },
  {
    key: 230,
    name: 'Cảm giác và hợp lí (triết)',
    code: '1D'
  },
  {
    key: 231,
    name: 'Cảm lạnh',
    code: '',
    children: [{ key: 231_1, name: '- sinh lí bệnh', code: '615' }]
  },
  {
    key: 232,
    name: 'Cảm ứng',
    code: '',
    children: [
      { key: 232_1, name: '- dụng cụ đo điện', code: '6C2' },
      { key: 232_2, name: '- nung', code: '6C2' }
    ]
  },
  {
    key: 233,
    name: 'Cán bộ - đào tạo',
    code: '',
    children: [
      { key: 232_1, name: '- trong các trường chuyên nghiệp', code: '373' },
      { key: 232_2, name: '- trong các trường đại học', code: '378' }
    ]
  },
  {
    key: 234,
    name: 'Cán bộ khoa học',
    code: '',
    children: [{ key: 232_1, name: '- đào tạo', code: '001(V)' }]
  },
  {
    key: 235,
    name: 'Cán bộ y tế',
    code: '610'
  },
  {
    key: 236,
    name: 'Cảng khẩu (công việc)',
    code: '6V'
  },
  {
    key: 237,
    name: 'Canh tác (kĩ thuật)',
    code: '631'
  },
  {
    key: 238,
    name: 'Cánh cứng - côn trùng',
    code: '592'
  },
  {
    key: 238,
    name: 'Cánh kiến',
    code: '633'
  },
  {
    key: 239,
    name: 'Cánh màng (côn trùng)',
    code: '592'
  },
  {
    key: 240,
    name: 'Cánh thẳng (côn trùng)',
    code: '592'
  },
  {
    key: 241,
    name: 'Cạnh tranh thương mại',
    code: '339'
  },
  {
    key: 242,
    name: 'Cảnh giác chính trị - của nhân dân Việt Nam',
    code: '32(V)'
  },
  {
    key: 243,
    name: 'Cảnh quan đô thị - quy hoạch',
    code: '72'
  },
  {
    key: 244,
    name: 'Cảnh quan học',
    code: '551'
  },
  {
    key: 245,
    name: 'Canxi - kim loại học',
    code: '6C3'
  },
  {
    key: 246,
    name: 'Cao Đài (đạo)',
    code: '29'
  },
  {
    key: 247,
    name: 'Cao lanh',
    code: '',
    children: [
      { key: 247_1, name: '- khai thác', code: '6C1' },
      { key: 247_2, name: '- mỏ', code: '553' }
    ]
  },
  {
    key: 248,
    name: 'Cao lương',
    code: '633'
  },
  {
    key: 249,
    name: 'Cao phân tử (hợp chất)',
    code: '547',
    children: [{ key: 249_1, name: '- công nghiệp hóa học', code: '6C7' }]
  },
  {
    key: 250,
    name: 'Cao su - sản xuất',
    code: '6C7'
  },
  {
    key: 251,
    name: 'Cào cỏ (nông cụ)',
    code: '631'
  },
  {
    key: 252,
    name: 'Cáo',
    code: '639'
  },
  {
    key: 253,
    name: 'Cắm trại',
    code: '',
    children: [
      { key: 253_1, name: '- cho học sinh', code: '373' },
      { key: 253_2, name: '- thể thao', code: '7A6' }
    ]
  },
  {
    key: 254,
    name: 'Cắt cỏ',
    code: '631'
  },
  {
    key: 255,
    name: 'Cắt may trong gia đình',
    code: '6C9'
  },
  {
    key: 256,
    name: 'Câm điếc (tật)',
    code: '617'
  },
  {
    key: 257,
    name: 'Cấm vận',
    code: '',
    children: [
      { key: 257_1, name: '- pháp luật', code: '341' },
      { key: 257_2, name: '- kinh tế', code: '339' }
    ]
  },
  {
    key: 258,
    name: 'Cẩm thạch',
    code: '',
    children: [
      { key: 257_1, name: '- khai thác', code: '6C1' },
      { key: 257_2, name: '- mỏ', code: '553' }
    ]
  },
  {
    key: 259,
    name: 'Cân đo - dụng cụ',
    code: '6C5'
  },
  {
    key: 260,
    name: 'Cận thị (tật)',
    code: '617'
  },
  {
    key: 261,
    name: 'Cấp cứu',
    code: '610',
    children: [{ key: 261_1, name: '- ngoại khoa', code: '617' }]
  },
  {
    key: 262,
    name: 'Cấp cứu trên núi',
    code: '7A6'
  },
  {
    key: 263,
    name: 'Câu cá',
    code: '639'
  },
  {
    key: 264,
    name: 'Câu đối',
    code: 'V19, V29'
  },
  {
    key: 265,
    name: 'Câu lạc bộ - công tác',
    code: '396'
  },
  {
    key: 266,
    name: 'Câu lạc bộ du lịch',
    code: '7A6'
  },
  {
    key: 267,
    name: 'Câu lạc bộ sinh viên',
    code: '378'
  },
  {
    key: 268,
    name: 'Câu lạc bộ thiếu nhi',
    code: '373'
  },
  {
    key: 269,
    name: 'Cầu',
    code: '6X'
  },
  {
    key: 270,
    name: 'Cầu chì',
    code: '6C2'
  },
  {
    key: 271,
    name: 'Cầu lông',
    code: '7A8'
  },
  {
    key: 272,
    name: 'Cầu mây',
    code: '7A8'
  },
  {
    key: 273,
    name: 'Cầu truyền hình',
    code: '79'
  },
  {
    key: 274,
    name: 'Cầu và hầm - xây dựng',
    code: '6X'
  },
  {
    key: 275,
    name: 'Cầu vồng',
    code: '551'
  },
  {
    key: 276,
    name: 'Cấu tạo vật chất (vật lý)',
    code: '530'
  },
  {
    key: 277,
    name: 'Cấu trúc - ngôn ngữ',
    code: '40'
  },
  {
    key: 278,
    name: 'Cấu trúc (đại số)',
    code: '517'
  },
  {
    key: 277,
    name: 'Cây',
    code: '',
    children: [{ key: 277_1, name: '- nông nghiệp', code: '633/635' }]
  },
  {
    key: 279,
    name: 'Cây ăn quả (trồng trọt)',
    code: '634'
  },
  {
    key: 280,
    name: 'Cây cảnh (trồng trọt)',
    code: '634'
  },
  {
    key: 281,
    name: 'Cây có chất kích thích',
    code: '633'
  },
  {
    key: 282,
    name: 'Cây có đường - trồng trọt',
    code: '634'
  },
  {
    key: 283,
    name: 'Cây có nhựa',
    code: '633'
  },
  {
    key: 284,
    name: 'Cây có sợi - trồng trọt',
    code: '633'
  },
  {
    key: 285,
    name: 'Cây công nghiệp',
    code: '633'
  },
  {
    key: 286,
    name: 'Cây đậu đỗ - trồng trọt',
    code: '633'
  }
];
