import { IDENTITY_CONFIG } from 'constants/config';

export const LocalStorageEventTartget = new EventTarget();

/**
 * get user info from session storage
 * @return AuthOidc | null
 */
export const getUserInfoFromSS = (): AuthOidc | null => {
  const result = sessionStorage.getItem(`oidc.user:${IDENTITY_CONFIG.authority}:${IDENTITY_CONFIG.client_id}`) || '';

  return result ? JSON.parse(result) : null;
};

/**
 * clear data in session storage
 */
export const clearDataInSessionStorage = () => {
  const clearLSEvent = new Event('clearLS');
  LocalStorageEventTartget.dispatchEvent(clearLSEvent);
  sessionStorage.removeItem('2001');
  return sessionStorage.removeItem(`oidc.user:${IDENTITY_CONFIG.authority}:${IDENTITY_CONFIG.client_id}`);
};
