import { FilterFilled } from '@ant-design/icons';
import { Dropdown } from 'antd';
import type { ItemType } from 'antd/es/menu/hooks/useItems';
import { Sorter } from 'components/Sorter';
import { CSSProperties } from 'react';

type Props = {
  title: string;
  items: ItemType[];
  onClickFilterIcon: VoidFunction;
  visibleSorter: boolean;
  handleEnterKeyPressed: VoidFunction;
  columnSort: Option;
  enabledOptions?: Array<'equals' | 'contains' | 'startWiths' | 'endWiths' | 'isEmpty' | 'inNotEmpty'>;
  typeSort?: 'text' | 'date' | 'range';
  style?: CSSProperties;
};

const ColumnsTitleSorter = (props: Props) => {
  const {
    items,
    enabledOptions,
    visibleSorter,
    title,
    columnSort,
    typeSort = 'text',
    style,
    handleEnterKeyPressed,
    onClickFilterIcon
  } = props;
  return (
    <div className='relative flex items-center justify-center gap-2'>
      <p>{title}</p>

      <Dropdown menu={{ items }} placement='bottomRight' trigger={['click']}>
        <button onClick={onClickFilterIcon} className='-mt-1'>
          <FilterFilled />
        </button>
      </Dropdown>

      {visibleSorter && (
        <Sorter
          handleEnter={handleEnterKeyPressed}
          column={columnSort}
          enabledOptions={enabledOptions}
          type={typeSort}
          style={style}
        />
      )}
    </div>
  );
};

export default ColumnsTitleSorter;
