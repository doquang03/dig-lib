import { useMutation, useQuery } from '@tanstack/react-query';
import { Checkbox, DatePicker, Radio, RadioChangeEvent } from 'antd';
import { CheckboxChangeEvent } from 'antd/es/checkbox';
import locale from 'antd/es/date-picker/locale/vi_VN';
import { expenseApis, settingApis } from 'apis';
import classNames from 'classnames';
import { Button, Input, Title } from 'components';
import { FORMAT_DATE_PICKER } from 'constants/config';
import dayjs, { Dayjs } from 'dayjs';
import { useEffect, useMemo, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';

const exceptThisSymbols = ['e', 'E', '+', '-', '.', '.'];

const AddExpense = () => {
  const [form, setForm] = useState<{
    ThoiGianTao: string;
    LoaiKinhPhi: string;
    NoiDung: string;
    TongTien: string;
    TaoNganSach: boolean;
  }>({ ThoiGianTao: dayjs().toISOString(), LoaiKinhPhi: 'Thu', NoiDung: '', TongTien: '', TaoNganSach: false });
  const [error, setError] = useState<boolean>(false);

  const navigate = useNavigate();

  const { id } = useParams();

  const { isLoading: loadingExpense } = useQuery({
    queryKey: ['getOneExpense', id],
    queryFn: () => expenseApis.getOne(id + ''),
    enabled: Boolean(id),
    onSuccess: (data) => {
      const { GiaTien, NoiDung, ThoiGianTao, TrangThai } = data.data.Item;
      setForm({
        ThoiGianTao,
        LoaiKinhPhi: TrangThai,
        NoiDung,
        TongTien: GiaTien.toString()
          .replace(/\D/g, '')
          .replace(/\B(?=(\d{3})+(?!\d))/g, ','),
        TaoNganSach: false
      });
    },
    onError: () => {
      navigate(-1);
    }
  });

  const { data: startDateData } = useQuery({
    queryKey: ['startDate'],
    queryFn: settingApis.getStartDate
  });

  const { mutate: createExpense, isLoading: loadingCreate } = useMutation({
    mutationFn: expenseApis.create,
    onSuccess: () => {
      toast.success('Tạo sổ theo dõi kinh phí thành công');
      navigate(-1);
    }
  });

  const { mutate: updateExpense, isLoading: loadingUpdate } = useMutation({
    mutationFn: expenseApis.update,
    onSuccess: () => {
      toast.success('Cập nhật sổ theo dõi kinh phí thành công');
    }
  });

  const date = useMemo(() => {
    if (!startDateData?.data.Item.NgayBatDauNamHoc) return;

    const date = startDateData.data.Item.NgayBatDauNamHoc.split('/');

    const dateResult = `${date[1]}/${date[0]}/${new Date().getFullYear()}`;

    return dateResult;
  }, [startDateData?.data.Item.NgayBatDauNamHoc]);

  useEffect(() => {
    if (form.NoiDung && form.TongTien) {
      setError(false);
    }
  }, [form.NoiDung, form.TongTien]);

  const handleChangedate = (value: Dayjs | null) => {
    if (!value) return;
    setForm((prevState) => {
      return {
        ...prevState,
        ThoiGianTao: value.toISOString()
      };
    });
  };

  const handleChangeStatus = (e: RadioChangeEvent) => {
    if (!e) return;

    setForm((prevState) => {
      return {
        ...prevState,
        LoaiKinhPhi: e.target.value
      };
    });
  };

  const handleChangeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (!e) return;

    const { name, value } = e.target;

    setForm((prevState) => {
      return {
        ...prevState,
        [name]: value.replace(/ +(?= )/g, '')
      };
    });
  };

  const handleChangeInput1 = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (!e) return;

    const { name, value } = e.target;

    setForm((prevState) => {
      return {
        ...prevState,
        [name]: value.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',')
      };
    });
  };

  const handleBack = () => navigate(-1);

  const handleSubmit = () => {
    const { LoaiKinhPhi, NoiDung, ThoiGianTao, TongTien, TaoNganSach } = form;

    if (!NoiDung || NoiDung === ' ' || !TongTien || Number(TongTien.replaceAll(',', '')) <= 0) {
      setError(true);

      return;
    }

    if (id) {
      updateExpense({ Id: id, ThoiGianTao, TrangThai: LoaiKinhPhi, NoiDung, GiaTien: +TongTien.replaceAll(',', '') });
    } else {
      createExpense({
        ThoiGianTao,
        TrangThai: LoaiKinhPhi,
        NoiDung,
        GiaTien: +TongTien.replaceAll(',', ''),
        TaoNganSach
      });
    }
  };

  const handleChangeBudget = (e: CheckboxChangeEvent) => {
    setForm((prevState) => {
      return {
        ...prevState,
        TaoNganSach: e.target.checked
      };
    });
  };

  return (
    <div className='my-3'>
      <Title title={id ? 'Chỉnh sửa kinh phí thư viện' : 'Thêm kinh phí thư viện'} />

      <div className='mt-3 flex w-1/2 flex-col gap-4'>
        <div>
          <label className='mb-1 font-semibold'>
            Thời gian <span className='text-danger-10'>*</span>
          </label>

          <DatePicker
            format={FORMAT_DATE_PICKER}
            className='w-full py-2'
            placeholder='Chọn ngày'
            clearIcon={false}
            locale={locale}
            value={dayjs(form.ThoiGianTao)}
            disabledDate={(current) => {
              return (
                (current && current > dayjs().endOf('day')) || current < dayjs(date).endOf('day').subtract(1, 'year')
              );
            }}
            onChange={handleChangedate}
            disabled={(Boolean(id) && loadingExpense) || loadingCreate || loadingUpdate}
          />
        </div>

        <div className={form.NoiDung === 'Quỹ ngân sách của trường' ? 'hidden' : 'flex flex-col'}>
          <label className='mb-1 font-semibold'>Loại kinh phí</label>

          <div className='flex items-center gap-1'>
            <Radio
              value={'Thu'}
              disabled={Boolean(id)}
              onChange={handleChangeStatus}
              checked={form.LoaiKinhPhi === 'Thu'}
            >
              Thu
            </Radio>

            <Radio
              value={'Chi'}
              disabled={Boolean(id)}
              onChange={handleChangeStatus}
              checked={form.LoaiKinhPhi === 'Chi'}
            >
              Chi
            </Radio>
          </div>
        </div>

        <div className={form.NoiDung === 'Quỹ ngân sách của trường' ? 'hidden' : 'flex flex-col'}>
          <label className='mb-1 font-semibold'>
            Nội dung <span className='text-danger-10'>*</span>
          </label>

          <Input
            component={'textarea'}
            onChange={handleChangeInput}
            errorMessage={(error && !form.NoiDung) || form.NoiDung === ' ' ? 'Nội dung không được để trống' : ''}
            value={form.NoiDung}
            name='NoiDung'
            disabled={(Boolean(id) && loadingExpense) || loadingCreate || loadingUpdate}
          />
        </div>

        <div>
          <label className='mb-1 font-semibold'>
            Tổng tiền <span className='text-danger-10'>*</span>
          </label>

          <Input
            // type='number'
            placeholder='Nhập tổng tiền'
            containerClassName={classNames('pl-2 bg-white w-full border rounded-lg flex mb-1', {
              'border-danger-10':
                (error && !form.TongTien) ||
                (error && form.TongTien.replaceAll(',', '') && Number(form.TongTien.replaceAll(',', '')) <= 0)
            })}
            className='mr-2 w-full outline-none'
            right={
              <div className='rounded-br-md rounded-tr-md bg-secondary-20/30 py-3 px-3'>
                <span className='text-xs'>đồng</span>
              </div>
            }
            onKeyDown={(e) => {
              if (exceptThisSymbols.includes(e.key)) {
                e.preventDefault();
              }
            }}
            min={1}
            maxLength={12}
            onChange={handleChangeInput1}
            value={form.TongTien}
            name='TongTien'
            disabled={(Boolean(id) && loadingExpense) || loadingCreate || loadingUpdate}
          />

          {error && !form.TongTien && <p className='text-danger-10'>Tổng tiền được để trống</p>}

          {error && form.TongTien && Number(form.TongTien.replaceAll(',', '')) <= 0 && (
            <p className='text-danger-10'>Tổng tiền phải lớn hơn 0</p>
          )}
        </div>

        {form.LoaiKinhPhi === 'Chi' && (
          <>
            <Checkbox
              type='checkbox'
              checked={form.TaoNganSach}
              onChange={handleChangeBudget}
              disabled={Boolean(id) || loadingCreate || loadingUpdate}
            >
              Tạo ngân sách
            </Checkbox>
          </>
        )}
      </div>

      <div className='flex items-center justify-end gap-2'>
        <Button
          variant='secondary'
          onClick={handleBack}
          loading={(Boolean(id) && loadingExpense) || loadingCreate || loadingUpdate}
          disabled={(Boolean(id) && loadingExpense) || loadingCreate || loadingUpdate}
          type='button'
        >
          Quay về
        </Button>

        <Button
          onClick={handleSubmit}
          disabled={(Boolean(id) && loadingExpense) || loadingCreate || loadingUpdate}
          loading={(Boolean(id) && loadingExpense) || loadingCreate || loadingUpdate}
          type='button'
        >
          {Boolean(id) ? 'Cập nhật' : 'Thêm mới'}
        </Button>
      </div>
    </div>
  );
};

export default AddExpense;
