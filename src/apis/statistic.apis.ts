import httpThuVien from 'utils/httpThuVien';
import { serialize } from 'utils/utils';

const endPoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/Statistic`;
const endPointThongKe = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/ThongKe`;

const statisticApis = {
  DataInformation: () => httpThuVien.get(`/${endPoint}/DataInformation`),
  TTSach: () => httpThuVien.get(`/${endPoint}/TTSach`),
  BieuDoPhieuMuonTheoNam: (year: string, typeAccount: string) =>
    httpThuVien.get(`/${endPoint}/BieuDoPhieuMuon?year=${year}&typeAccount=${typeAccount}`),
  BieuDoPhieuMuonTheoThang: (month: string, year: string, typeAccount: string) =>
    httpThuVien.get<
      ResponseApi<{
        Month: number;
        SoNgayTrongThang: number;
        SumSL: number;
        Year: number;
        lsoNguoiKhongTraTrongNam: number[];
        lsoNguoiKhongTraTrongNgay: number[];
        lsoNguoiKhongTraTrongQuy: number[];
        lsoNguoiMuonSachTrongNam: number[];
        lsoNguoiMuonSachTrongQuy: number[];
        lsoNguoiMuonTrongNgay: number[];
        lsoNguoiMuonTaiChoTrongNgay: number[];
        lsoNguoiTraTreTrongNam: number[];
        lsoNguoiTraTreTrongNgay: number[];
        lsoNguoiTraTreTrongQuy: number[];
        lsoNguoiTraTrongNgay: number[];
        lsoPMTrongNgay: number[];
        lsoPhieuMuonTrongNam: number[];
        lsoPhieuMuonTrongQuy: number[];
        lsoSachDuocMuonTrongNam: number[];
        lsoSachDuocMuonTrongNgay: number[];
        lsoSachDuocMuonTrongQuy: number[];
        lsoSachDuocTraTrongNam: number[];
        lsoSachDuocTraTrongNgay: number[];
        lsoSachDuocTraTrongQuy: number[];
        lsoSachKhongTraTrongNam: number[];
        lsoSachKhongTraTrongNgay: number[];
        lsoSachKhongTraTrongQuy: number[];
        soNguoiTraSachTrongNam: number[];
      }>
    >(`/${endPoint}/BieuDoPhieuMuon?year=${year}&month=${month}&typeAccount=${typeAccount}`),
  StartDayAndlastDay: (dateTime: string, typeAccount: string) =>
    httpThuVien.get(`/${endPoint}/StartDayAndlastDay?dateTime=${dateTime}&typeAccount=${typeAccount}`),
  getDanhMucMuonSach: (
    params: Partial<{
      day: string;
      month: string;
      year: string;
      TinhTrang: 0 | 1 | 2 | 3 | 4 | 5;
      ListTinhTrang: Array<0 | 1 | 2 | 3 | 4 | 5>;
      styleUser: '' | 'hs' | 'gv';
      page: number;
      pageSize: number;
      SortMaKSCB: boolean;
      SortTenSach: boolean;
      SortNguoiMuon: boolean;
      SortNgayMuon: boolean;
      SortNgayPhaiTra: boolean;
      MaKSCB: SearchInputField;
      TenSach: SearchInputField;
      NguoiMuon: SearchInputField;
      NgayMuon: SearchRangeField;
      NgayPhaiTra: SearchRangeField;
      IdTinhTrang: SearchInputField;
    }>,
    isTaiCho?: boolean
  ) => {
    if (isTaiCho === undefined) isTaiCho = false;
    return httpThuVien.post<
      ResponseApi<{ SoSachDuocMuon: number; SoNguoiMuonSach: number; ListRutGon: []; count: number }>
    >(`/${endPointThongKe}/DanhSachMuonSach?isTaiCho=${isTaiCho}`, params);
  },
  downExcelThongKeMuonSach: (
    params?: Partial<{
      day: string;
      month: string;
      year: string;
      TinhTrang?: 0 | 1 | 2 | 3 | 4 | 5;
      styleUser: '' | 'hs' | 'gv';
      Online?: 0 | 1;
    }>,
    isTaiCho?: boolean
  ) => {
    if (isTaiCho === undefined) isTaiCho = false;
    return httpThuVien.post(`${endPointThongKe}/DownExcelThongKeMuonSach?isTaiCho=${isTaiCho}`, params, {
      responseType: 'arraybuffer'
    });
  },
  DownExcelSoMuonSach: (
    params?: Partial<{
      day: string;
      month: string;
      year: string;
      TinhTrang?: 0 | 1 | 2 | 3 | 4 | 5;
      styleUser: '' | 'hs' | 'gv';
      Online?: 0 | 1;
    }>,
    isTaiCho?: boolean,
    optionSplit?: boolean
  ) => {
    if (isTaiCho === undefined) isTaiCho = false;
    if (optionSplit === undefined) optionSplit = false;
    return httpThuVien.post(
      `${endPointThongKe}/DownExcelSoMuonSach?isTaiCho=${isTaiCho}&optionSplit=${optionSplit}`,
      params,
      {
        responseType: 'arraybuffer'
      }
    );
  },
  downloadExcelNotReturn: (
    params: Partial<{
      date: string;
      typeAccount: string;
      optionSplit: boolean;
    }>
  ) => {
    return httpThuVien.post(`${endPointThongKe}/DownloadDanhSachChuaTra`, params, {
      responseType: 'arraybuffer'
    });
  },
  downloadExcelThongKeThanhLy: (
    params?: Partial<{
      idKho: string;
      TuNgay: string;
      Denngay: string;
      sort: string;
    }>
  ) => {
    return httpThuVien.post(
      `${endPointThongKe}/DownloadExcelThongKeXuatSach?${serialize(params)}`,
      {},
      {
        responseType: 'arraybuffer'
      }
    );
  },
  downloadExcel_SLLDM: (
    params?: Partial<{
      idKho: string;
      TuNgay: string;
      Denngay: string;
      sort: string;
    }>
  ) => {
    return httpThuVien.post(
      `${endPointThongKe}/DownloadExcel_SLLDM?${serialize(params)}`,
      {},
      {
        responseType: 'arraybuffer'
      }
    );
  },
  getThongKeThanhLy: (payloads: {
    idKho?: string;
    TuNgay: string;
    DenNgay: string;
    page?: number;
    pageSize?: number;
    sort?: string;
  }) =>
    httpThuVien.post<ResponseApi<{ List_ThongKePXS: StatisticEpxort[]; TongSoLuong: number; TongTienThanhLy: number }>>(
      `${endPointThongKe}/ThongKeXuatSach_Kho?${serialize(payloads)}`
    ),
  GetData_SLAP: (payloads: {
    TuNgay: string;
    DenNgay: string;
    page?: number;
    pageSize?: number;
    sort?: string;
    IsGlobal: string | boolean | undefined;
  }) => {
    const newParams = {
      TuNgay: payloads.TuNgay,
      DenNgay: payloads.DenNgay,
      page: payloads.page,
      pageSize: payloads.pageSize,
      sort: payloads.sort,
      IsGlobal: payloads.IsGlobal === '1' ? true : payloads.IsGlobal === '0' ? false : undefined
    };
    if (newParams.IsGlobal === undefined) delete newParams.IsGlobal;
    return httpThuVien.post<
      ResponseApi<{
        ListKho: NumberOfBook[];
        TongSoLuong: number;
        SumTL: number;
        SumSLTLAnPham: number;
        SumSLLuotMuon: number;
      }>
    >(`${endPointThongKe}/GetData_SLAP?${serialize(newParams)}`);
  },
  GetData_SLLDM: (payloads: { TuNgay: string; DenNgay: string; page?: number; pageSize?: number; sort?: string }) =>
    httpThuVien.post<
      ResponseApi<{
        ListDauSach: NumberOfReading[];
        TongSoLuong: number;
      }>
    >(`${endPointThongKe}/GetData_SLLDM?${serialize(payloads)}`),
  downloadSoDangKyTongQuatKho: (payload: { YearFrom: number; YearTo: number; Down: boolean; currentPage?: number }) =>
    httpThuVien.post(
      `${endPointThongKe}/SoDangKyTongQuatKho?${serialize(payload)}`,
      {},
      { responseType: 'arraybuffer' }
    ),
  ViewA4SoDangKyCaBiet_GetData_Kho: (payload: {
    Month: string;
    Year: string;
    IdKhoSach: string;
    CurrentPage?: string;
    PageSize?: string;
  }) => httpThuVien.post(`${endPointThongKe}/ViewA4SoDangKyCaBiet_GetData_Kho`, payload),
  GetData_SoDKTQKho: (payload: { YearFrom: number; YearTo: number; currentPage?: number }) =>
    httpThuVien.post(`${endPointThongKe}/GetData_SoDKTQKho?${serialize(payload)}`),
  DownSoDangKyCaBiet_Kho: (payload: { Month?: string; Year?: string; IdKhoSach: string }) =>
    httpThuVien.post(
      `${endPointThongKe}/DownSoDangKyCaBiet_Kho?${serialize(payload)}`,
      {},
      { responseType: 'arraybuffer' }
    ),
  DownSoDangKyCaBietSGK_Kho: (payload: { month?: string; year?: string; IdKho: string }) =>
    httpThuVien.post(
      `${endPointThongKe}/DownSoDangKyCaBietSGK_Kho?${serialize(payload)}`,
      {},
      { responseType: 'arraybuffer' }
    ),
  PreviewSoDangKySGK_Kho: (payload: { month?: string; year?: string; idKhoSach: string }) =>
    httpThuVien.post(`${endPointThongKe}/PreviewSoDangKySGK_Kho?${serialize(payload)}`),
  ThuMucSachKho: (payload: { Sort: string; idKho: string }) =>
    httpThuVien.post(`${endPointThongKe}/ThuMucSachKho?${serialize(payload)}`, {}, { responseType: 'arraybuffer' }),
  XuatBaoCao_Json: (payload: { year: string }) =>
    httpThuVien.post(`${endPointThongKe}/XuatBaoCao_Json?${serialize(payload)}`),
  setDatabaseName: (payload: { idUser: string; DatabaseName: string; EnableEdit: boolean; tenDonVi: string }) =>
    httpThuVien.post(`${endPointThongKe}/SetDatabaseName?${serialize(payload)}`),
  GetDataFromDatabaseName: (listDatabaseName: string[], TuNgay: string, DenNgay: string) =>
    httpThuVien.post<ResponseApi<UnitLibrary[]>>(
      `${endPointThongKe}/GetDataFromDatabaseName?TuNgay=${TuNgay}&DenNgay=${DenNgay}`,
      listDatabaseName
    ),
  GetDataCount: () =>
    httpThuVien.get<
      ResponseApi<{ DangDatMuon: number; DangMuon: number; DangMuonTreHan: number; DangMuonDenHanTra: number }>
    >(`${endPointThongKe}/GetDataCount`),
  DownloadDataFromDatabaseName: (payload: {
    ListDatabase: string[];
    ListName: string[];
    TuNgay: string;
    DenNgay: string;
  }) => httpThuVien.post(`${endPointThongKe}/DownloadDataFromDatabaseName`, payload, { responseType: 'arraybuffer' }),
  getDashboarData: (params: { TuNgay: string; DenNgay: string; databaseNames: string[] }) =>
    httpThuVien.post<
      ResponseApi<{
        TongGiaoVien: number;
        TongTaiLieuSo: number;
        TongHocSinh: number;
        TongTaiLieuGiay: number;
        listModel: {
          DataBaseName: string;
          TongGiaoVien: number;
          TongHocSinh: number;
          TongLuotMuon: number;
          TongTaiLieuGiay: number;
          TongTaiLieuSo: number;
        }[];
      }>
    >(`${endPointThongKe}/GetDataDashBoard?TuNgay=${params.TuNgay}&DenNgay=${params.DenNgay}`, params.databaseNames),
  getSchoolDashboardData: (params: { TuNgay: string; DenNgay: string }) =>
    httpThuVien.post<ResponseApi<SchoolDashboard>>(
      `${endPointThongKe}/GetDataDashBoardUnit?TuNgay=${params.TuNgay}&DenNgay=${params.DenNgay}`
    ),
  getSchoolDashboardInteraction: (params: { TuNgay: string; DenNgay: string; isGlobal: boolean }) =>
    httpThuVien.post<
      ResponseApi<{
        LuotDoc: {
          [key: string]: number;
        };
        LuotTai: {
          [key: string]: number;
        };
        LuotMuon: {
          [key: string]: number;
        };
      }>
    >(
      `${endPointThongKe}/GetDataDashBoardUnitChart?TuNgay=${params.TuNgay}&DenNgay=${params.DenNgay}&IsGlobal=${params.isGlobal}`
    ),
  getTheMostBorrowingBooks: (params: { TuNgay: string; DenNgay: string; Page: number; IdKho: string }) =>
    httpThuVien.post<ResponseApi<{ ListModel: { TenSach: string; Value: number }[] }>>(
      `${endPointThongKe}/GetDataDashBoardUnitTopMuonSach?TuNgay=${params.TuNgay}&DenNgay=${params.DenNgay}&Page=${params.Page}&IdKho=${params.IdKho}`
    )
};

export default statisticApis;
