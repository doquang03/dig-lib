import { path } from 'constants/path';
import { Outlet, useMatch } from 'react-router-dom';
import TopicManagermentContent from './TopicManagermentContent/TopicManagermentContent';

const TopicManagementPage = () => {
  const match = useMatch(path.chudiem);

  return <>{Boolean(match) ? <TopicManagermentContent /> : <Outlet />}</>;
};

export default TopicManagementPage;
