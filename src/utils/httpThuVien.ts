import axios, { HttpStatusCode } from 'axios';
import { toast } from 'react-toastify';
import { clearDataInSessionStorage, getUserInfoFromSS } from './auth';
import { isAxiosUnauthorizedError } from './utils';

const httpThuVien = axios.create({
  baseURL: process.env.REACT_APP_API_BASE_URL,
  headers: { 'Content-Type': 'application/json', Accept: '*/*' }
});

// Add a request interceptor
httpThuVien.interceptors.request.use(
  function (config) {
    // check access token is existed
    let accessToken = getUserInfoFromSS()?.access_token;
    if (!!accessToken && config.headers) {
      config.headers.authorization = `Bearer ${accessToken}`;
      return config;
    }
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
httpThuVien.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    // toast error message exclude 401 and 422
    if (
      ![HttpStatusCode.UnprocessableEntity, HttpStatusCode.Unauthorized].includes(error.response?.status as number) &&
      (error.response?.data?.Code as number) !== 0 &&
      !error.config?.url.includes('ThanhVien/EditById') &&
      !error.config?.url.includes('MemberConnection/ConnectKey') &&
      !error.config?.url.includes('Document/InsertAndEdit')
    ) {
      const data: any | undefined = error.response?.data;

      const message = data?.Message || error.message;
      toast.error(message || 'Đã có lỗi xảy ra');
      localStorage.removeItem('CREATED_FAILED_TIME');
    }

    // catch Unauthorized error
    if (isAxiosUnauthorizedError(error) && error.response?.status === HttpStatusCode.Unauthorized) {
      clearDataInSessionStorage();
    }
    return Promise.reject(error);
  }
);
export default httpThuVien;
