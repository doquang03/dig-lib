type DocumentFolder = {
  Id: string;
  TenThuMuc: string;
  GhiChu: string;
  CreateDateTime: string;
  IdParent: string;
  MaThuMuc: string;
  Type: 'image' | 'document' | 'audio' | 'video' | 'paper';
  IsActive: boolean;
  IsUsed: boolean;
};
