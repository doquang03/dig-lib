import { ModalDelete, TitleDelete } from 'components';

type Props = {
  selectedStudent?: Student;
  type: 'lock' | 'unlock' | 'delete' | undefined;
  onCancel: () => void;
  onOk: () => void;
  loading: boolean;
};

const ActiveModal = (props: Props) => {
  const { selectedStudent, type, onCancel, onOk, loading } = props;

  return (
    <ModalDelete
      open={type && Boolean(selectedStudent) && ['lock', 'unlock'].includes(type)}
      closable={false}
      title={
        <TitleDelete
          firstText={`Bạn có chắn chắn muốn ${type === 'lock' ? 'khóa' : 'kích hoạt'} học sinh`}
          secondText={selectedStudent?.Ten}
        ></TitleDelete>
      }
      handleCancel={onCancel}
      handleOk={onOk}
      labelOK={type === 'lock' ? 'Khóa' : 'Kích hoạt'}
      loading={loading}
    />
  );
};

export default ActiveModal;
