import { isEmpty, omitBy } from 'lodash';
import httpThuVien from 'utils/httpThuVien';

const endpoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/BaoTapChi`;
const registerEndpoint = `${process.env.REACT_APP_SERVICE_THUVIENSO}/api/DangKyBaoTapChi`;

const newspaperMagazineApis = {
  getData: (payload: { TextForSearch?: string; isBao?: boolean; KyHan?: string; page: number; pageSize: number }) => {
    const result = omitBy(payload, isEmpty);

    return httpThuVien.post<ResponseApi<{ ListBaoTapChi: NewspaperMagazine[]; TotalCount: number }>>(
      `/${endpoint}/Index`,
      { ...result, isBao: payload.isBao }
    );
  },
  getOneData: (id: string) => httpThuVien.get<ResponseApi<NewspaperMagazine>>(`/${endpoint}/ChiTietBaoTapChi?Id=${id}`),
  createNewspapgerMagazine: (payload: {
    MaBaoTapChi: string;
    Ten: string;
    ISSN: string;
    TenSongNgu: string;
    KyHan: string;
    IdNgonNgu: string;
    CQBienTap: string;
    CQPhatHanh: string;
    CoGiay: string;
    SoTrang: number;
    GiaBia: number;
    isBao: boolean;
  }) =>
    httpThuVien.post<ResponseApi<{ ListBaoTapChi: NewspaperMagazine[]; TotalCount: number }>>(
      `/${endpoint}/Create`,
      payload
    ),
  updateNewspapgerMagazine: (payload: {
    Id: string;
    MaBaoTapChi: string;
    Ten: string;
    ISSN: string;
    TenSongNgu: string;
    KyHan: string;
    IdNgonNgu: string;
    CQBienTap: string;
    CQPhatHanh: string;
    CoGiay: string;
    SoTrang: number;
    GiaBia: number;
    isBao: boolean;
  }) =>
    httpThuVien.post<ResponseApi<{ ListBaoTapChi: NewspaperMagazine[]; TotalCount: number }>>(
      `/${endpoint}/Edit`,
      payload
    ),
  delete: (id: string) => httpThuVien.delete<ResponseApi>(`/${endpoint}/Delete/${id}`),
  deleteMultiRecords: (ids: string[]) => httpThuVien.delete<ResponseApi>(`/${endpoint}/DeleteMulti`, { data: ids }),
  getNewspaperMagazineRegisteration: ({ Min, Max }: { Min: string; Max: string }) =>
    httpThuVien.post<ResponseApi<{ ListDangKyBaoTapChi: NewspaperMagazineRegisteration[] }>>(
      `/${registerEndpoint}/Index`,
      {
        Ngay: {
          Min,
          Max
        }
      }
    ),
  modifyCell: (payload: NewspaperMagazineRegisteration) =>
    httpThuVien.post<ResponseApi>(`/${registerEndpoint}/Modify`, payload),
  registerNewspaperMagazine: (payload: {
    StartDay: string;
    EndDay: string;
    SoLuong: number;
    IdBaoTapChi: string;
    isTuan: boolean;
    ListDay: string[];
  }) => httpThuVien.post<ResponseApi>(`/${registerEndpoint}/UpdatePeriodic`, payload),
  printRegisteration: (payload: { Ngay: SearchRangeField[]; IdBaoTapChi: string; isNgay: boolean }) =>
    httpThuVien.post(`/${registerEndpoint}/Print`, payload, { responseType: 'arraybuffer' })
};

export default newspaperMagazineApis;
