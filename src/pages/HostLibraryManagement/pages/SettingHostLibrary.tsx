import { useMutation, useQuery } from '@tanstack/react-query';
import { connectionApis } from 'apis';
import { Button, Loading, ModalDelete, Select, Title, TitleDelete } from 'components';
import { useMemo, useState } from 'react';
import { toast } from 'react-toastify';

const SettingHostLibrary = () => {
  const [value, setValue] = useState<string>('');
  const [visibleModal, setVisibleModal] = useState<boolean>(false);

  const { data: formOptionsData, isLoading: isLoadingForm } = useQuery({
    queryKey: ['getListOfUnitDeparmentEduFull'],
    queryFn: connectionApis.getListOfUnitDeparmentEduFull
  });

  const { isLoading: isLoadingHostLibrary } = useQuery({
    queryKey: ['GetHostLibrary'],
    queryFn: connectionApis.GetHostLibrary,
    onSuccess: (data) => {
      setValue(data.data.Item.WorkingId || '');
    }
  });

  const { mutate: setHostLibrary, isLoading: loadingSetHostLibrary } = useMutation({
    mutationFn: (id: string) => connectionApis.SetHostLibrary(id),
    onSuccess: () => {
      toast.success('Cập nhật thư viện chủ trì thành công');
      // setValue(undefined);
      setVisibleModal(false);
    }
  });

  const options = useMemo(() => {
    if (!formOptionsData?.data.Item) return;

    return [{ label: 'Thư viện trung tâm', value: '' }, ...formOptionsData?.data.Item.DonVi];
  }, [formOptionsData]);
  return (
    <div className='p-5'>
      <Title title={`Chọn thư viện chủ trì`} />
      <div className='my-3 flex items-center gap-1'>
        <svg width='22' height='22' viewBox='0 0 22 22' fill='none' xmlns='http://www.w3.org/2000/svg'>
          <g clipPath='url(#clip0_7198_600)'>
            <path
              d='M11 21.3125C16.6954 21.3125 21.3125 16.6954 21.3125 11C21.3125 5.30456 16.6954 0.6875 11 0.6875C5.30456 0.6875 0.6875 5.30456 0.6875 11C0.6875 16.6954 5.30456 21.3125 11 21.3125Z'
              fill='#FF7D00'
            />
            <path
              d='M10.1595 13.0952C10.1469 13.0173 10.1406 12.9422 10.1406 12.87C10.1406 12.7979 10.1406 12.7222 10.1406 12.6432C10.1313 12.2629 10.2009 11.8848 10.3452 11.5329C10.4711 11.2358 10.6453 10.9617 10.8608 10.7216C11.0672 10.4975 11.2977 10.2969 11.5483 10.1235C11.7912 9.95276 12.0204 9.78088 12.2358 9.60786C12.428 9.45371 12.6011 9.27714 12.7514 9.08192C12.8902 8.89428 12.9622 8.66556 12.9559 8.43224C12.9596 8.27137 12.9228 8.11216 12.8489 7.96921C12.7751 7.82627 12.6665 7.70417 12.5331 7.61411C12.2501 7.41015 11.8118 7.30817 11.2183 7.30817C10.9356 7.30494 10.6545 7.34909 10.3864 7.4388C10.1435 7.5219 9.90876 7.62719 9.68516 7.75333C9.48605 7.86515 9.29585 7.99214 9.11625 8.13317C8.9501 8.26494 8.80401 8.37437 8.67797 8.46145L7.73438 7.23942C7.97111 6.98366 8.23837 6.75796 8.53016 6.56739C8.8259 6.37358 9.14168 6.21223 9.47203 6.08614C9.80332 5.9595 10.1457 5.86398 10.4947 5.80083C10.835 5.73886 11.1801 5.70723 11.5259 5.7063C12.587 5.7063 13.412 5.92802 14.0009 6.37145C14.2835 6.57454 14.5118 6.84391 14.6658 7.15588C14.8198 7.46786 14.8949 7.8129 14.8844 8.16067C14.8984 8.58355 14.821 9.00448 14.6575 9.39474C14.5215 9.70387 14.3305 9.98573 14.0938 10.2266C13.8614 10.4515 13.6075 10.653 13.3358 10.8282C13.0751 10.9982 12.8264 11.186 12.5916 11.3902C12.3636 11.5871 12.1686 11.8192 12.0141 12.0777C11.8471 12.3863 11.7663 12.7343 11.7803 13.0849L10.1595 13.0952ZM9.86734 15.1697C9.85986 15.0194 9.88467 14.8692 9.94011 14.7293C9.99555 14.5893 10.0803 14.4629 10.1887 14.3585C10.4214 14.1522 10.7255 14.0448 11.0361 14.0594C11.356 14.0417 11.6703 14.1489 11.9127 14.3585C12.021 14.463 12.1057 14.5894 12.1611 14.7293C12.2166 14.8693 12.2414 15.0194 12.2341 15.1697C12.2405 15.3205 12.2152 15.4709 12.1599 15.6112C12.1045 15.7515 12.0203 15.8787 11.9127 15.9844C11.6724 16.1982 11.3572 16.3082 11.0361 16.2904C10.8817 16.2976 10.7274 16.2743 10.582 16.2218C10.4366 16.1693 10.3029 16.0886 10.1887 15.9844C10.0814 15.8785 9.99755 15.7513 9.94248 15.611C9.88742 15.4707 9.8624 15.3203 9.86906 15.1697H9.86734Z'
              fill='white'
            />
          </g>
          <defs>
            <clipPath id='clip0_7198_600'>
              <rect width='22' height='22' fill='white' />
            </clipPath>
          </defs>
        </svg>

        <p className='font-bold text-primary-10'>Hướng dẫn:</p>
      </div>

      <ul className='ml-6 list-inside list-disc'>
        <li className='text-primary-10'>
          Hãy chọn một thư viện làm thư viện chủ trì Mục lục liên hợp trong danh sách những thư viện đang trực thuộc
          Sở/Phòng giáo dục và có sử dụng phần mềm quản lý Thư viện B.Lib
        </li>
      </ul>

      <div className='form-group row' style={{ marginBottom: '10px' }}>
        <label
          className='col-form-label col-sm-2 font-bold'
          style={{ marginBottom: '0', paddingTop: '5px', width: '100%' }}
        >
          Thư viện chủ trì <span style={{ color: 'red' }}>*</span>
        </label>
      </div>

      <div className='form-group row'>
        <div className='component-form-group flex '>
          <Select
            items={options || []}
            className='w-full grow'
            value={value || ''}
            onChange={(value) => setValue(value.target.value)}
          />

          <div className='flex shrink-0'>
            <Button
              variant='default'
              type='submit'
              className='ml-2 font-semibold'
              onClick={() => setVisibleModal(true)}
            >
              Xác nhận
            </Button>
          </div>
        </div>
      </div>

      <ModalDelete
        open={visibleModal}
        closable={false}
        title={
          <TitleDelete
            firstText={'Bạn có chắn chắn muốn chọn thư viện'}
            secondText={`${options?.find((item) => item.value === value)?.label} `}
            thirdText='làm thư viện chủ trì không?'
          ></TitleDelete>
        }
        labelOK='Xác nhận'
        handleCancel={() => {
          setVisibleModal(false);
        }}
        handleOk={() => {
          setHostLibrary(value || '');
        }}
      />

      <Loading open={isLoadingForm || isLoadingHostLibrary || loadingSetHostLibrary}></Loading>
    </div>
  );
};

export default SettingHostLibrary;
