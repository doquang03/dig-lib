import { useUser } from 'contexts/user.context';
import 'css/HomePage.css';
import 'css/bootstrap.min.css';
import { getUnitFromSS } from 'utils/utils';
import { EducationHome, EducationHomeClassic, SchoolHome } from './components';
import { path } from 'constants/path';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';

const HomePage = () => {
  const { userType, permission } = useUser();
  const unit = getUnitFromSS(Number(userType));
  const navigate = useNavigate();
  useEffect(() => {
    if (userType !== 3000 && !permission?.EnableDigital) navigate(path.listOfUnits);
  });
  if (!(userType !== 3000 && !permission?.EnableDigital))
    return Boolean(unit) || userType === 3000 ? (
      permission?.EnableDigital ? (
        <SchoolHome />
      ) : (
        <EducationHomeClassic />
      )
    ) : (
      <EducationHome />
    );
  else return <></>;
};

export default HomePage;
