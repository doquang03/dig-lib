import { useMutation, useQuery, type UseMutateAsyncFunction } from '@tanstack/react-query';
import { Modal, Radio, Spin, Switch, Tooltip, type ModalProps, type RadioChangeEvent } from 'antd';
import { connectionApis } from 'apis';
import { DetailedKeyData } from 'apis/connection.api';
import { AxiosResponse } from 'axios';
import { Button, Title } from 'components';
import dayjs from 'dayjs';
import { uniqueId } from 'lodash';
import { ChangeEvent, useMemo, useState } from 'react';
import { toast } from 'react-toastify';

const exceptThisSymbols = ['e', 'E', '+', '-', '.'];

type Props = {
  id?: string;
  loading?: boolean;
  onClose: VoidFunction;
  onSuccess: UseMutateAsyncFunction<
    AxiosResponse<
      ResponseApi<{
        Key: Key[];
        Count: number;
      }>,
      any
    >,
    unknown,
    void,
    unknown
  >;
} & ModalProps;

type Package = {
  lifeDays: number;
  keyType: 'isLimitedTime' | 'unending';
};

const UpdateKeyModal = (props: Props) => {
  const { open, id, loading, onClose, onSuccess, ...rest } = props;

  const [packages, setPackages] = useState<Package[]>([]);
  const [inputAcitvate, setInputActivate] = useState<number>(0);
  const [key, setKey] = useState<DetailedKeyData>({} as DetailedKeyData);
  const [isCopied, setIsCopied] = useState<boolean>(false);

  const { isLoading: loadingDetailedKey } = useQuery({
    queryKey: ['getDetailedKey', id],
    queryFn: () => connectionApis.getDetailedKey(id + ''),
    enabled: !!id && open,
    onSuccess: (data) => {
      setKey(data.data.Item);
    }
  });

  const { mutate: generateKey, isLoading: loadingGenerateKey } = useMutation({
    mutationFn: connectionApis.generateKey,
    onSuccess: (data) => {
      setKey((prevState) => {
        return { ...prevState, Value: data.data.Item.Key };
      });
      setIsCopied(false);
    }
  });

  const { mutate: updateKey, isLoading: loadingUpdateKey } = useMutation({
    mutationFn: connectionApis.updateDetailedKey,
    onSuccess: () => {
      toast.success('Cập nhật key thành công');
      onClose();
      onSuccess();
      setPackages([]);
      setKey({} as DetailedKeyData);
    }
  });

  const totalExtendDays = packages.reduce((total, _package) => (total += _package.lifeDays), 0) || 0;

  const extendDaysSuggestion = useMemo(() => {
    if (dayjs(key.ExpiredTime).isAfter(dayjs())) {
      return dayjs(key?.ExpiredTime).add(totalExtendDays, 'day').format('HH:mm:ss DD/MM/YYYY');
    }

    return dayjs().add(totalExtendDays, 'day').format('HH:mm:ss DD/MM/YYYY');
  }, [key.ExpiredTime, totalExtendDays]);

  const handleAddPackage = () => {
    const newPackage: Package = { lifeDays: 30, keyType: 'isLimitedTime' };

    setPackages((prevState) => [...prevState, newPackage]);
    setInputActivate((prevState) => prevState + 1);
  };

  const handleChangeKeyType = (e: RadioChangeEvent, _index: number) => {
    if (!e) return;

    setPackages(
      packages.map((_package, index) => {
        if (_index === index) {
          return { keyType: e.target.value, lifeDays: _package.lifeDays };
        }
        return _package;
      })
    );
    setInputActivate(_index);
  };

  const handleChangeLifeDays = (e: ChangeEvent<HTMLInputElement>, _index: number) => {
    if (!e) return;

    setPackages(
      packages.map((_package, index) => {
        if (_index === index) {
          return { keyType: _package.keyType, lifeDays: Math.ceil(+e.target.value) };
        }
        return _package;
      })
    );
  };

  const handleGenerateNewkey = () => !loadingGenerateKey && generateKey();

  const handleClose = () => {
    setPackages([]);
    onClose();
  };

  const handleChangeKeyStatus = (value: boolean) => {
    if (!key) {
      return;
    }

    setKey((prevState) => {
      return { ...prevState, IsActive: value };
    });
  };

  const handleSubmit = () => {
    const { IdKey, IsActive, NameMember, UnitWorkingId, Value } = key;
    !loadingUpdateKey &&
      updateKey({
        IsActive: IsActive,
        Key: Value,
        NameMember,
        WorkingId: UnitWorkingId,
        Id: IdKey,
        ListTimePackages:
          packages.some(({ keyType }) => keyType === 'unending') || !key.ExpiredTime
            ? [{ ExtensionTime: null }]
            : packages.map((pk) => {
                return { ExtensionTime: pk.lifeDays * 24 * 60 * 60 };
              }) || []
      });
  };

  const disabledAddingButton = packages.length === 3;
  const disabled = packages.some((_package) => _package.keyType === 'unending');

  return (
    <Modal closable={false} footer={null} {...rest} open={open} centered>
      <Title title='Cập nhật thông tin key' />

      <div className='mt-3 grid grid-cols-12 gap-3'>
        <p className='col-span-3 font-bold'>Đơn vị</p>

        <p className='col-span-8 font-bold'>{key?.NameMember || '--'}</p>
      </div>

      <div className='my-3 grid grid-cols-12 gap-3'>
        <p className='col-span-3 font-bold'>Key</p>

        <div className='col-span-8 flex items-center gap-2'>
          <Tooltip title={isCopied ? 'Copied' : 'Copy'}>
            <button
              className=' w-full border p-2 text-left hover:cursor-pointer'
              onClick={async () => {
                if ('clipboard' in navigator) {
                  setIsCopied(true);
                  return await navigator.clipboard.writeText(key.Value);
                } else {
                  setIsCopied(true);
                  return document.execCommand('copy', true, key.Value);
                }
              }}
            >
              <p>{key.Value} </p>
            </button>
          </Tooltip>

          <Button type='button' onClick={handleGenerateNewkey} disabled={loadingGenerateKey || loadingDetailedKey}>
            {loadingGenerateKey ? (
              <Spin />
            ) : (
              <svg width='24' height='24' viewBox='0 0 43 43' fill='none' xmlns='http://www.w3.org/2000/svg'>
                <path
                  d='M12.835 22.1323H11.3823C11.3825 19.9003 12.0773 17.7238 13.3703 15.9046C14.6634 14.0853 16.4905 12.7137 18.5982 11.9799C20.7059 11.2461 22.9897 11.1866 25.1328 11.8096C27.2759 12.4326 29.172 13.7072 30.558 15.4566V13.8512H32.0107V18.9361H26.9262V17.4833H30.1837C29.3669 16.0705 28.1931 14.8973 26.78 14.0814C25.3669 13.2654 23.7641 12.8353 22.1323 12.8342C17.0057 12.8342 12.835 17.0053 12.835 22.1323ZM31.4296 22.1323C31.4296 27.2594 27.2589 31.4304 22.1323 31.4304C20.5006 31.4293 18.8978 30.9993 17.4847 30.1833C16.0715 29.3673 14.8977 28.1941 14.081 26.7814H17.3384V25.3286H12.2539V30.4135H13.7066V28.8081C15.0927 30.5575 16.9887 31.8321 19.1318 32.455C21.2749 33.078 23.5587 33.0185 25.6665 32.2847C27.7742 31.551 29.6013 30.1793 30.8943 28.3601C32.1873 26.5409 32.8821 24.3643 32.8823 22.1323H31.4296Z'
                  fill='white'
                />
              </svg>
            )}
          </Button>
        </div>
      </div>

      <div className='my-3 grid grid-cols-12 gap-3'>
        <p className='col-span-3 font-bold'>Ngày hết hạn</p>

        <div className='col-span-8 flex w-full flex-grow flex-nowrap items-center gap-1'>
          {disabled ? (
            <p className='font-semibold text-tertiary-30'>Vĩnh viễn.</p>
          ) : (
            <>
              <p
                className={`whitespace-nowrap font-semibold  ${
                  !key.ExpiredTime ? 'text-tertiary-30' : 'text-danger-10'
                }`}
              >
                {key.ExpiredTime ? dayjs(key?.ExpiredTime).format('HH:mm:ss DD/MM/YYYY') : 'Vĩnh viễn'}
              </p>

              {!!packages.length && (
                <>
                  <svg width='18' height='12' viewBox='0 0 18 12' fill='none' xmlns='http://www.w3.org/2000/svg'>
                    <path
                      d='M4.37114e-07 7L14.17 7L10.59 10.59L12 12L18 6L12 -5.24537e-07L10.59 1.41L14.17 5L6.11959e-07 5L4.37114e-07 7Z'
                      fill='black'
                    />
                  </svg>

                  <p className='whitespace-nowrap font-semibold text-tertiary-30'>{extendDaysSuggestion}</p>
                </>
              )}
            </>
          )}
        </div>
      </div>

      <div className='my-3 grid grid-cols-12 gap-3'>
        <p className='col-span-3 font-bold'>Trạng thái key</p>

        <div className='col-span-8 flex items-center gap-1'>
          <Switch className='bg-primary-10' checked={key?.IsActive} onChange={handleChangeKeyStatus} />
        </div>
      </div>

      {key.ExpiredTime && (
        <button
          className={`flex items-center gap-1 rounded-md border-[1px] bg-white p-2 font-bold shadow-md outline-none ${
            disabledAddingButton || disabled || (loadingDetailedKey && !!id)
              ? 'cursor-not-allowed border-secondary-20 bg-secondary-30 text-secondary-10'
              : 'border-primary-10 text-primary-10'
          }`}
          onClick={handleAddPackage}
          disabled={disabledAddingButton || disabled || (loadingDetailedKey && !!id)}
        >
          <svg width='14' height='15' viewBox='0 0 14 15' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path
              d='M7 0C3.136 0 0 3.36 0 7.5C0 11.64 3.136 15 7 15C10.864 15 14 11.64 14 7.5C14 3.36 10.864 0 7 0ZM10.5 8.25H7.7V11.25H6.3V8.25H3.5V6.75H6.3V3.75H7.7V6.75H10.5V8.25Z'
              fill='#3472A2'
            />
          </svg>
          Gia hạn key
        </button>
      )}

      {packages.map((_package, index) => (
        <div key={uniqueId()} className='my-4'>
          <p className='font-bold text-danger-30'>Gói gia hạn {index + 1}:</p>

          <div className='flex items-center justify-between'>
            <div className='flex items-center gap-1'>
              <Radio
                onChange={(e) => handleChangeKeyType(e, index)}
                value={'isLimitedTime'}
                checked={_package.keyType === 'isLimitedTime'}
                disabled={disabled && index !== inputAcitvate}
              >
                Hết hạn sau
              </Radio>

              <div className='flex w-[90px] items-center rounded-lg border px-3'>
                <input
                  className='w-[31px] outline-none'
                  max={31}
                  value={_package.lifeDays}
                  disabled={_package.keyType !== 'isLimitedTime' || disabled}
                  onKeyPress={(event) => {
                    if (!/[0-9]/.test(event.key)) {
                      event.preventDefault();
                    }
                  }}
                  maxLength={3}
                  min={1}
                  onChange={(e) => handleChangeLifeDays(e, index)}
                  onBlur={(e) => {
                    if (!_package.lifeDays) {
                      setPackages(
                        packages.map((_package, index) => {
                          if (inputAcitvate === index) {
                            return { keyType: _package.keyType, lifeDays: 1 };
                          }
                          return _package;
                        })
                      );
                    }
                  }}
                  onKeyDown={(e) => exceptThisSymbols.includes(e.key) && e.preventDefault()}
                  autoFocus={inputAcitvate === index}
                  onFocus={() => setInputActivate(index)}
                />

                <div className='rounded-br-md rounded-tr-md border-l border-l-secondary-10 bg-secondary-20/30 py-3 px-2'>
                  <span className='text-xs'>Ngày</span>
                </div>
              </div>
            </div>

            <Radio
              disabled={disabled && index !== inputAcitvate}
              onChange={(e) => handleChangeKeyType(e, index)}
              value={'unending'}
              checked={_package.keyType === 'unending'}
            >
              Vĩnh viễn
            </Radio>
          </div>
        </div>
      ))}

      <div className='mt-3 flex items-center justify-end gap-2'>
        <Button
          variant='secondary'
          loading={loading || (loadingDetailedKey && !!id) || loadingUpdateKey}
          onClick={handleClose}
          type='button'
        >
          Quay về
        </Button>

        <Button
          loading={loading || (loadingDetailedKey && !!id) || loadingUpdateKey}
          type='submit'
          onClick={handleSubmit}
        >
          Cập nhật
        </Button>
      </div>
    </Modal>
  );
};

export default UpdateKeyModal;
