import { Outlet } from 'react-router-dom';

const DCCPage = () => {
  return <Outlet />;
};

export default DCCPage;
