import { Modal, ModalProps, Spin } from 'antd';

type Props = ModalProps & {
  open: boolean;
};

const Loading = (props: Props) => {
  const { open, ...rest } = props;

  return (
    <Modal open={open} footer={null} closable={false} {...rest} centered>
      <div className='flex items-center justify-center'>
        <h4 className='mr-2'>Hệ thống đang xử lý</h4>

        <Spin size='large' />
      </div>
    </Modal>
  );
};

export default Loading;
