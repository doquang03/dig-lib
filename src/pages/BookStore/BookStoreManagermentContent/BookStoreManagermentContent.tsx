import {
  BookFilled,
  CaretDownOutlined,
  CarryOutOutlined,
  DownloadOutlined,
  FormOutlined,
  InfoCircleOutlined,
  QrcodeOutlined
} from '@ant-design/icons';
import { useMutation, useQuery } from '@tanstack/react-query';
import { Checkbox, Collapse, Divider, Dropdown, Menu, Table, Tooltip } from 'antd';
import { ColumnsType, TableProps } from 'antd/es/table';
import { bookStoreApis, bookstoreTypeApis } from 'apis';
import { GetListSachCaBietByIdKho } from 'apis/bookstore.apis';
import { SettingIcon } from 'assets';
import classNames from 'classnames';
import { Button, Empty, Loading, ModalDelete, SizeChanger, Title, TitleDelete } from 'components';
import { ButtonCustomProps } from 'components/Button/Button';
import { path } from 'constants/path';
import { useUser } from 'contexts/user.context';
import 'css/BookStoreContent.css';
import dayjs from 'dayjs';
import { useColumnFilterTableProps, useQueryParams } from 'hooks';
import CreateReceipt from 'pages/Publications/AddPublication/components/CreateReceipt';
import UpdateLiquiBook from 'pages/Publications/AddPublication/components/UpdateLiquiBook';
import React, { Key, useMemo, useState } from 'react';
import { useCookies } from 'react-cookie';
import { useLocation, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { analysisColor, convertDate } from 'utils/utils';
import { ChangeBook, ChangeStore, CreatePoster, SettingDKCB, SettingModalBookStore, UpdateDKCB } from './components';
import ResetDKCBSelect from './components/ResetDKCBSelect';

const { Panel } = Collapse;

type BookResult = SachCaBiet & {
  TenSach: string;
  MaKiemSoat: string;
  MaCaBiet: string;
  GiaBia: string;
  TrangThai: string;
};

type exampleCheckStatus = {
  [key: string]: StatusBook;
};
type exampleCheckNCC = {
  [key: string]: NguonCungCap;
};

let locale = {
  emptyText: 'Không tìm được kết quả phù hợp'
};

const orderByLabel: Record<'bookName' | 'registerCode' | 'createdDate' | '', string> = {
  '': 'Sắp xếp theo',
  bookName: 'Tên sách',
  createdDate: 'Ngày vào sổ',
  registerCode: 'Mã đăng ký cá biệt'
};

const BookStoreManagermentContent = () => {
  const { state } = useLocation();
  const idKho = state ? state.idKho : undefined;
  const navigate = useNavigate();
  const fieldRef = React.useRef<HTMLInputElement>(null);
  const [listKhoSach, setListKhoSach] = useState<Array<BookStoreType>>();
  const [listKhoSachOrigin, setListKhoSachOrigin] = useState<Array<BookStoreType>>();
  const [totalKhoSach, setTotalKhoSach] = useState(0);
  const [listMaMau, setListMaMau] = useState<Array<Color>>([]);
  const [selectBookCS, setSelectBookCS] = useState<SachCaBiet[]>([]);
  const [visiableDelete, setVisiableDelete] = useState(false);
  const [visiableQR, setVisiableQR] = useState(false);
  const [visiablePhich, setVisiablePhich] = useState(false);
  const [visiableUpdateSCB, setVisiableUpdateSCB] = useState(false);
  const [visiableMaSCB, setVisiableMaSCB] = useState(false);
  const [visiableResetAll, setVisiableResetAll] = useState(false);
  const [visiableResetSelect, setVisiableResetSelect] = useState(false);
  const [visiableChangeStore, setVisiableChangeStore] = useState(false);
  const [visiableChangeBook, setVisiableChangeBook] = useState(false);
  const [visiableUpdateCardinal, setVisiableUpdateCardinal] = useState(false);
  const [khoSachSelect, setKhoSachSelect] = useState<BookStoreType>();
  const [List_NguonCungCap, setListNguonCungCap] = useState<NguonCungCap[]>([]);
  const [List_TrangThaiSach, setList_TrangThaiSach] = useState<StatusBook[]>([]);
  const [removeCache, setRemoveCache] = useState(false);
  const [orderBy, setOrderBy] = useState<'bookName' | 'registerCode' | 'createdDate' | ''>('createdDate');
  const [searchParams, setSearchParams] = useState<
    Partial<{
      MaKiemSoat: [string];
      TenSach: [string];
      MaKSCB: [string];
      SoChungTu: [string];
      SoVaoSoTongQuat: [string];
      NguonCungCap: [string];
      TrangThai: [string];
      NgayVaoSo: [string, string];
    }>
  >();
  const [isResetMaCB, setIsResetMaCB] = useState(false);
  const [openSetting, setOpenSetting] = useState<boolean>(false);
  const [showBookDeleted, setShowBookDeleted] = useState<boolean>(false);
  const [arrAvaliable, setArrAvaliable] = useState<Array<boolean>>([]);
  const [cookies, setCookie] = useCookies(['settings_bookstores']);
  const queryParams = useQueryParams();
  const [statusCheck, setStatusCheck] = useState<exampleCheckStatus>({});
  const [NCCCheck, setNCCCheck] = useState<exampleCheckNCC>({});
  const [page, setPage] = useState(1);
  const [paginationSize, setPaginationSize] = useState(30);
  const [col, setCol] = useState<keyof SachCaBiet | undefined>();

  const { getColumnSearchProps, getColumnFilterDateProps, clearAll } = useColumnFilterTableProps<SachCaBiet>((key) =>
    setCol(key)
  );

  const { isFetching, refetch: refetchCountBook } = useQuery({
    queryKey: ['LoaiKhoSach'],
    queryFn: () => bookstoreTypeApis.IndexByTreeCountBook(),
    onSuccess: (res) => {
      setListKhoSach(filterListKhoSach(res?.data?.Item?.ListKhoSach));
      setListKhoSachOrigin(res?.data?.Item?.ListKhoSach);
      setListMaMau(res?.data?.Item?.ListMaMau);
      if (!isFetchingSachCaBiet) {
        setVisiableDelete(false);
        setVisiableChangeStore(false);
      }
    }
  });

  const {
    data: dataListSachCaBiet,
    isLoading: isLoadingSachCaBiet,
    isFetching: isFetchingSachCaBiet,
    refetch: refetchSachCaBiet,
    remove
  } = useQuery({
    queryKey: [
      'SachCaBiet',
      khoSachSelect?.Id,
      page,
      paginationSize,
      queryParams,
      searchParams,
      showBookDeleted,
      orderBy
    ],
    queryFn: () => {
      let bodyReq: GetListSachCaBietByIdKho = {};
      bodyReq.page = page;
      bodyReq.pageSize = paginationSize;
      if (!!searchParams?.MaKiemSoat?.[0]) {
        bodyReq.MaKiemSoat = {
          truongSearch: searchParams?.MaKiemSoat?.[0],
          operatorSearch: 'contains'
        };
      }

      if (!!searchParams?.MaKSCB?.[0]) {
        bodyReq.MaKiemSoatCaBiet = {
          truongSearch: searchParams?.MaKSCB?.[0],
          operatorSearch: 'contains'
        };
      }

      if (!!searchParams?.TenSach?.[0]) {
        bodyReq.TenSach = {
          truongSearch: searchParams?.TenSach?.[0],
          operatorSearch: 'contains'
        };
      }

      if (!!searchParams?.SoChungTu?.[0]) {
        bodyReq.SoCT = {
          truongSearch: searchParams?.SoChungTu?.[0],
          operatorSearch: 'contains'
        };
      }

      if (!!searchParams?.SoVaoSoTongQuat?.[0]) {
        bodyReq.SoVSTQ = {
          truongSearch: searchParams?.SoVaoSoTongQuat?.[0],
          operatorSearch: 'contains'
        };
      }

      if (!!searchParams?.NgayVaoSo?.length) {
        bodyReq.NgayVaoSo = {
          Min: dayjs(searchParams.NgayVaoSo[0]).startOf('day').toISOString(),
          Max: dayjs(searchParams.NgayVaoSo[1]).endOf('day').toISOString()
        };
      }

      if (!!searchParams?.NguonCungCap?.[0]) {
        bodyReq.NguonCungCap = {
          truongSearch: searchParams?.NguonCungCap?.[0],
          operatorSearch: 'contains'
        };
      }

      if (!!searchParams?.TrangThai?.[0]) {
        bodyReq.TrangThai = {
          truongSearch: searchParams?.TrangThai?.[0],
          operatorSearch: 'contains'
        };
      }

      if (orderBy === 'bookName') {
        bodyReq.isSortTenSach = true;
      }

      if (orderBy === 'createdDate') {
        bodyReq.isSortNgayVaoSo = true;
      }

      if (orderBy === 'registerCode') {
        bodyReq.isSortMaDKCB = true;
      }

      return bookStoreApis.GetListSCB({ idKho: khoSachSelect?.Id + '', ...bodyReq }, false, showBookDeleted);
    },
    enabled: !!khoSachSelect?.Id,
    onSuccess: () => {
      if (!isFetching) {
        setVisiableDelete(false);
        setVisiableChangeStore(false);
      }
      setVisiableResetAll(false);
    }
  });

  const { mutate: ResetSachCaBietAll, isLoading: isLoadingResetCaBietAll } = useMutation({
    mutationFn: (idKho: string) => bookStoreApis.ResetSachCaBietAll(idKho),
    onSuccess() {
      remove();
      toast.success('Reset mã cá biệt thành công');
    }
  });

  const { mutate: DeleteMultiSCB, isLoading: isLoadingDeleteMultiSCB } = useMutation({
    mutationFn: (listSCB: Array<SachCaBiet>) => bookStoreApis.XoaBoSCB(listSCB, isResetMaCB),
    onSuccess: (res) => {
      remove();
      refetchCountBook();
      toast.success(res.data.Message);
      setIsResetMaCB(false);
      setSelectBookCS([]);
    }
  });

  const listSachCaBiet = useMemo(() => {
    const Item = dataListSachCaBiet?.data.Item;
    if (!Item) return [];
    let checkStatus = { ...statusCheck };
    Item?.List_TrangThai.forEach((element: StatusBook) => {
      checkStatus[element?.Id || '-1'] = element;
    });
    setStatusCheck(checkStatus);
    let checkNCC = { ...NCCCheck };
    Item?.List_NguonCungCap.forEach((element: NguonCungCap) => {
      checkNCC[element?.Id || '-1'] = element;
    });
    setNCCCheck(checkNCC);
    setTotalKhoSach(Item?.TotalCount || 0);

    setListNguonCungCap(Item.List_NguonCungCap);
    setList_TrangThaiSach(Item.List_TrangThai);
    return Item?.List_SachCB;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dataListSachCaBiet?.data.Item]);

  const filterListKhoSach = (data: any) => {
    type dicnationary = {
      [key: string]: any;
    };
    let checkParent: dicnationary = {};
    let final = [];
    for (let i = 0; i < data.length; i++) {
      let tempData = {
        Id: data[i].Id,
        IdParent: data[i].IdParent,
        Ten: data[i].Ten,
        CachTangMaCB: data[i].CachTangMaCB,
        MaKho: data[i].MaKho,
        MaCBCount: data[i].MaCBCount,
        CountBook: data[i].CountBook
      };
      if (checkParent[data[i].Id] === undefined) checkParent[data[i].Id] = tempData;
      if (data[i].IdParent === undefined || data[i].IdParent == null || data[i].IdParent === '') final.push(tempData);
      else {
        if (checkParent[data[i].IdParent] !== undefined) {
          if (checkParent[data[i].IdParent].children === undefined) checkParent[data[i].IdParent].children = [];
          checkParent[data[i].IdParent].children.push(tempData);
        }
      }
    }
    return final;
  };

  const renderElement = (bookStore: any) => {
    let arrColor: {
      Name: string;
    }[] = [];
    listMaMau.forEach((element) => {
      if (bookStore.Id === element.IdLienKet && element.KieuLienKet === 2) {
        arrColor = element.ListColor;
      }
    });
    const finalColor: string[] = analysisColor(arrColor);
    return (
      <Collapse key={'collapse' + bookStore.Id} ref={khoSachSelect?.Id === bookStore?.Id ? fieldRef : undefined}>
        <Panel
          collapsible='icon'
          className={classNames(
            (khoSachSelect?.Id === bookStore?.Id
              ? 'select-tab-khosach'
              : // eslint-disable-next-line no-useless-concat
                '') +
              ' ' +
              (Boolean(bookStore?.children) ? 'collapse-arrow' : 'collapse-no-arrow')
          )}
          extra={
            <div
              className='colorTab'
              onClick={() => {
                setPage(1);
                clearAll();
                setKhoSachSelect(bookStore);
                setSelectBookCS([]);
              }}
            >
              <div
                style={{
                  borderLeft: `15px solid ${finalColor[0]}`,
                  borderTop: `15px solid ${finalColor[1]}`,
                  borderRight: `15px solid ${finalColor[2]}`,
                  borderBottom: `15px solid ${finalColor[3]}`
                }}
              ></div>
            </div>
          }
          key={'panel' + bookStore.Id}
          header={
            <span
              className='headerTab'
              onClick={() => {
                setPage(1);
                clearAll();
                setKhoSachSelect(bookStore);
                setSelectBookCS([]);
              }}
            >
              {bookStore.Ten + ' (' + coutBook(bookStore) + ')'}
            </span>
          }
        >
          {bookStore.children?.map((x: BookStoreType) => {
            return renderElement(x);
          })}
        </Panel>
      </Collapse>
    );
  };

  const coutBook = (bookStore: BookStoreType) => {
    let count = bookStore?.CountBook ?? 0;
    bookStore.children?.forEach((x: BookStoreType) => {
      count = count + coutBook(x);
    });
    return count;
  };

  const onChange: TableProps<SachCaBiet>['onChange'] = (pagination, filters, sorter, extra) => {
    setSearchParams((prevState) => {
      return { ...prevState, ...filters };
    });
  };

  const columns: ColumnsType<SachCaBiet> = useMemo(() => {
    let cols: ColumnsType<SachCaBiet> = [];
    arrAvaliable?.[0] &&
      cols.push({
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        width: 50,
        render: (value: any, record: any, j: number) => (page - 1) * paginationSize + j + 1
      });

    arrAvaliable?.[1] &&
      cols.push({
        title: 'Mã kiểm soát',
        dataIndex: 'MaKiemSoat',
        key: 'MaKiemSoat',
        width: 80,
        render: (value: any, record: any) => record.MaKiemSoat || '',
        ...getColumnSearchProps('Mã kiểm soát', 'MaKiemSoat', col === 'MaKiemSoat')
      });

    arrAvaliable?.[2] &&
      cols.push({
        title: 'Mã sách ĐKCB',
        dataIndex: 'MaKSCB',
        key: 'MaKSCB',
        width: 100,
        onCell: (record) => ({
          className: record.IsDeleted ? 'text-danger-10' : ''
        }),
        ...getColumnSearchProps('Mã sách  ĐKCB', 'MaKSCB', col === 'MaKSCB')
      });

    arrAvaliable?.[3] &&
      cols.push({
        title: 'Tên sách',
        dataIndex: 'TenSach',
        key: 'TenSach',
        render: (value: any, record: any) => <p className='text-left'>{record.TenSach}</p> || '',
        ...getColumnSearchProps('Tên sách', 'TenSach', col === 'TenSach')
      });

    arrAvaliable?.[5] &&
      cols.push({
        title: 'Số chứng từ',
        dataIndex: 'SoChungTu',
        key: 'SoChungTu',
        width: 90,
        ...getColumnSearchProps('Số chứng từ', 'SoChungTu', col === 'SoChungTu')
      });

    arrAvaliable?.[6] &&
      cols.push({
        title: 'Số vào sổ tổng quát',
        dataIndex: 'SoVaoSoTongQuat',
        key: 'SoVaoSoTongQuat',
        width: 90,
        ...getColumnSearchProps('Số vào sổ tổng quát', 'SoVaoSoTongQuat', col === 'SoVaoSoTongQuat')
      });

    arrAvaliable?.[7] &&
      cols.push({
        title: 'Ngày vào sổ',
        dataIndex: 'NgayVaoSo',
        key: 'NgayVaoSo',
        width: 130,
        render: (value, { NgayVaoSo }) => <p>{convertDate(NgayVaoSo)}</p>,
        ...getColumnFilterDateProps('NgayVaoSo', col === 'NgayVaoSo')
      });

    arrAvaliable?.[8] &&
      cols.push({
        title: 'Nguồn cung cấp',
        dataIndex: 'NguonCungCap',
        key: 'NguonCungCap',
        width: 160,
        render: (value: any, record: any) => NCCCheck[record.IdNguonCungCap]?.TenNguonCungCap || '',
        ...getColumnSearchProps('Nguồn cung cấp', 'IdNguonCungCap', col === 'IdNguonCungCap')
      });

    arrAvaliable?.[4] &&
      cols.push({
        title: 'Tình trạng',
        dataIndex: 'TrangThai',
        key: 'TrangThai',
        width: 100,
        render: (value: any, record: any) => statusCheck[record.IdTrangThai]?.TenTT || '',
        ...getColumnSearchProps('Tình trạng sách', 'IdTrangThai', col === 'IdTrangThai')
      });

    cols.push({
      title: '',
      dataIndex: 'actions',
      key: 'actions',
      width: 100,
      render: (value, record) => {
        return (
          <div className='flex items-center justify-center gap-2 '>
            <Tooltip
              title={record.IdChiTietNhap ? 'Đã có trong sổ nhập kho' : 'Chưa có trong sổ nhập kho'}
              arrow={true}
            >
              <DownloadOutlined style={{ color: record.IdChiTietNhap ? '#08c' : 'gray' }} />
            </Tooltip>

            <Tooltip title={record.IdPhieuMuon ? 'Đang mượn' : 'Có sẵn'} arrow={true}>
              <CarryOutOutlined style={{ color: record.IdPhieuMuon ? '#08c' : 'gray' }} />
            </Tooltip>

            <Tooltip title={'Chưa được đặt mượn'} arrow={true}>
              <BookFilled style={{ color: 'gray' }} />
            </Tooltip>

            <Tooltip title={record.IsXuatQR ? 'Đã xuất QR' : 'Chưa xuất QR'} arrow={true}>
              <QrcodeOutlined style={{ color: record.IsXuatQR ? '#08c' : 'gray' }} />
            </Tooltip>
          </div>
        );
      }
    });

    return cols;
  }, [NCCCheck, arrAvaliable, getColumnFilterDateProps, getColumnSearchProps, page, paginationSize, statusCheck]);

  const onSelectChange = (_: Key[], selectedRowValue: SachCaBiet[]) => {
    setSelectBookCS(selectedRowValue);
  };

  const rowSelections = useMemo(() => {
    return {
      selectedRowKeys: selectBookCS.map((item) => item?.Id as string),
      preserveSelectedRowKeys: true,
      onChange: onSelectChange,
      getCheckboxProps: (record: SachCaBiet) => ({
        disabled: record.IsDeleted,
        name: record.TenSach
      })
    };
  }, [selectBookCS]);

  const handleExportSuccess = () => {
    selectBookCS.forEach((element) => {
      element.IsXuatQR = true;
    });
    setSelectBookCS([]);
    setVisiableQR(false);
    setVisiablePhich(false);
  };

  const handleUpdateSuccess = (isRemoveCache = false) => {
    setSelectBookCS([]);
    if (isRemoveCache) setRemoveCache(true);
    setVisiableUpdateSCB(false);
    setVisiableMaSCB(false);
    refetchSachCaBiet();
  };

  const handleResetAll = () => {
    ResetSachCaBietAll(khoSachSelect?.Id as string);
  };

  const handleResetSelect = () => {
    setPage(1);
    setPaginationSize(30);
    remove();
    setSelectBookCS([]);
  };

  const handleChangeStore = () => {
    setPage(1);
    setPaginationSize(30);
    remove();
    refetchCountBook();
    setSelectBookCS([]);
  };

  const handleUpdateDKCB = () => {};

  const handleChangeBook = () => {
    setSelectBookCS([]);
    setRemoveCache(true);
  };

  const handleDeleteBookCB = () => {
    let tempArray = [];
    for (let i = 0; i < selectBookCS.length; i++) {
      if (selectBookCS[i].IdPhieuMuon !== null && selectBookCS[i].IdPhieuMuon !== '') continue;
      selectBookCS[i].CreateDateTime = selectBookCS[i].NgayVaoSo as string;
      tempArray.push(selectBookCS[i]);
    }

    DeleteMultiSCB(tempArray);
  };

  useMemo(() => {
    if (idKho !== undefined && khoSachSelect === undefined)
      setKhoSachSelect(listKhoSach?.filter((_) => _.Id === idKho)[0]);
    else if (listKhoSach !== undefined && listKhoSach?.length > 0 && khoSachSelect === undefined)
      setKhoSachSelect(listKhoSach[0]);
  }, [idKho, khoSachSelect, listKhoSach]);

  const { isAllowedAdjustment } = useUser();

  useMemo(() => {
    if (removeCache && isFetchingSachCaBiet) {
      setRemoveCache(false);
      remove();
    }
  }, [isFetchingSachCaBiet, remove, removeCache]);

  useMemo(() => {
    if (arrAvaliable?.length === 0) {
      if (cookies.settings_bookstores) {
        setArrAvaliable(cookies.settings_bookstores);
      } else {
        setArrAvaliable([true, true, true, true, true, true, true, true, true]);
      }
    }
  }, [arrAvaliable?.length, cookies.settings_bookstores]);

  const handleShowColumns = () => {
    setOpenSetting(true);
  };

  const handleShowBookDeleted = () => setShowBookDeleted((prevState) => !prevState);

  return (
    <div className='bookstore-page p-5' id='bookstore-page-content'>
      <Title title='QUẢN LÝ KHO SÁCH' />
      <div className='grid w-full grid-cols-1 md:grid-cols-12'>
        <div className='relative col-span-2 mt-6 flex h-full w-full flex-col justify-start overflow-hidden'>
          <h2 className='mb-3 font-semibold text-primary-10'>Danh sách kho</h2>
          {listKhoSach?.map((x: BookStoreType) => {
            return renderElement(x);
          })}
        </div>

        <div className='custom-row col-span-10 px-2'>
          <div className='flex flex-wrap items-center md:flex-grow'>
            <div className={isAllowedAdjustment ? 'my-4 grow gap-1 md:flex md:md:flex-row' : 'hidden'}>
              {[
                {
                  label: 'Excel/Word',
                  items: [
                    {
                      key: 'excelnormal',
                      label: (
                        <span key='excelnormal' className='text-base'>
                          Thêm Excel thường
                        </span>
                      ),
                      onClick: () => {
                        navigate(`/KhoSach/ImportExcel_Thuong/${khoSachSelect?.Id}`);
                      }
                    },
                    {
                      key: 'exceldkcb',
                      label: (
                        <span key='exceldkcb' className='text-base'>
                          Thêm Excel sổ ĐKCB
                        </span>
                      ),
                      onClick: () => {
                        navigate(`/KhoSach/ImportExcel_SCB/${khoSachSelect?.Id}`);
                      }
                    },
                    {
                      key: 'exceldkcb_sgk',
                      label: (
                        <span key='exceldkcb_sgk' className='text-base'>
                          Thêm Excel SGK
                        </span>
                      ),
                      onClick: () => {
                        navigate(`/KhoSach/ImportExcel_SCB_SGK/${khoSachSelect?.Id}`);
                      }
                    },
                    {
                      key: 'dividerBookStore',
                      label: <Divider key='dividerBookStore' className='my-1' />
                    },
                    {
                      key: 'exportQR',
                      label: (
                        <span key='exportQR' className='text-base'>
                          Xuất Word QR
                        </span>
                      ),
                      onClick: () => {
                        setVisiableQR(true);
                      },
                      disabled: !selectBookCS.length
                    },
                    {
                      key: 'exportphich',
                      label: <span className='text-base'>Xuất Word Phích</span>,
                      onClick: () => {
                        setVisiablePhich(true);
                      },
                      disabled: !selectBookCS.length
                    }
                  ].filter((item) => {
                    if (khoSachSelect?.CachTangMaCB && +khoSachSelect?.CachTangMaCB === 2) {
                      if (item.key === 'exceldkcb') return false;
                    } else {
                      if (item.key === 'exceldkcb_sgk') return false;
                    }

                    if (item.key === 'exportQR' || item.key === 'exportphich' || item.key === 'dividerBookStore')
                      return true;

                    return true;
                  }),
                  variant: 'default'
                },
                {
                  label: 'Quản lý sách ĐKCB',
                  items: [
                    {
                      key: 'updateMaDKCB',
                      label: <span className='text-base'>Cập nhật số đăng kí cá biệt</span>,
                      onClick: () => {
                        setVisiableMaSCB(true);
                      },
                      disabled: selectBookCS.length !== 1
                    },
                    {
                      key: 'dividerBookStore-1',
                      label: <Divider className='my-1' />
                    },
                    {
                      key: 'ChangeStore',
                      label: <span className='text-base'>Chuyển sang kho khác</span>,
                      onClick: () => {
                        setVisiableChangeStore(true);
                      },
                      disabled: !selectBookCS.length
                    },
                    {
                      key: 'ChangeBook',
                      label: <span className='text-base'>Chuyển sang sách khác</span>,
                      onClick: () => {
                        setVisiableChangeBook(true);
                      },
                      disabled: !selectBookCS.length
                    },
                    {
                      key: 'dividerBookStore-2',
                      label: <Divider key='dividerBookStore' className='my-1' />
                    },
                    {
                      key: 'resetDKCB',
                      label: <span className='text-base'>Reset từ số ĐKCB được chọn</span>,
                      onClick: () => {
                        setVisiableResetSelect(true);
                      },
                      disabled: !selectBookCS.length
                    },
                    {
                      key: 'resetDKCBall',
                      label: <span className='text-base'>Reset toàn bộ số ĐKCB</span>,
                      onClick: () => {
                        setVisiableResetAll(true);
                      }
                    }
                  ].filter((item) => {
                    if (
                      item.key === 'ChangeStore' ||
                      item.key === 'ChangeBook' ||
                      item.key === 'dividerBookStore-2' ||
                      item.key === 'resetDKCB'
                    )
                      return true;

                    return true;
                  }),
                  variant: 'default',
                  onClick: () => {}
                },
                {
                  label: 'Sổ nhập xuất',
                  variant: 'default',
                  items: selectBookCS.length && [
                    {
                      key: 'sonhapkho',
                      label: <span className='text-base'>Thêm sổ nhập kho</span>,
                      onClick: () => {
                        let arr: BookResult[] = [];

                        selectBookCS.forEach((element: SachCaBiet) => {
                          if (!element.IdChiTietNhap) {
                            const temp: BookResult = {
                              ...element,
                              TenSach: element.TenSach as string,
                              MaCaBiet: element.MaKSCB as string,
                              MaKiemSoat: element.MaKiemSoat as string,
                              GiaBia: element.GiaBia as string,
                              TrangThai: statusCheck[element.IdTrangThai as string].TenTT as string
                            };
                            arr.push(temp);
                          }
                        });

                        if (!arr.length) {
                          toast.warning('Những sách cá biệt đang chọn đã được nhập kho');

                          return;
                        }

                        navigate(path.taoSoNhapKho, {
                          state: {
                            selectedSCB: arr
                          }
                        });
                      }
                    },
                    {
                      key: 'soxuatkho',
                      label: <span className='text-base'>Thêm sổ xuất kho</span>,
                      onClick: () => {
                        let arr: BookResult[] = [];
                        selectBookCS.forEach((element: SachCaBiet) => {
                          if (element?.IdPhieuMuon === null || element?.IdPhieuMuon === '') {
                            const temp: BookResult = {
                              ...element,
                              TenSach: element.TenSach as string,
                              MaCaBiet: element.MaKSCB as string,
                              MaKiemSoat: element.MaKiemSoat as string,
                              GiaBia: element.GiaBia as string,
                              TrangThai: statusCheck[element.IdTrangThai as string].TenTT as string
                            };
                            arr.push(temp);
                          }
                        });
                        navigate(path.taoPhieuXuatKho, {
                          state: {
                            selectedSCB: arr
                          }
                        });
                      }
                    }
                  ],
                  disabled: !selectBookCS.length
                },
                {
                  label: 'Chỉnh sửa',
                  variant: 'default',
                  onClick: () => {
                    setVisiableUpdateSCB(true);
                  },
                  disabled: !selectBookCS.length
                },
                {
                  label: 'Tải danh mục',
                  variant: 'default',
                  onClick: () => {
                    navigate(path.inDanhMuc);
                  }
                },
                {
                  label: 'Xóa',
                  variant: 'danger',
                  onClick: () => {
                    if (!selectBookCS.length) {
                      return;
                    }
                    setVisiableDelete(true);
                  },
                  disabled: !selectBookCS.length
                }
              ].map(({ label, items, variant, disabled, onClick }) => {
                const variantResult = disabled ? 'disabled' : variant;
                if (items) {
                  return (
                    <Dropdown
                      key={label}
                      menu={{
                        items
                      }}
                    >
                      <Button
                        variant={variantResult as ButtonCustomProps['variant']}
                        className={classNames('w-[100%] md:w-auto', {
                          'bg-gray-400': disabled,
                          'cursor-not-allowed': disabled,
                          'hover:bg-gray-400': disabled
                        })}
                        key={label}
                        onClick={onClick}
                        disabled={disabled}
                      >
                        {label}
                        <CaretDownOutlined />
                      </Button>
                    </Dropdown>
                  );
                } else
                  return (
                    <Button
                      variant={variantResult as ButtonCustomProps['variant']}
                      className={classNames('w-[100%] md:w-auto', {
                        'bg-gray-400': disabled,
                        'cursor-not-allowed': disabled,
                        'hover:bg-gray-400': disabled
                      })}
                      key={label}
                      onClick={onClick}
                      disabled={disabled}
                    >
                      {label}
                      {items === 0 && <CaretDownOutlined />}
                    </Button>
                  );
              })}
            </div>

            <div className='flex items-center justify-end gap-1 md:flex-grow'>
              <div className='flex items-center gap-1'>
                Sắp xếp theo:
                <Dropdown
                  menu={{
                    items: [
                      {
                        key: 'bookName',
                        label: <div>Tên sách</div>,
                        onClick: () => (orderBy === 'bookName' ? setOrderBy('') : setOrderBy('bookName')),
                        className: orderBy === 'bookName' ? 'bg-gray-300' : ''
                      },
                      {
                        key: 'registerCode',
                        label: <div>Mã đăng ký cá biệt</div>,
                        onClick: () => (orderBy === 'registerCode' ? setOrderBy('') : setOrderBy('registerCode')),
                        className: orderBy === 'registerCode' ? 'bg-gray-300' : ''
                      },
                      {
                        key: 'createdDate',
                        label: <div>Ngày vào sổ</div>,
                        onClick: () => (orderBy === 'createdDate' ? setOrderBy('') : setOrderBy('createdDate')),
                        className: orderBy === 'createdDate' ? 'bg-gray-300' : ''
                      }
                    ]
                  }}
                >
                  <div className='whitespace-nowrap rounded-md border-[1px] border-primary-10 bg-[#cce5ff] py-2 px-1 text-primary-10 hover:cursor-pointer'>
                    {orderByLabel[orderBy]}

                    <CaretDownOutlined className='ml-1' />
                  </div>
                </Dropdown>
              </div>

              <button
                className={classNames(
                  'm-1 inline-flex h-[40px]  w-[250px]',
                  'animation-card-change',
                  'justify-center',
                  'items-center',
                  'card-change-book',
                  'shrink-0',
                  'my-4',
                  'bookCount'
                )}
                style={{
                  cursor: khoSachSelect?.CachTangMaCB && +khoSachSelect?.CachTangMaCB === 3 ? 'pointer' : 'auto'
                }}
                onClick={() => {
                  if (khoSachSelect?.CachTangMaCB && +khoSachSelect?.CachTangMaCB !== 3) return;
                  setVisiableUpdateCardinal(true);
                }}
                disabled={!isAllowedAdjustment}
              >
                Số đếm ĐKCB:<span className='mx-1.5 font-bold'>{khoSachSelect?.MaCBCount || 0}</span> sách{' '}
                {khoSachSelect?.CachTangMaCB && +khoSachSelect?.CachTangMaCB === 3 && (
                  <FormOutlined className='mx-1.5 font-bold'></FormOutlined>
                )}
              </button>

              <Checkbox
                className='mr-1 whitespace-nowrap rounded-md border-[1px] border-primary-10 bg-[#cce5ff] py-2 px-1 text-primary-10'
                onChange={handleShowBookDeleted}
                checked={showBookDeleted}
              >
                Xem xuất kho
              </Checkbox>

              <button className={isAllowedAdjustment ? 'my-4 shrink-0' : 'hidden'} onClick={handleShowColumns}>
                <SettingIcon fill='#3472A2' />
              </button>
            </div>
          </div>
          <div>
            <Table
              loading={isLoadingSachCaBiet && !!khoSachSelect?.Id}
              columns={columns}
              rowSelection={rowSelections}
              pagination={{
                onChange(current, pageSize) {
                  setPage(current);
                  setPaginationSize(pageSize);
                },
                current: page,
                pageSize: paginationSize,
                total: totalKhoSach,
                defaultPageSize: paginationSize,
                hideOnSinglePage: true,
                showSizeChanger: false,
                showQuickJumper: true,
                locale: { jump_to: '', page: '' }
              }}
              dataSource={listSachCaBiet || []}
              scroll={{ x: 1280 }}
              rowKey={(record) => record.Id as string}
              className='custom-table'
              bordered
              locale={{
                emptyText: () => <Empty label={locale.emptyText} />
              }}
              onChange={onChange}
            />
          </div>

          <div
            className={classNames('relative', {
              // @ts-ignore
              'mt-[64px]': (page && paginationSize) >= totalKhoSach
            })}
          >
            <div className='absolute bottom-1'>
              <SizeChanger
                visible={!!listSachCaBiet?.length}
                value={paginationSize.toString()}
                currentPage={page.toString()}
                total={totalKhoSach.toString()}
                onChange={(pageSize) => {
                  setPage(1);
                  setPaginationSize(+pageSize);
                }}
              />
            </div>
          </div>
        </div>
      </div>
      <CreateReceipt
        ids={selectBookCS.map((item) => item.Id as string)}
        open={visiableQR}
        onHide={() => setVisiableQR(false)}
        onSuccess={() => {
          handleExportSuccess();
        }}
      />
      <CreatePoster
        ids={selectBookCS.map((item) => item.Id as string)}
        open={visiablePhich}
        onHide={() => setVisiablePhich(false)}
        onSuccess={handleExportSuccess}
        IdKho={khoSachSelect?.Id as string}
      />
      <UpdateLiquiBook
        open={visiableUpdateSCB}
        onHide={() => setVisiableUpdateSCB(false)}
        suppliers={List_NguonCungCap}
        bookStatus={List_TrangThaiSach}
        selectedSachCaBiets={selectBookCS as TrangThaiSachVM[]}
        onSuccess={handleUpdateSuccess}
      />
      <UpdateDKCB
        book={selectBookCS[0]}
        open={visiableMaSCB}
        onHide={() => setVisiableMaSCB(false)}
        onSuccess={() => {
          handleUpdateSuccess(true);
        }}
      />
      <ChangeStore
        ids={
          selectBookCS.map((item) => {
            return item.Id;
          }) as string[]
        }
        idKhoSach={khoSachSelect?.Id as string}
        onHide={() => {
          setVisiableChangeStore(false);
        }}
        isLoadingSachCaBiet={isFetchingSachCaBiet}
        onSuccess={handleChangeStore}
        open={!!selectBookCS.length && visiableChangeStore}
        ListKhoSach={listKhoSachOrigin as BookStoreType[]}
        ListMaMau={listMaMau}
      />
      <ChangeBook
        listCaBiet={selectBookCS}
        idKhoSach={khoSachSelect?.Id as string}
        onHide={() => {
          setVisiableChangeBook(false);
        }}
        isLoadingSachCaBiet={isFetchingSachCaBiet}
        onSuccess={handleChangeBook}
        open={!!selectBookCS.length && visiableChangeBook}
      />
      <ModalDelete
        open={!!selectBookCS.length && visiableDelete}
        closable={false}
        title={
          <>
            <h3 className='text-bold'>Bạn có muốn chắc chắn xóa những sách cá biệt này không?</h3>
            <p className='text-[16px] font-extralight leading-normal'>
              Bạn sẽ không thể khôi phục sau khi xóa, những sách cá biệt đang được mượn sẽ không thể xóa.
            </p>
            <Checkbox
              value={isResetMaCB}
              onChange={() => {
                setIsResetMaCB(!isResetMaCB);
              }}
              className='text-bold text-[16px]'
            >
              Thu hồi số đkcb
            </Checkbox>
          </>
        }
        loading={isLoadingDeleteMultiSCB || isFetchingSachCaBiet || isFetching}
        handleCancel={() => {
          setVisiableDelete(false);
        }}
        handleOk={() => {
          handleDeleteBookCB();
        }}
      >
        <div className='max-h-[300px] overflow-x-auto'>
          {selectBookCS.map(({ Id, MaKSCB, TenSach, IdPhieuMuon }, index) => {
            const inactive = IdPhieuMuon !== null && IdPhieuMuon !== '';

            return (
              <div key={Id} className='grid grid-cols-12 items-center border-b-2 px-2 py-2 font-bold'>
                <div className='col-span-1'>
                  <p className={inactive ? 'text-danger-10' : ''}>{index + 1} </p>
                </div>
                <div className='col-span-5'>
                  <p
                    className={classNames('truncate text-left', {
                      'text-danger-10': inactive
                    })}
                  >
                    {TenSach || ''}
                  </p>
                </div>
                <div className='col-span-6 grid grid-cols-12'>
                  <div className='col-span-6'>
                    <p
                      className={classNames('mr-4', {
                        'text-danger-10': inactive
                      })}
                    >
                      {MaKSCB}
                    </p>
                  </div>
                  <div className='col-span-6 flex-row items-center' style={{ display: 'flex' }}>
                    {inactive && (
                      <>
                        <p className='flex items-center gap-1 font-light text-danger-10'>
                          <InfoCircleOutlined style={{ color: 'red' }} />

                          <span className='text-[12px]'>{inactive && 'Không thể xóa'}</span>
                        </p>
                      </>
                    )}
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </ModalDelete>
      <ModalDelete
        labelOK='Xác nhận'
        className='model-delete'
        open={visiableResetAll}
        closable={false}
        loading={isLoadingResetCaBietAll || isFetchingSachCaBiet}
        handleCancel={() => {
          setVisiableResetAll(false);
        }}
        handleOk={handleResetAll}
        title={<TitleDelete firstText='Bạn xác nhận cài mới lại tất cả mã cá biệt trong kho'></TitleDelete>}
      ></ModalDelete>
      <ResetDKCBSelect
        khoSelect={khoSachSelect as BookStoreType}
        listIdSCB={selectBookCS.map((_) => _.Id as string)}
        onHide={() => {
          setVisiableResetSelect(false);
        }}
        onSuccess={handleResetSelect}
        open={!!khoSachSelect && visiableResetSelect}
      ></ResetDKCBSelect>
      <SettingModalBookStore
        arrAvaliable={arrAvaliable}
        open={openSetting}
        setVisibleModal={setOpenSetting}
        setArrAvaliable={setArrAvaliable}
        setCookie={setCookie}
      />
      <SettingDKCB
        khoSelect={khoSachSelect as BookStoreType}
        onHide={() => {
          setVisiableUpdateCardinal(false);
        }}
        onSuccess={handleUpdateDKCB}
        open={!!khoSachSelect && visiableUpdateCardinal}
      ></SettingDKCB>
      <Loading open={isLoadingDeleteMultiSCB || isFetching}></Loading>
    </div>
  );
};

export default BookStoreManagermentContent;
