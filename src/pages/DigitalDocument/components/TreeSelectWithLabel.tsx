import { TreeSelect, TreeSelectProps } from 'antd';
import { Button } from 'components';
import { Revert } from 'pages/TitleApproval/icons';

type Props = {
  label: string;
  required?: boolean;
  error?: string;
  onClickAddableButton?: VoidFunction;
  hasAddableButton?: boolean;
  hasMerge?: boolean;
  onMerge?: VoidFunction;
} & TreeSelectProps;
const TreeSelectWithLabel = (props: Props) => {
  const {
    label,
    required,
    error,
    onClickAddableButton,
    hasAddableButton = false,
    hasMerge = false,
    onMerge,
    ...rest
  } = props;
  return (
    <div className='grid grid-cols-12'>
      <label className='col-span-3 flex shrink-0 items-center font-bold'>
        {label}

        {required && <span className='text-danger-10'>*</span>}
      </label>

      <div className='col-span-9 flex items-center gap-1'>
        <TreeSelect className='w-[100%]' treeDefaultExpandAll {...rest} />
        {!!error && <p className='text-[14px] text-red-500'>{error}</p>}
        {hasAddableButton && (
          <Button onClick={onClickAddableButton}>
            <svg width='20' height='21' viewBox='0 0 20 21' fill='none' xmlns='http://www.w3.org/2000/svg'>
              <path
                d='M11.957 13.0723V20.7539H7.35156V13.0723H0.179688V8.58984H7.35156V0.996094H11.957V8.58984H19.1289V13.0723H11.957Z'
                fill='white'
              />
            </svg>
          </Button>
        )}
        {hasMerge ? (
          <button onClick={onMerge}>
            <Revert />
          </button>
        ) : null}
      </div>
    </div>
  );
};

export default TreeSelectWithLabel;
