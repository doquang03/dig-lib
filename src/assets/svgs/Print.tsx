import Icon from '@ant-design/icons';
import type { CustomIconComponentProps } from '@ant-design/icons/lib/components/Icon';

export const PrintSvg = () => {
  return (
    <svg width='24' height='24' viewBox='0 0 24 24' fill='#3472A2' xmlns='http://www.w3.org/2000/svg'>
      <path
        d='M11.9 3.61111H2.1C0.938 3.61111 0 4
.57889 0 5.77778V10.1111H2.8V13H11.2V10.1111H14V5.77778C14 4.57889 13.062 3.61111 11.9 3.61111ZM9.8 11.5556H4.2V7.94444H9.8V11.5556ZM11.9 6.5C11.515 6.5 11.2 6.175 11.2 5.77778C11.2 5.38056 11.515 5.05556 11.9 5.05556C12.285 5.05556 12.6 5.38056 12.6 5.77778C12.6 6.175 12.285 6.5 11.9 6.5ZM11.2 0H2.8V2.88889H11.2V0Z'
        fill='#3472A2'
      />
    </svg>
  );
};

const PrintIcon = (props: Partial<CustomIconComponentProps>) => {
  return <Icon component={() => <PrintSvg />} {...props} />;
};

export default PrintIcon;
