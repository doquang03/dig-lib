import axios, { HttpStatusCode } from 'axios';
import { REQUEST_TIME_OUT } from 'constants/config';
import { clearDataInSessionStorage, getUserInfoFromSS } from './auth';
import { isAxiosUnauthorizedError } from './utils';

const httpAuth = axios.create({
  baseURL: process.env.REACT_APP_API_AUTH,
  timeout: REQUEST_TIME_OUT,
  headers: { 'Content-Type': 'application/json' }
});

// Add a request interceptor
httpAuth.interceptors.request.use(
  function (config) {
    // check access token is existed
    let accessToken = getUserInfoFromSS()?.access_token;

    if (!!accessToken && config.headers) {
      config.headers.authorization = `Bearer ${accessToken}`;

      return config;
    }
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
httpAuth.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    if (isAxiosUnauthorizedError(error) && error.response?.status === HttpStatusCode.Unauthorized) {
      clearDataInSessionStorage();
    }
    return Promise.reject(error);
  }
);

export default httpAuth;
