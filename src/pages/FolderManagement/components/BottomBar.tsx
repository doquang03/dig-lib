import { useQueryClient } from '@tanstack/react-query';
import { Checkbox, Tooltip } from 'antd';
import { ImageModeViewIcon, ListIcon } from 'assets';
import { AxiosResponse } from 'axios';
import { useQueryParams } from 'hooks';
import { useParams } from 'react-router-dom';
import { useFolderManagement } from '../contexts/useFolderManagement';

function BottomBar() {
  const { viewMode, setViewMode, isPreviewMode, setIsPreviewMode, selectedBookIds } = useFolderManagement();

  const { id } = useParams();

  const { search } = useQueryParams();

  const queryClient = useQueryClient();

  const data = queryClient.getQueryData<AxiosResponse<ResponseApi<{ ListModel: []; count: number }>>>([
    'getBooksInFolders',
    id,
    search
  ]);

  function handleSwitchViewMode(mode: 'listing' | 'grid') {
    setViewMode(mode);
  }

  return (
    <div className='sticky bottom-1 left-0 flex items-center justify-between'>
      <div className='flex items-center gap-x-5'>
        <p className='text-xs text-[#A7A7A7]'>{data?.data.Item?.count || 0} tài liệu</p>

        {!!selectedBookIds.length && (
          <p className='text-xs text-[#A7A7A7]'>{selectedBookIds.length} tài liệu đang chọn</p>
        )}
      </div>

      <div className='flex items-center gap-x-5'>
        <Checkbox
          className='text-[#A7A7A7]'
          checked={isPreviewMode}
          onChange={(e) => setIsPreviewMode(e.target.checked)}
        >
          Xem trước
        </Checkbox>

        <div className='inline-flex items-center gap-2'>
          <Tooltip title='Xem dưới dạng danh sách'>
            <button
              onClick={() => handleSwitchViewMode('listing')}
              className={`p-2 transition-transform duration-150 ease-in ${
                viewMode === 'listing' ? 'border-[1px] border-[#2392C7] bg-[#CBE8F6] ' : ''
              }`}
            >
              <ListIcon />
            </button>
          </Tooltip>

          <Tooltip title='Xem dưới dạng lưới'>
            <button
              onClick={() => handleSwitchViewMode('grid')}
              className={`p-2 ${viewMode === 'grid' ? 'border-[1px] border-[#2392C7] bg-[#CBE8F6]' : ''}`}
            >
              <ImageModeViewIcon />
            </button>
          </Tooltip>
        </div>
      </div>
    </div>
  );
}

export default BottomBar;
