import Icon from '@ant-design/icons';
import type { CustomIconComponentProps } from '@ant-design/icons/lib/components/Icon';

const UploadSvg = ({ fill }: { fill: string }) => {
  return (
    <svg width='15' height='15' viewBox='0 0 15 15' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path
        d='M14.5 9.70588H10.5628V15H4.65711V9.70588H0.719971L7.60997 3.52941L14.5 9.70588ZM0.719971 1.76471V0H14.5V1.76471H0.719971Z'
        fill='white'
      />
    </svg>
  );
};

const UploadIcon = (props: Partial<CustomIconComponentProps & { fill: string }>) => {
  return <Icon component={() => <UploadSvg fill={props.fill || '#3472A2'} />} {...props} />;
};

export default UploadIcon;
