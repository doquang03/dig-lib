export const CACH_TANG_MA_CA_BIET_Value: Record<'' | '1' | '2' | '3', string> = {
  '': '',
  '3': 'Theo mã kho sách',
  '2': 'Theo nhan đề sách',
  '1': 'Theo tổng số sách'
};
