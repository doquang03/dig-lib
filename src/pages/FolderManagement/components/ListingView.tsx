import { Checkbox } from 'antd';
import { CheckboxChangeEvent } from 'antd/es/checkbox';
import { type BookInFolder } from 'apis/folder.apis';
import { Empty } from 'components';
import { path } from 'constants/path';
import { useWindowDimensions } from 'hooks';
import { type MouseEvent } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useFolderManagement } from '../contexts/useFolderManagement';

type Props = {
  data: BookInFolder[];
};

function ListingView(props: Props) {
  const { data } = props;

  const { height } = useWindowDimensions();

  const { id, bookId } = useParams();

  const navigate = useNavigate();

  const {
    contextBook: { book },
    setContextBook,
    selectedBookIds,
    setSelectedBookIds
  } = useFolderManagement();

  function handleShowContextBookMenu(e: MouseEvent<HTMLTableRowElement, globalThis.MouseEvent>, book: BookInFolder) {
    e.preventDefault();

    const { clientX: x, clientY: y } = e;

    setContextBook({ book, x, y });
  }

  function handleSelectBook(bookId: string) {
    if (selectedBookIds.includes(bookId)) {
      setSelectedBookIds(selectedBookIds.filter((item) => item !== bookId));
    } else {
      setSelectedBookIds((prevState) => [...prevState, bookId]);
    }
  }

  function handleCheckAll(e: CheckboxChangeEvent) {
    e.stopPropagation();

    for (let index = 0; index < data.length; index++) {
      const element = data[index];

      if (selectedBookIds.length === data.length) {
        setSelectedBookIds([]);
      } else {
        if (selectedBookIds.includes(element?.Id || '')) continue;
        else setSelectedBookIds((prevState) => [...prevState, element?.Id || '']);
      }
    }
  }

  return (
    <div
      className='relative flex-1 flex-col overflow-y-auto'
      style={{
        height: height - 50 - 42 - 150
      }}
    >
      {!!data.length && (
        <table width={'100%'}>
          <tbody>
            <tr className='sticky top-0 left-0 bg-[#F5F5F5] py-1'>
              <th className='text-center font-bold text-[#A7A7A7]' style={{ width: '5%' }}>
                {!!data.length && (
                  <Checkbox
                    type='checkbox'
                    onChange={handleCheckAll}
                    checked={selectedBookIds?.length === data.length}
                    className='sticky top-0 z-30 text-[#A7A7A7]'
                  />
                )}
              </th>
              <th style={{ width: '45%' }} className=' text-center font-bold text-[#A7A7A7]'>
                Tên tài liệu
              </th>

              <th style={{ width: '25%' }} className='whitespace-nowrap text-center text-[#A7A7A7]'>
                Tác giả
              </th>

              <th style={{ width: '10%' }} className='whitespace-nowrap text-center text-[#A7A7A7]'>
                Năm xuất bản
              </th>

              <th style={{ width: '15%' }} className='whitespace-nowrap text-center text-[#A7A7A7]'>
                ISBN
              </th>
            </tr>

            {!!data.length &&
              data?.map((item, index) => (
                <tr
                  className={`text-center transition-colors duration-150 ease-in hover:cursor-pointer  
                ${bookId === item.Id ? 'bg-[#BAE1FF] hover:bg-[#BAE1FF]' : ''}
                ${book?.Id === item.Id ? 'bg-[#3D9BE326] hover:bg-[#3D9BE326]' : 'hover:bg-[#BAE1FF]/30'}
               `}
                  onClick={() => navigate(`${path.folderManagement}/${id}/${item.Id}`)}
                  key={index}
                  onContextMenu={(e) => handleShowContextBookMenu(e, item)}
                >
                  <td>
                    <Checkbox
                      type='checkbox'
                      onChange={(e) => {
                        e.stopPropagation();
                        handleSelectBook(item?.Id || '');
                      }}
                      checked={selectedBookIds?.includes(item?.Id || '')}
                    />
                  </td>
                  <td className='flex items-center gap-x-2 py-1 text-left'>{item.TenSach || ''}</td>
                  <td>{item.TacGia}</td>
                  <td>{item.NamXuatBan}</td>
                  <td>{item.ISBN}</td>
                </tr>
              ))}
          </tbody>
        </table>
      )}

      {!data.length && (
        <div
          className={`grid place-items-center`}
          style={{
            height: height - 50 - 42 - 150
          }}
        >
          <Empty />
        </div>
      )}
    </div>
  );
}

export default ListingView;
